<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Tagline Entries</fullName>
        <field>Goals__c</field>
        <formula>&quot;N/A&quot;</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Closed BBI Q3 2014</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Idea.CommunityId</field>
            <operation>equals</operation>
            <value>BBI Q3 | 2014</value>
        </criteriaItems>
        <criteriaItems>
            <field>Idea.RecordTypeId</field>
            <operation>equals</operation>
            <value>BBI Q3 | 2014: Idea Record Type</value>
        </criteriaItems>
        <description>Workflow for closed BBI contest</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Tag%2C You%27re It%21</fullName>
        <actions>
            <name>Tagline Entries</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Idea.CommunityId</field>
            <operation>contains</operation>
            <value>Tag,You&apos;re It!</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
