<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>QE %7C RFQ QuoteGenStatus Filed update QLI</fullName>
        <description>Update QuoteGenStatus &quot;Error&quot; when any QLI Task Status is in Error status</description>
        <field>Status__c</field>
        <literalValue>Error</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>QuoteID__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>QE %7C QLI Error Status Update</fullName>
        <actions>
            <name>QE %7C RFQ QuoteGenStatus Filed update QLI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote_Line_Item__c.Task_Status__c</field>
            <operation>equals</operation>
            <value>Warning</value>
        </criteriaItems>
        <description>Update Qute status &apos;Error&apos; whenever a QLI status changes to Warning</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
