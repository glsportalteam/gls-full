<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>QE %7C QA IsAssignmentUpdated filed update</fullName>
        <description>Update IsAssignmentUpdated True, whenever Source Language values changes</description>
        <field>isAssignmentUpdated__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>QE %7C isAssignmentUpdate change</fullName>
        <actions>
            <name>QE %7C QA IsAssignmentUpdated filed update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule is created for updating isAssignmentUpdated true whenever source language get changes</description>
        <formula>ISCHANGED(Source_Language__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
