<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Community user signup notification</fullName>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>unfiled$public/Community_Signup_Lead</template>
    </alerts>
    <rules>
        <fullName>Community Lead Assignment rule</fullName>
        <actions>
            <name>Community user signup notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED( OwnerId ) &amp;&amp;  (TEXT(Status)== &apos;Open&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
