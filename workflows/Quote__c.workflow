<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>QE %7C GLS BDM Quote Review Submitted</fullName>
        <ccEmails>portal@globallanguages.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <template>GLS_Templates/BDM_Review_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>QE %7C GLS Client Quote Review Accepted</fullName>
        <ccEmails>portal@globallanguages.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>Submitter_Email_Id__c</field>
            <type>email</type>
        </recipients>
        <template>GLS_Templates/RFQ_Reveiw_Accepted</template>
    </alerts>
    <alerts>
        <fullName>QE %7C GLS Client Quote Review Rejected</fullName>
        <ccEmails>portal@globallanguages.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>Submitter_Email_Id__c</field>
            <type>email</type>
        </recipients>
        <template>GLS_Templates/RFQ_Reveiw_Rejected</template>
    </alerts>
    <alerts>
        <fullName>QE %7C GLS Client Quote Review Submitted</fullName>
        <ccEmails>portal@globallanguages.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Contact_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <template>GLS_Templates/Client_Review_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>QE %7C GLS Quote Reivew Submitted</fullName>
        <ccEmails>portal@globallanguages.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <template>GLS_Templates/GLS_Quote_Reveiw</template>
    </alerts>
    <alerts>
        <fullName>QE %7C GLS User Quote Review Accepted</fullName>
        <ccEmails>portal@globallanguages.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>Submitter_Email_Id__c</field>
            <type>email</type>
        </recipients>
        <template>GLS_Templates/GLS_Quote_Review_Accepted</template>
    </alerts>
    <alerts>
        <fullName>QE %7C GLS User Quote Review Rejected</fullName>
        <ccEmails>portal@globallanguages.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>Submitter_Email_Id__c</field>
            <type>email</type>
        </recipients>
        <template>GLS_Templates/GLS_Quote_Review_Rejected</template>
    </alerts>
    <alerts>
        <fullName>QE %7C QLI Analyze Fails</fullName>
        <protected>false</protected>
        <recipients>
            <field>Quote_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <template>GLS_Templates/Quote_Analyze_Error</template>
    </alerts>
    <alerts>
        <fullName>Reminder to submit Quote</fullName>
        <ccEmails>portal@globallanguages.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>Contact_Hidden__c</field>
            <type>contactLookup</type>
        </recipients>
        <template>GLS_Templates/Reminder_to_submit_Quote</template>
    </alerts>
    <fieldUpdates>
        <fullName>QE %7C RFQ QuoteGenStatus</fullName>
        <field>Status__c</field>
        <literalValue>Expired</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QE %7C Update  Submitter Email</fullName>
        <field>Submitter_Email_Id__c</field>
        <formula>$User.Email</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QE %7C Update  Submitter Email Empty</fullName>
        <field>Submitter_Email_Id__c</field>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QE %7C Update Approve Date</fullName>
        <field>Approval_Date__c</field>
        <formula>Today()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QE %7C Update Approve Date Empty</fullName>
        <field>Approval_Date__c</field>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QE %7C Update AssigneTo Empty</fullName>
        <field>Assigned_To__c</field>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QE %7C Update IsSentAprToBDM False</fullName>
        <field>isAprSentToBDM__c</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QE %7C Update Quote Number</fullName>
        <field>Quote_Number__c</field>
        <formula>SUBSTITUTE(Name,&apos;Q-&apos;,&apos;&apos;)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QE %7C Update QuoteGenStatus Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QE %7C Update QuoteGenStatus InPreparation</fullName>
        <field>Status__c</field>
        <literalValue>In Preparation</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QE %7C Update QuoteGenStatus Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QE %7C Update QuoteGenStatus Released</fullName>
        <field>Status__c</field>
        <literalValue>Released</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QE %7C Update QuoteGenStatus Reviewed</fullName>
        <field>Status__c</field>
        <literalValue>Reviewed</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QE %7C Update QuoteStatus In Review</fullName>
        <field>Status__c</field>
        <literalValue>In Review</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QE %7C Update isSentForApprovalToClient</fullName>
        <field>isAprSentToClient__c</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QE %7C UpdateApprovalStateFalse</fullName>
        <field>isSentforApproval__c</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QE %7C UpdateIsSentforApproval</fullName>
        <field>isSentforApproval__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Q to O</fullName>
        <field>Project_Number__c</field>
        <formula>SUBSTITUTE(Name,&apos;Q&apos;,&apos;O&apos;)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Quote Number if Request a Quote</fullName>
        <field>Project_Number__c</field>
        <formula>SUBSTITUTE(Name,&apos;Q-&apos;,&apos;&apos;)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Quote number of Draft</fullName>
        <field>Project_Number__c</field>
        <formula>SUBSTITUTE(Name, &apos;Q&apos;, &apos;D-&apos;&amp;TEXT(DAY(TODAY()))&amp;&apos;-&apos;&amp;TEXT(YEAR(TODAY())))</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update quote release date</fullName>
        <field>Submissiondate__c</field>
        <formula>now()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update submission date</fullName>
        <field>Submission_date__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BDM Assignment Creation</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>In Preparation</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Business,Sales</value>
        </criteriaItems>
        <description>Rule created for Tasks to be created for Contracts Team upon Quote Request by BDM</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email reminder after 7 days of quote request</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.CreatedDate</field>
            <operation>lessThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote__c.Is_Quote_requested__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Email reminder after 7 days of quote request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>QE %7C Custom Client Approval Send process</fullName>
        <actions>
            <name>QE %7C GLS Client Quote Review Submitted</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>QE %7C UpdateIsSentforApproval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.isAprSentToClient__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote__c.isSentforApproval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>Released</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>QE %7C Custom Client Aprove Process</fullName>
        <actions>
            <name>QE %7C Update Approve Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QE %7C Update isSentForApprovalToClient</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QE %7C UpdateApprovalStateFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Released</value>
        </criteriaItems>
        <description>Work flow to upde fields and notify user if client approves the qute</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>QE %7C Custom Client Reject Process</fullName>
        <actions>
            <name>QE %7C Update Approve Date Empty</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QE %7C Update isSentForApprovalToClient</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QE %7C UpdateApprovalStateFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>QE %7C QLI Analyzing Unsuccessful</fullName>
        <actions>
            <name>QE %7C QLI Analyze Fails</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This Rule will execute when Quote Status changes to Error</description>
        <formula>ISCHANGED( Status__c ) &amp;&amp; ISPICKVAL(Status__c, &apos;Error&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>QE %7C Update Expire Status</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Expired</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote__c.Quote_Due_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Quote status should updated to expire after 90 days from the date of submission</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>QE %7C Update Quote Number</fullName>
        <actions>
            <name>QE %7C Update Quote Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If no Parent Quote is associayed with Quote, Update Quote Number to Name</description>
        <formula>Original_Quote_Id__c  =  null</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Quote Submission Date Update</fullName>
        <actions>
            <name>Update submission date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>New,Direct Order</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote number of Draft</fullName>
        <actions>
            <name>Update Quote number of Draft</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Quote release date update</fullName>
        <actions>
            <name>Update quote release date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>Released</value>
        </criteriaItems>
        <description>Quote release date update when quote status is released</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Quote Number if Request a Quote</fullName>
        <actions>
            <name>Update Quote Number if Request a Quote</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.Is_Quote_requested__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update_Quote_Number</fullName>
        <actions>
            <name>Update Q to O</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Quote Number, When Status changes to Direct Order and set Reference to Quote Name</description>
        <formula>AND(ISCHANGED( Status__c ), ISPICKVAL(Status__c , &apos;Direct Order&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
