<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>LMS %7C Email Alert for %27Request Optional Training%27</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>dervin@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>silvie@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>victoria@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <template>GLS_Templates/LMS_Request_for_Training_Text</template>
    </alerts>
    <alerts>
        <fullName>LMS %7C Training Enroll Notification</fullName>
        <ccEmails>silvie@globallanguages.com;joyce@globallanguages.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>User_Name__c</field>
            <type>userLookup</type>
        </recipients>
        <template>GLS_Templates/LMS_Training_Enrollment_Notify</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update status</fullName>
        <field>Status__c</field>
        <literalValue>Re-take required</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>LMS %7C Training Enrollment Notify</fullName>
        <actions>
            <name>LMS %7C Training Enroll Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notify LMS user about Training enrollment</description>
        <formula>ISCHANGED(Due_Date__c) || (CreatedDate =  LastModifiedDate)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LMS %7C update status if failed</fullName>
        <actions>
            <name>Update status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_Training__c.Result__c</field>
            <operation>equals</operation>
            <value>Fail</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Request New Training</fullName>
        <actions>
            <name>LMS %7C Email Alert for %27Request Optional Training%27</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email triggered when Request New Training is chosen</description>
        <formula>IsAutoEnrolled__c==false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
