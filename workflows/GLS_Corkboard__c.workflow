<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>%5BALL%5D PIN %7C Assignment Email Alert</fullName>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Public/ALL_PIN_Assignment_Template</template>
    </alerts>
    <alerts>
        <fullName>%5BALL%5D PIN %7C Cancelled %28Status%29 Alert</fullName>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Public/DTP_QA_TM_PIN_Status_Changed_Template</template>
    </alerts>
    <alerts>
        <fullName>%5BALL%5D PIN %7C New Request Alert</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>dervin@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <template>Public/ALL_PIN_New_Request_Template</template>
    </alerts>
    <alerts>
        <fullName>%5BALL%5D PIN %7C Priority Alert</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>dervin@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <template>Public/ALL_PIN_Priority_Request_Template</template>
    </alerts>
    <alerts>
        <fullName>%5BALL%5D PIN %7C Project Status Alert</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>dervin@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <template>Public/ALL_PIN_Project_Status_Template</template>
    </alerts>
    <alerts>
        <fullName>%5BALL%5D PIN %7C Rejected %28Status%29 Alert</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>dervin@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <template>Public/ALL_PIN_Rejected_Template</template>
    </alerts>
    <alerts>
        <fullName>%5BALL%5D PIN %7C Unassigned Alert</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>dervin@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <template>Public/ALL_PIN_Unassigned_Template</template>
    </alerts>
    <alerts>
        <fullName>%5BDTP%5D%5BTM%5D Status Change Email Alert</fullName>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Public/DTP_QA_TM_PIN_Status_Changed_Template</template>
    </alerts>
    <alerts>
        <fullName>%5BQA%5D PIN %7C Closing Actions Alert</fullName>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Public/QA_PIN_QA_Closing_Actions_Template</template>
    </alerts>
    <alerts>
        <fullName>%5BQA%5D PIN %7C Project On Time Alert</fullName>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <template>Public/QA_PIN_Files_Received_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>%5BALL%5D PIN %7C Assigned Field Update</fullName>
        <description>Reference: Workflow PM-QA_Final.docx | DTP/TM Workflow
Source: Cony Rolon | Source: Viviana Bertinetto
Date: 12/22/2014</description>
        <field>Status__c</field>
        <literalValue>Assigned</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>%5BALL%5D PIN %7C Closed %28Status%29</fullName>
        <description>[ALL] To update the Completed field with a date.</description>
        <field>Completed__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>%5BALL%5D PIN %7C New %28Project Status%29</fullName>
        <field>Project_Status__c</field>
        <literalValue>New</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>%5BALL%5D PIN %7C New Request %28Status%29</fullName>
        <description>New request submitted with a PIN greater than 1, update status to NEW</description>
        <field>Status__c</field>
        <literalValue>New</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>%5BALL%5D PIN%7C Fetch Record ID Field Update</fullName>
        <field>Is_Cloned__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>%5BQA%5D PIN %7C On Track %28Project Status%29</fullName>
        <description>Field: Status will update from New to On Track</description>
        <field>Project_Status__c</field>
        <literalValue>On Track</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ApsicUpdate</fullName>
        <field>ApSIC_Report__c</field>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assigned To Update</fullName>
        <field>Assigned_To__c</field>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Link to Target Update</fullName>
        <field>Link_to_Target_Folder__c</field>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>%5BALL%5D PIN %7C Acceptance Rule</fullName>
        <actions>
            <name>%5BALL%5D PIN %7C Assignment Email Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>%5BALL%5D PIN %7C Assigned Field Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Reference: Workflow PM-QA_Final.docx | Reference: DTP/TM Workflow
Source: Cony Rolon | Source: Viviana Bertinetto
Date: 12/22/2014</description>
        <formula>RecordType.Name = &apos;QA Request&apos;&amp;&amp;  Assigned_To__r.IsActive = true||
RecordType.Name = &apos;DTP Request&apos;&amp;&amp;  Assigned_To__r.IsActive = true||
RecordType.Name = &apos;TM Request&apos;&amp;&amp;  Assigned_To__r.IsActive = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>%5BALL%5D PIN %7C Cancelled Request Rule</fullName>
        <actions>
            <name>%5BALL%5D PIN %7C Cancelled %28Status%29 Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Triggers alert to dept. for cancellations initiated by Owner</description>
        <formula>OwnerId  =  LastModifiedById &amp;&amp;
ISPICKVAL(Status__c, &apos;Cancelled&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>%5BALL%5D PIN %7C Clear Fields Upon Clone</fullName>
        <actions>
            <name>ApsicUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Assigned To Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Link to Target Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>GLS_Corkboard__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>GLS_Corkboard__c.Project_Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>%5BALL%5D PIN %7C Closed Rule</fullName>
        <actions>
            <name>%5BALL%5D PIN %7C Closed %28Status%29</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>GLS_Corkboard__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>%5BALL%5D PIN %7C New Request Rule</fullName>
        <actions>
            <name>%5BALL%5D PIN %7C New %28Project Status%29</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>%5BALL%5D PIN %7C New Request %28Status%29</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>GLS_Corkboard__c.Name</field>
            <operation>greaterThan</operation>
            <value>00001</value>
        </criteriaItems>
        <description>Rule to change status to New</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>%5BALL%5D PIN %7C Priority Rule</fullName>
        <actions>
            <name>%5BALL%5D PIN %7C Priority Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Rule to trigger an alert to QA when RUSH priority is selected, and User &gt; Dept. when Priority changes</description>
        <formula>ISCHANGED(Priority__c) &amp;&amp;
OR (
ISPICKVAL(Priority__c, &apos;Rush&apos;)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>%5BALL%5D PIN %7C Rejected %28Status%29 Rule</fullName>
        <actions>
            <name>%5BALL%5D PIN %7C Rejected %28Status%29 Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>GLS_Corkboard__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>Triggers alert for &quot;Rejected&quot; status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>%5BALL%5D Project Status Change Rule</fullName>
        <actions>
            <name>%5BALL%5D PIN %7C Project Status Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>GLS_Corkboard__c.Project_Status__c</field>
            <operation>equals</operation>
            <value>Delayed,On Hold,Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>GLS_Corkboard__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>DTP Request,QA Request,TM Request</value>
        </criteriaItems>
        <description>Triggers alert to &quot;Assigned To&quot; regarding the change of &quot;Project Status&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>%5BDTP%5D%5BTM%5D PIN %7C New Request Rule</fullName>
        <actions>
            <name>%5BALL%5D PIN %7C New Request Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>GLS_Corkboard__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>GLS_Corkboard__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>DTP Request,TM Request</value>
        </criteriaItems>
        <description>Triggers New Request email alert</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>%5BDTP%5D%5BTM%5D PIN %7C Status Changed Rule</fullName>
        <actions>
            <name>%5BDTP%5D%5BTM%5D Status Change Email Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Triggers alert to Owner if &apos;Status&apos; changes</description>
        <formula>ISCHANGED(Status__c) &amp;&amp;
OR(
ISPICKVAL(Status__c, &apos;In Progress&apos;),
ISPICKVAL(Status__c, &apos;Awaiting Confirmation&apos;),
ISPICKVAL(Status__c, &apos;Delayed&apos;),
ISPICKVAL(Status__c, &apos;Cancelled&apos;)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>%5BQA%5D PIN %7C Project On Time</fullName>
        <actions>
            <name>%5BQA%5D PIN %7C Project On Time Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>%5BQA%5D PIN %7C On Track %28Project Status%29</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3) OR 4</booleanFilter>
        <criteriaItems>
            <field>GLS_Corkboard__c.Link_to_Target_Folder__c</field>
            <operation>contains</operation>
            <value>L10N</value>
        </criteriaItems>
        <criteriaItems>
            <field>GLS_Corkboard__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>QA Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>GLS_Corkboard__c.Expected_Date_In__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>GLS_Corkboard__c.Link_to_Compared_File__c</field>
            <operation>contains</operation>
            <value>L10N</value>
        </criteriaItems>
        <description>Reference: Workflow PM-QA_Final.docx
Section: PM&lt;&gt;QA Request – Project on time
Source: Cony Rolon
Date: 12/22/2014</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>%5BQA%5D PIN %7C QA Closing Actions Rule</fullName>
        <actions>
            <name>%5BQA%5D PIN %7C Closing Actions Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>%5BALL%5D PIN %7C Closed %28Status%29</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>GLS_Corkboard__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>GLS_Corkboard__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>QA Request</value>
        </criteriaItems>
        <description>QA_Findings.docx
Source Cony Rolon
Routes QA Findings Report to Owner when status updated to &apos;Closed&apos;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
