<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>DTP %7C Feedback Submitted</fullName>
        <ccEmails>dervin@globallanguages.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>viviana@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <template>GLS_Templates/DTP_Feedback_Submitted</template>
    </alerts>
    <alerts>
        <fullName>Feedback Confirmation</fullName>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>GLS_Templates/Feedback_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>PM Feedback %7C QA Reply</fullName>
        <ccEmails>cony@globallanguages.com; jonathan@globallanguages.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <template>Public/QA_Thank_you_for_your_feedback</template>
    </alerts>
    <alerts>
        <fullName>QA %7C Feedback Submitted</fullName>
        <ccEmails>dervin@globallanguages.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>cony@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <template>GLS_Templates/QA_Feedback_Submitted</template>
    </alerts>
    <alerts>
        <fullName>TM %7C Feedback Submitted</fullName>
        <ccEmails>dervin@globallanguages.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>viviana@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <template>GLS_Templates/TM_Feedback_Submitted</template>
    </alerts>
    <alerts>
        <fullName>Thanks for your feedback</fullName>
        <ccEmails>manelyn@globallanguages.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>alejandra@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>andrzej@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>carine@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>christine@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mariam@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sarah@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <template>Public/PM_Feedback_VM_reply</template>
    </alerts>
    <rules>
        <fullName>DTP %7C Feedback Submitted</fullName>
        <actions>
            <name>DTP %7C Feedback Submitted</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Feedback Confirmation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule will send an email alert to DTP when feedback has been submitted.</description>
        <formula>ISBLANK(DTP_Specialist__c) = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>QA %7C Feedback Submitted</fullName>
        <actions>
            <name>QA %7C Feedback Submitted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule will sent an email alert to QA management when QA feedback has been submitted.</description>
        <formula>ISBLANK(QA_Specialist__c ) = False</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>QA - PM Feedback reply</fullName>
        <actions>
            <name>PM Feedback %7C QA Reply</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PM_Feedback__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>GLS Internal Resource Feedback</value>
        </criteriaItems>
        <criteriaItems>
            <field>PM_Feedback__c.Closed_Internal_QAuse_only__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TM %7C Feedback Submitted</fullName>
        <actions>
            <name>Feedback Confirmation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>TM %7C Feedback Submitted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule will send an email alert to TM when feedback has been submitted.</description>
        <formula>ISBLANK(TM_Specialist__c) = False</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VM - PM Feedback reply</fullName>
        <actions>
            <name>Thanks for your feedback</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PM_Feedback__c.Closed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Alert VM team on PM feedback reply and actions taken by VCO</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
