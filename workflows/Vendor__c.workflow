<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert%3A Inactive Vendor</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>Vendor Management</recipient>
            <type>role</type>
        </recipients>
        <template>Public/VM_DNU_Vendor</template>
    </alerts>
    <alerts>
        <fullName>Email to alert VM that a vendor has been Inactivated</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>dervin@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>manelyn@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <template>Public/VM_DNU_Vendor</template>
    </alerts>
    <alerts>
        <fullName>LEAD has been converted to ACTIVE</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>alejandra@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>andrzej@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>carine@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>christine@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>manelyn@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mariam@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sarah@globallanguages.com.full</recipient>
            <type>user</type>
        </recipients>
        <template>Public/VM_NEW_Active_Vendor</template>
    </alerts>
    <alerts>
        <fullName>VM %7C Vendor Status Updated</fullName>
        <ccEmails>dervin@globallanguages.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>[VM]: Non-Restricted VMs</recipient>
            <type>group</type>
        </recipients>
        <template>Public/VM_Vendor_Status_Updated</template>
    </alerts>
    <fieldUpdates>
        <fullName>Status Lead to Active</fullName>
        <description>Since when the vendor is Active in our Database [Lock the date]</description>
        <field>Status_Last_Modified__c</field>
        <formula>TODAY()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>VM %7C Vendor Status Updated</fullName>
        <actions>
            <name>VM %7C Vendor Status Updated</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Status Lead to Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Requested by MH_10-09-2014 | Change date when status is updated.</description>
        <formula>ISCHANGED(Vendor_Status__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VM - Inactive Vendors - Bye</fullName>
        <actions>
            <name>Email to alert VM that a vendor has been Inactivated</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Vendor__c.Vendor_Status__c</field>
            <operation>equals</operation>
            <value>Inactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Vendor__c.Vendor_Status__c</field>
            <operation>equals</operation>
            <value>Inactive DNU</value>
        </criteriaItems>
        <criteriaItems>
            <field>Vendor__c.Vendor_Status__c</field>
            <operation>equals</operation>
            <value>Not Applicable</value>
        </criteriaItems>
        <description>Alert VM when an ACTIVE vendor has been converted to INACTIVE
Deactivated per Manelyn on 12-03-2014</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VM - New Active vendors</fullName>
        <actions>
            <name>LEAD has been converted to ACTIVE</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Vendor__c.Vendor_Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <description>Alert VM when a LEAD has been converted to ACTIVE</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VM - Status Lead to Active</fullName>
        <actions>
            <name>Status Lead to Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Save date that the vendor was converted from Lead to Active [Lock the date]</description>
        <formula>AND( 
ISPICKVAL(PRIORVALUE(Vendor_Status__c), &quot;Lead&quot;), 
ISPICKVAL(Vendor_Status__c, &quot;Active&quot;) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
