<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case closed alert</fullName>
        <ccEmails>portal@globallanguages.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <template>GLS_Templates/Case_Closed_Template</template>
    </alerts>
    <alerts>
        <fullName>Inform case owner and portal user alert</fullName>
        <ccEmails>portal@globallanguages.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <template>GLS_Templates/Case_Created_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Accept Should Contact Be Updated</fullName>
        <field>Should_Contact_Be_Updated__c</field>
        <literalValue>Accepted</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close Update Profile Case</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close Update Profile Case</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reject Should Contact Be Updated</fullName>
        <field>Should_Contact_Be_Updated__c</field>
        <literalValue>Rejected</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Case Created Alert</fullName>
        <actions>
            <name>Inform case owner and portal user alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>Community Portal Update</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Marked - Closed</fullName>
        <actions>
            <name>Case closed alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>Community Portal Update</value>
        </criteriaItems>
        <description>when the case is closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck Initiate Approval Process during case close</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
