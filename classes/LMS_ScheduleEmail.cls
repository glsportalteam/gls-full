global class LMS_ScheduleEmail implements Schedulable {
  
    global void execute(SchedulableContext sc) {

        Date todaysDate = Date.today();
       String query = 'select Id, Name, Status__c,Training__r.Training_Name__c,Remainder_Sent_Date__c,Due_Date__c,Training__r.End_Date__c,Training__r.Training_Methodologies__c,Training__r.Start_Date__c,Training__r.Trainer_Name__c,Training__r.Name,Training__r.Reminder_Email_Days__c,User_Name__c,User_Name__r.name from User_Training__c where Training__r.Active__c = true AND (Remainder_Sent_Date__c = :todaysDate OR Due_Date__c = :todaysDate)';
        system.debug('---------query-----'+query);
        LMS_SceduleEmailBatchClass scheduleEmailInstance = new LMS_SceduleEmailBatchClass(query);
        Id batchprocessid = Database.executeBatch(scheduleEmailInstance,200);
    
    
  
           }
}