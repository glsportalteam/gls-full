/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSLPW_ProtocolControllerExt Class
* Function: This class is used as a controller extension for the GLSLPW_ProtocolDetailPage
*/

public class GLSLPW_ProtocolControllerExt {

    //Variable declaration for the class
    public Protocol__c protocolObj                              {get; set;}
    public Id paId                                              {get; set;}
    public List<Purchase_Agreement__c> lstPurchaseAgreement     {get; set;}
    public List<Protocol_Country__c> lstApprovedCountries       {get; set;}
    public List<Estimate_Line_Item__c> lstEstimateLineItem      {get; set;}
    public Id protocolId; 
    public Boolean enableDelete									{get; set;}
    public Boolean isRendered                                 	{get; set;}
    //Constructor
    public GLSLPW_ProtocolControllerExt(ApexPages.StandardController stdcon){
       
        //Initialization
        protocolObj = new Protocol__c();
        lstPurchaseAgreement = new List<Purchase_Agreement__c>();
        lstApprovedCountries = new List<Protocol_Country__c>();
        lstEstimateLineItem = new List<Estimate_Line_Item__c>();
        paId = null;                               
        
        //Get IDs from URL
        protocolId = ApexPages.currentPage().getParameters().get('id');
            if(protocolId != null){
            protocolObj = new Protocol__c();
            protocolObj = getProtocolRecord(protocolId );
            lstPurchaseAgreement = getPurchaseAgreement(protocolId);
            lstApprovedCountries = getApprovedCountries(protocolId);
            lstEstimateLineItem = getEstimateLineItemHistory(protocolId); 
            isRendered = true;
         }
     	 if (Schema.sObjectType.Purchase_Agreement__c.isDeletable()){
      	 	enableDelete = true;
      	 }
      	 else{
      	 	enableDelete = false;
      	 }
    }
    
    /* 
    @Description: To get the complete record of Protocol
    @Params: Protocol Id
    @Return Type: Protocol__c
    */
    private Protocol__c getProtocolRecord(Id protocolId){
        protocolObj =  [SELECT Protocol_Name__c,Status__c,Program_Name__r.Program_Name__c,Approved_Amount__c,Program_Name__r.Account__r.name,
                        Program_Name__r.Contact__r.name,PA_Amount__c,Program_Name__r.Sponsor_Image__c,Invoiced_Amount__c
                        FROM Protocol__c
                        WHERE Id =: protocolId];
        return protocolObj;
    }
    
   /* 
    @Description: To get the record of Purchase Agreement
    @Params: Protocol Id
    @Return Type:List<Purchase_Agreement__c> 
    */
    private List<Purchase_Agreement__c> getPurchaseAgreement(Id protocolId){
        lstPurchaseAgreement = [SELECT Name,Issue_Date__c,Expiration_Date__c,Status__c,Total_Amount__c,Invoiced_Amount__c,Remaining_Balance__c
                                FROM Purchase_Agreement__c
                                WHERE Protocol_Id__c =: protocolId
                                ORDER BY CreatedDate DESC ];
        return lstPurchaseAgreement;
    }
    
   /* 
    @Description: To get the complete record of Approved Countries
    @Params: Protocol Id
    @Return Type:List<Protocol_Country__c> 
    */
    private List<Protocol_Country__c> getApprovedCountries(Id protocolId){
        lstApprovedCountries = [SELECT Country_Id__r.name,Number_of_Sites__c
                                FROM Protocol_Country__c
                                WHERE Protocol_Id__c =: protocolId
                                ORDER BY CreatedDate DESC ];
        return lstApprovedCountries;
    }
    
     /* 
    @Description: To get the complete record of Estimate Line Item
    @Params: Protocol Id
    @Return Type:List<Estimate_Line_Item__c> 
    */
    private List<Estimate_Line_Item__c> getEstimateLineItemHistory(Id protocolId){
        lstEstimateLineItem = [ SELECT name,Estimate_Id__r.LP_Estimate_ID__c,Amount__c , Estimate_Id__r.Status__c
                                FROM Estimate_Line_Item__c
                                WHERE Protocol_Id__c =: protocolId
                                ORDER BY CreatedDate DESC ];
        return lstEstimateLineItem;
    }
    
    /* 
    @Description: Deletes the Purchase Agreement Record.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference deleteRecord(){
      List<Purchase_Agreement__c> pa = new List<Purchase_Agreement__c>();
        if(paId!=null){
            pa = [SELECT Id 
                  FROM Purchase_Agreement__c
                  WHERE Id =: paId LIMIT 1];
        }
        
        if(pa.size()>0){
            try{
             delete pa[0];
            }catch(DMLException de){
                    system.debug(GLSConfig.DMLExceptionOccured + de);
            }catch(Exception e){
                    system.debug(GLSConfig.ExceptionOccured + e);
            }
        }
      
       lstPurchaseAgreement = getPurchaseAgreement(protocolId);
       protocolObj = getProtocolRecord(protocolId ); 
       return null; 

   }
   
    /* 
    @Description: Method to edit the Purchase Agreement record.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference customEdit(){
        isRendered = false;
        return null;
    }
     public PageReference customCancel(){
        PageReference pref = null;
        isRendered = true;
        return pref;
    }
    
    public PageReference customSave(){
    	 PageReference pRef = null;
    	 isRendered = false;
    	 try{
         update protocolObj;
                if(protocolObj.Id != null){
                    pRef = new PageReference('/apex/GLSLPW_ProtocolDetailPage?id='+protocolObj.Id);
                    pRef.setRedirect(true);
                }
    	 }catch(DMLException de){
            system.debug(GLSConfig.DMLExceptionOccured + de);
        }
        catch(Exception e){
            system.debug(GLSConfig.ExceptionOccured + e);
        }
        return pRef;
    }
}