/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSLPW_RPT_QLIUpdateExtTest {

    static testMethod void myUnitTest() {
    	Account accObj 					= GLSLPW_SetUpData.createAccount('Amgen');
        Contact conObj 					= GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
   	    Quote__c quoteObj 			 	= GLSLPW_SetUpData.createQuote(accObj.Id, conObj.Id, 'Billed');
   	    Country__c countryObj 			= GLSLPW_SetUpData.createCountry('Test Country');
   	    Language_List_Item__c lliObj1	= GLSLPW_SetUpData.createLanguageListItem('Test Language 1');
        Language_List_Item__c lliObj2	= GLSLPW_SetUpData.createLanguageListItem('Test Language 2');
   	    Quote_Line_Item__c quoteLI1 	= GLSLPW_SetUpData.createQuoteLineItem(quoteObj.Id, countryObj.Id, lliObj1.Id, lliObj2.Id);
       Test.startTest();
			GLSLPW_RPT_QLIUpdateExt obj = new GLSLPW_RPT_QLIUpdateExt(new ApexPages.StandardController(quoteLI1));
			obj.changeModeE();
			obj.changeModeS();
			obj.saveQLIInfo();
			obj.cancelChanges();
			ApexPages.currentPage().getParameters().put('id', quoteLI1.id);
			obj.backToPrvious();
			Quote_Line_Item__c quoteLI2 = new Quote_Line_Item__c();
			GLSLPW_RPT_QLIUpdateExt obj1 = new GLSLPW_RPT_QLIUpdateExt(new ApexPages.StandardController(quoteLI2));
			obj1.saveQLIInfo();
	    Test.stopTest();
    }
}