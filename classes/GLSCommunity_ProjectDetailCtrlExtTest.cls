/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSCommunity_ProjectDetailCtrlExtTest {
	
	static User u;
    static testMethod void myUnitTest() {
        Account accObj = GLSLPW_SetUpData.createAccount('Amgen');
		Contact conObj = GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
		Quote__c quoteObj = GLSLPW_SetUpData.createQuote(accObj.Id,conObj.Id,'');
		Quote_Assignment__c qaObj = GLSLPW_SetUpData.createQuoteAssignment(quoteObj.Id);
		
		Test.startTest();
              Profile p = [select id from profile where name=: GLSConfig.AccountManager];
              u = new User(LastName = 'test user 2', 
                             Username = 'test.user.2@example.com', 
                             Email = 'test.2@example.com', 
                             Alias = 'testu2',
                             contactId = conObj.Id, 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = p.Id, 
                             LanguageLocaleKey = 'en_US'
                             );
             System.runAs (u){
                ApexPages.currentPage().getParameters().put('qId', quoteObj.Id);
                ApexPages.currentPage().getParameters().put('qaId', qaObj.Id);
                ApexPages.currentPage().getParameters().put('conId', conObj.Id);
                GLSCommunity_ProjectDetailControllerExt obj = new GLSCommunity_ProjectDetailControllerExt();
                obj.certificationType = 'Test Certification';
                obj.quoteStatus = 'requestQuote';
                obj.sendEmailOnPlacingOrder();
                obj.redirectToQuoteOrderPage();
                obj.quoteStatus = 'placeOrder';
                obj.sendEmailOnPlacingOrder();
                obj.redirectToQuoteOrderPage();
             }
     	Test.stopTest();   
    }
}