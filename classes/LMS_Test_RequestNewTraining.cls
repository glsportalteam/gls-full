@isTest
public class LMS_Test_RequestNewTraining{
    public static Task task;
    public static List<String> lstEmailId;
  //  public list<user> usrlst;
  @isTest
    public static void testRequestNewController(){
      list<user>  usrlst = new list<user>();
        lstEmailId = new List<String>();
        lstEmailId.add('abc@bbc.com');
        LMS_Test_Util util = new LMS_Test_Util();
        task = new Task();
        task.training_name__c = 'New Training';
        task.Description__c = 'description';
        task.skills__c = 'java';       
        task.subject = 'Require New Training';
        task.status = 'Not Started';
        task.ownerId = LMS_Test_Util.projectManager.id;
        task.Priority = 'Normal';
          usrlst.add(LMS_Test_Util.projectManager);     
        // Database.insert(task);
            Group g1 = new Group(Name='LMS Training Admin', type='Queue');
            Database.insert(g1);
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Training__c');
           Database.insert(q1);
           
            test.StartTest();
            
            LMS_RequestNewTrainingController LMS_RequestNewTrainingController1 = new LMS_RequestNewTrainingController();
            LMS_RequestNewTrainingController1.skills = 'java';
            LMS_RequestNewTrainingController1.trainingName= 'JAva';
            LMS_RequestNewTrainingController1.description = 'abs';
            LMS_RequestNewTrainingController1.send();
            LMS_RequestNewTrainingController1.cancel();
            LMS_RequestNewTrainingController1.createTask(task.training_name__c,task.skills__c,task.Description__c,usrlst);
            LMS_RequestNewTrainingController1.sentEmail(task.training_name__c,task.skills__c,task.Description__c,lstEmailId);
            test.stopTest();
    }
}