public with sharing class LMS_RequestNewTrainingController {
    public String trainingName      {get;set;}
    public String Skills            {get;set;}
    public String description       {get;set;}   

    public PageReference send() {
        List<String> lstUserId = new List<String> ();
        List<String> lstEmailId = new List<String> ();
        List<User> lstUser = new List<User>();
        List<User> user = new List<User>(); 
        String queueName='LMS Training Admin';    
        List<GroupMember> lstQueueMember = new List<GroupMember>();
        String emailAddress = '';
        boolean isEmailsent = false;
        boolean isTaskCreate = false;
        PageReference home= null;
        String QueueId = '';
        
            if(trainingName != null && trainingName != '' && Skills!= null && Skills != ''){
        
                QueueId = [ Select Id, Type, Name From Group  where type ='Queue' and Name ='LMS Training Admin' limit 1 ].Id;
                system.debug('---QueueId---: '+QueueId);
        
                if(QueueId != ''){
                    lstQueueMember = [ Select UserOrGroupId, GroupId From GroupMember where groupId =: QueueId] ;
                }
                system.debug('---lstQueueMember---: '+lstQueueMember);
        
                if(lstQueueMember != null && lstQueueMember.size()>0){
                    for(GroupMember grpM : lstQueueMember){
                        lstUserId.add(grpM.UserOrGroupId);
                    }
                }
                system.debug('---lstUserId---: '+lstUserId);
        
                if(lstUserId != null && lstUserId.size()>0){
                    lstUser = [Select Id, Email From User Where Id in: lstUserId];
                }
                 system.debug('---lstUser---: '+lstUser);
                if(lstUser != null && lstUser.size()>0){
                     for(User userEmail: lstUser){
                        lstEmailId.add(userEmail.email);
                     }
                }
                
                if(lstEmailId != null && lstEmailId.size()>0){
                    isEmailsent = sentEmail(trainingName, Skills, description, lstEmailId);
                    if (isEmailsent){
                        isTaskCreate = createTask(trainingName, Skills, description, lstUser );
                        if(isTaskCreate == true){
                            home= Page.glsLmsHome; // use the name of the second VF page        
                            home.setRedirect(true);
                        }
                    }
                }else{
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Your Request is not submitted');
                    ApexPages.addMessage(myMsg);
                    home= null;
                }
            }else{
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Please Enter the value for all fields.');
                ApexPages.addMessage(myMsg);
                home= null;
            }
        return home;
    }
    
    public PageReference cancel() {
        system.debug('-----cancle-----');
        PageReference cancle= null;
        cancle= Page.glsLmsHome; // use the name of the second VF page
        cancle.setRedirect(true);
        return cancle;
    }
    
    public boolean sentEmail(String trainingName, String Skills, String description, List<String> lstEmailId){
         List<GroupMember> lstQueueMember = new List<GroupMember>();
         boolean flag = false;
         String userName = '';
         userName = [select name From User where id=:UserInfo.getUserId()  limit 1].Name;
         system.debug('---lstEmailId---: '+lstEmailId);
         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
         mail.setToAddresses(lstEmailId);
        // mail.setSenderDisplayName(user[0].name);
         mail.setSubject('New Training Request  for : ' +trainingName);
         mail.setBccSender(false);
         mail.setUseSignature(false);
        // mail.setPlainTextBody('User has requested the new training Name:' +trainingName+ ' Skills:' +Skills + 'Description' +description);
         mail.setHtmlBody('<p> <img src="https://c.ap1.content.force.com/servlet/servlet.ImageServer?id=01590000003uX86&oid=00D90000000uV70&lastMod=1402396559000" width="120" height="50">'  +'</br></br>Dear Director of Training, </br></br>'+userName+' has requested the following new training:  </br> </br>' +  +
                         'Training Name: '+trainingName+'</br>' +
                         'Skills: '+Skills +
                         '</br>Description: '+description +'</p>' +'</br>Thank you!,</br>'+'The LMS Team</br>');
        // Send the email you have created.
         try{
             Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
             flag= true;
         }catch(Exception e){
            system.debug(e);
            flag = false;
        }
        return flag;
    }
    
    public boolean createTask(String trainingName, String Skills, String description, List<User> lstUser ){
        boolean flag = false;
        String UserName = UserInfo.getFirstName()+' '+UserInfo.getLastName();
        //List<User> lstadminUserId = new List<User>() ;
        List<Task> lstTask = new List<Task>();
        //lstadminUserId =  [select Id From User where Profile.Name=:LMS_Literals.AdminProfileName]; 
        system.debug('--- inside Task creater--' +lstUser);
        
        if(lstUser != null && lstUser.size()>0){
            for(User userId :  lstUser){
                Task task = new Task();
                task.training_name__c = trainingName;
                task.Description__c = description;
                task.skills__c = Skills;
                task.ActivityDate = Date.today();
  
               // task.whoId = user[0].Id;
                task.subject = UserName + ': requested  new '+ trainingName +' Training';
                task.status = 'Not Started';
                task.ownerId = userId.Id;
                task.Priority = 'Normal';
                lstTask.add(task);
            }
            try{
                if(lstTask != null && lstTask.size()>0){
                    insert lstTask;
                    flag = true;
                }
            }catch(Exception e){
                system.debug('---in Task Creation Exception--: '+e);
                flag = false;
            }
        }else flag = false;
        
      return flag;
    }
}