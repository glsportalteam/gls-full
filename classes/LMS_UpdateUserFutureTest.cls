@isTest
private class LMS_UpdateUserFutureTest {
	
    
    static testMethod void myUnitTest() {
    	Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'lmsuser1', email='lmsuser1@testGLS.com',emailencodingkey='UTF-8',
                      lastname='Testing', languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id,
                       timezonesidkey='America/Los_Angeles', username='lmsuser1@gls.com');
        insert u;	
   		List<String> userIds = new List<String>();
   		userIds.add(u.Id);
   		test.startTest();
   			LMS_UpdateUserFuture.updateUser(userIds);
   		test.stopTest();
    }
}