/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSAnalyzeFileAPICallTest {

/**
    * This Unit Test method to test AnalyzeFile API call Success
    */
    static testMethod void testAnalyzeFileAPIcallSuccess() {
        GLSSetupData.createQLIServices();
        Test.startTest();
        GLSSetupData.isReponseCorrect =false;
        GLSAPICaller.analyzeQuoteLineItems(GLSSetupData.lstQuote[0].id);
     //   System.assertEquals(GLSConfig.analyzeComplete,GLSAPICaller.returnMessage);
        Test.stopTest();
    }
    
    static testMethod void testAnalyzeFileAPIcallSuccess1() {
        GLSSetupData.createQLIServices();
        Test.startTest();
        GLSSetupData.isReponseCorrect =true;
        GLSAPICaller.analyzeQuoteLineItems(GLSSetupData.lstQuote[0].id);
     //   System.assertEquals(GLSConfig.analyzeComplete,GLSAPICaller.returnMessage);
        Test.stopTest();
    }
    
    /**
    * This Unit Test method to test AnalyzeFile API call with Empty TM File
    */
    static testMethod void testAnalyzeFileNoTMFileSelectedSuccessWithoutTM() {
        GLSSetupData.createQLIFiles();
        Test.startTest();
        Quote_Line_Item__c qliUpdate1 = new Quote_Line_Item__c(id= GLSSetupData.quoteLI1.Id);
        qliUpdate1.Client_TM__c = GLSSetupData.clientTM1.Id;
        update qliUpdate1;
        
        Quote_Line_Item__c qliUpdate2 = new Quote_Line_Item__c(id= GLSSetupData.quoteLI2.Id);
        qliUpdate2.Client_TM__c = GLSSetupData.clientTM1.Id;
        update qliUpdate2;
        
      
        GLSAPICaller.analyzeQuoteLineItems(GLSSetupData.lstQuote[0].id);
        System.assert(GLSAPICaller.returnMessage != null);
        Test.stopTest();
    }
    
    
    /**
    * This Unit Test method to test AnalyzeFile API call when No TM File Selected
    */
    static testMethod void testAnalyzeFileNoTMFileSelectedFailue() {
        GLSSetupData.createQLIFiles();
        Test.startTest();
        Quote_Line_Item__c qliUpdate1 = new Quote_Line_Item__c(id= GLSSetupData.quoteLI1.Id);
        qliUpdate1.isTMFound__c = true;
        qliUpdate1.Client_TM__c = null;
        qliUpdate1.isAnalyzed__c = false;
        qliUpdate1.isReadyForAnalyse__c = true;
        update qliUpdate1;
        
        Quote_Line_Item__c qliUpdate2 = new Quote_Line_Item__c(id= GLSSetupData.quoteLI2.Id);
        qliUpdate2.Client_TM__c = null;
        qliUpdate2.isAnalyzed__c = false;
        qliUpdate2.isReadyForAnalyse__c = true;
        update qliUpdate2;
        
        /*List<Quote_Line_Item__c> lstQLIs = [Select Id, Name,QuoteID__c,Trados_Language_Pair__c from Quote_Line_Item__c where QuoteID__c =: GLSSetupData.lstQuote[0].id];
        String QuoteName = '';
        for(Quote_Line_Item__c qli : lstQLIs){
            QuoteName += qli.Trados_Language_Pair__c+',';
        }
        Integer index = QuoteName.lastIndexOf(',');
        QuoteName = QuoteName.substring(0, index);*/
        
        GLSAPICaller.analyzeQuoteLineItems(GLSSetupData.lstQuote[0].id);
        Test.stopTest();
    }
    
    
      /**
    * This Unit Test method to test AnalyzeFile API call when SF Ids are empty
    */
    static testMethod void testAnalyzeFileEmptySFIds() {
        GLSSetupData.createQLIFiles();
        Quotes_File__c qFilesUpd1 = new Quotes_File__c(id= GLSSetupData.qFiles1.Id);
        qFilesUpd1.TradosAPIFileID__c = '123456';
        qFilesUpd1.isSPCharFile__c = true;
        update qFilesUpd1;
        
        Quotes_File__c qFilesUpd2 = new Quotes_File__c(id= GLSSetupData.qFiles2.Id);
        qFilesUpd2.TradosAPIFileID__c = '123457';
        qFilesUpd2.isSPCharFile__c = true;
        update qFilesUpd2;

        Test.startTest();
        GLSAPICaller.analyzeQuoteLineItems(GLSSetupData.lstQuote[0].id);
        Test.stopTest();
    }
               
    /**
    * This Unit Test method to test AnalyzeFile API call when No QLI created to analyze
    */
    static testMethod void testAnalyzeFileNoQLICreatedFailure() {
        GLSSetupData.setupData();
        Test.startTest();
        GLSAPICaller.analyzeQuoteLineItems(GLSSetupData.lstQuote[0].id);
        System.assertEquals(GLSConfig.NoQLICreatedforAnalyze,GLSAPICaller.returnMessage);
        Test.stopTest();
    }
    
    /**
    * This Unit Test method to test AnalyzeFile API call Success for individual QLI
    */
    static testMethod void testAnalyzeFileAPIcallSuccessIndiv() {
        GLSSetupData.createQLIServices();
        Test.startTest();
        GLSAPICaller.analyzeFile(GLSSetupData.quoteLI1.id);
     //   System.assertEquals(GLSConfig.analyzeComplete,GLSAPICaller.returnMessage);
        Test.stopTest();
    }
    
  /**
    * This Unit Test method to test AnalyzeFile API call Success for individual QLI without TM
    */
    static testMethod void testAnalyzeFileAPIcallSuccessWithoutTM() {
        GLSSetupData.createQLIServices();
       
        Test.startTest();
        Quote_Line_Item__c qli= new Quote_Line_Item__c (id=GLSSetupData.quoteLI1.id);
        qli.Client_TM__c = GLSSetupData.clientTM1.Id;
        update qli;
        GLSAPICaller.analyzeFile(qli.Id);
       // System.assertEquals(GLSConfig.analyzeComplete,GLSAPICaller.returnMessage);
        Test.stopTest();
    }
    
    /**
    * This Unit Test method to test AnalyzeFile API call when No QLI created to analyze
    */
    static testMethod void testAnalyzeFileNoQLICreatedFailureIndiv() {
        GLSSetupData.setupData();
        Test.startTest();
        GLSAPICaller.analyzeFile(GLSSetupData.quoteLI1.id);
        System.assertEquals(GLSConfig.NoQLICreatedforAnalyze,GLSAPICaller.returnMessage);
        Test.stopTest();
    }
    
  
      static testMethod void myUnitTest4() {
        GLSSetupData.createQLIFiles();
        test.startTest();
            GLSAPICaller obj = new GLSAPICaller();
            GLSAPICaller.analyzeFile(GLSSetupData.quoteLI2.id);
        test.stopTest();
    }
    
     static testMethod void myUnitTest6() {
        GLSSetupData.createQLIServices();
        String url = 'test';
        test.startTest();
            GLSAPICaller obj = new GLSAPICaller();
            GLSAPICaller.analyzeFile(GLSSetupData.quoteLI3.id);
            GLSAPICaller.getResponse(url);
        test.stopTest();
    }
    
    static testMethod void myUnitTest1() {
        GLSSetupData.createQLI();
        test.startTest();
            GLSAPICaller obj = new GLSAPICaller();
            GLSAPICaller.analyzeQuoteLineItems(GLSSetupData.quote.id);
        test.stopTest();
     }
  
  /*
    
     
     static testMethod void myUnitTest2() {
        GLSSetupData.createQLIFiles();
        test.startTest();
            GLSAPICaller obj = new GLSAPICaller();
            GLSAPICaller.analyzeQuoteLineItems(GLSSetupData.quote.id);
        test.stopTest();
    }
    
    static testMethod void myUnitTest3() {
        GLSSetupData.createQLIFiles();
        test.startTest();
            GLSAPICaller obj = new GLSAPICaller();
            GLSAPICaller.analyzeFile(GLSSetupData.quoteLI1.id);
        test.stopTest();
    }
    

    
    static testMethod void myUnitTest5() {
        GLSSetupData.createQLIServices();
        String url = 'test';
        test.startTest();
            GLSAPICaller obj = new GLSAPICaller();
            GLSAPICaller.analyzeQuoteLineItems(GLSSetupData.quote2.id);
            GLSAPICaller.getResponse(url);
        test.stopTest();
    }
    

    static testMethod void myUnitTest7() {
        GLSSetupData.createQLIFiles();
        test.startTest();
            GLSAPICaller obj = new GLSAPICaller();
            GLSAPICaller.analyzeQuoteLineItems(GLSSetupData.quote3.id);
        test.stopTest();
    }
    static testMethod void myUnitTest8() {
        GLSSetupData.setupData();
        test.startTest();
            GLSAPICaller obj = new GLSAPICaller();
            GLSAPICaller.analyzeQuoteLineItems(GLSSetupData.quote4.id);
        test.stopTest();
    }
    static testMethod void myUnitTest9() {
        GLSSetupData.setupData();
        test.startTest();
            GLSAPICaller obj = new GLSAPICaller();
            GLSAPICaller.analyzeFile(GLSSetupData.quoteLI5.id);
        test.stopTest();
    }*/
}