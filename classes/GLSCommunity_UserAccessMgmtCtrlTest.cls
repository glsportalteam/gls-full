/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSCommunity_UserAccessMgmtCtrlTest {

    static testMethod void myUnitTest() {
		Account accObj = GLSLPW_SetUpData.createAccount('Amgen');
		Contact conObj1 = GLSLPW_SetUpData.createContact(accObj.Id,'TestName1');
		Contact conObj2 = GLSLPW_SetUpData.createContact(accObj.Id,'TestName2');
		Contact conObj3 = GLSLPW_SetUpData.createContact(accObj.Id,'TestName3');
		Program__c prg = GLSLPW_SetUpData.createProgram(accObj.Id, conObj1.Id, 'TestName', 'Test Ref');
		Program__c prg2 = GLSLPW_SetUpData.createProgram(accObj.Id, conObj1.Id, 'TestName2', 'Test Ref2');
		Protocol__c pro = GLSLPW_SetUpData.createProtocol(prg.Id, 'Open');
		Protocol__c pro2 = GLSLPW_SetUpData.createProtocol(prg.Id, 'Open');
		Protocol__c pro3 = GLSLPW_SetUpData.createProtocol(prg2.Id, 'Open');
				
		Profile p1 = [select id from profile where name=: GLSConfig.AccountManager];
        User u1 = new User(LastName = 'test user 1', 
                             Username = 'test.user.1@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'testu1',
                             contactId = conObj1.Id, 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = p1.Id, 
                             LanguageLocaleKey = 'en_US'
                             );
        insert u1;
        
        Profile p2 = [select id from profile where name=: GLSConfig.ProtocolManager];
        User u2 = new User(LastName = 'test user 2', 
                             Username = 'test.user.2@example.com', 
                             Email = 'test.2@example.com', 
                             Alias = 'testu2',
                             contactId = conObj2.Id, 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = p2.Id, 
                             LanguageLocaleKey = 'en_US'
                             );
        insert u2;
        
        Profile p3 = [select id from profile where name=: GLSConfig.ProgramManager];
        User u3 = new User(LastName = 'test user 3', 
                             Username = 'test.user.3@example.com', 
                             Email = 'test.3@example.com', 
                             Alias = 'testu3',
                             contactId = conObj3.Id, 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = p3.Id, 
                             LanguageLocaleKey = 'en_US'
                             );
        insert u3;
        
        
        
		Test.startTest();
              
             System.runAs (u1){
             	User_Access_Management__c uam = GLSLPW_SetUpData.createUAMRecord(u1.Id, prg.Id, null, GLSConfig.ProgramManager);
                ApexPages.currentPage().getParameters().put('conId', conObj1.Id);
                GLSCommunity_UserAccessManagementCtrl obj = new GLSCommunity_UserAccessManagementCtrl();
                obj.selectedProfile = GLSConfig.ProgramManager;
                obj.getUsersForProfile();
                obj.selectedUser = u3.Name;
                obj.getProgramsProtocols();
                obj.lstProgramWrapper.add(new GLSCommunity_UserAccessManagementCtrl.ProgramWrapper(prg2, true));
                obj.addUAMRecords();
                obj.selectedProfile = GLSConfig.ProtocolManager;
                obj.getUsersForProfile();
                obj.selectedUser = u2.Name;
                obj.getProgramsProtocols();
                obj.addUAMRecords();
                obj.deleteUAMRecordId = uam.Id;
                obj.deleteUAMRecord();
             }
     	Test.stopTest();   
    }
    
    static testMethod void myUnitTest2() {
		Account accObj = GLSLPW_SetUpData.createAccount('Amgen');
		Contact conObj1 = GLSLPW_SetUpData.createContact(accObj.Id,'TestName1');
		Contact conObj2 = GLSLPW_SetUpData.createContact(accObj.Id,'TestName2');
		Contact conObj3 = GLSLPW_SetUpData.createContact(accObj.Id,'TestName3');
		Program__c prg = GLSLPW_SetUpData.createProgram(accObj.Id, conObj1.Id, 'TestName', 'Test Ref');
		Program__c prg2 = GLSLPW_SetUpData.createProgram(accObj.Id, conObj1.Id, 'TestName2', 'Test Ref2');
		Protocol__c pro = GLSLPW_SetUpData.createProtocol(prg.Id, 'Open');
		Protocol__c pro2 = GLSLPW_SetUpData.createProtocol(prg.Id, 'Open');
		Protocol__c pro3 = GLSLPW_SetUpData.createProtocol(prg2.Id, 'Open');

        
        Profile p2 = [select id from profile where name=: GLSConfig.ProtocolManager];
        User u2 = new User(LastName = 'test user 2', 
                             Username = 'test.user.2@example.com', 
                             Email = 'test.2@example.com', 
                             Alias = 'testu2',
                             contactId = conObj2.Id, 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = p2.Id, 
                             LanguageLocaleKey = 'en_US'
                             );
        insert u2;
        
        Profile p3 = [select id from profile where name=: GLSConfig.ProgramManager];
        User u3 = new User(LastName = 'test user 3', 
                             Username = 'test.user.3@example.com', 
                             Email = 'test.3@example.com', 
                             Alias = 'testu3',
                             contactId = conObj3.Id, 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = p3.Id, 
                             LanguageLocaleKey = 'en_US'
                             );
        insert u3;

		Test.startTest();
              
             System.runAs (u3){
             	User_Access_Management__c uam = GLSLPW_SetUpData.createUAMRecord(u3.Id, null, pro2.Id, GLSConfig.ProtocolManager);
                ApexPages.currentPage().getParameters().put('conId', conObj3.Id);
                GLSCommunity_UserAccessManagementCtrl obj = new GLSCommunity_UserAccessManagementCtrl();
                obj.selectedProfile = GLSConfig.ProtocolManager;
                obj.getUsersForProfile();
                obj.selectedUser = u2.Name;
                obj.getProgramsProtocols();
                obj.addUAMRecords();
                obj.deleteUAMRecord();
             }
     	Test.stopTest();   
    }
}