@isTest(SeeAllData=true)
public class RHX_TEST_Purchase_Agreement {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM Purchase_Agreement__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new Purchase_Agreement__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}