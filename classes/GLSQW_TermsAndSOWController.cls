public with sharing class GLSQW_TermsAndSOWController {
    public list<WrapperSow> lstWrappersow{get;set;}
    public Quote__c quote{get;set;}
    //public list<WrapperTerms> lstWrapperTerms{get;set;}
    public list<Standard_SOW_Items_Table__c> lstStandardSOWItems{get;set;}
    public RFQ_Terms_Table__c RFQTermsTableObj {get;set;}
    //public string mandatory {get;set;}
    //public string termsValue{get;set;}
    public Integer rfqRecordCount;
    public Boolean renderAddItemSection{get;set;}
    public string RFQTermsName{get;set;}
    public List<SelectOption> lstOption{get;set;}
    public string rfqTermOrder{get;set;}
    public Boolean rfqTermModified{get;set;}
    public string rfqTermDesc{get;set;}
    public string statusMessage {get;set;}
    public string statusMessage1 {get;set;}
    public string statusMessage2 {get;set;}
    public string quoteId;
    public string clientId;
    public String pages{get;set;}
    public String currTemId {get;set;}
    public Boolean updateTermsSave {get;set;}
    public Boolean updateSOWSave {get;set;}
    Transient Schema.DescribeSObjectResult perfixRfqTerms;
    public Boolean disableBtn {get; set;}
    
    public GLSQW_TermsAndSOWController(){
        statusMessage = '';
        statusMessage1 = '';
        statusMessage2 = '';
        disableBtn = true;
        if(!test.isRunningTest()){
            String url = ApexPages.currentPage().getUrl();
            url = url.split('\\?')[0];
            pages = url.substring(url.lastIndexOf('/')+1);
            if(pages != null) pages = pages.toLowerCase();
        }
        lstStandardSOWItems=new list<Standard_SOW_Items_Table__c>();
        quoteId = ApexPages.currentPage().getParameters().get('id');
        if(quoteId != ''){
            quote=[select id,Client__c,Terms__c,Client__r.id,Name, Status__c from Quote__c where id=: quoteId limit 1];
            clientId=quote.Client__r.id;
            getStandardSOWItems(clientId);
            if(GLSConfig.btnEnableStatus.contains(quote.Status__c)){
	        	disableBtn = false;
        	}
        }
    }
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('gls_qwquotelistview','Quote List View'));
        options.add(new SelectOption('glsqw_newquotepage','Quote Edit'));
        options.add(new SelectOption('glsqw_scopeofservicespage','Scope of Services'));
        options.add(new SelectOption('glsqw_quoteoverview','Quote Overview'));
        options.add(new SelectOption('glsqw_ratesummarypage','Rate Summary'));
        options.add(new SelectOption('glsqw_termsandsowpage','Terms And SOW'));
        options.add(new SelectOption('glsqw_approvalprocesspage','Review and Submit'));
        
        return options;
    }
    public void getStandardSOWItems(string rfqClientId){
        map<String,RFQ_SOW_Items_Table__c> mapRFQSOWItem=new map<String,RFQ_SOW_Items_Table__c>();
        map<String,Boolean> mapRFQSOWItemChecked=new map<String,Boolean>();
        lstWrappersow= new list<WrapperSow>();
        if(rfqClientId != ''){
            
            lstStandardSOWItems=[Select id,Standard_SOW_Item_Text__c,Standard_SOWItemText__c,Standard_SOW_Item_Order__c,Standard_SOW_Item_Name__c,Standard_SOW_Item_Display_Name__c,OwnerId,Name From Standard_SOW_Items_Table__c order by Standard_SOW_Item_Order__c];
            list<RFQ_SOW_Items_Table__c> lstRFQSOWItemsTable=[select id,RFQ_Id__c,RFQ_SOW_Item_Description__c,RFQ_SOWItemDescription__c,RFQ_SOW_Item_Modified__c,RFQ_SOW_Item_Name__c,RFQ_SOW_Item_Order__c from RFQ_SOW_Items_Table__c where RFQ_Id__c=:quoteId];
            rfqRecordCount=lstRFQSOWItemsTable.size();
            for(RFQ_SOW_Items_Table__c rfq: lstRFQSOWItemsTable){
                mapRFQSOWItemChecked.put(rfq.RFQ_SOW_Item_Name__c,true);
                mapRFQSOWItem.put(rfq.RFQ_SOW_Item_Name__c,rfq);
            }
            for(Standard_SOW_Items_Table__c std:lstStandardSOWItems){
                if(mapRFQSOWItem.get(std.Standard_SOW_Item_Display_Name__c) != null){
                    lstWrappersow.add(new WrapperSow(null,mapRFQSOWItem.get(std.Standard_SOW_Item_Display_Name__c),mapRFQSOWItemChecked.get(std.Standard_SOW_Item_Display_Name__c)));
                }else{
                        lstWrappersow.add(new WrapperSow(std,null,false));
                }
            }
        }
        
    }
    public pageReference updateSOW(){
        map<string,RFQ_SOW_Items_Table__c> mapTemp= new map<string,RFQ_SOW_Items_Table__c>();
        map<string,Standard_SOW_Items_Table__c> mapStandardSOW= new map<string,Standard_SOW_Items_Table__c>();
        set<RFQ_SOW_Items_Table__c> setInsertRFQSOW= new set<RFQ_SOW_Items_Table__c>();
        list<RFQ_SOW_Items_Table__c> lstInsertRFQSOW= new list<RFQ_SOW_Items_Table__c>();
        map<Id,RFQ_SOW_Items_Table__c> mapDeleteRFQSOW= new map<Id,RFQ_SOW_Items_Table__c>();
        list<RFQ_SOW_Items_Table__c> lstRFQSOWItemsTable=[select id,RFQ_Id__c,RFQ_SOW_Item_Description__c,RFQ_SOWItemDescription__c,RFQ_SOW_Item_Modified__c,RFQ_SOW_Item_Name__c,RFQ_SOW_Item_Order__c from RFQ_SOW_Items_Table__c where RFQ_Id__c=:quoteId];
        list<Standard_SOW_Items_Table__c> lstInsertStandardSOW=[Select id,Standard_SOW_Item_Text__c,Standard_SOWItemText__c,Standard_SOW_Item_Order__c,Standard_SOW_Item_Name__c,Standard_SOW_Item_Display_Name__c,OwnerId,Name From Standard_SOW_Items_Table__c];
        Integer recordOrder=0;
        recordOrder=lstRFQSOWItemsTable.size();
        currTemId = '';
        updateTermsSave = false;
        updateSOWSave = true;
        try{    
        	for(RFQ_SOW_Items_Table__c rfq:lstRFQSOWItemsTable){
            mapTemp.put(rfq.RFQ_SOW_Item_Name__c,rfq);
        }
        for(Standard_SOW_Items_Table__c std:lstInsertStandardSOW){
            mapStandardSOW.put(std.Standard_SOW_Item_Name__c,std);
        }
        RFQ_SOW_Items_Table__c rfqSOW;
        for(WrapperSow wrap:lstWrappersow){
            if(wrap.isChecked && wrap.standardSOWItemsObj != null){
                if(mapTemp.get(wrap.standardSOWItemsObj.Standard_SOW_Item_Name__c) == null){
                    recordOrder++;
                    rfqSOW= new RFQ_SOW_Items_Table__c();
                    rfqSOW.RFQ_Id__c=quoteId;
                    rfqSOW.RFQ_SOW_Item_Name__c=mapStandardSOW.get(wrap.standardSOWItemsObj.Standard_SOW_Item_Name__c).Standard_SOW_Item_Display_Name__c;
                    rfqSOW.RFQ_SOW_Item_Description__c=mapStandardSOW.get(wrap.standardSOWItemsObj.Standard_SOW_Item_Name__c).Standard_SOW_Item_Text__c;
                    rfqSOW.RFQ_SOWItemDescription__c=mapStandardSOW.get(wrap.standardSOWItemsObj.Standard_SOW_Item_Name__c).Standard_SOWItemText__c;
                    rfqSOW.RFQ_SOW_Item_Order__c=mapStandardSOW.get(wrap.standardSOWItemsObj.Standard_SOW_Item_Name__c).Standard_SOW_Item_Order__c;
                    setInsertRFQSOW.add(rfqSOW);
                }
            }else if(!wrap.isChecked && wrap.standardSOWItemsObj == null){
                if(mapTemp.get(wrap.rfqSOWItemsObj.RFQ_SOW_Item_Name__c) != null){
                    mapDeleteRFQSOW.put(mapTemp.get(wrap.rfqSOWItemsObj.RFQ_SOW_Item_Name__c).id,mapTemp.get(wrap.rfqSOWItemsObj.RFQ_SOW_Item_Name__c));
                }
            }
        }
        lstInsertRFQSOW.addAll(setInsertRFQSOW);
        if(lstInsertRFQSOW.size()>0)
        	insert lstInsertRFQSOW;
        statusMessage = 'Changes Saved';
        if(mapDeleteRFQSOW.size()>0)
        	delete mapDeleteRFQSOW.values();
	    }catch(exception ex){
	        statusMessage = '';
	    }
	
	    ApexPages.Message statusMsg = new ApexPages.Message(ApexPages.Severity.INFO, statusMessage);
	    ApexPages.addMessage(statusMsg);
	    return null;
	}

	public pageReference updateSOWTerms(){
	    try{
	        statusMessage = '';
	        String termsId= ApexPages.currentPage().getParameters().get('standardTermsId');
	        currTemId = termsId;
	        updateTermsSave = false;
	        updateSOWSave = false;
	        perfixRfqTerms = RFQ_SOW_Items_Table__c.SObjectType.getDescribe();
	        if(termsId.startsWith(perfixRfqTerms.getKeyPrefix())){
	            RFQ_SOW_Items_Table__c rfq= new RFQ_SOW_Items_Table__c();
	            for(WrapperSow wrap:lstWrappersow){
	                if(wrap.rfqSOWItemsObj != null && wrap.rfqSOWItemsObj.id == termsId){
	                    rfq= new RFQ_SOW_Items_Table__c(id=termsId);
	                    rfq.RFQ_SOW_Item_Description__c=wrap.rfqSOWItemsObj.RFQ_SOW_Item_Description__c;
	                    rfq.RFQ_SOWItemDescription__c=wrap.rfqSOWItemsObj.RFQ_SOWItemDescription__c;
	                }
	            }
	            update rfq;
	        }else{
	            RFQ_SOW_Items_Table__c rfq= new RFQ_SOW_Items_Table__c();
	            for(WrapperSow wrap:lstWrappersow){
	                if(wrap.rfqSOWItemsObj == null && wrap.standardSOWItemsObj.id == termsId){
	                    rfq= new RFQ_SOW_Items_Table__c();
	                    rfq.RFQ_Id__c=quoteId;
	                    rfq.RFQ_SOW_Item_Name__c=wrap.standardSOWItemsObj.Standard_SOW_Item_Display_Name__c;
	                    rfq.RFQ_SOW_Item_Order__c=rfqRecordCount+1;
	                    rfq.RFQ_SOW_Item_Description__c=wrap.standardSOWItemsObj.Standard_SOW_Item_Text__c;
	                    rfq.RFQ_SOWItemDescription__c=wrap.standardSOWItemsObj.Standard_SOWItemText__c;
	                }
	            }
	            insert rfq;
	        }
	        statusMessage2 = 'Changes Updated';
	        getStandardSOWItems(clientId);
		    }catch(Exception ex){
		        statusMessage2 = '';
		        system.debug(ex);
		    }
	    ApexPages.Message statusMsg2 = new ApexPages.Message(ApexPages.Severity.INFO, statusMessage2);
	    ApexPages.addMessage(statusMsg2);
	    return null;
	}
	public pageReference updateTerms(){
	    currTemId = '';
	    updateTermsSave = true;
	    updateSOWSave = false;
	    if(quoteId != null){
	        Quote__c q= new Quote__c(id= quoteId);
	        q.Terms__c=quote.Terms__c;
	        try{
	            update q;
	            statusMessage1 = 'Changes Updated';
	        }catch(Exception e){
	            statusMessage1 = '';
	        }
	    }else statusMessage1 = '';
	    ApexPages.Message statusMsg1 = new ApexPages.Message(ApexPages.Severity.INFO, statusMessage1);
	    ApexPages.addMessage(statusMsg1);
	    return null;
	}

	public class WrapperSow{
	    public Standard_SOW_Items_Table__c standardSOWItemsObj{get;set;}
	    public RFQ_SOW_Items_Table__c rfqSOWItemsObj{get;set;}
	    public Boolean isChecked{get;set;}
	    public WrapperSow(Standard_SOW_Items_Table__c sOWItemsObj,RFQ_SOW_Items_Table__c rfqItemsObj,Boolean check){
	        this.standardSOWItemsObj=sOWItemsObj;
	        this.rfqSOWItemsObj=rfqItemsObj;
	        this.isChecked=check;
	    }
	}

}