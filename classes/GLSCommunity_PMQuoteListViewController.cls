/**
  @Author       : Global Language solution Systems Ltd.
  @Date         : 01/01/2016
  @Description  : This Class is for generating Project Manager Quote List View
*/


public class GLSCommunity_PMQuoteListViewController {
    public List<Quote__c> lstQuote {get; set;}
    public Quote__c quoteObj{get;set;}
    public boolean renderQuoteListView {get;set;}
    public transient List<ToDisplay> toDisplayList{get;set;} 
    public integer totalRecs {get;set;}
    public integer OffsetSize {get;set;}
    public integer LimitSize {get;set;}
    public integer totalPages {get;set;}
    public Integer selectedPage {get;set;}
    public List<SelectOption> lstRecordsPerPageOptions {get;set;}
    public string searchStr   {get;set;}
    public String sortField {get;set;}
    public String sortOrder {get;set;}
    public list<selectOption> lstprojectDUeDateFilter  {get;set;}
    public string filterDateRange          {get;set;}
    public string createdDateRangeFilter   {get;set;}
    public String selectedUserId           {get;set;}
    public String startDate                {get;set;}
    public String endDate                  {get;set;}     
    public String isCustomSearch           {get;set;}
    public string pref {get; set;}
    public ApexPages.StandardSetController setCtrlInst  {get;set;}
    
    public GLSCommunity_PMQuoteListViewController(ApexPages.StandardController stdcon) {
        lstprojectDUeDateFilter = new list<SelectOption>();
        lstprojectDUeDateFilter.add(new Selectoption('1W','Last 1 Week'));
        lstprojectDUeDateFilter.add(new Selectoption('1M','Last 1 Month'));
        lstprojectDUeDateFilter.add(new Selectoption('3M','Last 3 Months'));        
        lstRecordsPerPageOptions = new list<SelectOption>(); 
        lstRecordsPerPageOptions.add(new Selectoption('10','10'));
        lstRecordsPerPageOptions.add(new Selectoption('25','25'));
        lstRecordsPerPageOptions.add(new Selectoption('50','50'));
        lstRecordsPerPageOptions.add(new Selectoption('100','100'));
        lstRecordsPerPageOptions.add(new Selectoption('500','500'));
        LimitSize= 10;
        OffsetSize = 0;
        totalRecs = 0;        
        quoteObj = (Quote__c)stdcon.getRecord(); 
        lstQuote = new List<Quote__c>();
        sortField = 'CreatedDate';
        sortOrder = 'Desc';
        searchQuote();
    }
                 
    public void searchInQuote(){        
        List<Quote__c> lstQuote;                    
        if(searchStr != null && searchStr.length() > 0){
            String searchString='%'+searchStr+'%';
            string query = 'Select Id, Name, Quote_Number__c, Client__c, Client__r.Name, Contact__c,Contact_Hidden__c, Contact__r.Name, CreatedDate,Status__c, Owner.Name, Quote_Name__c, Quote_Due_Date__c,Project_Due_Date__c,Assigned_To__r.Name From Quote__c';
            string whereClause = ' where ((Quote_Number__c LIKE:searchString or Status__c LIKE:searchString or Contact__r.Name LIKE:searchString or Client__r.Name LIKE:searchString) AND Assigned_To__c = \''+ userInfo.getUserId()+'\')  LIMIT 1000';          
            query = query + whereClause;                        
            system.debug('---query----' +query);
            lstQuote = Database.query(query);                   
        }                                               
        if(lstQuote != null && lstQuote.size() > 0){        
            toDisplayList = new List<ToDisplay>();        
            try{                                    
                for(Integer i=0; i < lstQuote.size(); i++){
                    toDisplayList.add(
                    new toDisplay( lstQuote[i].Id,lstQuote[i].Quote_Number__c,lstQuote[i].Createddate,lstQuote[i].Status__c,
                               lstQuote[i].Client__r.Name, lstQuote[i].Client__c, lstQuote[i].Contact__r.Name,lstQuote[i].Contact__c,
                               lstQuote[i].Project_Due_Date__c, lstQuote[i].owner.Name, lstQuote[i].Assigned_To__r.Name));
                                
                }               
            }catch(Exception e){
                system.debug('Exception occured'+e);
            }           
        } 
    }         
        
    public pageReference searchQuote() { 
        string query = 'Select Id, Name, Quote_Number__c, Client__c, Client__r.Name, Contact__c,Contact_Hidden__c, Contact__r.Name, CreatedDate,Status__c, Owner.Name, Quote_Name__c, Quote_Due_Date__c,Project_Due_Date__c, Assigned_To__r.Name From Quote__c';
        string queryCount = 'Select count() From Quote__c Where Assigned_To__c = \''+userInfo.getUserId()+'\' limit 40000';
        string whereClause = '';
        String orderby = createOrderClause();
        string expiredStatus='Expired';
        whereClause =' Where Assigned_To__c = \''+userInfo.getUserId()+'\'';
        
         
        if(isCustomSearch=='true'){
            OffsetSize=0;
            isCustomSearch='false';
        }
        toDisplayList = new List<ToDisplay>();        
        String limitClause= ' LIMIT '+LimitSize; 
        query = query+whereClause +orderby +' LIMIT 10000';        
        
        try{
                totalRecs = Database.countQuery(queryCount);
            system.debug('--------totalRecs-----: '+totalRecs);
            if(totalRecs<=10000){
                if(math.mod(totalRecs,LimitSize) == 0 ){
                    totalPages=totalRecs/LimitSize;
                }else{
                    totalPages=(totalRecs/LimitSize)+1;
                }
            }else{
                if(math.mod(totalRecs,LimitSize) == 0 ){
                    totalPages=10000/LimitSize;
                }else{
                    totalPages=(10000/LimitSize);
                }
            }           
            if(OffsetSize > 10000){
                selectedPage = (10000/LimitSize);
            }else{
                selectedPage = (OffsetSize/LimitSize)+1;
            }
            setCtrlInst = new ApexPages.StandardSetController(Database.getQueryLocator(query));
            setCtrlInst.setPageSize(LimitSize);
            setCtrlInst.setPageNumber(selectedPage);
            lstQuote = (List<Quote__c>)setCtrlInst.getRecords();
            system.debug(lstQuote+'------------lstQuote--------------');                    
            for(Integer i=0; i<lstQuote.size(); i++){
            toDisplayList.add(
                new toDisplay( lstQuote[i].Id,lstQuote[i].Quote_Number__c,lstQuote[i].Createddate,lstQuote[i].Status__c,
                               lstQuote[i].Client__r.Name, lstQuote[i].Client__c, lstQuote[i].Contact__r.Name,lstQuote[i].Contact__c,
                               lstQuote[i].Project_Due_Date__c, lstQuote[i].owner.Name, lstQuote[i].Assigned_To__r.Name));
                                
           }                      
        }catch(QueryException qe){
            system.debug('---Query Exception--'+qe);
        }
        
        renderQuoteListView = true;
        return null;
    }   
    
    
    public String createOrderClause(){
        String orderClause='';
        if(sortField != null && sortField != ''){
            orderClause = orderClause +' order by '+sortField;
            if(sortOrder == 'Desc'){
                orderClause=orderClause +' '+sortOrder; 
            }  
            orderClause=orderClause + ' NULLS LAST';
        }
        return orderClause;
    }      
    
    public void FirstPage(){        
        OffsetSize = 0;
        searchQuote();
    }
    
    public void previous(){     
        OffsetSize = OffsetSize - LimitSize;
        searchQuote();
    }
    
    public void next(){     
        OffsetSize = OffsetSize + LimitSize;        
        searchQuote();
    }
    
    public void LastPage(){     
        if(totalRecs<=10000){
            if(math.mod(totalRecs,LimitSize)==0){
                OffsetSize = totalrecs - LimitSize;
            }else{
                OffsetSize = totalrecs - math.mod(totalRecs,LimitSize);
            }               
        }else{
            OffsetSize = 10000 - LimitSize;
        }
        searchQuote();
    }
    
    public boolean getprev(){
        if(OffsetSize == 0)
        return true;
        else
        return false;
    }
    
    public boolean getnxt(){
        if(totalRecs<= 10000){
            if((OffsetSize + LimitSize) >= totalRecs)
            return true;
            else
            return false;
        }else{
            if((OffsetSize + LimitSize) >= 10000)
            return true;
            else
            return false;
        }       
    }
    
    public class ToDisplay{
        public String quoteId{get;set;}
        public String quoteNumber{get;set;}
        public DateTime createdDate{get;set;}
        public String Status{get;set;}
        public String AccountName{get;set;}
        public String AccountId{get;set;}
        public String contactName{get;set;}
        public String contactId{get;set;}
        public Date pDueDate{get;set;}
        public String ownerName{get;set;}
        public String assignedTo{get;set;}

       
        public ToDisplay(   
                            string quoteId,
                            String quoteNumber,
                            DateTime createdDate,
                            String Status,
                            String AccountName,
                            String AccountId,
                            String contactName,
                            String contactId,
                            Date pDueDate,
                            String ownerName,
                            String assignedTo
                            ){
            this.quoteId      = quoteId      ;
            this.quoteNumber      = quoteNumber      ;
            this.createdDate      = createdDate     ;
            this.Status           = Status        ;
            this.AccountName      = AccountName       ;
            this.AccountId        = AccountId      ;
            this.contactName      = contactName   ;
            this.contactId        = contactId   ;
            this.pDueDate         = pDueDate   ;
            this.ownerName        = ownerName     ;
            this.assignedTo       = assignedTo == null ? 'None' : assignedTo;
                    
        }
    }
}