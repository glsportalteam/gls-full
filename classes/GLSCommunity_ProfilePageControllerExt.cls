public class GLSCommunity_ProfilePageControllerExt {
	    public User userObj 			{get;set;}
	    public account acc				{get;set;}
	    public String contactName 		{get;set;}
	    public String contactLastName 	{get;set;}
	    public String contactTitle 		{get;set;}
	    public String contactDept 		{get;set;}
	    public String contactPhone 		{get;set;}
	    public String contactEmail 		{get;set;}
	    public String contactFax 		{get;set;}
	    public string ccEmail 			{get;set;}
	    public String streetName 		{get;set;}
	    public String city 				{get;set;}
	    public String state 			{get;set;}
	    public String country 			{get;set;}
	    public String zipCode 			{get;set;}
	    public string address			{get;set;}
		public Boolean isUpdated		{get;set;}
		public Boolean approvalRequired	{get;set;}
		public String contactId         {get;set;} 
		public GLSCommunity_ProfilePageControllerExt(ApexPages.StandardController stdcon){
	        	
	        	contactId = ApexPages.currentPage().getParameters().get('conID');
	        	if(contactId != null && contactId !=''){
	        		userObj=[SELECT Id,Email, UserType,Contact.Account.Name, Contact.FirstName,Contact.LastName ,LastLoginDate, ContactId,
		           				   Contact.Account.Direct_Order_Allowed__c, AccountId, Contact.Name, Name, Contact.Title, Contact.Department, 
		           				   Contact.Phone, Contact.Email, Contact.Fax, Contact.Profile_Image__c, Contact.MailingPostalCode, Contact.MailingStreet, 
		           				   Contact.MailingState,Contact.MailingCity,Contact.MailingCountry,Contact.Cc_Email__c,Contact.Account.Owner.Name, 
		           				   Contact.Account.OwnerId,Contact.Temp_LastName__c ,Contact.Temp_cc_email__c,Contact.Temp_Name__c,Contact.Temp_Phone__c, 
		           				   Contact.Temp_Fax__c,Contact.Initiate_Approval_Process__c,Contact.Temp_MailingPostalCode__c,Contact.Temp_MailingStreet__c, 
		           				   Contact.Temp_MailingState__c, Contact.Temp_MailingCity__c, Contact.Temp_MailingCountry__c,Contact.Temp_Department__c,Contact.Temp_Title__c  
		           				   FROM User 
		           				   WHERE ContactId =: contactId limit 1];
	        		
	        	}else{	        	 
		        	  userObj=[SELECT Id,Email, UserType,Contact.Account.Name, Contact.FirstName,Contact.LastName ,LastLoginDate, ContactId,
		           				   Contact.Account.Direct_Order_Allowed__c, AccountId, Contact.Name, Name, Contact.Title, Contact.Department, 
		           				   Contact.Phone, Contact.Email, Contact.Fax, Contact.Profile_Image__c, Contact.MailingPostalCode, Contact.MailingStreet, 
		           				   Contact.MailingState,Contact.MailingCity,Contact.MailingCountry,Contact.Cc_Email__c,Contact.Account.Owner.Name, 
		           				   Contact.Account.OwnerId,Contact.Temp_LastName__c ,Contact.Temp_cc_email__c,Contact.Temp_Name__c,Contact.Temp_Phone__c, 
		           				   Contact.Temp_Fax__c,Contact.Initiate_Approval_Process__c,Contact.Temp_MailingPostalCode__c,Contact.Temp_MailingStreet__c, 
		           				   Contact.Temp_MailingState__c, Contact.Temp_MailingCity__c, Contact.Temp_MailingCountry__c,Contact.Temp_Department__c,Contact.Temp_Title__c  
		           				   FROM User 
		           				   WHERE Id = :userInfo.getUserId() limit 1];
	        	}
	            
	            approvalRequired = userObj.Contact.Initiate_Approval_Process__c;
			 	acc = new account();
			 	try{
               		acc = [select id, Owner_name__c from account where id =:userObj.Contact.Accountid];
           		}catch(QueryException qe){
               		system.debug('---Exception in Query---: '+qe);
           		}
           		 address=userObj.Contact.MailingStreet;
           		 if(address != null && address != ''){
               		address=address.replace('\n', ' ').replace('\r', ' ');
           		}
           		
		}
		
		/**
        *   @Description : method to update the contact details
        *   @Param : none
        **/
        public void updateUserProfile(){
        		
            	try{
        			if(userObj.Contact.Temp_Title__c  == contactTitle && userObj.Contact.Temp_Department__c == contactDept && userObj.Contact.Temp_LastName__c == contactLastName && userObj.Contact.Temp_cc_email__c == ccEmail && userObj.Contact.Temp_Name__c == contactName && userObj.Contact.Temp_Phone__c == contactPhone && userObj.Contact.Temp_Fax__c == contactFax)
		           	{
	            			isUpdated = false;
	            	}
	            	else{
		            	isUpdated = true;
	            	}
            		system.debug('---------------isUpdated-----------'+isUpdated);
            		if(isUpdated==true){
            				approvalRequired = true;
		                    userObj.Contact.Temp_Title__c = contactTitle;
		                    userObj.Contact.Temp_Department__c = contactDept;  
		                    userObj.Contact.Temp_LastName__c = contactLastName;
		                    userObj.Contact.Temp_cc_email__c = ccEmail;
		                    userObj.Contact.Temp_Name__c = contactName;
		                    userObj.Contact.Temp_Phone__c = contactPhone;
		                    userObj.Contact.Temp_Fax__c = contactFax;
		                    userObj.Contact.Temp_Title__c = contactTitle;
		                    userObj.Contact.Temp_Department__c = contactDept;
		                    userObj.Contact.Initiate_Approval_Process__c = true;
		                   	update userObj.Contact;
		                   // if(userObj.Contact.LastName != contactLastName || userObj.Contact.Temp_cc_email__c!= ccEmail || userObj.Contact.Temp_Name__c != contactName || userObj.Contact.Temp_Phone__c != contactPhone || userObj.Contact.Temp_Fax__c != contactFax){
			                    Case updateProfileCase = new Case();
			                    updateProfileCase.Status = 'New';
			                    updateProfileCase.Origin = 'Community Portal Update';
			                    updateProfileCase.Temp_fax__c=contactFax;
			                    updateProfileCase.Temp_phone__c=contactPhone;
			                    updateProfileCase.Temp_lname__c=contactLastName;
			                    updateProfileCase.Temp_fname__c=contactName;
			                    updateProfileCase.Temp_dept__c=contactDept;
			                    updateProfileCase.Temp_title__c=contactTitle;
			                     updateProfileCase.Temp_email__c=ccEmail;
			                    updateProfileCase.Contact__c = userObj.ContactId;
			                    updateProfileCase.Description = 'Original Info : \n First name - ' + userObj.Contact.FirstName  +
			                                                    '\n Last name - '+ userObj.Contact.LastName +
			                                                    '\n Title - '+ userObj.Contact.Title +
			                                                    '\n Department - '+ userObj.Contact.Department +
			                                                    '\n CC Email - ' + userObj.Contact.cc_email__c +
			                                                    '\n Phone - '+ userObj.Contact.Phone +
			                                                    '\n Fax - '+ userObj.Contact.Fax + 
			                                                    '\n Street - '+ userObj.Contact.MailingStreet + 
			                                                    '\n City - '+ userObj.Contact.MailingCity + 
			                                                    '\n State - '+ userObj.Contact.MailingState +
			                                                    '\n Country - '+ userObj.Contact.MailingCountry + 
			                                                    '\n Postal Code - '+ userObj.Contact.MailingPostalCode + 
			                                                    '\n\nRequested (for update) Info : \n First name - ' + contactName  +
			                                                    '\n Last name - '+ contactLastName +
			                                                    '\n Title - '+ contactTitle +
			                                                    '\n Department - '+ contactDept +
			                                                    '\n CC Email - ' + ccEmail +
			                                                    '\n Phone - '+ contactPhone +
			                                                    '\n Fax - '+ contactFax + 
			                                                    '\n Street - '+ userObj.Contact.Temp_MailingStreet__c + 
			                                                    '\n City - '+ userObj.Contact.Temp_MailingCity__c + 
			                                                    '\n State - '+ userObj.Contact.Temp_MailingState__c +
			                                                    '\n Country - '+ userObj.Contact.Temp_MailingCountry__c + 
			                                                    '\n Postal Code - '+ userObj.Contact.Temp_MailingPostalCode__c ;
			                    updateProfileCase.Subject = 'Profile Update Request From : ' + userObj.Contact.FirstName + ' ' + userObj.Contact.LastName ;
			                    insert updateProfileCase;
			                    Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
			                    req1.setComments('Profile Submitted for Approval');
			                    req1.setObjectId(updateProfileCase.Id);
			                    req1.setNextApproverIds(new Id[] {userObj.Contact.Account.ownerId});
			                    Approval.ProcessResult result = Approval.process(req1);
		                   // } 
		                     
            		}
            }
            catch(Exception e){
                System.debug('Exception in updating admin content--->' + e); 
            }
            //PageReference page = new PageReference('/apex/GLSCommunity_ProfilePage');
            //return page;
        }
        
        /**
        *   @Description : method to update contact's address from popup in Profile section
        *   @Param : none
        **/ 
        public PageReference saveAddressToContact(){
            /*userObj.Contact.MailingStreet = streetName;
            userObj.Contact.MailingCity = city;
            userObj.Contact.MailingState = state;
            userObj.Contact.MailingCountry = country;
            userObj.Contact.MailingPostalCode = zipCode;*/
             userObj.Contact.Temp_MailingStreet__c = streetName;
            userObj.Contact.Temp_MailingCity__c = city;
            userObj.Contact.Temp_MailingState__c = state;
            userObj.Contact.Temp_MailingCountry__c = country;
            userObj.Contact.Temp_MailingPostalCode__c = zipCode;
            
            try{
            	update userObj.Contact;
            } catch(Exception e){
            }
            return null;
        }
}