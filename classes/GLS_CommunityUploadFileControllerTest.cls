/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod 
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLS_CommunityUploadFileControllerTest {

    public static Account accountObj;  
    public static User u;
    public static Contact contactObj;
    public static Quote__c quote;
    public static Quote_Assignment__c qAsst1; 
    public static Quote_Assignment__c qAsst2; 
    public static Language_List_Item__c defLanguage;
    public static Quotes_File__c qFiles1;
    public static Quotes_File__c qFiles2;
    public static list<Quotes_File__c> lstqFiles1;
    public static Quote_Assignment_File__c qAsstFile1;
    public static Quote_Assignment_File__c qAsstFile2;
    public static Quote_Assignment_File__c qAsstFile3;
    public static Quote_Assignment_File__c qAsstFile4;
    public static list<Quote_Assignment_File__c> lstqAsstFile;
    public static File_Format__c fileFormat;
    public static GLS_CommunityFAQ__c FAQ;
    public static case caseObj;
   
    static void setupData(){        
        quote = new Quote__c();
        quote.Quote_Name__c = 'Test Quote';
        //quote.Client__c = accountObj.Id;
        quote.Client__c = u.AccountId;
        quote.Status__c='New';
        //quote.Contact__c = contactObj.Id;
        quote.Contact__c = u.contactId;
        insert quote;
        
        
        defLanguage = new Language_List_Item__c();
        defLanguage.Name = 'English';
        defLanguage.Language_Name__c = 'en-US';
        insert defLanguage;
        
        list<Quote_Assignment__c> lstQuoteAssignment = new list<Quote_Assignment__c>(); 
        qAsst1 = new Quote_Assignment__c();
        qAsst1.Quote__c = quote.Id; 
        qAsst1.Source_Language__c = defLanguage.Id;     
        qAsst1.Allowed_File_Formats__c = 'MS Word';
        qAsst1.Document_Type__c = 'IFC,SomeData;';
        qAsst1.Keywords__c='one;';
        //insert qAsst1;  
        lstQuoteAssignment.add(qAsst1);
        
        qAsst2 = new Quote_Assignment__c();
        qAsst2.Quote__c = quote.Id; 
        qAsst2.Source_Language__c = defLanguage.Id;     
        qAsst2.Allowed_File_Formats__c = 'MS Word';
        qAsst2.Document_Type__c = 'IFC,SomeData;';
        qAsst2.Keywords__c='';
        //insert qAsst2;
        lstQuoteAssignment.add(qAsst2);
        
        insert lstQuoteAssignment;
         
        list<Quotes_File__c> lstQuoteFile = new list<Quotes_File__c>(); 
        qFiles1 = new Quotes_File__c();
        qFiles1.File_Name__c='Test1.doc';
        qFiles1.TradosAPIFileID__c='1234'; 
        qFiles1.TradosAPITransactionID__c='xasafafdsf143';
        qFiles1.Quote__c=quote.id;
        //insert qFiles1;
        lstQuoteFile.add(qFiles1);
        qFiles2 = new Quotes_File__c();
        qFiles2.File_Name__c='Test2.doc';
        qFiles2.TradosAPIFileID__c='1234'; 
        qFiles2.TradosAPITransactionID__c='xasafafdsf143';
        qFiles2.Quote__c=quote.id;
        //insert qFiles2;
        lstQuoteFile.add(qFiles2);
        insert lstQuoteFile;
        
        list<Quote_Assignment_File__c> lstQuoteAssignFile = new list<Quote_Assignment_File__c>();  
        qAsstFile1 = new Quote_Assignment_File__c();
        qAsstFile1.Quote_Assignment__c = qAsst1.Id;
        qAsstFile1.Quote_Files__c = qFiles1.id;
        qAsstFile1.File_Usage__c = 'Client';   
        //insert qAsstFile1;
        lstQuoteAssignFile.add(qAsstFile1);
        
        qAsstFile2 = new Quote_Assignment_File__c();
        qAsstFile2.Quote_Assignment__c = qAsst1.Id;
        qAsstFile2.Quote_Files__c = qFiles2.id;
        qAsstFile2.File_Usage__c = 'Client';   
        //insert qAsstFile2;
        lstQuoteAssignFile.add(qAsstFile2);
        
        qAsstFile3 = new Quote_Assignment_File__c();
        qAsstFile3.Quote_Assignment__c = qAsst2.Id;
        qAsstFile3.Quote_Files__c = qFiles1.id;
        qAsstFile3.File_Usage__c = 'Client';   
        //insert qAsstFile3;
        lstQuoteAssignFile.add(qAsstFile3);
        
        qAsstFile4 = new Quote_Assignment_File__c();
        qAsstFile4.Quote_Assignment__c = qAsst2.Id;
        qAsstFile4.Quote_Files__c = qFiles2.id;
        qAsstFile4.File_Usage__c = 'Client';   
        //insert qAsstFile4;
        lstQuoteAssignFile.add(qAsstFile4);
        
        insert lstQuoteAssignFile; 
        fileFormat = new File_Format__c();
        fileFormat.Name = 'MS Word';
        fileFormat.File_Extension__c = '.doc; .docm; .docx; .dot; .dotm; .dotx';
        insert fileFormat;
        
      
    } 
    
    static void setupData1(){
        
        
        quote = new Quote__c();
        quote.Quote_Name__c = 'Test Quote';
        //quote.Client__c = accountObj.Id;
        quote.Client__c = u.AccountId;
        quote.Status__c='Draft';
        //quote.Contact__c = contactObj.Id;
        quote.Contact__c = u.contactId;
        insert quote;
        
        
        defLanguage = new Language_List_Item__c();
        defLanguage.Name = 'English';
        defLanguage.Language_Name__c = 'en-US';
        insert defLanguage;
        
        list<Quote_Assignment__c> lstQuoteAssignment = new list<Quote_Assignment__c>(); 
        qAsst1 = new Quote_Assignment__c();
        qAsst1.Quote__c = quote.Id; 
        qAsst1.Source_Language__c = defLanguage.Id;     
        qAsst1.Allowed_File_Formats__c = 'MS Word';
        qAsst1.Document_Type__c = 'IFC,SomeData;';
        qAsst1.Keywords__c='one;';
        //insert qAsst1;  
        lstQuoteAssignment.add(qAsst1);
        
        qAsst2 = new Quote_Assignment__c();
        qAsst2.Quote__c = quote.Id; 
        qAsst2.Source_Language__c = defLanguage.Id;     
        qAsst2.Allowed_File_Formats__c = 'MS Word';
        qAsst2.Document_Type__c = 'IFC,SomeData;';
        qAsst2.Keywords__c='';
        //insert qAsst2;
        lstQuoteAssignment.add(qAsst2);
        insert lstQuoteAssignment;  
        
        list<Quotes_File__c> lstQuoteFile = new list<Quotes_File__c>(); 
        qFiles1 = new Quotes_File__c();
        qFiles1.File_Name__c='Test1.doc';
        qFiles1.TradosAPIFileID__c='1234'; 
        qFiles1.TradosAPITransactionID__c='xasafafdsf143';
        qFiles1.Quote__c=quote.id;
        //insert qFiles1;
        lstQuoteFile.add(qFiles1);
        
        qFiles2 = new Quotes_File__c();
        qFiles2.File_Name__c='Test2.doc';
        qFiles2.TradosAPIFileID__c='1234'; 
        qFiles2.TradosAPITransactionID__c='xasafafdsf143';
        qFiles2.Quote__c=quote.id;
        //insert qFiles2;
        lstQuoteFile.add(qFiles2);
        insert lstQuoteFile; 
        
        list<Quote_Assignment_File__c> lstQuoteAssignmentFile =new list<Quote_Assignment_File__c>(); 
        qAsstFile1 = new Quote_Assignment_File__c();
        qAsstFile1.Quote_Assignment__c = qAsst1.Id;
        qAsstFile1.Quote_Files__c = qFiles1.id;
        qAsstFile1.File_Usage__c = 'Client';   
        //insert qAsstFile1;
        lstQuoteAssignmentFile.add(qAsstFile1);
        
        qAsstFile2 = new Quote_Assignment_File__c();
        qAsstFile2.Quote_Assignment__c = qAsst1.Id;
        qAsstFile2.Quote_Files__c = qFiles2.id;
        qAsstFile2.File_Usage__c = 'Client';   
        //insert qAsstFile2;
        lstQuoteAssignmentFile.add(qAsstFile2);
                
        qAsstFile3 = new Quote_Assignment_File__c();
        qAsstFile3.Quote_Assignment__c = qAsst2.Id;
        qAsstFile3.Quote_Files__c = qFiles1.id;
        qAsstFile3.File_Usage__c = 'Client';   
        //insert qAsstFile3;
        lstQuoteAssignmentFile.add(qAsstFile3);
        
        qAsstFile4 = new Quote_Assignment_File__c();
        qAsstFile4.Quote_Assignment__c = qAsst2.Id;
        qAsstFile4.Quote_Files__c = qFiles2.id;
        qAsstFile4.File_Usage__c = 'Client';   
        //insert qAsstFile4;
        lstQuoteAssignmentFile.add(qAsstFile4);
        
        insert lstQuoteAssignmentFile;
        fileFormat = new File_Format__c();
        fileFormat.Name = 'MS Word';
        fileFormat.File_Extension__c = '.doc; .docm; .docx; .dot; .dotm; .dotx';
        insert fileFormat;
        
      
    } 
    
    static testMethod void myUnitTest1() {
         
        test.startTest();
        
        accountObj = new Account();
        accountObj.Name = 'Amgen';
        accountObj.BillingState ='Texas';
        insert accountObj;
         
        contactObj = new Contact(); 
        contactObj.AccountId = accountObj.Id;
        contactObj.FirstName = 'Jennifer A';
        contactObj.LastName = 'Jennifer A';
        insert contactObj; 
        
        Profile p = [select id from profile where UserType='CspLitePortal' limit 1][0];

        u = [SELECT Id, contactId, accountId  FROM User where contactId != null AND accountid != null ][0];
        
        System.runAs ( new User(Id = u.id) ){
            setupData();
            GLS_CommunityUploadFileController obj = new GLS_CommunityUploadFileController();
            obj.updateCurrentUserQuotes();
            obj.orderIsFromPlaceOrder();
            obj.existingQuoteId = quote.Id;
            obj.openExistingQuote();
            obj.switchSubProjId = qAsst2.Id;
            obj.changeSubProject();
            
            obj.commentFileId = qFiles1.Id;
            obj.commentForFile ='Single File Comment';
            obj.addCommentToSingleFile();
            
            obj.commentFileIdExisting = qFiles1.Id;
            obj.findCommentToSingleFile();
            
            obj.keywords='one;two;three;four;';
            obj.updateKeywordsToAssignment();
            
            obj.expedited = true;
            obj.saveQuoteRelatedInfo();
            
            obj.expedited = false;
            obj.saveQuoteRelatedInfo();
            
            //obj.switchSubProjId = qAsst1.Id;
            obj.keywords='one;two;three;four;';
            obj.updateKeywordsToAssignment();
            
            obj.keywordToDelete = 'one';
            obj.deleteExistingKeyword();
            
            obj.linkFiles.Link_To_File__c = 'www.googledocs.com/data1';
            obj.linkFiles.File_Description__c ='test link file1';
            obj.addFileLink();
            
            obj.linkFiles.Link_To_File__c = 'www.googledocs.com/data2';
            obj.linkFiles.File_Description__c ='test link file2';
            obj.addFileLink();
            
            obj.linkFileComment ='Some Comment';
            obj.lstAssnFilesWrapper[0].isSelected = true;
            obj.addCommentToLink();
            
            obj.linkFilesList[0].isSelected = true;
            obj.refLinkFileMultiple = 'Some data';
            obj.refLinkFileMultipleURL = '';
            obj.addRefToLink();
            
            obj.refLinkFileMultiple = '';
            obj.refLinkFileMultipleURL = 'Some data';
            obj.addRefToLink();
            
            obj.referenceFileId = qFiles1.Id;
            obj.referenceForFile = 'Some Data';
            obj.referenceForFileURL = '';
            obj.addReferenceToSingleFile();
            
            obj.referenceFileId = qFiles2.Id;
            obj.referenceForFile = '';
            obj.referenceForFileURL = 'Some Data';
            obj.addReferenceToSingleFile();
            
            obj.referenceForLinkFile = 'Some Data';
            obj.referenceForLinkFileURL = '';
            obj.referenceForLinkIndex =0;
            obj.addReferencetToSingleLinkFile();
            
            obj.referenceForLinkFile = '';
            obj.referenceForLinkFileURL = 'Some Link';
            obj.referenceForLinkIndex =0;
            obj.addReferencetToSingleLinkFile();
            
            obj.uploadedFilesExtensions = '.doc';
            
            obj.uploadProfileImage();
           
            //obj.saveAddressToContact();
            
            obj.deleteLinkIndex =0;
            obj.deleteSingleLinkFile();

            
        }
        test.stopTest();
    }
    
    
    static testMethod void myUnitTest2() {
                
        accountObj = new Account();
        accountObj.Name = 'Amgen';
        accountObj.BillingState ='Texas';
        insert accountObj;
         
        contactObj = new Contact(); 
        contactObj.FirstName = 'Jennifer A';
        contactObj.LastName = 'Jennifer A';
        contactObj.AccountId = accountObj.Id;
        insert contactObj; 
        
        Profile p = [select id from profile where name=: GLSConfig.Communityprofile];

        u = [SELECT Id, contactId, accountId  FROM User where contactId != null AND accountid != null ][0];        
        System.runAs ( new User(Id = u.id) ){        	
            setupData();            
            test.startTest();
            GLS_CommunityUploadFileController obj = new GLS_CommunityUploadFileController();
            obj.existingQuoteId = quote.id;
            obj.openExistingQuote();
            
            obj.keywords='one;two;three;four;';
            obj.updateKeywordsToAssignment();
            
            obj.keywordToDelete = 'one';
            obj.deleteExistingKeyword();
            
            obj.linkFiles.Link_To_File__c = 'www.googledocs.com/data1';
            obj.linkFiles.File_Description__c ='test link file1';
            obj.addFileLink();
            
            obj.linkFiles.Link_To_File__c = 'www.googledocs.com/data2';
            obj.linkFiles.File_Description__c ='test link file2';
            obj.addFileLink();
            
            obj.commentFileLinkIndex = 0;
            obj.commentForLinkFile= 'SomeFile description';
            obj.addCommentToSingleLinkFile();
            
            
            
            obj.updateUserProfile();
            
            obj.saveandcontinue();
            
            obj.orderIsFromPlaceOrder();
            obj.sendEmailOnPlacingOrder();
            obj.deleteNewSubProject();         
            test.stopTest();   
        }     
    }
    
    
    
     static testMethod void myUnitTest4() {
        
        test.startTest();
        accountObj = new Account();
        accountObj.Name = 'Amgen';
        accountObj.BillingState ='Texas';
        insert accountObj;
         
        contactObj = new Contact(); 
        contactObj.FirstName = 'Jennifer A';
        contactObj.LastName = 'Jennifer A';
        contactObj.AccountId = accountObj.Id;
        insert contactObj; 
        
        Profile p = [select id from profile where name=: GLSConfig.Communityprofile];

        u = [SELECT Id, contactId, accountId  FROM User where contactId != null AND accountid != null ][0];
        
        System.runAs ( new User(Id = u.id) ){
            Quote__c quote1 = new Quote__c();
            quote1.Client__c = u.AccountId;
            quote1.Status__c='New';
            quote1.Contact__c = u.contactId;
            insert quote1;
            GLS_CommunityUploadFileController obj = new GLS_CommunityUploadFileController();
            obj.existingQuoteId = quote1.id;
            obj.openExistingQuote();
            obj.areLangsForAllProjectsSelected();
        }
         test.stopTest();
     }
     
     
    static testMethod void myUnitTest3() {
        
        test.startTest();
        accountObj = new Account();
        accountObj.Name = 'Amgen';
        accountObj.BillingState ='Texas';
        insert accountObj;
         
        contactObj = new Contact(); 
        contactObj.FirstName = 'Jennifer A';
        contactObj.LastName = 'Jennifer A';
        contactObj.AccountId = accountObj.Id;
        insert contactObj; 
        
        Profile p = [select id from profile where name=: GLSConfig.Communityprofile];

        u = [SELECT Id, contactId, accountId  FROM User where contactId != null AND accountid != null ][0];
         
        System.runAs ( new User(Id = u.id) ){
            setupData();
            GLS_CommunityUploadFileController obj = new GLS_CommunityUploadFileController();
            obj.existingQuoteId = quote.id;
            obj.openExistingQuote();
            obj.linkFiles.Link_To_File__c = 'www.googledocs.com/data1';
            obj.linkFiles.File_Description__c ='test link file1';
            obj.addFileLink();
            
            obj.linkFiles.Link_To_File__c = 'www.googledocs.com/data2';
            obj.linkFiles.File_Description__c ='test link file2';
            obj.addFileLink(); 
            obj.quoteObj = quote;   
            
            
            obj.lstAssnFilesWrapper[0].isSelected = true;
            obj.linkFilesList[0].isSelected = true;
            obj.removeFileLinkAndAmazon();      
            //obj.saveandNew();
            
            obj.deleteFileId = qFiles2.Id;
            obj.deleteSingleExistingFile();
            
            obj.deleteSubProjectId = qAsst2.Id;
            obj.deleteSubProject();
            
            obj.deleteProjectId = quote.Id;
            //obj.deleteProject();
  
            obj.editBundle(); 
           
            
            
        }
        test.stopTest();
    }
    
    
    
    static testMethod void myUnitTest5() {
        
        test.startTest();
        accountObj = new Account();
        accountObj.Name = 'Amgen';
        accountObj.BillingState ='Texas';
        insert accountObj;
         
        contactObj = new Contact(); 
        contactObj.FirstName = 'Jennifer A';
        contactObj.LastName = 'Jennifer A';
        contactObj.AccountId = accountObj.Id;
        insert contactObj; 
        
        Profile p = [select id from profile where name=: GLSConfig.Communityprofile];

        u = [SELECT Id, contactId, accountId  FROM User where contactId != null AND accountid != null ][0];
        
        System.runAs ( new User(Id = u.id) ){
            setupData();
            GLS_CommunityUploadFileController obj = new GLS_CommunityUploadFileController();
            obj.existingQuoteId = quote.id;
            obj.openExistingQuote();
            obj.newFileName = 'testt.doc';
            obj.QuoteId_value=quote.Id;
            obj.fileSize = 2048;
            obj.successFileupload();
            obj.successFileuploadRef();
            obj.obtainListOfFiles();
            obj.resetctr();
            
        }
        test.stopTest();
    } 
    
       static testMethod void myUnitTest6() {
        
        test.startTest();
        accountObj = new Account();
        accountObj.Name = 'Amgen';
        accountObj.BillingState ='Texas';
        insert accountObj;
         
        contactObj = new Contact(); 
        contactObj.FirstName = 'Jennifer A';
        contactObj.LastName = 'Jennifer A';
        contactObj.AccountId = accountObj.Id;
        insert contactObj; 
        
        Profile p = [select id from profile where name=: GLSConfig.Communityprofile];

        u = [SELECT Id, contactId, accountId  FROM User where contactId != null AND accountid != null ][0];
        System.runAs ( new User(Id = u.id) ){
            setupData();
            GLS_CommunityUploadFileController obj = new GLS_CommunityUploadFileController();
            obj.existingQuoteId = quote.id;
            obj.openExistingQuote();
            
            obj.keywords='one;two;three;four;';
            obj.updateKeywordsToAssignment();
            
            obj.keywordToDelete = 'one';
            obj.deleteExistingKeyword();
            
                
            
            
            obj.updateUserProfile();
            
            obj.saveandNew();
            obj.quoteObj = null;
            obj.createNewQuote();
            obj.quoteAssignmentObj = null;
            obj.createNewQuoteAssignment();
            
            obj.refreshQuoteAssignmentObj(qAsst1.Id);
        }
        test.stopTest();
    }
    
     static testMethod void myUnitTest7() {
        
        test.startTest();
        accountObj = new Account();
        accountObj.Name = 'Amgen';
        accountObj.BillingState ='Texas';
        insert accountObj;
         
        contactObj = new Contact(); 
        contactObj.FirstName = 'Jennifer A';
        contactObj.LastName = 'Jennifer A';
        contactObj.AccountId = accountObj.Id;
        insert contactObj; 
        
        Profile p = [select id from profile where name=: GLSConfig.Communityprofile];

        u = [SELECT Id, contactId, accountId  FROM User where contactId != null AND accountid != null ][0];
        System.runAs ( new User(Id = u.id) ){
            setupData1();
            GLS_CommunityUploadFileController obj = new GLS_CommunityUploadFileController();
            obj.existingQuoteId = quote.id;
            obj.openExistingQuote();
            
            obj.keywords='one;two;three;four;';
            obj.updateKeywordsToAssignment();
            
            obj.keywordToDelete = 'one';
            obj.deleteExistingKeyword();
            
            obj.linkFiles.Link_To_File__c = 'www.googledocs.com/data1';
            obj.linkFiles.File_Description__c ='test link file1';
            obj.addFileLink();
            
            obj.linkFiles.Link_To_File__c = 'www.googledocs.com/data2';
            obj.linkFiles.File_Description__c ='test link file2';
            obj.addFileLink();
            
            obj.commentFileLinkIndex = 0;
            obj.commentForLinkFile= 'SomeFile description';
            obj.addCommentToSingleLinkFile();
            
            
            
            obj.updateUserProfile();
            
            obj.saveandNew();
         Quote__c  quote1 = new Quote__c();
        quote1.Quote_Name__c = 'Test Quote';
        //quote.Client__c = accountObj.Id;
        quote1.Client__c = u.AccountId;
        quote1.Status__c='Draft';
        //quote.Contact__c = contactObj.Id;
        quote1.Contact__c = u.contactId;
        insert quote1;
            obj.removeFileLinkAndAmazon();
               obj.deleteProjectId = quote.Id;
            obj.deleteProject() ;   
            
        }
        test.stopTest();
    }
    
    static testMethod void myUnitTest8() {
        
        test.startTest();
        accountObj = new Account();
        accountObj.Name = 'Amgen';
        accountObj.BillingState ='Texas';
        insert accountObj;
         
        contactObj = new Contact(); 
        contactObj.FirstName = 'Jennifer A';
        contactObj.LastName = 'Jennifer A';
        contactObj.AccountId = accountObj.Id;
        insert contactObj; 
        
        Profile p = [select id from profile where name=: GLSConfig.Communityprofile];

        u = [SELECT Id, contactId, accountId  FROM User where contactId != null AND accountid != null ][0];
         
        System.runAs ( new User(Id = u.id) ){
            setupData();
            GLS_CommunityUploadFileController obj = new GLS_CommunityUploadFileController();
            obj.existingQuoteId = quote.id;
            obj.openExistingQuote();
            
            
            obj.refreshFAQ();
            caseObj = new case();
            caseObj.Subject ='Case Question';
            caseObj.Description = 'some desc';
            insert caseObj;
            
            FAQ = new GLS_CommunityFAQ__c ();
            FAQ.Question__c ='Question';
            FAQ.Solution__c = 'Solution';
            
            GLS_CommunityUploadFileController.QuestionsWrapperClass QueWrapper = new GLS_CommunityUploadFileController.QuestionsWrapperClass();
            
            GLS_CommunityUploadFileController.DocumentTypeWrapperClass dTwrapper = new GLS_CommunityUploadFileController.DocumentTypeWrapperClass ();
            GLS_CommunityUploadFileController.DeliveryFormatWrapperClass dformatWrapper = new GLS_CommunityUploadFileController.DeliveryFormatWrapperClass();
            GLS_CommunityUploadFileController.FileWrapperClass fWrapper1 = new GLS_CommunityUploadFileController.FileWrapperClass(qFiles2, true, true);
            GLS_CommunityUploadFileController.FileWrapperClass fWrapper2 = new GLS_CommunityUploadFileController.FileWrapperClass();
            GLS_CommunityUploadFileController.AssnFileWrapperClass assWrapper1 = new GLS_CommunityUploadFileController.AssnFileWrapperClass(qAsstFile1, true, true);
            GLS_CommunityUploadFileController.AssnFileWrapperClass assWrapper2 = new GLS_CommunityUploadFileController.AssnFileWrapperClass ();
            obj.refreshFAQ();
            obj.replyCaseId = caseObj.Id;
            obj.replyComment = 'case Comment';
            obj.createNewCommentForCase();
            
            for(Case cs :obj.lstcases){
                if(cs.Id == obj.replyCaseId){
                    cs.Status = 'Closed';
                }
            }
                       
            
            obj.replyCaseId = caseObj.Id;
            obj.replyComment = 'case Comment reopen';
            obj.createNewCommentForCase();
            
            obj.caseSubject= 'subject ';
            obj.caseDescription = 'description';
            obj.createNewQuestion();
            
            
            obj.editBundle(); 
           
            
            
        }
        test.stopTest();
    }
}