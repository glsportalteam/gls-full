/*
* Class 		: 	GLSCommunity_ShareLinkPage
* Description	:	Controller extension class for login to download the deliverable files for the USER3 as well
			        as for existing portal users
*					
* Dependencies	: 
*		Classes		: 	GLSConfig
*		VF Page		: 	GLSCommunity_UserAuthenticationPage
*				   	      
*/
without sharing global class GLSCommunity_UserAuthenticationCtrl {
	
	/*Password entered by user*/           
    public string password                                     {get;set;}
    
    /*Email id entered by user if user is not portal user*/
    public string email                                        {get;set;}
    
    /*Username entered by the user if user is portal user*/ 
    public string userName                                     {get;set;}
      
    public boolean validuser                                   {get;set;}
    
    public list<Quotes_File__c> quotefilelst                   {get;set;}
    
    public id downloadFileId                                   {get;set;}
    
    public list<fileWrapper> lstfileWrapper                    {get;set;}
    
    public LPFileSharingAccessManagement__c fileAccessRec      {get;set;}
    
    private string quoteId;
                        
    public Boolean isPortalUser                                {get;set;}
    public boolean isAccessExpired                             {get;set;}
    /*Constructor*/
    public GLSCommunity_UserAuthenticationCtrl(){
    	isPortalUser = false;
    	String accessRecord = ApexPages.currentPage().getParameters().get('accessId');
    	isAccessExpired = false;
    	if(accessRecord != null){
    		/*Fetching the LPSharingAccessManagement record for verifying user credential while login*/
	    	list<LPFileSharingAccessManagement__c> fileSharingRecordLst = [SELECT id,Email__c,First_Name__c,
	    																		  IncorrectLoginAttempt__c,
	    																		  User_Id__r.Name,
												   								  isAccessActive__c,Last_Name__c,
												   								  Password__c,Name,
												   								  Quote_Id__c,User_Id__c,
												   								  User_Id__r.contactId ,CreatedBy.Name
										   								  FROM LPFileSharingAccessManagement__c 
										   								  WHERE Id=:accessRecord And 
										   								        IncorrectLoginAttempt__c < 6 
										   								        And isAccessActive__c = true
										   								  limit 1];
										   								  
			if(fileSharingRecordLst != null && fileSharingRecordLst.size() > 0){
				fileAccessRec = fileSharingRecordLst[0];
				quoteId = fileSharingRecordLst[0].Quote_Id__c;
				if(fileAccessRec.User_Id__c != null && fileAccessRec.User_Id__r.contactId != null ){
		    		List<contact> con = [SELECT id,isPortalActivated__c 
		    							 FROM contact 
		    							 WHERE id=: fileAccessRec.User_Id__r.contactId limit 1];
		    		if(con != null && con.size() > 0){
		    			isPortalUser = con[0].isPortalActivated__c;
		    		}
		    	}
			}else{
				isAccessExpired = true;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'Access expired! Please contact portal administrator.'));
			}							   								  
    	}
    }
    
    /*
	*	Method		:	login	 
	*	Description	:	Validate the user credentials, standard portal user have it enter its org credentials 
						for rest the username,password shared by the admin user have to be entered.
						On successful authentication user is provided with the list delivereable files with
						download button for downloading individual file. 				 
	*   param       :   --> Username
						--> Password 
	* 	return 		: 	Void						
	*/
    public void login(){
    	/*Enters inside if user is standard portal user*/
    	if(isPortalUser != null && isPortalUser){
    	   PageReference pr = null;
           try{           		
                pr = Site.login(userName,password, '/apex/GLSCommunity_DraftPage');                
           }catch(Exception e){           		           		
           		string strMsg='';    
           		if(userName == ''){
           			strMsg = 'Please Enter your User Name.';
           		} else if(password == ''){
           			strMsg = 'Please Enter your Password.';
           		} else{       
             		strMsg = 'Portal is under construction, please try after some time or contact to helpdesk.';
           		}
           		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,strMsg));
            }
            /*Enters inside if on unsuccessful user authentication*/
        	if(pr == null){              
                try{
                	/*On each incorrect login attemp increase invalidLogin count by 1*/
    		        if(Integer.valueOf(fileAccessRec.IncorrectLoginAttempt__c) < 5){
	    		        fileAccessRec.IncorrectLoginAttempt__c = fileAccessRec.IncorrectLoginAttempt__c + 1;
	    			    update fileAccessRec;
			    	    Integer remainingAttempt = (5 - Integer.valueOf(fileAccessRec.IncorrectLoginAttempt__c)); 
			    	    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,remainingAttempt+' login attempt remaining.'));
	    		    }else{
	    				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'User account is locked. Please contact portal administrator.'));
	    		    }
	    		}catch(DMLException ex){
	    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'An unexpected error occured please contact system administrator.'));
	    		}catch(Exception e){
	    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'An unexpected error occured please contact system administrator.'));
	    		}
	    		validuser = false; 
           }else{
           	   /*On each successful user authentication update IncorrectLogin attempt with zero*/
           	   if(Integer.valueOf(fileAccessRec.IncorrectLoginAttempt__c) != 0){
	    			fileAccessRec.IncorrectLoginAttempt__c = 0;
	    			try{
	    				update fileAccessRec;
	    			}catch(DMLException e){
	    				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,e.getMessage()));
	    			}
	    	   }
               validuser = true;
           }           
    	}else if(password == fileAccessRec.Password__c && email == fileAccessRec.Email__c){
    		if(Integer.valueOf(fileAccessRec.IncorrectLoginAttempt__c) != 0){
    			fileAccessRec.IncorrectLoginAttempt__c = 0;
    			try{
    				update fileAccessRec;
    			}catch(DMLException e){
    				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,e.getMessage()));
    			}
    		}
    		validuser = true;
    	}else{    		
    		try{
    			if(Integer.valueOf(fileAccessRec.IncorrectLoginAttempt__c) < 5){
    				fileAccessRec.IncorrectLoginAttempt__c = fileAccessRec.IncorrectLoginAttempt__c + 1;
    				update fileAccessRec;
		    		Integer remainingAttempt = (5 - Integer.valueOf(fileAccessRec.IncorrectLoginAttempt__c)); 
		    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,remainingAttempt+' login attempt remaining.'));
    			}else{
    				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'User account is locked. Please contact Portal Administrator.'));
    			}
    		}catch(Exception e){
    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'An unexpected error occured please contact system administrator.'));
    		}
    		validuser = false; 
    	}
    	/*If user is valid user than fecth list of all the all the deliverable files*/
    	if(validuser){
    		 /*Insert a logger record on successful file download*/            
    		try{ 
	            LP_File_Sharing_Access_Management_Logger__c	 logger = new LP_File_Sharing_Access_Management_Logger__c();
			        logger.Access_Granter_Name__c = fileAccessRec.CreatedBy.Name;
			        logger.Access_Recipient_Name__c = fileAccessRec.Email__c;
			        logger.isPortalUser__c = isPortalUser;
			        logger.LPFileSharingAccessManagement__c = fileAccessRec.Id;
			        if(quoteId != null)
			        	logger.Quote_Number__c = quoteId;
			        //logger.Quote_Files__c = FileId.Id;	        
		        insert logger;
    		}catch(DMLException ex){
    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'An unexpected error occured please contact system administrator.'));
    		}
    		if(quoteId != null){
    			quotefilelst = [SELECT id,File_Name__c,File_Size__c,File_Type__c,Language_Pair__c,Quote__r.Name
    						    FROM quotes_file__c 
    						    WHERE Quote__c=:quoteId and File_Type__c =: GLSConfig.deliverable and Released__c = true];
    						    
    			lstfileWrapper = new List<fileWrapper>();
    			for(quotes_file__c qf:quotefilelst){
    				fileWrapper fw = new fileWrapper();
    				fw.fId =  qf.id;
    				fw.fname = qf.File_Name__c;
    				if(qf.File_Size__c != null){
    					fw.fsize = qf.File_Size__c.setScale(2);
    				}    				
    				fw.languagePair = qf.Language_Pair__c;
    				fw.ftype = qf.File_Type__c;
    				fw.qName = qf.Quote__r.Name;
    				lstfileWrapper.add(fw);
    			}
    		}
    	}
    }

    /*
	*	Method		:	downloadFile	 
-	*	Description	:	Download the file on click of download button on files table and make one entry in logger object for the 
				        downloaded file. 				 
	*   param       :   downloadFileId						
	* 	return 		: 	pageReference(File Download amazon link)						
	*/
    global pageReference downloadFile(){ 
        String StMessage = '';   
        String StatusMessage ='';
        PageReference pref = null;        
        Quotes_File__c FileId= [SELECT Quote__r.Id,Quote__r.Name,File_Name__c,AWSFilePath__c, 
        							   Bucket_Name__c,Amazon_S3_source_file_path__c
        					    FROM Quotes_File__c 
        					    WHERE id=:downloadFileId LIMIT 1];        
        String bucketName = FileId.Bucket_Name__c;        
        String path = FileId.Amazon_S3_source_file_path__c;
        
        /*Add a save point*/               
        Savepoint sp = Database.setSavepoint();
        try{
            StatusMessage = GLSAmazonUtility.downloadQuoteFile(path, GLSConfig.bucketName);
            if(StatusMessage != null)
                pref = new Pagereference(StatusMessage);
            
            /*Insert a logger record on successful file download*/            
            /*LP_File_Sharing_Access_Management_Logger__c	 logger = new LP_File_Sharing_Access_Management_Logger__c();
		        logger.Access_Granter_Name__c = fileAccessRec.CreatedBy.Name;
		        logger.Access_Recipient_Name__c = fileAccessRec.Email__c;
		        logger.isPortalUser__c = isPortalUser;
		        logger.LPFileSharingAccessManagement__c = fileAccessRec.Id;
		        logger.Quote_Files__c = FileId.Id;	        
	        insert logger;*/        
	                
        }catch(DMLException ex){
        	StMessage = ex.getMessage();
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'Access expired! Please contact Portal Administrator'));
        	Database.rollback(sp);
        }catch(Exception e){
            StMessage = e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'Access expired! Please contact Portal Administrator'));
            Database.rollback(sp);                           
        } 
        return pref;           
    }
    
    /*Wrapper class*/
    public class fileWrapper{
    	public String fId         {get;set;}
    	public string fname       {get;set;}
    	public string ftype       {get;set;} 
    	public Double fsize       {get;set;}
    	public string qName       {get;set;}
    	public string languagePair{get;set;}
    }
}