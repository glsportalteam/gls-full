public with sharing class LMS_EnrollForTrainingController {
    public User_Training__c enrolledTraining 			{get;set;}
    public List<User_Training__c> enrolledTrainingList	{get;set;}
    public String URL									{get;set;}
    Public Training__c training 						{get;set;}
    String trainingId 									{get;set;}
    public boolean flag									{get;set;}
    
    //constructor
    public LMS_EnrollForTrainingController() 
    {
    	    	
    	enrolledTrainingList = new List<User_Training__c>();
        trainingId =  ApexPages.currentPage().getParameters().get('id');      
        enrolledTraining = new User_Training__c();
        training = new Training__c();
        //Get the loggedin user info
        User user = [select id from User where id=:UserInfo.getUserId()];  
        //get the training to be enrolled
        If(trainingId != null || trainingId != '')
        {
            training = [SELECT name,Training_Name__c, End_Date__c,Duration__c ,Criterion__c,Training_Required_for__c,Start_Date__c,Trainer_Name__c,Training_Methodologies__c,Training_Description__c FROM Training__c WHERE id =:trainingId limit 1];
            	enrolledTraining = validateEnroll(training);
			if(enrolledTraining.Due_Date__c != null){	
				flag =true;
	            enrolledTraining.User_Name__c = user.Id;
	            enrolledTraining.Date_of_Enrollment__c =Date.today();
	            if(enrolledTraining.Status__c=='completed')
	                enrolledTraining.Date_of_Completion__c =training.End_Date__c;
	            enrolledTraining.status__c = LMS_Literals.InitialEnrollmentStatus;
	            enrolledTraining.Training__c = trainingId;
	            enrolledTraining.IsAutoEnrolled__c = false;
	            enrolledTrainingList.add(enrolledTraining);
	            
			}
			else{
				flag = false;
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Can not Enroll for the training as due date has passed');
                  ApexPages.addMessage(myMsg);
			
			}
        }        
    }
    
    //function to Enroll for the training
    public PageReference save()
    {
        if(enrolledTraining != null)
        {
            try
            {
                if(enrolledTrainingList != null && enrolledTrainingList.size()>0)
                	insert enrolledTraining;
            }
            catch(DMLException e){
                
            }
            PageReference homePage = null;
            homePage = Page.glsLmsHome; // use the name of the second VF page            
            homePage.setRedirect(true);
            return homePage;
        }
        else
        {
            return null;
        }
        
    }
    public User_Training__c validateEnroll( Training__C training){
    Decimal  duration =0;
				
				if(training.End_Date__c != null)
				{
					if(training.Duration__c != null){
						duration = training.Duration__c;
						if((Date.today().addDays(Integer.valueOf(duration))) <=  training.End_Date__c)
						{
							
							if( Date.today().addDays(Integer.valueOF(training.Criterion__c)) <
							 training.End_Date__c )
							{
									enrolledTraining.Due_Date__c = Date.today().addDays(Integer.valueOf(training.Criterion__c));
							}
							else
							{
									enrolledTraining.Due_Date__c = training.End_Date__c;
							}
						}
						 
					}
					
				}
				else
				{
				
					enrolledTraining.Due_Date__c =Date.today().addDays(Integer.valueOF(training.Criterion__c));
				}
    return enrolledTraining;
    }
    //go back to home page
    public PageReference cancel() {        
        PageReference cancle= null;
        cancle= Page.glsLmsHome; // use the name of the second VF page
        cancle.setRedirect(true);
        return cancle;  
    }
    
    
    
}