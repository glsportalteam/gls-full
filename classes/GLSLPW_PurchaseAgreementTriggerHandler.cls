public with sharing class GLSLPW_PurchaseAgreementTriggerHandler {
    public static boolean skipPATrigger = false;
    
    /**
    * Update the Protcol status to 
      1. 'Open'when new PA created
      2. 'Pending' when PA is deleted
    */
    public static void updateProtocol(set<String> setProtocolId, Boolean isDel){   
        list<Purchase_Agreement__c> lstPA = new list<Purchase_Agreement__c>();
        List<Protocol__c> lstProtocol = new List<Protocol__c>();
        if(setProtocolId != null && setProtocolId.size()>0)
            lstPA = [Select Id From Purchase_Agreement__c Where Protocol_Id__c in: setProtocolId] ;
            
            system.debug('---lstPA---: '+lstPA +'---'+ lstPA.size());
            if(lstPA.size()==0){
                for(String ps : setProtocolId){
                    Protocol__c protObj = new Protocol__c(Id = ps );
                    if(!isDel)
                        protObj.Status__c = GLSconfig.Open;
                    else
                        protObj.Status__c = Glsconfig.Pending;
                    lstProtocol.add(protObj);
                }
            }
          
            if(lstProtocol != null && lstProtocol.size()>0){
                try{
                    skipPATrigger = true;
                    update lstProtocol;
                }catch(DMLException de){
                    system.debug(GLSConfig.DMLExceptionoccured + de);
                }
            } 
            system.debug('---skipPATrigger---: '+skipPATrigger);  
    }
}