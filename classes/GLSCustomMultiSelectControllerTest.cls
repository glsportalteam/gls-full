/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *  
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSCustomMultiSelectControllerTest {
 
    static testMethod void myUnitTest() {
        test.startTest();
        List<Selectoption> lstLeftSelOpt = new List<Selectoption>();
        List<Selectoption> lstRightSelOpt = new List<Selectoption>();
        String hiddenLeft ='test';
         String hiddenRight ='test';
        lstLeftSelOpt.add(new Selectoption('test1','test1'));
        lstLeftSelOpt.add(new Selectoption('test2','test2')); 
        lstRightSelOpt.add(new Selectoption('test3','test3'));
        lstRightSelOpt.add(new Selectoption('test4','test4'));
        Selectoption[] newSelOpt = new Selectoption[]{};
        GLSCustomMultiSelectController obj = new GLSCustomMultiSelectController();
        obj.leftOptions = lstLeftSelOpt;
        obj.rightOptions = lstRightSelOpt;
        obj.leftOptionsHidden = hiddenLeft;
        obj.rightOptionsHidden = hiddenRight;
        lstLeftSelOpt = obj.leftOptions;
        lstRightSelOpt = obj.rightOptions;
        obj.leftOptions.add(new Selectoption('test5','test5'));
        //obj.setOptions(newSelOpt,'test1 & test2');
        test.stopTest();
        
    }
}