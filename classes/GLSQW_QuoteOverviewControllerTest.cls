/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSQW_QuoteOverviewControllerTest {

    static testMethod void myUnitTest1() {
        GLSSetupData.createQLI();
        String currentSPs = 'Sample Assignmnet SP,'+'\n'+'Sample Assignment SP 2';
        String currentSP = 'Sample Assignmnet SP,'+'\n'+'Sample Assignment SP 2';
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('assgDetailsText','This is sample text area');
            ApexPages.currentPage().getParameters().put('currentSPs',currentSPs);
            ApexPages.currentPage().getParameters().put('currentSP',currentSP);
            ApexPages.currentPage().getParameters().put('assgNumber','1');
            ApexPages.currentPage().getParameters().put('emailAction','BDM');
            ApexPages.currentPage().getParameters().put('checkBoxValue','true');
            GLSQW_QuoteOverviewController obj = new GLSQW_QuoteOverviewController(new ApexPages.StandardController(GLSSetupData.quote));
            obj.getItems();
            obj.updateSpecialProvisionsMethod();
            obj.updateSpecialProvisionsMethodQuote(); 
            obj.updateAssgSpecialProvisionsMethod();
            obj.updateAssgSpecialProvisionsMethodQuote();
            obj.getAllSpecialProvision();
            obj.getAllSpecialProvisionQuote(); 
            obj.sendEmailMethod();
        test.stopTest();
    }
  /*  static testMethod void myUnitTest2() {
        GLSSetupData.createQLI();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote1.id);
            ApexPages.currentPage().getParameters().put('emailAction','Client');
            GLSQW_QuoteOverviewController obj = new GLSQW_QuoteOverviewController(new ApexPages.StandardController(GLSSetupData.quote1));
            obj.sendEmailMethod();
        test.stopTest();
    }*/
    static testMethod void myUnitTest3() {
        GLSSetupData.createQLI();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote1.id);
            ApexPages.currentPage().getParameters().put('emailAction','File Prep');
            GLSQW_QuoteOverviewController obj = new GLSQW_QuoteOverviewController(new ApexPages.StandardController(GLSSetupData.quote1));
            obj.sendEmailMethod();
        test.stopTest();
    }
    static testMethod void myUnitTest4() {
        GLSSetupData.createQLI();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('emailAction','Scope Review');
            GLSQW_QuoteOverviewController obj = new GLSQW_QuoteOverviewController(new ApexPages.StandardController(GLSSetupData.quote));
            obj.sendEmailMethod();
        test.stopTest();
    }
}