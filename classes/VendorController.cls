public with sharing class VendorController 
{

    public vendor__c vendor
    {
        get;set;
    }
    
    public string ProfileURL{get;set;}
    public string QueryType {get; set;}
    public Integer TotalResults{get;set;}
    public Integer TotalPages { get; set; }      
    public List<string> alphabet{get;set;}     
    public double RatePerWordMin{get;set;}
    public double RatePerWordMax{get;set;}
    public double RatePerEditMin{get;set;}
    public double RatePerEditMax{get;set;}
    public Id VendorId{get;set;}
    public Integer SelectedId{get;set;}
    public Set<Id> resultIds{get;set;}
    public List<Id> lstResultId{get;set;}
    public boolean IsNext{get;set;}
    public Map<ID,Vendor__c> mapVendors{get;set;}
    public boolean IsPrevious{get;set;}
     // the soql without the order and limit
      private String soql {get;set;}
      // the collection of contacts to display
      public List<Vendor__c> vendors {get;set;}
    
    // init the controller and display some sample data when the page loads
    public VendorController()
    {
        IsNext = false;
        IsPrevious = true;
        vendor = new vendor__c();
        lstResultId = new List<Id>();
        VendorId = null;
        alphabet = new string[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'All'};
        soql = 'select Id, Name, First_Name_Vendor__c, Last_Name_Vendor__c, Agency_Name__c, Native_Language__c, All_Languages__c,';
        soql = soql + ' Translation_rates_per_word__c, TEP_Rates__c, Editing_Rates_per_word__c, Hourly_rate__c, ';
        soql = soql + 'Minimum_Charge__c, Country__c, Preferred_Vendors__c  from vendor__c where vendor__c.name != null';
        SelectedId = 0;
        SetTotalPageCount();
        //runQuery();
    }
    
    public ApexPages.StandardSetController standardSetCon 
    {
        get {
            if(standardSetCon == null) {
                standardSetCon = new ApexPages.StandardSetController(Database.getQueryLocator(soql));
                // sets the number of records in each page set
                AddIdToList();
                SetTotalPageCount();
                standardSetCon.setPageSize(10);
            }
            return standardSetCon ;
        }
        set;
    }
    
    public void SetTotalPageCount()
    {
        TotalResults = standardSetCon.getResultSize();
        system.debug('Results:' + TotalResults);
        Decimal dtotalPages = (standardSetCon.getResultSize() / standardSetCon.getPageSize());
        dtotalPages = Math.floor(dtotalPages) + ((Math.mod(standardSetCon.getResultSize(), 10)>0) ? 1 : 0);
        TotalPages = Integer.valueOf(dtotalPages);
        system.debug('dtotalPages:' +dtotalPages );
    }
    
    public void AddIdToList()
    {
        vendors = (List<Vendor__c>)standardSetCon.getRecords();
        mapVendors = new Map<ID,Vendor__c>(vendors);
        resultIds = (new Map<Id,Vendor__c>(vendors)).keySet();
        lstResultId.clear();
        lstResultId.addall(resultIds);
    }
    
    
    
    public List<Vendor__c> getCurrentList() 
    {
       //vendors = (List<Vendor__c>)standardSetCon.getRecords();
       //resultIds = (new Map<Id,Vendor__c>(vendors)).keySet();
       AddIdToList();
       return vendors;
    } 
    
    public PageReference NextProfile()
    {
        try
        {
            IsPrevious = false;
            system.debug('Size:' + lstResultId.size());      
            if(selectedId < lstResultId.size())
            {
                VendorId = lstResultId[SelectedId];
                Vendor__c selectedVendor = mapVendors.get(VendorId);
                ProfileURL = '/apex/vendorprofile?id=' + VendorId;
                if(selectedId == (lstResultId.size() - 1))
                {
                    SelectedId -= 1;
                    IsNext = true;
                }  
                SelectedId += 1;
            }
            else
            {
                IsNext = true;
            }   
        } 
        catch(Exception ex)
        {}    
        
        return null;
    }
    
    public PageReference PreviousProfile()
    {
        try
        {
            IsNext = false;
            SelectedId = SelectedId - 1;
            if(selectedId == 0)
            {            
                IsPrevious = true;
            }
            
            VendorId = lstResultId[SelectedId];
            Vendor__c selectedVendor = mapVendors.get(VendorId);
            ProfileURL = '/apex/vendorprofile?id=' + VendorId;    
        }
        catch(exception ex)
        {}        
        return null;
    }
 
  // the current sort direction. defaults to asc
  public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'desc'; } return sortDir;  }
    set;
  }
 
  // the current field to sort by. defaults to First name
  public String sortField {
    get  { if (sortField == null) {sortField = 'First_Name_Vendor__c'; } return sortField;  }
    set;
  }
 
  // format the soql for display on the visualforce page
  public String debugSoql {
    get { return soql; }
    set;
  }
  
  // toggles the sorting of query from asc<-->desc
  public void toggleSort() {
    // simply toggle the direction
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
    //sortDir = 'asc';
    // run the query again
    //QueryType = 'Sort';
    runQuery();    
  }
 
  // runs the actual query
  public void runQuery() 
  {
 
    try 
    {
        if(QueryType == 'Sort')
        {
            soql = soql + ' order by ' + sortField + ' ' + sortDir;
        }
        else
        {
            //vendors = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 500');
            soql = getQuery() + ' order by ' + sortField + ' ' + sortDir;
            system.debug(soql);
        }
      
      
      standardSetCon = new ApexPages.StandardSetController(Database.getQueryLocator(soql));
      standardSetCon.setPageSize(10);
      
      getCurrentList();
      SetTotalPageCount();
      
    } catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error occured in query!'));
    }
 
  }
 
  // runs the search with parameters passed via Javascript
  public PageReference runSearch() 
  {
      ProfileURL = NULL;
      SelectedId = 0;
      VendorId = null;
      runQuery();
 
      return null;
  } 
  
  public string getQuery()
  {
    String firstName = vendor.First_Name_Vendor__c;
    String lastName = vendor.Last_Name_Vendor__c;
    String agency = vendor.Agency_Name__c;
    String nativeLang = vendor.Native_Language__c;
         
    soql = 'select Id, Name, First_Name_Vendor__c, Last_Name_Vendor__c, Agency_Name__c, Native_Language__c, All_Languages__c,';
    soql = soql + ' Translation_rates_per_word__c, TEP_Rates__c, Editing_Rates_per_word__c, Hourly_rate__c, ';
    soql = soql + 'Minimum_Charge__c, Country__c, Preferred_Vendors__c  from vendor__c where vendor__c.name != null';
    if (!vendor.First_Name_Vendor__c.equals(''))
      soql += ' and First_Name_Vendor__c LIKE \''+String.escapeSingleQuotes(firstName)+'%\'';
    if (!vendor.Last_Name_Vendor__c.equals(''))
      soql += ' and Last_Name_Vendor__c LIKE \'%'+String.escapeSingleQuotes(lastName)+'%\'';
    if (!vendor.Agency_Name__c.equals(''))
      soql += ' and Agency_Name__c LIKE \'%'+String.escapeSingleQuotes(agency)+'%\'';  
    if (!vendor.Native_Language__c.equals(''))
      soql += ' and Native_Language__c includes (\''+nativeLang+'\')';
    if (!vendor.All_Languages__c.equals(''))
      soql += ' and All_Languages__c includes (\''+vendor.All_Languages__c+'\')';
    if (!vendor.CAT_Tools__c.equals(''))
      soql += ' and CAT_Tools__c LIKE \'%'+String.escapeSingleQuotes(vendor.CAT_Tools__c)+'%\'';  
    if (!vendor.Expertise__c.equals(''))
      soql += ' and Expertise__c LIKE \'%'+String.escapeSingleQuotes(vendor.Expertise__c)+'%\'';
    if (vendor.Preferred_Vendors__c != false)
      soql += ' and Preferred_Vendors__c = ' + vendor.Preferred_Vendors__c; 
    if (RatePerWordMin != null)
    {
      soql += ' and ((Translation_rates_per_word__c >= ' + RatePerWordMin + ' and Translation_rates_per_word__c <= ' + RatePerWordMax + ')'; 
      soql += ' or (TEP_Rates__c >= ' + RatePerWordMin + ' and TEP_Rates__c <= ' + RatePerWordMax + ') or (Translation_rates_per_word__c = null and TEP_Rates__c = null))';
    }
    if (RatePerEditMin != null)
      soql += ' and ((Editing_Rates_per_word__c >= ' + RatePerEditMin + ' and Editing_Rates_per_word__c <= ' + RatePerEditMax + ') or Editing_Rates_per_word__c = null)'; 
      
      return soql;
  }
  
  /*public PageReference refreshList2() {       
       standardSetCon = null;     
       string s;
       if(apexpages.currentpage().getparameters().get('alpha') == 'All')
           s='%';
       else
           s= apexpages.currentpage().getparameters().get('alpha')+'%';
       
       soql = getQuery() + ' and First_Name_Vendor__c like' +'\''+s +'\'';
       
       getCurrentList();       
        return null;
    }*/
    
}