/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the  organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest

private class GLSDeleteQuoteFilesBatchClassTest {
    
     static testMethod void myUnitTest1() {
        Test.StartTest();
        Quotes_File__c qf = new Quotes_File__c();
        qf.File_Name__c ='Testfile.txt';
        qf.Amazon_S3_source_file_path__c = 'Companyname/Testfile.txt';
        insert qf;
        GLSDeleteQuoteFilesBatchClass d = new GLSDeleteQuoteFilesBatchClass();
        d.isTestRun = true;
        database.executeBatch(d);
        Test.StopTest();
       
    }
    
     static testMethod void myUnitTest2() {
        Test.StartTest();
        Quotes_File__c qf = new Quotes_File__c();
        qf.File_Name__c ='Testfile.txt';
        qf.Amazon_S3_source_file_path__c = 'Companyname/Testfile.txt';
        insert qf;
        string query = 'Select Id,Amazon_S3_source_file_path__c, File_Name__c,Quote__r.Name,Quote__r.Quote_Number__c, Quote__r.Client__r.Name,Quote__r.Id,Bucket_Name__c from Quotes_File__c  limit 1';        
        GLSDeleteQuoteFilesBatchClass d = new GLSDeleteQuoteFilesBatchClass(query);
        d.isTestRun = true;
        database.executeBatch(d);
        Test.StopTest();
       
    }
   

}