public with sharing class GLSQW_UploadLogFilesController {
    
    public transient Quote_Line_Item__c qliUpdate     {get;set;}
    public transient string qliID                     {get;set;}
    public String quoteId_Value             {get;set;}
    public String Quote_Name             {get;set;}
    public String Account_Name             {get;set;}
    public S3.AmazonS3 as3                  {get; private set;}
    public string userType                  {get;set;}
    public string newFileName               {get;set;}
    public string AmazonPath                {get;set;} 
    public string path                {get;set;} 
    public string bucket                    {get;set;}
    public AWSKeys credentials              {get;set;}
    public String AWSCredentialName = 'NAME OF KEY TO USE'; //Modify this string variable to be the name of the AWS Credential record that contains the proper AWS keys and secret
    public String secret            {get{return credentials.secret;}}       
    public string key; /*{get{return credentials.key;}}*/                      
    public List<File_Format__c> ext;
    public string currentSignature          {get;set;}
    public Quote__c currentQuote            {get;set;}
    public String ParseResult ='';
    public String xmlDataToParse ='';
    GLSXMLParser glsxmlParser = new GLSXMLParser();
    GLSCSVParser glsCSVParser = new GLSCSVParser();
    public Boolean disableBtn {get; set;}
    
    /*public String getsecret(){
        if(credentials != null){
            return credentials.secret;
        }else{
            return '';
        }
    }*/
    
    public String getkey(){
        if(credentials != null){
            return credentials.key;
        }else{
            return '';
        }
    }
    
    Datetime expire = system.now().addDays(1);
    String formattedexpire = expire.formatGmt('yyyy-MM-dd')+'T'+expire.formatGmt('HH:mm:ss')+'.'+expire.formatGMT('SSS')+'Z';                    
    String policy { get {return 
            '{ "expiration": "'+formattedexpire+'","conditions": [ {"bucket": "'+bucket +'" } ,{ "acl": "'+'public-read'+'" },'+'{"success_action_status": "201" },'+
            '["starts-with", "$key", ""] ]}';       } }
       
    public GLSQW_UploadLogFilesController(ApexPages.StandardController controller) {
        qliID = ApexPages.currentPage().getParameters().get('id');
        
        system.debug('----qliID----'+qliID);
        disableBtn = true;
        if(qliID.length() != 15)
           qliID = qliID.subString(0,15);
        try{
            List<Quote_Line_Item__c> lstQLI = [Select of_Pages__c, isTMFound__c, isReadyForAnalyse__c, isProcessed__c, isAnalyzed__c, XML_Data__c, X95_99_Word_Count__c, 
                         X85_95_Word_Count__c, X75_84_Word_Count__c, X50_74_Word_Count__c, X100_Word_Count__c, Unit_of_Measure__c, Translation_Memory_Options_Flag__c,
                         Trados_Language_Pair__c, Total_Amount__c, Task_Status__c, Target_Language_ID__c, TM_Export_Zip_File__c, TM_Export_Transaction_ID__c, 
                         TM_Export_File_ID__c, Subline_Total_Amount__c, Sub_Total_Amount__c, Source_Language_ID__c, Repetitions_Word_Count__c, Rate__c, Rate_Type__c, 
                         Quote_Line_Item_Id__c, QuoteID__c, Quantity__c, Perfect_Match__c, No_Match_Word_Count__c, Name, Min_Charge_Applied__c, Line_Item_Duration__c,
                         Id, File_Prep_Format_Surcharge_Percentage__c, File_Prep_Format_Surcharge_Amount__c, File_Prep_Description__c, 
                         File_Prep_Amount__c, Extra_Days__c, Error_Message__c, Discounted_Percentage__c, DTP_Hours__c, Crossfile_Repetitions__c, 
                         Context_TM_Word_Count__c, Client_TM__c, Analyzed_Version_ID__c, Analyzed_Transaction_ID__c, Analyzed_File_URL__c, Additional_Line_Items__c,
                         Add_Outsourced_Services__c, Add_Discount__c, Actual_Words__c From Quote_Line_Item__c Where Id =: qliID ];
            
           if(lstQLI != null && lstQLI.size() > 0){
                qliUpdate = lstQLI[0];
                getawss3LoginCredentials();
                String quoteId = qliUpdate.QuoteID__c;
                //quoteId_Value = quoteId.substring(0,15);
                quoteId_Value = quoteId;
                currentQuote = [Select Id,Bucket_Name__c,Quote_Number__c, Client__r.Name, Name, Status__c From Quote__c Where Id =: quoteId limit 1];
                Quote_Name = currentQuote.Quote_Number__c;
                bucket = currentQuote.Bucket_Name__c;
                User u = [SELECT Id,UserType,Contact.Account.Name  FROM User WHERE Id = :userInfo.getUserId()limit 1];
                userType = u.UserType;
                if(u.UserType=='Standard'){
                  Account_Name=currentQuote.Client__r.Name;
               }else if(u.UserType=='PowerPartner'){
                  Account_Name=u.Contact.Account.Name;
               }
               
            }
            if(currentQuote != null && GLSConfig.btnEnableStatus.contains(currentQuote.Status__c)){
                disableBtn = false;
            }
        }catch(QueryException qe){
            System.debug('Exception in Query: '+qe.getMessage());
        }
    }
    
    /**
    *   @Description : This method sets Aws s3 credentials
    *   @Param : none
    **/       
    public PageReference getawss3LoginCredentials(){  
        try{
            credentials = new AWSKeys(AWSCredentialName);
            as3 = new S3.AmazonS3(credentials.key,credentials.secret);              
        }catch(AWSKeys.AWSKeysException AWSEx){
            System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
            ApexPages.addMessage(errorMsg);
        } 
       return null;
    }
    
    /**
    *   @Description : This method returns s3 policies
    *   @Param : none
    **/

    public String getPolicy() {
        return EncodingUtil.base64Encode(Blob.valueOf(policy));
    }

   /**
    *   @Description : This method returns signature
    *   @Param : none
    **/

    public String getSignedPolicy() {    
        return make_sig(EncodingUtil.base64Encode(Blob.valueOf(policy)));        
    }

   /**
    *   @Description : This method returns encoded policy 
    *   @Param : none
    **/

    public String getHexPolicy() {
        String p = getPolicy();
        return EncodingUtil.convertToHex(Blob.valueOf(p));
    }

   /**
    *   @Description : This method returns encrypted signature.
    *   @Param : String Canonical Buffer
    **/

    private String make_sig(string canonicalBuffer) {        
        String macUrl = '';
        String signingKey;
        Blob mac;
        //secret = credentials.secret;
        //key = credentials.key;
       if(!Test.isRunningTest()){
            if(secret != null && secret != ''){
                signingKey = EncodingUtil.base64Encode(Blob.valueOf(secret));
                mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof(Secret));
                macUrl = EncodingUtil.base64Encode(mac); 
            } 
        }
        return macUrl;
    } 
           /**
        *   @Description : This method saves Quote File Record
        *   @Param : none
        **/
          
        public void successFileupload() {
            String[] filelines = new String[]{};//Added by Amit
            qliID = ApexPages.currentPage().getParameters().get('id');
            Account_Name = EncodingUtil.urlEncode(Account_Name, 'UTF-8');
            path = Account_Name+path;
            System.debug('==='+path);
            if(qliID != null){     
                if(qliID.length()!=15)
                     qliID = qliID.subString(0,15);
                getawss3LoginCredentials();
                String signed = getSignedPolicy();
                String codedsigned = EncodingUtil.urlEncode(signed,'UTF-8'); 
                Long Lexpires = expire.getTime()/1000;
                String analysedURL ='';
                analysedURL = downloadQuoteFile(qliId,newFileName);
                system.debug('--analysedURL----: '+analysedURL);
                if(analysedURL != ''){
                    Httpresponse resp = GLSQLICreator.getResponse(analysedURL);
                    xmlDataToParse = resp.getBody();
                }
                 
                if(xmlDataToParse != ''){
                String [] fileNameArr;
                fileNameArr = newFileName.trim().split('\\.');
                String fileExt;
                fileExt = fileNameArr[1];
                System.debug('==='+newFileName+'==='+fileNameArr);
                    if(fileExt=='csv')
                    {
                            system.debug('---csvDataToParse---:' +xmlDataToParse);
                            try{
                                String analyzedFor = 'analyzedSingleQLI';
                                ParseResult = glsCSVParser.parser(qliID,quoteId_Value,analysedURL,'','Success', xmlDataToParse, analyzedFor,false);
                                system.debug('----ParseResult-----: '+ParseResult);
                            }catch(DMLException de){
                                System.debug(GLSConfig.DMLExceptionOccured+'---'+de.getMessage());
                            }catch(Exception e){
                                System.debug(GLSConfig.ExceptionOccured+'---'+e.getMessage());
                            }
                       
                    }
                    else
                    {
                        System.debug('===XMl Region');
                        system.debug('---xmlDataToParse---:' +xmlDataToParse);
                        try{
                            String analyzedFor = 'analyzedSingleQLI';
                            ParseResult = glsxmlParser.parser(qliID,quoteId_Value,analysedURL,'','Success', xmlDataToParse, analyzedFor);
                            system.debug('----ParseResult-----: '+ParseResult);
                        }catch(DMLException de){
                            System.debug(GLSConfig.DMLExceptionOccured+'---'+de.getMessage());
                        }catch(Exception e){
                            System.debug(GLSConfig.ExceptionOccured+'---'+e.getMessage());
                        }
                    }
                }
               
            }
        }
        private string downloadQuoteFile(string qliID,string fName){
            getawss3LoginCredentials();
            String filename = EncodingUtil.urlEncode(fName,'UTF-8');
            system.debug('--filename----: '+filename);
            Datetime now = DateTime.now();
            Datetime expireson = now.AddSeconds(120);
            Long Lexpires = expireson.getTime()/1000;
            String stringtosign = 'GET\n\n\n'+Lexpires+'\n/'+GLSConfig.bucketName+'/'+filename;
            String signed = make_sig(stringtosign);
            String codedsigned = EncodingUtil.urlEncode(signed,'UTF-8');
            String url = '';
            if(!Test.isRunningTest()){
                 url = 'https://'+GLSConfig.bucketName+'.s3.amazonaws.com/'+path+filename+'?AWSAccessKeyId='+as3.key+'&Expires='+Lexpires;
            }else url = 'testurl';
           system.debug('--url----: '+url);
           return url;
           
        }
}