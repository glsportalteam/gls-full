public  class GLSQW_RateSheetCloneController {
 public Rate_Sheet__c ratesheet {get;set;}
 public string ratesheetname {get;set;}
 public list<Rate_Sheet__c> insertratesheet = new list<Rate_Sheet__c>();
 public string currentid {get;set;}
 public list<Rate_Sheet_Item__c> ratesheetitem = new list <Rate_Sheet_Item__c>();
 public list<Rate_Sheet_Item__c> insertratesheetitem = new list <Rate_Sheet_Item__c>();
 public list<Rate_Sheet_Item__c> queryratesheetitem = new list <Rate_Sheet_Item__c>();
 public list<Rate_Sheet__c> Listratesheet= new list<Rate_Sheet__c>();
 public set<id> ratesheetitemid = new set<id>();
 public list<Rate_Sheet_Subitem__c> Listratesheetsubitm= new list<Rate_Sheet_Subitem__c>();
 public list<Rate_Sheet_Subitem__c> updateListratesheetsubitm= new list<Rate_Sheet_Subitem__c>();
 public map<id,id> storeids= new map<id,id>();
 public set<string> rateSheetNames= new set<string>();
    public GLSQW_RateSheetCloneController(ApexPages.StandardController con) {
        currentid=ApexPages.CurrentPage().getparameters().get('id');
        Rate_Sheet__c temp =[select id,Name,ownerid from  Rate_Sheet__c where id =: currentid ];
        if(temp !=null){
        //ratesheetname=temp.Name;
        ratesheet=temp;
        }
      }
    /* This method is used to
      clone th data from ratesheet with ratesheet item
      ratesheet sub item ,tados catagories( which is updated through trigger)
     */
    public pagereference Clonedata(){
        
        Listratesheet=[select id,Name from  Rate_Sheet__c where id =: currentid ];
        for(Rate_Sheet__c  rs: [select id,Name from  Rate_Sheet__c limit 1000 ]){
            
            rateSheetNames.add(rs.name);
        }
        system.debug('line34'+rateSheetNames);
            if(Listratesheet != null && Listratesheet.size()>0){
                if(rateSheetNames != null && rateSheetNames.size()>0){
                     if(rateSheetNames.contains(ratesheetname)==true ){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Duplicate Rate Sheet name'));
                    return null;
                }   
                    
                
               
                else{
                    ratesheetitem = [select id,Hourly_Rate__c,Language__c,Min_Amount__c,Min_Amount_Description__c,Rate_Sheet__c,
                                    Service__c ,Source_Language_Code__c,Target_Language_Code__c,Unit_of_Measure__c from Rate_Sheet_Item__c
                                    where Rate_Sheet__c = : Listratesheet[0].id];
                                    system.debug('line7'+currentid +'data'+ ratesheetname );
                                    for (Rate_Sheet__c rs :Listratesheet){
                                        Rate_Sheet__c rtsheet = new Rate_Sheet__c();
                                        if(ratesheetname != null){
                                        rtsheet.Name= ratesheetname;
                                        }
                                        insertratesheet.add(rtsheet);
                                    }
                }
            }   
        }
     if(insertratesheet != null && insertratesheet.size()>0){
                try{
                insert insertratesheet;
                }
                catch (Exception e){
                    system.debug('------Exception----'+ e.getmessage());    
                }
            }   
            if(ratesheetitem !=null && ratesheetitem.size()>0){
                for(Rate_Sheet_Item__c rsi : ratesheetitem){
                    ratesheetitemid.add(rsi.id);
                    Rate_Sheet_Item__c clonersi = new Rate_Sheet_Item__c();
                    
                    if(rsi.Hourly_Rate__c !=null){
                        clonersi.Hourly_Rate__c=rsi.Hourly_Rate__c;
                    }
                    if(rsi.Language__c !=null){
                        clonersi.Language__c = rsi.Language__c;
                        
                    }
                    if(rsi.Min_Amount__c !=null){
                        clonersi.Min_Amount__c = rsi.Min_Amount__c;
                        
                    }
                    if(rsi.Min_Amount_Description__c !=null){
                        clonersi.Min_Amount_Description__c = rsi.Min_Amount_Description__c;
                        
                    }
                    if(rsi.Service__c !=null){
                        clonersi.Service__c = rsi.Service__c;
                        
                    }
                    if(rsi.Source_Language_Code__c !=null){
                        clonersi.Source_Language_Code__c = rsi.Source_Language_Code__c;
                        
                    }
                        if(rsi.Target_Language_Code__c !=null){
                        clonersi.Target_Language_Code__c = rsi.Target_Language_Code__c;
                        
                    }
                        if(rsi.Unit_of_Measure__c !=null){
                        clonersi.Unit_of_Measure__c = rsi.Unit_of_Measure__c;
                        
                    }
                    clonersi.Rate_Sheet__c = insertratesheet[0].id; 
                    clonersi.Temp_Ratesheetid__c=rsi.id;
                    insertratesheetitem.add(clonersi);
                        
                }
            }
            if(insertratesheetitem !=null && insertratesheetitem.size()>0){
                try{
                    insert insertratesheetitem;
                }
                catch (Exception e){
                    System.debug('------- RatesheetitemException-----'+ e.getmessage());
                    }
            }
            if(ratesheetitemid !=null && ratesheetitemid.size()>0){
                
                Listratesheetsubitm=[select id,Name,Description__c,Rate__c,Rate_Sheet_Item__c,Trados_Categories__c from Rate_Sheet_Subitem__c where
                                     Rate_Sheet_Item__c in: ratesheetitemid ];
            }
            if(Listratesheetsubitm !=null && Listratesheetsubitm.size()>0){
            queryratesheetitem =[select id,Temp_Ratesheetid__c from Rate_Sheet_Item__c where Rate_Sheet__c =:insertratesheet[0].id];    
            if(queryratesheetitem !=null && queryratesheetitem.size()>0){
                 for(Rate_Sheet_Item__c rs : queryratesheetitem){
                 storeids.put(rs.Temp_Ratesheetid__c,rs.id);    
                 }  
             }
              for(Rate_Sheet_Subitem__c rssi :Listratesheetsubitm ){
                                
                            string  temp =  storeids.get(rssi.Rate_Sheet_Item__c);
                            Rate_Sheet_Subitem__c rsatesheetsubitem = new Rate_Sheet_Subitem__c();
                            if( temp != null){
                                if(rssi.Description__c !=null){
                                    rsatesheetsubitem.Description__c = rssi.Description__c;
                                    
                                }
                                if(rssi.Rate__c !=null){
                                    rsatesheetsubitem.Rate__c = rssi.Rate__c;
                                    
                                }
                                if(rssi.Trados_Categories__c !=null){
                                    rsatesheetsubitem.Trados_Categories__c= rssi.Trados_Categories__c;
                                    
                                }
                                rsatesheetsubitem.Rate_Sheet_Item__c=temp;
                                updateListratesheetsubitm.add(rsatesheetsubitem);
                            }
                    
               }    
                
            }
        
        if(updateListratesheetsubitm !=null && updateListratesheetsubitm.size()>0){
            try{
                insert updateListratesheetsubitm;
            }
            catch (Exception e){
                system.debug('--------Ratesheet subitme exception------'+ e.getmessage());
            }
        }
        PageReference newocp = new PageReference('/'+insertratesheet[0].id);
        newocp.setRedirect(true);
        return newocp;
        
     }
     
    public pagereference cancelclone(){
        
        PageReference newocp1 = new PageReference('/'+currentid);
        newocp1.setRedirect(true);
        return newocp1;
        
        
    }
}