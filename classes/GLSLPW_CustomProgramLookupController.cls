public with sharing class GLSLPW_CustomProgramLookupController { 
  public List<Program__c> results           {get;set;} // search results
  public string searchString{get;set;} // search keyword
 
  public GLSLPW_CustomProgramLookupController() {
    searchString = System.currentPageReference().getParameters().get('lksrch');
    runSearch();  
  }
 
  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }
 
  // prepare the query and issue the search command
  private void runSearch() {
    if(searchString != null)
        searchString = String.escapeSingleQuotes(searchString);
    results = performSearch(searchString);               
  } 
 
  // run the search and return the records found. 
  private List<Program__c> performSearch(string searchString) {          
  	String accountId = System.currentPageReference().getParameters().get('accId');
  	
    String soql = 'select id,name,Account__r.Name,Program_Name__c from Program__c';
    if(searchString != '' && searchString != null)
      soql = soql +  ' where name LIKE \'%' + searchString +'%\' and (Status__c=\'Open\')';
    else 
      soql = soql +  ' where name LIKE \'%\'and (Status__c=\'Open\')';  
    
    if(accountId != null && accountId != ''){
    	soql = soql +' AND Account__r.id ='+'\''+accountId+'\'';
    }
            
    soql = soql + ' order by Name Desc limit 250';
    System.debug(soql);
    return database.query(soql);  
  }
 
  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
 
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
}