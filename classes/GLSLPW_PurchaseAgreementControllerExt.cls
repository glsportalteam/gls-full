/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSLPW_PurchaseAgreementControllerExt Class
* Function: This class is used as a controller extension for the GLSLPW_PurchaseAgreementPage
*/


public class GLSLPW_PurchaseAgreementControllerExt {
      
      //Variable declaration for the class
      public Id paId                                            {get; set;}
      public Id pafileId                                        {get; set;}
      public Purchase_Agreement__c paObj                        {get; set;}
      public Boolean isRendered                                 {get; set;}
      public Protocol__c protObj                                {get; set;}
      public List<Purchase_Agreement_File__c> lstPAFiles        {get; set;}
      public Boolean enableDelete                               {get; set;}
      public ApexPages.StandardController controller                      {get;set;} 
     //Constructor
     public GLSLPW_PurchaseAgreementControllerExt(ApexPages.StandardController stdcon){
                    
                 controller = stdcon;
                 //Initialization
                 paObj                  = new Purchase_Agreement__c();
                 protObj                = new Protocol__c();
                 lstPAFiles             = new List<Purchase_Agreement_File__c>();
                 paObj.Protocol_Id__c   = ApexPages.currentPage().getParameters().get('prtId');
                 paId                   = ApexPages.currentPage().getParameters().get('paId');
                 protObj                = getProtocol(paObj.Protocol_Id__c);
                 if(paId != null){
                    paObj = getPurchaseAgreement(paId);
                    isRendered = true;
                    getPAFileRecord();
                 }
                 else{
                    isRendered = false;
                 }
                if(Schema.sObjectType.Purchase_Agreement_File__c.isDeletable()){
                    enableDelete = true;
                }
                else{
                    enableDelete = false;
                }
    }
      
    /* 
    @Description: To get the complete record of Protocol
    @Params: Protocol Id
    @Return Type: Protocol__c
    */
     private Protocol__c getProtocol(Id protId){
        protObj =  [SELECT Name,status__c
                        FROM Protocol__c
                        WHERE Id =: paObj.Protocol_Id__c LIMIT 1];
        return protObj;
    }
    
    /* 
    @Description: To get the complete record of Purchase Agreement
    @Params: Purchase Agreement Id
    @Return Type: Purchase_Agreement__c
    */
    private Purchase_Agreement__c getPurchaseAgreement(Id paId){
         paObj = [ SELECT PA_Number__c, Write_Off__c, Total_Amount__c, Status__c, Remaining_Balance__c, Protocol_Id__c, Name,  
                                            Invoiced_Amount__c, Id, Expiration_Date__c, Comment__c,Issue_Date__c,Protocol_Id__r.Status__c,Protocol_Id__r.Name 
                                            FROM Purchase_Agreement__c
                                            WHERE Id =: paId
                                            ORDER BY CreatedDate DESC];
        return paObj;
     }
     
     /* 
    @Description: Save method to insert the Purchase Agreement record.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference savePARecord(){
        PageReference pRef = null;
        if(paId == null){
                 if(paObj.Issue_Date__c>paObj.Expiration_Date__c){
                    ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Issue Date Can not be after Expiration Date');
                    ApexPages.addMessage(errorMsg);
                }
                else{
                    try{    
                        upsert paObj;
                     }catch(DMLException de){
                        system.debug(GLSConfig.DMLExceptionOccured + de);
                    }
                    catch(Exception e){
                        system.debug(GLSConfig.ExceptionOccured + e);
                    }
                     isRendered =true; 
                     pRef = new PageReference('/apex/GLSLPW_PurchaseAgreementPage?paId='+paObj.Id+'&prtId='+paObj.Protocol_Id__c);
                     pRef.setRedirect(true);
                }
            }
            else{                
                 if(paObj.Issue_Date__c>paObj.Expiration_Date__c){
                    ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Issue Date Can not be after Expiration Date');
                    ApexPages.addMessage(errorMsg);
                }
                else{
                    update paObj;
                    isRendered = true;
                    pRef = new PageReference('/apex/GLSLPW_PurchaseAgreementPage?paId='+paObj.Id+'&prtId='+paObj.Protocol_Id__c);
                    pRef.setRedirect(true);
                }
            }
        return pRef;
     }
     
    public pageReference cancel(){
        PageReference pRef = null;
        if(paId==null){
            pRef =  new PageReference('/apex/GLSLPW_ProtocolDetailPage?Id='+protObj.Id);
            pRef.setRedirect(true);             
        }else{          
            pRef = new PageReference('/apex/GLSLPW_PurchaseAgreementPage?prtId='+paObj.Protocol_Id__c+'&paId='+paId);
            pRef.setRedirect(true);             
        }                   
        return pRef;
    }
     
    /* 
    @Description: To get the list of Purchase Agreement files
    @Params: None
    @Return Type: None
    */
    public void getPAFileRecord(){
            lstPAFiles = [SELECT Name,PA_File_Name__c,Description__c 
                          FROM Purchase_Agreement_File__c 
                          WHERE Purchase_Agreement_Id__c = : paId];
    }
    
    /* 
    @Description: Method to edit the Purchase Agreement record.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference customEdit(){
        isRendered = false;
        return null;        
    }
    
    /* 
    @Description: Downloads the Purchase Agreement File from S3 server.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference downloadPAFile(){
        String stMessage = '';
        String statusMessage ='';
        String prefStr = '';
        PageReference pref = null;
        if(paId != null){
            Purchase_Agreement_File__c paFiles = [  SELECT Name,PA_File_Name__c,Description__c,AWSFilePath__c
                                                    FROM Purchase_Agreement_File__c
                                                    WHERE Purchase_Agreement_Id__c =: paId 
                                                    AND Id =: pafileId LIMIT 1];
            if(paFiles != null){
                String path = paFiles.AWSFilePath__c + paFiles.PA_File_Name__c;
                if(path != null){                                 
                    statusMessage = GLSAmazonUtility.downloadQuoteFile(path,GLSConfig.bucketName); 
                    try{
                        if(statusMessage != null){
                            List<String> urlParams = statusMessage.split('Signature=');
                            if(urlParams != null && urlParams.size() > 1){                          
                                String signature = EncodingUtil.urlDecode(urlParams[1],'UTF-8');   
                                String sigEncoded;
                                /*To overcome the error of "Signature Does Not Match" when the generated signature contains space which is in 
                                turn replaced by "+" */
                                if(signature.contains('+')){
                                    sigEncoded = signature.replace('+','%2B');
                                }    
                                else{
                                    sigEncoded = signature;
                                }    
                                prefStr = urlParams[0] + 'Signature=' + sigEncoded;
                            }
                        }                                                                                                                        
                    }
                    catch(Exception e){                    
                        stMessage = e.getMessage();                    
                    }
                }
                pref = new PageReference(prefStr);
            }
        }
        return pref;
    }
    
    /* 
    @Description: Deletes the Purchase Agreement File from S3 server.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference deletePAFile(){
        String stMessage = '';
        String statusMessage ='';
        if(paId != null){
             Purchase_Agreement_File__c paFiles = [ SELECT Name,PA_File_Name__c,Description__c,AWSFilePath__c
                                                    FROM Purchase_Agreement_File__c
                                                    WHERE Purchase_Agreement_Id__c =: paId 
                                                    AND Id =: pafileId LIMIT 1];
            if(paFiles != null){
                String path = paFiles.AWSFilePath__c + paFiles.PA_File_Name__c;
                try{
                    String bucketName = GLSConfig.bucketName;
                    StatusMessage = GLSAmazonUtility.deleteFilesFromAmazon(pafileId,path);
                    if(StatusMessage.equalsIgnoreCase('true')){
                        StMessage ='File Deleted Successfully';
                    }
                    else{
                        StMessage ='File not deleted';
                    }
                }
                catch(Exception e){
                    StMessage = e.getMessage();
                    system.debug(GLSConfig.ExceptionOccured);
                }
            }
        }
        ApexPages.Message statusMsg = new ApexPages.Message(ApexPages.Severity.INFO, StMessage);
        ApexPages.addMessage(statusMsg);
        getPAFileRecord();
        return null; 
    }
}
