global class LMS_SceduleEmailBatchClass implements Database.Batchable<SObject>, Database.AllowsCallouts{
     private String query;
     Private Date todaysDate;
    global LMS_SceduleEmailBatchClass(String query){
        todaysDate =Date.today();
         this.query = query;
    } 
    global Database.QueryLocator start(Database.BatchableContext bcStart) {       
        todaysDate =Date.today(); 
        return Database.getQueryLocator(query);
    } 
     global void execute(Database.BatchableContext bcExecute, List<User_Training__c> lstAsset){
         list<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
         list<Messaging.SingleEmailMessage> mailList1 = new List<Messaging.SingleEmailMessage>();
         list<User_Training__c> lstUserTrainingObj= new list<User_Training__c>();
         list<User_Training__c> onDueDateUserTrainingObj= new list<User_Training__c>();
         String subject;
        
         
         
         //List<User_Training__c> lstAsset=[select Id,Status__c,Training__r.Training_Name__c,Remainder_Sent_Date__c,Due_Date__c,Training__r.End_Date__c,Training__r.Training_Methodologies__c,Training__r.Start_Date__c,Training__r.Trainer_Name__c,Training__r.Name,Training__r.Reminder_Email_Days__c,User_Name__c from User_Training__c where Training__r.Active__c = true];
            for(User_Training__c a:lstAsset){
               
                    
              if( a.Status__c != 'Completed'){ 
              //a.Training__r.Duration__c != null &&
                if(a.Remainder_Sent_Date__c.month() == date.today().month() && a.Remainder_Sent_Date__c.year() == date.today().year()&& a.Remainder_Sent_Date__c.day() == date.today().day()){
                  lstUserTrainingObj.add(a);
                }
              }
              
              if(a.Due_Date__c != null && a.Status__c != 'Completed'){ 
                    if(a.Due_Date__c.month() == date.today().month() && a.Due_Date__c.year() == date.today().year()&& a.Due_Date__c.day() == date.today().day()){
                      onDueDateUserTrainingObj.add(a);
                }
              }
              
            }
             
           
        //Send a reminder mail before due date
        if(lstUserTrainingObj != null && lstUserTrainingObj.size()>0){
            for(User_Training__c a: lstUserTrainingObj){
                 String Trainer = '';
                    if(a.Training__r.Trainer_Name__c != null)
                        Trainer =a.Training__r.Trainer_Name__c;
                     String endDate = '';
                    if(a.Training__r.End_Date__c!= null)
                        endDate =String.ValueOF(a.Training__r.End_Date__c);
                    
                    system.debug('----endDate ----:' +endDate )    ;
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    subject = 'Reminder Mail';
                    mail.setTargetObjectId(a.User_Name__c);
                    mail.setSubject(subject);
                    mail.setHtmlBody('<p style="font-size:15px" face="verdana">Hi '+a.User_Name__r.name+',</br></br>This mail is to remind you that your: '+a.Training__r.Training_Name__c  +' is due in '+a.Training__r.Reminder_Email_Days__c+' day(s). </br>Following are the details of training:</br></br>Date: '+a.Training__r.Start_Date__c.format()+' - '+endDate+'</br>Training Name: '+a.Training__r.Training_Name__c +'</br>Trainer Name: '+Trainer +'</br>Training Methodologies: '+a.Training__r.Training_Methodologies__c +'</br>Due Date: '+a.Due_Date__c.format() +'</br></br>Sincerely,</br>Your LMS Team </br></br><img src="https://c.ap1.content.force.com/servlet/servlet.ImageServer?id=01590000003uX86&oid=00D90000000uV70&lastMod=1402396559000" width="120" height="50"></p>');
                    mail.setSaveAsActivity(false);    
                    mailList.add(mail);
                } 
        }     
        
         // send a reminder mail on due date     
         if(onDueDateUserTrainingObj != null && onDueDateUserTrainingObj.size()>0){  
               for(User_Training__c a: onDueDateUserTrainingObj){
               String Trainer = '';
                    if(a.Training__r.Trainer_Name__c != null)
                        Trainer =a.Training__r.Trainer_Name__c;
                     String endDate = '';
                    if(a.Training__r.End_Date__c!= null)
                        endDate =String.ValueOF(a.Training__r.End_Date__c);
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    subject = 'Reminder Mail';
                    mail.setTargetObjectId(a.User_Name__c);
                    mail.setSubject(subject);
                    mail.setHtmlBody('<p style="font-size:15px" face="verdana">Hi '+a.User_Name__r.name+',</br></br>This mail is to remind  that today is the Due Date for '+a.Training__r.Training_Name__c  +'</br>Following are the details of training:</br></br>Date: '+a.Training__r.Start_Date__c.format()+' - '+endDate+'</br>Training Name: '+a.Training__r.Training_Name__c +'</br>Trainer Name: '+Trainer +'</br>Training Methodologies: '+a.Training__r.Training_Methodologies__c +'</br>Due Date: '+a.Due_Date__c.format() +'</br></br>Sincerely,</br>Your LMS Team </br></br><img src="https://c.ap1.content.force.com/servlet/servlet.ImageServer?id=01590000003uX86&oid=00D90000000uV70&lastMod=1402396559000" width="120" height="50"></p>');
                  
                    mail.setSaveAsActivity(false);    
                    mailList1.add(mail);
                } 
         }
         
       
         if(mailList != null && mailList.size() > 0){
            List<Messaging.SendEmailResult> results = Messaging.sendEmail(mailList);
         
         }
        
         
         
            
         
         if(mailList1 != null && mailList1.size() > 0){
            List<Messaging.SendEmailResult> results = Messaging.sendEmail(mailList1);
            
         }
     }
     
      global void finish(Database.BatchableContext bcFinish){
        

    }
}