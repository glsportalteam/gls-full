/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSCommunity_ProfilePageCtrlExtTest {
	public static User u;
    static testMethod void myUnitTest() {
    	Account accObj 						= GLSLPW_SetUpData.createAccount('Amgen');
         Contact conObj 					= GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
         conObj.MailingStreet				= 'Test';
         conObj.MailingCity					= 'Test';
         conObj.MailingState				= 'Test';
         conObj.MailingCountry				= 'Test';
         conObj.MailingPostalCode			= 'Test';
         update conObj;
     	Quote__c quoteObj  					= GLSLPW_SetUpData.createQuote(accObj.Id,conObj.Id,'Approved');
         test.startTest();
	         Profile p = [select id from profile where name=: GLSConfig.Communityprofile];
	          u = new User(LastName = 'test user 2', 
                             Username = 'test.user.2@example.com', 
                             Email = 'test.2@example.com', 
                             Alias = 'testu2',
                             contactId = conObj.Id, 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = p.Id, 
                             LanguageLocaleKey = 'en_US'
                             );
            System.runAs (u){
	         	GLSCommunity_ProfilePageControllerExt obj = new GLSCommunity_ProfilePageControllerExt(new ApexPages.StandardController(quoteObj));
	         	obj.contactName = 'Test Name';
	         	obj.contactLastName = 'Test Last Name';
	         	obj.contactTitle = 'Test Title';
	         	obj.contactDept = 'Test Deprt';
	         	obj.contactPhone = '9999999999';
	         	obj.contactEmail = 'Test Email';
	         	obj.contactFax = '999999';
	         	obj.ccEmail = 'test@test.com';
	         	obj.streetName = 'Test Street';
	         	obj.city = 'Test City';
	         	obj.state = 'Test State';
	         	obj.country = 'Test Country';
	         	obj.zipCode = '444444';
	    	    obj.isUpdated = true;
	         	obj.updateUserProfile(); 
	         	obj.saveAddressToContact();
	          }
	         test.stopTest();
    }
}