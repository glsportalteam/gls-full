global class LMS_AutoDeactivateTrainings implements Schedulable {
  
    global void execute(SchedulableContext sc) {
        List<Training__c> allTrainings = new List<Training__c>();
        List<Training__c> deactivateTrainings = new List<Training__c>();
        allTrainings = [Select Id,End_Date__c,Active__c from Training__c where Active__c=true];
        for(Training__c training :  allTrainings)
        {
            system.debug('----training-----' +training);
            if(training.End_Date__c < Date.today() && training.End_Date__c != null )
            {
                
                training.Active__c = false; 
                deactivateTrainings.add(training);
                system.debug('----deactivateTrainings-----' +deactivateTrainings);
            }
        }
        
        
        Try{
        if(deactivateTrainings != null && deactivateTrainings.size()>0)
            update deactivateTrainings;
        }
        catch(DMLException de)
        {
            system.debug('***DML Exception occured****' +de);
        }
 
           }
}