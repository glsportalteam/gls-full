/**
 * This class contains unit tests for GLSAPIListener Salesforce API
 * and triggers.
 */
 
@isTest
private class GLSAPITransactionCompleteTest {
    
    
     /**
    * Unit test method for getExternalFiles API call Success
    */
    static testMethod void getExternalAPICall() {
        GLSSetupData.setupData();
        test.startTest();
            GLSSetupData.isTransaction =true;
            GLSSetupData.isReponseCorrect = true;
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','GetExternalFiles','<xmlString><TransactionID>70a4684b-2cf0-4646-b47e-lidanahk2706</TransactionID><Files><FileDetail><FileSalesforceFileId>'+GLSSetupData.qFiles3.Id+'</FileSalesforceFileId><APIFileId>1107</APIFileId><Translatable>true</Translatable></FileDetail><FileDetail><FileSalesforceFileId>a0kZ0000005YsHnIAK</FileSalesforceFileId><APIFileId>1108</APIFileId><Translatable>true</Translatable></FileDetail></Files><ErrorMessage></ErrorMessage></xmlString>' );
            System.assertEquals(GLSAPITransactionComplete.response,'false,NULL, ');
        test.stopTest();
    }
    
     static testMethod void getExternalAPICall1() {
        GLSSetupData.setupData();
        test.startTest();
            GLSSetupData.isTransaction =false;
             GLSSetupData.isReponseCorrect = false;
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','GetExternalFiles','<xmlString><TransactionID>70a4684b-2cf0-4646-b47e-lidanahk2706</TransactionID><Files><FileDetail><FileSalesforceFileId>'+GLSSetupData.qFiles3.Id+'</FileSalesforceFileId><APIFileId>1107</APIFileId><Translatable>true</Translatable></FileDetail><FileDetail><FileSalesforceFileId>a0kZ0000005YsHnIAK</FileSalesforceFileId><APIFileId>1108</APIFileId><Translatable>true</Translatable></FileDetail></Files><ErrorMessage></ErrorMessage></xmlString>' );
            System.assertEquals(GLSAPITransactionComplete.response,'true,0, ');
        test.stopTest();
    }
    
    /**
    * Unit test method for getExternalFiles API call Success with Error Message
    */
   static testMethod void getExternalAPICallError() {
        GLSSetupData.setupData();
        test.startTest();
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','GetExternalFiles','<xmlString><TransactionID>70a4684b-2cf0-4646-b47e-lidanahk2706</TransactionID><Files><FileDetail><FileSalesforceFileId>'+GLSSetupData.qFiles3.Id+'</FileSalesforceFileId><APIFileId>1107</APIFileId><Translatable>true</Translatable></FileDetail><FileDetail><FileSalesforceFileId>a0kZ0000005YsHnIAK</FileSalesforceFileId><APIFileId>1108</APIFileId><Translatable>true</Translatable></FileDetail></Files><ErrorMessage>Test Error</ErrorMessage></xmlString>' );
            System.assertEquals(GLSAPITransactionComplete.response,'true,0, ');
        test.stopTest();
    }
    
    /**
    * Unit test method for AnalyzeFiles API call Success
    */
   static testMethod void analyzeFileAPICall(){
        GLSSetupData.createQLI();
        test.startTest();
            GLSSetupData.isTransaction =true;
            GLSSetupData.isReponseCorrect = true;
            GLSSetupData.isSuccessCode = true;
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','AnalyzeFiles','<xmlString><TransactionID>625</TransactionID><BucketName>gls-us</BucketName><OutputPath>amgen*'+GLSSetupData.quote.Id+'</OutputPath><AWSFilename>AutomaticTask20140401_080230_853242.xml</AWSFilename><AWSVersionID/><APIFileID>943</APIFileID><ErrorMessage></ErrorMessage></xmlString>');
         //  System.assertEquals(GLSAPITransactionComplete.response,'true,0, ');
        test.stopTest();
    }
    
    static testMethod void analyzeFileAPICall1(){
        GLSSetupData.createQLI();
        test.startTest();
            GLSSetupData.isTransaction =false;
            GLSSetupData.isReponseCorrect = false;
            GLSSetupData.isSuccessCode = false;
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','AnalyzeFiles','<xmlString><TransactionID>625</TransactionID><BucketName>gls-us</BucketName><OutputPath>amgen*'+GLSSetupData.quote.Id+'</OutputPath><AWSFilename>AutomaticTask20140401_080230_853242.xml</AWSFilename><AWSVersionID/><APIFileID>943</APIFileID><ErrorMessage></ErrorMessage></xmlString>');
         //  System.assertEquals(GLSAPITransactionComplete.response,'true,0, ');
        test.stopTest();
    }
    /**
    * Unit test method for AnalyzeFiles API call Success With Error Messgae
    */
   static testMethod void analyzeFileAPICallError(){
        GLSSetupData.createQLI();
        test.startTest();
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','AnalyzeFiles','<xmlString><TransactionID>625</TransactionID><BucketName>gls-us</BucketName><OutputPath>amgen*'+GLSSetupData.quote.Id+'</OutputPath><AWSFilename>AutomaticTask20140401_080230_853242.xml</AWSFilename><AWSVersionID/><APIFileID>943</APIFileID><ErrorMessage>Test Error</ErrorMessage></xmlString>');
         //   System.assertEquals(GLSAPITransactionComplete.response,'true,0, ');
        test.stopTest();
    }
    
    /**
    * Unit test method for TMExport API call Success
    */
   static testMethod void TMExportAPICall(){
        GLSSetupData.createQLI();
        test.startTest();
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','TMExport','<xmlString><TransactionID>626</TransactionID><BucketName>gls-us</BucketName><OutputPath>amgen*'+GLSSetupData.quote.Id+'</OutputPath><AWSFilename>AutomaticTask20140401_080230_853242.xml</AWSFilename><AWSVersionID/><APIFileID>943</APIFileID><ErrorMessage></ErrorMessage></xmlString>');
         //   System.assertEquals(GLSAPITransactionComplete.response,'true,0, ');
        test.stopTest();
    }
    
    
    /**
    * Unit test method for UpdateSFTMTable API call Success
    */
    static testMethod void UpdateSFTMTableAPICall(){
        GLSSetupData.setupData();
        test.startTest();
            GLSSetupData.isTransaction =true;
            GLSSetupData.isReponseCorrect = true;
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','UpdateSFTMTable','<xmlString><TransactionID>9a5285bc-6d59-48a6-a49c-e689b1216f85</TransactionID><SalesforceClientID>'+GLSSetupdata.accountObj.Id+'</SalesforceClientID><TMName>French (All)...</TMName><GLSTMFileID>111</GLSTMFileID> <TMFileFullPath>//glsnas/Operations/IT/GLS API/Test-TM-Files/Amgen_EPR2EN-US.sdltm</TMFileFullPath><TMFileName>Amgen_EPR2EN-US.sdltm</TMFileName><SourceLanguageCode>en-US</SourceLanguageCode><UpdateType> </UpdateType><TargetLanguageCode>es-PR</TargetLanguageCode></xmlString>');
            String TMName = [Select Name From Client_TM__c Where Id =: GLSSetupData.clientTM.Id limit 1].Name;
          //  System.assertEquals(GLSAPITransactionComplete.response,'true,0,'+TMName);
        test.stopTest();
    }
    
    static testMethod void UpdateSFTMTableAPICall1(){
        GLSSetupData.setupData();
        test.startTest();
            GLSSetupData.isTransaction =false;
            GLSSetupData.isReponseCorrect = false;
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','UpdateSFTMTable','<xmlString><TransactionID>9a5285bc-6d59-48a6-a49c-e689b1216f85</TransactionID><SalesforceClientID>'+GLSSetupdata.accountObj.Id+'</SalesforceClientID><TMName>French (All)...</TMName><GLSTMFileID>111</GLSTMFileID> <TMFileFullPath>//glsnas/Operations/IT/GLS API/Test-TM-Files/Amgen_EPR2EN-US.sdltm</TMFileFullPath><TMFileName>Amgen_EPR2EN-US.sdltm</TMFileName><SourceLanguageCode>en-US</SourceLanguageCode><UpdateType> </UpdateType><TargetLanguageCode>es-PR</TargetLanguageCode></xmlString>');
            String TMName = [Select Name From Client_TM__c Where Id =: GLSSetupData.clientTM.Id limit 1].Name;
          //  System.assertEquals(GLSAPITransactionComplete.response,'true,0,'+TMName);
        test.stopTest();
    }
    /**
    * Unit test method for UpdateSFTMTable API call Without Source Code
    */
    static testMethod void UpdateSFTMTableAPICallNoSourceCode(){
        GLSSetupData.setupData();
        test.startTest();
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','UpdateSFTMTable','<xmlString><TransactionID>9a5285bc-6d59-48a6-a49c-e689b1216f85</TransactionID><SalesforceClientID>'+GLSSetupdata.accountObj.Id+'</SalesforceClientID><TMName>French (All)...</TMName><GLSTMFileID>111</GLSTMFileID> <TMFileFullPath>//glsnas/Operations/IT/GLS API/Test-TM-Files/Amgen_EPR2EN-US.sdltm</TMFileFullPath><TMFileName>Amgen_EPR2EN-US.sdltm</TMFileName><SourceLanguageCode></SourceLanguageCode><UpdateType> </UpdateType><TargetLanguageCode>es-PR</TargetLanguageCode></xmlString>');
            String TMName = [Select Name From Client_TM__c Where Id =: GLSSetupData.clientTM.Id limit 1].Name;
            System.assertEquals(GLSAPITransactionComplete.response,'true,1,Source Language code is Empty');
        test.stopTest();
    }
    
    /**
    * Unit test method for UpdateSFTMTable API call Without Target Code
    */
    static testMethod void UpdateSFTMTableAPICallNoTrgCode(){
        GLSSetupData.setupData();
        test.startTest();
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','UpdateSFTMTable','<xmlString><TransactionID>9a5285bc-6d59-48a6-a49c-e689b1216f85</TransactionID><SalesforceClientID>'+GLSSetupdata.accountObj.Id+'</SalesforceClientID><TMName>French (All)...</TMName><GLSTMFileID>111</GLSTMFileID> <TMFileFullPath>//glsnas/Operations/IT/GLS API/Test-TM-Files/Amgen_EPR2EN-US.sdltm</TMFileFullPath><TMFileName>Amgen_EPR2EN-US.sdltm</TMFileName><SourceLanguageCode>en-US</SourceLanguageCode><UpdateType> </UpdateType><TargetLanguageCode></TargetLanguageCode></xmlString>');
            String TMName = [Select Name From Client_TM__c Where Id =: GLSSetupData.clientTM.Id limit 1].Name;
            System.assertEquals(GLSAPITransactionComplete.response,'true,1,Target Language code is empty');
        test.stopTest();
    }
    
    
    /**
    * Unit test method for UpdateSFTMTable API call Without Client Code
    */
    static testMethod void UpdateSFTMTableAPICallNoClient(){
        GLSSetupData.setupData();
        test.startTest();
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','UpdateSFTMTable','<xmlString><TransactionID>9a5285bc-6d59-48a6-a49c-e689b1216f85</TransactionID><SalesforceClientID></SalesforceClientID><TMName>French (All)...</TMName><GLSTMFileID>111</GLSTMFileID> <TMFileFullPath>//glsnas/Operations/IT/GLS API/Test-TM-Files/Amgen_EPR2EN-US.sdltm</TMFileFullPath><TMFileName>Amgen_EPR2EN-US.sdltm</TMFileName><SourceLanguageCode>en-US</SourceLanguageCode><UpdateType> </UpdateType><TargetLanguageCode>es-PR</TargetLanguageCode></xmlString>');
            String TMName = [Select Name From Client_TM__c Where Id =: GLSSetupData.clientTM.Id limit 1].Name;
            System.assertEquals(GLSAPITransactionComplete.response,'true,1,Client Id is Empty');
        test.stopTest();
    }
    
    
    /**
    * Unit test method for UpdateSFTMTable API call Remove
    */
    static testMethod void UpdateSFTMTableAPICallRemove(){
        GLSSetupData.setupData();
        test.startTest();
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','UpdateSFTMTable','<xmlString><TransactionID>9a5285bc-6d59-48a6-a49c-e689b1216f85</TransactionID><SalesforceClientID>'+GLSSetupdata.accountObj.Id+'</SalesforceClientID><TMName>French (All)...</TMName><GLSTMFileID>111</GLSTMFileID> <TMFileFullPath>//glsnas/Operations/IT/GLS API/Test-TM-Files/Amgen_EPR2EN-US.sdltm</TMFileFullPath><TMFileName>Amgen_EPR2EN-US.sdltm</TMFileName><SourceLanguageCode>en-US</SourceLanguageCode><UpdateType>Remove</UpdateType><TargetLanguageCode>es-PR</TargetLanguageCode></xmlString>');
            String TMName = [Select Name From Client_TM__c Where Id =: GLSSetupData.clientTM.Id limit 1].Name;
           // System.assertEquals(GLSAPITransactionComplete.response,'true,0,'+TMName);
        test.stopTest();
    }
    /*changes*/
     static testMethod void unittest1(){
        GLSSetupData.createQLI();
        test.startTest();
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','TMExport','<xmlString><TransactionID> 645 </TransactionID><BucketName>gls-us</BucketName><OutputPath>amgen*'+GLSSetupData.quote.Id+'</OutputPath><AWSFilename>AutomaticTask20140401_080230_853242.xml</AWSFilename><AWSVersionID/><APIFileID>943</APIFileID><ErrorMessage>Test error</ErrorMessage></xmlString>');
           // System.assertEquals(GLSAPITransactionComplete.response,'true,0, ');
        test.stopTest();
    
    }
     static testMethod void unittest2() {
        GLSSetupData.setupData();
        test.startTest();
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','GetExternalFiles','<xmlString><TransactionID></TransactionID><Files><FileDetail><FileSalesforceFileId>'+GLSSetupData.qFiles3.Id+'</FileSalesforceFileId><APIFileId>1107</APIFileId><Translatable>true</Translatable></FileDetail><FileDetail><FileSalesforceFileId>a0kZ0000005YsHnIAK</FileSalesforceFileId><APIFileId>1108</APIFileId><Translatable>true</Translatable></FileDetail></Files><ErrorMessage>Test Error</ErrorMessage></xmlString>' );
           // System.assertEquals(GLSAPITransactionComplete.response,'true,0, ');
           
        test.stopTest();
         
    }
     static testMethod void unittest3() {
        GLSSetupData.createQLI();
        test.startTest();
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','TMExport','<xmlString><TransactionID></TransactionID><BucketName>gls-us</BucketName><OutputPath>amgen*'+GLSSetupData.quote.Id+'</OutputPath><AWSFilename>AutomaticTask20140401_080230_853242.xml</AWSFilename><AWSVersionID/><APIFileID></APIFileID><ErrorMessage></ErrorMessage></xmlString>');
          // System.assertEquals(GLSAPITransactionComplete.response,'true,0, ');
        test.stopTest();
         
    }
     static testMethod void unittest4() {
        GLSSetupData.setupData();
        test.startTest();
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','AnalyzeFiles','<xmlString></xmlString>');
          // System.assertEquals(GLSAPITransactionComplete.response,'true,0, ');
        test.stopTest();
         
    }
     static testMethod void unittest5() {
        GLSSetupData.setupData();
        test.startTest();
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','test','<xmlString></xmlString>');
          // System.assertEquals(GLSAPITransactionComplete.response,'true,0, ');
        test.stopTest();
         
    }
    static testMethod void unittest6() {
        GLSSetupData.setupData();
        test.startTest();
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','','<xmlString></xmlString>');
          // System.assertEquals(GLSAPITransactionComplete.response,'true,0, ');
        test.stopTest();
         
    }
     static testMethod void unittest7(){
        GLSSetupData.setupData();
        test.startTest();
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','UpdateSFTMTable','<xmlString><TransactionID>9a5285bc-6d59-48a6-a49c-e689b1216f85</TransactionID><SalesforceClientID>'+GLSSetupdata.accountObj.Id+'</SalesforceClientID><TMName>French (All)...</TMName><GLSTMFileID></GLSTMFileID> <TMFileFullPath>//glsnas/Operations/IT/GLS API/Test-TM-Files/Amgen_EPR2EN-US.sdltm</TMFileFullPath><TMFileName>Amgen_EPR2EN-US.sdltm</TMFileName><SourceLanguageCode>en-US</SourceLanguageCode><UpdateType> </UpdateType><TargetLanguageCode>es-PR</TargetLanguageCode></xmlString>');
            String TMName = [Select Name From Client_TM__c Where Id =: GLSSetupData.clientTM.Id limit 1].Name;
           // System.assertEquals(GLSAPITransactionComplete.response,'true,0,'+TMName);
        test.stopTest();
    }
   
       static testMethod void unittest8(){
         GLSSetupData.createQLI1();
            test.startTest();
            GLSAPITransactionComplete.NotifyTransactionComplete('20', 'testsecuritytoken','AnalyzeFiles','<xmlString><TransactionID>625</TransactionID><BucketName>gls-us</BucketName><OutputPath>amgen*'+GLSSetupData.quote.Id+'</OutputPath><AWSFilename>AutomaticTask20140401_080230_853242.xml</AWSFilename><AWSVersionID/><APIFileID>943</APIFileID><ErrorMessage>Test Error</ErrorMessage></xmlString>');
          // System.assertEquals(GLSAPITransactionComplete.response,'true,0, ');
            test.stopTest();
        }
}