@isTest
private with sharing class VendorControllerTests
{
    public static testMethod void testSearch()
    {
        //Inserting dummy data for search
        List<Vendor__c> lstVendors = new List<Vendor__c>();
        decimal rate = 0;
        for(Integer i = 1; i < 6; i++)
        {
            rate = rate + 0.01;
            Vendor__c vendor = new Vendor__c();
            vendor.First_Name_Vendor__c = 'TestFirstname' + i;
            vendor.Last_Name_Vendor__c = 'TestLastname' + i;
            vendor.Native_Language__c = 'English';
            vendor.All_Languages__c = 'English;French';
            vendor.Translation_rates_per_word__c = rate + 0.01;  
            vendor.Editing_Rates_per_word__c = rate;
            vendor.Agency_Name__c = 'TestAgency' + i;
            vendor.CAT_Tools__c = 'TestCAT';
            vendor.Expertise__c = 'writing';
            vendor.Email_Vendor__c ='adil_khan'+i+'@persistent.co.in';
            vendor.Job_Type__c = 'TestJobType';
            vendor.Documentation__c = 'TestDocumentation';
            lstVendors.add(vendor);                     
        }
        insert lstVendors;
        
        // test search
        VendorController controller = new VendorController();
        
        controller.vendor.First_Name_Vendor__c = 'test';
        controller.vendor.Last_Name_Vendor__c = 'test';
        controller.vendor.Native_Language__c = 'English';
        controller.vendor.All_Languages__c = 'French';
        controller.vendor.Agency_Name__c = 'test';
        controller.vendor.CAT_Tools__c = 'test';
        controller.vendor.Expertise__c = 'writing';
        controller.RatePerWordMin = 0;
        controller.RatePerWordMax = 0.5;
        controller.RatePerEditMin = 0;
        controller.RatePerEditMax = 0.5;
        controller.runSearch();
        controller.NextProfile();
        controller.PreviousProfile();
        system.assert(controller.vendors.size() > 0);
        
        //test sort
        controller.sortField = 'First_Name_Vendor__c';
        controller.toggleSort();
    }
}