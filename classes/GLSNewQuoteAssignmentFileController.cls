public class GLSNewQuoteAssignmentFileController{
    /*
    * Parameters used for custom Note functionality page.
    */
    public Quote_Assignment_File__c newQuoteAssignmentFile{get;set;}
    public Id quoteAssignmentFileId {get;set;}
    public string quoteId{get;set;}
    public string returnUrl;
    public Boolean isReadOnly{get;set;}
    public Boolean isEdit{get;set;}
    public string userType{get;set;}
    public string quoteAssignmentName{get;set;}
    public string quoteAssignment;
    public string quoteFilePerfix{get;set;} 
    Transient Schema.DescribeSObjectResult perfixQAssignment;
    Transient Schema.DescribeSObjectResult perfixQuoteFile;
  
    /*
    * Constructor
    */
    public GLSNewQuoteAssignmentFileController(ApexPages.StandardController stdcon) {
      
      Quote_Assignment__c Qassignment;
      Quote_Assignment_File__c QAssignmentFile;
     // quoteAssignmentFileId = stdcon.getId();
      newQuoteAssignmentFile=(Quote_Assignment_File__c)stdcon.getRecord();
      isReadOnly=false;
      isEdit=false;
      User u=[SELECT Id,UserType FROM User WHERE Id = :userInfo.getUserId()limit 1];
      userType=u.UserType;
      perfixQAssignment = Quote_Assignment__c.SObjectType.getDescribe();
      perfixQuoteFile= Quotes_File__c.SObjectType.getDescribe();
      quoteFilePerfix=perfixQuoteFile.getKeyPrefix().substring(0,3);
      returnUrl = ApexPages.currentPage().getParameters().get('retURL');
      quoteAssignmentFileId = ApexPages.currentPage().getParameters().get('id');
      if(returnUrl != null){
         quoteAssignment=returnUrl.removeStart('/');
         if(quoteAssignment.startsWith(perfixQAssignment.getKeyPrefix())){
           Qassignment=[select name,Id,Quote__c from Quote_Assignment__c where id=:quoteAssignment limit 1];
           newQuoteAssignmentFile.Quote_Assignment__c=Qassignment.Id;
           quoteId=Qassignment.Quote__c;
        }else if(quoteAssignment.contains('customer')){
        	quoteAssignment=quoteAssignment.removeStart('customer/');
            quoteAssignment=quoteAssignment.substring(0,15);
             if(quoteAssignment.startsWith(perfixQAssignment.getKeyPrefix())){
          	    Qassignment=[select name,Id,Quote__c from Quote_Assignment__c where id=:quoteAssignment limit 1];
            	newQuoteAssignmentFile.Quote_Assignment__c=Qassignment.Id;
           	    quoteId=Qassignment.Quote__c;
            }
        }
      }
      if(quoteAssignmentFileId !=null){
            isReadOnly=true;
        }
        if(quoteAssignmentFileId !=null && returnUrl != null ){
            isReadOnly=false;
        }
           getQuoteAssignementFile();
    }
    
   public void getQuoteAssignementFile(){ 
         if(quoteAssignmentFileId!=null){
            isEdit=true;	
        	newQuoteAssignmentFile=[select id,Quote_Assignment__c,Quote_Assignment__r.Quote__c,Name,Quote_Files__c,File_Usage__c from Quote_Assignment_File__c where id=:quoteAssignmentFileId limit 1];
            quoteId=newQuoteAssignmentFile.Quote_Assignment__r.Quote__c;
            quoteAssignmentName=newQuoteAssignmentFile.Name;
            returnUrl='/'+newQuoteAssignmentFile.Quote_Assignment__c;
         }
   }
   public pageReference customSave(){
      try{
	       Database.upsert(newQuoteAssignmentFile);
	       return new PageReference(returnUrl);
	     } 
	      catch(Exception e){
		    return null;    
		 }
    } 
   public pageReference saveAndNew(){
       try{ 
            Database.upsert(newQuoteAssignmentFile);
	        newQuoteAssignmentFile = new Quote_Assignment_File__c();
	        PageReference pRef=new PageReference('/apex/GLSNewQuoteAssignmentFilePage?retURL='+returnUrl); 
	        pRef.setRedirect(true);
	        return pRef;
	        
	      } 
	   catch(Exception e){
		     return null;    
		   }   
   } 
   /**
    *  Custom Edit method.
    **/
    public pagereference customEdit(){
        isReadOnly=false;
        return null;
    } 
   /**
    *  Custom Clone method.
    **/ 
    
     public PageReference cloneQuoteAssignmentFile(){
       try{
            isEdit=false;
            isReadOnly=false;
            newQuoteAssignmentFile = newQuoteAssignmentFile.clone();
            return null; 
       }catch(Exception ex){return null;} 
     }
  /**
    *  Custom Delete method.
    **/
  public PageReference customDelete(){
        string QuoteFileAssignId= Apexpages.currentPage().getParameters().get('id');
        if(QuoteFileAssignId != null){
	        Quote_Assignment_File__c QuoteFileAssign=[select id,Quote_Assignment__c,Name,Quote_Files__c,File_Usage__c from Quote_Assignment_File__c where id=:QuoteFileAssignId limit 1];
	        string retUrl =QuoteFileAssign.Quote_Assignment__c;
	        delete QuoteFileAssign;
	        PageReference pRef=new PageReference('/'+retUrl);
	        pRef.setRedirect(true);
		    return pRef;
        }else{
        return null;
        }    
    
    } 
 
}