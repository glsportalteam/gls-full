/* Copyright (c) 2013, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSQW_UploadLogFilesController  Class
* Function: Upload files to Amazon S3 server and create corresponding Quote file record for the uploaded file
*/

@isTest
private class GLSQW_UploadLogFilesControllerTest {

    /**
    * This Unit Test method will test the constructor 
    **/
    static testMethod void myUnitTest() { 
        GLSSetupData.setupData();
        Test.startTest();
         
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.Id);
        GLSQW_UploadLogFilesController uploadFile = new GLSQW_UploadLogFilesController(new ApexPages.StandardController(GLSSetupData.quote));
       /*String credName = createTestCredentials();
            uploadFile.AWSCredentialName = credName;
            uploadFile.getawss3LoginCredentials();*/
     //   system.assert( uploadFile.Account_Name != null );
        System.runAs(GLSSetupData.u){ 
         uploadFile = new GLSQW_UploadLogFilesController(new ApexPages.StandardController(GLSSetupData.quote));
       }
        Test.stopTest();
    }

    /**
    * This Unit Test method will test successful creation of Quote file record in Salesforce 
    **/
    static testMethod void successFileuploadTest() {
        GLSSetupData.createQLI();
        Test.startTest();
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.quoteLI1.Id);
        GLSQW_UploadLogFilesController uploadFile = new GLSQW_UploadLogFilesController(new ApexPages.StandardController(GLSSetupData.quoteLI1));
        uploadFile.newFileName='Test.txt';
        uploadFile.qliUpdate.Analyzed_Transaction_ID__c = '1234567';
        uploadFile.successFileupload();
        Test.stopTest();
    }
    /**
    * This method will create dummy credentials for testing 
    **/
    private static String createTestCredentials(){
        AWSKey__c testKey = new AWSKey__c(name='test keys',key__c='key',secret__c='secret');
        insert testKey;
        return testKey.name;
     } 
    /**
    * This method run amazon's signature's and policy tester
    **/
    static testmethod void policyTest() {
            GLSSetupData.createQLI();
            Test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.Id);
            GLSQW_UploadLogFilesController uploadFile = new GLSQW_UploadLogFilesController(new ApexPages.StandardController(GLSSetupData.quote));
            String credName = createTestCredentials();
            uploadFile.AWSCredentialName = credName;
            uploadFile.getawss3LoginCredentials();
            try{
                uploadFile.getHexPolicy( );
                uploadFile.getSignedPolicy();
            }catch(Exception ex){
            }
           system.assert( uploadFile.getHexPolicy() != null );
           system.assert( uploadFile.getSignedPolicy() != null );
           Test.stopTest(); 
     }
}