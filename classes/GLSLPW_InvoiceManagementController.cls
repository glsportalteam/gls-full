public with sharing class GLSLPW_InvoiceManagementController{
    public Program__c invoiceProgram                            {get;set;}
    public Protocol__c invoiceProtocol                          {get;set;}
    public Estimate__c estimate                                 {get;set;}
    
    public string selectedProtocolId                            {get;set;}
    public string selectedProgramId                             {get;set;}
    public string selectedAccountId                             {get;set;}
    public string selectedPAId                                  {get;set;}
        
    public List<OrderWrapper> lstOrderWrapper                   {get;set;}
    public Boolean noOrdersExists                               {get;set;}
    public Decimal PAAmount                                     {get;set;} 
    public Date invoiceDate                                     {get;set;}
    public Contact tempContact                                  {get;set;}
    private String protocolNumber                               {get;set;}
    public List<Purchase_Agreement__c> lstPurchaseAgreement     {get;set;}
    public Invoice__c invoice                                   {get;set;} 
    
    public Estimate_Line_Item__c estLineItem  {get;set;}
    
    public GLSLPW_InvoiceManagementController(){
        PAAmount = 0.00;        
        //invoiceDate = System.Today();
        lstOrderWrapper = new List<OrderWrapper>(); 
        noOrdersExists = true; 
        estimate = new Estimate__c();
        estLineItem = new Estimate_Line_Item__c();      
        invoiceProgram = new Program__c();
        lstPurchaseAgreement = new List<Purchase_Agreement__c>();  
        invoice = new Invoice__c();
        invoice.Date_of_Invoice__c= System.Today();          
    }
    /*
        * @param:
          selectedAccountId : Contains the account Id against which orders need to be fetched.        
          selectedProgramId : Contains the Program against which orders need to be fetched.
          selectedProtocolId: Contains the Protocol against which orders need to be fetched.
        * @description : This method will make search for all the orders which are billable status.  
    */
    public void searchOrder(){   
        PAAmount = 0.00;                                                                     
        List<quote__c> lstQuotes;  
        lstOrderWrapper = new List<OrderWrapper>();
        Protocol__c selectedProtocol;
        if(selectedAccountId != null && selectedAccountId != ''){
            if(selectedProgramId != null && selectedProgramId != ''){
                if(selectedProtocolId != null && selectedProtocolId != ''){    
                    selectedProtocol = [select id,PA_Amount__c,Invoiced_Amount__c from protocol__c where id=:selectedProtocolId];                                   
                    lstQuotes = [select id,Protocol__c,Protocol__r.PA_Amount__c,Protocol__r.Protocol_Name__c,Quote_Number__c,Urgency__c,
                    			 Protocol__r.Id,Expedited_Surcharge_Amount__c,Expedited_Surcharge__c,Quote_Total_Amount__c,Actual_Total_Amount__c,QLIESOverridden__c
                                 from quote__c where Protocol__r.id = : selectedProtocolId and Status__c =: GLSConfig.BillableStatus];
                    
                    if(selectedProtocol!=null){
                        if(selectedProtocol.Invoiced_Amount__c != null && selectedProtocol.PA_Amount__c != null)
                            PAAmount = selectedProtocol.PA_Amount__c - selectedProtocol.Invoiced_Amount__c;                         
                    }            
                                     
                }else{
                    List<Protocol__c> lstProtocol = [select id from Protocol__c where Program_Name__c =: selectedProgramId];
                    if(lstProtocol != null && lstProtocol.size() > 0){
                        lstQuotes = [select id,Protocol__c,Protocol__r.PA_Amount__c,Quote_Total_Amount__c,Actual_Total_Amount__c,Quote_Number__c,Urgency__c,QLIESOverridden__c,
                                        Protocol__r.Protocol_Name__c,Protocol__r.Id,Expedited_Surcharge_Amount__c,Expedited_Surcharge__c 
                                        from quote__c where Protocol__c IN: lstProtocol 
                                        and Status__c=:GLSConfig.BillableStatus];
                    }
                }
            }else{              
                lstQuotes = [select id,Protocol__c,Protocol__r.PA_Amount__c,Actual_Total_Amount__c,Quote_Total_Amount__c,Quote_Number__c,Urgency__c,QLIESOverridden__c,
                                        Protocol__r.Protocol_Name__c,Protocol__r.Id,Expedited_Surcharge_Amount__c,Expedited_Surcharge__c 
                                        from quote__c where Client__c =: selectedAccountId
                                        and Status__c=:GLSConfig.BillableStatus and Protocol__c = null];
            }                                                                      
            if(lstQuotes != null && lstQuotes.size() > 0){              
                /*if(selectedProgramId != null && selectedProgramId != ''){                                                         
                    for(Quote__C rfq : lstQuotes){
                        if(rfq.Protocol__r.PA_Amount__c!=null)  
                            PAAmount = PAAmount + rfq.Protocol__r.PA_Amount__c;
                    }                                       
                }*/
                /*
                List<Quote_Line_Item__c> lstQLI = [select id,QuoteID__r.Id,QuoteID__r.Protocol__r.Id,QuoteID__r.Quote_Number__c,
                                                  Country__r.name,File_Count__c,Source_Language_ID__r.Name,Target_Language_ID__r.Name,
                                                  Total_Amount__c,Expedited_Surcharge__c,Expedited_Total_Amount__c from Quote_Line_Item__c where QuoteID__c IN :lstQuotes];
                
                                                  
                if(lstQLI != null && lstQLI.size() > 0){
                    for(Quote_Line_Item__c QLI : lstQLI){
                    	                    	
                        OrderWrapper owObj = new OrderWrapper();                                                
                        owObj.quoteId = QLI.QuoteID__r.Id;
                        owObj.isSelected = false;
                        owObj.qlineItem =  QLI;
                        owObj.protocolId = QLI.QuoteID__r.Protocol__r.Id;
                        lstOrderWrapper.add(owObj);
                    }
                }
                */
                for( Quote__c rfq:lstQuotes ){
                	OrderWrapper owObj = new OrderWrapper();
                	owObj.quoteId = rfq.Id;
                    owObj.isSelected = false;                    
                    owObj.protocolId = rfq.Protocol__r.Id;
                    owObj.quote = rfq;
                    owObj.Expedite_Surcharge_Amount = rfq.Quote_Total_Amount__c - rfq.Actual_Total_Amount__c;
                    lstOrderWrapper.add(owObj);
                }
            }     
            if(lstOrderWrapper != null && lstOrderWrapper.size() > 0){
                noOrdersExists = false;
            }else{
                noOrdersExists = true;
            }          
        }
        if(PAAmount != null && PAAmount == 0){
        	PAAmount =0.00;
        }
    }
    
    /*
        @param : None
        @description : This method will return list of all purchase agreements related with selected program and protocol.    
    */
    public void getPurchaseAgreements(){
        Set<Id> setProtocolId = new Set<Id>();
        if(lstOrderWrapper != null && lstOrderWrapper.size() > 0){
            for(OrderWrapper ord : lstOrderWrapper){
                if(ord.isSelected == true){
                    setProtocolId.add(ord.protocolId);
                }
            }           
            if(setProtocolId != null && setProtocolId.size() > 0){
                List<id> lstprotocolId = new List<id>();
                lstprotocolId.addAll(setProtocolId);
                if(lstprotocolId != null && lstprotocolId.size() > 0){
                    lstPurchaseAgreement = [select id,Name,Issue_Date__c,Status__c,Total_Amount__c,Invoiced_Amount__c,Remaining_Balance__c ,Protocol_Id__r.Protocol_Name__c 
                                           from Purchase_Agreement__c where Protocol_Id__c IN: lstprotocolId];
                } 
            }
        }
    }
    
    /*
        @param: 
            SelectedPAId : contains the id of purchase agreement against which invoice needs to be created.
        @description : This method will  generate an invoice record,update related purchase agreement record and notify user with email.
    */
    public PageReference generateInvoice(){
        Set<Id> setQuoteId = new Set<Id>();
        List<Id> lstQuoteId = new List<Id>();
        //List<Quote_Line_Item__c> lstSelectedQLI = new List<Quote_Line_Item__c>();
        List<Quote__c> lstSelectedQuotes = new List<Quote__c>();  
        List<Quote__c> lstQuotes = new List<Quote__c>(); 
        Decimal invAmount=0.00;        
        //invoice = new Invoice__c();        
        //invoice.Date_of_Invoice__c = invoiceDate;
        system.debug(invoice.Date_of_Invoice__c+'invoice.Date_of_Invoice__c------------------------------');
        if(selectedPAId != null && selectedPAId != ''){
            invoice.Purchase_Agreement_Id__c = selectedPAId;
        }
        
        for(OrderWrapper ord : lstOrderWrapper){
            if(ord.isSelected == true){             
                setQuoteId.add(ord.quoteId);
                invAmount = invAmount + ord.quote.Quote_Total_Amount__c;
                /*if(ord.quote.Urgency__c=='Expedited' || ord.quote.QLIESOverridden__c){
                	invAmount = invAmount + ord.quote.Expedited_Surcharge_Amount__c;
                }else{
                	invAmount = invAmount + ord.quote.Actual_Total_Amount__c;
                }*/	 
            }
        }
        if(setQuoteId != null){
            lstQuoteId.addAll(setQuoteId);
            invoice.Number_of_Order__c = setQuoteId.size();
        }
        invoice.Invoice_Amount__c = invAmount;
        if(lstQuoteId != null && lstQuoteId.size() > 0){
            for(Id qId : lstQuoteId){
                Quote__c quote = new Quote__c();
                quote.id = qId;
                quote.status__c=GLSConfig.BilledStatus;
                lstQuotes.add(quote); 
            }   
        }
        
        if(lstPurchaseAgreement != null && lstPurchaseAgreement.size() > 0){
            Purchase_Agreement__c currentAgreement;
            for(Purchase_Agreement__c agreement : lstPurchaseAgreement){
                if(agreement.id == selectedPAId){
                    if(agreement.Protocol_Id__r.Protocol_Name__c!=null)    
                        protocolNumber = agreement.Protocol_Id__r.Protocol_Name__c;
                                        
                    Decimal totalThreshold = 0;
                    if(agreement.Total_Amount__c != null && agreement.Total_Amount__c != 0){
                        totalThreshold = (agreement.Total_Amount__c*90)/100; 
                    } else{
                        totalThreshold = 0;
                    } 
                
                    if(invAmount > totalThreshold){
                        currentAgreement = new Purchase_Agreement__c();
                        currentAgreement.id = agreement.id;
                        currentAgreement.Status__c = GLSConfig.Pending;
                    }                       
                }
            }
            if(currentAgreement != null && currentAgreement.id != null){
                try{
                    update currentAgreement;
                }catch(DMLException ex){
                    ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
                    ApexPages.addMessage(Msg);                  
                }
            }
        }
        try{
            if(lstQuotes!=null && lstQuotes.size()>0){
                update lstQuotes;
            }
            insert invoice;
            /*for(OrderWrapper ord : lstOrderWrapper){
                if(ord.isSelected == true){             
                    Quote_Line_Item__c qli = ord.qlineItem;
                    qli.Invoice_Id__c = invoice.id;
                    lstSelectedQLI.add(qli); 
                }
            }
            if(lstSelectedQLI != null && lstSelectedQLI.size() > 0){
                update lstSelectedQLI;
            } */
            	
            for(OrderWrapper ord : lstOrderWrapper){
                if(ord.isSelected == true){             
                    Quote__c q = ord.quote;
                    q.Invoice__c = invoice.id;
                    lstSelectedQuotes.add(q); 
                }
            }
            if(lstSelectedQuotes != null && lstSelectedQuotes.size() > 0){
                update lstSelectedQuotes;
            }            	                       
                      
            String mailSub;
            if(protocolNumber!=null){
                mailSub= 'Invoice Created:'+ protocolNumber +'-'+ invoice.Name + String.valueOf(invoiceDate);
            }else{
                mailSub= 'Invoice Created: '+invoice.Name+String.valueOf(invoiceDate);
            }
            List<String> toAddresses = new List<String> {'yash_mittal@persistent.co.in'};
            GLSEmailUtility.sendEmails(UserInfo.getUserId(),mailSub,toAddresses,GLSConfig.Invoice_Email_Template,invoice.id);           
            PageReference pref = new PageReference('/apex/GLSLPW_InvoiceDetailPage?invID='+invoice.id);
            pref.setRedirect(true);
            return pref;            
        }catch(DMLException ex){
            ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
            ApexPages.addMessage(Msg);          
        } 
        return null;                
    }
    
    public class OrderWrapper{
        public Id quoteId    {get;set;}
        public Id protocolId {get;set;}
        Public boolean isSelected {get;set;}
        public Double Expedite_Surcharge_Amount   {get;set;}
        //public Quote_Line_Item__c qlineItem {get;set;}
        public Quote__c quote {get;set;}
        public OrderWrapper(){
            
        }
    }
}