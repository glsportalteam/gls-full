/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSLPW_ProtocolCountryControllerExt Class
* Function: This class is used as a controller extension for the GLSLPW_ProtocolCountryPage
*/

public with sharing class GLSLPW_ProtocolCountryControllerExt {
    
    //Variable declaration for the class
    public List<String> lstCountries                {get; set;}
    public Set<String> setCountries                 {get; set;}
    public Map<String,Integer> mapcountrySites      {get; set;}
    public String countryName                       {get; set;}
    public Id protocolId                            {get; set;}
    
    private List<Protocol_Country__c> lstProtocolCountries;
    private Map<String,Integer> mapcountrySitesInitial;
    
    //Constructor
    public GLSLPW_ProtocolCountryControllerExt(ApexPages.StandardController stdcon){
        lstCountries = new List<String>();
        setCountries = new Set<String>();
        mapcountrySites = new Map<String,Integer>();
        mapcountrySitesInitial = new Map<String,Integer>();
        protocolId = ApexPages.currentPage().getParameters().get('id');
        if(protocolId != null){
            lstProtocolCountries = getProtocolCountries(protocolId);
        }
    }

    /* 
    @Description: Getter for obtaining the values of select list "Country" to be displayed on page
    @Params: None
    @Return Type: List<SelectOption>
    */
    public List<SelectOption> getCountryValues(){
        List<Country__c> lstCountry = new List<Country__c>();
        List<SelectOption> lstCountryValues = new List<SelectOption>();
        lstCountryValues.add(new SelectOption('', '--None--'));
        for(Country__c ctry : [SELECT Id, Name FROM Country__c ORDER BY Name ASC]){
            lstCountryValues.add(new SelectOption(ctry.Name, ctry.Name));
        }
        return lstCountryValues;
    }
    
    /* 
    @Description: Method to add the countries to map on clicking "Add Country to List" button
    @Params: None
    @Return Type: PageReference
    */  
    public PageReference addCountries(){
        if(lstCountries != null && lstCountries.size() > 0){
            for(String country : lstCountries){
                setCountries.add(country);
                mapcountrySites.put(country, 0);
            }
        }
        else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, GLSconfig.selectCountry); 
            ApexPages.addMessage(myMsg);
        }
        return null;
    }
    
    /* 
    @Description: To get the list of Protocol Countries associated with the current Protocol.
    @Params: Protocol Id
    @Return Type: List<Protocol_Country__c>
    */  
    private List<Protocol_Country__c> getProtocolCountries(Id protocolId){
        List<Protocol_Country__c> lstProtocolCountriesLocal = new List<Protocol_Country__c>();
        lstProtocolCountriesLocal = [SELECT Id, Number_of_Sites__c, Country_Id__c, Protocol_Id__c, Country_Id__r.Name
                                                                FROM Protocol_Country__c
                                                                WHERE Protocol_Id__c =: protocolId];
        if(lstProtocolCountriesLocal != null && lstProtocolCountriesLocal.size() > 0){
            for(Protocol_Country__c pc : lstProtocolCountriesLocal){
                mapcountrySites.put(pc.Country_Id__r.Name, Integer.valueOf(pc.Number_of_Sites__c));
                mapcountrySitesInitial.put(pc.Country_Id__r.Name, Integer.valueOf(pc.Number_of_Sites__c));
                setCountries.add(pc.Country_Id__r.Name);
            }
        }
        return lstProtocolCountriesLocal;
    }
    
     /* 
    @Description: Method to insert/update/delete records of Protocol Country
    @Params: None
    @Return Type: PageReference
    */  
    public PageReference saveAction(){
        PageReference pRef = null;
        try{
            
            Protocol_Country__c pcLocal = new Protocol_Country__c();
            //Form the map of Country:Record Id (further used to populate id values of country name in the lookup for inserting Protocol Country records)
            Map<String, Id> mapCountries = new Map<String, Id>();
            Map<String, Id> mapCountryIdLocal = new Map<String, Id>();
            for(Country__c country : [SELECT Id, Name FROM Country__c]){
                mapCountries.put(country.Name, country.Id);
            }   
            for(Protocol_Country__c pc : lstProtocolCountries){
                mapCountryIdLocal.put(pc.Country_Id__r.Name, pc.Id);
            }
            List<Protocol_Country__c> lstPCInsert = new List<Protocol_Country__c>();
            List<Protocol_Country__c> lstPCUpdate = new List<Protocol_Country__c>();
            List<Protocol_Country__c> lstPCDelete = new List<Protocol_Country__c>();
            
            if(mapcountrySites != null && mapcountrySites.size() > 0){
                for(String str : mapcountrySites.keyset()){
                    if(mapcountrySitesInitial.containsKey(str)){
                        if(mapcountrySites.get(str) != mapcountrySitesInitial.get(str)){
                            pcLocal = new Protocol_Country__c(Id = mapCountryIdLocal.get(str));
                            pcLocal.Number_of_Sites__c = mapcountrySites.get(str);
                            lstPCUpdate.add(pcLocal);
                        }
                    }
                    else{
                        pcLocal = new Protocol_Country__c(Number_of_Sites__c = mapcountrySites.get(str),
                                                          Protocol_Id__c = protocolId,
                                                          Country_Id__c = mapCountries.containsKey(str) ? mapCountries.get(str) : null);
                        lstPCInsert.add(pcLocal);
                    }
                }
                for(String str : mapcountrySitesInitial.keyset()){
                    if(!mapcountrySites.containsKey(str)){
                        pcLocal = new Protocol_Country__c(Id = mapCountryIdLocal.get(str));
                        lstPCDelete.add(pcLocal);
                    }
                }
            }
            if(lstPCInsert != null && lstPCInsert.size() > 0){
                insert lstPCInsert;
            }
            if(lstPCUpdate != null && lstPCUpdate.size() > 0){
                update lstPCUpdate;
            }
            if(lstPCDelete != null && lstPCDelete.size() > 0){
                delete lstPCDelete;
            }
            pRef = new PageReference('/apex/GLSLPW_ProtocolDetailPage?id=' + protocolId);
            pRef.setRedirect(true);
        }
        catch(DMLException de){
            system.debug(GLSConfig.DMLExceptionOccured+de);
        }
        catch(Exception e){
            system.debug(GLSConfig.ExceptionOccured+e);
        }
        return pRef;
    }
    
    /* 
    @Description: Removing the country from "Selected Countries" table if "Del" link is clicked.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference removeCountryAction(){
        mapcountrySites.remove(countryName);
        setCountries.remove(countryName);
        return null;
    }
}