public with sharing class GLSLPW_RPT_QLIUpdateExt {
 public boolean editMode {get;set;}
 public boolean saveMode {get;set;}
 public Quote_Line_Item__c currQLI {get;set;} 
 public GLSLPW_RPT_QLIUpdateExt(ApexPages.StandardController stdcon){
 	   currQLI = (Quote_Line_Item__c)stdcon.getRecord();
 	   saveMode = true;
 	   editMode = false;	 	    	     	   	
 }
 public void changeModeS(){
 	editMode = false;
 	saveMode= true;
 }
 public void changeModeE(){
 	editMode = true;
 	saveMode= false;
 }
 public void saveQLIInfo(){
 	system.debug(currQLI+'----------------currQLI-------------');
 	try{
 		update currQLI;
 		editMode = false;
 		saveMode= true;
 	}Catch(DMLException e){
 		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Future dates are not allowed for Delivery Date Renegotiation Initiated field'); 
        ApexPages.addMessage(myMsg);
 		editMode = true;
 		saveMode= false;
 	}
 }
 public void cancelChanges(){
 	editMode = false;
 	saveMode= true;
 }
 public PageReference backToPrvious(){
 	String QLIID = ApexPages.currentPage().getParameters().get('id');
 	PageReference pref = new PageReference('/'+QLIID);
    pref.setRedirect(true);
    return pref;
 }
}