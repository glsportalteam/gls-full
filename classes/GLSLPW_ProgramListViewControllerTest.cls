/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSLPW_ProgramListViewControllerTest {

    static testMethod void myUnitTest() {
        
        GLSSetupData.createQLIServices();
        Program__c prg = new Program__c(Account__c = GLSSetupData.accountObj.Id, Contact__c = GLSSetupData.contactObj.Id, 
                                        Therapeutic_Area__c='Immunology', Sponsor__c = GLSSetupData.accountObj.Id, Program_Name__c = 'Test Program');
        insert prg;
        
        test.startTest();
            GLSLPW_ProgramListViewController prgListViewObj = new GLSLPW_ProgramListViewController(new ApexPages.StandardController(prg));
            prgListViewObj.isChecked = false;
            prgListViewObj.spName = 'Amgen';
            prgListViewObj.prgName = 'Test Program';
            prgListViewObj.tArea = 'Immunology';
            prgListViewObj.prgAccount = 'Amgen';
            prgListViewObj.searchProgram();
            
            prgListViewObj.spName = '';
            prgListViewObj.prgName = 'Test Program';
            prgListViewObj.tArea = '';
            prgListViewObj.prgAccount = '';
            prgListViewObj.searchProgram();
            
            prgListViewObj.spName = '';
            prgListViewObj.prgName = '';
            prgListViewObj.tArea = '';
            prgListViewObj.prgAccount = '';
            prgListViewObj.searchProgram();
            
            prgListViewObj.spName = '';
            prgListViewObj.prgName = '';
            prgListViewObj.tArea = '';
            prgListViewObj.prgAccount = 'Amgen';
            prgListViewObj.searchProgram();
            
            prgListViewObj.spName = '';
            prgListViewObj.prgName = '';
            prgListViewObj.tArea = 'Immunology';
            prgListViewObj.prgAccount = '';
            prgListViewObj.searchProgram();
            
            prgListViewObj.spName = 'Amgen';
            prgListViewObj.prgName = '';
            prgListViewObj.tArea = '';
            prgListViewObj.prgAccount = '';
            prgListViewObj.searchProgram();
            
            prgListViewObj.spName = 'Amgen';
            prgListViewObj.prgName = 'Test Program';
            prgListViewObj.tArea = '';
            prgListViewObj.prgAccount = '';
            prgListViewObj.searchProgram();
            
            prgListViewObj.spName = '';
            prgListViewObj.prgName = 'Test Program';
            prgListViewObj.tArea = '';
            prgListViewObj.prgAccount = 'Amgen';
            prgListViewObj.searchProgram();

            prgListViewObj.spName = '';
            prgListViewObj.prgName = 'Test Program';
            prgListViewObj.tArea = 'Immunology';
            prgListViewObj.prgAccount = '';
            prgListViewObj.searchProgram();
            
            prgListViewObj.spName = '';
            prgListViewObj.prgName = 'Test Program';
            prgListViewObj.tArea = 'Immunology';
            prgListViewObj.prgAccount = '';
            prgListViewObj.searchProgram();
            
            prgListViewObj.spName = 'Amgen';
            prgListViewObj.prgName = '';
            prgListViewObj.tArea = 'Immunology';
            prgListViewObj.prgAccount = '';
            prgListViewObj.searchProgram();
            
            prgListViewObj.spName = 'Amgen';
            prgListViewObj.prgName = '';
            prgListViewObj.tArea = '';
            prgListViewObj.prgAccount = 'Amgen';
            prgListViewObj.searchProgram();
            
            prgListViewObj.spName = '';
            prgListViewObj.prgName = '';
            prgListViewObj.tArea = 'Immunology';
            prgListViewObj.prgAccount = 'Amgen';
            prgListViewObj.searchProgram();
            
            prgListViewObj.spName = '';
            prgListViewObj.prgName = 'Test Program';
            prgListViewObj.tArea = 'Immunology';
            prgListViewObj.prgAccount = 'Amgen';
            prgListViewObj.searchProgram();
            
            prgListViewObj.spName = 'Amgen';
            prgListViewObj.prgName = '';
            prgListViewObj.tArea = 'Immunology';
            prgListViewObj.prgAccount = 'Amgen';
            prgListViewObj.searchProgram();
            
            prgListViewObj.spName = 'Amgen';
            prgListViewObj.prgName = 'Test Program';
            prgListViewObj.tArea = 'Immunology';
            prgListViewObj.prgAccount = '';
            prgListViewObj.searchProgram();
            
            prgListViewObj.spName = 'Amgen';
            prgListViewObj.prgName = 'Test Program';
            prgListViewObj.tArea = '';
            prgListViewObj.prgAccount = 'Amgen';
            prgListViewObj.searchProgram();
            
            prgListViewObj.spName = '';
            prgListViewObj.prgName = 'Dummy Test';
            prgListViewObj.tArea = '';
            prgListViewObj.prgAccount = '';
            prgListViewObj.searchProgram();
        test.stopTest();
        
    }

}