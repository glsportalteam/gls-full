public with sharing class TaskBarController {
	/*Contains list of all the associated quotesAssignment records which are associated with the quote in process*/
	public list<Quote_Assignment__c> quoteAssignmentLst      {get;set;}
	public String quoteAssignId                              {get;set;}
	public string assignId                                   {get;set;}
	public string projectName                                {get;set;}
	public string isNewAssign                                       {get;set;}
	public string contactId                                         {get;set;}
	 
	/*Id of the quote in process*/
	public string quoteId             {get;set{
		if(value != null && value != ''){
			String qId = value;
			quoteAssignmentLst = [Select id from Quote_Assignment__c where Quote__c =:qId order by CreatedDate ]; 
			quoteId = value;
		}
	}
	}
		
	public PageReference switchTaskTab(){		
		if(quoteAssignId !=null && quoteAssignId !='' && quoteId != null && quoteId !=''){			
			PageReference pageref = Page.GLSCommunity_TaskType;
			pageref.getParameters().put('qId',quoteId);
			pageref.getParameters().put('qaId',quoteAssignId);
			if(contactId != null && contactId!=''){
				pageref.getParameters().put('conId',contactId);
			}
			pageref.setRedirect(true);
			return pageref;			
		}
		return null;
	}	
	
	public pageReference createNewAssignment(){
		if(quoteId != null && quoteId !=''){
			PageReference pageref = Page.GLSCommunity_TaskType;
			pageref.getParameters().put('qId',quoteId);
			pageref.getParameters().put('isNew','true');
			if(contactId != null && contactId!=''){
				pageref.getParameters().put('conId',contactId);
			}
			pageref.setRedirect(true);
			return pageref;
		}
		return null;
	}
	public pageReference deleteAssignment(){
		String assignId = ApexPages.currentPage().getParameters().get('delAssignId');
		if(assignId != null && assignId != ''){
			PageReference pageref = Page.GLSCommunity_TaskType;
			if(assignId != 'new'){
				Quote_Assignment__c qAssignment = new Quote_Assignment__c(id=assignId);
				try{
					delete qAssignment;
					if(quoteId != null && quoteId !=''){
						List<Quote_Assignment__c> qAssignLst = [select id from Quote_Assignment__c where Quote__c =:quoteId ORDER BY CreatedDate Limit 1];
						if(qAssignLst != null && qAssignLst.size()>0){
							pageref.getParameters().put('qaId',qAssignLst[0].Id);
							pageref.getParameters().put('isNew','false');
						}else{
							pageref.getParameters().put('isNew','true');
						}
					}
				}catch(Exception ex){
					system.debug(ex.getMessage());
				}  	
			}else{
				List<Quote_Assignment__c> qAssignLst = [select id from Quote_Assignment__c where Quote__c =:quoteId ORDER BY CreatedDate Limit 1];
				if(qAssignLst != null && qAssignLst.size()>0){
					pageref.getParameters().put('qaId',qAssignLst[0].Id);
					pageref.getParameters().put('isNew','false');
				}else{
					pageref.getParameters().put('isNew','true');
				}
			}
			pageref.getParameters().put('qId',quoteId);
			//pageref.getParameters().put('isNew','false');
			if(contactId != null && contactId!=''){
				pageref.getParameters().put('conId',contactId);
			}
			pageref.setRedirect(true);
			return pageref;
		}
		return null;
	}
	public TaskBarController(){
		quoteAssignmentLst = new List<Quote_Assignment__c>();
		isNewAssign = 'false';
	}	
}