/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSQW_ScopeOfServicesControllerTest {

    static testMethod void retrieveAllDataTest() {
        GLSSetupData.createQLI();
        test.startTest();
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
        ApexPages.currentPage().getParameters().put('assgDetailsText//Text?','This is sample text area');
        GLSQW_ScopeOfServicesController obj = new GLSQW_ScopeOfServicesController(new ApexPages.StandardController(GLSSetupData.quote));
        obj.selectQAssgRecord = GLSSetupData.qAsst;
        obj.getselSourceFilesArray();
        obj.getselServicesArray();
        obj.getselRefFilesArray();
        obj.getselDeliverablesArray();
        obj.setlstSelAnalyzeFiles(null);
        obj.setselSourceFilesArray(null);
        obj.setselAnalyzeFilesArray(null);
        obj.setselRefFilesArray(null);
        obj.setselServicesArray(null);
        obj.setselDeliverablesArray(null);
        obj.getselAnalyzeFilesArray();
        obj.getlstSelAnalyzeFiles();
        obj.getItems();
        test.stopTest();
    }
  
  
  static testMethod void customeDeleteTest() {
        
        test.startTest();
       
        GLSSetupData.createQLI();
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote1.id);
        ApexPages.currentPage().getParameters().put('assgDetailsText','This is sample text area');
        GLSQW_ScopeOfServicesController obj = new GLSQW_ScopeOfServicesController(new ApexPages.StandardController(GLSSetupData.quote));
        Quote_Assignment__c qaObj           = new Quote_Assignment__c(Quote__c = GLSSetupData.quote.id);
        insert qaObj;
        Quote_Assignment__c qaObj1          = new Quote_Assignment__c(Quote__c = GLSSetupData.quote.id);
        insert qaObj1;
        obj.selectQAssgRecord = GLSSetupData.qAsst;
        obj.getselSourceFilesArray();
        obj.getselServicesArray();
        obj.getselRefFilesArray();
        obj.getselDeliverablesArray();
        obj.setlstSelAnalyzeFiles(null);
        obj.setselSourceFilesArray(null);
        obj.setselAnalyzeFilesArray(null);
        obj.setselRefFilesArray(null);
        obj.setselServicesArray(null);
        obj.setselDeliverablesArray(null);
        obj.getselAnalyzeFilesArray();
        obj.getlstSelAnalyzeFiles();
        obj.getItems();
        obj.customDel();
        test.stopTest();
  }
   
   static testMethod void customSaveAndNewTest() {
        
        GLSSetupData.createQLI();
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote1.id);
        ApexPages.currentPage().getParameters().put('assgDetailsText','This is sample text area');
        Quote_Assignment__c qaObj           = new Quote_Assignment__c(Quote__c = GLSSetupData.quote1.id);
        insert qaObj;
        GLSQW_ScopeOfServicesController obj = new GLSQW_ScopeOfServicesController(new ApexPages.StandardController(GLSSetupData.quote));
        obj.selectQAssgRecord = GLSSetupData.qAsst;
        obj.getselSourceFilesArray();
        obj.getselServicesArray();
        obj.getselRefFilesArray();
        obj.getselDeliverablesArray();
        obj.setlstSelAnalyzeFiles(null);
        obj.setselSourceFilesArray(null);
        obj.setselAnalyzeFilesArray(null);
        obj.setselRefFilesArray(null);
        obj.setselServicesArray(null);
        obj.setselDeliverablesArray(null);
        obj.getselAnalyzeFilesArray();
        obj.getlstSelAnalyzeFiles();
        obj.getItems();
        test.startTest();
        obj.retrieveAssgDetails();
        obj.customSaveAndNew();
        test.stopTest();
  }
  
  static testMethod void callAnalyseAllTest() {
        
        test.startTest();
       
        GLSSetupData.createQLI();
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote1.id);
        ApexPages.currentPage().getParameters().put('assgDetailsText','This is sample text area');
        GLSQW_ScopeOfServicesController obj = new GLSQW_ScopeOfServicesController(new ApexPages.StandardController(GLSSetupData.quote));
        obj.selectQAssgRecord = GLSSetupData.qAsst;
        obj.getselSourceFilesArray();
        obj.getselServicesArray();
        obj.getselRefFilesArray();
        obj.getselDeliverablesArray();
        obj.setlstSelAnalyzeFiles(null);
        obj.setselSourceFilesArray(null);
        obj.setselAnalyzeFilesArray(null);
        obj.setselRefFilesArray(null);
        obj.setselServicesArray(null);
        obj.setselDeliverablesArray(null);
        obj.getselAnalyzeFilesArray();
        obj.getlstSelAnalyzeFiles();
        obj.getItems();
        obj.callAnalyseAll();
        test.stopTest();
  }
  
   
   static testMethod void updateQLI() {
        
        test.startTest();
       
        GLSSetupData.createQLI();
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote1.id);
        ApexPages.currentPage().getParameters().put('assgDetailsText','This is sample text area');
        GLSQW_ScopeOfServicesController obj = new GLSQW_ScopeOfServicesController(new ApexPages.StandardController(GLSSetupData.quote));
        obj.selectQAssgRecord = GLSSetupData.qAsst;
        obj.getselSourceFilesArray();
        obj.getselServicesArray();
        obj.getselRefFilesArray();
        obj.getselDeliverablesArray();
        obj.setlstSelAnalyzeFiles(null);
        obj.setselSourceFilesArray(null);
        obj.setselAnalyzeFilesArray(null);
        obj.setselRefFilesArray(null);
        obj.setselServicesArray(null);
        obj.setselDeliverablesArray(null);
        obj.getselAnalyzeFilesArray();
        obj.getlstSelAnalyzeFiles();
        obj.getItems();
        obj.updateQLI();
        test.stopTest();
  }
    
    static testMethod void callGenerateLineItemsTest() {
        GLSSetupData.createQLI();
        String url= 'https://c.cs11.visual.force.com/apex/GLSQW_ScopeOfServicesPage?id='+GLSSetupData.quote3.id;
        test.startTest();
       
        ApexPages.currentPage().getParameters().put('Url',url);
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote3.id);
        
        GLSQW_ScopeOfServicesController obj = new GLSQW_ScopeOfServicesController(new ApexPages.StandardController(GLSSetupData.quote3));
        obj.selectQAssgRecord = GLSSetupData.qAsst;
        obj.quoteID=GLSSetupData.quote2.id;
        obj.callGenerateLineItems();
        test.stopTest();  
    }
    
    static testMethod void createNewAssgTest() {
        GLSSetupData.createQLI();
        String url= 'https://c.cs11.visual.force.com/apex/GLSQW_ScopeOfServicesPage?id='+GLSSetupData.quote3.id;
        test.startTest();
       
        ApexPages.currentPage().getParameters().put('Url',url);
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote3.id);
        
        GLSQW_ScopeOfServicesController obj = new GLSQW_ScopeOfServicesController(new ApexPages.StandardController(GLSSetupData.quote3));
        obj.selectQAssgRecord = GLSSetupData.qAsst;
        obj.quoteID=GLSSetupData.quote2.id;
        obj.createNewAssg();
        test.stopTest();  
    }
    
      static testMethod void callAnalyseAllOnCompleteTest() {
        GLSSetupData.createQLI();
        String url= 'https://c.cs11.visual.force.com/apex/GLSQW_ScopeOfServicesPage?id='+GLSSetupData.quote3.id;
        test.startTest();
       
        ApexPages.currentPage().getParameters().put('Url',url);
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote3.id);
        
        GLSQW_ScopeOfServicesController obj = new GLSQW_ScopeOfServicesController(new ApexPages.StandardController(GLSSetupData.quote3));
        obj.selectQAssgRecord = GLSSetupData.qAsst;
        obj.quoteID=GLSSetupData.quote2.id;
        obj.callAnalyseAllOnComplete();
        test.stopTest();  
    }
    
    static testMethod void clearAllQLITest() {
        GLSSetupData.createQLI();
        String url= 'https://c.cs11.visual.force.com/apex/GLSQW_ScopeOfServicesPage?id='+GLSSetupData.quote3.id;
        test.startTest();
       
        ApexPages.currentPage().getParameters().put('Url',url);
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote3.id);
        
        GLSQW_ScopeOfServicesController obj = new GLSQW_ScopeOfServicesController(new ApexPages.StandardController(GLSSetupData.quote3));
        obj.selectQAssgRecord = GLSSetupData.qAsst;
        obj.clearAllQLI();
        test.stopTest();  
    }
     static testMethod void updateONEQLITest() {
        GLSSetupData.createQLI();
        String url= 'https://c.cs11.visual.force.com/apex/GLSQW_ScopeOfServicesPage?id='+GLSSetupData.quote3.id;
        test.startTest();
       
        ApexPages.currentPage().getParameters().put('Url',url);
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote3.id);
        ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id);
        GLSQW_ScopeOfServicesController obj = new GLSQW_ScopeOfServicesController(new ApexPages.StandardController(GLSSetupData.quote3));
        obj.lstQLIAndCTM.add( new GLSQW_ScopeOfServicesController.WrapperForQuotlineItem(GLSSetupData.quoteLI1,'','','','',''));
        obj.updateONEQLI();
        test.stopTest();  
    }
    
    static testMethod void callAnalyseONEQLITest() {
        GLSSetupData.createQLI();
        String url= 'https://c.cs11.visual.force.com/apex/GLSQW_ScopeOfServicesPage?id='+GLSSetupData.quote3.id;
        test.startTest();
       
        ApexPages.currentPage().getParameters().put('Url',url);
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote3.id);
        
        GLSQW_ScopeOfServicesController obj = new GLSQW_ScopeOfServicesController(new ApexPages.StandardController(GLSSetupData.quote3));
        obj.callAnalyseONEQLI();
        test.stopTest();  
    }
    
   static testMethod void saveAllQLITest(){
        Account accObj                      = GLSLPW_SetUpData.createAccount('Amgen');
        Contact conObj                      = GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
        Program__c prgObj                   = GLSLPW_SetUpData.createProgram(accObj.Id,conObj.Id,'Program_Test', 'Amgen');
        Protocol__c protObj                 = GLSLPW_SetUpData.createProtocol(prgObj.Id, 'Open');
        Protocol__c protObj1                    = GLSLPW_SetUpData.createProtocol(prgObj.Id, 'Open');
        Country__c country                  = GLSLPW_SetUpData.createCountry('Test Country1');
        Protocol_Country__c pcObj           = GLSLPW_SetUpData.createProtocolCountry(protObj.Id, country.Id, 6, 'Active');
        Quote__c quoteObj                   = GLSLPW_SetUpData.createQuote(accObj.Id, conObj.Id, 'Draft');
        quoteObj.Protocol__c                = protObj.Id;
        quoteObj.Notarized__c               = true;
        quoteObj.Standard__c                = true;
        update quoteObj;
        Language_List_Item__c lliObj1       = GLSLPW_SetUpData.createLanguageListItem('Test Language 1');
        Language_List_Item__c lliObj2       = GLSLPW_SetUpData.createLanguageListItem('Test Language 2');
        Quote_Line_Item__c quoteLI          = GLSLPW_SetUpData.createQuoteLineItem(quoteObj.Id, country.Id, lliObj1.Id, lliObj2.Id);
        quoteLI.Process_Status__c           = 'In Process';
        update quoteLI;
        Quote_Assignment__c qaObj           = new Quote_Assignment__c(Quote__c = quoteObj.id);
        insert qaObj;
        String url= 'https://c.cs11.visual.force.com/apex/GLSQW_ScopeOfServicesPage?id='+quoteObj.id;
       
        test.startTest();
                ApexPages.currentPage().getParameters().put('Url',url);
                ApexPages.currentPage().getParameters().put('id',quoteObj.id);  
                ApexPages.currentPage().getParameters().put('qliId',quoteLI.id);
                GLSQW_ScopeOfServicesController obj = new GLSQW_ScopeOfServicesController(new ApexPages.StandardController(quoteObj));
                //obj.lstQLIAndCTM.add( new GLSQW_ScopeOfServicesController.WrapperForQuotlineItem(quoteLI,null,'','',null,''));
                obj.quoteID = quoteObj.Id;
                obj.updateONEQLI();
                obj.saveAllQLI();
                quoteObj.Protocol__c = protObj1.Id;
                update quoteObj;
                obj.saveAllQLI();
                obj.createCertificationQLLI();
                obj.qliDel();
                obj.refreshIsAnalysisComplete();
                obj.customDel();
        test.stopTest();
   }

}