/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLS_QWQuoteListViewControllerTest {

    static testMethod void testWithIsAllChecked() {
    	GLSSetupData.setupData();
    	test.startTest();
        	GLS_QWQuoteListViewController qlistViewController = new GLS_QWQuoteListViewController(new ApexPages.StandardController(GLSSetupData.quote));
        	qlistViewController.selectedQuoteId = GLSSetupData.quote.id;
        	qlistViewController.cloneQID = GLSSetupData.quote.id;
        	qlistViewController.callCloneMethod();
        	qlistViewController.editQuote();
        	qlistViewController.cancelQuote();
        	qlistViewController.decideRedirectPage();
        test.stopTest();
    }
    static testMethod void testWithoutIsAllChecked1() {
    	GLSSetupData.setupData();
    	test.startTest();
        	GLS_QWQuoteListViewController qlistViewController = new GLS_QWQuoteListViewController(new ApexPages.StandardController(GLSSetupData.quote));
        	qlistViewController.isAllChecked = false;
        	qlistViewController.accountVal = '';
        	qlistViewController.contactVal = '';
        	qlistViewController.StatusVal = '';
        	qlistViewController.ownerVal = '';
        	qlistViewController.startDate='';
        	qlistViewController.endDate='';        	
        	qlistViewController.searchQuote();
        	qlistViewController.cloneQID = GLSSetupData.quote.id;
        	qlistViewController.iscloneWithFiles = false;
        	qlistViewController.iscloneWithoutFiles = true;
        	qlistViewController.callCloneMethod();
        test.stopTest();
    }
    static testMethod void testWithoutIsAllChecked2() {
    	GLSSetupData.setupData();
    	test.startTest();
        	GLS_QWQuoteListViewController qlistViewController = new GLS_QWQuoteListViewController(new ApexPages.StandardController(GLSSetupData.quote));
        	qlistViewController.isAllChecked = false;
        	qlistViewController.accountVal = 'test';
        	qlistViewController.contactVal = '';
        	qlistViewController.StatusVal = '';
        	qlistViewController.ownerVal = '';
        	qlistViewController.searchQuote();
        test.stopTest();
    }
    static testMethod void testWithoutIsAllChecked3() {
    	GLSSetupData.setupData();
    	test.startTest();
        	GLS_QWQuoteListViewController qlistViewController = new GLS_QWQuoteListViewController(new ApexPages.StandardController(GLSSetupData.quote));
        	qlistViewController.isAllChecked = false;
        	qlistViewController.accountVal = '';
        	qlistViewController.contactVal = 'test';
        	qlistViewController.StatusVal = '';
        	qlistViewController.ownerVal = '';
        	qlistViewController.searchQuote();
        test.stopTest();
    }
    static testMethod void testWithoutIsAllChecked4() {
    	GLSSetupData.setupData();
    	test.startTest();
        	GLS_QWQuoteListViewController qlistViewController = new GLS_QWQuoteListViewController(new ApexPages.StandardController(GLSSetupData.quote));
        	qlistViewController.isAllChecked = false;
        	qlistViewController.accountVal = '';
        	qlistViewController.contactVal = '';
        	qlistViewController.StatusVal = 'test';
        	qlistViewController.ownerVal = '';
        	qlistViewController.searchQuote();
        test.stopTest();
    }
    static testMethod void testWithoutIsAllChecked5() {
    	GLSSetupData.setupData();
    	test.startTest();
        	GLS_QWQuoteListViewController qlistViewController = new GLS_QWQuoteListViewController(new ApexPages.StandardController(GLSSetupData.quote));
        	qlistViewController.isAllChecked = false;
        	qlistViewController.accountVal = '';
        	qlistViewController.contactVal = '';
        	qlistViewController.StatusVal = '';
        	qlistViewController.ownerVal = 'test';
        	qlistViewController.searchQuote();
        test.stopTest();
    }
    static testMethod void testWithoutIsAllChecked6() {
    	GLSSetupData.setupData();
    	test.startTest();
        	GLS_QWQuoteListViewController qlistViewController = new GLS_QWQuoteListViewController(new ApexPages.StandardController(GLSSetupData.quote));
        	qlistViewController.isAllChecked = false;
        	qlistViewController.accountVal = 'test';
        	qlistViewController.contactVal = 'test';
        	qlistViewController.StatusVal = '';
        	qlistViewController.ownerVal = '';
        	qlistViewController.searchQuote();
        test.stopTest();
    }
    static testMethod void testWithoutIsAllChecked7() {
    	GLSSetupData.setupData();
    	test.startTest();
        	GLS_QWQuoteListViewController qlistViewController = new GLS_QWQuoteListViewController(new ApexPages.StandardController(GLSSetupData.quote));
        	qlistViewController.isAllChecked = false;
        	qlistViewController.accountVal = 'test';
        	qlistViewController.contactVal = '';
        	qlistViewController.StatusVal = 'test';
        	qlistViewController.ownerVal = '';
        	qlistViewController.searchQuote();
        test.stopTest();
    }
    static testMethod void testWithoutIsAllChecked8() {
    	GLSSetupData.setupData();
    	test.startTest();
        	GLS_QWQuoteListViewController qlistViewController = new GLS_QWQuoteListViewController(new ApexPages.StandardController(GLSSetupData.quote));
        	qlistViewController.isAllChecked = false;
        	qlistViewController.accountVal = 'test';
        	qlistViewController.contactVal = '';
        	qlistViewController.StatusVal = '';
        	qlistViewController.ownerVal = 'test';
        	qlistViewController.searchQuote();
        test.stopTest();
    }
    static testMethod void testWithoutIsAllChecked9() {
    	GLSSetupData.setupData();
    	test.startTest();
        	GLS_QWQuoteListViewController qlistViewController = new GLS_QWQuoteListViewController(new ApexPages.StandardController(GLSSetupData.quote));
        	qlistViewController.isAllChecked = false;
        	qlistViewController.accountVal = '';
        	qlistViewController.contactVal = 'test';
        	qlistViewController.StatusVal = 'test';
        	qlistViewController.ownerVal = '';
        	qlistViewController.searchQuote();
        test.stopTest();
    }
    static testMethod void testWithoutIsAllChecked10() {
    	GLSSetupData.setupData();
    	test.startTest();
        	GLS_QWQuoteListViewController qlistViewController = new GLS_QWQuoteListViewController(new ApexPages.StandardController(GLSSetupData.quote));
        	qlistViewController.isAllChecked = false;
        	qlistViewController.accountVal = '';
        	qlistViewController.contactVal = 'test';
        	qlistViewController.StatusVal = '';
        	qlistViewController.ownerVal = 'test';
        	qlistViewController.searchQuote();
        test.stopTest();
    }
    static testMethod void testWithoutIsAllChecked11() {
    	GLSSetupData.setupData();
    	test.startTest();
        	GLS_QWQuoteListViewController qlistViewController = new GLS_QWQuoteListViewController(new ApexPages.StandardController(GLSSetupData.quote));
        	qlistViewController.isAllChecked = false;
        	qlistViewController.accountVal = '';
        	qlistViewController.contactVal = '';
        	qlistViewController.StatusVal = 'test';
        	qlistViewController.ownerVal = 'test';
        	qlistViewController.searchQuote();
        test.stopTest();
    }
    static testMethod void testWithoutIsAllChecked12() {
    	GLSSetupData.setupData();
    	test.startTest();
        	GLS_QWQuoteListViewController qlistViewController = new GLS_QWQuoteListViewController(new ApexPages.StandardController(GLSSetupData.quote));
        	qlistViewController.isAllChecked = false;
        	qlistViewController.accountVal = 'test';
        	qlistViewController.contactVal = 'test';
        	qlistViewController.StatusVal = 'test';
        	qlistViewController.ownerVal = '';
        	qlistViewController.searchQuote();
        test.stopTest();
    }
    static testMethod void testWithoutIsAllChecked13() {
    	GLSSetupData.setupData();
    	test.startTest();
        	GLS_QWQuoteListViewController qlistViewController = new GLS_QWQuoteListViewController(new ApexPages.StandardController(GLSSetupData.quote));
        	qlistViewController.isAllChecked = false;
        	qlistViewController.accountVal = '';
        	qlistViewController.contactVal = 'test';
        	qlistViewController.StatusVal = 'test';
        	qlistViewController.ownerVal = 'test';        	
        	qlistViewController.searchQuote();
        	
        	qlistViewController.selectedQID = GLSSetupData.quote.id;        	
        	Quote_PDF_File__c qPDFFile=new Quote_PDF_File__c();
        	qPDFFile.id = GLSSetupData.quotePDFFile1.id;    
        	upsert qPDFFile;
        	qlistViewController.fecthQuotePDFFile();
        test.stopTest();
    }
    static testMethod void testWithoutIsAllChecked14() {
    	GLSSetupData.setupData();
    	test.startTest();
        	GLS_QWQuoteListViewController qlistViewController = new GLS_QWQuoteListViewController(new ApexPages.StandardController(GLSSetupData.quote));
        	qlistViewController.isAllChecked = false;
        	qlistViewController.accountVal = 'test';
        	qlistViewController.contactVal = 'test';
        	qlistViewController.StatusVal = 'test';
        	qlistViewController.ownerVal = 'test';
        	qlistViewController.isCustomSearch='true';
        	qlistViewController.startDate='01/01/2013';
        	qlistViewController.endDate='01/01/2016';
        	qlistViewController.searchQuote();
        	qlistViewController.next();
        	qlistViewController.previous();
        	qlistViewController.LastPage();
        	qlistViewController.FirstPage();
        	Boolean getPrve=qlistViewController.getprev();
        	Boolean getnxt=qlistViewController.getnxt();
        	qlistViewController.selectedQuoteId = GLSSetupData.quote.id;    
        	qlistViewController.fecthQuotePDFFile();    	
        	qlistViewController.searchStr='In';
        	qlistViewController.searchInQuote();
        	qlistViewController.quoteObjUpdated.id = GLSSetupData.quote.id;
        	qlistViewController.selectedUserId = GLSSetupData.newUser.id;
        	qlistViewController.changeQuoteAssignee();
        test.stopTest();
    }
    /**
    * This method will create dummy credentials for testing 
    **/
    private static String createTestCredentials(){
        AWS_Credential__c testKey = new AWS_Credential__c(name='test keys',key__c='key',secret__c='secret');
        insert testKey;
        return testKey.name;
     }
    /**
    * This method run amazon's signature's and policy tester
    **/
    static testmethod void policyTest() {
            GLSSetupData.setupData();
            Test.startTest();
            //AWSFileUploadController uploadFile = new AWSFileUploadController();
            GLS_QWQuoteListViewController qlistViewController = new GLS_QWQuoteListViewController(new ApexPages.StandardController(GLSSetupData.quote));                        
            String credName = createTestCredentials();
            qlistViewController.AWSCredentialName = credName;
            qlistViewController.getawss3LoginCredentials();
            try{
                qlistViewController.getHexPolicy( );
                qlistViewController.getSignedPolicy();
            }catch(Exception ex){
            }
           system.assert( qlistViewController.getHexPolicy() != null );
           system.assert( qlistViewController.getSignedPolicy() != null );
           Test.stopTest(); 
     }
}