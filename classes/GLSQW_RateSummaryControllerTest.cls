/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 */
 
@isTest
private class GLSQW_RateSummaryControllerTest {  
      /**
       * This is Unit Test method
     */
    static testMethod void myUnitTest() {  
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('idVal',GLSSetupData.quoteLI1.id); 
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
           
            rateSummary.selectedServiceValue = 'Linguistic Validation';
            rateSummary.minStdChargeEnteredLabel = 'test';
            rateSummary.idVal = GLSSetupData.quoteSubline.id;
            rateSummary.qliVal = GLSSetupData.quoteLI1.id;
            rateSummary.qlliIdVal = GLSSetupData.qlli1.id;
            rateSummary.selectedValueServiceStd = 'Back Translation, No Match';
            rateSummary.qliStd = GLSSetupData.quoteLI1.id;
            rateSummary.minChargeEnteredLabel = 'Linguistic Validation';
            rateSummary.selectedValueServiceOut = 'DTP Recreation';
            rateSummary.qliOut = GLSSetupData.qlli1.id;
            rateSummary.quoteObjId = GLSSetupData.quote.id;
            rateSummary.quotePer = '10';
            rateSummary.quoteFilePprepPer = '10';
            rateSummary.standardQuantity = '10';
            rateSummary.quantityVal = '120';
            rateSummary.unitpriceVal = '150';
            rateSummary.deletewcQLIId = GLSSetupData.quoteLI1.id;
            rateSummary.additionalItemQuantity = '120';
            rateSummary.additionalItemUnitPrice = '10';
                                   
            rateSummary.getItems();
            rateSummary.selectedIndex = 0; 
            rateSummary.getlist();
            rateSummary.getPercent();
            rateSummary.getDiscount();
            rateSummary.getOutServicePercentage();
            rateSummary.getQuoteLineOutServicePercentage();
            rateSummary.newDiscountSection();
            rateSummary.newSelectedRateType();
            rateSummary.newAdditionalSectionService();
            rateSummary.newAdditionalDiscountSection();
            rateSummary.newAddSectionService();
            rateSummary.addUpdateOutsourcedQuoteLevelItems();
            rateSummary.addUpdateStandardQuoteLevelItems();
            rateSummary.UpdateSubline();
            rateSummary.UpdateFileSurchargeAmount();
            rateSummary.UpdateQuoteLine();
            rateSummary.deleteQlis();      
            rateSummary.deleteQlid();
            rateSummary.deleteQuoteWordCount();
            rateSummary.deleteQlli();
            rateSummary.deleteQd();
            rateSummary.minChargeEnteredLabel = '';
            rateSummary.addUpdateqlis();
            rateSummary.getOutSourceServicePercetageList();
           
        test.stopTest();
    }
    
    static testMethod void myUnitTest1() {  
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('idVal',GLSSetupData.quoteLI1.id); 
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
           
            rateSummary.selectedServiceValue = 'Linguistic Validation';
            rateSummary.minStdChargeEnteredLabel = 'test';
            rateSummary.idVal = GLSSetupData.quoteSubline.id;
            rateSummary.qliVal = GLSSetupData.quoteLI1.id;
            rateSummary.qlliIdVal = GLSSetupData.qlli1.id;
            rateSummary.selectedValueServiceStd = 'Back Translation, No Match';
            rateSummary.qliStd = GLSSetupData.quoteLI1.id;
            rateSummary.minChargeEnteredLabel = 'Linguistic Validation';
            rateSummary.selectedValueServiceOut = 'DTP Recreation';
            rateSummary.qliOut = GLSSetupData.qlli1.id;
            rateSummary.quoteObjId = GLSSetupData.quote.id;
            rateSummary.quotePer = '10';
            rateSummary.quoteFilePprepPer = '10';
            rateSummary.standardQuantity = '10';
            rateSummary.quantityVal = '120';
            rateSummary.unitpriceVal = '150';
            rateSummary.deletewcQLIId = GLSSetupData.quoteLI1.id;
            rateSummary.additionalItemQuantity = '120';
            rateSummary.additionalItemUnitPrice = '10';
                                   
            rateSummary.getItems();
            rateSummary.selectedIndex = 0; 
            rateSummary.minChargeEnteredLabel = '';
            rateSummary.addUpdateServiceqlis();
            rateSummary.updateDS();
            rateSummary.addDPqlis();
            rateSummary.addUpdateDiscount();
            rateSummary.addQuoteDiscount();
            rateSummary.changeQuoteStatus();           
        test.stopTest();
    }
    
    static testMethod void myUnitTest2() {  
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('idVal',GLSSetupData.quoteLI1.id); 
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
           
            rateSummary.selectedServiceValue = 'Linguistic Validation';
            rateSummary.minStdChargeEnteredLabel = 'test';
            rateSummary.idVal = GLSSetupData.quoteSubline.id;
            rateSummary.qliVal = GLSSetupData.quoteLI1.id;
            rateSummary.qlliIdVal = GLSSetupData.qlli1.id;
            rateSummary.selectedValueServiceStd = 'Back Translation, 0 - 74% Match';
            rateSummary.qliStd = GLSSetupData.quoteLI1.id;
            rateSummary.minChargeEnteredLabel = 'Linguistic Validation';
            rateSummary.selectedValueServiceOut = 'DTP Recreation';
            rateSummary.qliOut = GLSSetupData.qlli1.id;
            rateSummary.quoteObjId = GLSSetupData.quote.id;
            rateSummary.quotePer = '10';
            rateSummary.quoteFilePprepPer = '10';
            rateSummary.standardQuantity = '10';
            rateSummary.quantityVal = '120';
            rateSummary.unitpriceVal = '150';
            rateSummary.deletewcQLIId = GLSSetupData.quoteLI1.id;
            rateSummary.additionalItemQuantity = '120';
            rateSummary.additionalItemUnitPrice = '10';
                          
            rateSummary.getItems();
            rateSummary.selectedIndex = 0; 
            rateSummary.getlist();
            rateSummary.updateQuote();
            rateSummary.UpdateProMangAmount();
            rateSummary.UpdateFilePrepAmount();
            rateSummary.checkRender();
            rateSummary.getselectedOutSourcedPercentage();
            rateSummary.setselectedOutSourcedPercentage('60');
            rateSummary.getselectedQuoteOutSourcedPercentage();
            rateSummary.setselectedQuoteOutSourcedPercentage('60');
            rateSummary.createDeliverySchedule('Proof Reading, Hour(s)','10');
            rateSummary.UpdateExpiditedSurchargeAmount();
           
        test.stopTest();
    }
    static testMethod void addDiscountAtQLILevelTest1(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0');
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('quoteLineItemId',GLSSetupData.quoteLI1.id);
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.selectedIndex = 0;
            rateSummary.lstWrapperForQuotlineItem[rateSummary.selectedIndex].newQuoteLineItemDiscount = GLSSetupData.qliDisc1;
            rateSummary.getDiscountValue();
        test.stopTest();
    } 
    
    static testMethod void addDiscountAtQLILevelTest2(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0');
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('quoteLineItemId',GLSSetupData.quoteLI1.id);
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.selectedIndex = 0;
            rateSummary.lstWrapperForQuotlineItem[rateSummary.selectedIndex].newQuoteLineItemDiscount = GLSSetupData.qliDisc2;
            rateSummary.getDiscountValue();
        test.stopTest();
    } 
    
    static testMethod void addDiscountAtQLILevelTest3(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0');
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('quoteLineItemId',GLSSetupData.quoteLI1.id);
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.selectedIndex = 0;
            rateSummary.lstWrapperForQuotlineItem[rateSummary.selectedIndex].newQuoteLineItemDiscount = GLSSetupData.qliDisc3;
            rateSummary.getDiscountValue();
        test.stopTest();
    } 
    
    static testMethod void addDiscountAtQLILevelTest4(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0');
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('quoteLineItemId',GLSSetupData.quoteLI1.id);
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.selectedIndex = 0;
            rateSummary.lstWrapperForQuotlineItem[rateSummary.selectedIndex].newQuoteLineItemDiscount = GLSSetupData.qliDisc4;
            rateSummary.getDiscountValue();
        test.stopTest();
    } 
    
    static testMethod void addDiscountAtQLILevelTest5(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0');
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('quoteLineItemId',GLSSetupData.quoteLI1.id);
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.selectedIndex = 0;
            rateSummary.lstWrapperForQuotlineItem[rateSummary.selectedIndex].newQuoteLineItemDiscount = GLSSetupData.qliDisc5;
            rateSummary.getDiscountValue();
        test.stopTest();
    } 
    
    static testMethod void addDiscountAtQuoteLevelTest1(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0');
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('quoteLineItemId',GLSSetupData.quoteLI1.id);
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.lstWrapperForQuotlevellineItem[0].newQuoteDiscount = GLSSetupData.qd1;
            rateSummary.getDiscountValueQuoteLevel();
        test.stopTest();
    } 
    
    static testMethod void addDiscountAtQuoteLevelTest2(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0');
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('quoteLineItemId',GLSSetupData.quoteLI1.id);
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.lstWrapperForQuotlevellineItem[0].newQuoteDiscount = GLSSetupData.qd2;
            rateSummary.getDiscountValueQuoteLevel();
        test.stopTest();
    } 
    
    static testMethod void addDiscountAtQuoteLevelTest3(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0');
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('quoteLineItemId',GLSSetupData.quoteLI1.id);
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.lstWrapperForQuotlevellineItem[0].newQuoteDiscount = GLSSetupData.qd3;
            rateSummary.getDiscountValueQuoteLevel();
        test.stopTest();
    } 
   
    static testMethod void addDiscountAtQuoteLevelTest4(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0');
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('quoteLineItemId',GLSSetupData.quoteLI1.id);
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.lstWrapperForQuotlevellineItem[0].newQuoteDiscount = GLSSetupData.qd4;
            rateSummary.getDiscountValueQuoteLevel();
        test.stopTest();
    } 
    
    static testMethod void addDiscountAtQuoteLevelTest5(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0');
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('quoteLineItemId',GLSSetupData.quoteLI1.id);
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.lstWrapperForQuotlevellineItem[0].newQuoteDiscount = GLSSetupData.qd5;
            rateSummary.getDiscountValueQuoteLevel();
        test.stopTest();
    } 
    
    static testMethod void newAdditionalLineItemSectionTest() {
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('quoteLineItemId',GLSSetupData.quoteLI1.id);
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.lstWrapperForQuotlineItem[0].wrapperQLIObj.Additional_Line_Items__c = 'Back Translation, No Match';
            rateSummary.newAdditionalLineItemSection(); 
            
            rateSummary.lstWrapperForQuotlineItem[0].wrapperQLIObj.Additional_Line_Items__c = 'Translation Edit';
            rateSummary.newAdditionalLineItemSection(); 
            
            rateSummary.lstWrapperForQuotlineItem[0].wrapperQLIObj.Additional_Line_Items__c = 'Cognitive Debriefing';
            rateSummary.newAdditionalLineItemSection(); 
            
            rateSummary.lstWrapperForQuotlineItem[0].wrapperQLIObj.Additional_Line_Items__c = '2nd Editorial Review';
            rateSummary.newAdditionalLineItemSection();
            
            rateSummary.lstWrapperForQuotlineItem[0].wrapperQLIObj.Additional_Line_Items__c = '0 - 74% Match';
            rateSummary.newAdditionalLineItemSection();
        test.stopTest();
        
     }
        
    static testMethod void newAdditionalSectionTest1(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.newAdditionalSection();
        test.stopTest();
    } 
    
    static testMethod void newAdditionalSectionTest2(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.lstWrapperForQuotlevellineItem[0].wrapperQLLIObj.Standard_Service__c = 'Minimum Charge';
            rateSummary.newAdditionalSection();
        test.stopTest();
    }
    
    static testMethod void newAdditionalLineItemSectionTest2(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.lstWrapperForQuotlineItem[0].wrapperQLIObj.Additional_Line_Items__c = 'Minimum Charge';
            rateSummary.newAdditionalLineItemSection();
        test.stopTest();
    }
    
    static testMethod void newAdditionalLineItemSectionTest3(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('quoteLineItemId',GLSSetupData.quoteLI1.id);
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.lstWrapperForQuotlineItem[0].wrapperQLIObj.Additional_Line_Items__c = 'Hour(s), Desktop Publishing';
            rateSummary.newAdditionalLineItemSection();
        test.stopTest();
    }
    
    static testMethod void newAdditionalLineItemSectionTest4() {
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI3.id); 
            ApexPages.currentPage().getParameters().put('quoteLineItemId',GLSSetupData.quoteLI3.id);
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.lstWrapperForQuotlineItem[0].wrapperQLIObj.Additional_Line_Items__c = 'Translation Edit/Localization';
            rateSummary.newAdditionalLineItemSection(); 
            
           // rateSummary.lstWrapperForQuotlineItem[0].wrapperQLIObj.Additional_Line_Items__c = 'Cognitive Debriefing';
           // rateSummary.newAdditionalLineItemSection(); 
        test.stopTest();
        
     }
    
    
    static testMethod void addUpdateOutsourcedQuoteLevelItemsTest(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('quoteLineItemId',GLSSetupData.quoteLI1.id);
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.selectedServiceValue = 'DTP Recreation'; 
            rateSummary.outSourcedQuantity = '10';
            rateSummary.addUpdateOutsourcedQuoteLevelItems();
        test.stopTest();
    }
    
    static testMethod void addUpdateqlisTest1(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('valFor','test'); 
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('isChanged','true');
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.qliStd = GLSSetupData.quoteLI1.id;
            rateSummary.additionalItemQuantity = '12';
            rateSummary.selectedValueServiceStd = 'Desktop Publishing';
            rateSummary.minChargeEnteredLabel = '';
            rateSummary.addUpdateqlis(); 
        test.stopTest();
    }
    static testMethod void addUpdateqlisTest2(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('isChanged','true');
            ApexPages.currentPage().getParameters().put('valFor','test'); 
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.additionalItemQuantity = '12';
            rateSummary.qliStd = GLSSetupData.quoteLI1.id;
            rateSummary.selectedValueServiceStd = 'Hour(s), Proofreading (Only)';
            rateSummary.minChargeEnteredLabel = '';
            rateSummary.addUpdateqlis(); 
        test.stopTest();
    }
    static testMethod void addUpdateqlisTest3(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('isChanged','true');
            ApexPages.currentPage().getParameters().put('valFor','test'); 
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.additionalItemQuantity = '12';
            rateSummary.qliStd = GLSSetupData.quoteLI1.id;
            rateSummary.selectedValueServiceStd = 'Hour(s), Equivalency Statement(s)';
            rateSummary.minChargeEnteredLabel = '';
            rateSummary.addUpdateqlis(); 
        test.stopTest();
    }
    static testMethod void addUpdateqlisTest4(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('isChanged','true');
            ApexPages.currentPage().getParameters().put('valFor','test'); 
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.additionalItemQuantity = '12';
            rateSummary.qliStd = GLSSetupData.quoteLI1.id;
            rateSummary.selectedValueServiceStd = 'Hour(s), 2nd Editorial Review';
            rateSummary.minChargeEnteredLabel = '';
            rateSummary.addUpdateqlis(); 
        test.stopTest();
    }
    static testMethod void addUpdateqlisTest5(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('isChanged','true');
            ApexPages.currentPage().getParameters().put('valFor','test'); 
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.additionalItemQuantity = '12';
            rateSummary.qliStd = GLSSetupData.quoteLI1.id;
            rateSummary.selectedValueServiceStd = 'Certification (Standard)';
            rateSummary.minChargeEnteredLabel = '';
            rateSummary.addUpdateqlis(); 
        test.stopTest();
    }        
    static testMethod void addUpdateqlisTest6(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('isChanged','true');
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.additionalItemQuantity = '12';
            rateSummary.qliStd = GLSSetupData.quoteLI1.id;
            rateSummary.selectedValueServiceStd = 'Certification (Standard)';
            rateSummary.minChargeEnteredLabel = 'Back Translation, Minimum Charge';
            rateSummary.addUpdateqlis();      
        test.stopTest();
    }
    static testMethod void refreshAllQliTest1() {
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('isChanged','true');
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.quotetLineItemIdForApplyAll=GLSSetupData.quoteLI1.id;
            rateSummary.lstWrapperForQuotlineItem[0].wrapperQLIObj.Additional_Line_Items__c = 'Translation Edit';
            rateSummary.lstWrapperForQuotlineItem[0].wrapperQLIObj.Additional_Line_Items__c = '2nd Editorial Review';
            rateSummary.selectedIndexValue='1';
            rateSummary.refreshAllQli(); 
        
         ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI2.id); 
            ApexPages.currentPage().getParameters().put('isChanged','true');
            rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.quotetLineItemIdForApplyAll=GLSSetupData.quoteLI2.id;
            rateSummary.lstWrapperForQuotlineItem[0].wrapperQLIObj.Additional_Line_Items__c = 'Translation Edit';
            rateSummary.lstWrapperForQuotlineItem[0].wrapperQLIObj.Additional_Line_Items__c = '2nd Editorial Review';
            rateSummary.selectedIndexValue='1';
            rateSummary.refreshAllQli();
        test.stopTest();
        
    }
     
    static testMethod void refreshQliTest() {
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
        	ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI1.id); 
            ApexPages.currentPage().getParameters().put('quoteLineItemId',GLSSetupData.quoteLI1.id);
            ApexPages.currentPage().getParameters().put('isChanged','true'); 
            GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.selectedIndexValue='1';
            rateSummary.lstWrapperForQuotlineItem[0].wrapperQLIObj.Additional_Line_Items__c = 'Translation Edit';
            rateSummary.lstWrapperForQuotlineItem[0].wrapperQLIObj.Additional_Line_Items__c = '2nd Editorial Review';
            rateSummary.qliStd = GLSSetupData.quoteLI1.id;
            rateSummary.refreshQli(); 
        
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
            ApexPages.currentPage().getParameters().put('selectedValue','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
            ApexPages.currentPage().getParameters().put('selectedIndex','0'); 
        	ApexPages.currentPage().getParameters().put('qliId',GLSSetupData.quoteLI2.id); 
            ApexPages.currentPage().getParameters().put('quoteLineItemId',GLSSetupData.quoteLI2.id);
            ApexPages.currentPage().getParameters().put('isChanged','true'); 
            rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
            rateSummary.selectedIndexValue='1';
            rateSummary.lstWrapperForQuotlineItem[0].wrapperQLIObj.Additional_Line_Items__c = 'Translation Edit';
            rateSummary.lstWrapperForQuotlineItem[0].wrapperQLIObj.Additional_Line_Items__c = '2nd Editorial Review';
            rateSummary.qliStd = GLSSetupData.quoteLI2.id;
            rateSummary.refreshQli();
        test.stopTest();
     }
     
     static testMethod void updateOutSourcedSubline(){
     	GLSSetupData.createQLI();
     	test.startTest();
     	ApexPages.currentPage().getParameters().put('idVal',GLSSetupData.quoteLI1.id); 
     	GLSQW_RateSummaryController rateSummary= new GLSQW_RateSummaryController(new ApexPages.StandardController(GLSSetupData.quote));
     	rateSummary.idVal = GLSSetupData.quoteSubline.id;
     	rateSummary.quantityVal = '120';
     	rateSummary.unitpriceVal = '150';
     	rateSummary.percentageVal = '25';
     	rateSummary.updateOutSourcedSubline();
     	rateSummary.dummy();
     	test.stopTest();
     }
}