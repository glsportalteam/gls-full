/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSLPW_EstimateApprovalController Class
* Function: This class is used as a controller for the GLSLPW_EstimateApprovalPage.
*/
public with sharing class GLSLPW_EstimateApprovalController {      
    public String estimateId  {get;set;}
    public list<Estimate_Line_Item__c> lstEstLineItems  {get;set;}  
    public String masterEstimateId     {get;set;}
    public List<EstimateLIWrapper> lstEstLineItemWrapper  {get;set;}
    List<Program_Files__c> lstProgrmFiles {get;set;}
    List<Estimate_Files__c> lstEstFile   {get;set;}
    public Map<String,String> mapEstLineItemProtocol {get;set;}
    public Map<String,List<Estimate_Line_Country__c>> mapEstLItemIdCountry {get;set;}
    public Map<String,List<Protocol_Country__c>> mapEstProtocolIdCountry {get;set;}
    public string estProgramId {get;set;}
    public List<SelectOption> getProtocolStatus() { 
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(GLSConfig.Pending,GLSConfig.Pending));
        options.add(new SelectOption(GLSConfig.Open,GLSConfig.Open));        
        return options;
    }
        
    public GLSLPW_EstimateApprovalController (){
        EstimateId = ApexPages.currentPage().getParameters().get('id');             
        getEstLineItemInfo();                   
    }        
    
    String prgmFilePath;
    /*
        @param: none
        @description : This method fecth all the estimate line item records which needs to be shown on approval page
    */
    public void getEstLineItemInfo(){  
        prgmFilePath='';              
        mapEstLineItemProtocol = new Map<String,String>();      
        lstEstLineItemWrapper = new List<EstimateLIWrapper>();      
        if(EstimateId != null && EstimateId != ''){
            List<Estimate__c> lstEstimate = [select id,(select id,Name,Protocol_Id__c,Protocol_Id__r.Name,Protocol_Id__r.Status__c,Protocol_Id__r.Approved_Amount__c,Amount__c from Estimate_Line_Items__r),Original_Estimate__c,Program_Id__c from Estimate__c where id=:EstimateId limit 1];            
            if(lstEstimate != null && lstEstimate.size() > 0){
                masterEstimateId = lstEstimate[0].Original_Estimate__c;
                lstEstLineItems = lstEstimate[0].Estimate_Line_Items__r;    
                estProgramId =  lstEstimate[0].Program_Id__c;       
                if(lstEstLineItems != null && lstEstLineItems.size() > 0){
                    for(Estimate_Line_Item__c estLineItem : lstEstLineItems){   
                        if(estLineItem.Protocol_Id__c!=null){                   
                            mapEstLineItemProtocol.put(estLineItem.id,estLineItem.Protocol_Id__c);                                                                      
                            EstimateLIWrapper ELIWrapperObj = new EstimateLIWrapper();                      
                            Protocol__c EstProObj = new Protocol__c();
                            EstProObj.id = estLineItem.Protocol_Id__c;                      
                            EstProObj.Status__c = estLineItem.Protocol_Id__r.Status__c;
                            EstProObj.Approved_Amount__c = estLineItem.Protocol_Id__r.Approved_Amount__c;                                               
                            ELIWrapperObj.EstLineItem = estLineItem;
                            ELIWrapperObj.EstProtocol = EstProObj;
                            ELIWrapperObj.ProName =  estLineItem.Protocol_Id__r.Name;                       
                            lstEstLineItemWrapper.add(ELIWrapperObj);                               
                        }               
                    }
                } 
            }
        }
    }
    
    /*
    @param: None
    @Description: 
        Method will update all the estimate line items against the estimate Id passed in the URL with open staus,
        Update all protocol with the status selected by the user in the protocol status column in user,
        Update all other estimate related with parent estimate record with Obsolete Status,
        Update current estimate with Approved status.
    */
    public void approveEstimate(){                                    
            Estimate__c estimate = new Estimate__c();
            if(EstimateId!=null && EstimateId!=''){
                estimate.status__c=GLSConfig.Approved;
                estimate.id = EstimateId;
            }               
            //Below list is for updating Protocol with the status Selected by the user. 
            Set<Protocol__c> setUpdateProtocol = new Set<Protocol__c>();                                  
            if(lstEstLineItemWrapper != null && lstEstLineItemWrapper.size() > 0){
                for(EstimateLIWrapper ELIWrapper:lstEstLineItemWrapper){                    
                    setUpdateProtocol.add(ELIWrapper.EstProtocol);
                }
            }
            
            Program__c estProgram;
            if(estProgramId != null && estProgramId != ''){
                estProgram = new Program__c();
                estProgram.id = estProgramId;
                estProgram.Status__c = GLSConfig.Open;          
            }
            mapEstLItemIdCountry = new Map<String,List<Estimate_Line_Country__c>>();
            mapEstProtocolIdCountry = new Map<String,List<Protocol_Country__c>>();
            
            List<Estimate_Line_Item__c> lstEstLineItem;
            if(mapEstLineItemProtocol.keySet() != null && mapEstLineItemProtocol.size() > 0){ 
                lstEstLineItem = [select id,(select id,Country_Id__c,Number_of_Sites__c from Estimate_Line_Country__r) from Estimate_Line_Item__c where Id IN:mapEstLineItemProtocol.keySet()];
            }
            if(lstEstLineItem != null && lstEstLineItem.size() > 0){
                for(Estimate_Line_Item__c estLineItem:lstEstLineItem){
                    mapEstLItemIdCountry.put(estLineItem.id,estLineItem.Estimate_Line_Country__r);
                }
            }
            
            List<Protocol__c> lstprotocol;
            if(mapEstLineItemProtocol.values() != null && mapEstLineItemProtocol.values().size() > 0){
                lstprotocol = [select id,(select id,Country_Id__c,Number_of_Sites__c from Protocol_Countries__r) from Protocol__c where id IN:mapEstLineItemProtocol.values()];
            }
            if(lstprotocol != null && lstprotocol.size() > 0){
                for(Protocol__c pro : lstprotocol){
                    mapEstProtocolIdCountry.put(pro.id,pro.Protocol_Countries__r);
                }
            }
            List<Protocol_Country__c> newProtocolCountries = new List<Protocol_Country__c>();                          
            Map<String,Decimal> mapProtocolSite = new Map<String,Decimal>();
            List<Protocol_Country__c> oldProtocolCountries = new List<Protocol_Country__c>(); 
            for(String lid : mapEstLItemIdCountry.keySet()){                
                List<Estimate_Line_Country__c> lstLIC = mapEstLItemIdCountry.get(lid);
                List<Protocol_Country__c> lstPrC = mapEstProtocolIdCountry.get(mapEstLineItemProtocol.get(lid));                 
                Boolean isCountryExist;
                if(lstLIC != null && lstLIC.size() > 0){   
                    for(Estimate_Line_Country__c liCountry : lstLIC){
                        isCountryExist = false;
                        
                        for(Protocol_Country__c psCountry : lstPrC){
                            if(liCountry.Country_Id__c == psCountry.Country_Id__c){                             
                                isCountryExist=true;                               
                                if(liCountry.Number_of_Sites__c != null){
                                    if(mapProtocolSite.containsKey(psCountry.id)){
                                        Decimal numSites=0;
                                        mapProtocolSite.put(psCountry.id,mapProtocolSite.get(psCountry.id) + liCountry.Number_of_Sites__c);
                                    }else{
                                        if(psCountry.Number_of_Sites__c != null){
                                            mapProtocolSite.put(psCountry.id,psCountry.Number_of_Sites__c + liCountry.Number_of_Sites__c);
                                        }else{
                                            mapProtocolSite.put(psCountry.id,liCountry.Number_of_Sites__c);
                                        }
                                    }         
                                }
                                break;
                            } 
                        }
                        if(!isCountryExist){
                            Protocol_Country__c pCountry = new Protocol_Country__c();
                            pCountry.Country_Id__c= liCountry.Country_Id__c;
                            pCountry.Number_of_Sites__c = liCountry.Number_of_Sites__c;
                            pCountry.Protocol_Id__c = mapEstLineItemProtocol.get(lid);
                            newProtocolCountries.add(pCountry);                                                  
                        }
                    }                                                                                                                                                                   
                }                                                               
            }            
            if(mapProtocolSite != null && mapProtocolSite.size() > 0){
                for(String pid : mapProtocolSite.keySet()){
                    Protocol_Country__c pCountry = new Protocol_Country__c();
                    pCountry.id = pid;                                                                    
                    pCountry.Number_of_Sites__c = mapProtocolSite.get(pid);                                 
                    oldProtocolCountries.add(pCountry);
                }  
            }            
            Savepoint sp;                   
            try{
                sp = Database.setSavepoint();               
                if(newProtocolCountries!=null && newProtocolCountries.size()>0){
                    insert newProtocolCountries;
                }               
                if(oldProtocolCountries != null && oldProtocolCountries.size()>0){
                    update oldProtocolCountries;
                }
                List<Protocol__c> lstProtocols = new List<Protocol__c>();
                if(setUpdateProtocol!=null && setUpdateProtocol.size()>0)
                    lstProtocols.addAll(setUpdateProtocol);
                    
                if(lstProtocols!=null && lstProtocols.size()>0)
                    update lstProtocols;     
                                              
                update estProgram;
                
                if(EstimateId!=null && EstimateId!='')
                    update estimate;
                
                copyReferenceFilesToProgramFiles(EstimateId,estProgramId);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                Set<Id> setBDMUserId =  new Set<Id>();
                List<String> setToAddress = new List<String>();
                setToAddress.add('contracts@globallanguages.com');    
                String userType = Schema.SObjectType.User.getKeyPrefix();   
                for (GroupMember m : [Select Id, UserOrGroupId From GroupMember Where  group.name = :GLSConfig.BDMGroupName]){
                    // If the user or group id is a user
                    if (((String)m.UserOrGroupId).startsWith(userType)){                        
                        setBDMUserId.add(m.UserOrGroupId);
                    }               
                }
                system.debug(setBDMUserId+'setBDMUserId------------------------');
                if(setBDMUserId != null && setBDMUserId.size() > 0){
                    for(User u : [SELECT id,Email FROM user where id IN:setBDMUserId]){
                            if(u.email != null){
                                setToAddress.add(u.email);
                            }   
                    }    
                }
                system.debug(setToAddress+'setToAddress--------------------------');  
                mail.setTargetObjectId(UserInfo.getUserId());
                mail.setWhatId(EstimateId);                
                if(setToAddress!=null && setToAddress.size()>0)
                    mail.setToAddresses(setToAddress);   
                mail.setSaveAsActivity(false);                                                                                       
                Id templateId = [SELECT e.Id, e.DeveloperName 
                                FROM EmailTemplate e 
                                WHERE e.DeveloperName = 'Estimate_Approved_Template'
                                LIMIT 1].Id;
                if(templateId != null){
                    mail.setTemplateId(templateId);
                }
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });                                
            }catch(DMLException ex){                
                ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
                ApexPages.addMessage(Msg);
                Database.rollback(sp);
            }catch(Exception e){                
                ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
                ApexPages.addMessage(Msg);
                Database.rollback(sp);
            }                                                   
    }   
    
    /*
    @param: None
    @Description: 
        Method will create the refrence and estimate file records in salesforce 
    */
    public void copyReferenceFilesToProgramFiles(String estId, String prgmId){                      
        if(estId != null && estId != '' && prgmId != null && prgmId != ''){
            
            List<String> lstEstimateFileNames = new List<String>();  
            lstProgrmFiles = new List<Program_Files__c>();
            List<Program_Files__c> lstNewProgramFiles = new List<Program_Files__c>(); 
            List<Program_Files__c> lstExistingProgramFile;
                                                
            List<Program__c> lstPrograms = [select id,Contact__r.Account.Name,Name from Program__c where id=:prgmId limit 1];                        
            if(lstPrograms !=null && lstPrograms.size()>0){
                String pId = lstPrograms[0].id;
                pId = pId.subString(0,15);              
                prgmFilePath = lstPrograms[0].Contact__r.Account.Name+'/'+ lstPrograms[0].Name+'_'+pId+'/Program_File/'; 
            }             
                                      
            lstEstFile = [select id,AWSFilePath__c,Estimate_Id__c,AWS_File_Id__c,File_Type__c,Estimate_File_Name__c from Estimate_Files__c where Estimate_Id__c=:estId and File_Type__c=:GLSConfig.EstFileTyperef];
            
            if(lstEstFile != null && lstEstFile.size() > 0){                    
                for(Estimate_Files__c estFile : lstEstFile){
                    lstEstimateFileNames.add(estFile.Estimate_File_Name__c);                                          
                }
                
                if(lstEstimateFileNames != null && lstEstimateFileNames.size() > 0 && prgmFilePath != null && prgmFilePath != ''){
                    lstExistingProgramFile = [select id,AWSFilePath__c,Program_File_Name__c from Program_Files__c where AWSFilePath__c=:prgmFilePath and Program_File_Name__c IN:lstEstimateFileNames];
                    if(lstExistingProgramFile != null && lstExistingProgramFile.size() > 0){                        
                        for(Estimate_Files__c estFile : lstEstFile){
                            Boolean isExistingFile = false;                     
                            for(Program_Files__c extPrgmFileName : lstExistingProgramFile){
                                if(estFile.Estimate_File_Name__c == extPrgmFileName.Program_File_Name__c){
                                    isExistingFile = true;
                                    break;
                                }                                                
                            }   
                            if(isExistingFile == false){
                                Program_Files__c pFile = new Program_Files__c();
                                pFile.Program_Id__c = prgmId;
                                pFile.Program_File_Name__c = estFile.Estimate_File_Name__c;
                                lstNewProgramFiles.add(pFile);
                            }  
                        }                       
                        try{
                            if(lstNewProgramFiles != null && lstNewProgramFiles.size() > 0)
                                insert lstNewProgramFiles;
                        }catch(DMLException ex){
                            ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
                            ApexPages.addMessage(Msg);              
                        }        
                        lstProgrmFiles.addAll(lstExistingProgramFile);                                                                      
                    }else{
                        for(Estimate_Files__c estFile : lstEstFile){                            
                            Program_Files__c pFile = new Program_Files__c();
                            pFile.Program_Id__c = prgmId;
                            pFile.Program_File_Name__c = estFile.Estimate_File_Name__c;
                            lstNewProgramFiles.add(pFile);                           
                        }
                        try{
                            if(lstNewProgramFiles != null && lstNewProgramFiles.size() > 0)
                                insert lstNewProgramFiles;
                        }catch(DMLException ex){
                            ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
                            ApexPages.addMessage(Msg);              
                        }
                    }
                    if(lstNewProgramFiles != null && lstNewProgramFiles.size()>0){
                        lstProgrmFiles.addAll(lstNewProgramFiles);
                    }   
                }               
            }                                                           
        }                       
    }
    /*
    @param: None
    @Description: 
        Method will Copy all refrence file to program files by making a copy object amazon callout.
    */
    public PageReference createProgramFileOnS3(){
        if((lstProgrmFiles != null && lstProgrmFiles.size() > 0) && (lstEstFile != null && lstEstFile.size() > 0)){
            GLSQW_CloneFileController.copyProgramFiles(lstProgrmFiles,lstEstFile,prgmFilePath);       
        }
        PageReference estref =new pageReference('/apex/GLSLPW_EstimateDetailPage?id='+EstimateId);
        return estref;
    }
    
    /*
    @param: None
    @Description: 
        Method will redirect back user to GLSPW_EstimateDetailPage from where user clicks approved button. 
    */
    public PageReference cancel(){
        PageReference estref =new pageReference('/apex/GLSLPW_EstimateDetailPage?id='+EstimateId);
        return estref;
    }
    
    public class EstimateLIWrapper{
        public Estimate_Line_Item__c EstLineItem {get;set;}
        public Protocol__c EstProtocol  {get;set;}
        public String ProName  {get;set;}
    }
}