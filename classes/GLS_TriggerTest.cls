/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts. 
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLS_TriggerTest{
    
    static testMethod void testMethod1() {
        Rate_Sheet__c rateSheet= new Rate_Sheet__c();
        rateSheet.Name='Standard';
        insert rateSheet;
        
        Rate_Sheet_Item__c rsi= new Rate_Sheet_Item__c();
        rsi.Rate_Sheet__c=rateSheet.Id;
        insert rsi;
        
        Rate_Sheet_Subitem__c rssi = new Rate_Sheet_Subitem__c();
        rssi.Description__c='No Match';
        rssi.Rate_Sheet_Item__c=rsi.Id;
        
        rssi.Trados_Categories__c='New, 50% - 74%, 75% - 84%, 85% - 94%, 95% - 99%';
        insert rssi;
        
         rssi.Trados_Categories__c='New, 50% - 74%, 85% - 94%, 95% - 99%';
        update rssi;
        
        Test.startTest();
        Database.UpsertResult result = Database.upsert(rssi, false);
        Test.stopTest();
        System.assert(result.isSuccess());
    }
    
    static testMethod void DeleteRateSheetsubItemTrigger() {
        Rate_Sheet__c rateSheet= new Rate_Sheet__c();
        rateSheet.Name='Standard';
        insert rateSheet;
        
        Account_Rate_Sheet__c accRS = new Account_Rate_Sheet__c();
        accRS.Rate_Sheet__c = rateSheet.Id;
        insert accRS;
        
        Rate_Sheet_Item__c rsi= new Rate_Sheet_Item__c();
        rsi.Rate_Sheet__c=rateSheet.Id;
        insert rsi;
        
        Rate_Sheet_Subitem__c rssi = new Rate_Sheet_Subitem__c();
        rssi.Description__c='No Match';
        rssi.Rate_Sheet_Item__c=rsi.Id;
        insert rssi;
        
        Sub_Item_Trados_Category__c SICategory = new Sub_Item_Trados_Category__c();
        SICategory.Rate_Sheet_Subitem__c = rssi.Id;
        insert SICategory;
        
        Test.startTest();
        Database.DeleteResult result = Database.Delete(rateSheet, false);
        Test.stopTest();
        System.assert(result.isSuccess());
    }
   
   static testMethod void testMethod2()
   {
      Account accountObj = new Account();
        accountObj.Name = 'Amgen';
        insert accountObj;
    
       Contact contactObj = new Contact();
        contactObj.FirstName = 'Jennifer A';
        contactObj.LastName = 'Jennifer A';
        contactObj.AccountId = accountObj.Id;
        insert contactObj;

       Quote__c quote1 = new Quote__c();
        quote1.Quote_Name__c = 'Test Quote';
        quote1.Client__c = accountObj.Id;
        quote1.Contact__c = contactObj.Id;
        quote1.Terms__c = 'Test Terms';
        quote1.Status__c='Released';
       insert quote1;
       
       RFQ_Terms_Table__c rfqtt = new RFQ_Terms_Table__c();
       rfqtt.RFQ_Id__c= quote1.Id;
       rfqtt.RFQ_Term_Name__c='Instruction';
       rfqtt.RFQ_Term_Description__c='Test';
       insert rfqtt;
       
       RFQ_Terms_Table__c rfqttUpd = new RFQ_Terms_Table__c(Id = rfqtt.Id);
       rfqttUpd.RFQ_Term_Description__c = 'Test New';
       rfqttUpd.RFQ_Term_Order__c = 2;
       update rfqttUpd;
       
       Test.startTest();
       Database.UpsertResult result = Database.upsert(rfqtt, false);
       Test.stopTest();
       System.assert(result.isSuccess());
   }
    static testMethod void testMethod3()
   {
        Account accountObj = new Account();
        accountObj.Name = 'Amgen';
        insert accountObj;
    
       Contact contactObj = new Contact();
        contactObj.FirstName = 'Jennifer A';
        contactObj.LastName = 'Jennifer A';
        contactObj.AccountId = accountObj.Id;
        insert contactObj;

       Quote__c quote1 = new Quote__c();
        quote1.Quote_Name__c = 'Test Quote';
        quote1.Client__c = accountObj.Id;
        quote1.Contact__c = contactObj.Id;
     
        quote1.Status__c='Released';
       insert quote1;
       
       Standard_Special_Provisions__c ssp =new Standard_Special_Provisions__c();
       ssp.Special_Provision_Name__c='test1';
       ssp.Special_Provision_Text__c='test2';
       insert ssp;
       
       Standard_Special_Provisions__c ssp2 =new Standard_Special_Provisions__c();
       ssp2.Special_Provision_Name__c='test12';
       ssp2.Special_Provision_Text__c='test22';
       insert ssp2;
       
       RFQ_Special_Provisions__c rsp=new RFQ_Special_Provisions__c();
       rsp.RFQ_Id__c=quote1.Id;
       rsp.Standard_Special_Provisions__c=ssp.Id;
       rsp.RFQ_Special_Provision_Name__c='test3';
       rsp.RFQ_Special_Provision_Text__c='test4';
       insert rsp;
       
       RFQ_Special_Provisions__c Updrsp = new RFQ_Special_Provisions__c(Id = rsp.Id);
       Updrsp.Standard_Special_Provisions__c = ssp2.Id;
       Updrsp.RFQ_Special_Provision_Name__c='test31';
       Updrsp.RFQ_Special_Provision_Text__c='test41';
       update Updrsp;
       
       Test.startTest();
        Database.UpsertResult result = Database.upsert(rsp,false);
        Test.stopTest();
        System.assert(result.isSuccess());
   } 
  static testMethod void testMethod4()
   {
      list<Client_Service__c> lstClientService = new list<Client_Service__c>();
      Client_Service__c service = new Client_Service__c();
      Client_Service__c btservice = new Client_Service__c();
      Client_Service__c hoursService = new Client_Service__c();
      Client_Service__c outsourcedService = new Client_Service__c();
      Client_Service__c twcService = new Client_Service__c();
      Client_Service__c type5Service = new Client_Service__c();
     List<Quote_Line_Service__c> lstQLIServices = new List<Quote_Line_Service__c>();
       
     Quote_Line_Service__c qliService1 = new Quote_Line_Service__c();
     Quote_Line_Service__c qliService2 = new Quote_Line_Service__c();
     Quote_Line_Service__c qliService3 = new Quote_Line_Service__c();
     Quote_Line_Service__c qliService4 = new Quote_Line_Service__c();
     Quote_Line_Service__c qliService5 = new Quote_Line_Service__c();
     Quote_Line_Service__c qliService6 = new Quote_Line_Service__c();
       list<Sub_Item_Trados_Category__c> lstsitc = new list<Sub_Item_Trados_Category__c>(); 
       
       
       Account accountObj = new Account();
        accountObj.Name = 'Amgen';
        insert accountObj;
    
       Contact contactObj = new Contact();
        contactObj.FirstName = 'Jennifer A';
        contactObj.LastName = 'Jennifer A';
        contactObj.AccountId = accountObj.Id;
        insert contactObj;

  
       Rate_Sheet__c rateSheet2= new Rate_Sheet__c();
        rateSheet2.Name='Standard';
        insert rateSheet2;
        
       Quote__c quote = new Quote__c();
        quote.Quote_Name__c = 'Test Quote';
        quote.Client__c = accountObj.Id;
        quote.Status__c='In Preparation';
        quote.Contact__c = contactObj.Id;
        quote.Certified__c=false;
        quote.Rate_Sheet__c =  rateSheet2.id;
       insert quote;
       
        Language_List_Item__c sLanguage = new Language_List_Item__c();
        sLanguage.Name = 'English (United States)';
        sLanguage.Language_Name__c = 'en-US';
        insert sLanguage;
       
        Language_List_Item__c tLanguage = new Language_List_Item__c();
        tLanguage.Name = 'Arabic';
        tLanguage.Language_Name__c = 'ar';
        insert tLanguage;
       
        service = new Client_Service__c();
        service.Name = 'Translation';
        service.Unit_of_Measure__c = 'per Word';
        lstClientService.add(service);
        
        btservice = new Client_Service__c();
        btservice.Name = 'Back Translation';
        btservice.Unit_of_Measure__c = 'per Word';
        btservice.Additional_Percentage__c = '15';
        lstClientService.add(btservice);
        
        hoursService = new Client_Service__c();
        hoursService.Name = 'Hour(s), Proofreading (Only)';
        hoursService.Unit_of_Measure__c = 'per Hour';
        lstClientService.add(hoursService);

        outsourcedService = new Client_Service__c();
        outsourcedService.Name = 'DTP Recreation';
        outsourcedService.IsOutsourced__c = true;
        lstClientService.add(outsourcedService);
        
        twcService = new Client_Service__c();
        twcService.Name = 'Translation Edit';
        twcService.IsTWCLogic__C = true;
        lstClientService.add(twcService);
        
        type5Service = new Client_Service__c();
        type5Service.Name = 'Subtitling';       
        lstClientService.add(type5Service);
        
        insert lstClientService;
       
        
       
      Client_TM__c  clientTM = new Client_TM__c();
        clientTM.Client_Name__c = accountObj.Id;
        clientTM.TM_Name__c = '//glsnas/operations/Projects/L10N/Amgen/TMs/Amgen_French(France)/Amgen_French(France).sdltm';
        clientTM.TM_Filename__c = 'Amgen_French(France).sdltm';
        clientTM.Source_Language_ID__c = sLanguage.Id;
        clientTM.Target_Language_ID__c = tLanguage.Id;
        clientTM.GLSTmFileID__c = '111';
        clientTM.IsActive__c = true;
        insert clientTM;
       
       Quote_Line_Item__c quoteLI1=new Quote_Line_Item__c();
       quoteLI1.QuoteID__c = quote.Id;
        quoteLI1.Rate_Type__c='Client Rates';
        quoteLI1.Source_Language_ID__c = sLanguage.Id;
        quoteLI1.Target_Language_ID__c = tLanguage.Id;
        quoteLI1.Trados_Language_Pair__c = 'English (United States)-French (France)';
        quoteLI1.Client_TM__c = clientTM.Id;
        quoteLI1.isTMFound__c = false;
        quoteLI1.isAnalyzed__c = false;
        quoteLI1.isReadyForAnalyse__c = true;
        quoteLI1.XML_Data__c='';
        quoteLI1.Analyzed_File_URL__c = 'www.test.com';
        quoteLI1.Analyzed_Transaction_ID__c = '625';
        quoteLI1.Process_Status__c = 'In Process';
        quoteLI1.TM_Export_Transaction_ID__c = '626';
        quoteLI1.Actual_Words__c = 500;
        GLSQW_RateSummaryController.isNotExecutQLITrigger = false;
       
       insert quoteLI1;
       
        qliService1.Quote_Line_Item_Id__c = quoteLI1.Id;
        qliService1.Service_Id__c = service.Id;
        lstQLIServices.add(qliService1);
        
        qliService2.Quote_Line_Item_Id__c = quoteLI1.Id;
        qliService2.Service_Id__c = btservice.Id;
        lstQLIServices.add(qliService2);
        
        qliService3.Quote_Line_Item_Id__c = quoteLI1.Id;
        qliService3.Service_Id__c = outsourcedService.Id;
        lstQLIServices.add(qliService3);
        
        qliService4.Quote_Line_Item_Id__c = quoteLI1.Id;
        qliService4.Service_Id__c = twcService.Id;
        lstQLIServices.add(qliService4);
        
        qliService5.Quote_Line_Item_Id__c = quoteLI1.Id;
        qliService5.Service_Id__c = type5Service.Id;
        lstQLIServices.add(qliService5);
        
        qliService6.Quote_Line_Item_Id__c = quoteLI1.Id;
        qliService6.Service_Id__c = hoursService.Id;
        lstQLIServices.add(qliService6);
        
        
        /*qliService1.Quote_Line_Item_Id__c = quoteLI1.Id;
        qliService1.Service_Id__c = service.Id;
        lstQLIServices.add(qliService2);*/
        
        insert lstQLIServices;       
       
       Quote_Line_Item_Subline__c quoteSubline = new Quote_Line_Item_Subline__c();
       quoteSubline.Description__c='No Match';
        quoteSubline.IsManuallyAdded__c=true;
        quoteSubline.Manual_Entry__c=true;
        quoteSubline.Quote_Line_Item_Id__c=quoteLI1.id;
       insert quoteSubline;
       
        Rate_Summary_Line_Item_Master__c rateSummaryLineItemMaster = new Rate_Summary_Line_Item_Master__c();
        rateSummaryLineItemMaster.Default_Order__c=1;
        rateSummaryLineItemMaster.Default_Value__c=0;
        rateSummaryLineItemMaster.Quantity__c='New; 50% - 74%; 75% - 84%; 85% - 94%; 95% - 99%';
        rateSummaryLineItemMaster.Rate__c='No Match';
        rateSummaryLineItemMaster.Trados_Category__c='No Match';
        insert rateSummaryLineItemMaster;
      
              
       Quote_Level_Line_Item__c qlli1 = new Quote_Level_Line_Item__c();
        qlli1.IsStandard__c = true;
        qlli1.Minimum_Charge_Notes__c = 'test';
       // qlli1.Name = 'test';
        qlli1.Quote__c = quote.id;
        qlli1.Outsourced_Unit_Price__c = 10.0;
        qlli1.Outsourced_Service_Percentage__c = '10%';
        qlli1.Outsourced_Service__c = 'Linguistic Validation';
        qlli1.Outsourced_Quantity__c = 4;
       insert qlli1;
       
       Rate_Sheet__c rateSheet= new Rate_Sheet__c();
        rateSheet.Name='Amgen';
        insert rateSheet;
      
        
        Rate_Sheet_Item__c rsi= new Rate_Sheet_Item__c();
        rsi.Rate_Sheet__c=rateSheet.Id;       
        rsi.Language__c=tLanguage.Id;
        rsi.Hourly_Rate__c = 50;
        rsi.Min_Amount__c = 2;
        rsi.Min_Amount_Description__c = 'Minimun Charge';
        rsi.Service__c=service.Id;
        insert rsi;
        
        Rate_Sheet_Item__c rsi1= new Rate_Sheet_Item__c();
        rsi1.Rate_Sheet__c=rateSheet.Id;       
        rsi1.Language__c=tLanguage.Id;
        rsi1.Hourly_Rate__c = 50;
        rsi1.Min_Amount__c = 2;
        rsi1.Min_Amount_Description__c = 'Minimun Charge';
        rsi1.Service__c=btservice.Id;
        insert rsi1;
        
        Rate_Sheet_Subitem__c rssi1 = new Rate_Sheet_Subitem__c();
        rssi1.Description__c='0 - 74% Match';
        rssi1.Rate_Sheet_Item__c=rsi.Id;
        insert rssi1;
         
        Rate_Sheet_Subitem__c rssi2 = new Rate_Sheet_Subitem__c();
        rssi2.Description__c='Repetitions';
        rssi2.Rate_Sheet_Item__c=rsi.Id;
        insert rssi2;
        
        Rate_Sheet_Subitem__c rssi3 = new Rate_Sheet_Subitem__c();
        rssi3.Description__c='0 - 74% Match';
        rssi3.Rate_Sheet_Item__c=rsi1.Id;
        insert rssi3;
        
        Rate_Sheet_Subitem__c rssi4 = new Rate_Sheet_Subitem__c();
        rssi4.Description__c='Repetitions';
        rssi4.Rate_Sheet_Item__c=rsi1.Id;
        insert rssi4;
       
       Sub_Item_Trados_Category__c sitc1 = new Sub_Item_Trados_Category__c();
       sitc1.Rate_Sheet_Subitem__c=rssi1.Id;
       sitc1.Trados_Category__c= '95% - 99%';
       //lstsitc.add(sitc1);
       insert sitc1;
       
       Sub_Item_Trados_Category__c sitc2 = new Sub_Item_Trados_Category__c();
       sitc2.Rate_Sheet_Subitem__c=rssi2.Id;
       sitc2.Trados_Category__c= '95% - 99%';
       //lstsitc.add(sitc1);
       insert sitc2;
       
       Sub_Item_Trados_Category__c sitc3 = new Sub_Item_Trados_Category__c();
       sitc3.Rate_Sheet_Subitem__c=rssi3.Id;
       sitc3.Trados_Category__c= '95% - 99%';
       //lstsitc.add(sitc1);
       insert sitc3;
       
       Sub_Item_Trados_Category__c sitc4 = new Sub_Item_Trados_Category__c();
       sitc4.Rate_Sheet_Subitem__c=rssi4.Id;
       sitc4.Trados_Category__c= '95% - 99%';
       //lstsitc.add(sitc1);
       insert sitc4;
       
      /* Sub_Item_Trados_Category__c sitc2 = new Sub_Item_Trados_Category__c();
       sitc2.Rate_Sheet_Subitem__c=rssi.Id;
       sitc2.Trados_Category__c= '85% - 94%';
       //lstsitc.add(sitc2);
       insert sitc2;
       //insert lstsitc;*/
       
        quote.Rate_Sheet__c=rateSheet.Id;
        quote.Pushback_Status__c='Success';
        quote.Certified__c=true;
       //quote.Rate_Sheet__c=rateSheet.Id;
        update quote;
        
        
        
       
       Test.startTest();
       
       Database.UpsertResult result = Database.upsert(quote,false);
       
       quoteLI1.Actual_Words__c = 400;
       GLSQW_RateSummaryController.isNotExecutQLITrigger = false;
       Database.Upsertresult result1 = Database.upsert(quoteLI1,false);
              
       Test.stopTest();
       System.assert(result.isSuccess());
   }
    static  testMethod void testMethod5()
    {
       Account accountObj = new Account();
       accountObj.Name = 'Amgen';
       insert accountObj;
    
       Contact contactObj = new Contact();
       contactObj.FirstName = 'Jennifer A';
       contactObj.LastName = 'Jennifer A';
       contactObj.AccountId = accountObj.Id;
       insert contactObj;

       Quote__c quote1 = new Quote__c();
       quote1.Quote_Name__c = 'Test Quote';
       quote1.Client__c = accountObj.Id;
       quote1.Contact__c = contactObj.Id;
       quote1.Status__c='Released';
       insert quote1;
        
        Language_List_Item__c sLanguage = new Language_List_Item__c();
        sLanguage.Name = 'English (United States)';
        sLanguage.Language_Name__c = 'en-US';
        insert sLanguage;
        
        Quote_Assignment__c qAsst = new Quote_Assignment__c();
        qAsst.Quote__c = quote1.Id; 
        qAsst.Source_Language__c = sLanguage.Id;
        qAsst.Allowed_File_Formats__c = 'MS Word';
        insert qAsst;
          
        Standard_Special_Provisions__c  standardSpec= new Standard_Special_Provisions__c();
        standardSpec.Special_Provision_Name__c='test';
        standardSpec.Special_Provision_Text__c='testestestestestestestest';
        insert standardSpec;
        
        Standard_Special_Provisions__c  standardSpec2= new Standard_Special_Provisions__c();
        standardSpec2.Special_Provision_Name__c='test1';
        standardSpec2.Special_Provision_Text__c='testestestestestestestest111';
        insert standardSpec2;
            
        Quote_Assignment_Special_Provisions__c  quoteAssSpecProvision= new Quote_Assignment_Special_Provisions__c();
        quoteAssSpecProvision.Quote_Assignment_Id__c=qAsst.id;
        quoteAssSpecProvision.Quote_Assignment_Special_Provision_Name__c='test1';
        quoteAssSpecProvision.Quote_Assignment_Special_Provision_Text__c='testestestestestestestest1';
        quoteAssSpecProvision.Standard_Special_Provisions__c=standardSpec.id;
        insert quoteAssSpecProvision;
        
        Quote_Assignment_Special_Provisions__c updQASP = new Quote_Assignment_Special_Provisions__c (id= quoteAssSpecProvision.Id);
        updQASP.Standard_Special_Provisions__c = standardSpec2.Id;
        update updQASP;
          
         Test.startTest();
              Database.UpsertResult result = Database.upsert(quoteAssSpecProvision,false);
          Test.stopTest();
       System.assert(result.isSuccess());
    }
  
  static testMethod void leadCrudCommunityLeadSignupMethod(){
    Lead leads = new Lead();
    leads.FirstName = 'Test';
    leads.LastName = 'Lead';
    leads.Company ='GLS';
    leads.LeadSource = 'Called in';
    leads.NumberOfEmployees = 100;
    leads.AnnualRevenue = 15500.00;
    leads.Annual_Revenue_Year__c = '2014';
    leads.Industry = 'Legal';
    leads.State ='CA';
    leads.Country = 'US';
    leads.Lead_Source_Type__c = 'Web';
    insert leads;
    
    Lead leadUp = new Lead (id = leads.Id);
    leadUp.LastName = 'LeadUp';
    
    Test.startTest();
    Database.SaveResult result = Database.update(leadUp);
    Test.stopTest();
    System.assert(result.isSuccess());
  }
  
   static testMethod void newLeadByWebMethodTest() {
    List<Lead> lstLead = new List<Lead>();
    set<String> strleds = new Set<String>();
    Lead leads = new Lead();
    leads.FirstName = 'Test';
    leads.LastName = 'Lead';
    leads.Company ='GLS';
    leads.LeadSource = 'Called in';
    leads.NumberOfEmployees = 100;
    leads.AnnualRevenue = 15500.00;
    leads.Annual_Revenue_Year__c = '2014';
    leads.Industry = 'Legal';
    leads.State ='CA';
    leads.Country = 'US';
    leads.Lead_Source_Type__c = 'Web';
    lstLead.add(leads);
        
    Lead leads2 = new Lead();
    leads2.FirstName = 'Mt Test';
    leads2.LastName = 'Lead';
    leads2.Company ='MY GLS';
    leads2.LeadSource = 'Called in';
    leads2.NumberOfEmployees = 100;
    leads2.AnnualRevenue = 15500.00;
    leads2.Annual_Revenue_Year__c = '2014';
    leads2.Industry = 'Legal';
    leads2.State ='CA';
    leads2.Country = 'US';
    leads2.Lead_Source_Type__c = 'Web';
    lstLead.add(leads2);
    
    insert lstLead;
    
    
    Account acc = new Account();
    acc.Name = 'GLS';
    insert acc;
    
    for(Lead le : lstLead){
        strleds.add(le.id);
    }
    
    Test.startTest();
    GLS_CommunityNewLeadByWebClass.newLeadByWebMethod(strleds);
    Test.stopTest();
  }
   
   static testMethod void RFQSowItemCrudMethod(){
     RFQ_SOW_Items_Table__c rfwSOW = new RFQ_SOW_Items_Table__c();
     rfwSOW.RFQ_SOW_Item_Description__c = 'test test test';
     rfwSOW.RFQ_SOW_Item_Name__c = 'test sow item display name';
     rfwSOW.RFQ_SOW_Item_Order__c = 1;
     insert rfwSOW;
     
     system.debug('----rfwSOW---: '+rfwSOW.Id)   ;
     RFQ_SOW_Items_Table__c rfwSOWUpdate = new RFQ_SOW_Items_Table__c(Id = rfwSOW.Id); 
     rfwSOWUpdate.RFQ_SOW_Item_Description__c = 'test test';
     rfwSOWUpdate.RFQ_SOW_Item_Name__c = 'test sow item display name updated';
     rfwSOWUpdate.RFQ_SOW_Item_Order__c = 2;
          
     Test.startTest();
     Database.SaveResult result = Database.update(rfwSOWUpdate);
     Test.stopTest();
     System.assert(result.isSuccess());
   }
  
  static testMethod void testMethod6()
   {
    Account accountObj = new Account();
        accountObj.Name = 'Amgen';
        insert accountObj;
    
       Contact contactObj = new Contact();
        contactObj.FirstName = 'Jennifer A';
        contactObj.LastName = 'Jennifer A';
        contactObj.AccountId = accountObj.Id;
        insert contactObj;

       Quote__c quote1 = new Quote__c();
        quote1.Quote_Name__c = 'Test Quote';
        quote1.Client__c = accountObj.Id;
        quote1.Contact__c = contactObj.Id;
      //  quote1.Clone_From__c=quote.id;
       // quote1.Original_Quote_Id__c=quote.id;
        quote1.Status__c='Released';
       insert quote1;
        
        
        
        
        Language_List_Item__c sLanguage = new Language_List_Item__c();
        sLanguage.Name = 'English (United States)';
        sLanguage.Language_Name__c = 'en-US';
        insert sLanguage;
        
       Quote_Assignment__c qAsst = new Quote_Assignment__c();
         qAsst.Quote__c = quote1.Id; 
         qAsst.Source_Language__c = sLanguage.Id;
       qAsst.Display_Languages_Without_Dialect__c=false;
          qAsst.Allowed_File_Formats__c = 'MS Word';
          insert qAsst;
    
       Quote_Assignment__c qAsst1 = new Quote_Assignment__c();
         qAsst1.Quote__c = quote1.Id; 
         qAsst1.Display_Languages_Without_Dialect__c=true;
         qAsst1.Source_Language__c = sLanguage.Id;
          qAsst1.Allowed_File_Formats__c = 'MS Word;Text';
          insert qAsst1;
       
      Quote_Assignment__c qAsst2 = new Quote_Assignment__c();
         qAsst2.Quote__c = quote1.Id; 
         qAsst2.Display_Languages_Without_Dialect__c=true;
         qAsst2.Source_Language__c = sLanguage.Id;
          qAsst2.Allowed_File_Formats__c = 'MS Word;Text;Test';
          insert qAsst2; 
       
     Language_List_Item__c tLanguage = new Language_List_Item__c();
        tLanguage.Name = 'French (France)';
        tLanguage.Language_Name__c = 'fr-FR';
        insert tLanguage;
       
      Language_List_Item__c tLanguage1 = new Language_List_Item__c();
        tLanguage1.Name = 'French';
        tLanguage1.Language_Name__c = 'fr';
        insert tLanguage1;
       
       
     Quote_Assignment_Language__c   qAsstTLang1 = new Quote_Assignment_Language__c();
            qAsstTLang1.Quote_Assignment__c = qAsst.Id;
            qAsstTLang1.Language_List_Item__c = tLanguage.Id;
           insert qAsstTLang1;
           
       Quote_Assignment_Language__c   qAsstTLang2 = new Quote_Assignment_Language__c();
            qAsstTLang2.Quote_Assignment__c = qAsst1.Id;
            qAsstTLang2.Language_List_Item__c = tLanguage.Id;
           insert qAsstTLang2;
       qAsstTLang2.Language_List_Item__c = tLanguage1.Id;
       update qAsstTLang2;
       
        Quote_Assignment_Language__c   qAsstTLang3 = new Quote_Assignment_Language__c();
            qAsstTLang3.Quote_Assignment__c = qAsst1.Id;
            qAsstTLang3.Language_List_Item__c = tLanguage.Id;
           insert qAsstTLang3;
       
        Client_Service__c service = new Client_Service__c();
        service.Name = 'Back Translation';
        insert service;
       // service.Name = 'Translation';
      // update service;
       
       Client_Service__c service1  = new Client_Service__c();
        service1.Name = 'Translation';
        insert service1;
       
       Quote_Assignment_Service__c  qService = new Quote_Assignment_Service__c();
            qService.Quote_Assignment__c = qAsst.Id;
            qService.Client_Service__c = service.Id;
            insert qService;
       
         qService.Client_Service__c = service1.Id;
         update qService;
       
       
        Quote_Assignment_Service__c  qService1 = new Quote_Assignment_Service__c();
            qService1.Quote_Assignment__c = qAsst1.Id;
            qService1.Client_Service__c = service1.Id;
            insert qService1;
       
       Quote_Assignment_Service__c  qService2 = new Quote_Assignment_Service__c();
            qService2.Quote_Assignment__c = qAsst2.Id;
            qService2.Client_Service__c = service1.Id;
            insert qService2;
       
     Trados_Supported_Files__c   TSFCustSett = new Trados_Supported_Files__c();
                TSFCustSett.Name = 'Text';
                TSFCustSett.Extension_Name__c = '.txt';    
            insert TSFCustSett;
          Quote_Assignment_Language__c   qAsstTLang12 = new Quote_Assignment_Language__c();
          qAsstTLang1.Quote_Assignment__c = qAsst.Id;
          qAsstTLang1.Language_List_Item__c = tLanguage.Id;
          insert qAsstTLang12 ;
      
           GLSQW_CloneFileController.skipTrigger=false; 
           GLSAmazonUtility.skipTrigger=false;
       
           delete qAsstTLang12; 
            
        Test.startTest();
              Database.UpsertResult result = Database.upsert(qAsst,false);
                Database.UpsertResult result1 = Database.upsert(qAsst1,false);
               Database.UpsertResult result3 = Database.upsert(qAsst2,false);
        Database.UpsertResult result4 = Database.upsert(qService,false);
        Database.UpsertResult result5 = Database.upsert(qAsstTLang2,false);
        Database.DeleteResult result2 = Database.delete(qAsst,false);
        Test.stopTest();
          System.assert(result.isSuccess()); 
          System.assert(result1.isSuccess()); 
          System.assert(result2.isSuccess()); 
 System.assert(result3.isSuccess()); 

   }
   static testmethod void mytest7(){
       contact c1= new contact ();
       c1.lastname='test';
       c1.MailingCity ='test';
       c1.MailingStreet ='test';
       c1.FirstName='test';
       c1.MailingState='test';
       c1.MailingCountry='test';
       insert c1;
       case c = new case();
       // contact c1=[ select id from contact limit 1];
       c.status='New';
       c.Origin='Email';
       c.contact__c=c1.id;
       insert c;
       
       c.status='Closed';
       c.Should_Contact_Be_Updated__c ='Accepted';
       update c;
        c.status='On Hold';
       c.Should_Contact_Be_Updated__c ='Rejected';
       update c;
        c.status='Closed';
       c.Should_Contact_Be_Updated__c ='Rejected';
       update c;
   }
   
  
   static testmethod void cDSOWItemCrudTrigger(){
        GLSSetupData.DefaultLoaderSetupData();
        Client_Default_SOW_Items_Table__c cDSowI = [Select Id From Client_Default_SOW_Items_Table__c Where Id = : GLSSetupData.cDefaultSowItemDL.Id];
        if(cDSowI != null){
            Test.startTest();
            Database.DeleteResult result = Database.Delete(cDSowI, false);
            Test.stopTest();
            System.assert(result.isSuccess());
        }
   }
   
   static testmethod void cDefaultTermCrudTrigger(){
        GLSSetupData.DefaultLoaderSetupData();
        Client_Default_Terms_Table__c cDSowT = [Select Id From Client_Default_Terms_Table__c Where Id = : GLSSetupData.cDefaultTermsDL.Id];
        if(cDSowT != null){
            Test.startTest();
            Database.DeleteResult result = Database.Delete(cDSowT, false);
            Test.stopTest();
            System.assert(result.isSuccess());
        }
   }
   
   static testmethod void GLSAccountCloneCrud (){
       List<Account> lstAcc = new List<Account>();
       Account Pacc1 = new Account();
       Pacc1.Name = 'Parent Account 1';
       lstAcc.add(Pacc1);
       
       Account Pacc2 = new Account();
       Pacc2.Name = 'Parent Account 2';
       lstAcc.add(Pacc2);
       
       Account Cacc = new Account();
       Cacc.Name = 'Child Account';
       Cacc.ParentId  = Pacc1.Id;
       lstAcc.add(Cacc);
       
       if(lstAcc != null && lstAcc.size()>0)
           insert lstAcc;
       
       Account accUpd = new Account(Id = Cacc.Id);
       accUpd.Name = 'Updated Test';
       accUpd.ParentId  = Pacc2.Id;
       update accUpd ;
    
       Test.startTest();
       Database.SaveResult result = Database.Update(accUpd, false);
       Test.stopTest();
       System.assert(result.isSuccess());
        
   }
}