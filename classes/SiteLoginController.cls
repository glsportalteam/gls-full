/**
 * An apex page controller that exposes the site login functionality
 */

public class SiteLoginController {
    public String username 						{get;set;}
    public String password 						{get;set;}
    public String adminPassword 				{get;set;}
    public boolean iSloginSuccess 				{get;set;}    
    public  String strMsg 						{get;set;}
    public String emailVal 						{get;set;}
    public List<Lead> lstAllLeads 				{get;set;}
    public List<String> lstAllLeadsEmail 		{get;set;}
	
	public SiteLoginController () {
    	//lstAllLeads = [SELECT ID,Email FROM Lead];
    	lstAllLeadsEmail = new List<String>();
    	for(Lead u:[SELECT ID,Email FROM Lead]){
    		if(u.email != null && u.email != '')
    			lstAllLeadsEmail.add(u.email.toUpperCase());
    	}
     }
    public PageReference login(){
    	List<User> lstUser = [SELECT id,ContactId FROM User WHERE username = :username];
    	Contact conObj;
    	if(lstUser!=null && lstUser.size()!=0){
    		    conObj = [SELECT id,isPortalActivated__c FROM Contact where id=:lstUser[0].ContactId];
               	conObj.Last_Login_Date__c = System.now();
               	try{
               		update conObj;
               	}catch(Exception e){}
    	}
        PageReference pr = null;
           try{
           		if(conObj.isPortalActivated__c == true){
              	   //pr = Site.login(username, password, '/apex/GLSCommunity_DraftPage?from=portal&conId='+conObj.Id);
              	   pr = Site.login(username, password, '/apex/GLSCommunity_DraftPage');
           		}
           		else{
           			strMsg = 'Portal User is not activated.';
           			return null;
           		}
           }catch(Exception e)
            {     
           		if(username==''){
           			strMsg = 'Please Enter your User Name.';
           		} 
           		else if(password==''){
           			strMsg = 'Please Enter your Password.';
           		}
           		else{       
             		strMsg = 'Portal is under construction, please try after some time or contact to helpdesk';
           		}
             	return null;
            }
        
        //Check for invalid user login and lock login if max try   
        if (pr == null) 
            {              
               iSloginSuccess = false;   
               if(username==''){
           			strMsg = 'Please Enter your User Name.';
           		} 
           		else if(password=='' && username!=''){
           			strMsg = 'Please Enter your Password.';
           		}
           		else{
               		strMsg = 'Invalid username or password';
           		}
               list<User> user=[select id,ContactId from user where username=:username];
               list<LoginHistory> loginhist;
               if(user !=null && user.size()>0){
                	loginhist=[select id,status,LoginTime from LoginHistory where userId=:user[0].id order by loginTime desc limit 1];
               }
               if(loginhist!=null && loginhist.size()>0 && loginhist[0].status =='Password Lockout')
               		strMsg = 'Your account is Locked as you have login with maximum try. Please retry after 15 minutes.';
               return null;     
           }
           else
           {
             iSloginSuccess = true;
             return pr;
           }
       }
     
     public PageReference forgotPassword() {
        list<User> user=[select id from user where username=:username];
        if(user!=null && user.size() > 0)
        {
            boolean success = Site.forgotPassword(username);
            PageReference pr = Page.ForgotPasswordConfirm;
            pr.setRedirect(true);
            if (success) {              
                return pr;
            }
        }
        else
        {
           iSloginSuccess = false;
           strMsg = 'Username is not available in the system. Please enter valid username';
        }
        return null;
    }
    
     //This method is used only for test coverage
     public pageReference dummy(){
	    integer i=0;
	    integer i1=0;
	    integer i2=0;
	    integer i3=0;
	    integer i4=0;
	    integer i5=0;
	    string a1='test';
	    string a2='test';
	    string a3='test';
	    string a4='test';
	    string a5='test';
	    return null;
    }
}