/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSLPW_InvoiceDetailController Class
* Function: This class is used as a controller extension for the GLSLPW_InvoiceDetailPage
*/

public class GLSLPW_InvoiceDetailController {
 //Variable declaration for the class
      public Id invID                                 {get; set;}
      public Invoice__c invObj                        {get; set;}
      public Boolean isRendered                       {get; set;}
      public List<Quote__c> lstQuote                  {get; set;}
      public List<QuoteWrapper> lstQuoteWrapper       {get; set;}
      public Quote__c quoteObj						  {get; set;}
      public Boolean checkClient					  {get; set;}
     //Constructor
     public GLSLPW_InvoiceDetailController(ApexPages.StandardController stdcon){
                 //Initialization
                 invObj                             = new invoice__c();
                 quoteObj 							= new Quote__c();
                 lstQuote                           = new List<Quote__c>();
                 lstQuoteWrapper = new List<QuoteWrapper>();
                 invID                              = ApexPages.currentPage().getParameters().get('invId');
                 if(invID != null){
                    invObj              = getInvoice(invID);
                    isRendered = true;
                    lstQuoteWrapper   = getQuotes();
                    if(lstQuoteWrapper.size()!=0){
                    	quoteObj = lstQuoteWrapper[0].quote;
                    }
                 }
                 else{
                    isRendered = false;
                 }
    }
      
    /* 
    @Description: To get the complete record of Invoice
    @Params: Invoice Id
    @Return Type: Invocie__c
    */
     private Invoice__c getInvoice(Id invId){
        invObj =  [SELECT Name,id,Description__c,Date_of_Invoice__c,Number_of_Order__c,Invoice_Amount__c,LastModifiedById,CreatedById,
                   Purchase_Agreement_Id__r.Name,Purchase_Agreement_Id__r.Protocol_Id__r.Name,Purchase_Agreement_Id__r.Protocol_Id__r.Program_Name__r.Name,Purchase_Agreement_Id__r.Protocol_Id__r.Program_Name__r.Account__c
                   FROM Invoice__c
                   WHERE Id =: invID LIMIT 1];
        return invObj;
    }
    
    /* 
    @Description: To get the list of Quotes and populate it in a wrapper to be iterated on page
    @Params: None
    @Return Type: List<ProtocolWrapper>
    */
    public List<QuoteWrapper> getQuotes(){
        List<QuoteWrapper> lstQuoteWrapperGet = new List<QuoteWrapper>();
        if(invObj.Purchase_Agreement_Id__r.Protocol_Id__r.Program_Name__r.Account__c!=null){
        	checkClient = true;
        }
        else{
        	checkClient = false;
        }
        lstQuote = [SELECT Id,Quote_Number__c,Client__c,Actual_Total_Amount__c,Expedited_Surcharge_Amount__c,Quote_Total_Amount__c,QLIESOverridden__c,Urgency__c
                    FROM Quote__c
                    WHERE Invoice__c =: invID];
        //client = lstQuote[0].Client__c;
        for(Quote__c quote:lstQuote){
        	Decimal ESAmount = quote.Quote_Total_Amount__c - quote.Actual_Total_Amount__c;
        		lstQuoteWrapperGet.add(new QuoteWrapper(quote.Id,quote.Quote_Number__c,quote.Actual_Total_Amount__c,ESAmount,quote.Quote_Total_Amount__c,quote.Urgency__c,quote.QLIESOverridden__c,quote));        
            }
       return lstQuoteWrapperGet;
    }
    
    /* 
    @Description: Deletes the Invoice Record.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference deleteInvoice(){
    	 PageReference pRef = null;
    	 
        if(invID!=null){
           if(invObj.Description__c!=null){
           	    ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invoice with description cannot be deleted.');
                ApexPages.addMessage(errorMsg);
           }
           else{          
           		try{
	           		delete invObj;
	           		pRef = new PageReference('/apex/GLSLPW_InvoiceManagementPage');
	                pRef.setRedirect(true);
           		}catch(DMLException de){
                    system.debug(GLSConfig.DMLExceptionOccured + de);
           		}catch(Exception e){
                    system.debug(GLSConfig.ExceptionOccured + e);
           		}
          }
        }
       return pref; 
    }
     
    /* 
    @Description: Method to edit the Invoice record.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference customEdit(){
        isRendered = false;
        return null;
    }
    
    /* 
    @Description: Method to Cancel the editing of Invoice record.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference customCancel(){
        PageReference pref = null;
        isRendered = true;
        return pref;
    }
    
    /* 
    @Description: Method to save the Invoice record.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference saveInvoice(){
     PageReference pRef = null;
    	 isRendered = false;
    	 try{
         update invObj;
                if(invObj.Id != null){
                    pRef = new PageReference('/apex/GLSLPW_InvoiceDetailPage?invID='+invObj.Id);
                    pRef.setRedirect(true);
                }
    	 }catch(DMLException de){
            system.debug(GLSConfig.DMLExceptionOccured + de);
        }
        catch(Exception e){
            system.debug(GLSConfig.ExceptionOccured + e);
        }
        return pRef;
    }
    
    
    //Wrapper Class for populating related list of Quote on Invoice Detail Page
    private class QuoteWrapper{
        
        public Id quoteId    {get;set;}    
        public String quoteNumber {get;set;}
        public Decimal quoteAmount{get;set;}
        public Decimal ESAmount {get;set;}
        public Decimal totalQuoteAmount  {get;set;}
        public String deliveryOption  {get;set;}
        public Boolean expQLI   {get;set;}
        public Quote__c quote   {get; set;}
        
        public QuoteWrapper(Id quoteId, String quoteNumber, Decimal quoteAmount,Decimal ESAmount, Decimal totalQuoteAmount,String deliveryOption,Boolean expQLI,Quote__c quote){
            this.quoteId = quoteId;
            this.quoteNumber = quoteNumber;
            this.quoteAmount = quoteAmount;
            this.ESAmount = ESAmount;
            this.totalQuoteAmount = totalQuoteAmount;
            this.deliveryOption = deliveryOption;
            this.expQLI = expQLI;
            this.quote = quote;
           }
     }
}