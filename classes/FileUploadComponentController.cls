global with sharing class FileUploadComponentController {  
	/*
	*	@discription : Below varibale contains the parent ID value of the object against which files will be uploaded
	*/  
    public string parentIdValue{
    	get;set{    		
    		lstFileTypeOptions = new list<SelectOption>();
    		this.parentIdValue = value;
    		Id parentRecordId = value;
    		if(parentRecordId != null){
    		 	parentObjectName = parentRecordId.getSobjectType().getDescribe().getName();    		 	
    		 	if(parentObjectName != null && parentObjectName != ''){
    		 		if(parentObjectName == 'Quote__c'){    		 			
    		 			lstFileTypeOptions.add(new SelectOption(GLSConfig.Quote_File,GLSConfig.Quote_File));
    		 			lstFileTypeOptions.add(new SelectOption(GLSConfig.QuotePDF,GLSConfig.QuotePDF));    		 			
    		 			Quote__c currQuote=[select id,Name,Quote_Number__c, Client__r.Name,Bucket_Name__c from Quote__c where id=:parentRecordId limit 1];
    		 			if(currQuote != null){
    		 				quoteName = currQuote.Quote_Number__c;
			            	accountName = currQuote.Client__r.Name;
    		 			}		    		 				            			                		 		
    		 		}	    		 	    		            
    		 	}    		 	    		 	
    		}
    	}    
    } 
    public string quoteName {get;set;}
    public string accountName {get;set;}
    /*
    @discription : In the below setter disableUploadButton variable which is used to disable file upload based on the file status is initialzed
    */    
    public string parentObjStatus{
	    get;
	    set{
	    	this.parentObjStatus=value;
	        if(parentObjectName==GLSConfig.QuoteObj){
	        	disableUploadButton = true;	 
	        	for(String quoteStatus:GLSConfig.ValidQuoteStatus){
	        		if(value == quoteStatus){
	        			disableUploadButton = false;
	        		}
	        	}       		        	
	        }
	    }
    }     
    public string deleteFileName   {get;set;}    
    public S3.AmazonS3 as3 { get; private set;} 
    private boolean flag=false;    
    public string userType{get;set;}        
    public static string ClientName{get;set;}   
    public static string awsFileName {get;set;}
    public static string clientId {get;set;}    
        
    public string newFileName{get;set;}
    public static string quoteFileName{get;set;}
    public static boolean forAllQFiles{get;set;}
    public Decimal fileSize {get;set;}
    public Boolean IsSPCharFile {get;set;}
    public string amazonPath{get;set;}     
    public string VersionId{get;set;}
    public string bucket{get;set;}
    public string quoteFileId{get;set;}     
    public AWSKeys credentials {get;set;}
    public String AWSCredentialName = 'NAME OF KEY TO USE'; //Modify this string variable to be the name of the AWS Credential record that contains the proper AWS keys and secret
    public string secret { get {return credentials.secret;} }
    public string key { get {return credentials.key;} }                       
    public List<File_Format__c> ext;
    public static List<Trados_Configuration__c> lstTradosConf = new List<Trados_Configuration__c>();            
    public String parentObjectName     {get;set;}            
    public string selectedFileType {get;set;}        
    public boolean disableUploadButton   {get;set;}        
    public list<SelectOption> lstFileTypeOptions     {get;set;}            
    public string FileNameStr {get;set;}    
    public map<string,string> fileNamePathMap {get;set;}    
    public string fileNamePathJSON {get;set;}
    public list<string> lstExistingfileName {get;set;}
                    
   /**
        *   @Description : Constructor
        *       
   **/ 
    public FileUploadComponentController() {             	
       FileNameStr='';     
       fileNamePathJSON='';               	           	
       disableUploadButton = false;           		                                    
       getawss3LoginCredentials();
       IsSPCharFile = false;
       forAllQFiles = false;
       quoteFileId = '';                                                                                                    
       User currentUser = [SELECT Id,UserType,Contact.Account.Name  FROM User WHERE Id = :userInfo.getUserId() limit 1];	                                                                             
       userType=currentUser.UserType;                                                	           	                                                                                                                                                                                                                                                                                                                        
       bucket = GLSConfig.bucketName;
    }                                       
                    
    /**
    	*   @Description : This method sets Aws s3 credentials
    	*   @Param : none
    **/        
    public PageReference getawss3LoginCredentials(){    
        try{
            credentials = new AWSKeys(AWSCredentialName);
            as3 = new S3.AmazonS3(credentials.key,credentials.secret);
         }catch(AWSKeys.AWSKeysException AWSEx){
            System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
            ApexPages.addMessage(errorMsg);	             
        } 
       return null;
    }
    
    Datetime expire = system.now().addDays(1);
    String formattedexpire = expire.formatGmt('yyyy-MM-dd')+'T'+
    expire.formatGmt('HH:mm:ss')+'.'+expire.formatGMT('SSS')+'Z';           
    string policy { get {return 
               '{ "expiration": "'+formattedexpire+'","conditions": [ {"bucket": "'+
       bucket +'" } ,{ "acl": "'+
       'private'+'" },'+
       '{"success_action_status": "201" },'+
       '["starts-with", "$key", ""] ]}';       } }
       
         
    /**
    	*   @Description : This method returns s3 policies
    	*   @Param : none
    **/    
    public String getPolicy() {
        return EncodingUtil.base64Encode(Blob.valueOf(policy));
    }
    
    /**
   		*   @Description : This method returns signature
    	*   @Param : none
    **/    
    public String getSignedPolicy() {    
        return make_sig(EncodingUtil.base64Encode(Blob.valueOf(policy)));        
    }
    
    /**
    	*   @Description : This method returns encoded policy 
    	*   @Param : none
    **/    
    public String getHexPolicy() {
        String p = getPolicy();
        return EncodingUtil.convertToHex(Blob.valueOf(p));
    }                
        
    /**
    	*   @Description : This method returns encrypted signature.
    	*   @Param : String Canonical Buffer
    	*   @return: String
    **/    
    private String make_sig(string canonicalBuffer) {        
        String macUrl ;
        String signingKey = EncodingUtil.base64Encode(Blob.valueOf(secret));
        Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof(Secret)); 
        macUrl = EncodingUtil.base64Encode(mac);                
        return macUrl;
    } 
                
    /** 
    	*  @Description: Create new Quote_PDF_File or Quotes_File__c records based on the file type selected for all all the files selected by user for upload.
    	*                This method is called before the files are uploaded in amozon s3 server.  
    	*  @param:none        
    **/
    public void createAWSRecords(){        	         	 
	 	if(selectedFileType==GLSConfig.QuotePDF){
	 		createNewQuotePDFRecord();
	 	}
	 	if(selectedFileType==GLSConfig.Quote_File){
	 		createNewQuoteFileRecord();
	 	}        	 	        	 	        	         	
    }
    
    /* 
    	*  @Description: Create new Quote_PDF_File records for all all the files selected by user for upload.
    	*                This method is called before the files are uploaded in amozon s3 server.  
    	*  @param:none        
    */
    public void createNewQuotePDFRecord(){    
    	 fileNamePathMap = new Map<String,String>();          	   	             	 
    	 String[] fileNameArrayList = new list<String>();         	         	         	                 	         	
    	 fileNameArrayList = FileNameStr.split(':');        	     
    	         	 
    	 SET<String> fileNameArraySet= new SET<String>();    	 
    	 fileNameArraySet.addAll(fileNameArrayList);   
    	      	 
    	 String[] newfileNameArray = new list<string>();
    	 lstExistingfileName = new list<string>();
    	 String queryString;
    	 
    	 if(fileNameArraySet!=null && fileNameArraySet.size()>0){        	 	        	 	
	 		 queryString = 'select id,File_Name__c from Quote_PDF_File__c where Quote_ID__c =\''+parentIdValue+'\' and File_Name__c IN: fileNameArraySet';
	 		 List<sObject> fileList = Database.Query(queryString);        	 		        	 		        	 		        	 		
	 		 if(fileList != null && fileList.size() > 0){
	 			for(sObject s:fileList){
	 				lstExistingfileName.add(String.valueOf(s.get('File_Name__c')));
	 			}
	 		 }        	 		        	 	        	 	        	 	        	 		        	 		        	 		        	 		        	 		        	 		
	 		 for(String s : fileNameArraySet){        	 		
	 			Boolean isExist=false;
	 			if(lstExistingfileName != null && lstExistingfileName.size() > 0){
    	 			for(String existingFileName : lstExistingfileName){
    	 				if(s == existingFileName){
    	 					isExist = true;
    	 				}
    	 			}
	 			}
	 			if(!isExist){
	 				newfileNameArray.add(s);
	 			}
	 		 }        	 		        	 		
        	 List<Quote_PDF_File__c> newFile=new List<Quote_PDF_File__c>();
	 		 for(String fName : newfileNameArray){		 			
	 			Quote_PDF_File__c PDFFile = new Quote_PDF_File__c();
	 			PDFFile.File_Name__c = fName;
	 			PDFFile.Quote_ID__c = parentIdValue;
	 			PDFFile.Method__c = 'Manual';
	 			newFile.add(PDFFile);
	 		 }        	 		
	 		 try{
	 			insert newFile;				 			 			    	 			
	 		 }catch(DMLException e){
	 			system.debug(e+'exeption-------------');
	 		 }
	 		 //below code if for creating the FileNamePath JSON which is used for populating the file path
	 		 list<Quote_PDF_File__c> QuotePDFFileList = [select id,AWSFilePath__c,File_Name__c from Quote_PDF_File__c where Quote_ID__c =:parentIdValue and File_Name__c IN: fileNameArraySet];
	 		 for(Quote_PDF_File__c PDF : QuotePDFFileList){
	 		 	fileNamePathMap.put(PDF.File_Name__c,PDF.AWSFilePath__c);
	 		 }		 		 		 
	 		 if(fileNamePathMap!=null){
	 		 	fileNamePathJSON = JSON.serialize(fileNamePathMap);
	 		 }		 		 		          	 		 		 	
    	 }        	         	        
    }
    /* 
    	*  @Description: Create new Quotes_File__c records for all all the files selected by user for upload.
    	*                This method is called before the files are uploaded in amozon s3 server.  
    	*  @param:none        
    */
    public void createNewQuoteFileRecord(){    
    	 fileNamePathMap = new Map<String,String>();    	        	 
    	 String[] fileNameArrayList = new list<String>(); 
    	 SET<String> fileNameArraySet= new SET<String>();        	                 	         	
    	 fileNameArrayList = FileNameStr.split(':');        	         	 
    	 fileNameArraySet.addAll(fileNameArrayList);        	 
    	 String[] newfileNameArray = new list<string>();
    	 lstExistingfileName = new list<string>();
    	 String queryString;
    	 if(fileNameArraySet!=null && fileNameArraySet.size()>0){        	 	        	 	
	 		 queryString = 'select id,File_Name__c from Quotes_File__c where Quote__c =\''+parentIdValue+'\' and File_Name__c IN: fileNameArraySet';
	 		 List<sObject> fileList = Database.Query(queryString);        	 		        	 		        	 		        	 		
	 		 if(fileList != null && fileList.size() > 0){
	 			for(sObject s:fileList){
	 				lstExistingfileName.add(String.valueOf(s.get('File_Name__c')));
	 			}
	 		 }        	 
	 		  /*********
	 		 	This needs to be uncommented in phase three when actual process followed for quote_files__c as well.
	 		 ***********/        	 		 
	 		 /* 		        	 	        	 	        	 	        	 		        	 		        	 		        	 		        	 		        	 		
	 		 for(String s : fileNameArraySet){        	 		
	 			Boolean isExist=false;
	 			if(lstExistingfileName != null && lstExistingfileName.size() > 0){
    	 			for(String existingFileName : lstExistingfileName){
    	 				if(s==existingFileName){
    	 					isExist = true;
    	 				}
    	 			}
	 			}
	 			if(!isExist){
	 				newfileNameArray.add(s);
	 			}
	 		 }
	 		      	 		
        	 List<Quotes_File__c> newFile=new List<Quotes_File__c>();
	 		 for(String fName : newfileNameArray){		 			
	 			Quotes_File__c QuoteFile = new Quotes_File__c();
	 			QuoteFile.File_Name__c = fName;
	 			QuoteFile.Quote__c = parentIdValue;		 			
	 			newFile.add(QuoteFile);
	 		 }        	 		
	 		 try{
	 		 	//below insert will become valid in third phase
	 			//insert newFile;
	 		 }catch(DMLException e){
	 			system.debug(e+'exeption------------------------------------');
	 		 }		 		 
	 		 list<Quotes_File__c> QuoteFileList = [select id,AWSFilePath__c,File_Name__c from Quotes_File__c where Quote__c =:parentIdValue and File_Name__c IN: fileNameArraySet];
	 		 for(Quotes_File__c qFile : QuoteFileList){
	 		 	fileNamePathMap.put(qFile.File_Name__c,qFile.AWSFilePath__c);
	 		 }		 		 		 
	 		 if(fileNamePathMap!=null){
	 		 	fileNamePathJSON = JSON.serialize(fileNamePathMap);
	 		 } 
	 		 */       	 	
    	 }        	         	        
    }        
                
    /**
    	*   @Description : This method saves AWS File Record and update the Quote_file__c record or Quote_PDF_file__c record based on the file type selected.
    				   This method is called after the file is successfully uploaded to amazon server.	
    	*   @Param : none
    **/          
    public void successFileupload() {        	                                   
	    if(parentIdValue != null && selectedFileType != null){     
	       List<RecordType> recordType;	               
	       string transactionId = '';
	       String field;                              
	       if(newFileName != null && newFileName.lastIndexOf('.') != -1){	       	    
	       	    fileSize = (fileSize/1024);               	                   	                                                     	
	            AWS_File__c objRecord = new AWS_File__c();                                                                                                    
	            objRecord.File_Name__c = newFileName;                    
	            objRecord.Bucket_Name__c = GLSConfig.bucketName;
	                                                                                                              
	            if(amazonPath.length() > 255)
	                 amazonPath = amazonPath.substring(0,255);
	                                          
	            objRecord.Amazon_S3_source_file_path__c = amazonPath;
	            objRecord.Record_Type__c = selectedFileType;
	            decimal maxversion=0;                                                            
	            List <AWS_File__c> File_list = new List <AWS_File__c>();                                        
	            try{                                                           	 
	                String FileListQuery='SELECT Id, Version__c FROM AWS_File__c WHERE Amazon_S3_source_file_path__c =\''+amazonPath+'\' and File_Name__c=\''+newFileName+'\'';                                                               
	                File_list= Database.Query(FileListQuery);    
	            }catch(QueryException de){
	                System.debug('Query Exception occured.');
	            }catch(Exception e){
	                System.debug('Exception occured.');
	            }                    
	            if(File_list != null && File_list.size()>0){
	                for (AWS_File__c temp : File_list) {
	                    if(maxversion<temp.Version__c)
	                          maxversion=temp.Version__c;
	                    objRecord.Id= temp.Id; 
	                }                        
	            }                                                                                                          
	            objRecord.Version__c=maxversion+1;
	            objRecord.version_id__c=VersionId;                   
	            try{
	                upsert(objRecord);                        
	                if(selectedFileType==GLSConfig.QuotePDF){
	                	populateQuotePDF(objRecord.id,newFileName);
	                }
	                if(selectedFileType==GLSConfig.Quote_File){
	                	populateQuoteFile(objRecord.id,newFileName,fileSize);
	                }	                    	                    
	             }catch(DmlException de){
	                flag = false;
	                quoteFileId = '';
	                System.debug('DML Exception occured.');
	             }catch(Exception e){
	                flag = false;
	                quoteFileId = '';                        
	             }                                                                                                                                                                     
	        } 
	    }
	}      
                        
    /**
    	*   @Description : This method saves Quote File Record and used for only phase one after phase three quote files will be uploaded by successFileUpload method only
    	*   @Param : none
    **/
          
    public void successQuoteFileupload() {        
        if(parentIdValue!=null)
        {     
           string extension;
           string transactionId = '';
           ext= new List<File_Format__c>();
           if(newFileName.lastIndexOf('.')!= -1){
                extension=newFileName.substring(newFileName.lastIndexOf('.'),newFileName.length());
                ext=[select id,File_Extension__c,Name from File_Format__c where File_Extension__c INCLUDES(:extension) limit 1];
                Quotes_File__c objRecord= new Quotes_File__c();
                objRecord.Quote__c = parentIdValue;
                objRecord.File_Name__c = newFileName;
                if(bucket != null)
                    objRecord.Bucket_Name__c = bucket;
                else
                    objRecord.Bucket_Name__c = GLSConfig.bucketName;
                fileSize = (fileSize/1024);
                objRecord.File_Size__c = fileSize;
                objRecord.isSPCharFile__c = IsSPCharFile;
                
                if(ext.size() > 0 ){
                    objRecord.File_Format__c=ext[0].id;
                }
                if(amazonPath.length() > 255)
                     amazonPath = amazonPath.substring(0,255);
                objRecord.Amazon_S3_source_file_path__c = amazonPath;
                decimal maxversion=0;
                List <Quotes_File__c> File_list = new List <Quotes_File__c>();
                
                try{
                    File_list= [SELECT Id, Version__c FROM Quotes_File__c WHERE File_Name__c = :newFileName and Quote__c = :parentIdValue];    
                }catch(QueryException de){
                    System.debug('Query Exception occured.');
                }catch(Exception e){
                    System.debug('Exception occured.');
                }
                if(File_list != null && File_list.size()>0){
                    for (Quotes_File__c temp : File_list) {
                        if(maxversion<temp.Version__c)
                              maxversion=temp.Version__c;
                              objRecord.Id= temp.Id; 
                    }
                }else
                    objRecord.Translatable__c = 'Error';
                    
                objRecord.Version__c=maxversion+1;
                objRecord.version_id__c=VersionId;
               
                try{
                    upsert(objRecord);
                    quoteFileId = objRecord.Id;
                    flag = true;
                 }catch(DmlException de){
                    flag = false;
                    quoteFileId = '';
                    System.debug('DML Exception occured.');
                 }catch(Exception e){
                    flag = false;
                    quoteFileId = '';
                    System.debug('Exception occured.');
                 }
              } 
           }
        }
        
        /**
        	*   @Description : This method is used for deleting those file records which are failed to upload on amaon server.	        				   	
        	*   @Param : none
        **/       
        public void deleteUnuploadedFiles(){
        	if(selectedFileType==GLSConfig.QuotePDF){                	                	
            	list<Quote_PDF_File__c> QPDFList = [select id,AWS_File_Id__c from Quote_PDF_File__c where Quote_ID__c=:parentIdValue and File_Name__c=:deleteFileName limit 1];
            	if(QPDFList!=null && QPDFList.size()>0){                		
            		Quote_PDF_File__c fileToBeDeleted = QPDFList[0];
            		if(fileToBeDeleted != null && fileToBeDeleted.AWS_File_Id__c==null){
            			system.debug(fileToBeDeleted.AWS_File_Id__c+'AWS_File_Id__c-----------');
            			try{
            				delete fileToBeDeleted;
            			}catch(DMLException e){
            				system.debug(e.getMessage()+GLSConfig.DMLExceptionOccured);
            			}	 
            		}  
            	}
            }
            if(selectedFileType==GLSConfig.Quote_File){                	                	                	
            	list<Quotes_File__c> qFileList= [select id,AWS_File__c from Quotes_File__c where Quote__c=:parentIdValue and File_Name__c=:deleteFileName limit 1];
            	if(qFileList!=null && qFileList.size()>0){                		
            		Quotes_File__c fileToBeDeleted = qFileList[0];
            		if(fileToBeDeleted != null && fileToBeDeleted.AWS_File__c==null){
            			system.debug(fileToBeDeleted.AWS_File__c+'AWS_File__c-----------AWS_File__c');
            			try{
            				delete fileToBeDeleted; 
            			}catch(DMLException e){
            				system.debug(e.getMessage()+GLSConfig.DMLExceptionOccured);
            			}
            		}  
            	}
            }
        }
        /*
        	*	@Description  : This method is used for updating the parent object for storing the latest uploaded AWSFileId value, 
        		this method is called only for quote PDF Files upload
        	*   @param  :  none
        	*   @return :  none	    
        */
        public void updateParentObjAWSInfo(){         	
        	if(selectedFileType==GLSConfig.QuotePDF){
        		list<Quote_PDF_File__c> quoteFileList = [select id,AWS_File_Id__c from Quote_PDF_File__c where Quote_ID__c=:parentIdValue order by LastModifiedDate desc];
        		if(quoteFileList!=null && quoteFileList.size()>0){
        			Quote__c parentQuote = new Quote__c(id=parentIdValue);
        			parentQuote.Latest_AWS_File__c = quoteFileList[0].AWS_File_Id__c;
        			GLSQW_RateSummaryController.isNotExecutQLITrigger = true;
        			try{
        				update parentQuote;
        			}catch(DMLException e){
        				system.debug(e.getMessage()+'----------------------exception-----------------------');
        			}	             			 
        		}
        	}            	            	
        }
        /**
        	*   @Description : This method update the Quote_PDF_file__c records with AWS_File__c id value in it after the file gets succesfully uploaded to amazon.	      	
        	*   @Param : awsObjId contains the id of the correponding AWS_File__c object,filename contains the name of the corresponding file
        	*	@return : void
        **/       
        public void populateQuotePDF(Id awsObjId,String filename){
        	Quote_PDF_File__c pdfObj;            		            	
            list<Quote_PDF_File__c> QPDFList = [select id from Quote_PDF_File__c where Quote_ID__c=:parentIdValue and File_Name__c=:filename];	            
        	if(QPDFList!=null && QPDFList.size()>0){
        		pdfObj = QPDFList[0];
        		pdfObj.AWS_File_Id__c = awsObjId;	            	            		
        	}	            	
        	try{
        		if(pdfObj != null){
        			update pdfObj;
        		}
        	}catch(DMLException e){
        		system.debug('Exception occured---'+e);
        	}	            	       	            	
        }
        
        /**
        	*   @Description : This method update the Quote_file__c records with AWS_File__c id value in it after the file gets succesfully uploaded to amazon.	      	
        	*   @Param : awsObjId contains the id of the correponding AWS_File__c object,filename contains the name of the corresponding file
        	*	@return : void
        **/
        public void populateQuoteFile(Id awsObjId,String fileName,Decimal filesize){
        	string extension;
        	ext= new List<File_Format__c>();
        	Quotes_File__c qFileObj;
        	list<Quotes_File__c> qFileList= [select id from Quotes_File__c where Quote__c=:parentIdValue and File_Name__c=:filename];
        	if(qFileList!=null && qFileList.size()>0){
        		qFileObj = qFileList[0];
        		if(fileName != null && fileName.lastIndexOf('.') != -1){
            		extension=newFileName.substring(newFileName.lastIndexOf('.'),newFileName.length());
	            	ext = [select id,File_Extension__c,Name from File_Format__c where File_Extension__c INCLUDES(:extension) limit 1];
	            	if(ext.size() > 0 ){
                    	qFileObj.File_Format__c = ext[0].id;                        
                    }
            	}            		            	            	
            	qFileObj.AWS_File__c = awsObjId;	            	
            	qFileObj.File_Type__c = 'Source';
            	qFileObj.Translatable__c = 'error';
            	qFileObj.File_Size__c=filesize;	            	
            	quoteFileId = qFileObj.Id;
        	}            	
        	if(qFileObj!=null){
        		try{
        			update qFileObj;
        		}catch(DMLException e){
            		system.debug('Exception occured---'+e);
            	}
        	}	                        	            	            		            	            	           	
        }
                                                          	                 
        WebService static string recallGEFAPI(String FileId){
	        if(FileId != null){
	            try{
	                quoteFileName = FileId;
	                forAllQFiles = true;
	                callGetExternalFileWS();
	            }catch(Exception e){
	                system.debug(GLSConfig.ExceptionOccured+'-'+e);
	            }
	        }
	        return null;
        }
                                  
        /**
      		* callGetExternalFileWS: This method is used to send the Quote file for GetExternalAPICall and 
      		*create the Outbound API Logger record
      	*/
	     public static void callGetExternalFileWS(){               	     
            GLS_API_Logger__c glsApiLogger= new GLS_API_Logger__c();
            String bucket ='';
            RecordType recordType=[Select  SobjectType, Name,Id,DeveloperName,Description From RecordType  where  SObjectType='GLS_API_Logger__c' and DeveloperName='Quote_File_Logger_Record_Type'];
            lstTradosConf = Trados_Configuration__c.getall().values();
            if(lstTradosConf != null && lstTradosConf.size()>0){
                 List<Quotes_File__c> qFile = new List<Quotes_File__c>();
                 try{                        
                    if(forAllQFiles){                                
                        qFile = [Select Id, Quote__c, File_Name__c, version_id__c, TradosAPIFileID__c, Quote__r.Client__c,
                                 Quote__r.Client__r.Name, Quote__r.Quote_Number__c, Quote__r.Bucket_Name__c
                                 From Quotes_File__c 
                                 Where Id=: quoteFileName And (TradosAPIFileID__c = '' OR TradosAPIFileID__c = null)
                                 limit 1];
                    }else{                                
                        qFile = [Select Id, Quote__c, File_Name__c, version_id__c, TradosAPIFileID__c, Quote__r.Client__c,
                                 Quote__r.Client__r.Name, Quote__r.Quote_Number__c, Quote__r.Bucket_Name__c
                                 From Quotes_File__c 
                                 Where Id=: quoteFileName And (TradosAPITransactionID__c = '' OR TradosAPITransactionID__c = null)
                                 limit 1];
                    }
                 }catch(QueryException de){
                    System.debug('Query Exception occured. '+de);
                }                                                            
                if(qFile != null && qFile.size()>0){
                    String transactionId = '';
                    clientName = '';
                    clientId = '';
                    if(qFile[0].Quote__r.Client__r.Name != null)
                        clientName = EncodingUtil.urlEncode(qFile[0].Quote__r.Client__r.Name, 'UTF-8');
                    String qId = qFile[0].Quote__c;
                    if(qId.length()!=15)
                        qId=qId.subString(0,15);
                    awsFileName = qFile[0].Quote__r.Quote_Number__c+'_'+qId;
                    clientId = qFile[0].Quote__r.Client__c;
                    bucket =  qFile[0].Quote__r.Bucket_Name__c;
                    String quotePath = clientName+'/'+awsFileName;
                    String fileName = EncodingUtil.urlEncode(qFile[0].File_Name__c, 'UTF-8');
                    String fileVersionId = qFile[0].version_id__c;
                    String sfFileIds = qFile[0].Id;
                    String secure ='';
                    glsApiLogger.Quote_Number__c=qFile[0].Quote__c;
                    glsApiLogger.Quote_File_Number__c =qFile[0].Id;
                    glsApiLogger.RecordTypeId  =recordType.Id; 
                    glsApiLogger.API_Call_Type__c='GetExternalFile';
                    Quotes_File__c qFileUpdate = new Quotes_File__c(Id =qFile[0].Id);
                    HttpResponse resL;
                    
                    if(lstTradosConf[0].isSecure__c)
                     secure = 'https://';
                    else
                     secure = 'http://';
                    Integer UserId = Integer.valueOf(lstTradosConf[0].User_Id__c);
                    if(clientName != '' && clientId != '' && awsFileName != '' && bucket != ''){
                        String tradosEndpointURl = secure+lstTradosConf[0].DNS_Name__c + GLSconfig.getExternalFileAPICall+'?'+ GLSconfig.paramSecretKey + lstTradosConf[0].Secret_Key__c + 
                                           GLSconfig.paramUserId + UserId + GLSconfig.paramBucketName + bucket + GLSconfig.ParamoutputPath + quotePath + 
                                           GLSconfig.ParamSfFileIds + sfFileIds + GLSconfig.paramFileName + fileName + GLSconfig.paramVersionId + fileVersionId +
                                           GLSconfig.paramSource + lstTradosConf[0].IP_Address__c + GLSconfig.paramClientId + clientId;
                        system.debug(tradosEndpointURl+'tradosEndpointURl----------------');                    
                        glsApiLogger.API_Rest_URL__c=tradosEndpointURl;
                        glsApiLogger.URL_Parameter__c=parameterKeyValuePair(tradosEndpointURl);
                        glsApiLogger.Timestamp__c=system.now();
                        resL = GLSQLICreator.getResponse(tradosEndpointURl); 
                        system.debug('---resL---: '+resL);
                        if(resL != null){
                            String httpResponseStatusCode = String.valueOf(resL.getStatusCode());
                            if(httpResponseStatusCode == GLSconfig.successCode){
                                Dom.XmlNode xmlNode = resL.getBodyDocument().getRootElement();
                                if(xmlNode != null && xmlNode.getText() != ''){
                                    transactionId = xmlNode.getText();
                                    qFileUpdate.Translatable__c = 'Still Processing';
                                }
                                if(transactionId == null || transactionId == ''){
                                    transactionId = '0';
                                    qFileUpdate.Translatable__c = 'Error';
                                    qFileUpdate.Error_Message__c = GLSConfig.TransactionIdNotFound;
                                } 
                                glsApiLogger.Status__c='Success';
                            }else{
                                 Dom.XmlNode errxmlNode = resL.getBodyDocument().getRootElement();
                                 if(errxmlNode != null && errxmlNode.getText() != ''){
                                   transactionId = '0';
                                   qFileUpdate.Translatable__c = 'Error';
                                   qFileUpdate.Error_Message__c = errxmlNode.getText();
                                   glsApiLogger.Error_Message__c=errxmlNode.getText();
                                 }
                                 glsApiLogger.Status__c='Error';
                            } 
                            glsApiLogger.Response_of_API_call__c=transactionId;                             
                        }else{
                            transactionId = '0';
                            qFileUpdate.Translatable__c = 'Error';
                            qFileUpdate.Error_Message__c = GLSConfig.ResponseNotRecieved;
                            glsApiLogger.Error_Message__c=GLSConfig.ResponseNotRecieved;
                            glsApiLogger.Status__c='Error';
                        }
                        qFileUpdate.TradosAPITransactionID__c = transactionId;
                        try{
                            update qFileUpdate;
                        }catch(DMLException de){
                            system.debug(GLSConfig.DMLExceptionOccured+'-'+de);
                        }
                        try{
                           insert glsApiLogger;
                        }catch(DMLException de){
                            system.debug(GLSConfig.DMLExceptionOccured+'-'+de);
                        }catch(Exception ex){}
                    }
                }
            }
        }                      
            
        private static string parameterKeyValuePair(string tradosEndpointURl){            
         string parameter='';
         parameter+= 'secretKey'+tradosEndpointURl.substringBetween('secretKey','&')+'\n';
         parameter+= 'userID'+tradosEndpointURl.substringBetween('userID','&')+'\n';
         parameter+= 'bucketName'+tradosEndpointURl.substringBetween('bucketName','&')+'\n';
         parameter+= 'outputPath'+tradosEndpointURl.substringBetween('outputPath','&')+'\n';
         parameter+= 'SalesforceFileIds'+tradosEndpointURl.substringBetween('SalesforceFileIds','&')+'\n';
         parameter+= 'fileNames'+tradosEndpointURl.substringBetween('fileNames','&')+'\n';
         parameter+= 'versionIds'+tradosEndpointURl.substringBetween('versionIds','&')+'\n';
         parameter+= 'source'+tradosEndpointURl.substringBetween('source','&')+'\n';
         parameter+= 'clientId'+tradosEndpointURl.substringAfter('clientId');
         return parameter;            
      }
}