/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 */
 
@isTest
private class GLSQuoteLineItemCreatorTest {
       
    /**
    * This Unit Test method to test the getExternalFile API call
    */
    static testMethod void testgenerateQLISuccess() {
        GLSSetupData.setupData();
        Test.startTest();
        GLSQLICreator.createQuoteLineItem(GLSSetupData.lstQuote[0].id);
        System.assertEquals(GLSQLICreator.returnMessage,GLSConfig.LineItemCreatedSuccessfully);
         GLSQLICreator.dummy();
        Test.stopTest();
    }
    
    /**
    * This Unit Test method to test the Transaction ID null or not,
    * If we get transaction Id in return then the quote file size will not be 0
    */
    static testMethod void testTransactionIdSuccess() {
        GLSSetupData.setupData();
        Test.startTest();
        GLSSetupData.isReponseCorrect=false;
        GLSSetupData.isTransaction=false;
        GLSSetupData.isSuccessCode = false;
        GLSQLICreator.createQuoteLineItem(GLSSetupData.lstQuote[0].id);
        List<Quotes_File__c> qFiles = [select Id, Quote__c, File_Name__c, version_id__c, TradosAPIFileID__c, TradosAPITransactionID__c
                                        From Quotes_File__c 
                                        Where Quote__c =:GLSSetupData.lstQuote[0].id AND
                                        TradosAPITransactionID__c != null ];
               
        System.assertNotEquals(qFiles.size(),0);
        Test.stopTest();
    }
    
    static testMethod void testTransactionIdSuccess1() {
        GLSSetupData.setupData();
        Test.startTest();
        GLSSetupData.isReponseCorrect=false;
        GLSSetupData.isTransaction=true;
        GLSSetupData.isSuccessCode = false;
        GLSQLICreator.createQuoteLineItem(GLSSetupData.lstQuote[0].id);
        List<Quotes_File__c> qFiles = [select Id, Quote__c, File_Name__c, version_id__c, TradosAPIFileID__c, TradosAPITransactionID__c
                                        From Quotes_File__c 
                                        Where Quote__c =:GLSSetupData.lstQuote[0].id AND
                                        TradosAPITransactionID__c != null ];
               
        System.assertNotEquals(qFiles.size(),0);
        Test.stopTest();
    }
    
    static testMethod void testTransactionIdSuccess2() {
        GLSSetupData.setupData();
        Test.startTest();
        GLSSetupData.isReponseCorrect=false;
      //  GLSSetupData.isTransaction=true;
        GLSSetupData.isSuccessCode = true;
        GLSQLICreator.createQuoteLineItem(GLSSetupData.lstQuote[0].id);
        List<Quotes_File__c> qFiles = [select Id, Quote__c, File_Name__c, version_id__c, TradosAPIFileID__c, TradosAPITransactionID__c
                                        From Quotes_File__c 
                                        Where Quote__c =:GLSSetupData.lstQuote[0].id AND
                                        TradosAPITransactionID__c != null ];
               
        System.assertNotEquals(qFiles.size(),0);
        Test.stopTest();
    }
    /**
    * This Unit Test method to test If no Quote Assignment Created and
    * User try to Generate Quote Line Items
    */
    static testMethod void testQuoteAssignmentFailure() {
        GLSSetupData.setupData();
        List<Quote_Assignment__c> qAsst = [Select Id from Quote_Assignment__c where Id =:GLSSetupData.qAsst.id];
        if(qAsst != null && qAsst.size()>0){
            delete qAsst;
        }
        Test.startTest();
        GLSQLICreator.createQuoteLineItem(GLSSetupData.lstQuote[0].id);
        System.assertEquals(GLSConfig.QuoteAsstNotCreated,GLSQLICreator.returnMessage);
        Test.stopTest();
    }
    
   
    /* Below Test Methods are for TMExport Functionality. Currently it is commented. Need to uncomment when TMExport feature is in scope
    /**
    * This Unit Test method to test the TMExport API call Success
    * /
    static testMethod void testTMExportAPIcallSuccess() {
        GLSSetupData.createQLIServices();
        Test.startTest();
        GLSAPICaller.tmExportQuoteLineItems(GLSSetupData.lstQuote[0].id);
        System.assertEquals(GLSConfig.TMExportSuccess,GLSAPICaller.returnMessage);
        Test.stopTest();
    }
    
    /**
    * This Unit Test method to test the TMExport API call Success
    * /
    static testMethod void testTMExportAPIcallSuccessIndv() {
        GLSSetupData.createQLIServices();
        Quote_Line_Item__c qliUpdate1 = new Quote_Line_Item__c(id= GLSSetupData.quoteLI1.Id);
        qliUpdate1.TM_Export_Transaction_ID__c = '';
        qliUpdate1.TM_Export_Transaction_ID__c =null;
        update qliUpdate1;
        Test.startTest();
        GLSAPICaller.exportTM(qliUpdate1.id);
        System.assertEquals(GLSConfig.TMExportSuccess,GLSAPICaller.returnMessage);
        Test.stopTest();
    }
    
    /**
    * This Unit Test method to test the TMExport API call, When no QLI avaliable 
    * /
    static testMethod void testTMExportNoQLICreatedFailure() {
        GLSSetupData.createQLIServices();
        Quote_Line_Item__c qliUpdate1 = new Quote_Line_Item__c(id= GLSSetupData.quoteLI1.Id);
        qliUpdate1.TM_Export_Transaction_ID__c = '2234';
        update qliUpdate1;
        
        Quote_Line_Item__c qliUpdate2 = new Quote_Line_Item__c(id= GLSSetupData.quoteLI2.Id);
        qliUpdate2.TM_Export_Transaction_ID__c = '1234';
        update qliUpdate2;
        Test.startTest();

        GLSAPICaller.tmExportQuoteLineItems(GLSSetupData.lstQuote[0].id);
        //System.assertEquals(GLSConfig.NoQLICreatedforTMExport,GLSAPICaller.returnMessage);
        Test.stopTest();
    }
    
    /**
    * This Unit Test method to test AnalyzeFile API call Fail When no QLI avaliable 
    * /
    static testMethod void testTMExportNoQLICreatedFailureIndiv() {
        GLSSetupData.createQLIServices();
        Quote_Line_Item__c qli = new Quote_Line_Item__c(id=GLSSetupData.quoteLI1.Id);
        qli.TM_Export_Transaction_ID__c = '123456';
        update qli;
        Test.startTest();
        GLSAPICaller.exportTM(qli.id);
        System.assertEquals(GLSConfig.NoQLICreatedforTMExport,GLSAPICaller.returnMessage);
        Test.stopTest();
    }
    */
    
}