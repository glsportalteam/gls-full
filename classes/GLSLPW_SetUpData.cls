/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public with sharing class GLSLPW_SetUpData {
    
    public static Account createAccount(String accName){
        Account accountObj = new Account(Name               = accName,
                                         BillingStreet      = 'Test',
                                         BillingCity        = 'Test',
                                         BillingState       = 'Test',
                                         BillingPostalCode  ='Test',
                                         BillingCountry     ='Test'
                                         );
        insert accountObj;
        return accountObj;
    }
    
    public static Contact createContact(Id accId,String lastName){
        Contact contactObj = new Contact(FirstName  = 'Jennifer Test',
                                         LastName   = lastName,
                                         AccountId  = accId
                                         );
        insert contactObj;
        return contactObj;
    }
    
    public static Opportunity createOpportunity(Id conId,Id accObj,String oppName,String oppStageName,Date oppCloseDate){
    	Opportunity oppObj = new Opportunity(Contact__c = conId,
    										 AccountId  = accObj,
    										 CloseDate	= oppCloseDate,
    										 Name 		= oppName,
    										 StageName	= oppStageName
    										);
    	insert oppObj;
    	return oppObj;
    } 
    
    public static Program__c createProgram(Id accId, Id conId, String prgName, String ref){
        Program__c programObj = new Program__c(Reference__c     = ref,  
                                            Account__c          = accId,
                                            Contact__c          = conId,    
                                            Program_Name__c     = prgNAme
                                            );
        insert programObj;
        return programObj;
     }
     
    public static Program_Files__c createProgramFiles(Id prgId,Id awsId){
    	Program_Files__c programFileObj = new Program_Files__c(Program_Id__c 	= prgId,
    														   AWS_File_Id__c	= awsId,
    														   Program_File_Name__c = 'Test Program File'
    														   );
    	
    	insert programFileObj;
    	return programFileObj;
    }
     
    public static Estimate__c createEstimate(Id prgId, String status){
        Estimate__c estimateObj = new Estimate__c(Program_Id__c = prgId,
                                                  Status__c     = status
                                                 );
        insert estimateObj;
        return estimateObj;                                      
     }
   
    public static Estimate_Line_Item__c createELI(Id protocolId,Id estimateId){
        Estimate_Line_Item__c eliObj = new Estimate_Line_Item__c(Protocol_Id__c = protocolId,
                                                                 Estimate_Id__c = estimateId,
                                                                 Amount__c      = 1000
                                                                 );
       insert eliObj;
       return eliObj;                                                        
    }   
    
    public static Estimate_Line_Country__c createELC(Integer noOfSites,Id countryId,Id eliId){
        Estimate_Line_Country__c elcObj = new Estimate_Line_Country__c(Number_of_Sites__c = noOfSites,  
                                                                       Country_Id__c      = countryId,
                                                                       Estimate_Line_Id__c= eliId
                                                                       ); 
        insert elcObj;
        return elcobj; 
    }
    
    public static Protocol__c createProtocol(Id prgId,String status){
        Protocol__c protocolObj = new Protocol__c(Program_Name__c   = prgId,
                                                  Status__c         = status,
                                                  Approved_Amount__c=1000.00,
                                                  Protocol_Name__c  ='Test Protocol'
                                                  );
        insert protocolObj;
        return protocolObj;
    }
    
    public static Protocol_Country__c createProtocolCountry(Id protocolId,Id countryId,Integer noOfSites,String status){
        Protocol_Country__c pcObj = new Protocol_Country__c(Protocol_Id__c    =protocolId,
                                                            Country_Id__c     =countryId,
                                                            Number_of_Sites__c=noOfSites,
                                                            Status__c         =status       
                                                            );
        insert pcObj;
        return pcObj;                                                         
    }
    
    public static Purchase_Agreement__c createPurchaseAgreement(Id protocolId){
        Purchase_Agreement__c paObj = new Purchase_Agreement__c(Protocol_Id__c = protocolId
                                                                );
        insert paObj;
        return paObj;
    }
    
    public static Estimate_Files__c createEstimateFile(String fileType, Id estimateId, id awsId){
        Estimate_Files__c efObj = new Estimate_Files__c(File_Type__c            = filetype,
                                                        Estimate_Id__c          = estimateId,
                                                        AWS_File_Id__c          = awsId,
                                                        Estimate_File_Name__c   ='Test File'
                                                        );
        insert efObj;
        return efObj;                                               
    }
    
    public static Purchase_Agreement_File__c createPurchaseAgreementFile(Id paId,id awsId){
        Purchase_Agreement_File__c pafObj = new Purchase_Agreement_File__c(Purchase_Agreement_Id__c = paId, 
                                                                           PA_File_Name__c          = 'Test PAFile',
                                                                           AWS_File_Id__c           = awsId
                                                                           );
        insert pafObj;
        return pafObj;                                                                 
    }
    
    public static Quote__c createQuote(Id accId, Id conId,String status){ 
        Quote__c quoteObj = new Quote__c( Client__c     = accId,
                                          Contact__c    = conId,    
                                          Status__c     = status
                                        );
        insert quoteObj;
        return quoteObj;
    }
    
	public static Quote_Assignment__c createQuoteAssignment(Id quoteId){ 
	    Quote_Assignment__c qaObj = new Quote_Assignment__c( Quote__c = quoteId
	                                    );
	    insert qaObj;
	    return qaObj;
    }
    
     public static File_Format__c createFileFormat(String extension){ 
	    File_Format__c formatObj = new File_Format__c( File_Extension__c = extension
	                                    );
	    insert formatObj;
	    return formatObj;
    }
    
    public static Quotes_File__c createQuoteFile(Id quoteId){ 
	    Quotes_File__c qfObj = new Quotes_File__c( Quote__c 		= quoteId,
	    										   File_Name__c 	= 'Test.text'
	                                    );
	    insert qfObj;
	    return qfObj;
    }
    
    public static Quote_Assignment_File__c createQuoteAssignmentFile(Id qfId){ 
	    Quote_Assignment_File__c qafObj = new Quote_Assignment_File__c( Quote_Files__c 		= qfId
	                                    );
	    insert qafObj;
	    return qafObj;
    }
    
    public static Quote_Assignment_Service__c createQuoteAssignmentService(Id qaID,String service){ 
	    Quote_Assignment_Service__c qasObj = new Quote_Assignment_Service__c( Quote_Assignment__c  = qaId,
	    													  				  Service_Name__c 	 = service
	                                    );
	    insert qasObj;
	    return qasObj;
    }
    
    public static Quote_Assignment_Language__c createQuoteAssignmentLanguage(Id qaId,String language){ 
	    Quote_Assignment_Language__c qalObj = new Quote_Assignment_Language__c( Quote_Assignment__c  = qaId,
	    													  					Language_Name__c	 = language
	                                    );
	    insert qalObj;
	    return qalObj;
    }
    
    public static Quote_PDF_File__c createQuotePDFFile(Id quoteId){ 
	    Quote_PDF_File__c qpdfObj = new Quote_PDF_File__c( Quote_ID__c = quoteId,
	    												  File_Name__c = 'TestQuotePDF'
	                                    );
	    insert qpdfObj;
	    return qpdfObj;
    }
    
    
    public static Client_TM__c createClientTM(Id accId,Id sourceId,Id targetId){
        Client_TM__c clientObj = new Client_TM__c( Client_Name__c 			= accId,
        										   Source_Language_ID__c	= sourceId,
        										   Target_Language_ID__c 	= targetId,
        										   IsActive__c 				= true
    	 									   	 );
 		insert clientObj;
 		return clientObj;							   	 
    }
    
    public static Quote_Line_Item__c createQuoteLineItem(Id quoteId,Id countryId,Id sourceId,Id targetId){ 
        Quote_Line_Item__c qliObj = new Quote_Line_Item__c( QuoteID__c 				= quoteId,
        													Source_Language_ID__c 	= sourceId,
                                         					Target_Language_ID__c 	= targetId,
                                         					Country__c 				= countryId
                                         				  );
        insert qliObj;
        return qliObj;
    }
    
    public static Language_List_Item__c createLanguageListItem(String languageName){
    	Language_List_Item__c lliObj = new Language_List_Item__c( Language_Name__c = languageName
    															 );
    	insert lliObj;
    	return lliObj;														 
    }
   
    public static Language__c createLanguage(String description){
		Language__c languageObj = new Language__c(Description__c = description);	
	    insert languageObj;
	    return languageObj;
	}
	
	public static Invoice__c createInvoice(Id paId){
		Invoice__c invoiceObj = new Invoice__c(Purchase_Agreement_Id__c = paId);	
	    insert invoiceObj;
	    return invoiceObj;
	}
 	public static Country__c createCountry(String countryName){
        Country__c countryObj = new Country__c(Name         = countryName,
                                               GMT_Offset__c= 05.50
                                               );
        insert countryObj;
        return countryObj;
    }
	
	public static Country_Language__c createCountryLanguage(Id cId,Id lId ){
		Country_Language__c clObj = new Country_Language__c(Country__c = cId,
															Language__c = lId
															);
		insert clObj;
		return clObj;
	
	}
	 
    
    public static AWS_File__c createAWSFile(String sourceFilePath, String bucketName, String fileName, String recordType, String s3VersionId, Integer sfVersion){
        AWS_File__c awsObj = new AWS_File__c(Amazon_S3_source_file_path__c = sourceFilePath,
                                             Bucket_Name__c                = bucketName,
                                             File_Name__c                  = fileName,
                                             Record_Type__c                = recordType,
                                             version_id__c                 = s3VersionId,  
                                             Version__c                    = sfVersion
                                            );
        insert awsObj;
        return awsObj;
    }
}