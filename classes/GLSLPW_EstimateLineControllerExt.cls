/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSLPW_EstimateLineControllerExt Class
* Function: This class is used as a controller extension for the GLSLPW_EstimateLinePage
*/

public with sharing class GLSLPW_EstimateLineControllerExt {
    
    //Variable Declaration for the class
    public String protocolType                      {get; set;}
    public Boolean isNew                            {get; set;}
    public Boolean isNewELI                         {get; set;}
    public String newProtocol                       {get; set;} 
    public List<String> lstCountry                  {get; set;}
    public Set<String> setCountries                 {get; set;}
    public Map<String, Integer> mapcountrySites     {get; set;}
    public Estimate__c est                          {get; set;}
    public Estimate_Line_Item__c eli                {get; set;}
    public String countryName                       {get; set;}
    public transient String keyPrefix				{get; set;}
    public Id estId									{get; set;}
    
    private Id estLineId;
    private List<Estimate_Line_Country__c> lstELC;
    
    //Constructor
    public GLSLPW_EstimateLineControllerExt(ApexPages.StandardController stdcon){
    	keyPrefix = '';
    	Schema.DescribeSObjectResult res = Protocol__c.sObjectType.getDescribe();
		keyPrefix = res.getKeyPrefix();
        protocolType = '';
        isNew = true;
        isNewELI = false;
        newProtocol = '';
        lstCountry = new List<String>();
        eli = new Estimate_Line_Item__c();
        lstELC = new List<Estimate_Line_Country__c>();
        setCountries = new Set<String>();
        mapcountrySites = new Map<String, Integer>();
        estId = ApexPages.currentPage().getParameters().get('eid');
        estLineId = ApexPages.currentPage().getParameters().get('id');
        if(estId != null){
            est = getEstimateRecord(estId);
        }
        if(estLineId != null){
            isNewELI = true;
            eli = getEstimateLineRecord(estLineId);
        }
    }
    
    /* 
    @Description: Getter for obtaining the Estimnate__c record
    @Params: Estimate__c Id
    @Return Type: Estimate__c
    */
    private Estimate__c getEstimateRecord(Id estId){
        est = [SELECT  Id, Estimate_Amount__c, LP_Estimate_ID__c, Program_Id__c, Status__c
                FROM Estimate__c
                WHERE Id =: estId LIMIT 1];
        return est;
    }
    
    /* 
    @Description: Method for obtaining the Estimate_Line_Item__c and Estimate_Line_Country__c record
    @Params: Estimate_Line_Item__c Id
    @Return Type: Estimate_Line_Item__c
    */
    private Estimate_Line_Item__c getEstimateLineRecord(Id estLineId){
        eli = [SELECT Id, Protocol_Id__r.Protocol_Name__c 
                FROM Estimate_Line_Item__c
                WHERE Id =: estLineId LIMIT 1];
        
        lstELC = [SELECT Number_of_Sites__c, Country_Id__r.Name
                    FROM Estimate_Line_Country__c
                    WHERE Estimate_Line_Id__c =: estLineId];
        
        if(lstELC != null && lstELC.size() > 0){
            for(Estimate_Line_Country__c elc : lstELC){
                setCountries.add(elc.Country_Id__r.Name);
                mapcountrySites.put(elc.Country_Id__r.Name, Integer.valueOf(elc.Number_of_Sites__c));
            }
        }
        return eli;
    }
    
    /* 
    @Description: Removing the country from "Selected Countries" table if "Del" link is clicked.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference removeCountryAction(){
        if(countryName != ''){
            mapcountrySites.remove(countryName);
            setCountries.remove(countryName);
        }
        return null;
    }
    
    /* 
    @Description: Getter for obtaining the values of select list "Protocol Type" to be displayed on page
    @Params: None
    @Return Type: List<SelectOption>
    */
    public List<SelectOption> getProtocolTypeValues(){
        List<SelectOption> lstProtocolTypeValues = new List<SelectOption>();
        lstProtocolTypeValues = GLSconfig.protocolTypeConfig();
        return lstProtocolTypeValues;
    }
    
    /* 
    @Description: Method to identify if the field to be displayed for Protocol will be for new protocol or existing protocol
    @Params: None
    @Return Type: PageReference
    */
    public PageReference protocolNameField(){
        isNew = (protocolType == GLSconfig.NewQuote) ? true : false;
        return null;
    }
    
    /* 
    @Description: Getter for obtaining the values of select list "Country" to be displayed on page
    @Params: None
    @Return Type: List<SelectOption>
    */
    public List<SelectOption> getCountryValues(){
        List<Country__c> lstCountry = new List<Country__c>();
        List<SelectOption> lstCountryValues = new List<SelectOption>();
        lstCountryValues.add(new SelectOption('', '--None--'));
        for(Country__c ctry : [SELECT Id, Name FROM Country__c ORDER BY Name ASC]){
            lstCountryValues.add(new SelectOption(ctry.Name, ctry.Name));
        }
        return lstCountryValues;
    }
    
    /* 
    @Description: Method to add the countries to map on clicking "Add Country to List" button
    @Params: None
    @Return Type: PageReference
    */
    public PageReference addCountries(){
    	if(lstCountry != null && lstCountry.size() > 0){
    		for(String ctry : lstCountry){
		        setCountries.add(ctry);
		        mapcountrySites.put(ctry, 0);
    		}
    	}
    	else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, GLSconfig.selectCountry); 
            ApexPages.addMessage(myMsg);
        }
    	return null;
    }
    
    /* 
    @Description: Method to create Protocol, Estimate Line and Estimate Line Item Country on clicking "OK" button
    @Params: None
    @Return Type: PageReference
    */
    public PageReference saveAction(){
        PageReference pRef = null;
        
        //Form the map of Country:Record Id (further used to populate id values of country name in the lookup for inserting Line Country records)
        Map<String, Id> mapCountries = new Map<String, Id>();
        for(Country__c country : [SELECT Id, Name FROM Country__c]){
            mapCountries.put(country.Name, country.Id);
        }
        
        //Maps to store already existing Estimate Line Item Country:# of Sites and Country:ELIC Record Id records
        Map<String, Integer> mapCountrySitesLocal = new Map<String, Integer>();
        Map<String, Id> mapCountryIdLocal = new Map<String, Id>();
        
        try{
	        //If already created Estimate Line Item record is opened.
	        if(estLineId != null){
	            
	            List<Estimate_Line_Country__c> lstELCInsert = new List<Estimate_Line_Country__c>();
	            List<Estimate_Line_Country__c> lstELCUpdate = new List<Estimate_Line_Country__c>();
	            List<Estimate_Line_Country__c> lstELCDelete = new List<Estimate_Line_Country__c>();
	            
	            Estimate_Line_Country__c elcLocal = new Estimate_Line_Country__c();
	            if(lstELC != null && lstELC.size() > 0){
	                //Populate already existing ELIC records in the corresponding maps
	                for(Estimate_Line_Country__c elc : lstELC){
	                    mapCountrySitesLocal.put(elc.Country_Id__r.Name, Integer.valueOf(elc.Number_of_Sites__c));
	                    mapCountryIdLocal.put(elc.Country_Id__r.Name, elc.Id);
	                }
	                
	                if(mapcountrySites != null && mapcountrySites.size() > 0){
	                    for(String str : mapcountrySites.keyset()){
	                        if(mapCountrySitesLocal.containsKey(str)){
	                            if(mapcountrySites.get(str) != mapCountrySitesLocal.get(str)){
	                                elcLocal = new Estimate_Line_Country__c(Id = mapCountryIdLocal.get(str));
	                                elcLocal.Number_of_Sites__c = mapcountrySites.get(str);
	                                lstELCUpdate.add(elcLocal);
	                            }
	                        }
	                        else{
	                            elcLocal = new Estimate_Line_Country__c(Number_of_Sites__c = mapcountrySites.get(str),
	                                                                    Estimate_Line_Id__c = eli.Id,
	                                                                    Country_Id__c = mapCountries.containsKey(str) ? mapCountries.get(str) : null);
	                            lstELCInsert.add(elcLocal);
	                        }
	                    }
	                }
	                if(mapCountrySitesLocal != null && mapCountrySitesLocal.size() > 0){
	                    for(String str : mapCountrySitesLocal.keyset()){
	                        if(!mapcountrySites.containsKey(str)){
	                            elcLocal = new Estimate_Line_Country__c(Id = mapCountryIdLocal.get(str));
	                            lstELCDelete.add(elcLocal);
	                        }
	                    }
	                }
	            }
	            else{
	            	if(mapcountrySites != null && mapcountrySites.size() > 0){
		            	for(String str : mapcountrySites.keyset()){
			            	elcLocal = new Estimate_Line_Country__c(Number_of_Sites__c = mapcountrySites.get(str),
			                                                        Estimate_Line_Id__c = eli.Id,
			                                                        Country_Id__c = mapCountries.containsKey(str) ? mapCountries.get(str) : null);
			                lstELCInsert.add(elcLocal);
		            	}
	            	}
	            }
	            if(lstELCInsert != null && lstELCInsert.size() > 0){
	                insert lstELCInsert;
	            }
	            if(lstELCUpdate != null && lstELCUpdate.size() > 0){
	            	update lstELCUpdate;
	            }
	            if(lstELCDelete != null && lstELCDelete.size() > 0){
	                delete lstELCDelete;
	            }
	            pRef = new PageReference('/apex/GLSLPW_EstimateDetailPage?id=' + est.Id);
		        pRef.setRedirect(true);
	        }
	        //If new Estimate Line Item is to be created.
	        else{
	        	Boolean proceed = true;
	            Protocol__c protocol = new Protocol__c();
	            if(isNew){
	            	if(newProtocol != ''){
		                protocol = new Protocol__c(Protocol_Name__c = newProtocol,
		                                                        Program_Name__c = est.Program_Id__c);
		                insert protocol;
	            	}
	            	else{
	            		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, GLSconfig.selectProtocol); 
	            		ApexPages.addMessage(myMsg);
	            		proceed = false;
	            	}
	            }
	            
	            else{
	            	if(eli.Protocol_Id__c != null){
	                	protocol = new Protocol__c(Id = eli.Protocol_Id__c);
	            	}
	            	else{
	            		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, GLSconfig.selectProtocol); 
	            		ApexPages.addMessage(myMsg);
	            		proceed = false;
	            	}
	            }
	            
	            if(proceed){
		            Estimate_Line_Item__c eliNew = new Estimate_Line_Item__c(Estimate_Id__c = estId,
		                                                                  Protocol_Id__c = protocol.Id);
		            insert eliNew;
		    
		            List<Estimate_Line_Country__c> lstEstLineCountry = new List<Estimate_Line_Country__c>();
		            Estimate_Line_Country__c elc = new Estimate_Line_Country__c();
		            for(String country : mapcountrySites.keyset()){
		                elc = new Estimate_Line_Country__c ( Number_of_Sites__c = mapcountrySites.get(country),
		                                                    Estimate_Line_Id__c = eliNew.Id,
		                                                    Country_Id__c = mapCountries.containsKey(country) ? mapCountries.get(country) : null);
		                lstEstLineCountry.add(elc);
		            }
		            if(lstEstLineCountry != null && lstEstLineCountry.size() > 0){
		            	insert lstEstLineCountry;  
		            }
		            pRef = new PageReference('/apex/GLSLPW_EstimateDetailPage?id=' + est.Id);
		        	pRef.setRedirect(true);
		        }
	        }
	        if(est != null){
	        	if(est.Status__c == GLSconfig.ReviewedQuote){
            		est.Status__c = GLSconfig.InPrepStat;
            		update est;
            	}
	        }
    	}
    	catch(DMLException de){
            system.debug(GLSConfig.DMLExceptionOccured+de);
        }
        catch(Exception e){
            system.debug(GLSConfig.ExceptionOccured+e);
        }
        return pRef;
    }
    
}