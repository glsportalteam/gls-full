public class GLSCommunity_HelpDeskPageControllerExt {
		public User userObj 									{get;set;}
		public Case caseObj 									{get;set;}
		public CaseComment caseCommentObj 						{get;set;}
		public Quote_Assignment__c quoteAssignmentObj 			{get;set;}
		public Attachment document								{set;get{
														            	if (document == null)
														                document = new Attachment();
														                return document;
														            }
														 		}
	    public string caseSubject 								{get;set;}
	    public string caseDescription 							{get;set;}
	    public string replyCaseId								{get;set;}
    	public string replyComment 								{get;set;}
	    public list<case> lstcases 								{get;set;}
	    public list<CaseComment> lstCaseComments 				{get;set;}
	    public list<GLS_CommunityFAQ__c> lstFAQ					{get;set;}
	    public list<QuestionsWrapperClass> lstCaseAndComments 	{get;set;}
	    
	    
	    public GLSCommunity_HelpDeskPageControllerExt(ApexPages.StandardController stdcon){
	    				userObj=[SELECT Id,Email, UserType,Contact.Account.Name, Contact.FirstName,Contact.LastName ,LastLoginDate, 
	    						 ContactId, Contact.Account.Direct_Order_Allowed__c, AccountId, Contact.Name, Name, Contact.Title, 
	    						 Contact.Department, Contact.Phone, Contact.Email, Contact.Fax, Contact.Profile_Image__c, Contact.MailingPostalCode, 
	    						 Contact.MailingStreet, Contact.MailingState, Contact.MailingCity, Contact.MailingCountry, Contact.Cc_Email__c, 
	    						 Contact.Account.Owner.Name, Contact.Account.OwnerId, Contact.Temp_LastName__c , Contact.Temp_cc_email__c,  
	    						 Contact.Temp_Name__c, Contact.Temp_Phone__c, Contact.Temp_Fax__c, Contact.Initiate_Approval_Process__c, 
	    						 Contact.Temp_MailingPostalCode__c, Contact.Temp_MailingStreet__c, Contact.Temp_MailingState__c, Contact.Temp_MailingCity__c, 
	    						 Contact.Temp_MailingCountry__c  
	    						 FROM User 
	    						 WHERE Id = :userInfo.getUserId()limit 1];
						
	    				caseObj = new Case();
	    				caseDescription='';
	    	            caseSubject='';
	    	            caseCommentObj = new CaseComment();
	    	            lstFAQ= new list<GLS_CommunityFAQ__c> ();
	    }
	    
	    /**
        *   @Description : method to create a new question (Help desk)
        *   @Param : none
        **/
         public PageReference createNewQuestion(){
            if( caseSubject != null &&  caseDescription != null &&  caseSubject != '' && caseDescription !=''  ){
                caseObj.Subject = caseSubject;
                caseObj.Description = caseDescription;
                caseObj.Origin ='Community Portal'; //test, to be removed later
                caseObj.Status ='New'; //test, to be removed later
                caseObj.ContactId = userObj.ContactId;
                insert caseObj;
                obtainlstCaseAndComments();
                caseObj = new case();
            }
            return null;
        }
        
        /**
        *   @Description :Method to fetch the existing Questions asked and related comments
        *   @Param : none
        **/
        public void obtainlstCaseAndComments(){
            lstcases = [SELECT c.Id, c.Subject, c.Description, c.CreatedById, c.Status, c.createdDate 
            			FROM Case c 
            			WHERE ContactId =: userObj.ContactId AND c.Origin =: 'Community Portal'  
            			ORDER BY createdDate Desc ];
            if(lstcases == null){
                lstcases = new list<case>();
            }
            
            list<ID> caseIds = new list<ID>();
            for(Case tempCase : lstcases){
                string idd = String.valueOf(tempCase.Id).subString(0,15);
                caseIds.add(idd);
            }
            lstCaseComments = [SELECT c.ParentId, c.Id, c.CreatedBy.Name, c.CreatedDate, c.CreatedById,   c.CommentBody 
            				   FROM CaseComment c 
            				   WHERE ParentId IN :caseIds order by createdDate];
            if(lstCaseComments == null){
                lstCaseComments = new list<caseComment>();
            }
            
            if(lstCaseComments != null){
                lstCaseAndComments = new list<QuestionsWrapperClass>();
                list<CaseComment> lstCommentTemp ;
                for(case casetemp : lstcases){
                    lstCommentTemp = new list<CaseComment>();
                    for(CaseComment cComment : lstCaseComments){
                        if(casetemp.Id == cComment.ParentId){
                            lstCommentTemp.add(cComment);
                        }
                    }   
                    lstCaseAndComments.add(new QuestionsWrapperClass(casetemp, lstCommentTemp));            
                }
            }
        }
        
        /**
        *   @Description :Method to save comment entered by user (help desk)
        *   @Param : none
        **/
        public pageReference createNewCommentForCase(){
            caseCommentObj = new CaseComment();
            Case parentCase = new Case();
            if(replyCaseId !=null && replyComment !=null && replyCaseId !='' && replyComment !=''){
            boolean isClosed = false;
                for(Case cs :lstcases){
                    if(cs.Id == replyCaseId){
                        if(cs.Status == 'Closed'){
                            isClosed = true;
                            parentCase = cs;
                        }
                    }
                }
                if(!isClosed){
                    caseCommentObj.CommentBody = replyComment;
                    caseCommentObj.ParentId = replyCaseId;
                    insert caseCommentObj;
                }else{
                    caseObj = new case();
                    caseObj.Subject = parentCase.Subject + ' [Reopened] ';
                    caseObj.Description = parentCase.Description;
                    caseObj.Origin ='Community Portal'; //test, to be removed later
                    caseObj.Status ='New'; //test, to be removed later
                    caseObj.Parent = parentCase;
                    caseObj.ContactId = userObj.ContactId;  
                    insert caseObj;
                    caseCommentObj.CommentBody = replyComment;
                    caseCommentObj.ParentId = caseObj.Id;
                    insert caseCommentObj;
                    caseObj = new case();
                }
                obtainlstCaseAndComments();
            }
           return null;
            
        }
        
        /**
        *   @Description :Method to fetch custom settings for FAQ
        *   @Param : none
        **/
        public void obtainFAQ(){
            lstFAQ = [Select Question__c, Solution__c from GLS_CommunityFAQ__c];
            if(lstFAQ == null){
                lstFAQ = new list<GLS_CommunityFAQ__c>();
            }
            
        }
        
        /**
        *   @Description :Method to refresh QuoteAssignmentObj
        *   @Param : Id
        **/
        public void refreshQuoteAssignmentObj(Id asId){
            quoteAssignmentObj = [SELECT id,Quote__c,Additional_Note__c,Keywords__c,Source_Language__c,AssignmentNumber1__c,Step_one_complete__c,Step_two_complete__c 
            					  FROM Quote_Assignment__c 
            					  WHERE Id=:asId LIMIT 1 ];
        }
        
        /**
        *   @Description :Method to refresh FAQ
        *   @Param : none
        **/
        public void refreshFAQ(){
            obtainlstCaseAndComments();
            obtainFAQ();
        }
        
        
        /**
        *   @Description :Wrapper class for Case, CaseComments and Attachments
        *   @Param : none
        **/
        public class QuestionsWrapperClass{
            public case caseData 						{get;set;}
            public list<CaseComment> lstCaseComments 	{get;set;} 
            
            /*public QuestionsWrapperClass(){
                caseData = new case();
                lstCaseComments = new list<CaseComment>();
            }*/
            
            public QuestionsWrapperClass(case cse, list<CaseComment> lstcscom){
                caseData = new case();
                lstCaseComments = new list<CaseComment>();
                caseData =cse;
                lstCaseComments = lstcscom;
            }
        }
}