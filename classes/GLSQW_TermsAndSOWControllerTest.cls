/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 */ 
 
@isTest 
private class GLSQW_TermsAndSOWControllerTest {     
      /**
       * This Unit Test method  
     */
    static testMethod void myUnitTest1() {    
       GLSSetupData.setupData();
        test.startTest();
           ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
           ApexPages.currentPage().getParameters().put('standardTermsId',GLSSetupData.rfwSOW1.id);
           GLSQW_TermsAndSOWController termsAndSowController = new GLSQW_TermsAndSOWController();
           termsAndSowController.getItems();
           termsAndSowController.updateSOW();
           termsAndSowController.updateSOWTerms();
           termsAndSowController.updateTerms();
        test.stopTest();
    }
    static testMethod void myUnitTest2() {    
       GLSSetupData.setupData();
        test.startTest();
           ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.id);
           ApexPages.currentPage().getParameters().put('standardTermsId','');
           GLSQW_TermsAndSOWController termsAndSowController = new GLSQW_TermsAndSOWController();
           termsAndSowController.updateSOWTerms();
        test.stopTest();
    }
 }