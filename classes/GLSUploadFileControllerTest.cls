/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 */
 
@isTest
private class GLSUploadFileControllerTest {
       
       
    /**
    * This Unit Test method will test the constructor 
    **/
    static testMethod void myUnitTest() { 
        GLSSetupData.setupData();
        Test.startTest();
         
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.Id);
        GLSUploadFileController uploadFile = new GLSUploadFileController(new ApexPages.StandardController(GLSSetupData.quote));
       /*String credName = createTestCredentials();
            uploadFile.AWSCredentialName = credName;
            uploadFile.getawss3LoginCredentials();*/
        system.assert( uploadFile.Account_Name != null );
        System.runAs(GLSSetupData.u){ 
         uploadFile = new GLSUploadFileController(new ApexPages.StandardController(GLSSetupData.quote));
       }
        Test.stopTest();
    }

    /**
    * This Unit Test method will test successful creation of Quote file record in Salesforce 
    **/
    static testMethod void successFileuploadTest() {
        GLSSetupData.setupData();
        Test.startTest();
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.Id);
        GLSUploadFileController uploadFile = new GLSUploadFileController(new ApexPages.StandardController(GLSSetupData.quote));
        uploadFile.newFileName='Test.txt';
        uploadFile.fileSize=9000;
        uploadFile.AmazonPath = 'Test Amazon path';
        uploadFile.successFileupload();
        System.assertEquals(uploadFile.ext[0].Name,'Text');
        Test.stopTest();
    }
    /**
    * This method will create dummy credentials for testing 
    **/
    private static String createTestCredentials(){
        AWS_Credential__c testKey = new AWS_Credential__c(name='test keys',key__c='key',secret__c='secret');
        insert testKey;
        return testKey.name;
     } 
    /**
    * This method run amazon's signature's and policy tester
    **/
    static testmethod void policyTest() {
            GLSSetupData.setupData();
            Test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.Id);
            GLSUploadFileController uploadFile = new GLSUploadFileController(new ApexPages.StandardController(GLSSetupData.quote));
            String credName = createTestCredentials();
            uploadFile.AWSCredentialName = credName;
            uploadFile.getawss3LoginCredentials();
            try{
                uploadFile.getHexPolicy( );
                uploadFile.getSignedPolicy();
            }catch(Exception ex){
            }
           system.assert( uploadFile.getHexPolicy() != null );
           system.assert( uploadFile.getSignedPolicy() != null );
           Test.stopTest(); 
     }

     static testmethod void recallGEFAPITest() {
            GLSSetupData.setupData();
            Test.startTest();
            ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.Id);
            GLSUploadFileController.recallGEFAPI(GLSSetupData.qFiles1.Id);
            system.assert(GLSUploadFileController.lstTradosConf != null);
            Test.stopTest(); 
     }
    }