global class GLSDeleteQuoteFilesSchedulerClass implements Schedulable{
   /* String qliId;
    global GLSAnalyseSchedulerClass(String qliId){
      //this.lstQLIId = lstQLIId;
      this.qliId = qliId;
    }*/
    
    global void execute(SchedulableContext sc){
       // String query = 'SELECT Analyzed_File_URL__c,Analyzed_Transaction_ID__c,test__c,Id,isProcessed__c,XML_Data__c,QuoteID__c FROM Quote_Line_Item__c WHERE Id =\''+qliId+'\'';
        String query = 'Select Id,Amazon_S3_source_file_path__c, File_Name__c,Quote__r.Name,Quote__r.Quote_Number__c, Quote__r.Client__r.Name,Quote__r.Id,Bucket_Name__c from Quotes_File__c  limit 1';        
        system.debug('---------query-----'+query);
        GLSDeleteQuoteFilesBatchClass deleteQuoteFileInstance = new GLSDeleteQuoteFilesBatchClass();
        Id batchprocessid = Database.executeBatch(deleteQuoteFileInstance ,10); 
    }
}