/* Copyright (c) 2013, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSUploadFileController  Class
* Function: Upload files to Amazon S3 server and create corresponding Quote file record for the uploaded file
*/

public class GLSQW_UploadDeliverablesController{

    
    public string QuoteId_value{get;set;}
    public S3.AmazonS3 as3 { get; private set; }
    private boolean flag=false;
    public string currQuote;
    public string userType{get;set;}
    public string Quote_Name{get;set;}
    public string Account_Name{get;set;}
    public string newFileName{get;set;}
    public Decimal fileSize {get;set;}
    public string AmazonPath{get;set;}     
    public string VersionId{get;set;}
    public string bucket{get;set;}
    public Quotes_File__c record{get;set;} 
    public AWSKeys credentials {get;set;}
    public String AWSCredentialName = 'NAME OF KEY TO USE'; //Modify this string variable to be the name of the AWS Credential record that contains the proper AWS keys and secret
    public string secret { get {return credentials.secret;} }
    public string key { get {return credentials.key;} }
    public List<string>lstQuoteFileName {get;set;}
    public list<Quotes_File__c> lstQuoteFile;
    public List<File_Format__c> ext;
    public boolean isAnyFileUploaded {get;set;}
    public List<Quotes_File__c> lstDeliverableFiles{get;set;}
    public list<FileWrapperClass>lstQuoteFilesWrapper {get;set;}
    public list<FileWrapperClass>lstDeleteQuoteFilesWrapper {get;set;}
    public string saveId {get;set;}
    public string deleteId {get;set;}
    public list<SelectOption> lstLanguagePair;
    public boolean selectAll {get;set;}
       
             
         public GLSQW_UploadDeliverablesController() {
            lstDeleteQuoteFilesWrapper = new list<FileWrapperClass>();
            selectAll = false;
           System.debug('Consturctor'); 
           lstQuoteFile= new list<Quotes_File__c>();
           lstQuoteFileName= new list<String>();
           getawss3LoginCredentials();
           
           QuoteId_value = ApexPages.currentPage().getParameters().get('id');
           if(QuoteId_value.length()!=15)
           QuoteId_value=QuoteId_value.subString(0,15);
           Quote__c currQuote=[select id,Name,Quote_Number__c, Client__r.Name,Bucket_Name__c from Quote__c where id=:QuoteId_value limit 1];
           lstQuoteFile=[select id,File_Name__c from Quotes_File__c where Quote__c =:QuoteId_value];
           if(lstQuoteFile.size()>0){
            
            for(Quotes_File__c qf:lstQuoteFile){
                lstQuoteFileName.add(qf.File_Name__c);
            }
           }
         //  Quote_Name=currQuote.Name;
           Quote_Name=currQuote.Quote_Number__c;
           bucket=currQuote.Bucket_Name__c;
           this.record = new Quotes_File__c(); 
           User u=[SELECT Id,UserType,Contact.Account.Name  FROM User WHERE Id = :userInfo.getUserId()limit 1];
           userType=u.UserType;
           if(u.UserType=='Standard'){
              Account_Name=currQuote.Client__r.Name;
           }else if(u.UserType=='PowerPartner'){
              Account_Name=u.Contact.Account.Name;
           }
           obtainDeliverablesList();
        }
        
       /**
        *   @Description : This method sets Aws s3 credentials
        *   @Param : none
        **/
        
    public PageReference getawss3LoginCredentials(){
    
        try{

            credentials = new AWSKeys(AWSCredentialName);
            as3 = new S3.AmazonS3(credentials.key,credentials.secret);
                
        }catch(AWSKeys.AWSKeysException AWSEx){

            System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
            ApexPages.addMessage(errorMsg);
            //throw new AWSKeys.AWSKeysException(AWSEx);
            //ApexPages.addMessage(AWSEx);    
        } 
       return null;
    }
    Datetime expire = system.now().addDays(1);
        String formattedexpire = expire.formatGmt('yyyy-MM-dd')+'T'+
                expire.formatGmt('HH:mm:ss')+'.'+expire.formatGMT('SSS')+'Z';           
              
        string policy { get {return 
                '{ "expiration": "'+formattedexpire+'","conditions": [ {"bucket": "'+
        bucket +'" } ,{ "acl": "'+
       'private'+'" },'+
       '{"success_action_status": "201" },'+
       '["starts-with", "$key", ""] ]}';       } }  
 
       /**
        *   @Description : This method returns s3 policies
        *   @Param : none
        **/
    
        public String getPolicy() {
            return EncodingUtil.base64Encode(Blob.valueOf(policy));
        }
    
       /**
        *   @Description : This method returns signature
        *   @Param : none
        **/
    
        public String getSignedPolicy() {    
            return make_sig(EncodingUtil.base64Encode(Blob.valueOf(policy)));        
        }
    
       /**
        *   @Description : This method returns encoded policy 
        *   @Param : none
        **/
    
        public String getHexPolicy() {
            String p = getPolicy();
            return EncodingUtil.convertToHex(Blob.valueOf(p));
        }
    
       /**
        *   @Description : This method returns encrypted signature.
        *   @Param : String Canonical Buffer
        **/
    
        private String make_sig(string canonicalBuffer) {        
            String macUrl ;
            String signingKey = EncodingUtil.base64Encode(Blob.valueOf(secret));
            Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof(Secret)); 
            macUrl = EncodingUtil.base64Encode(mac);                
            return macUrl;
        } 
    
       /**
        *   @Description : This method saves Quote File Record
        *   @Param : none
        **/
          
        public void successFileupload() {
            if(QuoteId_value!=null)
            {     
               string extension;
               ext= new List<File_Format__c>();
               if(newFileName.lastIndexOf('.')!= -1){
                    extension=newFileName.substring(newFileName.lastIndexOf('.'),newFileName.length());
                    ext=[select id,File_Extension__c,Name from File_Format__c where File_Extension__c INCLUDES(:extension) limit 1];
                    Quotes_File__c objRecord= new Quotes_File__c();
                    objRecord.Quote__c = QuoteId_value;
                    objRecord.File_Name__c = newFileName;
                    objRecord.Bucket_Name__c = bucket;
                    fileSize = (fileSize/1024);
                    objRecord.File_Size__c = fileSize;
                    if(ext.size() > 0 ){
                        objRecord.File_Format__c=ext[0].id;
                    }
                    objRecord.Amazon_S3_source_file_path__c = AmazonPath;
                    decimal maxversion=0;
                    List <Quotes_File__c> File_list = new List <Quotes_File__c>();
                    
                    try{
                        File_list= [SELECT Id, Version__c FROM Quotes_File__c WHERE File_Name__c = :newFileName and Quote__c = :QuoteId_value];    
                    }catch(QueryException de){
                        System.debug('Query Exception occured.');
                    }catch(Exception e){
                        System.debug('Exception occured.');
                    }
                    for (Quotes_File__c temp : File_list) {
                        if(maxversion<temp.Version__c)
                              maxversion=temp.Version__c;
                              objRecord.Id= temp.Id; 
                    }
                    objRecord.Version__c=maxversion+1;
                    objRecord.version_id__c=VersionId;
                    objRecord.File_Type__c ='Deliverable';
                   
                    try{
                        upsert (objRecord);
                        flag = true;
                     }catch(DmlException de){
                        flag = false;
                        System.debug('DML Exception occured.');
                     }catch(Exception e){
                        flag = false;
                        System.debug('Exception occured.');
                     }
                     
                    /**
                    * This part maintaining Version of the files. Creting new records in Quote Files Version object
                    * /
                     if(flag){
                        Quote_File_Version__c qFversion= new Quote_File_Version__c();
                        qFversion.Quote_Files__c = objRecord.Id;
                        qFversion.Name = newFileName;
                        qFversion.Bucket_Name__c = bucket;
                        if(ext.size() > 0 )
                            qFversion.File_Format__c=ext[0].id;
                       qFversion.Amazon_S3_source_file_path__c = AmazonPath;
                        maxversion = 0;
                        List <Quote_File_Version__c> lsQFversion = new List <Quote_File_Version__c>();
                        
                        try{
                            lsQFversion= [SELECT SF_Version__c FROM Quote_File_Version__c WHERE Name = :newFileName and Quote_Files__c = :objRecord.Id];    
                        }catch(QueryException de) {
                            System.debug('Query Exception occured.');
                        }catch(Exception e){
                            System.debug('Exception occured.');
                        }
                        for (Quote_File_Version__c temp : lsQFversion) {
                             if(maxversion < temp.SF_Version__c)
                                 maxversion=temp.SF_Version__c;
                        }
                        qFversion.SF_Version__c = maxversion+1;
                        qFversion.S3_Version_Id__c = VersionId;
                       
                        try{
                            insert qFversion;
                        }catch(DmlException de){
                            System.debug('DML Exception occured.');
                        }catch(Exception e){
                            System.debug('Exception occured.');
                       }
                    }*/
                  } 
               }
            
        }
        
         /**
        *   @Description : This method gets the list of deliverables
        *   @Param : none
        **/
    
        public pageReference obtainDeliverablesList(){
             lstDeliverableFiles = new List<Quotes_File__c>();
             lstQuoteFilesWrapper = new list<FileWrapperClass>();
             /*lstLanguagePair = new List<SelectOption>();
             list<Quote_Line_Item__c> lstQliItems = [Select Trados_Language_Pair__c from Quote_Line_Item__c where QuoteID__c =: QuoteId_value];
             if(lstQliItems != null && lstQliItems.size() > 0){
                for(Quote_Line_Item__c qli : lstQliItems){
                     lstLanguagePair.add(new SelectOption(qli.Trados_Language_Pair__c,qli.Trados_Language_Pair__c));
                }
             }*/
             
             if(QuoteId_value != null && QuoteId_value != ''){
             
                 lstDeliverableFiles = [Select q.version_id__c, q.Version__c, q.TradosAPITransactionID__c, q.TradosAPIFileID__c, q.SystemModstamp, q.Success__c, 
                                          q.SF_Preview_Link__c, q.Quote__c, q.Quote_File_Id__c, q.Preview_Link__c, q.OwnerId, q.Number_of_Pages__c, q.Name, q.MapKey__c, 
                                          q.File_Type__c, q.LastModifiedDate, q.LastModifiedById, q.LastActivityDate, q.IsDeleted, q.Id, 
                                          q.File_Format__c, q.Language_Pair__c, q.File_Description__c, q.Error_Message__c, q.CreatedDate, q.CreatedById, q.Bucket_Name__c, 
                                          q.Amazon_S3_source_file_path__c,q.Released__c,q.File_Size__c, q.Access__c, q.File_Format__r.File_Extension__c, q.File_Format__r.Name, q.Link_To_File__c, q.File_Name__c,
                                          q.Quote__r.Quote_Number__c, q.Quote__r.Client__r.Name, q.Release_date__c 
                                          From Quotes_File__c q where Quote__c =: QuoteId_value and File_Type__c =: 'Deliverable' order by File_Name__c];
             }
             system.debug(lstDeliverableFiles);
             if(lstDeliverableFiles != null && lstDeliverableFiles.size() > 0){
                for(Quotes_File__c qFile :lstDeliverableFiles){
                   lstQuoteFilesWrapper.add(new FileWrapperClass(qFile, false));
                }
                
                
                isAnyFileUploaded = true;
             }
             else{
                lstDeliverableFiles = new List<Quotes_File__c>();
                lstQuoteFilesWrapper = new list<FileWrapperClass>();
                isAnyFileUploaded = false;
             }
             

             return null;
        }
        
        
        
        public List<SelectOption> getlstLanguagePair(){
            lstLanguagePair = new List<SelectOption>();
             list<Quote_Line_Item__c> lstQliItems = [Select Trados_Language_Pair__c from Quote_Line_Item__c where QuoteID__c =: QuoteId_value];
             if(lstQliItems != null && lstQliItems.size() > 0){
                lstLanguagePair.add(new SelectOption('None','None'));
                for(Quote_Line_Item__c qli : lstQliItems){
                     lstLanguagePair.add(new SelectOption(qli.Trados_Language_Pair__c,qli.Trados_Language_Pair__c));
                }
             }
             return lstLanguagePair;
        }
        
         /**
        *   @Description : This method deletes deliverable
        *   @Param : none
        **/

        public pageReference deleteDeliverable(){
            if(deleteId != null && lstQuoteFilesWrapper !=null ){
                integer deleteIndex;
                String path = '';
                string StatusMessage='';
                string StMessage='';
                for(integer i=0; i<lstQuoteFilesWrapper.size(); i++){
                    if(lstQuoteFilesWrapper[i].quoteFile.Id == deleteId ){
                        String qID =lstQuoteFilesWrapper[i].quoteFile.Id; 
                        qID = qID.substring(0,15);
                        path= lstQuoteFilesWrapper[i].quoteFile.Amazon_S3_source_file_path__c;
                        //lstQuoteFilesWrapper[i].quoteFile.Quote__r.Client__r.Name+'/'+lstQuoteFilesWrapper[i].quoteFile.Quote__r.Quote_Number__c+'_'+qID+'/Final/'+lstQuoteFilesWrapper[i].quoteFile.File_Name__c;
                        system.debug(path + ' ++++++ path');
                        try{
                            StatusMessage = GLSAmazonUtility.deleteQuoteFile(deleteId,path,lstQuoteFilesWrapper[i].quoteFile.Bucket_Name__c);
                            if(StatusMessage.equalsIgnoreCase('true'))
                                StMessage ='File Deleted Successfully';
                            else
                                StMessage ='File not deleted';
                            deleteIndex= i;
                        }catch(Exception e){
                           // StMessage = e.getMessage();
                            system.debug(GLSConfig.ExceptionOccured);
                        }
                        ApexPages.Message statusMsg = new ApexPages.Message(ApexPages.Severity.INFO, StMessage);
                        ApexPages.addMessage(statusMsg);
                        //delete lstQuoteFilesWrapper[i];
                        break;
                    }
                                        
                }
                lstQuoteFilesWrapper.remove(deleteIndex);
                
            }
            return null;
        }
        
         /**
        *   @Description : This method deletes deliverable
        *   @Param : none
        **/

        public pageReference saveDeliverable(){
                        
            if(saveId != null && lstDeliverableFiles !=null ){
                for(Quotes_File__c quoteFile : lstDeliverableFiles){
                    if(quoteFile.Id == saveId ){
                        update quoteFile;
                    }
                }
                
            }
            return null;
        }
        
        
        
        
        
        /**
        *   @Description : Save all 
        *   @Param : none
        **/
        
        public pageReference saveAllDeliverable(){
                        
            if(lstQuoteFilesWrapper !=null ){
            list<Quotes_File__c> updateQuoteFile = new list<Quotes_File__c>();  
            
                for(FileWrapperClass fWrapper : lstQuoteFilesWrapper){
                    if(fWrapper.quoteFile.Released__c == true && fWrapper.quoteFile.Release_Date__c == null ){
                        fWrapper.quoteFile.Release_Date__c = DateTime.Now();
                    }else if(fWrapper.quoteFile.Released__c == false){
                        fWrapper.quoteFile.Release_Date__c = null;
                    }
                    
                    updateQuoteFile.add(fWrapper.quoteFile);
                }
                update updateQuoteFile; 
            }
            return null;
        }
        
        /**
        *   @Description : Delete all 
        *   @Param : none
        **/
        public boolean areFilesLeft {get;set;}
        public pageReference deleteAllDeliverable(){
                         
            if(lstQuoteFilesWrapper !=null && lstDeleteQuoteFilesWrapper != null && lstQuoteFilesWrapper.size() > 0  && lstDeleteQuoteFilesWrapper.size() == 0){
                
                for(integer i=0; i<lstQuoteFilesWrapper.size(); i++){
                    if(lstQuoteFilesWrapper[i].isSelected == true){
                        lstDeleteQuoteFilesWrapper.add(new FileWrapperClass(lstQuoteFilesWrapper[i].quoteFile, true) );
                        system.debug('Selected Files '+ i +' : ' +lstQuoteFilesWrapper[i].quoteFile.File_Name__c );
                    }
                }
            }
            
            if(lstDeleteQuoteFilesWrapper !=null && lstDeleteQuoteFilesWrapper.size() > 0 ){
                String path = '';
                string StatusMessage='';
                string StMessage='';
                for(integer i=0; i<lstDeleteQuoteFilesWrapper.size(); i++){
                    
                    //if(lstDeleteQuoteFilesWrapper[i].isSelected == true){
                        lstDeleteQuoteFilesWrapper[i].isSelected =false;
                        String qID =lstDeleteQuoteFilesWrapper[i].quoteFile.Id; 
                        qID = qID.substring(0,15);
                        path=  lstQuoteFilesWrapper[i].quoteFile.Amazon_S3_source_file_path__c;
                        //lstDeleteQuoteFilesWrapper[i].quoteFile.Quote__r.Client__r.Name+'/'+lstDeleteQuoteFilesWrapper[i].quoteFile.Quote__r.Quote_Number__c+'_'+qID+'/Final/'+lstDeleteQuoteFilesWrapper[i].quoteFile.File_Name__c;
                        //try{
                            StatusMessage = GLSAmazonUtility.deleteQuoteFile(lstDeleteQuoteFilesWrapper[i].quoteFile.Id,path,lstDeleteQuoteFilesWrapper[i].quoteFile.Bucket_Name__c);
                            if(StatusMessage.equalsIgnoreCase('true')){
                                StMessage ='File Deleted Successfully';
                                for(integer k=0; k<lstQuoteFilesWrapper.size(); k++){
                                    if(lstQuoteFilesWrapper[k].quoteFile.Id == lstDeleteQuoteFilesWrapper[i].quoteFile.Id){
                                        lstQuoteFilesWrapper.remove(k);
                                    }
                                }
                                system.debug('Deleted ' +lstDeleteQuoteFilesWrapper[i].quoteFile.File_Name__c );
                                lstDeleteQuoteFilesWrapper.remove(i);
                            }
                            else
                                StMessage ='File not deleted';
                        break;
                            
                      //  }catch(Exception e){
                      //      StMessage = e.getMessage();
                      //     system.debug(GLSConfig.ExceptionOccured);
                      //  }
                        //ApexPages.Message statusMsg = new ApexPages.Message(ApexPages.Severity.INFO, StMessage);
                        //ApexPages.addMessage(statusMsg);
                        //delete lstDeliverableFiles[i];
                    //}
                }
                
                
                
                
                //obtainDeliverablesList();
                if(lstDeleteQuoteFilesWrapper !=null && lstDeleteQuoteFilesWrapper.size() > 0){
                    /*boolean flag=false;
                    for(integer j= 0; j<lstDeleteQuoteFilesWrapper.size(); j++ ){
                        if(lstDeleteQuoteFilesWrapper[j].isSelected ==true){
                            flag = true;
                            break; 
                        }
                    }*/
                    areFilesLeft = true;
                    
                }else{
                    areFilesLeft = false;
                    StMessage ='Selected Files Deleted Successfully';
                    selectAll=false;
                    ApexPages.Message statusMsg = new ApexPages.Message(ApexPages.Severity.INFO, StMessage);
                    ApexPages.addMessage(statusMsg);
                }
                
            }
            return null; 
        }
        
        public void selectAllDeliverables(){
            if(selectAll == true){
                if(lstQuoteFilesWrapper !=null && lstQuoteFilesWrapper.size() > 0  ){
                    for(integer i=0; i<lstQuoteFilesWrapper.size(); i++){
                        lstQuoteFilesWrapper[i].isSelected = true;
                    }
                }
            }else{
                if(lstQuoteFilesWrapper !=null && lstQuoteFilesWrapper.size() > 0  ){
                    for(integer i=0; i<lstQuoteFilesWrapper.size(); i++){
                        lstQuoteFilesWrapper[i].isSelected = false;
                    }
                }
            }
        }


         public class FileWrapperClass{
            public Quotes_File__c quoteFile {get;set;} 
            public boolean isSelected {get;set;}
           
            
            public FileWrapperClass(Quotes_File__c quoteFile, boolean isSelected){
                this.quoteFile = new Quotes_File__c();
                this.quoteFile = quoteFile;
                this.isSelected = isSelected;
            }
             
         }
        

}