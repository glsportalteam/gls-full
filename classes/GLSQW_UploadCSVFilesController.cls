public with sharing class GLSQW_UploadCSVFilesController{
    
    public transient Quote_Line_Item__c qliUpdate     {get;set;}
   // public transient string qliID                     {get;set;}
    public transient string quoID                     {get;set;}
    public String quoteId_Value             {get;set;}
    public String Quote_Name             {get;set;}
    public String Account_Name             {get;set;}
    public S3.AmazonS3 as3                  {get; private set;}
    public string userType                  {get;set;}
    public string newFileName               {get;set;}
    public string AmazonPath                {get;set;} 
    public string path                {get;set;} 
    public string bucket                    {get;set;}
    public AWSKeys credentials              {get;set;}
    public String AWSCredentialName = 'NAME OF KEY TO USE'; //Modify this string variable to be the name of the AWS Credential record that contains the proper AWS keys and secret
    public String secret            {get{return credentials.secret;}}       
    public string key;                      
    public List<File_Format__c> ext;
    public string currentSignature          {get;set;}
    public Quote__c currentQuote            {get;set;}
    public String ParseResult ='';
    public String csvDataToParse ='';
    GLSXMLParser glsxmlParser = new GLSXMLParser();
    GLSCSVParser glsCSVParser = new GLSCSVParser();
    public Boolean disableBtn {get; set;}
    public List<Quote_Line_Item__c> qliList{get;set;}
   // public Map<String,Integer> langPairMap{get;set;}
    public List<String> langImportStatusList{get;set;}
    
    public String getkey(){
        if(credentials != null){
            return credentials.key;
        }else{
            return '';
        }
    }
    
    Datetime expire = system.now().addDays(1);
    String formattedexpire = expire.formatGmt('yyyy-MM-dd')+'T'+expire.formatGmt('HH:mm:ss')+'.'+expire.formatGMT('SSS')+'Z';                    
    String policy { get {return 
            '{ "expiration": "'+formattedexpire+'","conditions": [ {"bucket": "'+bucket +'" } ,{ "acl": "'+'public-read'+'" },'+'{"success_action_status": "201" },'+
            '["starts-with", "$key", ""] ]}';       } }
       
    public GLSQW_UploadCSVFilesController(ApexPages.StandardController controller) {
          Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=Edge'); 
       // qliID = ApexPages.currentPage().getParameters().get('id');
        quoID = ApexPages.currentPage().getParameters().get('quoteId');
        qliList = new List<Quote_Line_Item__c>();
        
        
        system.debug('----quoID----'+quoID);
        disableBtn = true;
        if(quoID.length() != 15)
           quoID = quoID.subString(0,15);
        try{
               qliList = [Select of_Pages__c, isTMFound__c, isReadyForAnalyse__c, isProcessed__c, isAnalyzed__c, XML_Data__c, X95_99_Word_Count__c, 
                         Analyzed_Version_ID__c, Analyzed_Transaction_ID__c, Analyzed_File_URL__c, Additional_Line_Items__c, Add_Outsourced_Services__c, Add_Discount__c, Actual_Words__c,Memsource_Language_Pair__c From Quote_Line_Item__c Where QuoteID__c =: quoID ];
            
           if(qliList != null && qliList.size() > 0){
           
               
                getawss3LoginCredentials();
               
                quoteId_Value = quoID;
                currentQuote = [Select Id,Bucket_Name__c,Quote_Number__c, Client__r.Name, Name, Status__c From Quote__c Where Id =: quoID limit 1];
                Quote_Name = currentQuote.Quote_Number__c;
                bucket = currentQuote.Bucket_Name__c;
                User u = [SELECT Id,UserType,Contact.Account.Name  FROM User WHERE Id = :userInfo.getUserId() limit 1];
                userType = u.UserType;
                if(u.UserType=='Standard'){
                  Account_Name=currentQuote.Client__r.Name;
               }else if(u.UserType=='PowerPartner'){
                  Account_Name=u.Contact.Account.Name;
               }
               
            }
            if(currentQuote != null && GLSConfig.btnEnableStatus.contains(currentQuote.Status__c)){
                disableBtn = false;
            }
        }
        catch(Exception e){
            System.debug('Exception in Query: '+e.getMessage());
            System.debug('Exception in Query: '+e.getLineNumber());
        }
    }
    
    /**
    *   @Description : This method sets Aws s3 credentials
    *   @Param : none
    **/       
    public PageReference getawss3LoginCredentials(){  
    
        try{
            credentials = new AWSKeys(AWSCredentialName);
            as3 = new S3.AmazonS3(credentials.key,credentials.secret);              
        }catch(AWSKeys.AWSKeysException AWSEx){
            System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
            ApexPages.addMessage(errorMsg);
        } 
        return null;
    }
    
    /**
    *   @Description : This method returns s3 policies
    *   @Param : none
    **/
    
    public String getPolicy() {
        return EncodingUtil.base64Encode(Blob.valueOf(policy));
    }
    
    /**
    *   @Description : This method returns signature
    *   @Param : none
    **/
    
    public String getSignedPolicy() {    
        return make_sig(EncodingUtil.base64Encode(Blob.valueOf(policy)));        
    }

    /**
    *   @Description : This method returns encoded policy 
    *   @Param : none
    **/
    
    public String getHexPolicy() {
        String p = getPolicy();
        return EncodingUtil.convertToHex(Blob.valueOf(p));
    }

    /**
    *   @Description : This method returns encrypted signature.
    *   @Param : String Canonical Buffer
    **/
    
    private String make_sig(string canonicalBuffer) { 
           
        String macUrl = '';
        String signingKey;
        Blob mac;
        if(!Test.isRunningTest()){
            if(secret != null && secret != ''){
                signingKey = EncodingUtil.base64Encode(Blob.valueOf(secret));
                mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof(Secret));
                macUrl = EncodingUtil.base64Encode(mac); 
            } 
        }
        return macUrl;
    } 
    
    /**
    *   @Description : This method saves Quote File Record
    *   @Param : none
    **/
   
    public void successFileupload()
    {
        String[] filelines = new String[]{};
        String[] fileRecords = new String[]{};
        String[] mapFields = new String[]{};
        String[] langCode = new String[]{};
        String csvLineRec;
        Map<String,Integer> csvLangCount = new Map<String,Integer>();
        Map<String,String> csvRecString = new Map<String,String>();
        Map<String,Set<String>> fileNameMap = new Map<String,Set<String>>();
        Set<String> fNameset;
        langImportStatusList = new List<String>();
        Set<String> qliSet = new Set<String>();
        String analysedURL ='';
        if(currentQuote!=null)
        quoID =currentQuote.id;
        if(Account_Name!=null)
        Account_Name = EncodingUtil.urlEncode(Account_Name, 'UTF-8');
        path = Account_Name+path;
     
        if(quoID != null){ 
            if(quoID.length()!=15)
            quoID = quoID.subString(0,15);
            getawss3LoginCredentials();
            String signed = getSignedPolicy();
            String codedsigned = EncodingUtil.urlEncode(signed,'UTF-8'); 
            Long Lexpires = expire.getTime()/1000;
           if(newFileName!=null)
            analysedURL = downloadQuoteFile(quoID,newFileName);
            system.debug('--analysedURL----: '+analysedURL);
            if(analysedURL != ''){
                Httpresponse resp = GLSQLICreator.getResponse(analysedURL);
                if(resp!=null)
                csvDataToParse = resp.getBody();
            }
        }
        System.debug('csvDataToParse====='+ csvDataToParse);
        if(csvDataToParse != ''){
            filelines = csvDataToParse.split('\n');
            for(Integer i = 2;i<filelines.size();i++)
            {
                //System.debug('==checking==='+ filelines[i].length());
                if(filelines[i]==null || filelines[i]=='' || filelines[i].length()==1)
                continue;
                fileRecords = filelines[i].split(';');
               // System.debug('fileRecords====='+ fileRecords);
                mapFields = fileRecords[0].split(';');
                langCode = mapFields[0].split('\\|');
               String fileName = langCode[0].trim();
                String langPair = langCode[1].trim();
               // system.debug('---csvDataToParse---:' +fileRecords+'=='+mapFields+'=='+langCode+'=='+langPair);
               
                if(fileNameMap.containsKey(langpair))
                {
                    csvLineRec = csvRecString.get(langPair);
                    csvLineRec = csvLineRec +'\n' + filelines[i];
                    fNameset = fileNameMap.get(langpair);
                    fNameset.add(fileName+langpair);
                    fileNameMap.put(langPair,fNameset);
                    csvRecString.put(langPair,csvLineRec);
                    
                }
                else{
                    fNameset = new Set<String>();
                    csvLineRec =  filelines[i];
                    fNameset.add(fileName+langpair);
                    fileNameMap.put(langPair,fNameset);
                    csvRecString.put(langPair,csvLineRec);
                   
                }
                system.debug('---fileNameMap---:' +fileNameMap+'---csvRecString---'+csvRecString);
                
               /* if(csvLangCount.containsKey(langPair))
                {
                    csvLangCount.put(langPair,csvLangCount.get(langPair)+1);
                    
                }
                else{
                    csvLangCount.put(langPair,1);
                    csvRecString.put(langPair,filelines[i]);
                }*/
            } 
            try{
                if(qliList.size()>0)
               
                for(Quote_Line_Item__c qli:qliList)
                {
                   
                    qliSet.add(qli.Memsource_Language_Pair__c);
                    /*if(csvLangCount.get(qli.Memsource_Language_Pair__c)==1 )
                    {
                        String langRecord = csvRecString.get(qli.Memsource_Language_Pair__c);
                        String analyzedFor = 'analyzedSingleQLI';
                        if(langRecord!=null || langRecord!='')
                        ParseResult = glsCSVParser.parser(qli.id,quoteId_Value,analysedURL,'','Success',langRecord, analyzedFor,true);
                        system.debug('----ParseResult-----: '+ParseResult);
                        String successMsg = qli.Memsource_Language_Pair__c+':'+' '+ 'Imported Successfully.';
                        langImportStatusList.add(successMsg);
                    }
                  
                    else if(csvLangCount.get(qli.Memsource_Language_Pair__c)>1)
                    {
                        String errorMsg = qli.Memsource_Language_Pair__c+':'+' '+ 'Could not Import because multiple matches found.';
                        langImportStatusList.add(errorMsg);
                    }*/
                    
                    //if(fileNameMap.containsKey(qli.Memsource_Language_Pair__c) && fileNameMap.get(qli.Memsource_Language_Pair__c).size()==1 )
                    //{ 
                        String successMsg = '';
                        String langRecord = csvRecString.get(qli.Memsource_Language_Pair__c);
                        String analyzedFor = 'analyzedSingleQLI';
                        System.debug('langRecordlangRecordlangRecord '+ langRecord);

                        if(langRecord!=null && !langRecord.equals('')) {
                            ParseResult = glsCSVParser.parser(qli.id,quoteId_Value,analysedURL,'','Success',langRecord, analyzedFor,true);
                            system.debug('----ParseResult-----: '+ParseResult);
                            successMsg = qli.Memsource_Language_Pair__c+':'+' '+ 'Imported Successfully.';
                            langImportStatusList.add(successMsg);
                        }
                        //else {
                        //    successMsg = qli.Memsource_Language_Pair__c+':'+' '+ 'Doesn\'t support.';
                       //     langImportStatusList.add(successMsg);
                       // }

                        //ParseResult = glsCSVParser.parser(qli.id,quoteId_Value,analysedURL,'','Success',csvLineRec, analyzedFor,true);

                        
                        
                    //}
                  
                    //else if(fileNameMap.containsKey(qli.Memsource_Language_Pair__c) && fileNameMap.get(qli.Memsource_Language_Pair__c).size()>1)
                    //{
                    //    String errorMsg = qli.Memsource_Language_Pair__c+':'+' '+ 'Could not Import because multiple matches found.';
                    //    langImportStatusList.add(errorMsg);
                   // }
                }
                Set<String> langPairList = new Set<String>();
                langPairList = fileNameMap.keySet();
                System.debug('langPairList==='+langPairList+'qliSet==='+qliSet);
                if(langPairList.size()>0)
                for(String str:langPairList)
                {
                    if(!qliSet.contains(str))
                    {
                        String errorMsg = str +':'+' '+ 'Could not Import because no matches found.';
                        langImportStatusList.add(errorMsg);
                    }
                     
                }
                system.debug('---fileNameMap---:' +fileNameMap+'---langImportStatusList---'+langImportStatusList);
            }catch(DMLException de){
                System.debug(GLSConfig.DMLExceptionOccured+'---'+de.getMessage()+'Line Number==='+de.getLineNumber());
            }catch(Exception e){
                System.debug(GLSConfig.ExceptionOccured+'---'+e.getMessage()+'Line Number==='+e.getLineNumber());
            }
        }
    }
    
    private string downloadQuoteFile(string quoID,string fName){
        
        String filename;
        getawss3LoginCredentials();
        if(fName!=null)
        filename = EncodingUtil.urlEncode(fName,'UTF-8');
        Datetime now = DateTime.now();
        Datetime expireson = now.AddSeconds(120);
        Long Lexpires = expireson.getTime()/1000;
        String stringtosign = 'GET\n\n\n'+Lexpires+'\n/'+GLSConfig.bucketName+'/'+filename;
        String signed = make_sig(stringtosign);
        String codedsigned = EncodingUtil.urlEncode(signed,'UTF-8');
        String url = '';
        if(!Test.isRunningTest()){
            url = 'https://'+GLSConfig.bucketName+'.s3.amazonaws.com/'+path+filename+'?AWSAccessKeyId='+as3.key+'&Expires='+Lexpires;
        }else url = 'testurl';
        return url;
    
    }
}