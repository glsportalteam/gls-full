public class GLS_CommunityServicesController{
    public Quote_Assignment__c assn {get;set;}
    public List<string> serviceNames {get;set;}
    public List<Quote_Assignment_Service__c> quoteAssnList {get;set;}
    public List<Client_Service__c> lstAllClientService {get;set;}
    public List<Client_Service__c> lstCommonClientService {get;set;}
    public List<Quote_Assignment_Service__c> lstQAssgServices {get;set;}
    public List<ServicesWrapper> servicesWrapperList {get;set;}
    public List<ServicesWrapper> commonServicesWrapperList {get;set;}
    public Map<Id, Quote_Assignment_Service__c> qaServiceMap {get;set;}
    public List<String> currentServiceNames {get;set;}
    public List<Client_Service__c> selectedServices {get;set;}
    public GLS_CommunityUploadFileController servicesUploadController {get;set;}
    public Boolean enablePlaceOrder {get;set;}
    public Boolean isDirectOrder {get;set;}
    public Integer clinicianIndex {get;set;}
    public Integer cognitiveIndex {get;set;}   
    
    /* Constructor
    *
    */
    public GLS_CommunityServicesController(GLS_CommunityUploadFileController controller){       
        enablePlaceOrder = false;
        isDirectOrder = false;
        servicesUploadController = controller;
        clinicianIndex = 2;
        cognitiveIndex = 3;
        assn = new Quote_Assignment__c();
          
        currentServiceNames = new List<String>();
        selectedServices = new List<Client_Service__c>();
         
        servicesWrapperList = new List<ServicesWrapper>();
        commonServicesWrapperList = new List<ServicesWrapper>();
        lstAllClientService = [Select Name, Id, Client_ServiceID__c,Order__c, Description__c From Client_Service__c where is_common__c = false order by Order__c,Name];
        
        lstCommonClientService = [Select Name, Id, Client_ServiceID__c,Order__c, Description__c From Client_Service__c where is_common__c = true order by Order__c,Name];
        //lstQAssgServices = [Select Service_Name__c, Name, Id, Client_Service__r.Name, Client_Service__c,Quote_Assignment__r.Name,Client_Service__r.Order__c 
        //                   From Quote_Assignment_Service__c Where Quote_Assignment__c =: assn.id Order By Client_Service__r.Order__c,Client_Service__r.Name];
        Integer serviceCounter = 0;
        if(lstCommonClientService == null){
            lstCommonClientService = new List<Client_Service__c>();
        }
        if(lstAllClientService == null){
            lstAllClientService = new List<Client_Service__c>();
        }
        servicesWrapperList.clear();
        for(Client_Service__c clService : lstAllClientService){
                servicesWrapperList.add(new ServicesWrapper(clService, false));                               
        }
        //snehal changes 5-2-2014
        //uncomment if sorted list is required
        /*system.debug('before sorting services list-->'+servicesWrapperList);
        servicesWrapperList.sort();
        reArrangeSeerviceList(servicesWrapperList);
       
        system.debug('after sorting services list-->'+servicesWrapperList);*/
        commonServicesWrapperList.clear();
        for(ServicesWrapper service : servicesWrapperList){
                if(service.clientService.Name.equals('Clinician Review')){
                         clinicianIndex = serviceCounter;           
                }
                if(service.clientService.Name.equals('Cognitive Debriefing')){
                         cognitiveIndex = serviceCounter;           
                }
                serviceCounter++;               
        }
        for(Client_Service__c clService : lstCommonClientService){
            commonServicesWrapperList.add(new ServicesWrapper(clService, false));
        }
        
    }
    
    /*public PageReference obtainServicesLists(){
        lstAllClientService = [Select Name, Id, Client_ServiceID__c,Order__c, Description__c From Client_Service__c where is_common__c = false order by Order__c,Name];
        
        lstCommonClientService = [Select Name, Id, Client_ServiceID__c,Order__c, Description__c From Client_Service__c where is_common__c = true order by Order__c,Name];
        //lstQAssgServices = [Select Service_Name__c, Name, Id, Client_Service__r.Name, Client_Service__c,Quote_Assignment__r.Name,Client_Service__r.Order__c 
        //                   From Quote_Assignment_Service__c Where Quote_Assignment__c =: assn.id Order By Client_Service__r.Order__c,Client_Service__r.Name];
        Integer serviceCounter = 0;
        if(lstCommonClientService == null){
            lstCommonClientService = new List<Client_Service__c>();
        }
        if(lstAllClientService == null){
            lstAllClientService = new List<Client_Service__c>();
        }
        servicesWrapperList.clear();
        for(Client_Service__c clService : lstAllClientService){
                servicesWrapperList.add(new ServicesWrapper(clService, false));                    
        }
        //snehal changes 5-2-2014
        //uncomment if sorted list is required
        /*servicesWrapperList.sort();
        reArrangeSeerviceList(servicesWrapperList);
        for(ServicesWrapper service : servicesWrapperList){
                if(service.clientService.Name.equals('Clinician Review')){
                         clinicianIndex = serviceCounter;           
                }
                if(service.clientService.Name.equals('Cognitive Debriefing')){
                         cognitiveIndex = serviceCounter;           
                }
                serviceCounter++;               
        }
        commonServicesWrapperList.clear();
        for(Client_Service__c clService : lstCommonClientService){
            commonServicesWrapperList.add(new ServicesWrapper(clService, false));
        }
        return null;
    }*/
    
    
    /*
    * Method to prepopulate services on selecting a quote from My Requests
    * @param : none
    * @return type : PageReference
    */
    public PageReference prepopulateServices(){
        //obtainServicesLists();
        isDirectOrder = servicesuploadcontroller.userObj.contact.account.Direct_Order_Allowed__c;
        //enablePlaceOrder = true;
        if(servicesUploadController.quoteObj != null){
            Quote_Assignment__c qAssnObj = servicesUploadController.quoteAssignmentObj;
            
            if(qAssnObj != null){
                lstQAssgServices = [Select Service_Name__c, Name, Id, Client_Service__r.Name, Client_Service__c,Quote_Assignment__r.Name,Client_Service__r.Order__c 
                                   From Quote_Assignment_Service__c Where Quote_Assignment__c =: qAssnObj.id Order By Client_Service__r.Order__c,Client_Service__r.Name];
                qaServiceMap = new Map<Id, Quote_Assignment_Service__c>();
                for(Quote_Assignment_Service__c qas : lstQAssgServices ){
                    qaServiceMap.put(qas.Client_Service__r.id, qas);
                }
                servicesWrapperList.clear();
                commonServicesWrapperList.clear();
                for(Client_Service__c clService : lstAllClientService){
                    if(qaServiceMap.containsKey(clService.id)){
                        servicesWrapperList.add(new ServicesWrapper(clService, true));
                    }
                    else
                        servicesWrapperList.add(new ServicesWrapper(clService, false));
                }
                for(Client_Service__c clService : lstCommonClientService){
                    if(qaServiceMap.containsKey(clService.id)){
                        commonServicesWrapperList.add(new ServicesWrapper(clService, true));
                    }
                    else
                        commonServicesWrapperList.add(new ServicesWrapper(clService, false));
                }
                
            }
            else{
                commonServicesWrapperList.clear();
                servicesWrapperList.clear();
                selectedServices.clear();
                for(Client_Service__c clService : lstAllClientService){       
                        servicesWrapperList.add(new ServicesWrapper(clService, false));
                }
                for(Client_Service__c clService : lstCommonClientService){       
                        commonServicesWrapperList.add(new ServicesWrapper(clService, false));
                }
                //selectedServices.clear();
            }
            //snehal changes 5-2-2014
            //uncomment if sorted list is required
        /*servicesWrapperList.sort();
        reArrangeSeerviceList(servicesWrapperList);*/
       
        }
        
        
        System.debug('------------>>enablePlaceOrder ' + enablePlaceOrder);
        //obtainServicesLists();
        return null;
    }
    
    /* 
    * Save services on clicking the Continue button
    * @param : none
    * @return type : void
    */
    public PageReference saveOnContinue(){
        if(servicesuploadcontroller.quoteObj == null){
            servicesuploadcontroller.createNewQuote();
            servicesuploadcontroller.createNewQuoteAssignment();
            assn = servicesuploadcontroller.quoteAssignmentObj;
        }
        else{
            assn = new Quote_Assignment__c(id=servicesuploadcontroller.quoteAssignmentObj.Id);
        }
        System.debug('Inside save on continue services****************');
        List<Quote_Assignment_Service__c> qaServicesToDelete = new List<Quote_Assignment_Service__c>();
        System.debug('servicesWrapperList---------->' + servicesWrapperList);
        for(ServicesWrapper sw : servicesWrapperList ){
            if(sw.isChecked == true){
                selectedServices.add(sw.clientService);
            }
        }
        System.debug('selectedServices-------------->>' + selectedServices);
        for(ServicesWrapper sw : commonServicesWrapperList ){
            if(sw.isChecked == true){
                selectedServices.add(sw.clientService);
            }
        }
        List<Quote_Assignment_Service__c> quoteAssgSrvcList = new List<Quote_Assignment_Service__c>();
        for(Client_Service__c cs : selectedServices){
             quoteAssgSrvcList.add(new Quote_Assignment_Service__c(Client_Service__c = cs.id, Quote_Assignment__c = assn.id));
        }
        qaServicesToDelete = [select id from Quote_Assignment_Service__c where Quote_Assignment__c =: assn.Id];
        if(qaServicesToDelete != null){
            delete qaServicesToDelete ;
        }
        if(quoteAssgSrvcList != null){
            insert quoteAssgSrvcList;
        }
        System.debug('---------** servicesuploadcontroller.userObj.contact.account.Direct_Order_Allowed__c' + servicesuploadcontroller.userObj.contact.account.Direct_Order_Allowed__c);
        isDirectOrder = servicesuploadcontroller.userObj.contact.account.Direct_Order_Allowed__c;
        enablePlaceOrder = true;
        servicesWrapperList.clear();
        commonServicesWrapperList.clear();
        selectedServices.clear();
        for(Client_Service__c clService : lstAllClientService){       
                 servicesWrapperList.add(new ServicesWrapper(clService, false));
        }
        for(Client_Service__c clService : lstCommonClientService){       
                 commonServicesWrapperList.add(new ServicesWrapper(clService, false));
        }
        //snehal changes 5-2-2014
        //uncomment if sorted list is required
        /*servicesWrapperList.sort();
        reArrangeSeerviceList(servicesWrapperList);*/
        //obtainServicesLists();
        return null;
    }
    //changes snehal 5-2-2014 begin
    //uncomment if sorted list is required
    /*public void reArrangeSeerviceList(List<ServicesWrapper>ServicesWrapperList){
        List<ServicesWrapper> newServicesWrapperList=new List<ServicesWrapper>();
        ServicesWrapper clinicalServiceObj,cognitiveServiceObj,linguisticServiceObj;
        integer clinicalServicePosition,cognitiveServicePosition,linguisticServicePosition;
        
        system.debug(ServicesWrapperList);
        
        clinicalServiceObj=getServicesWrapperobj('Clinician Review');
        if(clinicalServiceObj!=null){
            clinicalServicePosition=findPosition(clinicalServiceObj,ServicesWrapperList);
        }       
        newServicesWrapperList.addAll(ServicesWrapperList);
        newServicesWrapperList.remove(clinicalServicePosition);
        
        cognitiveServiceObj=getServicesWrapperobj('Cognitive Debriefing');
        if(cognitiveServiceObj!=null){
            cognitiveServicePosition=findPosition(cognitiveServiceObj,newServicesWrapperList);
        }
        newServicesWrapperList.remove(cognitiveServicePosition);
        
        linguisticServiceObj=getServicesWrapperobj('Linguistic Validation');
        if(linguisticServiceObj!=null){
            linguisticServicePosition=findPosition(linguisticServiceObj,newServicesWrapperList);
        }
        system.debug('after removal'+newServicesWrapperList);
        linguisticServicePosition++;
        newServicesWrapperList=insertelement(ServicesWrapperList.get(clinicalServicePosition),linguisticServicePosition,newServicesWrapperList);
        linguisticServicePosition++;
        
        cognitiveServicePosition=findPosition(cognitiveServiceObj,ServicesWrapperList);
        newServicesWrapperList=insertelement(ServicesWrapperList.get(cognitiveServicePosition),linguisticServicePosition,newServicesWrapperList);
        system.debug('after reseting'+newServicesWrapperList); 
        ServicesWrapperList.clear();
        ServicesWrapperList.addAll(newServicesWrapperList);   
    
    }
    public ServicesWrapper getServicesWrapperobj(String serviceName){       
        for(ServicesWrapper obj:ServicesWrapperList){
            if(obj.clientService.Name.equalsIgnoreCase(serviceName)){
                return obj;
                        
            }       
        
        }   
        return null; 
    
    }
    public integer findPosition(ServicesWrapper element,List<ServicesWrapper>ServicesWrapperList){
        integer count=0;
        integer position;
        for(ServicesWrapper service:ServicesWrapperList ){   
            if((service.clientService.id).equals(element.clientService.id)){ 
                position=count;
                break;
            }
           count++;
       }
        return position;
        
    }
    public List<ServicesWrapper> insertelement(ServicesWrapper e,integer position,List<ServicesWrapper>ServicesWrapperList){   
    ServicesWrapper tempi,tempj;
    tempi=e;
    system.debug('object to be inserted '+tempi);
    for(integer i=position;i<ServicesWrapperList.size();i++){
        tempj=ServicesWrapperList.get(i);
        ServicesWrapperList.set(i,tempi);
        tempi=tempj;
        
    }
    ServicesWrapperList.add(tempi);
    return ServicesWrapperList;    
    
    }*/
    
    
    
    //changes snehal 5-2-2014 ends
    
    
    
    /*
    * wrapper class for binding checkbox values with service type
    */
     public class ServicesWrapper /*implements Comparable*/ {
        public Client_Service__c clientService {get;set;}
        public Boolean isChecked {get;set;}
        public ServicesWrapper(Client_Service__c clService, Boolean isChkd){
            clientService = clService;
            isChecked = isChkd;
        }
        //uncomment if sorted list is required
        /*public Integer compareTo(Object ObjToCompare) {
              ServicesWrapper compareToSer = (ServicesWrapper)ObjToCompare;
              if ((clientService.Name).equals(compareToSer.clientService.Name)) return 0;
              if (((clientService.Name).compareTo(compareToSer.clientService.Name))>0) return 1;
              return -1; 
           
        }*/
        public ServicesWrapper(){
            clientService = new Client_Service__C();
        }
    }
}