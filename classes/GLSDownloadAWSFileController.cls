public with sharing class GLSDownloadAWSFileController {
    public String fileId   {get;set;}    
    public string downloadURL {get;set;}  
    public PageReference pref {get;set;}
    public string sObjName    {get;set;}
    public string fileName    {get;set;}
    public string bucket    {get;set;}
    public string fileType    {get;set;}
    public GLSDownloadAWSFileController(){
        bucket = GLSConfig.bucketName;      
        downloadURL='';
        fileId = ApexPages.currentPage().getParameters().get('id');                     
        Id recordId = fileId;
        
        if(recordId!=null)
            sObjName = recordId.getSObjectType().getDescribe().getName();
                
        downloadFile();
    }
    public PageReference downloadFile(){                
        String path;
        if(sObjName=='Quotes_File__c'){
            List<Quotes_File__c> qFileList=[select AWSFilePath__c,File_Name__c from Quotes_File__c where id=:fileId];
            system.debug(qFileList+'qFileList---------------');
            if(qFileList != null && qFileList.size() > 0){
                path = qFileList[0].AWSFilePath__c+qFileList[0].File_Name__c;
                fileName = qFileList[0].File_Name__c;
                fileType='Quote File';
            }
        }
        if(sObjName=='Quote_PDF_File__c'){
            List<Quote_PDF_File__c> qPDFFileList=[select id,AWSFilePath__c,File_Name__c from Quote_PDF_File__c where id=:fileId];           
            if(qPDFFileList != null && qPDFFileList.size() > 0){
                path = qPDFFileList[0].AWSFilePath__c+qPDFFileList[0].File_Name__c;
                fileName = qPDFFileList[0].File_Name__c;
                fileType='Quote PDF File';
            }
        }       
                if(sObjName=='Corkboard_Project_File__c'){
            List<Corkboard_Project_File__c> cbFileList=[select id,AWSFilePath__c,File_Name__c from Corkboard_Project_File__c where id=:fileId];           
            if(cbFileList != null && cbFileList.size() > 0){
                path = cbFileList[0].AWSFilePath__c+cbFileList[0].File_Name__c;
                fileName = cbFileList[0].File_Name__c;
                fileType='CB Project File';
            }
        }
        if(path != null && path != ''){
            String StatusMessage = GLSAmazonUtility.downloadQuoteFile(path,GLSConfig.bucketName);
            if(StatusMessage != null){
                pref = new Pagereference(StatusMessage);
                downloadURL=pref.getUrl();
            }                                                                
        }                                                   
        return null;
    }
}