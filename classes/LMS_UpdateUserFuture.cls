global class LMS_UpdateUserFuture {
    
    public static Boolean isUserUpdated = false;

    @future
    public static void updateUser(List<String> userIds){
        system.debug('---inside future method---; '+userIds);
        if(userIds != null && userIds.size()>0){
            List<User> lstUser = [Select Id from User where id in: userIds ];
            if(lstUser != null && lstUser.size()>0){
                try{
                    isUserUpdated = true;
                    update lstUser;
                }catch(DMLException de){
                    system.debug('---DML Exception--: '+de);
                }
            }
        }
    }
}