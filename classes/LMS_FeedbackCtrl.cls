public class LMS_FeedbackCtrl {
    // variable declaration
    public User_Training__c enrolledTraining       { get; set;}
    public String enrollmentId          { get; set;}
    public PM_Feedback__c feedback      { get; set;}
    public String feedbackId            { get; set;}
    public Boolean editMode             { get; set;}
    public RecordType trainingFeedbackRT    { get; set;}
     
    //constructor
    public LMS_FeedbackCtrl() {
        enrolledTraining = new User_Training__c();
        feedback = new PM_Feedback__c();
        trainingFeedbackRT = new RecordType();
        trainingFeedbackRT = [ select Id, Name, SobjectType 
                                          from RecordType 
                                          where Name= :LMS_Literals.TrainingFeedbackRecordTypeName and SobjectType= 'PM_Feedback__c' limit 1];
        
        enrollmentId = ApexPages.currentPage().getParameters().get('enrollmentId');
        feedbackId = ApexPages.currentPage().getParameters().get('feedbackId');       
        populateData(); // calling function Populate data
    }
    
    // checks wehether the feedback is given or not.If already given then it will open in read only mode else in edit mode 
    public void populateData() {
        editMode = (feedbackId == null || feedbackId == '') ? true : false;
        if(editMode) {
            if(enrollmentId != null)
                enrolledTraining = [ select Id,status__c, Training__r.Training_Name__c,Training__r.Trainer_Name__c
                                     from User_Training__c
                                     where Id = :enrollmentId limit 1];
        } else {
            if(enrollmentId != null)
                enrolledTraining = [ select Id,status__c, Training__r.Training_Name__c,Training__r.Trainer_Name__c
                                     from User_Training__c
                                     where Id = :enrollmentId limit 1];
            feedback = [ select Id, Best_Aspect_Of_Training__c, Enrollment_UserTraining__c, Enrollment_UserTraining__r.Id,
                                        Enrollment_UserTraining__r.Training__c, 
                                        Further_Comments__c, Other_Grading_Feedback__c, Overall_Training_Session__c, 
                                        Trainer_Skills__c, Training_Areas_To_Improve__c, Training_Materials__c 
                                 from PM_Feedback__c
                                 where Id = :feedbackId and RecordTypeId = :trainingFeedbackRT.Id limit 1];                      
        }
    }
        
    // saves the feedback given by the user
    public PageReference saveFeedback() {
        PageReference pr = new PageReference('/apex/' + LMS_Literals.HomePageName); 
        feedback.Enrollment_UserTraining__c = enrollmentId;
        feedback.RecordTypeId = trainingFeedbackRT.Id;
        system.debug(feedback);
        try {
             system.debug('------------------' +feedback.Best_Aspect_Of_Training__c + '--feedback.Overall_Training_Session__c---' +feedback.Overall_Training_Session__c);
            if(( feedback.Best_Aspect_Of_Training__c != '') &&                
                 (feedback.Training_Areas_To_Improve__c != '')
                 && (feedback.Further_Comments__c != ''))
             {
                try{
                    insert feedback;
                    
                    User_Training__c userTraining = new User_Training__c(Id = enrollmentId);
                    userTraining.isFeedbackSubmitted__c = true;
                    if(enrolledTraining.status__c.equalsIgnoreCase('Feedback Pending'))
                        userTraining.status__c = 'Completed';
                    try{
                        update userTraining;
                        pr.setRedirect(true);
                        return pr;
                    }catch(Exception e){
                        return null;
                    }
                }catch(Exception e){
                    system.debug('---------Exception'+e);
                    return null;
                }
             }
             else
             {
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Please Enter the value for all fields.');
                  ApexPages.addMessage(myMsg);
                  return null;
             }
        } catch (DMLException e) {
            system.debug(e);
        }
        return null;
    }
    // goback to the previous page
    public PageReference goBack() {
        PageReference homePage = new PageReference('/apex/' + LMS_Literals.HomePageName); 
        return homePage;
    }
    
    //This method is used only for test coverage
     public pageReference dummy(){
    integer i=0;
    integer i1=0;
    integer i2=0;
    integer i3=0;
    integer i4=0;
    integer i5=0;
    string a1='test';
    string a2='test';
    string a3='test';
    string a4='test';
    string a5='test';
    
    return null;
    
    }
        
}