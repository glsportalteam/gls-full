/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest

private class GLSQW_UploadDeliverablesControllerTest{
    public static Account accountObj;  
    public static User u;
    public static Contact contactObj;
    public static Quote__c quote;
    public static Quote_Assignment__c qAsst1; 
    public static Quote_Assignment__c qAsst2; 
    public static Language_List_Item__c defLanguage;
    public static Quotes_File__c qFiles1;
    public static Quotes_File__c qFiles2;
    public static list<Quotes_File__c> lstqFiles1;
    public static Quote_Assignment_File__c qAsstFile1;
    public static Quote_Assignment_File__c qAsstFile2;
    public static Quote_Assignment_File__c qAsstFile3;
    public static Quote_Assignment_File__c qAsstFile4;
    public static list<Quote_Assignment_File__c> lstqAsstFile;
    public static File_Format__c fileFormat;
    public static GLS_CommunityFAQ__c FAQ;
    public static case caseObj;
    static void setupData(){
        accountObj = new Account();
        accountObj.Name = 'Amgen';
        accountObj.BillingState ='Texas';
        insert accountObj;
        contactObj = new Contact(); 
        contactObj.FirstName = 'Jennifer A';
        contactObj.LastName = 'Jennifer A';
        contactObj.AccountId = accountObj.Id;
        insert contactObj; 
        
        quote = new Quote__c();
        quote.Quote_Name__c = 'Test Quote';
        quote.Client__c = accountObj.Id;
        quote.Status__c='New';
        quote.Contact__c = contactObj.Id;
        insert quote;
        
        
        defLanguage = new Language_List_Item__c();
        defLanguage.Name = 'English';
        defLanguage.Language_Name__c = 'en-US';
        insert defLanguage;
        
        qAsst1 = new Quote_Assignment__c();
        qAsst1.Quote__c = quote.Id; 
        qAsst1.Source_Language__c = defLanguage.Id;     
        qAsst1.Allowed_File_Formats__c = 'MS Word';
        qAsst1.Document_Type__c = 'IFC,SomeData;';
        qAsst1.Keywords__c='one;';
        insert qAsst1;  
        
        qAsst2 = new Quote_Assignment__c();
        qAsst2.Quote__c = quote.Id; 
        qAsst2.Source_Language__c = defLanguage.Id;     
        qAsst2.Allowed_File_Formats__c = 'MS Word';
        qAsst2.Document_Type__c = 'IFC,SomeData;';
        qAsst2.Keywords__c='';
        insert qAsst2;
                
        
        qFiles1 = new Quotes_File__c();
        qFiles1.File_Name__c='Test1.doc';
        qFiles1.TradosAPIFileID__c='1234'; 
        qFiles1.TradosAPITransactionID__c='xasafafdsf143';
        qFiles1.Quote__c=quote.id;
        qFiles1.File_Type__c = 'Deliverable';
        insert qFiles1;
        
        qFiles2 = new Quotes_File__c();
        qFiles2.File_Name__c='Test2.doc';
        qFiles2.TradosAPIFileID__c='1234'; 
        qFiles2.TradosAPITransactionID__c='xasafafdsf143';
        qFiles2.File_Type__c = 'Deliverable';
        qFiles2.Quote__c=quote.id;
        insert qFiles2;
        
        qAsstFile1 = new Quote_Assignment_File__c();
        qAsstFile1.Quote_Assignment__c = qAsst1.Id;
        qAsstFile1.Quote_Files__c = qFiles1.id;
        qAsstFile1.File_Usage__c = 'Client';   
        insert qAsstFile1;
        
        qAsstFile2 = new Quote_Assignment_File__c();
        qAsstFile2.Quote_Assignment__c = qAsst1.Id;
        qAsstFile2.Quote_Files__c = qFiles2.id;
        qAsstFile2.File_Usage__c = 'Client';   
        insert qAsstFile2;
        
        
        qAsstFile3 = new Quote_Assignment_File__c();
        qAsstFile3.Quote_Assignment__c = qAsst2.Id;
        qAsstFile3.Quote_Files__c = qFiles1.id;
        qAsstFile3.File_Usage__c = 'Client';   
        insert qAsstFile3;
        
        qAsstFile4 = new Quote_Assignment_File__c();
        qAsstFile4.Quote_Assignment__c = qAsst2.Id;
        qAsstFile4.Quote_Files__c = qFiles2.id;
        qAsstFile4.File_Usage__c = 'Client';   
        insert qAsstFile4;
        
        fileFormat = new File_Format__c();
        fileFormat.Name = 'MS Word';
        fileFormat.File_Extension__c = '.doc; .docm; .docx; .dot; .dotm; .dotx';
        insert fileFormat;
        
       /* Quote_Line_Item__c qli = new Quote_Line_Item__c();
        qli.QuoteID__c = quote.id;
        qli.Trados_Language_Pair__c ='English';
        insert qli; */
        
      
    } 
    
   static testMethod void myUnitTest1() {
        
        test.startTest();
        setupData();
        ApexPages.currentPage().getParameters().put('Id',quote.Id);
        GLSQW_UploadDeliverablesController obj = new GLSQW_UploadDeliverablesController();
        obj.newFileName = 'testt.doc';
        obj.QuoteId_value=quote.Id;
        obj.fileSize = 2048;
        obj.successFileupload();
        obj.obtainDeliverablesList();
        
        
        obj.saveId = qFiles1.Id;
        obj.saveDeliverable();
        
        obj.lstQuoteFilesWrapper[0].quoteFile.Released__c = true;
        obj.lstQuoteFilesWrapper[1].quoteFile.Released__c = false;
        obj.saveAllDeliverable();
       
        obj.getlstLanguagePair();
       
        obj.deleteId = qFiles2.Id;
        obj.deleteDeliverable();
        
        obj.selectAll=false;
        obj.selectAllDeliverables();
        
        obj.selectAll=true;
        obj.selectAllDeliverables();
        
        obj.deleteAllDeliverable();
        obj.obtainDeliverablesList();
        obj.deleteAllDeliverable();
        
        
        test.stopTest();
   }
    
    
    static testMethod void myUnitTest2() {
        
        test.startTest();
        accountObj = new Account();
        accountObj.Name = 'Amgen';
        accountObj.BillingState ='Texas';
        insert accountObj;
        contactObj = new Contact(); 
        contactObj.FirstName = 'Jennifer A';
        contactObj.LastName = 'Jennifer A';
        contactObj.AccountId = accountObj.Id;
        insert contactObj; 
        Quote__c quote1 = new Quote__c();
        quote1.Quote_Name__c = 'Test Quote1';
        quote1.Client__c = accountObj.Id;
        quote1.Status__c='New';
        quote1.Contact__c = contactObj.Id;
        insert quote1;
        
        ApexPages.currentPage().getParameters().put('Id',quote1.Id);
        GLSQW_UploadDeliverablesController obj = new GLSQW_UploadDeliverablesController();
        obj.obtainDeliverablesList();
        test.stopTest();
   }
    
}