public without sharing class GLSCommunity_DraftController {
	public List<Quote__c> currentUsersQuotes              {get;set;}
	public string existingQuoteId                         {get;set;}
	public string ProjectName                             {get;set;}
	public string protocolFromUser                        {get;set;}
	public string deleteProjectId                         {get;set;}
	public list<string> existingProjectNames              {get;set;}
	public list<Quote__c> allQuoteslst                    {get;set;}
	public list<string> protocolNumbr                     {get;set;}
	
	public list<string> existingClientRefLst              {get;set;}
	public list<string> existingSponsorRefLst             {get;set;}
	
	public User userObj                                   {get;set;}
	public Account userAccount                            {get;set;}
	//public List<Protocol__c> openProtocolList {get;set;}
	public List<SelectOption> openProtocolLst             {get;set;}
	public string selectedSponsor                         {get;set;}  
	
	private Map<Id,Program__c> protocolProgramMap         {get;set;}
	public String selectedProtocolId                      {get;set;}
	public boolean isDirectOrderAllowed                   {get;set;}
	public string selectedProgram                         {get;set;}
	public string referenceNumber                         {get;set;}
	public string selectedProtocolNumber                  {get;set;}
	public string selectedSponsorName                     {get;set;}
	private String contactId;
	private string redirectionPageUrl;
	private string profileName                            {get;set;}
	public boolean isOpenProtocolExist                    {get;set;}
	public list<string> delFilePathLst                      {get;set;}
	list<Quotes_File__c> delFiles                         {get;set;}
	public boolean isAllFileDeleted                       {get;set;}
	public GLSCommunity_DraftController(){
		isOpenProtocolExist = false;
		existingQuoteId = '';		
		currentUsersQuotes = new List<Quote__c>();
		existingProjectNames = new list<string>();
		openProtocolLst = new List<SelectOption>();
		protocolProgramMap = new Map<Id,Program__c>();
		contactId = ApexPages.currentPage().getParameters().get('conId');
		if(contactId !=null && contactId!=''){ 
	        userObj = [SELECT Id,Email,UserType,Contact.Account.Direct_Order_Allowed__c,
	        				  AccountId,ProfileId
	         		   FROM User 
	         		   WHERE ContactId =: contactId limit 1];
	        redirectionPageUrl= '/apex'; 		   
		}else{
        	userObj = [SELECT Id,Email,UserType,Contact.Account.Direct_Order_Allowed__c,
	        				  AccountId,ProfileId
	         		   FROM User 
	         		   WHERE Id = :userInfo.getUserId() limit 1];
	         redirectionPageUrl= '/customer';		   
		}
		if(userObj != null){
			profileName = [Select Name From Profile Where Id =: userObj.ProfileId].Name;
		} 
	         		    		    		
		// fecth all the quotes records against which logged in user is owner
		allQuoteslst = [SELECT id, Quote_Name__c, Project_Name__c,Client_Reference__c,Quote_Name_GLS__c,
							   Project_Name_Community__c 
					    FROM Quote__c  
					    WHERE OwnerId =:userObj.Id 
					    ORDER BY CreatedDate desc]; // Limit to be removed latter
					    
         		   
        // contains list of quotes in draft status against current user
        currentUsersQuotes = [SELECT id, Project_Number__c, Project_Name_Community__c,Name,
        							 Quote_Number__c,CreatedDate, Client__r.Name,Bucket_Name__c,
        							 Client__c, Project_Name__c,Last_Visited_Assignment_Id__c,Last_Visted_Stage__c,(select id,Step_one_complete__c,Step_two_complete__c from Quote_Assignments__r) 
        					  FROM Quote__c 
        					  WHERE OwnerId =:userObj.Id AND Status__c='Draft' order by CreatedDate desc];
        					  		
        //currentUsersQuotes = getAccessedQuoteList(profileName,userObj.Id);
        if(allQuoteslst != null && allQuoteslst.size() > 0){
        	
            set<string> protocolNumbrSet = new set<string>();
            set<string> clientReferenceSet = new set<string>();
            set<string> billingReferenceSet = new set<string>();
            
            for(Quote__c qt : allQuoteslst){
                if(qt.Quote_Name__c != null && qt.Quote_Name__c != ''){
                    protocolNumbrSet.add(qt.Quote_Name__c);
                }
                if(qt.Client_Reference__c != null && qt.Client_Reference__c != ''){
                    clientReferenceSet.add(qt.Client_Reference__c);
                }
                if(qt.Quote_Name_GLS__c != null && qt.Quote_Name_GLS__c != ''){
                    billingReferenceSet.add(qt.Quote_Name_GLS__c);
                }                
                if(qt.Project_Name_Community__c == null){
                    qt.Project_Name_Community__c = qt.Project_Name__c;
                }
                existingProjectNames.add(qt.Project_Name_Community__c);                                
            }
            
            //Contains list of all protocols 
            protocolNumbr = new list<string>(protocolNumbrSet);
            protocolNumbr.sort();
            
            // contains list of all client reference
            existingClientRefLst = new list<string>(clientReferenceSet);
            existingClientRefLst.sort();
            
            // contains list of all billing reference
            existingSponsorRefLst= new list<string>(billingReferenceSet);
            existingSponsorRefLst.sort();        
                                
        }else{
        	
            allQuoteslst = new list<Quote__c>();
            protocolNumbr = new list<string>();
            existingClientRefLst = new list<string>();
            existingSponsorRefLst= new list<string>();
            
        }
        if(currentUsersQuotes != null && currentUsersQuotes.size() > 0){
            for(Quote__c qte : currentUsersQuotes){
                if(qte.Project_Name_Community__c == null){
                    qte.Project_Name_Community__c = qte.Project_Name__c;
                }
            }
        }else{
            currentUsersQuotes = new List<Quote__c>();
        }      
        populateProtocolList();            
        
        if(openProtocolLst != null && openProtocolLst !=null && openProtocolLst.size() > 0 && isDirectOrderAllowed != null && isDirectOrderAllowed){
        	isOpenProtocolExist = true;
        }      
	}
	/*For fecthing list of open protocols associated with the  */
	public void populateProtocolList(){
		/*
		if profile account manager
		
		*/
		if(userObj != null && userObj.AccountId != null){
			// fecth accounts associated with the user
       		userAccount = [SELECT Id,Direct_Order_Allowed__c from Account where id=: userObj.AccountId limit 1];
       		if(userAccount != null && userAccount.Direct_Order_Allowed__c){
       			isDirectOrderAllowed =true;
       		}else{
       			isDirectOrderAllowed= false;
       		}
			if(profileName.equalsIgnoreCase(GLSConfig.AccountManager)){
	       		// fetch list of program associated with account of logged in user
	       		List<Program__c> programLst = [select id,name,Account__r.name,Sponsor__c,Sponsor__r.name,Reference__c,Program_Name__c,(select id,Name,Status__c,Protocol_Name__c from Protocols__r where Status__c='open' order by CreatedDate ) from Program__c where Account__c=:userObj.AccountId order by CreatedDate];
	       		// for getting list of only those programs for which user has access to
	       		//List<Program__c> programLst = GLSAccessUtility.getProgramList(userObj.AccountId);
	       		Boolean isOpenProtocolExist = false;
	       		// add the protocol associated with the program in protocol list
	       		if(programLst != null && programLst.size()>0){
	       			for(Program__c program : programLst){
	       				if(program.Protocols__r != null && program.Protocols__r.size() > 0){       					
	       					for(Protocol__c protocol : program.Protocols__r){       						
	       						protocolProgramMap.put(protocol.id,program);
	       						openProtocolLst.add(new SelectOption(protocol.id,protocol.Protocol_Name__c));
	       					}       					       					
	       				}
	       			}
	       			// not in list will be added only when there exist some open protocols       			
	       			if(openProtocolLst != null && openProtocolLst.size() >= 1)
	       				openProtocolLst.add(new SelectOption('Not in List','Not in List'));       		
	       		}
			}else{
				List<User_Access_Management__c> lstUAM = new List<User_Access_Management__c>();
				lstUAM = [Select ProgramId__c, ProtocolId__c From User_Access_Management__c Where User_Name__c =: userObj.Id];
				
				if(profileName.equalsIgnoreCase(GLSConfig.ProgramManager)){
					
					List<Program__c> programLst = new List<Program__c>();
					list<string> prgramIdLst = new List<string>();
					if(lstUAM != null && lstUAM.size()>0){
			        	for(User_Access_Management__c uAM : lstUAM){
			        		if(uAM.ProgramId__c != null)
			        			prgramIdLst.add(uAM.ProgramId__c);
			        	}
			        	if(prgramIdLst != null && prgramIdLst.size()>0){
			        		programLst = [select id,name,Account__r.name,Sponsor__c,Sponsor__r.name,Reference__c,Program_Name__c,(select id,Name,Status__c,Protocol_Name__c from Protocols__r where Status__c='open' order by CreatedDate ) from Program__c where Id IN:prgramIdLst  order by CreatedDate];
				        	for(Program__c program : programLst){
			       				if(program.Protocols__r != null && program.Protocols__r.size() > 0){       					
			       					for(Protocol__c protocol : program.Protocols__r){       						
			       						protocolProgramMap.put(protocol.id,program);
			       						openProtocolLst.add(new SelectOption(protocol.id,protocol.Protocol_Name__c));
			       					}       					       					
			       				}
			       			}
			       			// not in list will be added only when there exist some open protocols       			
			       			if(openProtocolLst != null && openProtocolLst.size() >= 1)
			       				openProtocolLst.add(new SelectOption('Not in List','Not in List'));    
			       				
			        	}	
			        }
		        		
		        }else if ((profileName.equalsIgnoreCase(GLSConfig.ProtocolManager) ||  profileName.equalsIgnoreCase(GLSConfig.LPStdUser))){
		        		if(lstUAM != null && lstUAM.size()>0){
		        			List<String> protocolIdLst =new List<String>();
				        	for(User_Access_Management__c uAM : lstUAM){
				        		if(uAM.ProtocolId__c != null)
				        			protocolIdLst.add(uAM.ProtocolId__c);
				        	}
				        	List<Protocol__c> protocolLst;
				        	if(protocolIdLst !=null && protocolIdLst.size()>0){
				        		protocolLst = [select id,Name,Status__c,Program_Name__c,Program_Name__r.name,Program_Name__r.Sponsor__c,Program_Name__r.Sponsor__r.name,Program_Name__r.Reference__c,Program_Name__r.Program_Name__c,Program_Name__r.Account__r.name,Protocol_Name__c from Protocol__c where Id IN: protocolIdLst and Status__c='open' order by CreatedDate];
				        		if(protocolLst != null && protocolLst.size()>0){
					        	    for(Protocol__c protocol : protocolLst){       	
					        	    	Program__c  prgm = protocol.Program_Name__r ;
					        	    	prgm.Program_Name__c = protocol.Program_Name__r.Program_Name__c;
								        prgm.Account__r = protocol.Program_Name__r.Account__r;
								        prgm.Sponsor__r = protocol.Program_Name__r.Sponsor__r;
								        //Account acc = new Account();
								        /*Account acc = protocol.Program_Name__r.Sponsor__r;
								        if(acc != null && acc.Id != null){
								        	prgm.Sponsor__r.Id = acc.Id;
								        	prgm.Sponsor__r.Name = acc.Name;
								        }*/
								        
								        prgm.Reference__c = protocol.Program_Name__r.Reference__c;
								        
			       						protocolProgramMap.put(protocol.id,prgm);
			       						openProtocolLst.add(new SelectOption(protocol.id,protocol.Protocol_Name__c));
			       					}
			       					if(openProtocolLst != null && openProtocolLst.size() >= 1)
			       						openProtocolLst.add(new SelectOption('Not in List','Not in List')); 
				        		}
				        	}
				        }
		        }
			}       		
       }
       /*if profile program manager*/
	}
	
	public pageReference continueWithQuote(){
		String qId = ApexPages.currentPage().getParameters().get('quoteID');
		String redirectToTask = ApexPages.currentPage().getParameters().get('redirectToTask');
		String lastVisitedAssignmentId = '';
		String lastVisitedSavedStage = ''; 
		if(qId != null && qId !=''){
			for(Quote__c quote : currentUsersQuotes){
				if(quote.Id == qId){
					lastVisitedAssignmentId = quote.Last_Visited_Assignment_Id__c;
					lastVisitedSavedStage = quote.Last_Visted_Stage__c;
				}
			}
		}
		PageReference pageRef = null;
		if(redirectToTask == 'continue' && lastVisitedAssignmentId != null && lastVisitedAssignmentId != '' && lastVisitedSavedStage != null && lastVisitedSavedStage !=''){
			//+'?ExQId='+qId+'&ExAssignId='+lastVisitedAssignmentId
			//pageRef = new PageReference(redirectionPageUrl+'/'+lastVisitedSavedStage);
			if(lastVisitedSavedStage=='GLSCommunity_LanguagePage')
				pageRef = Page.GLSCommunity_LanguagePage;
			else if(lastVisitedSavedStage=='GLSCommunity_ServicesPage')
				pageRef = Page.GLSCommunity_ServicesPage;
			else if(lastVisitedSavedStage=='GLSCommunity_PreviewPage')
				pageRef = Page.GLSCommunity_PreviewPage;
			else 
				pageRef = Page.GLSCommunity_TaskType;
								
			pageRef.getParameters().put('qId',qId);
			pageRef.getParameters().put('qaId',lastVisitedAssignmentId);
			if(contactId != null && contactId !=''){
				pageRef.getParameters().put('conID',contactId);
			}
		}else{
			String url = redirectionPageUrl+'/GLSCommunity_taskType';
			system.debug(url);
			//pageRef = new PageReference(url);
			pageRef = Page.GLSCommunity_taskType;
			pageRef.getParameters().put('qId',qId);
			pageRef.getParameters().put('qaID',lastVisitedAssignmentId);
			if(contactId != null && contactId !=''){
				pageRef.getParameters().put('conID',contactId);
			}
		}
		return pageRef;
	}
	public pageReference openTaskType(){		
		String quoteType = ApexPages.currentPage().getParameters().get('quoteType');
		String quoteName = ApexPages.currentPage().getParameters().get('projectName');
		String quoteProtocol = ApexPages.currentPage().getParameters().get('protocol');
		String quoteReference = ApexPages.currentPage().getParameters().get('reference');
		String quoteSponsor = ApexPages.currentPage().getParameters().get('sponsor');		
		
		PageReference pageref = Page.GLSCommunity_TaskType;
		if(quoteType != null && quoteType != '')
			pageref.getParameters().put('type',quoteType);
		
		if(quoteName != null && quoteName != '')
			pageref.getParameters().put('qName',quoteName);
		
		if(quoteProtocol != null && quoteProtocol != '')	
			pageref.getParameters().put('pr',quoteProtocol);
		
		if(quoteReference != null && quoteReference != '')	
			pageref.getParameters().put('ref',quoteReference);
		
		if(quoteType != null && quoteType=='WithProtocol'){
			if(selectedSponsor != null && selectedSponsor !=''){
				pageref.getParameters().put('sp',selectedSponsor);	
			}
			if(selectedProgram!= null && selectedProgram!=''){
				pageref.getParameters().put('selectPrgm',selectedProgram);
			}
		}else{
			if(quoteSponsor != null && quoteSponsor != '')	
				pageref.getParameters().put('sp',quoteSponsor);	
		}	
		if(contactId != null && contactId !=''){
			pageRef.getParameters().put('conID',contactId);
		}	
		pageref.setRedirect(true);
		return pageref;		 
	}
		
   /**
    *   @Description : method to delete an entire project
    *   @Param : none
    **/  
   public pageReference deleteProject(){
   		isAllFileDeleted = false;
	    if(deleteProjectId != null && deleteProjectId != ''){
	    	delFilePathLst = new List<string>();
	        list<Quote_Assignment__c> delAssn = [Select Id from Quote_Assignment__c where Quote__c =: deleteProjectId];
	        if(delAssn != null && delAssn.size() > 0){
	            delete delAssn;
	        }
	        delFiles = new List<Quotes_File__c>();
	        delFiles = [Select Id,Amazon_S3_source_file_path__c,File_Name__c from Quotes_File__c where Quote__c =: deleteProjectId];
	        /*if(delFiles !=null && delFiles.size() > 0){
	        	try{
	            	//delete delFiles;
	            }catch(Exception ex){
			   		delFilePathLst = null;
			   	}
	        }*/
	        delete [Select Id from Quote__c where Id =: deleteProjectId];
	        updateCurrentUserQuotes();
	    }
   	
    return null;
   }
   // method needs to be  ade efficient
   public void deleteFromAmazon(){
   		if(delFiles !=null && delFiles.size()>0){
   			for(Integer i=0;i<delFiles.size();i++){
   				String statusMessage = GLSAmazonUtility.deleteQuoteFile(delFiles[0].id,delFiles[0].Amazon_S3_source_file_path__c,Glsconfig.bucketName);
   				delFiles.remove(i);
   				break;
   			}
   		}else{
   			isAllFileDeleted = true;
   		}
   		/*delFiles = [Select Id,Amazon_S3_source_file_path__c,File_Name__c from Quotes_File__c where Quote__c =: deleteProjectId];
   		if(delFiles !=null && delFiles.size()>0){
   			
   		}*/
   }    
   /**
    *   @Description :update the list displayed on My request page with the latest user quotes
    *   @Param : none
    **/
    public void updateCurrentUserQuotes(){
        
        allQuoteslst = [select id, Quote_Name__c, Project_Name__c, Project_Name_Community__c from Quote__c  WHERE OwnerId =:userObj.Id order by CreatedDate desc];
        if(allQuoteslst != null && allQuoteslst.size() > 0){
            set<string> protocolNumbrSet = new set<string>();
            for(Quote__c qt : allQuoteslst){
                if(qt.Quote_Name__c != null && qt.Quote_Name__c !=''){
                    protocolNumbrSet.add(qt.Quote_Name__c);
                }
                
                if(qt.Project_Name_Community__c == null){
                    qt.Project_Name_Community__c = qt.Project_Name__c;
                }
                existingProjectNames.add(qt.Project_Name_Community__c);
            }
            protocolNumbr = new list<string>(protocolNumbrSet);
            protocolNumbr.sort();
        }else{
            allQuoteslst = new list<Quote__c>();
            protocolNumbr = new list<string>();
        }                
        currentUsersQuotes = [select id,Quote_Name__c,
        							 Project_Number__c,
        							 Project_Name_Community__c,Name,Quote_Number__c,CreatedDate,
        							 Client__r.Name,Bucket_Name__c, Client__c, Project_Name__c,Last_Visited_Assignment_Id__c,
        							 Last_Visted_Stage__c 
        					  from Quote__c where OwnerId =:userObj.Id AND Status__c='Draft' order by CreatedDate desc];
        if(currentUsersQuotes != null && currentUsersQuotes.size() > 0){
            //existingProjectNames = new list<string>();
            for(Quote__c qte : currentUsersQuotes){
                if(qte.Project_Name_Community__c == null){ 
                    qte.Project_Name_Community__c = qte.Project_Name__c;
                }
                //existingProjectNames.add(qte.Project_Name_Community__c);
            }
        }else{
            currentUsersQuotes =  new List<Quote__c>();
        }
    }  
    
    public void populateSponsorAndProgram(){    
    	if(selectedProtocolId=='Not In List'){
    		selectedProgram = '';
    		selectedSponsor = '';
    	}else{
    		selectedProgram = protocolProgramMap.get(selectedProtocolId).Program_Name__c;
    		if(protocolProgramMap.get(selectedProtocolId).Sponsor__c != null){
    			selectedSponsor = protocolProgramMap.get(selectedProtocolId).Sponsor__r.Name;
    		}else{
    			selectedSponsor = protocolProgramMap.get(selectedProtocolId).Reference__c;
    		}
    	}
    }
}