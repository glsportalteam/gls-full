public class SideMenuController {
	public Integer noOfQuotes 									{get;set;}
	public Integer noOfOrders 									{get;set;}
	public User thisUser	  									{get;set;}
	public String accountId 									{get;set;}
	public String profileId										{get;set;}
	public String contactId 									{get;set;} 
	public String profileName									{get;set;}
	public String userId 										{get;set;}
	
	public SideMenuController(){
		List<String> lstQuoteIds = new List<String>();
		String quoteQuery = '';
		String quoteWhere = '';
		contactId = ApexPages.currentPage().getParameters().get('conID');
		if(contactId != null && contactId !='')
			thisUser = [select Id, ContactId, AccountId, ProfileId, email from User where ContactId =: contactId limit 1]; 
		else
			thisUser = [select Id, ContactId, AccountId, ProfileId, email from User where id =: userInfo.getUserId() limit 1]; 

		if (thisUser != null){
			userId		=	thisUser.Id;
			accountId	=  thisUser.AccountId;
			profileId	=  thisUser.ProfileId;
			profileName = [Select Name From Profile Where Id =: profileId].Name;
		}
		
		List<Quote__c> lstQuoteAccess = new List<Quote__c>();
		lstQuoteAccess = getAccessedQuoteList(profileName, userId);
		if(lstQuoteAccess != null && lstQuoteAccess.size()>0){
        		for (Quote__c quote: lstQuoteAccess){
        			lstQuoteIds.add(quote.Id);
        		}
		}
		
		if(profileName != null){
			if(profileName.equalsIgnoreCase(GLSConfig.AccountManager) || profileName.equalsIgnoreCase('Customer Community Manager')){
				if(lstQuoteIds != null && lstQuoteIds.size()>0)
					quoteWhere = ' WHERE ((Client__c = \''+thisUser.AccountId+'\' OR OwnerId =\''+userId+'\' OR (Id in: lstQuoteIds))';
				else
					quoteWhere = ' WHERE ((Client__c = \''+thisUser.AccountId+'\' OR OwnerId =\''+userId+'\')';
			}else{
				if(lstQuoteIds != null && lstQuoteIds.size()>0)
					quoteWhere = ' WHERE ((OwnerId = \''+userId+'\' OR Id in: lstQuoteIds)';
				else
				   quoteWhere = ' WHERE ((OwnerId = \''+userId+'\')';
					
			}	
		}
		
		quoteQuery = 'SELECT count() FROM Quote__c';
		quoteQuery += quoteWhere + ' AND (Status__c = \'Approved\' OR Status__c =\'Direct Order\' OR Status__c = \'In Production\' OR Status__c = \'Completed\' OR Status__c = \'No Quote\' OR Status__c= \'Billable\' OR Status__c= \'Billed\' OR isDirectOrder__c =true ))';
	    noOfOrders = database.countQuery(quoteQuery);
	    
	    quoteQuery='';
	    quoteQuery = 'SELECT count() FROM Quote__c';
	    quoteQuery += quoteWhere + ' AND (Status__c != \'Approved\' AND Status__c != \'Direct Order\' AND Status__c != \'Draft\'  AND Status__c != \'In Production\' AND Status__c != \'No Quote\' AND Status__c != \'Billable\' AND Status__c != \'Billed\' AND Status__c != \'Completed\') AND isDirectOrder__c=false )';
	    noOfQuotes = database.countQuery(quoteQuery);
       				 
	}
	
	
    /* 
	@Description: Method to fetch the Quotes based on the Program and Protocol access given to the user
	@Params: profileName
	@Return Type: List if Quote
	*/
    public List<Quote__c> getAccessedQuoteList(string profileName, string userId){
        List<Quote__c> lstQuote = new List<Quote__c>();
        List<String> lstProgram = new List<String>();
        List<String> lstProtocol = new List<String>();
        List<User_Access_Management__c> lstUAM = new List<User_Access_Management__c>();
        String whereCondition = '';
        String query = '';
        try{
	        lstUAM = [Select ProgramId__c, ProtocolId__c From User_Access_Management__c Where User_Name__c =: userId];
	        
	        if(lstUAM != null && lstUAM.size()>0){
	        	for(User_Access_Management__c uAM : lstUAM){
	        		if(uAM.ProgramId__c != null)
	        			lstProgram.add(uAM.ProgramId__c);
	        		if(uAM.ProtocolId__c != null)
	        			lstProtocol.add(uAM.ProtocolId__c);
	        	}
	        }
	      
	        if(profileName != ''){
		        if((profileName.equalsIgnoreCase(GLSConfig.ProgramManager) || profileName.equalsIgnoreCase('Customer Community Manager')) && (lstProgram != null && lstProgram.size()>0)){
		        		whereCondition = 'Protocol__r.Program_Name__c IN : lstProgram';
		        }else if ((profileName.equalsIgnoreCase(GLSConfig.ProtocolManager) ||  (profileName.equalsIgnoreCase(GLSConfig.LPStdUser)) || profileName.equalsIgnoreCase('Customer Community User Custom')) && 
		        	(lstProtocol != null && lstProtocol.size()>0)){
		        		whereCondition = 'Protocol__c  IN : lstProtocol';
		        }
		        
		        if(whereCondition != ''){
		        	query += 'Select Id From Quote__c Where '+whereCondition;
		        	system.debug('----query----: '+query);
		        	lstQuote =  (List<Quote__c>)Database.query(query);
		        }
	        } 
        }catch(Exception e){
        	system.debug(GLSConfig.ExceptionOccured+'--'+e.getMessage());
        }
       
       system.debug('----lstQuote----: '+lstQuote);
       return lstQuote;
    }
    
}