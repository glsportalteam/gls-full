/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=true)
private class GLSEmailUtilityTest {

    static testMethod void myUnitTest() {
    	
    	EmailTemplate et 	= [SELECT Name,DeveloperName FROM EmailTemplate LIMIT 1];
       	Account accObj		= GLSLPW_SetUpData.createAccount('Amgen');
    	Contact conObj 		= GLSLPW_SetUpData.createContact(accObj.Id, 'Test');
    	conObj.Email 		= 'Test1@xyz.com';
    	update conObj;
    	   	
    	List<String> toAddresses = new List<String>();
        toAddresses.add('Test1@xyz.com');
        toAddresses.add('Test2@xyz.com');
        toAddresses.add('Test3@xyz.com');
    	Test.startTest();
        GLSEmailUtility.sendEmails(conObj.Id,'Test subject',toAddresses,et.DeveloperName,accObj.Id);
        Test.stopTest();
        }
}