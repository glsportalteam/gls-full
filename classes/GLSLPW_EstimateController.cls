/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSLPW_EstimateController Class
* Function: This class is used as a controller extension for the GLSLPW_EstimateDetailPage and GLSLPW_ProgramDetailPage
*/

public with sharing class GLSLPW_EstimateController {
    
    //Variable declarations for the class
    public String submitValue                                   {get; set;}
    public String clientDecisionValue                           {get; set;}
    public Boolean disableSubmit                                {get; set;}     
    public List<EstimateLineWrapper> lstEstimateLineWrapper     {get; set;}
    public Estimate__c est                                      {get; set;}
    public List<Estimate_Files__c> lstEstFiles                  {get; set;}
    public List<Estimate_Files__c> lstRefFiles                  {get; set;}
    public Id estLineId                                         {get; set;}
    public Id efileId                                         	{get; set;}
    public Boolean estFilesVisible                              {get; set;}
    public string estReason                                     {get; set;}
    public string comments                                      {get; set;} 
    public string estimateType                                  {get; set;}
    public string cloneType                                     {get; set;}
    public List<Estimate_Files__c> lstCloneEstFiles             {get; set;}
    public List<Estimate_Files__c> lstNewEstFiles               {get; set;} 
    public Boolean setELIAmount									{get; set;}
    public Boolean blockEliAction								{get; set;}
    public Decimal amtELI										{get; set;}
    public Estimate__c cloneEstimate                            {get; set;}
    public boolean validForObsolete                             {get; set;}
    
    private Id estId;
    private List<Estimate_Line_Item__c> lstEstimateLineItem;
    
    //Constructor
    public GLSLPW_EstimateController (ApexPages.StandardController stdcon){
    	validForObsolete = false;
        submitValue = '';
        clientDecisionValue = '';
        disableSubmit = true;
        estFilesVisible = false;
        setELIAmount = false;
        blockEliAction = true;
        amtELI = 0.00;
        est = new Estimate__c();
        lstEstimateLineItem = new List<Estimate_Line_Item__c>();
        lstEstFiles = new List<Estimate_Files__c>();
        lstRefFiles = new List<Estimate_Files__c>();
        lstEstimateLineWrapper = new List<EstimateLineWrapper>();
        estId = ApexPages.currentPage().getParameters().get('id');
        
        //To control the visibility of Estimate Files section for certain Profiles and users.
        List<Estimate_Files_Visibility__c> lstEstimateProfiles = Estimate_Files_Visibility__c.getall().values();
        User u = [SELECT Profile.Name, Estimate_Files_Visible__c
                    FROM User 
                    WHERE Id =: UserInfo.getUserId() LIMIT 1];
        if(lstEstimateProfiles != null && lstEstimateProfiles.size() > 0){
            for(Estimate_Files_Visibility__c efProfiles : lstEstimateProfiles){
                if(u.Profile.Name == efProfiles.Name){
                    estFilesVisible = true;
                }
                
                else if(u.Profile.Name == GLSconfig.contractManagerProfile){
                	ApexPages.Message warningMsg = new ApexPages.Message(ApexPages.Severity.WARNING, GLSconfig.warningMsg);
        			ApexPages.addMessage(warningMsg);
                	estFilesVisible = false;
                }
            }
        }
        if(u.Estimate_Files_Visible__c){
            estFilesVisible = true;
        }
        if(estId != null){
            lstEstimateLineItem = getEstimateLineItem(estId);
            est = getEstimateRecord(estId);
            lstEstimateLineWrapper = getEstLineWrapper(estId);
            getEstRefFiles();
            if(GLSconfig.statusSaveAmount.contains(est.Status__c) && u.Profile.Name == GLSconfig.contractManagerProfile){
            	setELIAmount = true;
            }
            else{
            	setELIAmount = false;
            }
			
			if(GLSconfig.statusBlockELIAction.contains(est.Status__c)){
				blockEliAction = true;
			}
			else{
				blockEliAction = false;
			}
			
            for(String estStatus:GLSConfig.ValidEstimateStatus){
                if(est.Status__c == estStatus){
                    validForObsolete = true;
                }
            } 
        }
        if(lstEstimateLineItem != null && lstEstimateLineItem.size()>0 && lstRefFiles != null && lstRefFiles.size()>0){
            disableSubmit = false;
        }
    }
    
    /* 
    @Description: To get the complete record of Estimate
    @Params: Estimate Id
    @Return Type: Estimate__c
    */
    private Estimate__c getEstimateRecord(Id estId){
        est =  [SELECT Estimate_Amount__c,Opportunity__c,Reason_for_Cancellation__c,LP_Estimate_ID__c, Original_Estimate__c, Program_Id__c,  
                Program_Id__r.Contact__r.Account.Name,Program_Id__r.Account__c,Program_Id__r.Program_Name__c, Program_Id__r.Contact__r.Name, 
                Program_Id__r.Account__r.Name,Program_Id__r.Sponsor__r.Name,Program_Id__r.Account__r.Industry, is_Approved__c,
                Program_Id__r.Therapeutic_Area__c, Status__c, Name, Id,Program_Id__r.Status__c,Program_Id__r.Name 
                FROM Estimate__c
                WHERE Id =: estId LIMIT 1];
        return est;
    }
    
    /* 
    @Description: Getter for obtaining the values of select list "Submit For" to be displayed on page
    @Params: None
    @Return Type: List<SelectOption>
    */
    public List<SelectOption> getSubmitForValues(){
        List<SelectOption> lstSubmitForValue = new List<SelectOption>();
        lstSubmitForValue = GLSconfig.submitForValueConfig();
        return lstSubmitForValue;
    }
    
    public void cloneEstimate(){
    	cloneEstimate = new Estimate__c();
    	Opportunity oppObj;
    	Map<string,Integer> mapEstTypeCount = new Map<string,Integer>();
    	mapEstTypeCount.put('EstimateVersion',0);
    	mapEstTypeCount.put('EstimateRevision',0);
    	mapEstTypeCount.put('ChangeOrder',0);
    	    	 
    	AggregateResult[] result=[select isChangeOrder__c,isEstimateVersion__c,isEstimateRevision__c,count(ID) from Estimate__c where Original_Estimate__c=:est.id GROUP BY isChangeOrder__c,isEstimateVersion__c,isEstimateRevision__c];
    	for (AggregateResult ar : result)  {
    		if(ar.get('isChangeOrder__c')==true){
    			mapEstTypeCount.put('ChangeOrder',Integer.valueOf(ar.get('expr0'))+1);
    		}
    		if(ar.get('isEstimateVersion__c')==true){
    			mapEstTypeCount.put('EstimateVersion',Integer.valueOf(ar.get('expr0'))+1);
    		}
    		if(ar.get('isEstimateRevision__c')==true){
    			mapEstTypeCount.put('EstimateRevision',Integer.valueOf(ar.get('expr0'))+1);
    		}    				    
		}
    	cloneEstimate.Original_Estimate__c = est.id;    	   
    	if(est.Program_Id__c!=null){	
    		cloneEstimate.Program_Id__c = est.Program_Id__c;
    	}
    	cloneEstimate.Reason_for_Cancellation__c = est.Reason_for_Cancellation__c;
    	cloneEstimate.Status__c = GLSConfig.DraftQuote;    
    	if(est.Opportunity__c != null){	
    		if(estimateType == GLSConfig.ChangeOrder){
    			oppObj = new Opportunity();
                oppObj.AccountId            = est.Program_Id__r.Account__c;
                oppObj.Amount               = est.Estimate_Amount__c;
                oppObj.Industry__c          = est.Program_Id__r.Account__r.Industry;
                oppObj.StageName            = 'B-Goals Shared';
                oppObj.Study_Sponsor__c     = est.Program_Id__r.Sponsor__r.Name;
                oppObj.TherapeuticArea__c	= est.Program_Id__r.Therapeutic_Area__c;
                oppObj.CloseDate = system.today() + 30;                
    		}else{
    			cloneEstimate.Opportunity__c =  est.Opportunity__c;
    		}
    	}
    	if(estimateType == GLSConfig.ChangeOrder){
    		cloneEstimate.isChangeOrder__c = true;
    		Integer tempversion = Integer.valueOf(mapEstTypeCount.get('ChangeOrder'))+1;
    		String version;
    		if(tempversion < 10){
    			version = 0 + '' + tempversion;
    		}else{
    			version = tempversion+'';
    		}
    		cloneEstimate.LP_Estimate_ID__c= est.LP_Estimate_ID__c + '-CHO'+version;
    	} 
    	if(estimateType == GLSConfig.EstimateVersion){
    		Integer tempversion = Integer.valueOf(mapEstTypeCount.get('EstimateVersion'))+1;
    		String version;
    		if(tempversion < 10){
    			version = 0 + '' + tempversion;
    		}else{
    			version = tempversion+'';
    		}
    		cloneEstimate.isEstimateVersion__c = true;
    		cloneEstimate.LP_Estimate_ID__c= est.LP_Estimate_ID__c +'-'+version;
    	}
    	if(estimateType == GLSConfig.EstimateRevision){
    		Integer tempversion = Integer.valueOf(mapEstTypeCount.get('EstimateRevision'))+1;
    		String version;
    		if(tempversion < 10){
    			version = 0 + '' + tempversion;
    		}else{
    			version = tempversion+'';
    		}
    		cloneEstimate.isEstimateRevision__c = true;    		
    		cloneEstimate.LP_Estimate_ID__c= est.LP_Estimate_ID__c +'-R'+version;
    	}    	    	
    	try{    	     	
    		insert cloneEstimate;
    		if(estimateType == GLSConfig.ChangeOrder){
    			if(cloneEstimate.LP_Estimate_ID__c != null)
                	oppObj.Name = cloneEstimate.LP_Estimate_ID__c;
                else
                	oppObj.Name = cloneEstimate.Name;
                	
    			oppObj.Primary_Estimate__c = cloneEstimate.id;
    			insert oppObj;
    			cloneEstimate.Opportunity__c = oppObj.id;
    			update cloneEstimate;
    		}
    	}catch(DMLException dmlEx){
	    	ApexPages.Message statusMsg = new ApexPages.Message(ApexPages.Severity.INFO,dmlEx.getMessage());
	        ApexPages.addMessage(statusMsg);
    	}catch(Exception e){
    		ApexPages.Message statusMsg = new ApexPages.Message(ApexPages.Severity.INFO,e.getMessage());
	        ApexPages.addMessage(statusMsg);
    	}    	
    	if(cloneType==GlSConfig.Clone)
    		cloneEstLineItems(est.id,cloneEstimate.id);
    	
    	if(cloneType==GlSConfig.CloneFilesWithFiles){ 
    		cloneEstLineItems(est.id,cloneEstimate.id);
    		CloneEstFiles(est.id,cloneEstimate.id);		
    	}
    			    	
    }
    public void cloneEstLineItems(Id EstId,Id NewEstId){
    	List<Estimate_Line_Item__c> LstEstLine;
    	List<Estimate_Line_Item__c> LstNewEstLine = new List<Estimate_Line_Item__c>();
    	List<Estimate_Line_Country__c> LstEstLineCountry = new List<Estimate_Line_Country__c>();
    	Map<Integer,String> MapEstLineItemIDIndex = new Map<Integer,String>();     	
    	Map<String,List<Estimate_Line_Country__c>> MapEstIdEstCountrylst = new Map<String,List<Estimate_Line_Country__c>>(); 
    	if(EstId!=null && NewEstId!=null)
    		LstEstLine=[select id,Amount__c,Estimate_Id__c,Protocol_Id__c,(select id,Number_of_Sites__c,Country_Id__c,Estimate_Line_Id__c from Estimate_Line_Country__r) from Estimate_Line_Item__c where Estimate_Id__c=:EstId];
    	
    	if(LstEstLine != null && LstEstLine.size() > 0){
    		Integer i=0;
    		for(Estimate_Line_Item__c estLI:LstEstLine){
    			MapEstLineItemIDIndex.put(i,estLI.id);
    			Estimate_Line_Item__c estLineItem = new Estimate_Line_Item__c();
    			estLineItem.Amount__c = estLI.Amount__c;
    			estLineItem.Protocol_Id__c = estLI.Protocol_Id__c;
    			estLineItem.Estimate_Id__c = NewEstId;
    			LstNewEstLine.add(estLineItem);
    			MapEstIdEstCountrylst.put(estLI.id,estLI.Estimate_Line_Country__r);
    			i = i+1;
    		}    		    		
    	}
    	try{
    		if(LstNewEstLine!=null && LstNewEstLine.size()>0){
    			insert LstNewEstLine;
    			for(Integer j=0;j<LstNewEstLine.size();j++){
    				List<Estimate_Line_Country__c> EstLineCountry = MapEstIdEstCountrylst.get(MapEstLineItemIDIndex.get(j));
    				if(EstLineCountry!=null && EstLineCountry.size()>0){
    					for(Estimate_Line_Country__c country:EstLineCountry){
    						Estimate_Line_Country__c estCountry = new Estimate_Line_Country__c();
    						estCountry.Number_of_Sites__c = country.Number_of_Sites__c; 
    						estCountry.Country_Id__c = country.Country_Id__c;
    						estCountry.Estimate_Line_Id__c=LstNewEstLine[j].id;
    						LstEstLineCountry.add(estCountry);  
    					}
    				}
    			}
    			insert LstEstLineCountry;
    		}    		
    	}catch(DMLException dmlEx){
    		ApexPages.Message statusMsg = new ApexPages.Message(ApexPages.Severity.INFO,dmlEx.getMessage());
	        ApexPages.addMessage(statusMsg);
    	}catch(Exception e){
    		ApexPages.Message statusMsg = new ApexPages.Message(ApexPages.Severity.INFO,e.getMessage());
	        ApexPages.addMessage(statusMsg);
    	}    
    }
    public pageReference redirectToCloneRecord(){
    	if(cloneEstimate!=null && cloneEstimate.id!=null){
	    	PageReference pref = new PageReference('/apex/GLSLPW_EstimateDetailPage?id='+cloneEstimate.id);
	    	pref.setRedirect(true);
	    	return pref;
    	}else{
    		return null;
    	}
    }
    public void CloneEstFiles(Id EstId,Id NewEstId){
    	LstNewEstFiles = new List<Estimate_Files__c>();    	
    	if(EstId!=null && NewEstId!=null)
    		lstCloneEstFiles=[select id,Estimate_File_Name__c,Estimate_Id__c,AWSFilePath__c,File_Type__c,Status__c from Estimate_Files__c where Estimate_Id__c=:EstId];
    	
    	if(lstCloneEstFiles != null && lstCloneEstFiles.size() > 0){
    		for(Estimate_Files__c estFile:lstCloneEstFiles){
    			Estimate_Files__c cloneEstFile = new Estimate_Files__c();
    			cloneEstFile.Estimate_File_Name__c = estFile.Estimate_File_Name__c;
    			cloneEstFile.File_Type__c = estFile.File_Type__c;
    			cloneEstFile.Status__c = estFile.Status__c;
    			cloneEstFile.Estimate_Id__c = NewEstId;
    			LstNewEstFiles.add(cloneEstFile);
    		}    		    		
    	}
    	try{
    		if(LstNewEstFiles != null && LstNewEstFiles.size() > 0){
    			insert LstNewEstFiles; 
    		}
    	}catch(DMLException dmlEx){
    		ApexPages.Message statusMsg = new ApexPages.Message(ApexPages.Severity.INFO,dmlEx.getMessage());
	        ApexPages.addMessage(statusMsg);
    	}catch(Exception e){
    		ApexPages.Message statusMsg = new ApexPages.Message(ApexPages.Severity.INFO,e.getMessage());
	        ApexPages.addMessage(statusMsg);
    	}    	    	    	
    }
    public void copyEstimateFiles(){
    	GLSQW_CloneFileController.copyEstimateFiles(LstNewEstFiles, lstCloneEstFiles);
    }
    
    /* 
    @Description: Method to delete the Estimate_Line_Item__c record.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference deleteELIRecord(){
    	Estimate_Line_Item__c eliObj = new Estimate_Line_Item__c();
    	if(estLineId != null){
	        eliObj = [SELECT Id 
	        			FROM Estimate_Line_Item__c 
	        			WHERE Id =: estLineId LIMIT 1];    	
	    	Integer delIndex;
	        if(eliObj != null){
	            try{
	                delete eliObj; 
	                for(Integer index=0; index < lstEstimateLineWrapper.size(); index++){
	                	if(lstEstimateLineWrapper[index].estLineRecId == estLineId){
	                		delIndex = index;
	                	}
	                }
	                lstEstimateLineWrapper.remove(delIndex);
	                if(est != null){
	                	if(est.Status__c == GLSconfig.ReviewedQuote){
	                		est.Status__c = GLSconfig.InPrepStat;
	                		update est;
	                	}
	                }
	            }
	            catch(DMLException de){
	                system.debug(GLSconfig.DMLExceptionOccured+de);
	            }
	            catch(Exception e){
	                system.debug(GLSconfig.ExceptionOccured+e);
	            }
	        }
    	}
        return null;
    }
    
    /* 
    @Description: Getter for obtaining the values of select list "Client Decision" to be displayed on page
    @Params: None
    @Return Type: List<SelectOption>
    */
    public List<SelectOption> getClientDecisionValues(){
        List<SelectOption> lstClientDecisionValues = new List<SelectOption>();
        lstClientDecisionValues = GLSconfig.clientDecisionConfig();
        return lstClientDecisionValues;
    }
    
    /* 
    @Description: Method to get the associated Estimate Line Items for the Estimate Id.
    @Params: Estimate Id
    @Return Type: List<Estimate_Line_Item__c>
    */
    private List<Estimate_Line_Item__c> getEstimateLineItem(Id estId){
        List<Estimate_Line_Item__c> lstEstimateLineItem = [SELECT Id, Protocol_Id__c
                                                            FROM Estimate_Line_Item__c
                                                            WHERE Estimate_Id__c =: estId];
        return lstEstimateLineItem;                                             
    }
    
    /* 
    @Description: Method to get the associated Estimate Files and Reference Files records for the Estimate Id.
    @Params: Estimate Id
    @Return Type: None
    */
    public void getEstRefFiles(){
        List<Estimate_Files__c> lstAllFiles = [SELECT AWS_File_Id__c, AWSFilePath__c, Description__c, Estimate_File_Name__c, Estimate_Id__c,
                                                File_Type__c, Status__c, CreatedBy.Name, CreatedDate, Name, Id
                                                FROM Estimate_Files__c
                                                WHERE Estimate_Id__c =: estId];
        lstRefFiles = new List<Estimate_Files__c>();
        lstEstFiles = new List<Estimate_Files__c>();
        if(lstAllFiles != null && lstAllFiles.size() > 0){
            for(Estimate_Files__c ef : lstAllFiles){
                if(ef.File_Type__c == GLSconfig.EstFileTyperef){
                    lstRefFiles.add(ef);
                }
                else if(ef.File_Type__c == GLSconfig.EstFileTypeSource){
                    lstEstFiles.add(ef);
                }
            }
        }
        if(lstEstimateLineItem != null && lstEstimateLineItem.size()>0 && lstRefFiles != null && lstRefFiles.size()>0){
            disableSubmit = false;
        } 
        else{
        	disableSubmit = true;
        } 
    }
    
    /* 
    @Description: Method to get the associated Estimate Line Item Country records and populating the wrapper for the Estimate Id.
    @Params: Estimate Id
    @Return Type: List<EstimateLineWrapper>
    */
    private List<EstimateLineWrapper> getEstLineWrapper(Id estId){
        List<EstimateLineWrapper> lstEstimateLineWrapperGet = new List<EstimateLineWrapper>();
        List<Estimate_Line_Item__c> lstEstimateLineItem = [SELECT Id, Protocol_Id__c, Name, Protocol_Id__r.Name,
                                                            Protocol_Id__r.Protocol_Name__c,Amount__c
                                                            FROM Estimate_Line_Item__c
                                                            WHERE Estimate_Id__c =: estId];
        
        Set<Id> setELIId = new Set<Id>();
        if(lstEstimateLineItem != null && lstEstimateLineItem.size() > 0){
            for(Estimate_Line_Item__c eli : lstEstimateLineItem){
                setELIId.add(eli.Id);
            }
        
            List<Estimate_Line_Country__c> lstEstimateLICountry = new List<Estimate_Line_Country__c>(); 
            Map<Id, String> mapEstimateCountry = new Map<Id, String>();
            Map<Id, Integer> mapEstimateCountryCount = new Map<Id, Integer>();
            if(!setELIId.isEmpty()){
               lstEstimateLICountry = [SELECT Estimate_Line_Id__c, Country_Id__r.Name, Number_of_Sites__c
                                        FROM Estimate_Line_Country__c
                                        WHERE Estimate_Line_Id__c IN : setELIId];
               
               if(lstEstimateLICountry != null && lstEstimateLICountry.size() > 0){
                    for(Estimate_Line_Country__c elic : lstEstimateLICountry){
                        
                       if(mapEstimateCountry.containsKey(elic.Estimate_Line_Id__c)){
                            String currentValue = mapEstimateCountry.get(elic.Estimate_Line_Id__c);
                            mapEstimateCountry.put(elic.Estimate_Line_Id__c, currentValue + ', ' + elic.Country_Id__r.Name 
                                                    + ' (' + elic.Number_of_Sites__c + ')');
                            Integer currentCount = mapEstimateCountryCount.get(elic.Estimate_Line_Id__c);
                            mapEstimateCountryCount.put(elic.Estimate_Line_Id__c, currentCount+1);
                       }
                       else{
                            mapEstimateCountry.put(elic.Estimate_Line_Id__c, elic.Country_Id__r.Name + ' (' + elic.Number_of_Sites__c + ')');
                            mapEstimateCountryCount.put(elic.Estimate_Line_Id__c,1);
                       }
                   }
               	}
            }
            
            for(Estimate_Line_Item__c eli : lstEstimateLineItem){
                lstEstimateLineWrapperGet.add(new EstimateLineWrapper(eli.Id, eli.Name, eli.Protocol_Id__r.Name, eli.Protocol_Id__r.Protocol_Name__c, 
                                                mapEstimateCountryCount != null && mapEstimateCountryCount.containsKey(eli.Id) ? mapEstimateCountryCount.get(eli.Id) : 0, 
                                                mapEstimateCountry != null && mapEstimateCountry.containsKey(eli.Id) ? mapEstimateCountry.get(eli.Id) : '',
                                                eli.Amount__c));
            }
        }
        return lstEstimateLineWrapperGet;
    }
    
    /* 
    @Description: Downloads the Estimate File from S3 server.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference fetchEstimateFile(){
    	String stMessage = '';
        String statusMessage ='';
        String prefStr = '';
        PageReference pref = null;
        if(estId != null){
        	Estimate_Files__c eFiles = [SELECT Id, AWSFilePath__c, Estimate_File_Name__c
        								FROM Estimate_Files__c
        								WHERE Id =: efileId LIMIT 1];
        	if(eFiles != null){
        		String path = eFiles.AWSFilePath__c + eFiles.Estimate_File_Name__c;
        		if(path != null){                                 
                    statusMessage = GLSAmazonUtility.downloadQuoteFile(path,GLSConfig.bucketName); 
                    try{
                        if(statusMessage != null){
                        	List<String> urlParams = statusMessage.split('Signature=');
                        	if(urlParams != null && urlParams.size() > 1){                        	
	                        	String signature = EncodingUtil.urlDecode(urlParams[1],'UTF-8');   
	                        	String sigEncoded;
	                        	/*To overcome the error of "Signature Does Not Match" when the generated signature contains space which is in 
	                        	turn replaced by "+" */
	                        	if(signature.contains('+')){
	                        		sigEncoded = signature.replace('+','%2B');
	                        	}    
	                        	else{
	                        		sigEncoded = signature;
	                        	}    
	                            prefStr = urlParams[0] + 'Signature=' + sigEncoded;
                        	}
                        }                                                                                                                        
                    }
                    catch(Exception e){                    
                        stMessage = e.getMessage();                    
                    }
                }
                pref = new PageReference(prefStr);
        	}
        }
        return pref;
    }
    
    /* 
    @Description: Deletes the Estimate File from S3 server.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference deleteEstimateFile(){
    	String stMessage = '';
        String statusMessage ='';
        if(estId != null){
        	Estimate_Files__c eFiles = [SELECT Id, AWSFilePath__c, Estimate_File_Name__c
        								FROM Estimate_Files__c
        								WHERE Id =: efileId LIMIT 1];
        	if(eFiles != null){
        		String path = eFiles.AWSFilePath__c + eFiles.Estimate_File_Name__c;
        		try{
                    String bucketName = GLSConfig.bucketName;
                    StatusMessage = GLSAmazonUtility.deleteFilesFromAmazon(efileId,path);
                    if(StatusMessage.equalsIgnoreCase('true')){
                        StMessage ='File Deleted Successfully';
                    }
                    else{
                        StMessage ='File not deleted';
                    }
                }
                catch(Exception e){
                    StMessage = e.getMessage();
                    system.debug(GLSConfig.ExceptionOccured);
                }
            }
        }
        ApexPages.Message statusMsg = new ApexPages.Message(ApexPages.Severity.INFO, StMessage);
        ApexPages.addMessage(statusMsg);
        getEstRefFiles();
        return null; 
    }
    
     /* 
    	@Description: Decline the estimate and update estimate status with reason for declination.
    	@Params: None
    	@Return Type: Void
    */
    public void declineEstimate(){
    	if(estId!=null){
    		Estimate__c est = new Estimate__c(Id=estId);
    		est.status__c = GLSConfig.Cancelled;
    		if(estReason!=null)
    			est.Reason_for_Cancellation__c = estReason;
    		if(comments!=null)
    			est.Comments_for_Cancellation__c = comments;	
    		try{
    			update est;
    		}catch(Exception e){
	    		ApexPages.Message statusMsg = new ApexPages.Message(ApexPages.Severity.INFO, e.getMessage());
	        	ApexPages.addMessage(statusMsg);
    		}
    		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();           
            mail.setTargetObjectId(UserInfo.getUserId());
            mail.setWhatId(estId);
            mail.setToAddresses(new list<string>{'yash_mittal@persistent.co.in'});                                                                              
            Id templateId = [SELECT e.Id, e.DeveloperName 
                            FROM EmailTemplate e 
                            WHERE e.DeveloperName = 'Est_Cancelled'
                            LIMIT 1].Id;
            if(templateId != null){
                mail.setTemplateId(templateId);
            }
            try{                
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }catch(Exception e){
                system.debug(e.getMessage());
            }
    	}
    }
    
    /* 
    @Description: Performs the "Estimate Tracker", "Internal Approval" and "Release to Client" actions for Submit For picklist. 
    @Params: None
    @Return Type: PageReference
    */
    public PageReference submitForProcess(){
    	try{
			if(submitValue == GLSconfig.EstimateTracker){
				if(est != null){
					est.Status__c = GLSconfig.NewQuote;					
					update est;
					setELIAmount = true;
				}
			}		
			else if(submitValue == GLSconfig.InternalApproval){
				if(est.Id != null){
					Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
			        req.setComments(GLSConfig.submitMsg);
			        req.setObjectId(est.id);
			        Approval.ProcessResult result = Approval.process(req);
				}
			}
			else if(submitValue == GLSconfig.ReleaseToClient){
				if(est != null){
					est.Status__c = GLSConfig.Released;
					update est;
					setELIAmount = true;
				}
			}
    	}
    	catch(DMLException de){
            system.debug(GLSConfig.DMLExceptionOccured+de);
        }
        catch(Exception e){
            system.debug(GLSConfig.ExceptionOccured+e);
        }
		return null;
    }
	
	/* 
    @Description: Saves the Estimate Line item amount for the particular Estimate Line Item record
    @Params: None
    @Return Type: PageReference
    */
    public PageReference saveELIAmount(){
    	if(estLineId != null){
    		Estimate_Line_Item__c eliRecord = [SELECT Id, Amount__c
    											FROM Estimate_Line_Item__c
    											WHERE Id =: estLineId LIMIT 1];
	    	if(eliRecord != null){
	    		if(eliRecord.Amount__c != amtELI){
			    	eliRecord.Amount__c = amtELI;
			    	try{
						update eliRecord;
						setELIAmount = true;
						if(est != null){
					    	est.Status__c = GLSconfig.InPrepStat;
							update est;
							setELIAmount = true;
				    	}
					}
					catch(DMLException de){
		                system.debug(GLSConfig.DMLExceptionOccured+de);
		            }
		            catch(Exception e){
		                system.debug(GLSConfig.ExceptionOccured+e);
		            }
	    		}
	    	}
    	}
    	return null;
    }
    
    //Wrapper Class for populating related list of Estimate Line Item on Estimate Detail Page
    public class EstimateLineWrapper{
        public Id estLineRecId          {get; set;}
        public String estLineId         {get; set;}
        public String protocolId        {get; set;}
        public String protocolName      {get; set;}
        public Integer numCountries     {get; set;}
        public String countries         {get; set;}
        public Decimal amount           {get; set;}
        
        public EstimateLineWrapper(Id estLineRecId, String estLineId, String protocolId, String protocolName, Integer numCountries, 
                                    String countries, Decimal amount){
            this.estLineRecId = estLineRecId;
            this.estLineId = estLineId;
            this.protocolId = protocolId;
            this.protocolName = protocolName;
            this.numCountries = numCountries;
            this.countries = countries;
            this.amount = amount;
        }
    }
}