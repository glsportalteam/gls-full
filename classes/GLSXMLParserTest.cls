/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSXMLParserTest {

    static testMethod void myUnitTest() {
        GLSSetupData.createQLIServices();
        String credName = createTestCredentials();
		GLSAmazonUtility.AWSCredentialName = credName;
		
        //String path = 'amgen*a0rK0000001AcOpIAK/Trados/625/AutomaticTask20140401_080230_853242.xml';
        String AnalyzedURL = GLSAmazonUtility.downloadQuoteFile('Test.txt',GLSConfig.bucketName); 
        String XML = '<task name="analyse"><taskInfo taskId="f37741fb-bc3b-4aeb-9959-e9343357f4b3" runAt="4/24/2014 6:05:32 AM" runTime="56 seconds"><project name="AutomaticTask20140424_060517_455065" number="21b852de-18b3-4802-987f-6b3d0bf097fd" /><language lcid="2057" name="English (United Kingdom)" /><tm name="empty tm.sdltm" /><settings reportInternalFuzzyLeverage="no" reportCrossFileRepetitions="yes" minimumMatchScore="70" searchMode="bestWins" missingFormattingPenalty="1" differentFormattingPenalty="1" multipleTranslationsPenalty="1" autoLocalizationPenalty="0" textReplacementPenalty="0" /></taskInfo><file name="Test Doc2.docx.sdlxliff" guid="f459e9d3-8c9a-4741-9fbc-6ae45c5637a1"><analyse><perfect segments="0" words="0" characters="0" placeables="0" tags="0" /><inContextExact segments="0" words="0" characters="0" placeables="0" tags="0" /><exact segments="0" words="0" characters="0" placeables="0" tags="0" /><crossFileRepeated segments="0" words="0" characters="0" placeables="0" tags="0" /><repeated segments="1993" words="40035" characters="231480" placeables="485" tags="0" /><total segments="1998" words="40122" characters="231984" placeables="486" tags="0" /><new segments="5" words="87" characters="504" placeables="1" tags="0" /><fuzzy min="50" max="74" segments="0" words="0" characters="0" placeables="0" tags="0" /><fuzzy min="75" max="84" segments="0" words="0" characters="0" placeables="0" tags="0" /><fuzzy min="85" max="94" segments="0" words="0" characters="0" placeables="0" tags="0" /><fuzzy min="95" max="99" segments="0" words="0" characters="0" placeables="0" tags="0" /></analyse></file><file name="Test Ratesheet.xlsx.sdlxliff" guid="2bcfd4ca-e4db-4453-8553-df18dc3f5023"><analyse><perfect segments="0" words="0" characters="0" placeables="0" tags="0" /><inContextExact segments="0" words="0" characters="0" placeables="0" tags="0" /><exact segments="0" words="0" characters="0" placeables="0" tags="0" /><crossFileRepeated segments="0" words="0" characters="0" placeables="0" tags="0" /><repeated segments="86" words="89" characters="282" placeables="83" tags="0" /><total segments="265" words="317" characters="1404" placeables="183" tags="8" /><new segments="179" words="228" characters="1122" placeables="100" tags="8" /><fuzzy min="50" max="74" segments="0" words="0" characters="0" placeables="0" tags="0" /><fuzzy min="75" max="84" segments="0" words="0" characters="0" placeables="0" tags="0" /><fuzzy min="85" max="94" segments="0" words="0" characters="0" placeables="0" tags="0" /><fuzzy min="95" max="99" segments="0" words="0" characters="0" placeables="0" tags="0" /></analyse></file><batchTotal><analyse><perfect segments="0" words="0" characters="0" placeables="0" tags="0" /><inContextExact segments="0" words="0" characters="0" placeables="0" tags="0" /><exact segments="0" words="0" characters="0" placeables="0" tags="0" /><crossFileRepeated segments="0" words="0" characters="0" placeables="0" tags="0" /><repeated segments="2079" words="40124" characters="231762" placeables="568" tags="0" /><total segments="2263" words="40439" characters="233388" placeables="669" tags="8" /><new segments="184" words="315" characters="1626" placeables="101" tags="8" /><fuzzy min="50" max="74" segments="0" words="0" characters="0" placeables="0" tags="0" /><fuzzy min="75" max="84" segments="0" words="0" characters="0" placeables="0" tags="0" /><fuzzy min="85" max="94" segments="0" words="0" characters="0" placeables="0" tags="0" /><fuzzy min="95" max="99" segments="0" words="0" characters="0" placeables="0" tags="0" /></analyse></batchTotal></task>';
        test.startTest();
        
        	GLSXMLParser parserObj = new GLSXMLParser();
        	parserObj.parser(GLSSetupData.quoteLI1.id, GLSSetupData.quote1.Id, AnalyzedURL,GLSSetupData.quoteLI1.Error_Message__c , GLSSetupData.quoteLI1.Task_Status__c, XML, 'analyzedSingleQLI');
        test.stopTest();
    }
    
    private static String createTestCredentials(){
    	AWSKey__c testKey = new AWSKey__c(name='test keys',key__c='key',secret__c='secret');
        insert testKey;
        return testKey.name;    
    }
}