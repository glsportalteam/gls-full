public with sharing class GLSEmailUtility {
	public static void sendEmails(String whoId,String subject,list<string> toAddresses,String templateName,String WhatId){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        mail.setTargetObjectId(whoId);
        if(WhatId != null && WhatId != '')
        	mail.setWhatId(WhatId);
        	
        if(subject!=null && subject!='')
        	//mail.setSubject(subject);
        		
        mail.setToAddresses(toAddresses);                
        Id templateId = [SELECT e.Id, e.DeveloperName 
                          FROM EmailTemplate e 
                          WHERE e.DeveloperName =: templateName
                          LIMIT 1].Id;
        if(templateId != null){
                mail.setTemplateId(templateId);
        }        
        mail.setSaveAsActivity(false);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });        
	}
}