public class GLS_CommunityNewLeadByWebClass
{

 public static boolean futureMethodDone = false;
 
 @future
 public static void newLeadByWebMethod(set<string> leadIds)
 {
    futureMethodDone = true;
    
    list<lead> leds=[select id,Company,IsLeadOwnerChange__c  from lead where id in:leadIds];
     String companyName ;
     String  leadId ;
     String ownerId ;
     Lead leadObj; 
     boolean IsLeadOwnerChange;
     list<Lead> newLeadsLst = new  list<Lead>();
     
     List<Account> lstAccount = new List<Account>();
     List<User> lstUser= new List<User>();
     
     for(Lead lead : leds){
                companyName = lead.Company;
                leadId = lead.Id;
                IsLeadOwnerChange = lead.IsLeadOwnerChange__c;
           
           System.debug('IsLeadOwnerChange--->'+IsLeadOwnerChange );
           
            if(!IsLeadOwnerChange)
            {
                 if(companyName!= null && companyName  !=''){
                     lstAccount = [Select Id,OwnerId, Name from Account where Name =: companyName limit 1];
                    
                 }
                 if(lstAccount  != null && lstAccount.size()>0)
                 {
                      System.debug('lstAccount  --->'+lstAccount  );
                      
                     ownerId = lstAccount[0].OwnerId;
                     System.debug('ownerId -'+ ownerId );
                     if(ownerId != null)
                     {
                         
                     leadObj = new Lead(id = leadId );
                     leadObj.OwnerId =  ownerId;
                     leadObj.status =  'Open';
                     //leadObj.state = 'CA';
                     leadObj.IsLeadOwnerChange__c =  true;
                     leadObj.LeadSource = 'Portal';
                     newLeadsLst.add(leadObj);        
                     
                     }
                   }else
                   {
                     String role= 'VP of Sales';
                     String roleId = [Select Id From UserRole Where Name =:role limit 1].Id;
                     
                     if(roleId != null && roleId != '')
                      lstUser = [Select Id from User where UserRoleId =: roleId limit 1];
                     
                      System.debug('lstUser --->'+lstUser );
                          
                     if(lstUser != null && lstUser.size()>0)
                     {
                         ownerId = lstUser[0].Id;
                         System.debug('ownerId -'+ ownerId );
                         if(ownerId != null)
                         {
                             
                             leadObj = new Lead(id = leadId );
                             leadObj.OwnerId =  ownerId;
                             leadObj.status =  'Open';
                             //leadObj.state = 'CA';
                             leadObj.IsLeadOwnerChange__c =  true;
                             leadObj.LeadSource = 'Portal';
                             newLeadsLst.add(leadObj);             
                        
                     }
                    }
                   }
        }
      }
      
      if(newLeadsLst.size()>0)
      {
        try{
                 update newLeadsLst;
                 
               }catch(Exception e)
               {
                 System.debug('Inside Lead records update-----' + e);
              }
       
     }
  }
  }