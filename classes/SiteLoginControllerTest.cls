/**
 * An apex page controller that exposes the site login functionality
 */
@IsTest global with sharing class SiteLoginControllerTest {
    @IsTest(SeeAllData=true) global static void testSiteLoginController () {
   profile p =[select id from profile where Name =: 'Customer Community Manager' limit 1];
   
    Account acc = new Account();
    acc.name = 'Test Account';
    acc.BillingStreet = 'Sample';
    acc.BillingCity = 'Sample';
    acc.BillingState = 'Sample';
    acc.BillingStreet = 'Sample';
    acc.BillingPostalCode='Sample';
    acc.BillingCountry='Sample';
    insert acc;
    
    Contact cont = new Contact();
    cont.AccountId = acc.Id;
    cont.FirstName= 'Test';
    cont.LastName = 'Contact';
    insert cont;
    
    User urs = new User();
     urs.FirstName='test';
     urs.LastName='test';
     urs.Email='test@test.com';
     urs.Alias='abcd';
     urs.ContactId=cont.id;
     urs.Username='test@salesforce1234.com';
     urs.CommunityNickname='abcd';
   //  urs.UserRoleId=usr.UserRoleId;
    // urs.isactive=true;
     urs.EmailEncodingKey='UTF-8';
     urs.timezonesidkey='America/Los_Angeles';
     urs.LocaleSidKey='en_US';
     
     urs.languagelocalekey='en_US';
     //urs.IsPartner=true;
     urs.ProfileId=p.id;
     insert urs;
        // Instantiate a new controller with all parameters in the page
        SiteLoginController controller = new SiteLoginController ();
        controller.username = 'test@salesforce1234.com';
        controller.password = '123456'; 
        controller.forgotPassword();
        controller.dummy();       
        System.assertEquals(controller.login(),null);                           
    }    
}