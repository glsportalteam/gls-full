/* Copyright (c) 2014, Global Languages Solutions, Inc. All rights reserved.
* Author : Persistent Systems
* Class  : GLSDeleteAPILogsScheduler 
* Function: This schedule class helps in deleting Logger records after specified days.
*/

global class GLSDeleteAPILogsScheduler implements Schedulable {
 
  global void execute(SchedulableContext sc) {
       Decimal numberOfDays=0;
       Delete_Logger_Record__c deleteLogger=Delete_Logger_Record__c.getInstance('Number of Days');

       if(deleteLogger != null){
           numberOfDays =deleteLogger.Value__c;
       }else{
           numberOfDays =30;
       }
       
       List<GLS_API_Listener_Logger__c> lstGLSAPIListenerLogger=[select id, Number_of_Days__c from GLS_API_Listener_Logger__c where     Number_of_Days__c>=: numberOfDays ];
       try{
         delete lstGLSAPIListenerLogger;
       }catch(Exception ex){
           System.debug(GLSConfig.ExceptionOccured);
       }
       List<GLS_API_Logger__c> lstGLSAPILogger=[select id,Number_of_Days__c  from GLS_API_Logger__c where   Number_of_Days__c >=: numberOfDays ];
       try{
         delete lstGLSAPILogger;
       }catch(Exception ex){
           System.debug(GLSConfig.ExceptionOccured);
       }

   }
  
  }