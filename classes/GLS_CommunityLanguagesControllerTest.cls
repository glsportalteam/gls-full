/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod 
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.

*/
@isTest

private class GLS_CommunityLanguagesControllerTest{
        public static Account accountObj;  
        public static User u;
        public static Contact contactObj;    
        public static Quote__c quote;
        public static Quote_Assignment__c qAsst1;
        public static Language_List_Item__c defLanguage;
        public static Language_List_Item__c srcLang;
        public static Language_List_Item__c targetLang;
        public static Language_List_Item__c srcLangunc;
        public static Language_List_Item__c targetLangUnc;
        public static Language_Bundle__c bundle;
    
        static testMethod void myUnitTest1() {
            test.startTest();
            
            accountObj = new Account();
            accountObj.Name = 'Amgen';
            accountObj.BillingState ='Texas';
            insert accountObj;
                     
            contactObj = new Contact(); 
            contactObj.FirstName = 'Jennifer A';
            contactObj.LastName = 'Jennifer A';
            contactObj.AccountId = accountObj.Id;
            insert contactObj; 
            
            srcLang = new Language_List_Item__c();
            srcLang.Name = 'Arabic';
            srcLang.Is_Common__c = true;            
            targetLang = new Language_List_Item__c();
            targetLang.Name = 'Bengali';
            targetLang.Is_Common__c = true;
            insert targetLang;
            insert srcLang;
            
            srcLangUnc = new Language_List_Item__c();
            srcLangUnc.Name = 'Marathi';
            srcLangUnc.Is_Common__c = false;            
            targetLangUnc = new Language_List_Item__c();
            targetLangUnc.Name = 'Hindi';
            targetLangUnc.Is_Common__c = false;
            insert targetLangUnc;
            insert srcLangUnc;
            
            Profile p = [select id from profile where name=: GLSConfig.Communityprofile];
          /*  u = new User(alias = 'gport1', email='GLSProgram1@testGLS.com',emailencodingkey='UTF-8',ContactId=contactObj.Id,
                        lastname='Testing', languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id,
                        timezonesidkey='America/Los_Angeles', username='program1@gls.com');
            insert u;   */
            u = [SELECT Id, contactId, accountId  FROM User where contactId != null AND accountid != null ][0];                     
            System.runAs ( new User(Id = u.id) ){
                quote = new Quote__c();
                quote.Quote_Name__c = 'Test Quote';
                quote.Client__c = u.accountId;
                quote.Status__c='New';
                quote.Contact__c = u.ContactId;
                quote.isFileClonedComplete__c = false;
                insert quote;
                    
                    
                defLanguage = new Language_List_Item__c();
                defLanguage.Name = 'English';
                defLanguage.Language_Name__c = 'en-US';
                insert defLanguage;
                    
                qAsst1 = new Quote_Assignment__c();
                qAsst1.Quote__c = quote.Id; 
                qAsst1.Source_Language__c = defLanguage.Id;     
                qAsst1.Allowed_File_Formats__c = 'MS Word';
                qAsst1.Document_Type__c = 'IFC,SomeData;';
                qAsst1.Keywords__c='one;';
                insert qAsst1; 
                
                bundle = new Language_Bundle__c();
                bundle.bundle_name__c = 'new bundle';
                bundle.Source_Language__c = srcLang.id;
                bundle.Target_Language__c = targetLang.id;
                bundle.User__c = u.ContactId;
                insert bundle;
                
                GLS_CommunityUploadFileController obj = new GLS_CommunityUploadFileController();
                obj.quoteObj = quote;
                obj.quoteAssignmentObj = qAsst1;                 
                Community_GlSSelectLanguagesController ctrlObj = new Community_GlSSelectLanguagesController();
                ctrlObj = new Community_GlSSelectLanguagesController(obj);
                
                Community_GlSSelectLanguagesController.SourceLanguagesWrapper slwData = new Community_GlSSelectLanguagesController.SourceLanguagesWrapper(srcLangUnc, true, false);
                Community_GlSSelectLanguagesController.TargetLanguagesWrapper tlwData = new Community_GlSSelectLanguagesController.TargetLanguagesWrapper(targetLangUnc, true, false);
                Community_GlSSelectLanguagesController.SourceLanguagesWrapper slwCommonData = new Community_GlSSelectLanguagesController.SourceLanguagesWrapper(srcLang, true, false);
                Community_GlSSelectLanguagesController.TargetLanguagesWrapper tlwCommonData = new Community_GlSSelectLanguagesController.TargetLanguagesWrapper(targetLang, true, false);
                Community_GlSSelectLanguagesController.SourceLanguagesWrapper slwEditBundleData = new Community_GlSSelectLanguagesController.SourceLanguagesWrapper(srcLangUnc, true, false);
                Community_GlSSelectLanguagesController.TargetLanguagesWrapper tlwEditBundleData = new Community_GlSSelectLanguagesController.TargetLanguagesWrapper(targetLangUnc, true, false);

                ctrlObj.targetLangWrapperList.add(tlwData);
                ctrlObj.sourceLangWrapperList.add(slwData);
                ctrlObj.commonTargetLangWrapperList.add(tlwCommonData);
                ctrlObj.commonSourceLangWrapperList.add(slwCommonData);
                ctrlObj.editBundleTargetLangWrapperList.add(tlwEditBundleData);
                ctrlObj.editBundleSourceLangWrapperList.add(slwEditBundleData);
                ctrlObj.prePopulateLanguages();
                ctrlObj.removeTargetLang();
                ctrlObj.removeSourceLang();
                ctrlObj.saveLanguagesToAssignment();
                
                
                ctrlObj.targetLangWrapperList.add(tlwData);
                ctrlObj.sourceLangWrapperList.add(slwData);
                ctrlObj.commonTargetLangWrapperList.add(tlwCommonData);
                ctrlObj.commonSourceLangWrapperList.add(slwCommonData);
                ctrlObj.sourceLangWrapperList.add(slwCommonData);
                ctrlObj.sourceTargetLangsSelected();
                ctrlObj.isSourceChange = true;
                ctrlObj.bundleList.add(bundle);
                ctrlObj.IdBundleToEdit = bundle.Id;
                ctrlObj.editBundleList();
                
                ctrlObj.editSourceLanguageId = bundle.Id;
                ctrlObj.editBundleSourceTargetLangsSelected(); 
                
                //ctrlObj.deleteBundle();    
                
                obj.quoteAssignmentObj.Source_Language__c = defLanguage.Id; 
                ctrlObj.addEnglishToSourceList();
                slwCommonData = new Community_GlSSelectLanguagesController.SourceLanguagesWrapper(srcLang, true, false);
                tlwCommonData = new Community_GlSSelectLanguagesController.TargetLanguagesWrapper(targetLang, true, false);
                tlwData = new Community_GlSSelectLanguagesController.TargetLanguagesWrapper(targetLangUnc, true, false);
                slwData = new Community_GlSSelectLanguagesController.SourceLanguagesWrapper(srcLangUnc, true, false);
                
                ctrlObj.bundleTargetLangWrapperList.add(tlwData);
                ctrlObj.commonBundleTargetLangWrapperList.add(tlwCommonData);
                ctrlObj.bundleSourceLangWrapperList.add(slwData);
                ctrlObj.commonBundleSourceLangWrapperList.add(slwCommonData);
                ctrlObj.bundleSourceTargetLangsSelected();
                
                slwCommonData = new Community_GlSSelectLanguagesController.SourceLanguagesWrapper(srcLang, true, false);
                tlwCommonData = new Community_GlSSelectLanguagesController.TargetLanguagesWrapper(targetLang, true, false);
                tlwData = new Community_GlSSelectLanguagesController.TargetLanguagesWrapper(targetLangUnc, true, false);
                slwData = new Community_GlSSelectLanguagesController.SourceLanguagesWrapper(srcLangUnc, true, false);
                
                
                ctrlObj.targetLangWrapperList.add(tlwData);
                ctrlObj.sourceLangWrapperList.add(slwData);
                ctrlObj.commonTargetLangWrapperList.add(tlwCommonData);
                ctrlObj.commonSourceLangWrapperList.add(slwCommonData);
                ctrlObj.addToCart();
                ctrlObj.clearPopulatedLanguageList();
                ctrlObj.clearPopulatedBundleLanguageList();
                ctrlObj.clearEditPopulatedBundleLanguageList();
                ctrlObj.clearCurrentList();
                
                ctrlObj.addbundleSrcTargetLang();
                                
                Community_GlSSelectLanguagesController.SourceLanguagesWrapper slw = new Community_GlSSelectLanguagesController.SourceLanguagesWrapper();
                Community_GlSSelectLanguagesController.TargetLanguagesWrapper tlw = new Community_GlSSelectLanguagesController.TargetLanguagesWrapper();
                 ctrlObj.dummy();
                test.stopTest();
            }
        }
}