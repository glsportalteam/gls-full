global class GLSDeleteQuoteFilesBatchClass implements Database.Batchable<SObject>, Database.AllowsCallouts{
    
    private String query;
    public AWSKeys credentials;
    private boolean isFlag=false;
    public boolean isTestRun = false;
    public S3.AmazonS3 as3 { get; private set; }
    private String AWSCredentialName = 'NAME OF KEY TO USE'; //Modify this string variable to be the name of the AWS Credential record that contains the proper AWS keys and secret
    public string secret { get {return credentials.secret;} }
    public  string key { get {return credentials.key;} }
    Integer sec;
    Integer min;
    Integer hour;
    Integer day;
    Integer month;
    Integer year;
    List<Quotes_File__c> lstDelQFiles;
    string StMessage = '';
    string StatusMessage ='';
    String path = '';
    
    global GLSDeleteQuoteFilesBatchClass (String query){
         this.query = query;
    } 
    global GLSDeleteQuoteFilesBatchClass (){
         
    } 
    global Database.QueryLocator start(Database.BatchableContext bcStart) { 
        query = 'Select Id,Amazon_S3_source_file_path__c, File_Name__c,Quote__r.Name,Quote__r.Quote_Number__c, Quote__r.Client__r.Name,Quote__r.Id,Bucket_Name__c from Quotes_File__c  limit 1';        
        return Database.getQueryLocator(query);
    }
      
    
    global void execute(Database.BatchableContext bcExecute, List<SObject> lstQLI){
        
        //if(qFileId != null){
            
            lstDelQFiles = new List<Quotes_File__c>();
            lstDelQFiles=[Select Id,Amazon_S3_source_file_path__c, File_Name__c,Quote__r.Name,Quote__r.Quote_Number__c, Quote__r.Client__r.Name,Quote__r.Id,Bucket_Name__c from Quotes_File__c where Quote__c=:null and bucket_name__c != null limit 1];
            if(lstDelQFiles != null && lstDelQFiles.size()>0){
                if(lstDelQFiles[0].Amazon_S3_source_file_path__c != null){
                
                    path =lstDelQFiles[0].Amazon_S3_source_file_path__c;
                    try{
                        StatusMessage = GLSAmazonUtility.deleteQuoteFile(lstDelQFiles[0].Id,path,lstDelQFiles[0].Bucket_Name__c);
                        if(StatusMessage.equalsIgnoreCase('true'))
                            StMessage ='File Deleted Successfully';
                        else
                            StMessage ='File not deleted';
                        
                    }catch(Exception e){
                        StMessage = e.getMessage();
                        system.debug(GLSConfig.ExceptionOccured);
                    }
                 }else{
                     delete lstDelQFiles;
                 }
            }

         
    } 
    global void finish(Database.BatchableContext bcFinish){
        Datetime sysTime = System.now().addSeconds(30) ;
        sec = sysTime.second();
        min = sysTime.minute();
        hour = sysTime.hour();
        day = sysTime.day();
        month = sysTime.month();
        year = sysTime.year();
        //String chronExpression = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        String chronExpression = '' + sec + ' ' + min + ' ' + hour + ' ' + day + ' ' + month + ' ? ' + year;
        if(!isTestRun){      
            System.schedule( 'Deletion of marked files [Amazon] - ' + sysTime, chronExpression, new GLSDeleteQuoteFilesSchedulerClass ());
        }else{
            System.debug('This is a test run');
            String scheduleName;
            scheduleName = 'Deletion of marked files [Amazon] - ' + sysTime;
            System.debug('Name of Schedule : ' + scheduleName);
            System.debug('Time :' + chronExpression);
            
            lstDelQFiles = new List<Quotes_File__c>();
            lstDelQFiles=[Select Id,Amazon_S3_source_file_path__c, File_Name__c,Quote__r.Name,Quote__r.Quote_Number__c, Quote__r.Client__r.Name,Quote__r.Id,Bucket_Name__c from Quotes_File__c where Quote__c=:null limit 1];
            if(lstDelQFiles != null && lstDelQFiles.size()>0){
                if(lstDelQFiles[0].Amazon_S3_source_file_path__c != null){
                     path =lstDelQFiles[0].Amazon_S3_source_file_path__c;
                     System.debug('Fil that would be deleted : '  + path ); 
                 }
            }
            
          
        
        }
    }
}