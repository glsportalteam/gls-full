/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSCommunity_ProjectDetailControllerExt Class
* Function: This class is used as a controller extension for the GLSCommunity_ProjectDetailPage
*/

public without sharing class GLSCommunity_ProjectDetailControllerExt {
	
	//Variable declaration for the class
	public Quote__c	quoteObj					{get; set;}
	public String certificationType				{get; set;}
	public String quoteStatus					{get; set;}
	public String accName						{get; set;}
	public String dateStr						{get; set;}
	public String addEmail						{get; set;}
	public String projectName 					{get; set;}
	public Id contactId							{get; set;} 
	public Id qaId                              {get; set;}
	public Id qId                               {get; set;}
	
	private Account acc;
	private User currentUser;
	public string quoteDate {get;set;}
	
	//Constructor
	public GLSCommunity_ProjectDetailControllerExt(){
		quoteObj = new Quote__c();
		accName = 'Account Certificate';
		projectName = '';
		qaId = ApexPages.currentPage().getParameters().get('qaId');
		qId = ApexPages.currentPage().getParameters().get('qId');
		contactId = ApexPages.currentPage().getParameters().get('conId');
		if(qId != null){
			quoteObj = getQuoteRecord(); 
		}
		if(quoteObj != null && quoteObj.Project_Due_Date__c != null){
			Datetime dt = datetime.newInstance(quoteObj.Project_Due_Date__c.year(), quoteObj.Project_Due_Date__c.month(),quoteObj.Project_Due_Date__c.day());
			quoteDate = dt.format('MM/dd/yyyy');
		}else{
			quoteDate='';
		}
		certificationType = '';
		quoteStatus = '';
		//numberOfFilesUploaded = '';		
		dateStr = '';
		addEmail = '';
	}
	
	/* 
    @Description: Method to get Quote, User and Account record.
    @Params: None
    @Return Type: Quote__c
    */
	private Quote__c getQuoteRecord(){
		Quote__c quote = [SELECT Id, Project_Due_Date__c, Certification_Type__c, Additional_Email__c, Expedited__c, Notes__c, Notarized__c,
							Status__c, Client__c, Quote_Number__c, Bucket_Name__c, Client__r.Name, Project_Name_Community__c, Contact__c, 
							Project_Number__c, Urgency__c, Quote_Due_Date__c, Client__r.Direct_Order_Allowed__c, SubmittedBy__c
							FROM Quote__c
							WHERE Id =: qId LIMIT 1];
		if(quote != null){
			projectName = quote.Project_Name_Community__c;
		}
		
		if(contactId == null){
			currentUser = [SELECT Contact.AccountId, Name, Id 
							FROM User 
							WHERE id =: userInfo.getUserId() LIMIT 1];
		}
		else{
			currentUser = [SELECT Contact.AccountId, Name, Id 
							FROM User 
							WHERE ContactId =: contactId LIMIT 1];
		}
		
		if(currentUser.Contact.AccountId != null){
			acc = [SELECT Id, Owner_name__c, Name 
					FROM Account 
					WHERE Id =: currentUser.Contact.AccountId LIMIT 1 /*Id =: quote.Client__c*/];
		}
		if(acc != null){
			accName = acc.Name + ' Certificate';
		}
		return quote;
	}
	
	/* 
    @Description: Method called when "Place Order" or "Request A Quote" button is clicked.
    @Params: None
    @Return Type: None
    */    
    public void sendEmailOnPlacingOrder(){
    	
	    if(quoteObj != null){
	    	try{
		    	
		    	/* Send uploaded source files for GEF API Call */
		    		if(quoteObj.Client__c != null && quoteObj.Quote_Number__c != '' && quoteObj.Bucket_Name__c != ''){
    		    		string quoteId =  quoteObj.Id;
    		    		String transactionId = '';
    		    		if(quoteId.length()!=15)
                  			quoteId = quoteId.subString(0,15);
    		    		string qutoeNumber = quoteObj.Quote_Number__c+'_' + quoteId; 
    		    		try {
    		    			transactionId = GLSQLICreator.gls_callGetExternalFileWS(quoteId, quoteObj.Client__r.Name, qutoeNumber, 
    		    																quoteObj.Bucket_Name__c, quoteObj.Client__c, 'External');
    		    		}catch(Exception e){
		    				system.debug('----Exception in GEF API Call ---: '+e);
		    			}
		    		}
    		    
    		    /* GEF API Call code ends here */
		    	
		    	//Update Quote with additional information
		    	
		    	if(certificationType != ''){
		    		quoteObj.Certification_Type__c = certificationType;
		    	}
		    	if(quoteStatus == 'requestQuote'){
		    		quoteObj.Status__c = GLSconfig.NewQuote;
		    	}
		    	else if(quoteStatus == 'placeOrder'){
		    		quoteObj.Status__c = GLSconfig.Direct_Order;
		    	}
		    	else{
		    	    quoteObj.Status__c = GLSconfig.DraftQuote;
		    	}
		    	if(quoteObj.Expedited__c){
		    		quoteObj.Urgency__c = GLSconfig.expeditedDelivery;
		    	}
		    	else{
		    		quoteObj.Urgency__c = GLSconfig.standardDelivery;
		    	}
		    	if(dateStr != ''){
		    		quoteObj.Project_Due_Date__c = Date.parse(dateStr);
		    	}else{
		    		quoteObj.Project_Due_Date__c = null;
		    	}
		    	quoteObj.Additional_Email__c = addEmail;
		    	quoteObj.SubmittedBy__c = currentUser.Id;
		    	
		    	update quoteObj;
		    	
		    	if(quoteStatus == 'requestQuote' || quoteStatus == 'placeOrder'){
    		    	//Send email for the confirmation of request
    		    	List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
    		    	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    		    	String qNumber = '';
    		    	String emailBody = ''; 
    		    	List<String> ccAddresses = new List<String>();
    		    	
    		    	Contact con = new Contact();
    		    	if(quoteObj.Contact__c != null){
    			    	con = [SELECT Id , Name, Contactowneremail__c, Owner.Name, Email 
    	    					FROM Contact 
    	    					WHERE Id =: quoteObj.Contact__c];
    		    	}
    		    	if(con != null){
    		    		ccAddresses.add(con.Contactowneremail__c);
    		    	}
    		    	
    		    	//Get the sender's email address
    		    	OrgWideEmailAddress orgWideEmail = [SELECT DisplayName, Id
    		    										FROM OrgWideEmailAddress
    		    										WHERE DisplayName =: 'GLS Portal'];
    		    	
    		    	List<Quote_Assignment__c> lstQuoteAssg = [SELECT Id, Name, AssignmentNumber1__c, Source_Language__c, Source_Language__r.Name, 
    		    												Quote__r.Quote_Number__c, Quote__r.Project_Number__c, Quote__c, 
    		    												(SELECT service_name__c, Client_Service__r.Name FROM Quote_Assignment_Services__r), 
    		    												(SELECT Language_List_Item__r.Name FROM Quote_Assignment_Languages__r), 
    		    												(SELECT Id FROM Quote_Assignment_Files__r WHERE File_Usage__c != 'Source') 
    		                                        			FROM Quote_Assignment__c 
    		                                        			WHERE Quote__c =: quoteObj.Id];
    		    	
    		    	if(quoteStatus == 'placeOrder'){
    		    		emailBody = '<img src="https://c.'+ system.label.GLS_Community_Instance+'.content.force.com/servlet/servlet.ImageServer?id='+system.label.GLS_Community_DocumentId+'&oid='+ system.label.GLS_Community_OrgId +'" /> <br> <hr> <br><br> <div style="font-family:Tahoma; font-size:13px; color:#4F5152">Dear ' + currentUser.Name + ',<br><br>' +
    		                           'Thank you very much for your Project Request: <font color="blue">'  + quoteObj.Project_Name_Community__c +  '</font> submitted on ' + System.Now() + '. <br><br>' +
    		                            '<table border="1" style="border-collapse:collapse; border:1px solid #1D385F; color:#1D385F"> <tr  style="background-color:#1D385F; color:#FFFFFF; font-family:Tahoma; height:40px; font-size:12px;"> <th  style="width :20em;text-align:left">' + quoteObj.Project_Name_Community__c.trim() + '</th> <th  style="width :12em;text-align:center"> # of Files </th> <th  style="width :30em;text-align:left"> Target Languages </th> <th  style="width :30em;text-align:left"> Services </th> </tr>';
    		    		//if(quoteObj.Project_Number__c != null){
    		    		if(quoteObj.Quote_Number__c != null){
    		    			qNumber = 'O-'+quoteObj.Quote_Number__c;
    		    		}
    		    		else{
    		    			qNumber = '';
    		    		}
    		    	}
    		    	else if(quoteStatus == 'requestQuote'){
    		    		emailBody = '<img src="https://c.'+ system.label.GLS_Community_Instance+'.content.force.com/servlet/servlet.ImageServer?id='+system.label.GLS_Community_DocumentId+'&oid='+ system.label.GLS_Community_OrgId +'" /> <br> <hr> <br><br> <div style="font-family:Tahoma; font-size:13px; color:#4F5152">Dear ' + currentUser.Name + ',<br><br>' +
    		                           'Your request has been received!  The progress of your request can be tracked by visiting the Quotes tab in the Portal; otherwise, an email will be sent to alert you when your quote is ready for review. <br><br>' +
    		                            '<table border="1" style="border-collapse:collapse; border:1px solid #1D385F; color:#1D385F"> <tr  style="background-color:#1D385F; color:#FFFFFF; font-family:Tahoma; height:40px; font-size:12px; "> <th  style="width :20em;text-align:left">' + quoteObj.Project_Name_Community__c.trim() + '</th> <th  style="width :12em;text-align:center"> # of Files </th> <th  style="width :30em;text-align:left"> Target Languages </th> <th  style="width :30em;text-align:left"> Services </th> </tr>';
    		    		if(quoteObj.Quote_Number__c != null){
    		    			qNumber = quoteObj.Quote_Number__c;
    		    		}
    		    		else{
    		    			qNumber = '';
    		    		}
    		    	}
    		    	
    		    	/*
    		    	if(quoteObj.Client__c != null && quoteObj.Quote_Number__c != '' && quoteObj.Bucket_Name__c != ''){
    		    		string quoteId =  quoteObj.Id;
    		    		if(quoteId.length()!=15)
                  			quoteId = quoteId.subString(0,15);
    		    		string qutoeNumber = quoteObj.Quote_Number__c+'_' + quoteId; 
    		    		transactionId = GLSQLICreator.gls_callGetExternalFileWS(quoteId, quoteObj.Client__r.Name, qutoeNumber, 
    		    																quoteObj.Bucket_Name__c, quoteObj.Client__c, 'External');
    		    	}*/
    		    	
    		    	if(lstQuoteAssg != null && lstQuoteAssg.size() > 0){
    			    	for(Quote_Assignment__c quoteAssn : lstQuoteAssg){
    			            String serviceNames = '';
    			            for(Quote_Assignment_Service__c cs : quoteAssn.Quote_Assignment_Services__r){
    			                serviceNames = serviceNames + cs.Client_Service__r.Name + '<br>';
    			            }
    			            String targetLangNames = '';
    			            for(Quote_Assignment_Language__c qal : quoteAssn.Quote_Assignment_Languages__r){
    			                targetLangNames = targetLangNames + qal.Language_List_Item__r.Name + '<br>';
    			            }
    			            /*Integer numberOfFilesUploaded = 0;
    			            for(Quote_Assignment_Files__c qaf : quoteAssn.Quote_Assignment_Files__r){
    			            	numberOfFilesUploaded = numberOfFilesUploaded + 1;
    			            }*/
    			            Integer numberOfFilesUploaded = quoteAssn.Quote_Assignment_Files__r.size();
    			            emailBody = emailBody + '<tr style="font-family:Tahoma;font-size:12px; color:#4F5152" ><td style="width :20em;text-align:left">' + qNumber + ' [' + quoteAssn.Name + '] ' + '</td> <td style="width :12em;text-align:center">' + numberOfFilesUploaded + '</td> <td  style="width :30em;text-align:left">' + targetLangNames + '</td> <td  style="width :30em;text-align:left">' + serviceNames + '</td> </tr>';
    			        }
    		    	}
    		    	
    		        if(quoteStatus == 'placeOrder'){
    					emailBody = emailBody + '</pre></table> <br>' +
    		                        'You are all done. Our linguists will be hard at work to translate all your materials.   A dedicated Project manager will contact you shortly to go over your projects specifics, as needed.<br><br>' +
    		                        'If you have any questions, please contact  <font color="blue"> ' + acc.Owner_name__c + '</font>, your GLS Account Manager. <br>' +
    		                        '<br><br>The GLS Portal Team</div>';
    		        }
    		        else if(quoteStatus == 'requestQuote'){
    		            emailBody = emailBody + '</pre></table>' +
    		                                    'Submission Date/Time: ' + System.Now() + '<br>' +
    		                                    'Portal ID: ' + qNumber + '<br><br><br><br>' +
    		                                    'As always, feel free to contact us with any questions or concerns. <br><br>' +
    		                                    'GLS Business Contact: ' + con.Owner.Name + ' [' + con.Contactowneremail__c + ']<br>' +
    		                                    'GLS Technical Contact: Portal@globallanguages.com <br><br><br>' +
    		                                    'Thank you!' +
    		                                    '<br><br>GLS Portal Team</div>';
    		        }
    		        
    		        List<String> sendTo = new List<String>();
    		        if(con != null && con.email != null){
    		        	sendTo.add(con.email);
    		        }
    		        
    		        if(quoteObj.Additional_Email__c != '' && quoteObj.Additional_Email__c != null){
    		        	List<String> ccAdd = quoteObj.Additional_Email__c.split(';');
    		        	for(String str : ccAdd){
    		        		ccAddresses.add(str);
    		        	}
    		        }
    		        
    		        mail.setHtmlBody(emailBody);
    		        mail.setReplyTo('portal@globallanguages.com');
    		        if(orgWideEmail != null ) {
    		            mail.setOrgWideEmailAddressId(orgWideEmail.Id);
    		        }
    		        mail.setSubject('GLS Confirmation of Request ' + quoteObj.Project_Name_Community__c + '/' + qNumber);
    		        if(sendTo != null && sendTo.size() > 0){
    		        	mail.setToAddresses(sendTo);
    		        }
    		        if(ccAddresses != null && ccAddresses.size() > 0){
    		        	mail.setCcAddresses(ccAddresses);
    		        }
    		        mail.saveAsActivity = false;
    		        mailList.add(mail);
    		        
    		        if(mailList.size() > 0){
    		            List<Messaging.SendEmailResult> results = new List<Messaging.SendEmailResult>();
    		            results = Messaging.sendEmail(mailList); 
    		            system.debug('---results----'+results);
    		        }
	    	    }
	    	}
	    	catch(DMLException de){
	            system.debug(GLSConfig.DMLExceptionOccured + de);
	        }
	        catch(Exception e){
	            system.debug(GLSConfig.ExceptionOccured + e);
	        }
	    }
    }
    
    /* 
    @Description: Method to redirect to Quotes/Order/Draft Page
    @Params: None
    @Return Type: PageReference
    */
    public PageReference redirectToQuoteOrderPage(){
    	PageReference pageref;
    	if(quoteStatus == 'requestQuote'){
    		pageref = Page.GLSCommunity_QuotesPage;
    	}
    	else if(quoteStatus == 'placeOrder'){ 
    		pageref = Page.GLSCommunity_OrdersPage;
    	}
    	else{
    	    pageref = Page.GLSCommunity_DraftPage;
    	}
		if(contactId != null){
			pageref.getParameters().put('conId',contactId);
		}
		pageref.setRedirect(true);
		return pageref;			
	}
}