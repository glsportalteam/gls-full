public with sharing class GLSLPW_InvoiceTriggerHandler {
	 public static void updateQuote(set<String> setInvoiceId){
       List<Quote__c> lstQuote = new List<Quote__c>();
       List<Quote__c> lstQuoteUpdate = new List<Quote__c>();
       if(setInvoiceId != null && setInvoiceId.size()>0)
       {
       		lstQuote = [Select Id,Status__c From Quote__c Where Invoice__c in: setInvoiceId];
        }
        for(Quote__c quote:lstQuote){
        	quote.Status__c = GLSConfig.BillableStatus;
            lstQuoteUpdate.add(quote);
        }   
        if(lstQuoteUpdate != null && lstQuoteUpdate.size()>0){
                try{
                   update lstQuoteUpdate;
                }catch(Exception de){
                    system.debug(GLSConfig.Exceptionoccured + de);
                }
        }
    }
}