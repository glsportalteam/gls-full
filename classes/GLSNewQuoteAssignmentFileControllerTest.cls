/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 */
 
@isTest
private class GLSNewQuoteAssignmentFileControllerTest { 

      /**
       * This Unit Test method will test the constructor 
      **/
    static testMethod void myUnitTest(){  
           GLSSetupData.setupData();
           test.startTest();
           ApexPages.currentPage().getParameters().put('retURL','/'+GLSSetupData.qAsst.id);
           ApexPages.currentPage().getParameters().put('retURL','/customer/'+GLSSetupData.qAsst.id);
           ApexPages.currentPage().getParameters().put('id',GLSSetupData.qAsstFile1.id);
           GLSNewQuoteAssignmentFileController customCont = new GLSNewQuoteAssignmentFileController(new ApexPages.StandardController(GLSSetupData.qAsstFile1));
           System.assert(customCont.quoteAssignment !=null);
           test.stopTest();
        }
     /**
       * This Unit Test method will test save function
      **/   
    static testMethod void customSaveTest() {
        GLSSetupData.setupData();
        PageReference pg;
        Test.startTest();
        ApexPages.currentPage().getParameters().put('retURL','/'+GLSSetupData.qAsst.id);
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.qAsstFile1.id);
        GLSNewQuoteAssignmentFileController customCont = new GLSNewQuoteAssignmentFileController(new ApexPages.StandardController(GLSSetupData.qAsstFile1));
         pg=customCont.customSave();
         system.assertEquals(pg.getUrl(),'/'+GLSSetupData.qAsst.id);
        Test.stopTest();
    }
     /**
       * This Unit Test method will test save and new function
      **/
    static testMethod void saveAndNewTest() {
        GLSSetupData.setupData();
        PageReference pg;
        Test.startTest();
        ApexPages.currentPage().getParameters().put('retURL','/'+GLSSetupData.qAsst.id);
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.qAsstFile1.id);
        GLSNewQuoteAssignmentFileController customCont = new GLSNewQuoteAssignmentFileController(new ApexPages.StandardController(GLSSetupData.qAsstFile1));
        pg=customCont.saveAndNew();
        system.assertEquals(pg.getUrl(),'/apex/GLSNewQuoteAssignmentFilePage?retURL=%2F'+GLSSetupData.qAsst.id);
        Test.stopTest();
    }
     /**
       * This Unit Test method will test edit function
      **/
    static testMethod void customEditTest() {
        GLSSetupData.setupData();
        Test.startTest();
        ApexPages.currentPage().getParameters().put('retURL','/'+GLSSetupData.qAsst.id);
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.qAsstFile1.id);
        GLSNewQuoteAssignmentFileController customCont = new GLSNewQuoteAssignmentFileController(new ApexPages.StandardController(GLSSetupData.qAsstFile1));
        customCont.customEdit();
        system.assertEquals(customCont.isReadOnly,false);
        Test.stopTest();
    }
     /**
       * This Unit Test method will test clone function
      **/
    static testMethod void cloneQuoteAssignmentFileTest() {
        GLSSetupData.setupData();
        PageReference pg;
        Test.startTest();
        ApexPages.currentPage().getParameters().put('retURL','/'+GLSSetupData.qAsst.id);
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.qAsstFile1.id);
        GLSNewQuoteAssignmentFileController customCont = new GLSNewQuoteAssignmentFileController(new ApexPages.StandardController(GLSSetupData.qAsstFile1));
        pg=customCont.cloneQuoteAssignmentFile();
        system.assertEquals(pg,null);
        Test.stopTest();
    }
     /**
       * This Unit Test method will test delete function
      **/
    static testMethod void customDeleteTest() {
        GLSSetupData.setupData();
        PageReference pg;
        Test.startTest();
        ApexPages.currentPage().getParameters().put('retURL','/'+GLSSetupData.qAsst.id);
        ApexPages.currentPage().getParameters().put('id',GLSSetupData.qAsstFile1.id);
        GLSNewQuoteAssignmentFileController customCont = new GLSNewQuoteAssignmentFileController(new ApexPages.StandardController(GLSSetupData.qAsstFile1));
        pg=customCont.customDelete();
        system.assertEquals(pg.getUrl(),'/'+GLSSetupData.qAsst.id);
        Test.stopTest();
    }
}