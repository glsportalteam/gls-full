global class GLSQW_CloneFileController {


    public static Boolean skipTrigger = false;
    global final String query;
    public static AWSKeys credentials;
    private boolean isFlag = false;
    public static S3.AmazonS3 as3 { get; private set; }
    public static String AWSCredentialName = 'NAME OF KEY TO USE'; //Modify this string variable to be the name of the AWS Credential record that contains the proper AWS keys and secret
    public string secret { get {return credentials.secret;} }
    public string key { get {return credentials.key;} }
    
       
    public static void getawss3LoginCredentials(){
        try{

            credentials = new AWSKeys(AWSCredentialName);
            as3 = new S3.AmazonS3(credentials.key,credentials.secret);
            system.debug('-----as3----: '+as3);
        }catch(AWSKeys.AWSKeysException AWSEx){
            System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
        } 
    }
    
    WebService static void copyEstimateFiles(List<Estimate_Files__c> LstNewEstFiles, List<Estimate_Files__c> LstOldEstFiles){
    	try{
			getawss3LoginCredentials();					
			Map<String,String> MapEstFileNamePath = new Map<String,String>();
			for(Estimate_Files__c estFile : LstOldEstFiles){						
				MapEstFileNamePath.put(estFile.Estimate_File_Name__c,estFile.AWSFilePath__c);
			}
			List<String> LstEstFileId = new List<String>(); 
			for(Estimate_Files__c Estfile : LstNewEstFiles){
				LstEstFileId.add(Estfile.id);
			}
			
			List<Estimate_Files__c> EstimateFiles;
			if(LstEstFileId != null && LstEstFileId.size() > 0) 
				EstimateFiles = [select id,AWSFilePath__c,Estimate_File_Name__c from Estimate_Files__c where Id IN:LstEstFileId];
			
			Map<String,AWS_File__c>	MapAWSNameFile = new Map<String,AWS_File__c>(); 
			if(EstimateFiles != null && EstimateFiles.size()>0){
				List<AWS_File__c> awsFileList = new List<AWS_File__c>();			 
				for(Estimate_Files__c estFile : EstimateFiles){
					String SourcePath = MapEstFileNamePath.get(estFile.Estimate_File_Name__c)+estFile.Estimate_File_Name__c;								
					Datetime now = Datetime.now();
					if(estFile.AWSFilePath__c!=null && estFile.AWSFilePath__c!=''){
						String DestinationPath = estFile.AWSFilePath__c + estFile.Estimate_File_Name__c;
						S3.CopyObjectResult copyObjectResult= as3.CopyObject(glsConfig.bucketName,SourcePath,glsConfig.bucketName,DestinationPath,'COPY',null,as3.key,now,as3.signature('CopyObject',now), as3.secret);
						if(copyObjectResult!=null){
							AWS_File__c objRecord = new AWS_File__c();                                                                                                    
							objRecord.File_Name__c = estFile.Estimate_File_Name__c;                    
	                		objRecord.Bucket_Name__c = GLSConfig.bucketName;                                                                                                                                  			                                              
	                		objRecord.Amazon_S3_source_file_path__c = estFile.AWSFilePath__c;
	                		objRecord.Record_Type__c = GLSConfig.EstimateFile;                		                                                                        		                                                                                                         
	                		objRecord.Version__c = 1;                		    
	                		awsFileList.add(objRecord);            		                
						}					
					}				
				}
				insert awsFileList;
				if(awsFileList != null && awsFileList.size() > 0){
					for(AWS_File__c awsFile : awsFileList){
						MapAWSNameFile.put(awsFile.File_Name__c,awsFile);
					}
				}
				for(Integer i = 0;i<EstimateFiles.size();i++){
					if(MapAWSNameFile.get(EstimateFiles[i].Estimate_File_Name__c) != null)
						EstimateFiles[i].AWS_File_Id__c = MapAWSNameFile.get(EstimateFiles[i].Estimate_File_Name__c).id;
				}
				update EstimateFiles;			
			}    	    	
    	}catch(DMLException ex){
    		system.debug(ex.getMessage()+'-----------------');
    	}catch(Exception e){
    		system.debug(e.getMessage()+'-----------exception----------------');
    	}	
    }  
    /*Change file type on amazon*/
    WebService static void changeFileType(string newPath, string oldPath){
    	try{
			getawss3LoginCredentials();			
			Datetime now = Datetime.now();		 
			S3.CopyObjectResult copyObjectResult= as3.CopyObject(glsConfig.bucketName,oldPath,glsConfig.bucketName,newPath,'COPY',null,as3.key,now,as3.signature('CopyObject',now), as3.secret);
    	}catch(DMLException ex){
    		system.debug(ex.getMessage()+'-----------------');
    	}catch(Exception e){
    		system.debug(e.getMessage()+'-----------exception----------------');
    	}	
    }
    /*
    	@Param: 
    	LstProgrmFiles: contains list of all program files into which file needs to be uploaded
    */
    WebService static void copyProgramFiles(List<Program_Files__c> LstProgrmFiles, List<Estimate_Files__c> LstEstFiles,String prgmFilePath){
    	try{
		getawss3LoginCredentials();					
		Map<String,String> MapEstFileNamePath = new Map<String,String>();
		for(Estimate_Files__c estFile : LstEstFiles){						
			MapEstFileNamePath.put(estFile.Estimate_File_Name__c,estFile.AWSFilePath__c);
		}
		List<String> LstPrgmFileId = new List<String>(); 
		for(Program_Files__c pf:LstProgrmFiles){
			LstPrgmFileId.add(pf.id);
		}		
		List<Program_Files__c> ProgramFiles;
		if(LstPrgmFileId!=null && LstPrgmFileId.size()>0) 
			ProgramFiles = [select id,AWSFilePath__c,Program_File_Name__c from Program_Files__c where Id IN:LstPrgmFileId];
		
		
		//List<AWS_File__c> lstAWSFiles =[SELECT Id, Version__c FROM AWS_File__c WHERE Amazon_S3_source_file_path__c =:amazonPath and File_Name__c IN=:];
		
		Map<String,AWS_File__c>	MapAWSNameFile = new Map<String,AWS_File__c>(); 
		if(ProgramFiles != null && ProgramFiles.size()>0){
			List<AWS_File__c> awsFileList = new List<AWS_File__c>();			 
			for(Program_Files__c prmFile : ProgramFiles){
				String SourcePath = MapEstFileNamePath.get(prmFile.Program_File_Name__c)+prmFile.Program_File_Name__c;								
				Datetime now = Datetime.now();
				if(prmFile.AWSFilePath__c!=null && prmFile.AWSFilePath__c!=''){
					String DestinationPath =prmFile.AWSFilePath__c+prmFile.Program_File_Name__c;
					S3.CopyObjectResult copyObjectResult = as3.CopyObject(glsConfig.bucketName,SourcePath,glsConfig.bucketName,DestinationPath,'COPY',null,as3.key,now,as3.signature('CopyObject',now), as3.secret);
					system.debug(copyObjectResult+'copyObjectResult------------------------');
					if(copyObjectResult != null){
						AWS_File__c objRecord = new AWS_File__c();                                                                                                    
						objRecord.File_Name__c = prmFile.Program_File_Name__c;                    
                		objRecord.Bucket_Name__c = GLSConfig.bucketName;                                                                                                                                  			                                              
                		objRecord.Amazon_S3_source_file_path__c = prmFile.AWSFilePath__c;
                		objRecord.Record_Type__c = GLSConfig.programRefFile;                		                                                                        		                                                                                                         
                		objRecord.Version__c = 1;                		    
                		awsFileList.add(objRecord);            		                
					}					
				}				
			}
			insert awsFileList;
			if(awsFileList != null && awsFileList.size() > 0){
				for(AWS_File__c awsFile : awsFileList){
					MapAWSNameFile.put(awsFile.File_Name__c,awsFile);
				}
			}
			for(Integer i=0;i<ProgramFiles.size();i++){
				if(MapAWSNameFile.get(ProgramFiles[i].Program_File_Name__c)!=null)
					ProgramFiles[i].AWS_File_Id__c = MapAWSNameFile.get(ProgramFiles[i].Program_File_Name__c).id;
			}
			update ProgramFiles;			
		}
    	}catch(DMLException ex){
    		system.debug(ex.getMessage()+'-----------------');
    	}catch(Exception e){
    		system.debug(e.getMessage()+'-----------exception----------------');
    	}
	}
    WebService static String cloneFileMethod(String masterQuoteId,String currentQuoteId){
        system.debug('----inside GLSQW_CloneFileController--- '+masterQuoteId+'---' +currentQuoteId);
        List<Quotes_File__c> newQuoteFile=new List<Quotes_File__c>();
        List<Quotes_File__c> lstCurrQFiles=new List<Quotes_File__c>();
        List<Quotes_File__c> lstParentQFiles=new List<Quotes_File__c>();
        list<Quote__c>  lstMasterQuote = new list<Quote__c>();
        List<Quote__c> lstQuote = new List<Quote__c>();
        List<Quote_Assignment_File__c> lstQAF = new List<Quote_Assignment_File__c>();
        List<Quote_Assignment_Service__c> lstQASParent = new List<Quote_Assignment_Service__c>();
        List<Quote_Assignment_Language__c> lstQALParent = new List<Quote_Assignment_Language__c>();
        List<Quote_Assignment_Special_Provisions__c> lstQASPParent = new List<Quote_Assignment_Special_Provisions__c>();
        List<Quote_Line_Item__c> lstQLIParent = new List<Quote_Line_Item__c>();
        List<Quote__c> lstQupdate = new List<Quote__c>();
        Boolean uploadAllFileSuccess = false;
        Boolean isFileClonedComplete = false; 
        Integer counter;
        List<String> lstFId = new List<String>();
        Set<String> setFId = new Set<String>();
        Map<String,String> oldfilesSet =new Map<String,String>();
        map<String,string> newfilesSet =new Map<String,String>();       
        try{
            getawss3LoginCredentials(); 
            Datetime now = Datetime.now(); 
            
            if(currentQuoteId !=null){
                    lstQuote = [Select Id, Name, Quote_Number__c, Client__r.Name, isFileClonedComplete__c From Quote__c where Id =: currentQuoteId];
                    lstCurrQFiles = [Select Id, Quote__c, File_Name__c From Quotes_File__c Where Quote__c =: currentQuoteId];
                    for(Quotes_File__c Qfile:lstCurrQFiles){
                        newfilesSet.put(QFile.File_Name__c,Qfile.id); 
                    }   
            }
            if(masterQuoteId !=null){
               lstMasterQuote = [Select id,Client__r.Name,Clone_From__c,
                                                     (Select File_Name__c,Link_To_File__c ,File_Format__c,Amazon_S3_source_file_path__c,version_id__c From Quotes_Files__r),
                                                     (Select Id,Name,Document_Type__c,Additional_Note__c,AssignmentNumber1__c, Delivery_File_Format__c, Quote__c, Special_Instructions__c, Countries__c, 
                                                     Source_Language__c, Allowed_File_Formats__c, Quote_Task_Description__c, Quote_Reference_Files_Instruction__c, 
                                                     Delivery_Format__c, isAssignmentUpdated__c,Keywords__c, Step_one_complete__c, Step_two_complete__c, isFileAssociated__c, Translation_Update__c, Previous_GLS_Project_Number__c From Quote_Assignments__r)                   
                                                  From Quote__c 
                                                  Where Id =:masterQuoteId];
                   
                    for(Quote__c quote:lstMasterQuote){
                      if(quote.Quotes_Files__r.size()>0){                                  
                          for(Quotes_File__c qPF: quote.Quotes_Files__r){
                                oldfilesSet.put(qPF.File_Name__c,qPF.id);
                          }
                      }
                    }
                                                        
            }
            
            if(lstMasterQuote.size()>0 && lstMasterQuote !=null){

                for(String str:oldfilesSet.keySet()){
                    if(!newfilesSet.containsKey(str)){
                        setFId.add(oldfilesSet.get(str));
                    }   
                }   
                
                system.debug('---setFId---  '+setFId);
                if(setFId != null && setFId.size()>0){
                    lstFId.addAll(setFId);
                    lstParentQFiles = [Select Id, File_Name__c,File_Format__c,Amazon_S3_source_file_path__c, AWSFilePath__c,version_id__c, Bucket_Name__c, Link_To_File__c, File_Description__c,File_Type__c, File_Size__c From Quotes_File__c Where Id in :(lstFId)];
                }
                system.debug('---lstParentQFiles---  '+lstParentQFiles.size()+'--'+lstParentQFiles);    
                if(lstParentQFiles!= null && lstParentQFiles.size()>0){ 
                     counter =0;
                     string folderName = GLSConfig.SourceFT;
                     Quotes_File__c qFile;
                     for(Quotes_File__c qf: lstParentQFiles){
                        if(counter <= 99){    
                            qFile= new Quotes_File__c();
                            qFile.Quote__c=currentQuoteId;
                            if(String.isEmpty(qf.Link_To_File__c)){
                                if(qf.File_Type__c == GLSConfig.SourceFT)
                                	folderName = GLSConfig.SourceFT;
                                else if(qf.File_Type__c == GLSConfig.ReferenceFolder)
                                	folderName = GLSConfig.ReferenceFolder;
                                else if(qf.File_Type__c == GLSConfig.PreviousTFT)
                                	folderName = GLSConfig.PreviousTFT; 
                                
                                qFile.Amazon_S3_source_file_path__c=lstQuote[0].Client__r.Name+'/'+lstQuote[0].Quote_Number__c+'_'+currentQuoteId.substring(0,15)+'/'+folderName+'/'+qf.File_Name__c;
                                system.debug('=====path'+qFile.Amazon_S3_source_file_path__c);
                            }
                            qFile.File_Format__c=qf.File_Format__c;
                            qFile.File_Name__c=qf.File_Name__c;
                            qFile.Link_To_File__c=qf.Link_To_File__c;
                            qFile.File_Type__c = qf.File_Type__c;
                            qFile.File_Description__c=qf.File_Description__c;
                            qFile.File_Size__c = qf.File_Size__c;
                            
                            system.debug('---Source Key ---: '+qf.Amazon_S3_source_file_path__c );
                            system.debug('---Destination Key ---: '+qFile.Amazon_S3_source_file_path__c);
                            
                            
                            try{
                                if(!test.isRunningTest()){
                                    //S3.CopyObjectResult copyObjectResult= as3.CopyObject(glsConfig.bucketName,qf.AWSFilePath__c+qf.File_Name__c,glsConfig.bucketName,qFile.Amazon_S3_source_file_path__c,'COPY',null,as3.key,now,as3.signature('CopyObject',now), as3.secret);
                                    S3.CopyObjectResult copyObjectResult= as3.CopyObject(glsConfig.bucketName,qf.Amazon_S3_source_file_path__c,glsConfig.bucketName,qFile.Amazon_S3_source_file_path__c,'COPY',null,as3.key,now,as3.signature('CopyObject',now), as3.secret);
                                }
                                newQuoteFile.add(qFile);    
                            }catch(Exception e){
                                system.debug(GLSConfig.ExceptionOccured);
                            }
                            if(!String.isEmpty(qf.Link_To_File__c)){
                                newQuoteFile.add(qFile); 
                            }
                           counter++; 
                        }else{
                            break;
                        }
                    system.debug('----counter----'+counter);
                    if(counter == lstParentQFiles.size()){      
                        Quote__c qObj = new Quote__c(Id=currentQuoteId);            
                        qObj.isFileClonedComplete__c = true;
                        isFileClonedComplete = true;
                        lstQupdate.add(qObj);
                    }
                 }
               if(lstQupdate != null && lstQupdate.size()>0){
                    Database.update(lstQupdate);
               }
               system.debug('----newQuoteFile----'+newQuoteFile.size()+'--- ' +newQuoteFile);
               if(newQuoteFile != null && newQuoteFile.size()>0){
                    try{
                        Database.insert(newQuoteFile); 
                        uploadAllFileSuccess = true; 
                        Quote__c qObj= new Quote__c(id=currentQuoteId); 
                        qObj.Clone_From__c='';
                        if(counter == lstParentQFiles.size())
                            qObj.isFilesCloned__c = 'Success';
                        update qObj; 
                    }catch(DMLException de){
                        system.debug(GLSConfig.DMLExceptionOccured);
                    }                                                  
               }else{
                    try{
                        Quote__c qObj= new Quote__c(id=currentQuoteId);
                        qObj.isFilesCloned__c = 'Error';
                        update qObj;
                    }catch(DMLException de){
                        system.debug(GLSConfig.DMLExceptionOccured);
                    }
               }
            }
               List<Quote_Assignment__c> lstQA = new List<Quote_Assignment__c>();
               Quote_Assignment__c quoteAss ;
               List<Quotes_File__c> lstQuotesFile = new List<Quotes_File__c>();
               
               if(isFileClonedComplete){
                   if(uploadAllFileSuccess){
                    
                    lstQAF        =     [Select Quote_Files__c,Quote_Files__r.File_Name__c, Quote_Assignment__c, Name, Id,File_Usage__c 
                                            From Quote_Assignment_File__c 
                                            Where Quote_Assignment__r.Quote__c =: masterQuoteId];
                
                    lstQASParent  =     [Select Service_Name__c, Quote_Assignment__r.Quote__c, Quote_Assignment__c, Name, Id, Client_Service__c 
                                            From Quote_Assignment_Service__c 
                                            Where Quote_Assignment__r.Quote__c =: masterQuoteId]; 
                
                    lstQALParent  =     [Select Quote_Assignment__r.Quote__c, Quote_Assignment__c, Name, Language_Name__c, Language_List_Item__c, Id 
                                            From Quote_Assignment_Language__c 
                                            Where Quote_Assignment__r.Quote__c =: masterQuoteId];
                
                    lstQASPParent =     [Select Standard_Special_Provisions__c, Quote_Assignment_Special_Provision_Text__c, Quote_Assignment_Special_Provision_Name__c, 
                                                Quote_Assignment_Id__c, Name, Id,Quote_Assignment_Id__r.Quote__c 
                                            From Quote_Assignment_Special_Provisions__c 
                                            Where Quote_Assignment_Id__r.Quote__c =: masterQuoteId];          
                
                    lstQLIParent  =     [Select isTMFound__c, isReadyForAnalyse__c, isProcessed__c, isAnalyzed__c, Unit_of_Measure__c, 
                                                Translation_Memory_Options_Flag__c, Trados_Language_Pair__c, Task_Status__c, Target_Language_ID__c, 
                                                TM_Export_Zip_File__c, TM_Export_Transaction_ID__c, TM_Export_File_ID__c, Source_Language_ID__c, 
                                                Service_Words__c, QuoteID__c, Line_Item_Duration__c, File_Prep_Description__c, File_Prep_Amount__c, 
                                                Extra_Days__c, Error_Message__c, Client_TM__c, Analyzed_Version_ID__c, Analyzed_Transaction_ID__c, 
                                                Analyzed_File_URL__c,Min_Charge_Applied__c,XML_Data__c,Quote_Line_Item_Id__c, X95_99_Word_Count__c,
                                                X85_95_Word_Count__c,X75_84_Word_Count__c, X50_74_Word_Count__c, X100_Word_Count__c, Repetitions_Word_Count__c, 
                                                Perfect_Match__c, No_Match_Word_Count__c, Crossfile_Repetitions__c, Back_Translation_Total_Words__c,
                                                Actual_Words__c,TWC__c,Context_TM_Word_Count__c, 
                                                (Select Service_Id__c, isProcessed__c, Quote_Line_Item_Id__c From Quote_Line_Services__r), 
                                                (Select Quote_Line_Item__c, Source_File_name__c, Source_File_name__r.File_Name__c, IsAnalysisProcessed__c, Special_Instructions__c From Quote_Line_Object_Assignment_Files__r),
                                                (Select Quote_Line_Item_Id__c, Client_Service__c, Unit_of_Measure__c, Quantity__c, Description__c, 
                                                        Unit_Price__c, Show_Units__c, Show_Unit_of_Measure__c, Show_Description__c, Show_Unit_Price__c, 
                                                        Show_Subline_Total__c, Manual_Entry__c, Default_Subline_Order__c, Default_Order__c, Unit_Price_Std__c 
                                                    From Quote_Line_Item_Sublines__r) 
                                            From Quote_Line_Item__c 
                                            Where QuoteID__c =: masterQuoteId];
                    
                   lstQuotesFile =      [Select Quote__c, Quote_File_Id__c, File_Name__c, MapKey__c, File_Type__c, File_Format__c 
                                         From Quotes_File__c Where Quote__c =: currentQuoteId];
                    for(Quote__c quote : lstMasterQuote){  
                        if(quote.Quote_Assignments__r.size() > 0){
                            for(Quote_Assignment__c qa : quote.Quote_Assignments__r){
                                quoteAss = new Quote_Assignment__c();
                                quoteAss.Quote__c = currentQuoteId;
                                quoteAss.Countries__c = qa.Countries__c;
                                quoteAss.Source_Language__c = qa.Source_Language__c;
                                quoteAss.Quote_Assignment_Id__c = qa.Id;    
                                quoteAss.Allowed_File_Formats__c = qa.Allowed_File_Formats__c;
                                quoteAss.Quote_Task_Description__c=qa.Quote_Task_Description__c;
                                quoteAss.Quote_Reference_Files_Instruction__c= qa.Quote_Reference_Files_Instruction__c;
                                quoteAss.Delivery_Format__c = qa.Delivery_Format__c;
                                quoteAss.isAssignmentUpdated__c = qa.isAssignmentUpdated__c; 
                                quoteAss.Keywords__c=qa.Keywords__c;
                                quoteAss.Step_one_complete__c=qa.Step_one_complete__c;
                                quoteAss.Step_two_complete__c=qa.Step_two_complete__c;  
                                quoteAss.Document_Type__c=qa.Document_Type__c;  
                                quoteAss.Additional_Note__c=qa.Additional_Note__c;
                                quoteAss.isFileAssociated__c  = qa.isFileAssociated__c;
                                quoteAss.Translation_Update__c = qa.Translation_Update__c;
                                quoteAss.Previous_GLS_Project_Number__c = qa.Previous_GLS_Project_Number__c;
                                lstQA.add(quoteAss);
                            }
                        }
                    }   
                 }                   
                system.debug('---lstQA-- '+lstQA);
                if(lstQA != null && lstQA.size()>0){
                    Database.insert(lstQA); 
                }  
                List<Quote_Assignment__c> lstQuoteAssg = [Select Id,Quote_Assignment_Id__c,Quote__c From Quote_Assignment__c Where Quote__c =: currentQuoteId];
                List<Quote_Assignment_File__c> lstQAFilesNew = new List<Quote_Assignment_File__c>();
                Quote_Assignment_File__c qaFileNew ; 
                Quote_Assignment_Service__c qaServiceNew ; 
                Quote_Assignment_Special_Provisions__c qaspNew ; 
                Quote_Assignment_Language__c qaTLangNew ; 
                List<Quote_Assignment_Service__c> lstQASNew = new List<Quote_Assignment_Service__c>();
                List<Quote_Assignment_Language__c> lstQATLangNew = new List<Quote_Assignment_Language__c>();
                List<Quote_Assignment_Special_Provisions__c> lstQASPNew = new List<Quote_Assignment_Special_Provisions__c>();
                Quote_Line_Item__c qliNew ;
                List<Quote_Line_Item__c> lstQLINew = new List<Quote_Line_Item__c>();
                Quote_Line_Service__c qlsNew; 
                List<Quote_Line_Service__c> lstQLSNew = new List<Quote_Line_Service__c>();
                Quote_Line_Item_Subline__c qlisNew ;
                List<Quote_Line_Item_Subline__c> lstQLISNew = new List<Quote_Line_Item_Subline__c>();
                
                system.debug('---lstQuoteAssg-- '+lstQuoteAssg);
                if(lstQuoteAssg != null && lstQuoteAssg.size()>0){
                    for(Quote_Assignment__c qaNew : lstQuoteAssg){
                        for(Quote_Assignment_File__c quoteAssFiles : lstQAF){
                            for(Quotes_File__c qf : lstQuotesFile){
                                if(qaNew.Quote_Assignment_Id__c == quoteAssFiles.Quote_Assignment__c && quoteAssFiles.Quote_Files__r.File_Name__c == qf.File_Name__c){
                                    qaFileNew = new Quote_Assignment_File__c();
                                    qaFileNew.File_Usage__c = quoteAssFiles.File_Usage__c;
                                    qaFileNew.Quote_Files__c = qf.Id;
                                    qaFileNew.Quote_Assignment__c = qaNew.Id;                                   
                                    lstQAFilesNew.add(qaFileNew);
                                }
                            }
                        }
                        for(Quote_Assignment_Service__c qas : lstQASParent){
                            if(qas.Quote_Assignment__c == qaNew.Quote_Assignment_Id__c){
                                qaServiceNew = new Quote_Assignment_Service__c();
                                qaServiceNew.Client_Service__c = qas.Client_Service__c;
                                qaServiceNew.Quote_Assignment__c = qaNew.Id;
                                qaServiceNew.Service_Name__c = qas.Service_Name__c;
                                lstQASNew.add(qaServiceNew);
                            }
                        }
                        for(Quote_Assignment_Language__c qal : lstQALParent){
                            if(qal.Quote_Assignment__c == qaNew.Quote_Assignment_Id__c){
                                qaTLangNew = new Quote_Assignment_Language__c();
                                qaTLangNew.Language_List_Item__c = qal.Language_List_Item__c;
                                qaTLangNew.Quote_Assignment__c = qaNew.Id;
                                qaTLangNew.Language_Name__c = qal.Language_Name__c;
                                lstQATLangNew.add(qaTLangNew);
                            }
                        }
                        for(Quote_Assignment_Special_Provisions__c qasp : lstQASPParent){
                            if(qasp.Quote_Assignment_Id__c == qaNew.Quote_Assignment_Id__c){
                                qaspNew = new Quote_Assignment_Special_Provisions__c();
                                qaspNew.Quote_Assignment_Id__c = qaNew.Id;
                                qaspNew.Standard_Special_Provisions__c = qasp.Standard_Special_Provisions__c;
                                qaspNew.Quote_Assignment_Special_Provision_Name__c =qasp.Quote_Assignment_Special_Provision_Text__c;
                                qaspNew.Quote_Assignment_Special_Provision_Text__c = qasp.Quote_Assignment_Special_Provision_Text__c;
                                lstQASPNew.add(qaspNew);
                            }
                        }
                    }
                    for(Quote_Line_Item__c qliParent : lstQLIParent){
                        qliNew = new Quote_Line_Item__c();
                        qliNew.Analyzed_File_URL__c = qliParent.Analyzed_File_URL__c;
                        qliNew.Analyzed_Transaction_ID__c = qliParent.Analyzed_Transaction_ID__c;
                        qliNew.Analyzed_Version_ID__c = qliParent.Analyzed_Version_ID__c;
                        qliNew.Client_TM__c = qliParent.Client_TM__c;
                        qliNew.Error_Message__c = qliParent.Error_Message__c;
                        qliNew.Extra_Days__c = qliParent.Extra_Days__c;
                        qliNew.File_Prep_Amount__c = qliParent.File_Prep_Amount__c;
                        qliNew.File_Prep_Description__c = qliParent.File_Prep_Description__c;
                        qliNew.isAnalyzed__c = qliParent.isAnalyzed__c;
                        qliNew.isTMFound__c = qliParent.isTMFound__c;
                        qliNew.Line_Item_Duration__c = qliParent.Line_Item_Duration__c;
                        qliNew.Min_Charge_Applied__c = qliParent.Min_Charge_Applied__c;
                        qliNew.QuoteID__c = currentQuoteId;
                        qliNew.Service_Words__c = qliParent.Service_Words__c;
                        qliNew.Source_Language_ID__c = qliParent.Source_Language_ID__c;
                        qliNew.Target_Language_ID__c = qliParent.Target_Language_ID__c;
                        qliNew.Task_Status__c = qliParent.Task_Status__c;
                        qliNew.TM_Export_File_ID__c = qliParent.TM_Export_File_ID__c;
                        qliNew.TM_Export_Transaction_ID__c = qliParent.TM_Export_Transaction_ID__c;
                        qliNew.TM_Export_Zip_File__c = qliParent.TM_Export_Zip_File__c;
                        qliNew.Trados_Language_Pair__c = qliParent.Trados_Language_Pair__c;
                        qliNew.Translation_Memory_Options_Flag__c = qliParent.Translation_Memory_Options_Flag__c;
                        qliNew.XML_Data__c = qliParent.XML_Data__c;
                        qliNew.Quote_Line_Item_Id__c = qliParent.Id;
                        qliNew.X95_99_Word_Count__c = qliParent.X95_99_Word_Count__c;
                        qliNew.X85_95_Word_Count__c = qliParent.X85_95_Word_Count__c;
                        qliNew.X75_84_Word_Count__c = qliParent.X75_84_Word_Count__c;
                        qliNew.X50_74_Word_Count__c = qliParent.X50_74_Word_Count__c;
                        qliNew.X100_Word_Count__c = qliParent.X100_Word_Count__c;
                        qliNew.Repetitions_Word_Count__c = qliParent.Repetitions_Word_Count__c;
                        qliNew.Perfect_Match__c = qliParent.Perfect_Match__c;
                        qliNew.No_Match_Word_Count__c  = qliParent.No_Match_Word_Count__c;
                        qliNew.Crossfile_Repetitions__c  = qliParent.Crossfile_Repetitions__c;
                        qliNew.Context_TM_Word_Count__c = qliParent.Context_TM_Word_Count__c;
                        qliNew.Actual_Words__c  = qliParent.Actual_Words__c ;
                        qliNew.Back_Translation_Total_Words__c = qliParent.Back_Translation_Total_Words__c ;
                        qliNew.isReadyForAnalyse__c = qliParent.isReadyForAnalyse__c;
                        qliNew.TWC__c  = qliParent.TWC__c;
                        qliNew.Context_TM_Word_Count__c  = qliParent.Context_TM_Word_Count__c;
                        lstQLINew.add(qliNew);
                    }
                }
                if(lstQAFilesNew != null && lstQAFilesNew.size()>0){
                    skipTrigger = true;
                    Database.insert(lstQAFilesNew); 
                    skipTrigger = false;
                }
                if(lstQASNew != null && lstQASNew.size()>0){
                    skipTrigger = true;
                    Database.insert(lstQASNew); 
                    skipTrigger = false;
                }
                if(lstQATLangNew != null && lstQATLangNew.size()>0){
                    skipTrigger = true;
                    Database.insert(lstQATLangNew); 
                    skipTrigger = true;
                }
                if(lstQASPNew != null && lstQASPNew.size()>0){
                    Database.insert(lstQASPNew); 
                }
                if(lstQLINew != null && lstQLINew.size()>0){
                    Database.insert(lstQLINew); 
                }
                List<Quote_Line_Item__c> lstQLIUpdate = [Select Id,QuoteID__c,Quote_Line_Item_Id__c From Quote_Line_Item__c Where QuoteID__c =: currentQuoteId];
                Quote_Line_Object_Assignment_File__c qloafNew ;
                List<Quote_Line_Object_Assignment_File__c> lstQLOAF = new List<Quote_Line_Object_Assignment_File__c>();
                for(Quote_Line_Item__c qli:lstQLIUpdate){
                    for(Quote_Line_Item__c qliOld : lstQLIParent){
                        if(qli.Quote_Line_Item_Id__c == qliOld.Id){
                            for(Quote_Line_Object_Assignment_File__c qloaf :  qliOld.Quote_Line_Object_Assignment_Files__r){
                                for(Quotes_File__c qf : lstQuotesFile){
                                    if(qf.File_Name__c == qloaf.Source_File_name__r.File_Name__c){
                                        qloafNew = new Quote_Line_Object_Assignment_File__c();
                                        qloafNew.Quote_Line_Item__c = qli.Id;
                                        qloafNew.Source_File_name__c = qf.Id;
                                        qloafNew.IsAnalysisProcessed__c = qloaf.IsAnalysisProcessed__c;
                                        qloafNew.special_Instructions__c = qloaf.special_Instructions__c;
                                        lstQLOAF.add(qloafNew);
                                    }
                                }
                            }
                            for(Quote_Line_Service__c qls : qliOld.Quote_Line_Services__r){
                                qlsNew = new   Quote_Line_Service__c();
                                qlsNew.Quote_Line_Item_Id__c = qli.Id;
                                qlsNew.Service_Id__c = qls.Service_Id__c;
                                lstQLSNew.add(qlsNew);
                            }
                            for(Quote_Line_Item_Subline__c qlis : qliOld.Quote_Line_Item_Sublines__r){
                                qlisNew = new Quote_Line_Item_Subline__c();
                                qlisNew.Client_Service__c = qlis.Client_Service__c;
                                qlisNew.Description__c = qlis.Description__c;
                                qlisNew.Manual_Entry__c = qlis.Manual_Entry__c;
                                qlisNew.Quantity__c = qlis.Quantity__c;
                                qlisNew.Quote_Line_Item_Id__c = qli.Id;
                                qlisNew.Show_Description__c = qlis.Show_Description__c;
                                qlisNew.Show_Subline_Total__c = qlis.Show_Subline_Total__c;
                                qlisNew.Show_Unit_of_Measure__c = qlis.Show_Unit_of_Measure__c;
                                qlisNew.Show_Unit_Price__c = qlis.Show_Unit_Price__c;
                                qlisNew.Show_Units__c = qlis.Show_Units__c;
                                qlisNew.Unit_of_Measure__c = qlis.Unit_of_Measure__c;
                                qlisNew.Unit_Price__c = qlis.Unit_Price__c;
                                qlisNew.Unit_Price_Std__c = qlis.Unit_Price_Std__c;
                                qlisNew.Default_Subline_Order__c  = qlis.Default_Subline_Order__c ;
                                qlisNew.Default_Order__c = qlis.Default_Order__c;
                                lstQLISNew.add(qlisNew);
                            }
                        }
                    }
                }
                if(lstQLOAF != null && lstQLOAF.size()>0){
                    Database.insert(lstQLOAF); 
                }
                if(lstQLSNew != null && lstQLSNew.size()>0){
                    Database.insert(lstQLSNew); 
                }
                if(lstQLISNew != null && lstQLISNew.size()>0){
                    Database.insert(lstQLISNew); 
                }
            } 
       }
    }catch(System.CalloutException callout){
        System.debug('CALLOUT EXCEPTION: ' + callout);
    }catch(Exception ex){
        System.debug(ex);
    }
    return '' ;
 }
    
  }