/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSLPW_EstimateTriggerHandler  Class
* Function: This class is the handler for Estimate trigger and creates corresponding Opportunity records. 
            
*/

public with sharing class GLSLPW_EstimateTriggerHandler {
	public static boolean skipTrigger = false;
    
    public static void updateOpportunity(List<Estimate__c> lstEstimate, Map<Id, Estimate__c> oldEstimateMap){       
        List<Opportunity>  lstInsertOpportunity = new List<Opportunity>();
        List<Estimate__c>  lstUpdateEstimate = new List<Estimate__c>();
        List<String> lstNewEstimateId = new List<String>();  
           
        List<Estimate__c> lstEstimateRec = new List<Estimate__c>();
        Estimate__c oldEstimate = new Estimate__c(); 
        Estimate__c oldUpdEstimate = new Estimate__c(); 
        List<Opportunity>  lstOpportunity = new List<Opportunity>();
        List<Opportunity>  lstUpdateOpportunity = new List<Opportunity>();
        List<String>  lstopportunityId = new List<String>();
                
    	List<Estimate__c> lstApprovedEstimates = new List<Estimate__c>();
    	List<Estimate__c> lstOboleteEstimates = new List<Estimate__c>();
    	List<Estimate__c> lstMasterEstimates = new List<Estimate__c>();
    	List<Estimate__c> lstAllEstimates = new list<Estimate__c>();
        List<Estimate__c> lstEstimateWithLineItems;
        List<Estimate__c> lstEstimatesForProtocol = new List<Estimate__c>();
        Set<Id> setAllProtocol = new Set<Id>(); 
        Map<Id,Set<Id>> mapEstIdProtocols = new Map<Id,Set<Id>>();
        Map<Id,List<Estimate_Line_Item__c>> mapProtocolEstLineItems = new Map<Id,List<Estimate_Line_Item__c>>();
        List<Protocol__c> lstAllProtocol;
        if(lstEstimate != null && lstEstimate.size() > 0){
	        for(Estimate__c estimate : lstEstimate){
	            oldEstimate = oldEstimateMap.get(estimate.ID);  
	            if(estimate.Status__c != oldEstimate.Status__c && estimate.Status__c == 'New' && !estimate.isEstimateRevision__c && !estimate.isEstimateVersion__c)
	                lstNewEstimateId.add(estimate.id);
	                                
	            if(estimate.Is_Primary__c && (estimate.Estimate_Amount__c != oldEstimate.Estimate_Amount__c || (estimate.Status__c != oldEstimate.Status__c && (estimate.Status__c == 'Approved' || estimate.Status__c == 'Cancelled')))){                
	                if(estimate.Status__c == GLSConfig.Approved && (!estimate.isChangeOrder__c)){                
	                	lstApprovedEstimates.add(estimate);                	
	                	Estimate__c est = new Estimate__c();
	                	if(estimate.Original_Estimate__c != null){
	                		est.id = estimate.Original_Estimate__c;   
	                	}else{
	                		est.id = estimate.Id;
	                	}	                	             	
	                	lstMasterEstimates.add(est);
	                }                 	                	
	                lstopportunityId.add(estimate.Opportunity__c);
	            }
	             
		        if((estimate.Estimate_Amount__c != oldEstimate.Estimate_Amount__c || estimate.Status__c != oldEstimate.Status__c) && estimate.Status__c == GLSConfig.Approved){
		        	lstEstimatesForProtocol.add(estimate);
		        }                                   	                     	                
	        }	        
	        if(lstEstimatesForProtocol != null && lstEstimatesForProtocol.size() > 0){
	        	lstEstimateWithLineItems = [select id,(select id,Protocol_Id__c from Estimate_Line_Items__r) from Estimate__c where id IN:lstEstimatesForProtocol];
	        }  
        }
        if(lstEstimateWithLineItems != null && lstEstimateWithLineItems.size() > 0){
        	for(Estimate__c est : lstEstimateWithLineItems){
        		List<Estimate_Line_Item__c> lstEstLineItem;        		
        		Set<Id> setProtocolId = new Set<id>(); 
        		lstEstLineItem = est.Estimate_Line_Items__r;
        		if(lstEstLineItem != null && lstEstLineItem.size() > 0){
        			for(Estimate_Line_Item__c estLineItem : lstEstLineItem){    
        				if(estLineItem.Protocol_Id__c!=null){    				
	        				setProtocolId.add(estLineItem.Protocol_Id__c);
	        				setAllProtocol.add(estLineItem.Protocol_Id__c);
        				}
        			}
        		}
        		if(setProtocolId != null && setProtocolId.size() > 0){
        			mapEstIdProtocols.put(est.id,setProtocolId);
        		}        		
        	}	        
	        if(setAllProtocol != null && setAllProtocol.size() > 0){
	        	List<Id> lstProtocolId = new List<Id>();
	        	lstProtocolId.addAll(setAllProtocol); 
	        	lstAllProtocol = [select id,(select id,Estimate_Id__r.Status__c,Amount__c from Estimate_Line_Items__r) from Protocol__c where ID IN: lstProtocolId];
	        }                
	        List<Protocol__c> lstProtocols = new List<Protocol__c>();
	        for(Id estId : mapEstIdProtocols.keySet()){
	        	Set<Id> lstTempProtocolId = mapEstIdProtocols.get(estId);
	        	Set<Estimate_Line_Item__c> setEstLineItem = new Set<Estimate_Line_Item__c>();
	        	for(Id prId : lstTempProtocolId){
		        	for(Protocol__c protocol : lstAllProtocol){
		        		if(protocol.id == prId){
		        			List<Estimate_Line_Item__c> lstEstimateLineItem = protocol.Estimate_Line_Items__r;
		        			if(lstEstimateLineItem != null && lstEstimateLineItem.size() > 0){
		        				mapProtocolEstLineItems.put(protocol.id,lstEstimateLineItem);		        				
		        				//setEstLineItem.addAll(lstEstimateLineItem);
		        			}
		        		}
		        	}	        		        	      
	        	}	        		        		        		        	
	        	if(mapProtocolEstLineItems != null && mapProtocolEstLineItems.keySet().size() > 0){
	        		Decimal approvedAmount;
		        	for(Id protocolId : mapProtocolEstLineItems.keySet()){
		        		approvedAmount = 0;
		        		List<Estimate_Line_Item__c> lstEstLineItems = mapProtocolEstLineItems.get(protocolId);
		        		if(lstEstLineItems != null && lstEstLineItems.size() > 0){
			        		for(Estimate_Line_Item__c estLine : lstEstLineItems){
				        		if(estLine.Estimate_Id__r.Status__c == GLSConfig.Approved){
				        				if(estLine.Amount__c != null)
				        					approvedAmount = approvedAmount + estLine.Amount__c;
				        		}
				        	}	
		        		}
		        		Protocol__c pr= new Protocol__c();
		        		pr.id = protocolId;        	
		        		pr.Approved_Amount__c = approvedAmount;
		        		lstProtocols.add(pr);
		        	}		        	
	        	}	        	        		        		
	        }      	        
	        try{	        	
	        	update lstProtocols;
	        }catch(DMLException e){
	        	system.debug(e.getMessage()+'----------------message-----------------');
	        }        	
        }
        
        List<Estimate__c> lstMasterEstimatesWithChild;        
        if(lstApprovedEstimates != null && lstApprovedEstimates.size() > 0){        
        	lstMasterEstimatesWithChild = [select id,(select id from Estimates__r) from Estimate__c where id IN:lstMasterEstimates];        	        	
        }        		
    	if(lstMasterEstimatesWithChild != null && lstMasterEstimatesWithChild.size() > 0){        	
        	for(Estimate__c est : lstMasterEstimatesWithChild){
        		List<Estimate__c> lstchildEst = est.Estimates__r;
        		if(lstMasterEstimates != null && lstMasterEstimates.size() > 0){
        			lstAllEstimates.addAll(lstMasterEstimates);
        		}        			        		
        		if(lstchildEst != null && lstchildEst.size() > 0){
        			lstAllEstimates.addAll(lstchildEst);        			
        		}          		
        	}
    	}        
    	if(lstAllEstimates != null && lstAllEstimates.size() > 0){
    		Boolean flag;
    		for(Estimate__c est : lstAllEstimates){
    			flag = false;
    			if(lstApprovedEstimates != null && lstApprovedEstimates.size() > 0){
        			for(Estimate__c appEst:lstApprovedEstimates){
        				if(appEst.id==est.id){
        					flag = true;
        					break;
        				}
        			}
    			}
    			if(flag==false){
    				est.status__c = GLSConfig.EstStatusObsolete;
    				est.Is_Primary__c=false;
    				lstOboleteEstimates.add(est);	
    			}
    		}
    		if(lstOboleteEstimates!=null && lstOboleteEstimates.size()>0){
    			try{        				
    				update lstOboleteEstimates;
    			}catch(DMLException dmlex){
    				system.debug(dmlex+'dmlex----------------------------');
    			} 
    		}
    	}
                        
        /**
        * Create New Opportunitty
        */
        system.debug('----lstNewEstimateId----: '+lstNewEstimateId);
        if(lstNewEstimateId != null && lstNewEstimateId.size()>0 ){
             
            lstEstimateRec = [Select Id, Status__c, Name, Opportunity__c, LP_Estimate_ID__c, Estimate_Amount__c, Program_Id__c, Program_Id__r.Account__c , Program_Id__r.Account__r.Name, Program_Id__r.Account__r.Industry, 
                                Program_Id__r.Sponsor__r.Name,  Program_Id__r.Therapeutic_Area__c, Is_Primary__c, Program_Id__r.Reference__c
                             From Estimate__c Where Id in : lstNewEstimateId];
            List<String> lstProgrmId = new List<String>();        
            for(Estimate__c estimate : lstEstimateRec){
            	if(estimate.Program_Id__c!=null)
            		lstProgrmId.add(estimate.Program_Id__c);
            }
            List<Program__c> lstProgram;
            Map<Id,List<Protocol__c>> mapProgrmProtocol = new Map<Id,List<Protocol__c>>();            
            
            if(lstProgrmId != null && lstProgrmId.size() > 0){
            	lstProgram = [select id,(select id,name,Protocol_Name__c from Protocols__r) from Program__c where id IN:lstProgrmId];
            	if(lstProgram != null && lstProgram.size() > 0){
            		for(Program__c prgm: lstProgram){
            			mapProgrmProtocol.put(prgm.id,prgm.Protocols__r);
            		}            			
            	}	
            }    
            
            for(Estimate__c estimate : lstEstimateRec){
                if(estimate.Status__c == 'New'){
                    Opportunity oppObj = new Opportunity();
                    oppObj.AccountId            = estimate.Program_Id__r.Account__c;
                    oppObj.Amount               = estimate.Estimate_Amount__c;
                    oppObj.Industry__c          = estimate.Program_Id__r.Account__r.Industry;
                    oppObj.IsAutoCreated__c     = true;
                    
                    List<Protocol__c> lstProtocols = mapProgrmProtocol.get(estimate.Program_Id__c);
                    String protocolNameStr = '';
                    if(lstProtocols!=null && lstProtocols.size()>0){
                    	for(Protocol__c pro: lstProtocols){
                    		protocolNameStr = protocolNameStr + pro.Protocol_Name__c+','; 
                    	}                    	 
                    }
                    if(protocolNameStr.length()>255)
                    	protocolNameStr = protocolNameStr.substring(0, 254);                    
                    	
                    oppObj.Protocol_Number__c = protocolNameStr;                        
                    
                    if(estimate.LP_Estimate_ID__c != null)
                    	oppObj.Name                 = estimate.Program_Id__r.Account__r.Name+': '+estimate.LP_Estimate_ID__c;
                    else
                    	oppObj.Name                 = estimate.Program_Id__r.Account__r.Name+': '+estimate.Name;
                    oppObj.StageName            = 'B-Goals Shared';
                    if(estimate.Program_Id__r.Sponsor__r.Name != null)
                    	oppObj.Study_Sponsor__c     = estimate.Program_Id__r.Sponsor__r.Name;
                    else
                    	oppObj.Study_Sponsor__c = estimate.Program_Id__r.Reference__c;
                    oppObj.TherapeuticArea__c	= estimate.Program_Id__r.Therapeutic_Area__c;
                    oppObj.Primary_Estimate__c 	= estimate.Id;
                    oppObj.CloseDate = system.today() + 30;
                    lstInsertOpportunity.add(oppObj);
                }
             }
            if(lstInsertOpportunity != null && lstInsertOpportunity.size()>0){
                    try{
                        insert lstInsertOpportunity;
                        for(Opportunity lstOpp : lstInsertOpportunity){
                        	for(Estimate__c estimate : lstEstimateRec){
                        		if(estimate.Id == lstOpp.Primary_Estimate__c){
                        			estimate.Opportunity__c = lstOpp.Id;
                        			lstUpdateEstimate.add(estimate);
                        		}
                        	}
                        }
                        if(lstUpdateEstimate != null && lstUpdateEstimate.size()>0){
                        	try{
                        		skipTrigger = true;
                        		update lstUpdateEstimate;
                        	}catch(DMLException de){
                        		system.debug(GLSConfig.DMLExceptionOccured+'Update Estimate' +de);
                    		}
                        }
                    }catch(DMLException de){
                        system.debug(GLSConfig.DMLExceptionOccured +de);
                    }
             }
        
            }
            
            /**
            * UpdateOpportunitty
            */ 
            
            if(lstopportunityId != null && lstopportunityId.size()>0 ){
                lstOpportunity = [Select Id, Amount, StageName From Opportunity Where Id in : lstopportunityId];
            
                system.debug('---lstOpportunity----: '+lstOpportunity);
                for(Estimate__c lstEst: lstEstimate){
                	if(lstEst.Is_Primary__c == true) {  
	                    oldUpdEstimate = oldEstimateMap.get(lstEst.ID);
	                    for(Opportunity opps : lstOpportunity){
	                        if(lstEst.Opportunity__c == opps.Id){
	                            opps.Amount = lstEst.Estimate_Amount__c;
	                            if(lstEst.Status__c != oldUpdEstimate.Status__c && lstEst.Status__c == 'Approved' )
	                                opps.StageName = 'W-Won';
	                            if(lstEst.Status__c != oldUpdEstimate.Status__c && lstEst.Status__c == 'Cancelled' ){
	                                opps.CloseDate  = system.today();
	                                if(lstEst.Reason_for_Cancellation__c == GlsConfig.OverAllCost)
	                                    opps.StageName = GLSConfig.Lostpricing;
	                                else if(lstEst.Reason_for_Cancellation__c == GlsConfig.TurnaroundTime)
	                                    opps.StageName = GLSConfig.Lostschedule;
	                                else if(lstEst.Reason_for_Cancellation__c == GlsConfig.ExceededClientBudget)
	                                    opps.StageName = GLSConfig.Lostcompetition;
	                                else if(lstEst.Reason_for_Cancellation__c == GlsConfig.VendorSelection)
	                                    opps.StageName = GLSConfig.LostGLSWithdraw;
	                                else
	                                    opps.StageName = GLSConfig.Lostcompetition;
	                            }
	                            lstUpdateOpportunity.add(opps);
	                        }
	                    }
                	}
                } 
                if(lstUpdateOpportunity != null && lstUpdateOpportunity.size()>0){
                      try{
                          update lstUpdateOpportunity;
                      }catch(DMLException de){
                         system.debug(GLSConfig.DMLExceptionOccured +de);
                      }
                }
            }
        } 
        
        /*
        	@Description: Method is called for before trigger on EStimate__c object,It will check isPrimary__c if estimate is approved.
        	@param: It will take Trigger.new as a parameter. 
        */
        public static void updateEstimate(List<Estimate__c> lstEst){        	        	
        	List<Estimate__c> lstApprovedEstimates = new List<Estimate__c>();        	
        	if(lstEst!=null && lstEst.size()>0){
	        	for(Estimate__c est : lstEst){
	        		if(est.status__c == GLSConfig.Approved){
	        			est.Is_Primary__c = true;	        			        			       				
	        		}
	        	} 
        	}        	        	        	
        }    
}