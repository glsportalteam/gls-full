/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSQW_CustomCountryLookupControllerTest {
	
	static testMethod void testCountryLookup() {
		Account accObj 					= GLSLPW_SetUpData.createAccount('Amgen');
        Contact conObj 					= GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
   	    Quote__c quoteObj1 			 	= GLSLPW_SetUpData.createQuote(accObj.Id, conObj.Id, 'Billed');
        Country__c countryObj 			= GLSLPW_SetUpData.createCountry('Test Country');
        
        Language__c languageObj1		= GLSLPW_SetUpData.createLanguage('Test Language 1');
        Language__c languageObj2		= GLSLPW_SetUpData.createLanguage('Test Language 2');
        
        Language_List_Item__c lliObj1	= GLSLPW_SetUpData.createLanguageListItem('Test Language 1');
        Language_List_Item__c lliObj2	= GLSLPW_SetUpData.createLanguageListItem('Test Language 2');
        
        Country_Language__c clObj 		= GLSLPW_SetUpData.createCountryLanguage(countryObj.Id, lliObj1.Id);
        Country_Language__c clObj1 		= GLSLPW_SetUpData.createCountryLanguage(countryObj.Id, lliObj2.Id);
        Quote_Line_Item__c quoteLI1 	= GLSLPW_SetUpData.createQuoteLineItem(quoteObj1.Id, countryObj.Id, lliObj1.Id, lliObj2.Id);
      
	 	 ApexPages.currentPage().getParameters().put('qliId', quoteLI1.Id);
	 	 Test.startTest();
         ApexPages.currentPage().getParameters().put('lksrch', 'Test Country');
         GLSQW_CustomCountryLookupController obj = new GLSQW_CustomCountryLookupController();
         system.assert(obj.searchString!='');
         obj.search();
         obj.getFormTag();
         obj.getTextBox();
         ApexPages.currentPage().getParameters().put('lksrch', '');
         GLSQW_CustomCountryLookupController obj1 = new GLSQW_CustomCountryLookupController();
         Test.stopTest();
   }
}