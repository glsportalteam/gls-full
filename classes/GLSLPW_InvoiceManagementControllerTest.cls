/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSLPW_InvoiceManagementControllerTest {

    static testMethod void setUpTestData(){
       Account accObj                   = GLSLPW_SetUpData.createAccount('Amgen');
       Contact conObj                   = GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
       Program__c prgObj                = GLSLPW_SetUpData.createProgram(accObj.Id,conObj.Id,'Program_Test', 'Amgen');
       Protocol__c protObj              = GLSLPW_SetUpData.createProtocol(prgObj.Id,'Closed');
       Purchase_Agreement__c paObj      = GLSLPW_SetUpData.createPurchaseAgreement(protObj.Id);
       Language_List_Item__c lliObj1    = GLSLPW_SetUpData.createLanguageListItem('Test Language 1');
       Language_List_Item__c lliObj2    = GLSLPW_SetUpData.createLanguageListItem('Test Language 2');
       Country__c countryObj            = GLSLPW_SetUpData.createCountry('Test Country');
       Quote__c quoteObj                = GLSLPW_SetUpData.createQuote(accObj.Id,conObj.Id,'Billable');
       Quote_Line_Item__c quoteLI       = GLSLPW_SetUpData.createQuoteLineItem(quoteObj.Id, countryObj.Id, lliObj1.Id, lliObj2.Id);
       
       quoteObj.Protocol__c             = protObj.Id;
       quoteObj.Program__c              = prgObj.Id;
       update quoteObj;
       protObj.PA_Amount__c             = 1000;
       update protObj;
       paObj.Total_Amount__c            = 2000;
       update paObj;
        }
    static testMethod void testSelectedFalse() {
        setUpTestData();
        Account accObj              = [ SELECT Name, Id FROM ACCOUNT LIMIT 1];
        Protocol__c protObj         = [ SELECT Name, Id FROM Protocol__c LIMIT 1];
        Program__c prgObj           = [ SELECT Name, Id FROM Program__c LIMIT 1];
        Quote__c quoteObj           = [ SELECT Name, Id FROM Quote__c LIMIT 1];
        Quote_Line_Item__c quoteLI  = [ SELECT Name, Id,Total_Amount__c FROM Quote_Line_Item__c LIMIT 1]; 
        Purchase_Agreement__c paObj = [ SELECT Name, Id FROM Purchase_Agreement__c LIMIT 1];
        Test.startTest();
         
        GLSLPW_InvoiceManagementController obj = new GLSLPW_InvoiceManagementController(); 
        obj.selectedAccountId   = accObj.Id;
        obj.selectedProgramId   = prgObj.Id;
        obj.selectedProtocolId  = protObj.Id;
        obj.selectedPAId        = paObj.Id;
        
        obj.searchOrder();
        obj.getPurchaseAgreements();
        obj.generateInvoice();
        
        obj.selectedProtocolId  = '';
        obj.searchOrder();
        
        quoteObj.Client__c = obj.selectedAccountId;
        quoteObj.Protocol__c = null;
        update quoteObj;
        obj.selectedProgramId   = '';
        obj.searchOrder();
        Test.stopTest();
    }
    
      static testMethod void testSelectedTrue(){
      setUpTestData();
        Account accObj                      = [ SELECT Name, Id FROM ACCOUNT LIMIT 1];
        Protocol__c protObj                 = [ SELECT Name, Id FROM Protocol__c LIMIT 1];
        Program__c prgObj                   = [ SELECT Name, Id FROM Program__c LIMIT 1];
        Purchase_Agreement__c paObj         = [ SELECT Name, Id FROM Purchase_Agreement__c LIMIT 1];
        Quote__c quoteObj                   = [ SELECT Name, Id,QLIESOverridden__c,Expedited__c,QLIESAmount__c,Expedited_Surcharge_Amount__c,Quote_Total_Amount__c FROM Quote__c LIMIT 1];
        Quote_Line_Item__c quoteLI          = [ SELECT Name, Id,Total_Amount__c FROM Quote_Line_Item__c LIMIT 1]; 
                                                            
        paObj.Total_Amount__c       = 0;
        update paObj;
        Test.startTest();
        GLSLPW_InvoiceManagementController obj = new GLSLPW_InvoiceManagementController(); 
        obj.selectedAccountId   = accObj.Id;
        obj.selectedProgramId   = prgObj.Id;
        obj.selectedProtocolId  = protObj.Id;
        obj.selectedPAId        = paObj.Id;
        
        GLSLPW_InvoiceManagementController.OrderWrapper owObj = new GLSLPW_InvoiceManagementController.OrderWrapper();
        owObj.quoteId = quoteObj.Id;
        owObj.isSelected = true;
        owObj.protocolId = protObj.Id;
        owObj.quote = quoteObj;
        obj.lstOrderWrapper.add(owObj);
        
        obj.getPurchaseAgreements();
        obj.generateInvoice();
        Test.stopTest();
      
      }
}