@isTest
private class GLSCSVParserTest {

    static testMethod void testCSVFeature() {
        GLSSetupData.createQLIServices();
        Quote_Line_Object_Assignment_File__c qLAsstFiles1 = new Quote_Line_Object_Assignment_File__c();
        list<Quote_Line_Object_Assignment_File__c> lstAssgnFile = new list<Quote_Line_Object_Assignment_File__c>();
        qLAsstFiles1.Quote_Line_Item__c = GLSSetupData.quoteLI1.Id;
        qLAsstFiles1.Source_File_name__c = GLSSetupData.qFiles1.id;
        lstAssgnFile.add(qLAsstFiles1);
        Quote_Line_Object_Assignment_File__c qLAsstFiles2 = new Quote_Line_Object_Assignment_File__c();
        qLAsstFiles2.Quote_Line_Item__c = GLSSetupData.quoteLI2.Id;
        qLAsstFiles2.Source_File_name__c = GLSSetupData.qFiles2.id;
        //insert qLAsstFiles2;
        lstAssgnFile.add(qLAsstFiles2);
        Quote_Line_Object_Assignment_File__c qLAsstFiles3 = new Quote_Line_Object_Assignment_File__c();
        qLAsstFiles3.Quote_Line_Item__c = GLSSetupData.quoteLI2.Id;
        qLAsstFiles3.Source_File_name__c = GLSSetupData.qFiles3.id;
        //insert qLAsstFiles3;
        lstAssgnFile.add(qLAsstFiles3);        
        insert lstAssgnFile; 
        String credName = createTestCredentials();
        GLSAmazonUtility.AWSCredentialName = credName;
        //String path = 'amgen*a0rK0000001AcOpIAK/Trados/625/AutomaticTask20140401_080230_853242.xml';
        String AnalyzedURL = GLSAmazonUtility.downloadQuoteFile('Test.txt',GLSConfig.bucketName); 
        String CSV = ';;;Context TM;;;;Repetitions;;;;100% Matches;;;;95% - 99%;;;;85% - 94%;;;;75% - 84%;;;;50% - 74%;;;;No Match;;;;Total'+'\n'+
        'File;Tagging Errors;Chars/Word;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Characters'+'\n'+
        'Test.txt | en>bg_bg;0;3.55;61;61;0;0.22;631;2460;0;8.73;168;166;0;0.59;176;753;0;2.67;56;929;0;3.3;67;644;0;2.28;243;1657;0;5.88;1796;21514;0;76.33;3198;28184;0;100067'+'\n'+
        'Test.txt | en>bg_bg;0;3.55;61;61;0;0.22;631;2460;0;8.73;168;166;0;0.59;176;753;0;2.67;56;929;0;3.3;67;644;0;2.28;243;1657;0;5.88;1796;21514;0;76.33;3198;28184;0;100067';
        test.startTest();
        GLSCSVParser parserObj = new GLSCSVParser();
        parserObj.parser(GLSSetupData.quoteLI1.id, GLSSetupData.quote1.Id, AnalyzedURL,GLSSetupData.quoteLI1.Error_Message__c , GLSSetupData.quoteLI1.Task_Status__c, CSV, 'analyzedSingleQLI');
        test.stopTest();
    }
        
    private static String createTestCredentials(){
        AWSKey__c testKey = new AWSKey__c(name='test keys',key__c='key',secret__c='secret');
        insert testKey;
        return testKey.name;    
    }
}