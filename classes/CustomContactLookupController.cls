public with sharing class CustomContactLookupController { 
  public List<Contact> results{get;set;} // search results
  public string searchString{get;set;} // search keyword
 
  public CustomContactLookupController() {
    searchString = System.currentPageReference().getParameters().get('lksrch');
    runSearch();  
  }
 
  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }
 
  // prepare the query and issue the search command
  private void runSearch() {
    if(searchString != null)
        searchString = String.escapeSingleQuotes(searchString);
    results = performSearch(searchString);               
  } 
 
  // run the search and return the records found. 
  private List<Contact> performSearch(string searchString) {
    String account = System.currentPageReference().getParameters().get('account');
    String soql = 'select id, name from contact';
    if(searchString != '' && searchString != null)
      soql = soql +  ' where name LIKE \'%' + searchString +'%\'';
    else 
      soql = soql +  ' where name LIKE \'%\'';  
    if(account != '' && account != null)
      soql = soql + ' AND AccountId = \'' + String.escapeSingleQuotes(account) + '\'';
    soql = soql + ' order by LastName, FirstName limit 250';
    System.debug(soql);
    return database.query(soql); 
 
  }
 
  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
 
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
}