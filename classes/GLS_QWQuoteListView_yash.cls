public class GLS_QWQuoteListView_yash {
	public List<Quote__c> lstQuote {get; set;}
	public String searchQuoteStr {get; set;}
	public String accountVal {get; set;}
	public String contactVal {get; set;}
	public String StatusVal {get; set;}
	public String ownerVal {get; set;}
	public Quote__c quoteObj{get;set;}
	public boolean isAllChecked{get;set;}
	public boolean renderQuoteListView {get;set;}
	public String cloneQID {get;set;}
	
	public Boolean iscloneWithFiles {get;set;}
	public Boolean iscloneWithoutFiles {get;set;}
	public transient List<ToDisplay> toDisplayList{get;set;} 
	public String selectedQuoteId{get;set;}
	private integer totalRecs = 0;
	private integer OffsetSize = 0;
	private integer LimitSize= 100;

	public GLS_QWQuoteListView_yash(ApexPages.StandardController stdcon) {
		quoteObj = (Quote__c)stdcon.getRecord(); 
		lstQuote = new List<Quote__c>();
		isAllChecked = true;
		searchQuote();
	}

	
	public pageReference searchQuote() { 
   
        string query = 'Select Id, Name, Quote_Number__c, Client__c, Client__r.Name, Contact__c,Contact_Hidden__c, Contact__r.Name, CreatedDate,Status__c, Owner.Name, Quote_Name__c, Quote_Due_Date__c, Assigned_To__r.Name From Quote__c';
        string queryCount = 'Select count() From Quote__c';
        string whereClause = '';
        String orderby = ' order by Createddate desc';
        string expiredStatus='Expired';
        if(isAllChecked){
            whereClause =' Where Status__c !=: expiredStatus';
        }else
        if (accountVal == '' && contactVal  == '' && statusVal == '' && OwnerVal == '') {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.INFO, GLSConfig.noFilterCriteriaSelected));
            renderQuoteListView = true;
            return null;
        }else if(accountVal != '' && contactVal  != '' && statusVal != '' && OwnerVal != '') {
            whereClause = ' Where Client__r.Name =:accountVal And Contact__r.Name =:contactVal And Status__c =: statusVal And (owner.Name =:ownerVal Or Assigned_To__r.Name =:ownerVal)';
        }else if(accountVal != '' && contactVal  == '' && statusVal == '' && OwnerVal == '') {
            whereClause = ' Where Client__r.Name =:accountVal and Status__c !=: expiredStatus';
        }else if(accountVal == '' && contactVal  != '' && statusVal == '' && OwnerVal == '') {
            whereClause = ' Where Contact__r.Name =:contactVal and Status__c !=: expiredStatus';
        }else if(accountVal == '' && contactVal  == '' && statusVal != '' && OwnerVal == '') {
            whereClause = ' Where status__c =:statusVal';
        }else if(accountVal == '' && contactVal  == '' && statusVal == '' && OwnerVal != '') {
            whereClause = ' Where (owner.Name =:ownerVal Or Assigned_To__r.Name =:ownerVal) and Status__c !=: expiredStatus';
        }else if(accountVal != '' && contactVal  != '' && statusVal == '' && OwnerVal == '') {
            whereClause = ' Where Client__r.Name =:accountVal And Contact__r.Name =:contactVal and Status__c !=: expiredStatus';
        }else if(accountVal != '' && contactVal  != '' && statusVal != '' && OwnerVal == '') {
            whereClause = ' Where Client__r.Name =:accountVal And Contact__r.Name =:contactVal AND Status__c =: statusVal';
        }/*else if(accountVal != '' && contactVal  == '' && statusVal == '' && OwnerVal == '') {
            whereClause = ' Where Client__r.Name =:accountVal And Status__c =: statusVal';
        }*/else if(accountVal != '' && contactVal  != '' && statusVal == '' && OwnerVal != '') {
            whereClause = ' Where Client__r.Name =:accountVal And Contact__r.Name =:contactVal And (owner.Name =:ownerVal Or Assigned_To__r.Name =:ownerVal) and Status__c !=: expiredStatus';
        }else if(accountVal != '' && contactVal  == '' && statusVal != '' && OwnerVal != '') {
            whereClause = ' Where Client__r.Name =:accountVal And Status__c =: statusVal And (owner.Name =:ownerVal Or Assigned_To__r.Name =:ownerVal)';
        }else if(accountVal == '' && contactVal  != '' && statusVal != '' && OwnerVal != '') {
            whereClause = ' Where contact__r.Name =:contactVal And Status__c =: statusVal And (owner.Name =:ownerVal Or Assigned_To__r.Name =:ownerVal)';
        }else if(accountVal == '' && contactVal  == '' && statusVal != '' && OwnerVal != '') {
            whereClause = ' Where Status__c =: statusVal And (owner.Name =:ownerVal Or Assigned_To__r.Name =:ownerVal)';
        }else if(accountVal == '' && contactVal  != '' && statusVal != '' && OwnerVal == '') {
            whereClause = ' Where contact__r.Name =:contactVal And Status__c =: statusVal';
        }else if(accountVal != '' && contactVal  == '' && statusVal == '' && OwnerVal != '') {
            whereClause = ' Where client__r.Name =:accountVal And (owner.Name =:ownerVal Or Assigned_To__r.Name =:ownerVal) and Status__c !=: expiredStatus';
        }else if(accountVal == '' && contactVal  != '' && statusVal == '' && OwnerVal != '') {
            whereClause = ' Where contact__r.Name =:contactVal And (owner.Name =:ownerVal Or Assigned_To__r.Name =:ownerVal) and Status__c !=: expiredStatus';
        }else if(accountVal != '' && contactVal  == '' && statusVal != '' && OwnerVal == '') {
            whereClause = ' Where client__r.Name =:accountVal And Status__c =: statusVal';
        }
        toDisplayList = new List<ToDisplay>();
        String limitClause= ' LIMIT '+LimitSize+' OFFSET '+ OffsetSize; 
        query = query+whereClause +orderby+limitClause;
        system.debug(query+'query-------------------------------');
        try{
        	totalRecs =Database.countQuery(queryCount);        	
            lstQuote = Database.query(query);
            for(Integer i=0; i<lstQuote.size(); i++){
            toDisplayList.add(
                new toDisplay( lstQuote[i].Id,lstQuote[i].Quote_Number__c,lstQuote[i].Createddate,lstQuote[i].Status__c,
                               lstQuote[i].Client__r.Name, lstQuote[i].Client__c, lstQuote[i].Contact__r.Name,lstQuote[i].Contact__c,
                               lstQuote[i].Quote_Due_Date__c, lstQuote[i].owner.Name, lstQuote[i].Assigned_To__r.Name));
                                
           }
        }catch(QueryException qe){
            system.debug('---Query Exception--'+qe);
        }
        
        renderQuoteListView = true;
        return null;
    } 
    public void FirstPage(){    	
		OffsetSize = 0;
		searchQuote();
	}
	
	public void previous(){		
		OffsetSize = OffsetSize - LimitSize;
		searchQuote();
	}
	
	public void next(){		
		OffsetSize = OffsetSize + LimitSize;
		searchQuote();
	}
	
	public void LastPage(){
		OffsetSize = totalrecs - math.mod(totalRecs,LimitSize);
		searchQuote();
	}
	
	public boolean getprev(){
		if(OffsetSize == 0)
		return true;
		else
		return false;
	}
	
	public boolean getnxt(){
		if((OffsetSize + LimitSize) > totalRecs)
		return true;
		else
		return false;
	}
		
	public pageReference callCloneMethod() { 
	 	system.debug('----inside callCloneMethod------');
	 	list<Quote__c> lstCloneQuote = new list<Quote__c>();
		PageReference pRef ;
	 	if(cloneQId != null){
		 	try{
			 	if(iscloneWithFiles){
			 		lstCloneQuote = GLSAmazonUtility.cloneQuote(cloneQId);
			 		pRef = new PageReference('/apex/GLSQW_NewQuotePage?id='+lstCloneQuote[0].id+'&parentId='+cloneQId+'&act=clone'); 
			 	}
			 	else if(iscloneWithoutFiles){
			 		lstCloneQuote = GLSAmazonUtility.cloneQuoteWithoutFiles(cloneQId);
			 		pRef = new PageReference('/apex/GLSQW_NewQuotePage?id='+lstCloneQuote[0].id); 
			 	}
		 	}catch(Exception e)	{
		 		system.debug(GLsConfig.Exceptionoccured);
		 	}
	 	}
	 	system.debug('---lstCloneQuote--' +lstCloneQuote);
	 	if(lstCloneQuote != null && lstCloneQuote.size()>0){
        	pRef.setRedirect(true);
	 		return pRef;
	 	}else return null;
	 }
	
	 public pageReference editQuote() { 
	 	if(cloneQId != null ){
	 		PageReference pRef = new PageReference('/apex/GLSQW_NewQuotePage?id='+cloneQId);
        	pRef.setRedirect(true);
            return pRef;
	 	}
        else return null;
	 }
	
	public pagereference cancelQuote(){
    	if(cloneQId != null){
    		PageReference pRef = new PageReference('/apex/GLS_QWQuoteListView');	
    			try{
    				Quote__c qOb = new Quote__c(id=cloneQId);
    				qOb.Status__c = 'Cancelled';
    				Database.update(qOb);
    				pRef.setRedirect(true);
    				return pRef;
    			}catch(Exception e){
             		return null;    
    			}
    	} 
    	return null;
    }
	
	
  	 public Pagereference decideRedirectPage(){
  	 	PageReference pref = null;
  	 	if(selectedQuoteId != null){
	  	 	List<Quote_Assignment__c> lstQA = [Select Id From Quote_Assignment__c Where Quote__c =: selectedQuoteId];
	  	 	if(lstQA != null && lstQA.size() > 0){
				pref = new Pagereference('/apex/GLSQW_QuoteOverview?id='+selectedQuoteId);
			}else{
				Quote_Assignment__c newQAssg = new Quote_Assignment__c(Quote__c = selectedQuoteId);     
            	try{
            		Database.SaveResult result = Database.insert(newQAssg);
            		pref = new Pagereference('/apex/GLSQW_QuoteOverview?id='+selectedQuoteId);
            	}catch(DMLException de){
            		system.debug(GLSConfig.DMLExceptionOccured+de);
            	}
			}
  	 	}
  	 	return pref;
  	 }
	 public class ToDisplay{
	 	public String quoteId{get;set;}
        public String quoteNumber{get;set;}
        public DateTime createdDate{get;set;}
        public String Status{get;set;}
        public String AccountName{get;set;}
        public String AccountId{get;set;}
        public String contactName{get;set;}
        public String contactId{get;set;}
        public Date qDueDate{get;set;}
        public String ownerName{get;set;}
        public String assignedTo{get;set;}

       
        public ToDisplay(   
                            string quoteId,
                            String quoteNumber,
                            DateTime createdDate,
                            String Status,
                            String AccountName,
                            String AccountId,
                            String contactName,
                            String contactId,
                            Date qDueDate,
                            String ownerName,
                            String assignedTo
                            ){
            this.quoteId      = quoteId      ;
            this.quoteNumber      = quoteNumber      ;
            this.createdDate      = createdDate     ;
            this.Status           = Status        ;
            this.AccountName      = AccountName       ;
            this.AccountId        = AccountId      ;
            this.contactName      = contactName   ;
            this.contactId        = contactId   ;
            this.qDueDate   	  = qDueDate   ;
            this.ownerName   	  = ownerName     ;
            this.assignedTo   	  = assignedTo == null ? ownerName : assignedTo;
                    
        }
    }
}