@isTest
public with sharing class GLSCommunity_DraftControllerTest {

    static testMethod void TestConstructorWithAccountManager() {
		Account accObj = GLSLPW_SetUpData.createAccount('Amgen');
		Contact conObj1 = GLSLPW_SetUpData.createContact(accObj.Id,'TestName1');
		Contact conObj2 = GLSLPW_SetUpData.createContact(accObj.Id,'TestName2');
		Contact conObj3 = GLSLPW_SetUpData.createContact(accObj.Id,'TestName3');
		Program__c prg = GLSLPW_SetUpData.createProgram(accObj.Id, conObj1.Id, 'TestName', 'Test Ref');
		Program__c prg2 = GLSLPW_SetUpData.createProgram(accObj.Id, conObj1.Id, 'TestName2', 'Test Ref2');
		Protocol__c pro = GLSLPW_SetUpData.createProtocol(prg.Id, 'Open');
		Protocol__c pro2 = GLSLPW_SetUpData.createProtocol(prg.Id, 'Open');
		Protocol__c pro3 = GLSLPW_SetUpData.createProtocol(prg2.Id, 'Open');
				
		Profile p1 = [select id from profile where name=: GLSConfig.AccountManager];
        User u1 = new User(LastName = 'test user 1', 
                             Username = 'test.user.1@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'testu1',
                             contactId = conObj1.Id, 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = p1.Id, 
                             LanguageLocaleKey = 'en_US'
                             );
        insert u1;
        List<Quote__c> quotelst = new List<Quote__c>();
        Quote__c quote = new Quote__c();
        quote.Quote_Name__c = 'Test Quote';
        quote.Client__c = u1.AccountId;
        quote.Status__c='Draft';
        quote.Contact__c = u1.contactId;
        quote.OwnerId = u1.Id;
        quote.Client_Reference__c ='client reference';
        quote.Quote_Name_GLS__c = 'billing reference';
        quote.Project_Name_Community__c = 'test Quote';
        quotelst.add(quote);
        
        Quote__c quote2 = new Quote__c();
        quote2.Quote_Name__c = 'Test Quote 2';
        quote2.Client__c = u1.AccountId;
        quote2.Status__c='Draft';
        quote2.Contact__c = u1.contactId;
        quote2.OwnerId = u1.Id;
        quote2.Client_Reference__c ='client reference 2';
        quote2.Quote_Name_GLS__c = 'billing reference 2';
        quote2.Project_Name_Community__c = 'test Quote 2';
        quotelst.add(quote2);
        
        insert quotelst;
        
        /*Profile p2 = [select id from profile where name=: GLSConfig.ProtocolManager];
        User u2 = new User(LastName = 'test user 2', 
                             Username = 'test.user.2@example.com', 
                             Email = 'test.2@example.com', 
                             Alias = 'testu2',
                             contactId = conObj2.Id, 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = p2.Id, 
                             LanguageLocaleKey = 'en_US'
                             );
        insert u2;
        */
        /*Profile p3 = [select id from profile where name=: GLSConfig.ProgramManager];
        User u3 = new User(LastName = 'test user 3', 
                             Username = 'test.user.3@example.com', 
                             Email = 'test.3@example.com', 
                             Alias = 'testu3',
                             contactId = conObj3.Id, 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = p3.Id, 
                             LanguageLocaleKey = 'en_US'
                             );
        insert u3;
        */
        
		Test.startTest();
              
             System.runAs (u1){
             	Pagereference pg;    			    	
	    		pg = Page.GLSCommunity_DraftPage;
	    		Test.setCurrentPageReference(pg);
             	GLSCommunity_DraftController obj = new GLSCommunity_DraftController();
             	System.currentPageReference().getParameters().put('conId',conObj1.Id);
             	GLSCommunity_DraftController obj2 = new GLSCommunity_DraftController();
             	/*User_Access_Management__c uam = GLSLPW_SetUpData.createUAMRecord(u1.Id, prg.Id, null, GLSConfig.ProgramManager);
                ApexPages.currentPage().getParameters().put('conId', conObj1.Id);
                GLSCommunity_UserAccessManagementCtrl obj = new GLSCommunity_UserAccessManagementCtrl();
                obj.selectedProfile = GLSConfig.ProgramManager;
                obj.getUsersForProfile();
                obj.selectedUser = u3.Name;
                obj.getProgramsProtocols();
                obj.lstProgramWrapper.add(new GLSCommunity_UserAccessManagementCtrl.ProgramWrapper(prg2, true));
                obj.addUAMRecords();
                obj.selectedProfile = GLSConfig.ProtocolManager;
                obj.getUsersForProfile();
                obj.selectedUser = u2.Name;
                obj.getProgramsProtocols();
                obj.addUAMRecords();
                obj.deleteUAMRecordId = uam.Id;
                obj.deleteUAMRecord();*/
             }
     	Test.stopTest();   
    }
    static testMethod void TestConstructorWithProgramManager() {
		Account accObj = GLSLPW_SetUpData.createAccount('Amgen');
		Contact conObj1 = GLSLPW_SetUpData.createContact(accObj.Id,'TestName1');
		Contact conObj2 = GLSLPW_SetUpData.createContact(accObj.Id,'TestName2');
		Contact conObj3 = GLSLPW_SetUpData.createContact(accObj.Id,'TestName3');
		Program__c prg = GLSLPW_SetUpData.createProgram(accObj.Id, conObj1.Id, 'TestName', 'Test Ref');
		Program__c prg2 = GLSLPW_SetUpData.createProgram(accObj.Id, conObj1.Id, 'TestName2', 'Test Ref2');
		Protocol__c pro = GLSLPW_SetUpData.createProtocol(prg.Id, 'Open');
		Protocol__c pro2 = GLSLPW_SetUpData.createProtocol(prg.Id, 'Open');
		Protocol__c pro3 = GLSLPW_SetUpData.createProtocol(prg2.Id, 'Open');
				
		Profile p1 = [select id from profile where name=: GLSConfig.ProgramManager];
        User u1 = new User(LastName = 'test user 1', 
                             Username = 'test.user.1@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'testu1',
                             contactId = conObj1.Id, 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = p1.Id, 
                             LanguageLocaleKey = 'en_US'
                             );
        insert u1;
        List<Quote__c> quotelst = new List<Quote__c>();
        Quote__c quote = new Quote__c();
        quote.Quote_Name__c = 'Test Quote';
        quote.Client__c = u1.AccountId;
        quote.Status__c='Draft';
        quote.Contact__c = u1.contactId;
        quote.OwnerId = u1.Id;
        quote.Client_Reference__c ='client reference';
        quote.Quote_Name_GLS__c = 'billing reference';
        quote.Project_Name_Community__c = 'test Quote';
        quotelst.add(quote);
        
        Quote__c quote2 = new Quote__c();
        quote2.Quote_Name__c = 'Test Quote 2';
        quote2.Client__c = u1.AccountId;
        quote2.Status__c='Draft';
        quote2.Contact__c = u1.contactId;
        quote2.OwnerId = u1.Id;
        quote2.Client_Reference__c ='client reference 2';
        quote2.Quote_Name_GLS__c = 'billing reference 2';
        quote2.Project_Name_Community__c = 'test Quote 2';
        quotelst.add(quote2);
        
        insert quotelst;
        
     	Language_List_Item__c defLanguage = new Language_List_Item__c();
        defLanguage.Name = 'English';
        defLanguage.Language_Name__c = 'en-US';
        insert defLanguage;
        
        list<Quote_Assignment__c> lstQuoteAssignment = new list<Quote_Assignment__c>(); 
        Quote_Assignment__c qAsst1 = new Quote_Assignment__c();
        qAsst1.Quote__c = quote.Id; 
        qAsst1.Source_Language__c = defLanguage.Id;     
        qAsst1.Allowed_File_Formats__c = 'MS Word';
        qAsst1.Document_Type__c = 'IFC,SomeData;';
        qAsst1.Keywords__c='one;';        
        lstQuoteAssignment.add(qAsst1);
        insert lstQuoteAssignment;
        
        quote.Last_Visited_Assignment_Id__c = qAsst1.Id;
        quote.Last_Visted_Stage__c = 'GLSCommunity_LanguagePage';
        update quote; 
        
		Test.startTest();
              
             System.runAs (u1){
             	Pagereference pg;    			    	
	    		pg = Page.GLSCommunity_DraftPage;
	    		Test.setCurrentPageReference(pg);
	    		User_Access_Management__c uam = GLSLPW_SetUpData.createUAMRecord(u1.Id, prg.Id, null, GLSConfig.ProgramManager);
             	GLSCommunity_DraftController obj = new GLSCommunity_DraftController();
             	System.currentPageReference().getParameters().put('conId',conObj1.Id);
             	GLSCommunity_DraftController obj2 = new GLSCommunity_DraftController();
             	
             	System.currentPageReference().getParameters().put('quoteID',quote.Id);
             	System.currentPageReference().getParameters().put('redirectToTask','');
        		obj.continueWithQuote();
        		
        		System.currentPageReference().getParameters().put('quoteID',quote.Id);
             	System.currentPageReference().getParameters().put('redirectToTask','continue');
        		obj.continueWithQuote();
        		
        		System.currentPageReference().getParameters().put('quoteType','withProtocol');
             	System.currentPageReference().getParameters().put('projectName','test Quote');
             	System.currentPageReference().getParameters().put('protocol','protocol');
             	System.currentPageReference().getParameters().put('reference','reference');
             	System.currentPageReference().getParameters().put('sponsor','sponsor');
        		obj.openTaskType();
        		
        		obj.deleteProjectId = quote.id;
        		obj.deleteProject();
        		obj.deleteFromAmazon();
        		
        		obj.selectedProtocolId = 'Not In List';
        		obj.populateSponsorAndProgram();
        		
        		obj.selectedProtocolId = pro.id;
        		obj.populateSponsorAndProgram();
        		
        		
             	/*User_Access_Management__c uam = GLSLPW_SetUpData.createUAMRecord(u1.Id, prg.Id, null, GLSConfig.ProgramManager);
                ApexPages.currentPage().getParameters().put('conId', conObj1.Id);
                GLSCommunity_UserAccessManagementCtrl obj = new GLSCommunity_UserAccessManagementCtrl();
                obj.selectedProfile = GLSConfig.ProgramManager;
                obj.getUsersForProfile();
                obj.selectedUser = u3.Name;
                obj.getProgramsProtocols();
                obj.lstProgramWrapper.add(new GLSCommunity_UserAccessManagementCtrl.ProgramWrapper(prg2, true));
                obj.addUAMRecords();
                obj.selectedProfile = GLSConfig.ProtocolManager;
                obj.getUsersForProfile();
                obj.selectedUser = u2.Name;
                obj.getProgramsProtocols();
                obj.addUAMRecords();
                obj.deleteUAMRecordId = uam.Id;
                obj.deleteUAMRecord();*/
             }
     	Test.stopTest();   
    }
        static testMethod void TestConstructorWithProtocolManager() {
		Account accObj = GLSLPW_SetUpData.createAccount('Amgen');
		Contact conObj1 = GLSLPW_SetUpData.createContact(accObj.Id,'TestName1');
		Contact conObj2 = GLSLPW_SetUpData.createContact(accObj.Id,'TestName2');
		Contact conObj3 = GLSLPW_SetUpData.createContact(accObj.Id,'TestName3');
		Program__c prg = GLSLPW_SetUpData.createProgram(accObj.Id, conObj1.Id, 'TestName', 'Test Ref');
		Program__c prg2 = GLSLPW_SetUpData.createProgram(accObj.Id, conObj1.Id, 'TestName2', 'Test Ref2');
		Protocol__c pro = GLSLPW_SetUpData.createProtocol(prg.Id, 'Open');
		Protocol__c pro2 = GLSLPW_SetUpData.createProtocol(prg.Id, 'Open');
		Protocol__c pro3 = GLSLPW_SetUpData.createProtocol(prg2.Id, 'Open');
				
		Profile p1 = [select id from profile where name=: GLSConfig.ProtocolManager];
        User u1 = new User(LastName = 'test user 1', 
                             Username = 'test.user.1@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'testu1',
                             contactId = conObj1.Id, 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = p1.Id, 
                             LanguageLocaleKey = 'en_US'
                             );
        insert u1;
        List<Quote__c> quotelst = new List<Quote__c>();
        Quote__c quote = new Quote__c();
        quote.Quote_Name__c = 'Test Quote';
        quote.Client__c = u1.AccountId;
        quote.Status__c='Draft';
        quote.Contact__c = u1.contactId;
        quote.OwnerId = u1.Id;
        quote.Client_Reference__c ='client reference';
        quote.Quote_Name_GLS__c = 'billing reference';
        quote.Project_Name_Community__c = 'test Quote';
        quotelst.add(quote);
        
        Quote__c quote2 = new Quote__c();
        quote2.Quote_Name__c = 'Test Quote 2';
        quote2.Client__c = u1.AccountId;
        quote2.Status__c='Draft';
        quote2.Contact__c = u1.contactId;
        quote2.OwnerId = u1.Id;
        quote2.Client_Reference__c ='client reference 2';
        quote2.Quote_Name_GLS__c = 'billing reference 2';
        quote2.Project_Name_Community__c = 'test Quote 2';
        quotelst.add(quote2);
        
        insert quotelst;
        
        /*Profile p2 = [select id from profile where name=: GLSConfig.ProtocolManager];
        User u2 = new User(LastName = 'test user 2', 
                             Username = 'test.user.2@example.com', 
                             Email = 'test.2@example.com', 
                             Alias = 'testu2',
                             contactId = conObj2.Id, 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = p2.Id, 
                             LanguageLocaleKey = 'en_US'
                             );
        insert u2;
        */
        /*Profile p3 = [select id from profile where name=: GLSConfig.ProgramManager];
        User u3 = new User(LastName = 'test user 3', 
                             Username = 'test.user.3@example.com', 
                             Email = 'test.3@example.com', 
                             Alias = 'testu3',
                             contactId = conObj3.Id, 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = p3.Id, 
                             LanguageLocaleKey = 'en_US'
                             );
        insert u3;
        */
        
		Test.startTest();
              
             System.runAs (u1){
             	Pagereference pg;    			    	
	    		pg = Page.GLSCommunity_DraftPage;
	    		Test.setCurrentPageReference(pg);
	    		User_Access_Management__c uam = GLSLPW_SetUpData.createUAMRecord(u1.Id, prg.Id, pro.Id, GLSConfig.ProgramManager);
             	GLSCommunity_DraftController obj = new GLSCommunity_DraftController();
             	System.currentPageReference().getParameters().put('conId',conObj1.Id);
             	GLSCommunity_DraftController obj2 = new GLSCommunity_DraftController();
             	/*User_Access_Management__c uam = GLSLPW_SetUpData.createUAMRecord(u1.Id, prg.Id, null, GLSConfig.ProgramManager);
                ApexPages.currentPage().getParameters().put('conId', conObj1.Id);
                GLSCommunity_UserAccessManagementCtrl obj = new GLSCommunity_UserAccessManagementCtrl();
                obj.selectedProfile = GLSConfig.ProgramManager;
                obj.getUsersForProfile();
                obj.selectedUser = u3.Name;
                obj.getProgramsProtocols();
                obj.lstProgramWrapper.add(new GLSCommunity_UserAccessManagementCtrl.ProgramWrapper(prg2, true));
                obj.addUAMRecords();
                obj.selectedProfile = GLSConfig.ProtocolManager;
                obj.getUsersForProfile();
                obj.selectedUser = u2.Name;
                obj.getProgramsProtocols();
                obj.addUAMRecords();
                obj.deleteUAMRecordId = uam.Id;
                obj.deleteUAMRecord();*/
             }
     	Test.stopTest();   
    }
}