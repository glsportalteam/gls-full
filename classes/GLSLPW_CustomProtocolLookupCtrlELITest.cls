/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSLPW_CustomProtocolLookupCtrlELITest {

     static testMethod void testELIProtocolLookup() {
    	
        Account accObj 						= GLSLPW_SetUpData.createAccount('Amgen');
        Contact conObj 						= GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
        Program__c prgObj 					= GLSLPW_SetUpData.createProgram(accObj.Id,conObj.Id,'Program_Test', 'Amgen');
        Protocol__c protObj 				= GLSLPW_SetUpData.createProtocol(prgObj.Id,'Draft');
        Estimate__c estObj					= GLSLPW_SetUpData.createEstimate(prgObj.Id, 'Draft');
        Estimate_Line_Item__c eliObj		= GLSLPW_SetUpData.createELI(protObj.Id, estObj.Id);
        ApexPages.currentPage().getParameters().put('pid', prgObj.Id);
        ApexPages.currentPage().getParameters().put('estId', estObj.Id);
        ApexPages.currentPage().getParameters().put('lksrch', 'Test String');
        Test.startTest();
	        GLSLPW_CustomProtocolLookupELIController obj  = new GLSLPW_CustomProtocolLookupELIController();
	        system.assert(obj.searchString != '');
	        obj.search();
	      	obj.getFormTag();
	        obj.getTextBox();
	        ApexPages.currentPage().getParameters().put('lksrch', '');
	        GLSLPW_CustomProtocolLookupELIController obj2  = new GLSLPW_CustomProtocolLookupELIController();
        Test.stopTest();
        }
}