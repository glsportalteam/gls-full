/** 
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSLPW_EstimateControllerExtTest {
    public static User u;
    static testMethod void setUpTestData() {
        Account accObj                      = GLSLPW_SetUpData.createAccount('Amgen');
        Contact conObj                      = GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
        Program__c prgObj                   = GLSLPW_SetUpData.createProgram(accObj.Id,conObj.Id,'Program_Test', 'Amgen');
        Protocol__c protObj                 = GLSLPW_SetUpData.createProtocol(prgObj.Id, 'Open');
        Estimate__c estObj                  = GLSLPW_SetUpData.createEstimate(prgObj.Id, 'Draft');
        AWS_File__c  awsObj                 = GLSLPW_SetUpData.createAWSFile('testPath','gls-us','testFile','testRecordType','testVersion',1);
        Estimate_Files__c estFileObj        = GLSLPW_SetUpData.createEstimateFile('Estimate', estObj.Id, awsObj.Id);
        Estimate_Line_Item__c eliObj        = GLSLPW_SetUpData.createELI(protObj.Id, estObj.Id);
        Estimate_Line_Item__c eliObj1       = GLSLPW_SetUpData.createELI(protObj.Id, estObj.Id);
        
        Country__c country                  = GLSLPW_SetUpData.createCountry('Test Country');
        Estimate_Line_Country__c elcObj1    = GLSLPW_SetUpData.createELC(6, country.Id, eliObj.Id);
        Estimate_Line_Country__c elcObj2    = GLSLPW_SetUpData.createELC(10,country.Id, eliObj.Id);
        Estimate_Line_Country__c elcObj3    = GLSLPW_SetUpData.createELC(2, country.Id, eliObj1.Id);
        Estimate__c estDummy                = GLSLPW_SetUpData.createEstimate(prgObj.Id, 'Draft');
        
        Date closeDate                      = system.today() + 30; 
        Opportunity oppObj                  = GLSLPW_SetUpData.createOpportunity(conObj.Id,accObj.Id,'Test Opp','B-Goals Shared',closeDate);
                oppObj.Amount               = estObj.Estimate_Amount__c;
                oppObj.Industry__c          = estObj.Program_Id__r.Account__r.Industry;
                oppObj.Study_Sponsor__c     = estObj.Program_Id__r.Sponsor__r.Name;
                oppObj.TherapeuticArea__c   = estObj.Program_Id__r.Therapeutic_Area__c;
                oppObj.Primary_Estimate__c  = estObj.Id;
        update oppObj;
        
        estObj.Opportunity__c               = oppObj.Id;
        estObj.isChangeOrder__c             = true;
        estObj.isEstimateRevision__c        = true;
        estObj.isEstimateVersion__c         = true;
        estObj.Is_Primary__c 				= true;
        update estObj;
      }
      
    static testMethod void testNotNullId(){
        setUpTestData();
        List<Estimate__c> lstEstObj         = [ SELECT id,name FROM Estimate__c ];
        Estimate__c estObj1                 = lstEstObj[0];
        Estimate__c estDummy                = lstEstObj[1];
        
        estObj1.Status__c                   ='Reviewed';
        update estObj1;
        Estimate_Line_Item__c eliObj        = [ SELECT id,name FROM Estimate_Line_Item__c LIMIT 1 ];
        Estimate_Files__c estFileObj        = [ SELECT id,name FROM Estimate_Files__c LIMIT 1 ];
        
        Profile p                           = [select id,Name FROM profile WHERE name=: 'Contracts Manager'];
        Estimate_Files_Visibility__c efvObj = new Estimate_Files_Visibility__c(Name = 'Contracts Manager');
        insert efvObj;
                u = new User(LastName = 'test user 1', 
                             Username = 'test.user.1@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'testu1', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = p.Id, 
                             LanguageLocaleKey = 'en_US',
                             Estimate_Files_Visible__c         = true);     
        Opportunity oppObj              = [ SELECT id,name,Amount FROM Opportunity LIMIT 1];    
                                      
        Test.startTest();
            ApexPages.currentPage().getParameters().put('Id', estObj1.Id);
         
            system.runAs(u){
            GLSLPW_EstimateControllerExt obj1 = new GLSLPW_EstimateControllerExt(new ApexPages.StandardController(estObj1));
            obj1.efileId = estFileObj.id;
            obj1.estLineId = eliObj.Id; 
            system.assert(obj1.efileId!=null);
            obj1.estimateType = 'EstimateRevision';

            List<SelectOption> lstSubmitForValueTest = obj1.getSubmitForValues();
            obj1.cloneEstimate();
            obj1.estimateType = ' ';
            obj1.cloneEstimate();
            obj1.estimateType = 'ChangeOrder';
            obj1.cloneType = 'Clone';
            obj1.cloneEstimate();
            obj1.estimateType = 'EstimateVersion';
            obj1.cloneType = 'CloneFilesWithFiles';
            obj1.cloneEstimate();
            
            obj1.cloneEstLineItems(estObj1.Id,estDummy.Id);
            obj1.redirectToCloneRecord();
            obj1.CloneEstFiles(estObj1.Id, estDummy.Id);
            List<SelectOption> lstClientDecisionValuesTest = obj1.getClientDecisionValues();
            obj1.getEstRefFiles();
            obj1.fetchEstimateFile();
            
            obj1.estReason = 'Test Reason';
            obj1.comments = 'Test Comments';
            obj1.declineEstimate();
            obj1.flag = true;
            obj1.submitValue = 'EstimateTracker';
            obj1.submitForProcess();
            obj1.submitValue = 'InternalApproval';
            obj1.submitForProcess();
            obj1.submitValue = 'ReleaseToClient';
            obj1.submitForProcess();
            
            obj1.saveELIAmount();
            obj1.deleteEstimateFile();
            estObj1.Status__c ='Reviewed';
            obj1.deleteELIRecord();
            obj1.obsoleteEstimate();
            obj1.deleteOpportunity();
            obj1.customSave();
            obj1.customEdit();
            obj1.saveAll();
            obj1.customCancel();
            }
         Test.stopTest();
    }
    static testMethod void testNullId(){
        setUpTestData();
        List<Estimate__c> lstEstObj         = [ SELECT id,name FROM Estimate__c ];
        Estimate__c estObj1                 = lstEstObj[0];
        estObj1.Status__c                   ='New';
        update estObj1;
        
        Estimate__c estDummy                = lstEstObj[1];
        estDummy.Original_Estimate__c       = estObj1.Id;
        update estDummy;
        
        Estimate_Files__c estFileObj        = [ SELECT id,name FROM Estimate_Files__c LIMIT 1 ];
       
        Test.startTest();
            ApexPages.currentPage().getParameters().put('Id', null);
            GLSLPW_EstimateControllerExt obj1 = new GLSLPW_EstimateControllerExt(new ApexPages.StandardController(estObj1));
            obj1.efileId = estFileObj.id;
            system.assert(obj1.efileId!=null);
            obj1.estimateType = 'ChangeOrder';
            obj1.flag = true;
            obj1.cloneEstimate();
            obj1.cloneEstLineItems(estObj1.Id,estDummy.Id);
            obj1.redirectToCloneRecord();
            obj1.CloneEstFiles(estObj1.Id, estDummy.Id);
            obj1.copyEstimateFiles();
            estObj1.Status__c ='Reviewed';
            update estObj1;
            obj1.deleteEstimateFile();
            estObj1.Status__c ='Released';
            update estObj1;
            obj1.obsoleteEstimate();
            obj1.deleteOpportunity();
        Test.stopTest();
    }
    
    static testMethod void testEstTrigger(){
        setUpTestData();
        List<Estimate__c> lstEstObj     = [ SELECT id,name,Opportunity__c,Estimate_Amount__c,Status__c  FROM Estimate__c];
        Estimate__c estObj              = lstEstObj[0]; 
        Estimate__c estObj1             = lstEstObj[1]; 
        estObj1.Status__c = 'Approved';
        update estObj1;
        Estimate_Line_Item__c eliObj    = [ SELECT id,name FROM Estimate_Line_Item__c LIMIT 1 ];
        eliObj.Amount__c=5000;
        update eliObj;
        Opportunity oppObj              = [ SELECT id,name,Amount FROM Opportunity LIMIT 1];
        Test.startTest();
            estObj.Is_Primary__c = true;
            estObj.Status__c = 'Approved';
            estObj.Reason_for_Cancellation__c = 'Overall Costs';
            estObj.Original_Estimate__c = estObj1.Id;
            update estObj;
            estObj.Status__c = 'Cancelled';
            estObj.Reason_for_Cancellation__c = 'Turnaround time';
            update estObj;
            
        Test.stopTest();
        }
}