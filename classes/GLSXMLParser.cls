/* Copyright (c) 2013, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSXMLParser  Class
* Function: Parse Analysis file xml data and populate statistic data in the Quote line item record
*/

public class GLSXMLParser {

    public String parsedText {get; set;}
  //  public  Map<Id,Map<string,Quote_Line_Object_Assignment_File__c>> mapQLIAF= new Map<Id,map<string,Quote_Line_Object_Assignment_File__c>>();
    public  Map<Id,string> mapQLI= new Map<Id,string>();
    public list<Quote_Line_Object_Assignment_File__c> lstQuoteAssignFile= new list<Quote_Line_Object_Assignment_File__c>();
    public set<Quote_Line_Object_Assignment_File__c> setQuoteAssignFile = new set<Quote_Line_Object_Assignment_File__c>();
    public Quote_Line_Object_Assignment_File__c qliaf;
    public list<string> lstFileName= new list<string>();
    public integer counter=0;
        /**
        *   @Description : This method parse xml data
        *   @Param : List of Quote line item 
        **/   
        
    public String parser(String qliId, String quoteId, String URL, String errorMessage, String taskStatus, String xmlData, String analyzedFor) {
        List<Quote_Line_Object_Assignment_File__c> lstQlaf= [ Select id,Name,Source_File_name__r.File_Name__c,Quote_Line_Item__c,IsAnalysisProcessed__c from Quote_Line_Object_Assignment_File__c where Quote_Line_Item__c =:qliId]; 
        map<string,Quote_Line_Object_Assignment_File__c> mapTemp;
        map<String,String> mapQuoteStatus = new map<String,String>();
        List<Quote__c> lstquoteUpdate = new List<Quote__c>();
        String quoteStatus ='';
         
        mapTemp= new map<string,Quote_Line_Object_Assignment_File__c>();
        for(Quote_Line_Object_Assignment_File__c assign: lstQlaf){
             mapTemp.put(assign.Source_File_name__r.File_Name__c,assign);
       }   
        
        if(xmlData != '') {
                parsedText = parse(xmlData,qliId,URL,errorMessage,taskStatus,mapTemp);
                system.debug('---parsedText----: '+parsedText);
                if(analyzedFor.equalsIgnoreCase('analyzedSingleQLI')){
                    if(parsedText.equalsIgnoreCase('Success') || parsedText.equalsIgnoreCase('Error')){
                        boolean flagInProcess=false;
                        boolean flagInError=false;
                        List<Quote_Line_Item__c> lstupdatedQli = [Select Id, QuoteID__c,Process_Status__c From Quote_Line_Item__c where QuoteID__c =: quoteId];
                            for(Quote_Line_Item__c Qli: lstupdatedQli){
                                    if(Qli.Process_Status__c.equalsIgnoreCase('In Process'))
                                        flagInProcess = true;
                                    if(Qli.Process_Status__c.equalsIgnoreCase('Error'))
                                        flagInError= true;
                            }   
                        if(!flagInProcess){
                            Quote__c qObj= new Quote__c(id=quoteId);
                                if(flagInError)
                                    qObj.Status__c = GLSConfig.InCompleteStat;
                                else
                                    qObj.Status__c = GLSConfig.InPrepStat;
                            lstquoteUpdate.add(qObj);
                        }
                        if (lstquoteUpdate != null && lstquoteUpdate.size()>0){
                            try{
                                Database.update(lstquoteUpdate);
                            }catch (DMLException dml){
                                system.debug(GLSConfig.DMLExceptionOccured+'---'+dml.getMessage());
                            }
                        }
                    }
                }
        } else parsedText=null;     
     system.debug('----parsedText----: '+parsedText);
     if(parsedText.equalsIgnoreCase('Success') || parsedText.equalsIgnoreCase('Error'))
        return parsedText;
     else return null;
    }
       
      /**
       *   @Description : This method parse xml data
       *   @Param : string xml data,quote Id,map of filename and Quote Line Object Assignment File
       **/
    public String parse(String toParse,Id quoteId,String URL, String errorMessage, String taskStatus, map<string,Quote_Line_Object_Assignment_File__c> mapQuoteAssignment) {
        system.debug('----toParse----' +toParse);
        DOM.Document doc = new DOM.Document();
        if(toParse != null && toParse != ''){
            try {
                String Status ='';
                doc.load(toParse);    
                DOM.XMLNode root = doc.getRootElement();
                String wlkthrough = walkThrough(root,quoteId,URL,errorMessage,taskStatus,mapQuoteAssignment); 
                system.debug('----wlkthrough----' +wlkthrough);
                Status = [select Process_Status__c from Quote_Line_Item__c where Id =:quoteId].Process_Status__c;
                system.debug('----Status----' +Status+'--'+quoteId);
                return Status;
            }catch (System.XMLException e) {  // invalid XML
                system.debug(GLSConfig.ExceptionOccured+'---'+e.getMessage());
                Quote_Line_Item__c qliErrUpdate = new Quote_Line_Item__c(id=quoteId);
                qliErrUpdate.isProcessed__c = false;
                update qliErrUpdate;
                return e.getMessage();
            }
        }
        return null;
    }
        /**
        *   @Description : This method recursively walk through the XML
        *   @Param : DOM>XMLNode,Quote Id,map of filename and Quote Line Object Assignment File
        **/
        
    private String walkThrough(DOM.XMLNode node,Id quoteId,String URL, String errorMessage, String taskStatus, map<string,Quote_Line_Object_Assignment_File__c> mapQuoteAssignment) {
      String result = '\n';
      String perfect,inContextExact,exact,repeated,crossFileRepeated,total,noMatch,X50_74WordCount,X75_84WordCount,X85_94WordCount,X95_99WordCount;
     
      if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
        if(node.getName() == 'batchTotal'){
            for (Dom.XMLNode child: node.getChildElements()) {
                  if(child.getName() == 'analyse'){ 
                      for(Dom.XMLNode childRequired: child.getChildElements()){ 
                             if(childRequired.getName() == 'perfect'){
                                if (childRequired.getAttributeCount() > 0) { 
                                for (Integer i = 0; i< childRequired.getAttributeCount(); i++ ) {
                                      if(childRequired.getAttributeKeyAt(i)=='words')
                                       perfect=childRequired.getAttributeValue(childRequired.getAttributeKeyAt(i), childRequired.getAttributeKeyNsAt(i));
                                    }  
                                }
                           }
                            if(childRequired.getName() == 'inContextExact'){
                                if (childRequired.getAttributeCount() > 0) { 
                                for (Integer i = 0; i< childRequired.getAttributeCount(); i++ ) {
                                      if(childRequired.getAttributeKeyAt(i)=='words')
                                      inContextExact=childRequired.getAttributeValue(childRequired.getAttributeKeyAt(i), childRequired.getAttributeKeyNsAt(i));
                                    }  
                                }
                           }
                            if(childRequired.getName() == 'exact'){
                                if (childRequired.getAttributeCount() > 0) { 
                                for (Integer i = 0; i< childRequired.getAttributeCount(); i++ ) {
                                      if(childRequired.getAttributeKeyAt(i)=='words')
                                      exact=childRequired.getAttributeValue(childRequired.getAttributeKeyAt(i), childRequired.getAttributeKeyNsAt(i));
                                    }  
                                }
                           }
                           if(childRequired.getName() == 'crossFileRepeated'){
                                if (childRequired.getAttributeCount() > 0) { 
                                for (Integer i = 0; i< childRequired.getAttributeCount(); i++ ) {
                                      if(childRequired.getAttributeKeyAt(i)=='words')
                                      crossFileRepeated=childRequired.getAttributeValue(childRequired.getAttributeKeyAt(i), childRequired.getAttributeKeyNsAt(i));
                                    }  
                                }
                           }
                           if(childRequired.getName() == 'repeated'){
                                if (childRequired.getAttributeCount() > 0) { 
                                for (Integer i = 0; i< childRequired.getAttributeCount(); i++ ) {
                                      if(childRequired.getAttributeKeyAt(i)=='words')
                                      repeated=childRequired.getAttributeValue(childRequired.getAttributeKeyAt(i), childRequired.getAttributeKeyNsAt(i));
                                    }  
                                }
                           }
                           if(childRequired.getName() == 'total'){
                                if (childRequired.getAttributeCount() > 0) { 
                                for (Integer i = 0; i< childRequired.getAttributeCount(); i++ ) {
                                      if(childRequired.getAttributeKeyAt(i)=='words')
                                      total=childRequired.getAttributeValue(childRequired.getAttributeKeyAt(i), childRequired.getAttributeKeyNsAt(i));
                                    }  
                                }
                           }
                           if(childRequired.getName() == 'new'){
                                if (childRequired.getAttributeCount() > 0) { 
                                for (Integer i = 0; i< childRequired.getAttributeCount(); i++ ) {
                                      if(childRequired.getAttributeKeyAt(i)=='words')
                                      noMatch=childRequired.getAttributeValue(childRequired.getAttributeKeyAt(i), childRequired.getAttributeKeyNsAt(i));
                                    }  
                                }
                           }
                           if(childRequired.getName() == 'fuzzy' && childRequired.getAttributeKeyAt(0)=='min' && childRequired.getAttributeValue(childRequired.getAttributeKeyAt(0), childRequired.getAttributeKeyNsAt(0))=='50'){
                                        X50_74WordCount=childRequired.getAttributeValue(childRequired.getAttributeKeyAt(3), childRequired.getAttributeKeyNsAt(3));
                            }
                           if(childRequired.getName() == 'fuzzy' && childRequired.getAttributeKeyAt(0)=='min' && childRequired.getAttributeValue(childRequired.getAttributeKeyAt(0), childRequired.getAttributeKeyNsAt(0))=='75'){
                                        X75_84WordCount=childRequired.getAttributeValue(childRequired.getAttributeKeyAt(3), childRequired.getAttributeKeyNsAt(3));
                            }
                           if(childRequired.getName() == 'fuzzy' && childRequired.getAttributeKeyAt(0)=='min' && childRequired.getAttributeValue(childRequired.getAttributeKeyAt(0), childRequired.getAttributeKeyNsAt(0))=='85'){
                                        X85_94WordCount=childRequired.getAttributeValue(childRequired.getAttributeKeyAt(3), childRequired.getAttributeKeyNsAt(3));
                            }
                           if(childRequired.getName() == 'fuzzy' && childRequired.getAttributeKeyAt(0)=='min' && childRequired.getAttributeValue(childRequired.getAttributeKeyAt(0), childRequired.getAttributeKeyNsAt(0))=='95'){
                                        X95_99WordCount=childRequired.getAttributeValue(childRequired.getAttributeKeyAt(3), childRequired.getAttributeKeyNsAt(3));
                            }
                      }
                }
            }
                system.debug('----counter----' +counter);
                Quote_Line_Item__c qliUpdate = new Quote_Line_Item__c(id=quoteId);
               
                if( counter != mapQuoteAssignment.size()){
                    lstQuoteAssignFile= new list<Quote_Line_Object_Assignment_File__c>();
                    setQuoteAssignFile= new set<Quote_Line_Object_Assignment_File__c>();
                    qliUpdate.Task_Status__c='Warning';
                    qliUpdate.Process_Status__c = 'Error';
                    qliUpdate.Error_Message__c='Files in Analysis File does not match with the files in the Quote Line Assignment Files';
                  //  qliUpdate.isReadyForAnalyse__c= true;
                    qliUpdate.isAnalyzed__c = false;       
                    for(string str:lstFileName){
                        if(mapQuoteAssignment.get(str) != null){
                             qliaf = new Quote_Line_Object_Assignment_File__c(id = mapQuoteAssignment.get(str).id);
                             qliaf.IsAnalysisProcessed__c=true;
                         //    qliaf.Quote_Line_Item__c = qliUpdate.Id;
                             setQuoteAssignFile.add(qliaf); 
                         }                       
                    }
                }else{
                    qliUpdate.Task_Status__c = taskStatus;
                    qliUpdate.Process_Status__c = 'Success';
                    qliUpdate.Error_Message__c = errorMessage;
                    qliUpdate.isAnalyzed__c = true;    
                }
                
                list<Quote_Line_Item__c> lstQLI = [Select Id, Target_Language_ID__r.Name From Quote_Line_Item__c Where Id =: qliUpdate.Id limit 1];
                String targetLanguage = '';
                if(lstQLI.size()> 0)
                {
                  targetLanguage = lstQLI[0].Target_Language_ID__r.Name;
                }
                
                if(targetLanguage.contains('English'))
                {
                  Decimal totals = Decimal.valueOf(total);
                  qliUpdate.Actual_Words__c = Math.ceil(totals + (totals*0.15));
                  qliUpdate.Back_Translation_Total_Words__c = Decimal.valueOf(total);
                  
                }else
                {
                  qliUpdate.Actual_Words__c=Decimal.valueOf(total);
                  Decimal totals = Decimal.valueOf(total);
                  qliUpdate.Back_Translation_Total_Words__c = Math.ceil(totals + (totals*0.15));
                }           
                qliUpdate.TWC__c = Decimal.valueOf(total);    
                qliUpdate.Repetitions_Word_Count__c=Decimal.valueOf(repeated);
                qliUpdate.Context_TM_Word_Count__c=Decimal.valueOf(inContextExact);
                qliUpdate.No_Match_Word_Count__c=Decimal.valueOf(noMatch);
                qliUpdate.Perfect_Match__c=Decimal.valueOf(perfect);
                qliUpdate.Crossfile_Repetitions__c=Decimal.valueOf(crossFileRepeated);
                qliUpdate.X100_Word_Count__c=Decimal.valueOf(exact);
                qliUpdate.X50_74_Word_Count__c=Decimal.valueOf(X50_74WordCount);
                qliUpdate.X75_84_Word_Count__c=Decimal.valueOf(X75_84WordCount);
                qliUpdate.X85_95_Word_Count__c=Decimal.valueOf(X85_94WordCount);
                qliUpdate.X95_99_Word_Count__c=Decimal.valueOf(X95_99WordCount);
                
                /* line 220 to 224 for solving 255 characters limit */
                if(URL.length() >= 255){
                    URL = URL.substring(0, 254);
                }
                
                
                qliUpdate.Analyzed_File_URL__c = URL;
                    
                    
                if(!setQuoteAssignFile.isEmpty()) { 
                    lstQuoteAssignFile.addAll(setQuoteAssignFile);  
                    system.debug('----lstQuoteAssignFile----' +lstQuoteAssignFile);
                    if(lstQuoteAssignFile != null && lstQuoteAssignFile.size()>0){
                            try{
                                Database.update(lstQuoteAssignFile);
                            }catch(DMLException de){
                                system.debug(GLSConfig.DMLExceptionOccured+'-'+de.getMessage());
                            }
                    } 
                }
                try{
                    Database.update(qliUpdate);  
                    counter=0;
                    lstQuoteAssignFile= new list<Quote_Line_Object_Assignment_File__c>();
                }catch(DMLException de){
                    system.debug(GLSConfig.DMLExceptionOccured+'---'+de.getMessage());
                }catch(Exception e){
                    system.debug(GLSConfig.ExceptionOccured+'---'+e.getMessage());
                }
            }
         else if(node.getName() == 'file'){
             counter++;
             if (node.getAttributeCount() > 0) { 
                for (Integer i = 0; i< node.getAttributeCount(); i++ ) {
                      if(node.getAttributeKeyAt(i)=='name'){
                        string fileName=node.getAttributeValue(node.getAttributeKeyAt(i), node.getAttributeKeyNsAt(i));
                        lstFileName.add(fileName.substring(0,fileName.lastIndexOf('.')));
                        if(mapQuoteAssignment.get(fileName.substring(0,fileName.lastIndexOf('.'))) != null){
                            qliaf= new Quote_Line_Object_Assignment_File__c();
                            qliaf = new Quote_Line_Object_Assignment_File__c(id = mapQuoteAssignment.get(fileName.substring(0,fileName.lastIndexOf('.'))).id);
                            qliaf.IsAnalysisProcessed__c=true;
                            lstQuoteAssignFile.add(qliaf);
                         }
                         //qliaf.IsAnalysisProcessed__c=true;
                     }   
                 }                  
              }  
              for (Dom.XMLNode child: node.getChildElements()) {
                walkThrough(child,quoteId,URL,errorMessage,taskStatus,mapQuoteAssignment);
              } 
            } 
        else{ 
            for (Dom.XMLNode child: node.getChildElements()) {
                 walkThrough(child,quoteId,URL,errorMessage,taskStatus,mapQuoteAssignment);
            }
        }
       return result;
      }
     return '';  //should never reach here
    }
}