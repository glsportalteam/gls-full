/* CustomContactLookupControllerTest
* Unit test cases for CustomContactLookupController
*
* 2014-05-15: / Zach Ries - 121 communications - Create test cases for CustomContactLookupController
*/
@isTest
private class CustomContactLookupControllerTest {

	// Test searching database from searchString with no account and valid contact
	static testMethod void testSearchNoAccountValidContact() {
		List<Contact> contacts = GenerateTestContacts(); 
		test.startTest();
			ApexPages.currentPage().getParameters().put('lksrch','FirstName 1');
			CustomContactLookupController cont = new CustomContactLookupController();
			System.assertEquals(1, cont.results.size());  	
       	test.stopTest();
	}
	
	// Test searching database from searchString with no account and invalid contact
	static testMethod void testSearchNoAccountInvalidContact() {
		List<Contact> contacts = GenerateTestContacts(); 
		test.startTest();
			ApexPages.currentPage().getParameters().put('lksrch','FirstName X');
			CustomContactLookupController cont = new CustomContactLookupController();
			System.assertEquals(0, cont.results.size());  	
       	test.stopTest();
	}
	
	// Test searching database from searchString with account and valid contact
	static testMethod void testSearchWithAccountValidContact() {
		List<Contact> contacts = GenerateTestContacts(); 
		test.startTest();
			ApexPages.currentPage().getParameters().put('lksrch','FirstName 1');
			ApexPages.currentPage().getParameters().put('account',contacts[1].AccountId);
			CustomContactLookupController cont = new CustomContactLookupController();
			System.assertEquals(1, cont.results.size());  	
       	test.stopTest();
	}
	
	// Test searching database from searchString with no account and invalid contact
	static testMethod void testSearchWithAccountInvalidContact() {
		List<Contact> contacts = GenerateTestContacts(); 
		test.startTest();
			ApexPages.currentPage().getParameters().put('lksrch','FirstName 2');
			ApexPages.currentPage().getParameters().put('account',contacts[1].AccountId);
			CustomContactLookupController cont = new CustomContactLookupController();
			System.assertEquals(0, cont.results.size());  	
       	test.stopTest();
	}
	
	// Test searching database with no search string
	static testMethod void testSearchWithAccountValidContactNoSearchString() {
		List<Contact> contacts = GenerateTestContacts(); 
		test.startTest();
			ApexPages.currentPage().getParameters().put('account',contacts[1].AccountId);
			CustomContactLookupController cont = new CustomContactLookupController();
			System.assertEquals(1, cont.results.size());  	
       	test.stopTest();
	}
	
	// Test searching database from searchString with no account and valid contact
	static testMethod void testSearchFunction() {
		List<Contact> contacts = GenerateTestContacts(); 
		test.startTest();
			CustomContactLookupController cont = new CustomContactLookupController();
			cont.searchString = 'FirstName 1';
			cont.search();
			System.assertEquals(1, cont.results.size());  	
       	test.stopTest();
	}
	
	static testMethod void testGetFormTag() {
		test.startTest();
			String testValue = 'testvalue';
			ApexPages.currentPage().getParameters().put('frm',testValue);
			CustomContactLookupController cont = new CustomContactLookupController();
			System.assertEquals(testValue, cont.getFormTag());  	
       	test.stopTest();
	}
	
	static testMethod void testGetTextBox() {
		test.startTest();
			String testValue = 'testvalue';
			ApexPages.currentPage().getParameters().put('txt',testValue);
			CustomContactLookupController cont = new CustomContactLookupController();
			System.assertEquals(testValue, cont.getTextBox());  	
       	test.stopTest();
	}
	
	static List<Contact> GenerateTestContacts() {
        
        List<Account> actList = new List<Account>();
        for(Integer i = 0; i < 5; i++) {
             Account testAccount = new Account( Name = 'Company ' + i );
            actList.add(testAccount);
        }
        insert actList;
        
    	// Create Contacts
    	List<Contact> contactList = new List<Contact>();
     	for(Integer i = 0; i < 5; i++) {
        	Contact testContact = new Contact(AccountId = actList[i].Id,
                                FirstName='FirstName ' + i,
                                LastName='LastName ' + i,
                                MailingStreet='MailingStreet ' + i,
                                MailingCity='MailingCity ' + i,
                                MailingState='S' + i,
                                MailingPostalCode='PC ' + i,
                                Title='Title ' + i,
                                Salutation='Salutation ' + i,
                                Phone='Phone ' + i,
                                Email= 'test' + i + '@glstest.com');
			contactList.add(testContact);
     	}
        insert contactList;
        
        List<Contact> contacts = [select 
                                    Contact.Id, Contact.Account.Name, Contact.Name,
                                    Contact.MailingStreet, Contact.MailingCity, Contact.MailingState,
                                    Contact.MailingPostalCode, Contact.Title, Contact.Salutation,
                                    Contact.Phone, Contact.LastActivityDate, Contact.FirstName,
                                    Contact.LastName, Contact.Email, Contact.AccountId
                                from 
                                    contact
                                where
                                    Contact.Email LIKE '%@glstest.com'];
        return contacts;
    }
}