@isTest(SeeAllData=true)
public class RHX_TEST_Quote_Line_Item_Subline {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM Quote_Line_Item_Subline__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new Quote_Line_Item_Subline__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}