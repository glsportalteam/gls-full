@isTest
public with sharing class GLSCommunity_ShareFileControllerTest {
	static testMethod void unitTest(){
		profile p =[select id from profile where Name =: GLSConfig.Communityprofile limit 1];   
	    Account acc = new Account();
	    acc.name = 'Test Account';
	    acc.BillingStreet = 'Sample';
	    acc.BillingCity = 'Sample';
	    acc.BillingState = 'Sample';
	    acc.BillingStreet = 'Sample';
	    acc.BillingPostalCode='Sample';
	    acc.BillingCountry='Sample';
	    insert acc;
	    
	    List<Contact> conLst =  new List<Contact>();
	    Contact cont = new Contact();
	    cont.AccountId = acc.Id;
	    cont.FirstName= 'Test';
	    cont.LastName = 'Contact';
	    cont.Email = 'test@GLS.com';	    
	    cont.isPortalActivated__c = true;
	    conLst.add(cont);
	    
	    Contact cont2 = new Contact();
	    cont2.AccountId = acc.Id;
	    cont2.FirstName= 'Test2';
	    cont2.LastName = 'Contact2';
	    cont2.Email = 'test2@GLS.com';
	    conLst.add(cont2);
	    
	    Contact cont3 = new Contact();
	    cont3.AccountId = acc.Id;
	    cont3.FirstName= 'Test3';
	    cont3.LastName = 'Contact3';
	    cont3.Email = 'test@GLS.com';
	    conLst.add(cont3);
	    
	    insert conLst; 
	    
	    User urs = new User();
	    urs.FirstName='test';
	    urs.LastName='test';
	    urs.Email='test@test.com';
	    urs.Alias='abcd';
	    urs.ContactId = cont.id;
	    urs.Username='test@salesforce1234.com';
	    urs.CommunityNickname='abcd';	   
	    urs.EmailEncodingKey='UTF-8';
	    urs.timezonesidkey='America/Los_Angeles';
	    urs.LocaleSidKey='en_US';	     
	    urs.languagelocalekey='en_US';
	     //urs.IsPartner=true;
	    urs.ProfileId=p.id;
	    insert urs;
	    
	    Quote__c quote = new Quote__c();
	    quote = new Quote__c();
        quote.Quote_Name__c = 'Test Quote';        
        quote.Client__c = urs.AccountId;
        quote.Status__c='New';        
        quote.Contact__c = urs.contactId;
        insert quote;
        
        List<LPFileSharingAccessManagement__c> accRecLst = new List<LPFileSharingAccessManagement__c>();
        LPFileSharingAccessManagement__c accRec = new LPFileSharingAccessManagement__c();
        accRec.Email__c = 'test@GLS.com';
        accRec.First_Name__c='Test';
        accRec.Last_Name__c = 'contact';
        accRec.IncorrectLoginAttempt__c = 0;
        accRec.isAccessActive__c = true;
        accRec.Password__c = 'test';
        accRec.Quote_Id__c = quote.id;
        accRec.User_Id__c = urs.Id;
        accRecLst.add(accRec);        
        
        LPFileSharingAccessManagement__c accRec2 = new LPFileSharingAccessManagement__c();
        accRec2.Email__c = 'test2@GLS.com';
        accRec2.First_Name__c='Test';
        accRec2.Last_Name__c = 'contact';
        accRec2.IncorrectLoginAttempt__c = 2;
        accRec2.isAccessActive__c = true;
        accRec2.Password__c = 'test123';
        accRec2.Quote_Id__c = quote.id;        
        accRecLst.add(accRec2);     
        
        insert accRecLst;
                
        quotes_file__c qFile = new quotes_file__c();
        qFile.File_Type__c = GLSConfig.deliverable;
        qFile.Quote__c = quote.Id;
        qFile.File_Name__c = 'test';
        qFile.File_Size__c = 2.5;
        insert qFile;
              
	    Test.startTest();
	    
	    GLSCommunity_ShareFileController obj;    		
		Pagereference pg;    			    	
		pg = Page.GLSCommunity_ShareLinkPage;
		Test.setCurrentPageReference(pg);
		System.currentPageReference().getParameters().put('accessId',accRec.id);						    		    				        	            	        		         	        		    		      
		obj = new GLSCommunity_ShareFileController();	   
		
		obj.userName = 'test@salesforce1234.com';
		obj.password = '123456';
		obj.login(); 	
		
		System.currentPageReference().getParameters().put('accessId',accRec2.id);
		obj = new GLSCommunity_ShareFileController();	
		obj.userName = 'test2@GLS.com';
		obj.password = 'test12';
		obj.login();
		
		obj.userName = 'test2@GLS.com';
		obj.password = 'test123';
		obj.login();
		
		obj.userName = 'test2@GLS.com';
		obj.password = 'test122';
		obj.login();
		obj.userName = 'test2@GLS.com';
		obj.password = 'test122';
		obj.login();
		obj.userName = 'test2@GLS.com';
		obj.password = 'test122';
		obj.login();
		obj.userName = 'test2@GLS.com';
		obj.password = 'test122';
		obj.login();
		obj.userName = 'test2@GLS.com';
		obj.password = 'test122';
		obj.login(); 	    
		obj.userName = 'test2@GLS.com';
		obj.password = 'test122';
		obj.login(); 	    
			    
		obj.downloadFileId = qFile.Id;
		obj.downloadFile();
			    
	    Test.stopTest();	    
	}
}