/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSLPW_EstimateApprovalControllerTest {

    static testMethod void testApproval() {
    	Account accObj					 	= GLSLPW_SetUpData.createAccount('Amgen');
	    Contact conObj					    = GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
	    Program__c prgObj					= GLSLPW_SetUpData.createProgram(accObj.Id,conObj.Id,'Program_Test', 'Amgen');
	    Protocol__c protObj					= GLSLPW_SetUpData.createProtocol(prgObj.Id, 'Open');
	  	Estimate__c estObj1					= GLSLPW_SetUpData.createEstimate(prgObj.Id, 'Draft');
	    Estimate__c estObj2					= GLSLPW_SetUpData.createEstimate(prgObj.Id, 'Draft');
	    	      
	    Estimate_Line_Item__c eliObj1		= GLSLPW_SetUpData.createELI(protObj.Id, estObj1.Id);
        Estimate_Line_Item__c eliObj2		= GLSLPW_SetUpData.createELI(protObj.Id, estObj1.Id);
        
        Country__c country1					= GLSLPW_SetUpData.createCountry('Test Country1');
        Estimate_Line_Country__c elcObj1	= GLSLPW_SetUpData.createELC(6, country1.Id, eliObj1.Id);
        
        AWS_File__c  awsObj 				= GLSLPW_SetUpData.createAWSFile('testPath','gls-us','testFile','testRecordType','testVersion',1);
        Estimate_Files__c estFileObj		= GLSLPW_SetUpData.createEstimateFile('Reference', estObj1.Id, awsObj.Id);
        Estimate_Files__c estFileObj1		= GLSLPW_SetUpData.createEstimateFile('Reference', estObj1.Id, awsObj.Id);
        estFileObj1.Estimate_File_Name__c	= 'Test File1';
        update estFileObj1;
        
        estFileObj.Estimate_File_Name__c	= 'Test File';
        update estFileObj;
        
      	Country__c country2					= GLSLPW_SetUpData.createCountry('Test Country2');
      	Protocol_Country__c pcObj			= GLSLPW_SetUpData.createProtocolCountry(protObj.Id, country1.Id, 6, 'Active');
        Program_Files__c pfObj				= GLSLPW_SetUpData.createProgramFiles(prgObj.Id, awsObj.Id);
        pfObj.Program_File_Name__c			= 'Test PF File';
        update pfObj;
        
        Test.startTest();
        	estObj1.Original_Estimate__c = estObj2.Id;
       		ApexPages.currentPage().getParameters().put('Id', estObj1.Id);
       		GLSLPW_EstimateApprovalController obj = new GLSLPW_EstimateApprovalController();
       		List<SelectOption> lstProtocolStatus = obj.getProtocolStatus();
       		obj.approveEstimate();
       		obj.createProgramFileOnS3();
       		system.assert(obj.lstEstLineItems.size()>0);
       		
       		pfObj.Program_File_Name__c			= 'Test File';
       	    update pfObj;
       	    obj.approveEstimate();
       	    
    		pfObj.Program_File_Name__c			= 'Test File1';
       	    update pfObj;
       	    obj.approveEstimate();
       	    
       	    pcObj.Country_Id__c = country2.Id;
       	    update pcObj;
       	    obj.approveEstimate();
       	    
       	    obj.cancel();
   		Test.stopTest();
    }
 }