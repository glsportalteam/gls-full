/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 */
 
@isTest 
private class GLSAmazonUtilityTest { 
   	private static String createTestCredentials(){
    	AWSKey__c testKey = new AWSKey__c(name='test keys',key__c='key',secret__c='secret');
        insert testKey;
        return testKey.name;    
    }
 	static testMethod void downloadQuoteFileTest(){  
   		GLSSetupData.setupData();
	   	test.startTest();
		    String credName = createTestCredentials();
		    GLSAmazonUtility.AWSCredentialName = credName;
		    GLSAmazonUtility.downloadQuoteFile('Test.txt',GLSConfig.bucketName); 
	    test.stopTest();
    }
  	public static testmethod void deleteQuoteFileTest() {
    	GLSSetupData.setupData();
        test.startTest();
	        String credName = createTestCredentials();
	        GLSAmazonUtility.AWSCredentialName = credName;
	        GLSAmazonUtility.getawss3LoginCredentials();
	        GLSAmazonUtility.deleteQuoteFile(GLSSetupData.qFiles1.id,'Amgen/Q-000480_a0jZ0000001UT9U/Source/Test.txt',GLSConfig.bucketName);
	        System.runAs(GLSSetupData.u){
	        	GLSAmazonUtility.deleteQuoteFile(GLSSetupData.qFiles1.id,'Amgen/Q-000480_a0jZ0000001UT9U/Source/Test.txt',GLSConfig.bucketName);
	        }	
        test.stopTest();
    }
    public static testmethod void cloneQuoteTest(){
    	GLSSetupData.setupData();
        test.startTest();
        	String credName = createTestCredentials();
            GLSAmazonUtility.AWSCredentialName = credName;
            GLSAmazonUtility.getawss3LoginCredentials();
            GLSAmazonUtility.cloneQuote(GLSSetupData.quote.id);
            GLSAmazonUtility.cloneQuote(GLSSetupData.quote1.id);
            GLSAmazonUtility.cloneQuote(GLSSetupData.quote2.id);
            GLSAmazonUtility.cloneQuote(GLSSetupData.quote3.id);
            GLSAmazonUtility.cloneQuote(GLSSetupData.quote4.id);
        test.stopTest();
    }   
    public static testmethod void cloneQuoteWithoutFilesTest(){
    	GLSSetupData.setupData();
        test.startTest();
        	String credName = createTestCredentials();
            GLSAmazonUtility.AWSCredentialName = credName;
            GLSAmazonUtility.getawss3LoginCredentials();
            GLSAmazonUtility.cloneQuoteWithoutFiles(GLSSetupData.quote.id);
        test.stopTest();
    } 
    public static testMethod void setObsoleteQuoteFileTypeTest(){
    	GLSSetupData.setupData();
    	test.startTest();
	    	String credName = createTestCredentials();
	        GLSAmazonUtility.AWSCredentialName = credName;
	        GLSAmazonUtility.getawss3LoginCredentials();
	        GLSAmazonUtility.setObsoleteQuoteFileType(GLSSetupData.qFiles1.Id);
	    test.stopTest();
    } 
    public static testMethod void deleteQuotePDFFileFromAmazonTest(){
    	GLSSetupData.setupData();
    	test.startTest();
	    	String credName = createTestCredentials();
	        GLSAmazonUtility.AWSCredentialName = credName;
	        GLSAmazonUtility.getawss3LoginCredentials();
	        GLSAmazonUtility.deleteQuotePDFFileFromAmazon(GLSSetupData.quotePDFFile1.Id,'Amgen/Q-000480_a0jZ0000001UT9U/Source/Test.txt');
	    test.stopTest();
    } 
    public static testMethod void deleteQuoteFileFromAmazonTest(){
    	GLSSetupData.setupData();
    	test.startTest();
	    	String credName = createTestCredentials();
	        GLSAmazonUtility.AWSCredentialName = credName;
	        GLSAmazonUtility.getawss3LoginCredentials();
	        GLSAmazonUtility.deleteQuoteFileFromAmazon(GLSSetupData.qFiles1.Id,'Amgen/Q-000480_a0jZ0000001UT9U/Source/Test.txt',GLSConfig.bucketName);
	    test.stopTest();
    }   
    public static testMethod void setObsoleteFileTypeTest(){
    	GLSSetupData.setupData();
    	test.startTest();
	    	String credName = createTestCredentials();
	        GLSAmazonUtility.AWSCredentialName = credName;
	        GLSAmazonUtility.getawss3LoginCredentials();
	        GLSAmazonUtility.setObsoleteFileType(GLSSetupData.AWSFile1.Id);
	    test.stopTest();
    }
    public static testMethod void getLoginHistoryTest(){
    	GLSSetupData.setupData();
    	test.startTest();
	    	String credName = createTestCredentials();
	        GLSAmazonUtility.AWSCredentialName = credName;
	        GLSAmazonUtility.getawss3LoginCredentials();
	        GLSAmazonUtility.getLoginHistory(GLSSetupData.newUser.Id);
	    test.stopTest();
    }
}