/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSLPW_ProgramControllerExtTest {

    static testMethod void myUnitTest1() {
        Account accObj 				= GLSLPW_SetUpData.createAccount('Amgen');
        Contact conObj 				= GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
        Program__c prgObj			= GLSLPW_SetUpData.createProgram(accObj.Id,conObj.Id,'Program_Test', 'Amgen');
        Protocol__c protObj 		= GLSLPW_SetUpData.createProtocol(prgObj.Id,'Draft');
        Estimate__c estimateObj		= GLSLPW_SetUpData.createEstimate(prgObj.Id, 'Draft');
        AWS_File__c  awsObj 		= GLSLPW_SetUpData.createAWSFile('testPath','gls-us','testFile','testRecordType','testVersion',1);
        Program_Files__c pfObj		= GLSLPW_SetUpData.createProgramFiles(prgObj.Id,awsObj.Id);
        Country__c countryObj		= GLSLPW_SetUpData.createCountry('Test Country');
        Protocol_Country__c pcObj 	= GLSLPW_SetUpData.createProtocolCountry(protObj.Id, countryObj.Id, 5,'Active');
        Protocol_Country__c pcObj1 	= GLSLPW_SetUpData.createProtocolCountry(protObj.Id, countryObj.Id, 10,'Active');
        ApexPages.currentPage().getParameters().put('aid', accObj.Id);
        ApexPages.currentPage().getParameters().put('cid', conObj.Id);
        ApexPages.currentPage().getParameters().put('id', prgObj.Id);
        Test.startTest();
        GLSLPW_ProgramControllerExt obj = new GLSLPW_ProgramControllerExt(new ApexPages.StandardController(prgObj));
        obj.pfileId = pfObj.Id;
        obj.customSave();
        obj.customEdit();
        obj.customCancel();
        obj.createNewLPEstimate();
        obj.fetchProgramFile();
        obj.deleteProgramFile();
        GLSLPW_ProgramControllerExt obj2 = new GLSLPW_ProgramControllerExt(new ApexPages.StandardController(prgObj));
        obj2.customSave();
        Test.stopTest();
       }
       
       static testMethod void myUnitTest2(){
       
        Account accObj1 				= GLSLPW_SetUpData.createAccount('Amgen');
        Contact conObj1 				= GLSLPW_SetUpData.createContact(accObj1.Id,'TestName');
        Program__c prgObj1			= GLSLPW_SetUpData.createProgram(accObj1.Id,conObj1.Id,'Program_Test', '');
        Program__c prgObj2			= GLSLPW_SetUpData.createProgram(accObj1.Id,conObj1.Id,'Program_Test', '');
        Protocol__c protObj1		= GLSLPW_SetUpData.createProtocol(prgObj1.Id,'Draft');
        Estimate__c estimateObj1		= GLSLPW_SetUpData.createEstimate(prgObj1.Id, 'Draft');
        AWS_File__c  awsObj1 		= GLSLPW_SetUpData.createAWSFile('testPath','gls-us','testFile','testRecordType','testVersion',1);
        Program_Files__c pfObj1		= GLSLPW_SetUpData.createProgramFiles(prgObj1.Id,awsObj1.Id);
        Country__c countryObj1		= GLSLPW_SetUpData.createCountry('Test Country');
        Protocol_Country__c pcObj1 	= GLSLPW_SetUpData.createProtocolCountry(protObj1.Id, countryObj1.Id, 5,'Active');
        ApexPages.currentPage().getParameters().put('aid', accObj1.Id);
        ApexPages.currentPage().getParameters().put('cid', conObj1.Id);
        ApexPages.currentPage().getParameters().put('id', null);
        
        AWS_File__c AWSFile1 = new AWS_File__c();
        AWSFile1.File_Name__c='Test1.txt';
        insert AWSFile1;
        Program_Files__c pfObj2 = new Program_Files__c();
        pfObj2.Program_File_Name__c='Test1.txt'; 
        pfObj2.Program_Id__c = prgObj1.Id;
        pfObj2.AWS_File_Id__c = AWSFile1.Id; 
        
        insert pfObj2;
        
        Test.startTest();
        
        //prgObj1.Id = null;
        prgObj1.Sponsor__c=null;
        prgObj1.Reference__c ='';
        update prgObj1;
       	GLSLPW_ProgramControllerExt obj1 = new GLSLPW_ProgramControllerExt(new ApexPages.StandardController(prgObj1));
       	
        obj1.pfileId = pfObj1.Id;
        obj1.customSave();
        obj1.customEdit();
        obj1.customCancel();
        obj1.createNewLPEstimate();
        obj1.fetchProgramFile();
        obj1.deleteProgramFile();
        Test.stopTest();
       
       
       }
}