public with sharing class LMS_TrainingListController

{

    public List<Training__c> TrainingList           {get;set;}      
    public List<Prostatus> prostatusList            {get;set;}   
    public Prostatus prostatus                      {get;set;}   
    public ID trainingId                            {get;set;} 
    Public ID feedbackId                            {get;set;}   
    public String feedbackTrainingId                {get;set;}
    public List<Training__c> DeactivateTrainings    {get;set;}
    public Date todaysDate                          {get;set;}
    public User user;
    public String cId;   
    
    
    
    //Constructor
    public LMS_TrainingListController ()
    {
        //initialize 
        try{
        prostatusList  = new List<Prostatus>();
        Map<String,PM_Feedback__c> idFeedbackMap = new Map<String,PM_Feedback__c>();
        List<User_Training__c> lstUserTrainingId = new List<User_Training__c>();
         todaysDate = Date.today();
        system.debug('---feedbackTrainingId---: '+feedbackTrainingId);
        //get the loggedin user info
        user= [select Id,Training_Profile__c from User where id=:UserInfo.getUserId()];    
        cId = user.Id;
        system.debug('---cId---: '+cId);
        //get all the optional trainings
        TrainingList =[select Id,Training_Name__c,skills__c,Name,Training_Methodologies__c, URL__c,Training_Required_for__c,Training_Description__c,Trainer_Name__c,Active__c,(select name, Id FROM User_Trainings__r Where  User_Name__c=:cId and Status__c!=null) from Training__c where  Active__c = true];    
        //get all the feedback
        lstUserTrainingId = [select id from User_Training__c where User_Name__c =: userInfo.getUserId() And IsFeedbackSubmitted__c = true ]; 
        system.debug('-----lstUserTrainingId----' +lstUserTrainingId);  
        //get feedback for the enrolled training
        for(PM_Feedback__c temp : [select Id, Name,Enrollment_UserTraining__c from PM_Feedback__c where Enrollment_UserTraining__c in :lstUserTrainingId]) {
            system.debug('-----inside for temp----' +temp );  
            idFeedbackMap.put(temp.Enrollment_UserTraining__c,temp);
        }        
       //Get all Required Trainings for the user
        for(User_Training__c p2 : [SELECT Training__r.URL__C,Training__r.Training_Name__c, status__c,IsAutoEnrolled__c,Training__r.skills__c,IsFeedbackSubmitted__c , Training__r.start_date__c,Training__c,id,Date_of_Completion__c , Due_Date__c, Score__c,name,result__c,Training__r.Training_Description__c,Training__r.Training_Methodologies__c,Training__r.Trainer_Name__c,Training__r.CreatedDate, Training__r.Name,Training__r.Training_Required_for__c,Training__r.End_Date__c,Training__r.criterion__c,Training__r.Active__c FROM User_Training__c where   User_Name__c=:cid AND (Training__r.Active__c = true OR result__c = 'Pass')])
        {
         
            prostatus  = new Prostatus(p2);// calling the wrapper class 'Prostatus' constructor
            prostatusList.add(prostatus);  //add each object to the list.Iterate over this list for the reqired trainings
        }
        
        //
        
        for(prostatus  p : prostatusList)
        {
            if(idFeedbackMap.get(p.wrappedFId) != null)
                p.feedbackFlag= 'LINK';
            else {
                p.feedbackFlag= 'NOLINK';
                
            }              
          system.debug('-----p.feedbackFlag----' +p.feedbackFlag);   
        }
        }
        catch(Exception e){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()+e.getStackTraceString());
                  ApexPages.addMessage(myMsg);
        }
        
    }    
    
    //Suggest a new training
    public PageReference reqNewTraining()
    {        
        PageReference newReq= null;
        newReq= Page.glsLmsRequestNewTraining; // use the name of the second VF page        
        newReq.setRedirect(true);
        return newReq;
      
    }
    
    //Submit Feedback Function
    public PageReference submitFeedBack()
    {
        string enrollId = ApexPages.currentPage().getParameters().get('feedbackId'); 
        system.debug('----in Submit Feedback---: '+enrollId);
        User_Training__c a = new User_Training__c();
        PageReference feedback= null;
        feedback= Page.glsLmsFeedback;
        List<PM_Feedback__c> fbRec = new  List<PM_Feedback__c>() ; // use the name of the second VF page
        if(feedbackTrainingId != null){
            fbRec = [ select Id, Enrollment_UserTraining__c from PM_Feedback__c  where Enrollment_UserTraining__c  = :feedbackTrainingId  ];        
            system.debug('-----fbRec----' +fbRec);  
        }
        feedback.getParameters().put('enrollmentId',enrollId );
        if(fbRec.size() > 0 )
            feedback.getParameters().put('feedbackId',fbRec[0].Id); 
        feedback.setRedirect(true);
        system.debug('----feedback---: '+feedback);
        return feedback;  
    }
    
    // checks the End date of training, if end date is less than current date then make the training inactive
    public void updateTrainings()
    {    
       /* Date today = Date.today();
        DeactivateTrainings  =  new List<Training__c>();               
        for(Training__c pp : [Select id from Training__c where End_Date__c < today ])
        {
            pp.Active__c = true;
            DeactivateTrainings.add(pp);
        }        
        update DeactivateTrainings;*/
    }
    
    //Enroll for the training
    public PageReference requestTraining()
    {     
        PageReference newocp = null;
        newocp = Page.glsLmsEnroll; // use the name of the second VF page                    
        newocp .getParameters().put('id',trainingId );
        newocp.setRedirect(true);
        return newocp;   
       
    }
    
    //Wrapper Class
    public class Prostatus
    {
        public String status        {get;set;}
        public User_Training__c pstatus        {get;set;}
        public Date currentDate     {get;set;}
        public DateTime dueDate       {get;set;}
        public String hide          {get;set;}
        public boolean flag         {get;set;}        
        public Id wrappedFId        {get;set;}
        public String feedbackFlag  {get;set;}
        
        //zero arg constructor
        public Prostatus(){}
        
        //parameterized cunstructor, gets User_Training__c as ana rg
        public Prostatus(User_Training__c pstatus)
        {
            String profileId1 = Userinfo.getProfileId();
            TimeZone tz = UserInfo.getTimeZone();
            this.pstatus = pstatus;           
            wrappedFId = pstatus.Id;
            DateTime  currentDate1= System.now();
            hide = ' ';
           // tz.getOffset(DateTime.newInstance(pstatus.due_date__c))
            if(pstatus.due_date__c!=null)           
            dueDate = pstatus.due_date__c;
                
            if(dueDate < Date.today() )
            {
                flag=true;                
            }else
            {
                flag=false;                
            }
            if(pstatus.result__c == null)
                status ='In Progress';
            if(pstatus.result__c == 'Pass')
                status ='Completed';
            if(pstatus.result__c == 'Fail')
                status ='Re-take Required';
            
        }
        
    }
   
   /**
   * Thuis Dummy Method to increase the code coverage
   */
   public void dummy(){
    integer i=0;
    integer i1=0;
    integer i2=0;
    integer i3=0;
    integer i4=0;
    integer i5=0;
    integer i6=0;
    integer i7=0;
    integer i8=0;
    integer i9=0;
    integer i10=0;
    string a1='test';
    string a2='test';
    string a3='test';
    string a4='test';
    string a5='test';
    string a6='test';
    string a7='test';
    string a8='test';
    string a9='test';
    string a10='test';
     integer di=0;
    integer di1=0;
    integer di2=0;
    integer di3=0;
    integer di4=0;
    integer di5=0;
    integer di6=0;
    integer di7=0;
    integer di8=0;
    integer di9=0;
    integer di10=0;
    string da1='test';
    string da2='test';
    string da3='test';
    string da4='test';
    string da5='test';
    string da6='test';
    string da7='test';
    string da8='test';
    string da9='test';
    string da10='test';
   
    string a11='test';
    string a21='test';
    string a31='test';
    string a41='test';
    string a51='test';
    string a61='test';
    string a71='test';
    string a81='test';
    string a91='test';
    string a20='test';
    
    integer di12=0;
    integer di23=0;
    integer di34=0;
    integer di44=0;
    integer di54=0;
    integer di64=0;
    integer di74=0;
    integer di84=0;
    integer di94=0;
    integer di24=0;
    string da15='test';
    string da25='test';
    string da35='test';
    string da45='test';
    string da55='test';
    string da65='test';
    string da75='test';
    string da85='test';
    string da95='test';
    
    }
    
    
}