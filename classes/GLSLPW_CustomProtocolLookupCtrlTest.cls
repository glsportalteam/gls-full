/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSLPW_CustomProtocolLookupCtrlTest {

    static testMethod void testProtocolLookup() {
        
        Account accObj = GLSLPW_SetUpData.createAccount('Amgen');
        Contact conObj = GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
        Program__c prgObj = GLSLPW_SetUpData.createProgram(accObj.Id,conObj.Id,'Program_Test', 'Amgen');
        Protocol__c protObj = GLSLPW_SetUpData.createProtocol(prgObj.Id,'Draft');
        Quote__c quoteObj	= GLSLPW_SetUpData.createQuote(accObj.Id, conObj.Id, 'Billable');
        Test.startTest();
	        ApexPages.currentPage().getParameters().put('prgmId', prgObj.Id);
	        ApexPages.currentPage().getParameters().put('account', accObj.Id);
	        ApexPages.currentPage().getParameters().put('lksrch', 'Test String');
	        ApexPages.currentPage().getParameters().put('forInvoice', 'true');
	   		
	        GLSLPW_CustomProtocolLookupController obj  = new GLSLPW_CustomProtocolLookupController();
	        system.assert(obj.searchString != '');
	        obj.search();
	        obj.getFormTag();
	        obj.getTextBox();
	        
	        ApexPages.currentPage().getParameters().put('lksrch', '');
	        ApexPages.currentPage().getParameters().put('forInvoice', 'true');
	        quoteObj.Protocol__c = protObj.Id;
	        update quoteObj;
	        GLSLPW_CustomProtocolLookupController obj1  = new GLSLPW_CustomProtocolLookupController();
	        
	        ApexPages.currentPage().getParameters().put('forInvoice','false');
	        ApexPages.currentPage().getParameters().put('lksrch', '');
	        GLSLPW_CustomProtocolLookupController obj2  = new GLSLPW_CustomProtocolLookupController();
	        
	        ApexPages.currentPage().getParameters().put('forInvoice','false');
	        ApexPages.currentPage().getParameters().put('lksrch', 'Test String');
	        GLSLPW_CustomProtocolLookupController obj3  = new GLSLPW_CustomProtocolLookupController();
	        
	        ApexPages.currentPage().getParameters().put('account', '');
	        GLSLPW_CustomProtocolLookupController obj4  = new GLSLPW_CustomProtocolLookupController();
	    Test.stopTest();
        }
}