/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSLPW_PCDetailControllerExtTest {

	static testMethod void setUpTestData(){
		
	}

    static testMethod void myUnitTest() {
    	Account accObj					 	= GLSLPW_SetUpData.createAccount('Amgen');
        Contact conObj						= GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
        Program__c prgObj					= GLSLPW_SetUpData.createProgram(accObj.Id,conObj.Id,'Program_Test', 'Amgen');
        Protocol__c protObj 				= GLSLPW_SetUpData.createProtocol(prgObj.Id,'Draft');
        Country__c cObj1 					= GLSLPW_SetUpData.createCountry('Test Country1');
        Protocol_Country__c pc2 			= GLSLPW_SetUpData.createProtocolCountry(protObj.Id, cObj1.Id, 3, 'TestStatus');
    	Protocol_Country__c pc1 = [SELECT Id FROM Protocol_Country__c LIMIT 1];
    	ApexPages.currentPage().getParameters().put('id', pc1.Id);
    	Test.startTest();
        	GLSLPW_PCDetailControllerExt obj = new GLSLPW_PCDetailControllerExt(new ApexPages.StandardController(pc1));
        	obj.customCancel();
        	obj.customDelete();
        	obj.customEdit();
        	obj.customSave();
        Test.stopTest();
    }
}