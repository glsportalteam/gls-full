/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSLPW_ProgramControllerExt Class
* Function: This class is used as a controller extension for the GLSLPW_NewProgramPage and GLSLPW_ProgramDetailPage
*/

public with sharing class GLSLPW_ProgramControllerExt {
    
    //Variable declaration for the class
    public Program__c prg                               {get; set;}
    public List<ProtocolWrapper> lstProtocolWrapper     {get; set;}
    public List<Estimate__c> lstEstimate                {get; set;}
    public List<Program_Files__c> lstRefFiles           {get; set;}
    public Id prgId                                     {get; set;}
    public Id pfileId                                   {get; set;}    
    public Boolean editMode                             {get; set;}
    public Boolean enableDelete							{get; set;}
    private Id conId;
    private Id accId;
    private Set<String> setProgramNames;
    
    //Constructor
    public GLSLPW_ProgramControllerExt(ApexPages.StandardController stdcon){
        
        //Initialization
        prg = new Program__c();
        lstEstimate = new List<Estimate__c>();
        lstProtocolWrapper = new List<ProtocolWrapper>();
        lstRefFiles = new List<Program_Files__c>();
        setProgramNames = new Set<String>();
        editMode = false;
        
        //Get IDs from URL
        conId = ApexPages.currentPage().getParameters().get('cid');
        accId = ApexPages.currentPage().getParameters().get('aid');
        prgId = ApexPages.currentPage().getParameters().get('id');
        if(accId != null){
            setProgramNames = getAllProgram(accId);
        }
        
        if(prgId != null){
            prg = new Program__c();
            prg = getProgramRecord(prgId);
            getReferenceFiles();
            lstProtocolWrapper = getProtocols(prgId);
            lstEstimate = getEstimates(prgId);
        }
        if(Schema.sObjectType.Program_Files__c.isDeletable()){
        	enableDelete = true;
        }
        else{
        	enableDelete = false;
        }
    }
    
    /* 
    @Description: To get the complete record of Program
    @Params: Program Id
    @Return Type: Program__c
    */
    private Program__c getProgramRecord(Id prgId){
        prg =  [SELECT Therapeutic_Area__c, Status__c, Program_Name__c, Number_of_Protocols__c, Name, Id, Contact__c, Account__c,
                Approved_Amount__c,  PA_Amount__c, Invoiced_Amount__c, Program_Manager__c, Sponsor__c, Sponsor_Image__c, Contact__r.Name,
                Contact__r.Account.Name, Program_Manager__r.Name
                FROM Program__c
                WHERE Id =: prgId];
        return prg;
    }
    
    /* 
    @Description: To get the list of reference files
    @Params: None
    @Return Type: Estimate_Files__c
    */
    public void getReferenceFiles(){
        lstRefFiles = [SELECT AWS_File_Id__c, Name, AWSFilePath__c, Description__c, CreatedBy.Name, CreatedDate, Program_File_Name__c,
                       Program_Id__c, View__c, Id 
                       FROM Program_Files__c
                       WHERE Program_Id__c =: prgId
                  	   ORDER BY Name DESC ];
    }
    
    /* 
    @Description: To get the list of protocols and populate it in a wrapper to be iterated on page
    @Params: Program Id
    @Return Type: List<ProtocolWrapper>
    */
    private List<ProtocolWrapper> getProtocols(Id prgId){
        List<ProtocolWrapper> lstProWrapperGet = new List<ProtocolWrapper>();
        List<Protocol__c> lstProtocols = [SELECT Name, Approved_Amount__c, Invoiced_Amount__c, PA_Amount__c, Protocol_Name__c, Status__c, Id
                                            FROM Protocol__c
                                            WHERE Program_Name__c =: prgId
                                            ORDER BY CreatedDate DESC ];
        Set<Id> setProtocolId = new Set<Id>();
        if(lstProtocols != null && lstProtocols.size() > 0){
            for(Protocol__c pro : lstProtocols){
                setProtocolId.add(pro.Id);
            }
            List<Protocol_Country__c> lstProtocolCountry = new List<Protocol_Country__c>();
            if(!setProtocolId.isEmpty()){
                lstProtocolCountry = [  SELECT Number_of_Sites__c, Protocol_Id__r.Name
                                        FROM Protocol_Country__c
                                        WHERE Protocol_Id__c IN : setProtocolId
                                        ORDER BY CreatedDate DESC];
            }
            Map<String, Decimal> mapNumCountries = new Map<String, Decimal>();
            if(lstProtocolCountry != null && lstProtocolCountry.size() > 0){
                for(Protocol_Country__c pc : lstProtocolCountry){
                    Decimal sites = 0;
                    if(mapNumCountries.containsKey(pc.Protocol_Id__r.Name)){
                        sites = mapNumCountries.get(pc.Protocol_Id__r.Name);
                        mapNumCountries.put(pc.Protocol_Id__r.Name, sites + 1);
                    }
                    else{
                        mapNumCountries.put(pc.Protocol_Id__r.Name, 1);
                    }
                }
            }
            //Populate list of Protocol wrapper to be iterated over page
            for(Protocol__c pro : lstProtocols){
                lstProWrapperGet.add(new ProtocolWrapper(pro.Id, pro.Name, pro.Protocol_Name__c, 
                                                        mapNumCountries.containsKey(pro.Name) ? mapNumCountries.get(pro.Name) : 0,
                                                        pro.Status__c, pro.Approved_Amount__c, pro.PA_Amount__c, pro.Invoiced_Amount__c));
            }
        }
        return lstProWrapperGet;
    }
    
    /* 
    @Description: To get the list of estimates and populate it in a wrapper to be iterated on page
    @Params: Program Id
    @Return Type: List<Estimate__c>
    */
    private List<Estimate__c> getEstimates(Id prgId){
        List<Estimate__c> lstEstimatesGet = [SELECT Name, Estimate_Amount__c, Status__c, Id,LP_Estimate_ID__c
                                            FROM Estimate__c
                                            WHERE Program_Id__c =: prgId
                                            ORDER BY CreatedDate DESC];
        return lstEstimatesGet;
    }
    
    /* 
    @Description: To get the list of all Program names to avoid duplicacy of Program names for same account.
    @Params: Accouont Id
    @Return Type: List<String>
    */
    private Set<String> getAllProgram(Id accId){
        List<Program__c> lstProgramsGet = [SELECT Program_Name__c
                                            FROM Program__c
                                            WHERE Account__c =: accId
                                            ORDER BY CreatedDate DESC];
        Set<String> setPrgNamesGet = new Set<String>();
        if(lstProgramsGet != null && lstProgramsGet.size() > 0){
            for(Program__c prgObj : lstProgramsGet){
                setPrgNamesGet.add(prgObj.Program_Name__c);
            }
        }
        return setPrgNamesGet;
    }
    
    /* 
    @Description: Save method to insert the Program record.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference customSave(){
        PageReference pRef = null;
        try{
            if(prgId == null){
                if(prg.Sponsor__c == null && prg.Reference__c == ''){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, GLSconfig.enterSponsorRef); 
                    ApexPages.addMessage(myMsg);
                }
                else if(setProgramNames.contains(prg.Program_Name__c)){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, GLSconfig.enterProgramName); 
                    ApexPages.addMessage(myMsg);
                }
                else{
                    prg.Account__c = accId;
                    prg.Contact__c = conId;
                    prg.Status__c = GLSConfig.DraftQuote;
                    insert prg;
                    if(prg.Id != null){
                        pRef = new PageReference('/apex/GLSLPW_ProgramDetailPage?id='+prg.Id);
                        pRef.setRedirect(true);
                    }
                }
            }
            else{
                editMode = false;
                update prg;
                if(prg.Id != null){
                    pRef = new PageReference('/apex/GLSLPW_ProgramDetailPage?id='+prg.Id);
                    pRef.setRedirect(true);
                }
            }
        }
        catch(DMLException de){
            system.debug(GLSConfig.DMLExceptionOccured + de);
        }
        catch(Exception e){
            system.debug(GLSConfig.ExceptionOccured + e);
        }
        return pRef;
    }
    
    /* 
    @Description: Method to edit the Program record.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference customEdit(){
        PageReference pref = null;
        editMode = true;
        return pref;
    }
    
    /* 
    @Description: Cancel method to stop editing Program record.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference customCancel(){
        PageReference pref = null;
        editMode = false;
        return pref;
    }
    
    /* 
    @Description: Creates New LP Estimate and returns the page reference for GLSNewLPEstimate
    @Params: None
    @Return Type: PageReference
    */
    public PageReference createNewLPEstimate(){
        PageReference pRef = null;
        Estimate__c estimateObj = new Estimate__c(Program_Id__c = prgId, Status__c = GLSConfig.DraftQuote, Is_Primary__c = true);
        try{
            insert estimateObj;
        }
        catch(DMLException de){
            system.debug(GLSConfig.DMLExceptionOccured+de);
        }
        catch(Exception e){
            system.debug(GLSConfig.ExceptionOccured+e);
        }
        if(estimateObj.Id != null){
            pRef = new PageReference('/apex/GLSLPW_EstimateDetailPage?id='+estimateObj.Id);
            pRef.setRedirect(true);
        }
        return pRef;
    }
    
    /* 
    @Description: Downloads the Program File from S3 server.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference fetchProgramFile(){
        String stMessage = '';
        String statusMessage ='';
        String prefStr = '';
        PageReference pref = null;
        if(prgId != null){
            Program_Files__c pFiles = [SELECT Id, AWSFilePath__c, Program_File_Name__c
                                        FROM Program_Files__c
                                        WHERE Program_Id__c =: prgId 
                                        AND Id =: pfileId LIMIT 1];
            if(pFiles != null){
                String path = pFiles.AWSFilePath__c + pFiles.Program_File_Name__c;
                if(path != null){                                 
                    statusMessage = GLSAmazonUtility.downloadQuoteFile(path,GLSConfig.bucketName); 
                    try{
                        if(statusMessage != null){
                            List<String> urlParams = statusMessage.split('Signature=');
                            if(urlParams != null && urlParams.size() > 1){                          
                                String signature = EncodingUtil.urlDecode(urlParams[1],'UTF-8');   
                                String sigEncoded;
                                /*To overcome the error of "Signature Does Not Match" when the generated signature contains space which is in 
                                turn replaced by "+" */
                                if(signature.contains('+')){
                                    sigEncoded = signature.replace('+','%2B');
                                }    
                                else{
                                    sigEncoded = signature;
                                }    
                                prefStr = urlParams[0] + 'Signature=' + sigEncoded;
                            }
                        }                                                                                                                        
                    }
                    catch(Exception e){                    
                        stMessage = e.getMessage();                    
                    }
                }
                pref = new PageReference(prefStr);
            }
        }
        return pref;
    }
    
    /* 
    @Description: Deletes the Program File from S3 server.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference deleteProgramFile(){
        String stMessage = '';
        String statusMessage ='';
        if(prgId != null){
            Program_Files__c pFiles = [SELECT Id, AWSFilePath__c, Program_File_Name__c
                                        FROM Program_Files__c
                                        WHERE Program_Id__c =: prgId 
                                        AND Id =: pfileId LIMIT 1];
            if(pFiles != null){
                String path = pFiles.AWSFilePath__c + pFiles.Program_File_Name__c;
                try{
                    String bucketName = GLSConfig.bucketName;
                    StatusMessage = GLSAmazonUtility.deleteFilesFromAmazon(pfileId,path);
                    if(StatusMessage.equalsIgnoreCase('true')){
                        StMessage ='File Deleted Successfully';
                    }
                    else{
                        StMessage ='File not deleted';
                    }
                }
                catch(Exception e){
                    StMessage = e.getMessage();
                    system.debug(GLSConfig.ExceptionOccured);
                }
            }
        }
        ApexPages.Message statusMsg = new ApexPages.Message(ApexPages.Severity.INFO, StMessage);
        ApexPages.addMessage(statusMsg);
        getReferenceFiles();
        return null; 
    }
    
    //Wrapper Class for populating related list of Protocol on Program Detail Page
    private class ProtocolWrapper{
        private Id proId;
        public Id getproId(){
            return proId;
        }
        private void setproId(Id proId){
            this.proId = proId;
        }
        
        private String protocolId;
        public String getprotocolId(){
            return protocolId;
        }
        private void setprotocolId(String protocolId){
            this.protocolId = protocolId;
        }
        
        private String protocolName;
        public String getprotocolName(){
            return protocolName;
        }
        private void setprotocolName(String protocolName){
            this.protocolName = protocolName;
        }
        
        private Decimal numCountries;
        public Decimal getnumCountries(){
            return numCountries;
        }
        private void setnumCountries(Decimal numCountries){
            this.numCountries = numCountries;
        }
        
        private String status;
        public String getstatus(){
            return status;
        }
        private void setstatus(String status){
            this.status = status;
        }
        
        private Decimal approvedAmt;
        public Decimal getapprovedAmt(){
            return approvedAmt;
        }
        private void setapprovedAmt(Decimal approvedAmt){
            this.approvedAmt = approvedAmt;
        }
        
        private Decimal poAmt;
        public Decimal getpoAmt(){
            return poAmt;
        }
        private void setpoAmt(Decimal poAmt){
            this.poAmt = poAmt;
        }
        
        private Decimal consumedAmt;
        public Decimal getconsumedAmt(){
            return consumedAmt;
        }
        private void setconsumedAmt(Decimal consumedAmt){
            this.consumedAmt = consumedAmt;
        }
        
        private ProtocolWrapper(Id proId, String protocolId, String protocolName, Decimal numCountries, String status, Decimal approvedAmt, 
                                Decimal poAmt, Decimal consumedAmt){
            this.proId = proId;
            this.protocolId = protocolId;
            this.protocolName = protocolName;
            this.numCountries = numCountries;
            this.status = status;
            this.approvedAmt = approvedAmt;
            this.poAmt = poAmt;
            this.consumedAmt = consumedAmt;
            getproId();
            setproId(proId);
            getprotocolId();
            setprotocolId(protocolId);
            getprotocolName();
            setprotocolName(protocolName);
            getnumCountries();
            setnumCountries(numCountries);
            getstatus();
            setstatus(status);
            getapprovedAmt();
            setapprovedAmt(approvedAmt);
            getpoAmt();
            setpoAmt(poAmt);
            getconsumedAmt();
            setconsumedAmt(consumedAmt);
        }
    }
}