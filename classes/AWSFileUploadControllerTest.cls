/** Copyright (c) 2013, Global Languages Solutions, Inc. All rights reserved.
* Class   :  AWSFileUploadControllerTest class
* Function : test class for AWSFileUploadController
*/
@isTest
public with sharing class AWSFileUploadControllerTest {

    /**
        * This Unit Test method will test the AWSFileUploadComponent for Quote Files 
    **/
    static testMethod void myUnitTest() { 
        GLSSetupData.setupData();
        Test.startTest();        
        AWSFileUploadController uploadFile = new AWSFileUploadController();
        
        /*
            Test for old approach
        */
        uploadFile.parentIdValue = GLSSetupData.quote.Id;
        uploadFile.parentObjStatus = GLSSetupData.quote.Status__c;
        uploadFile.FileNameStr='Test.txt';
        uploadFile.selectedFileType='Quote File';
        uploadFile.createAWSRecords();
        uploadFile.newFileName='Test.txt';
        uploadFile.fileSize=9000;
        uploadFile.AmazonPath = 'Test Amazon path';
        uploadFile.successQuoteFileupload();            
        AWSFileUploadController.quoteFileName=uploadFile.quoteFileId;                            
        AWSFileUploadController.callGetExternalFileWS();
        uploadFile.FileNameStr='Test12.txt';
        uploadFile.createAWSRecords();                                                 
        Test.stopTest();
        uploadFile.deleteFileName='Test.txt';
        uploadFile.selectedFileType='Quote File';
        uploadFile.deleteUnuploadedFiles();
        
        /*
            test with new approach
        */
        uploadFile.parentIdValue = GLSSetupData.quote.Id;
        uploadFile.parentObjStatus = GLSSetupData.quote.Status__c;
        uploadFile.FileNameStr='Test.txt';
        uploadFile.selectedFileType='Quote File';
        uploadFile.createAWSRecords();
        uploadFile.newFileName='Test.txt';
        uploadFile.fileSize=9000;
        uploadFile.AmazonPath = 'Test Amazon path';
        uploadFile.successFileupload();                
        uploadFile.populateQuoteFile(GLSSetupData.AWSFile2.id,'test.txt',9000);
        AWSFileUploadController.recallGEFAPI(GLSSetupData.qFiles1.id);
        system.assert( uploadFile.parentIdValue != null );
        //String s = uploadFile.getSignedPolicy();        
    }    
    /**
        * This Unit Test method will test the AWSFileUploadComponent for Quote PDF Files 
    **/
    static testMethod void myUnitTest2() { 
        GLSSetupData.setupData();
        Test.startTest();
        AWSFileUploadController uploadFile = new AWSFileUploadController();
        uploadFile.parentIdValue = GLSSetupData.quote.Id;
        uploadFile.parentObjStatus = GLSSetupData.quote.Status__c;
        uploadFile.FileNameStr='Test.txt';
        uploadFile.selectedFileType='Quote PDF';
        uploadFile.createAWSRecords();
        uploadFile.newFileName='Test.txt';
        uploadFile.fileSize=9000;
        uploadFile.AmazonPath = 'Test Amazon path';
        uploadFile.successFileupload();
        uploadFile.updateParentObjAWSInfo();                
        uploadFile.FileNameStr='Test12.txt';
        uploadFile.createAWSRecords();
        uploadFile.deleteFileName='Test.txt';     
        uploadFile.selectedFileType='Quote PDF';
        uploadFile.deleteUnuploadedFiles(); 
        uploadFile.selectedFileType='Quote PDF';        
        system.assert( uploadFile.parentIdValue != null );                                                   
        Test.stopTest();
    }
    
    /*Added by Amit on 8/3/2016 */
    
       /**
        * This Unit Test method will test the AWSFileUploadComponent for CBProjectFile Files 
    **/
    static testMethod void myUnitTest3() { 
        GLSSetupData.setupData();
        Test.startTest();
        AWSFileUploadController uploadFile = new AWSFileUploadController();
        uploadFile.parentIdValue = GLSSetupData.quote.Id;
        uploadFile.parentObjStatus = GLSSetupData.quote.Status__c;
        uploadFile.FileNameStr='Test.txt';
        uploadFile.selectedFileType='CBProjectFile';
        uploadFile.createAWSRecords();
        uploadFile.newFileName='Test.txt';
        uploadFile.fileSize=9000;
        uploadFile.AmazonPath = 'Test Amazon path';
        uploadFile.successFileupload();
        uploadFile.updateParentObjAWSInfo();                
        uploadFile.FileNameStr='Test12.txt';
        uploadFile.createAWSRecords();
        uploadFile.deleteFileName='Test.txt';     
        uploadFile.selectedFileType='CBProjectFile';
        uploadFile.deleteUnuploadedFiles(); 
        uploadFile.selectedFileType='CBProjectFile';        
        system.assert( uploadFile.parentIdValue != null );                                                   
        Test.stopTest();
    }
    
    
       /**
        * This Unit Test method will test the AWSFileUploadComponent for Estimate File  
    **/
    static testMethod void myUnitTest4() { 
        GLSSetupData.setupData();
        Test.startTest();
        AWSFileUploadController uploadFile = new AWSFileUploadController();
        uploadFile.parentIdValue = GLSSetupData.quote.Id;
        uploadFile.parentObjStatus = GLSSetupData.quote.Status__c;
        uploadFile.FileNameStr='Test.txt';
        uploadFile.selectedFileType='Estimate File';
        uploadFile.createAWSRecords();
        uploadFile.newFileName='Test.txt';
        uploadFile.fileSize=9000;
        uploadFile.AmazonPath = 'Test Amazon path';
        uploadFile.successFileupload();
        uploadFile.updateParentObjAWSInfo();                
        uploadFile.FileNameStr='Test12.txt';
        uploadFile.createAWSRecords();
        uploadFile.deleteFileName='Test.txt';     
        uploadFile.selectedFileType='Estimate File';
        uploadFile.deleteUnuploadedFiles(); 
        uploadFile.selectedFileType='Estimate File';        
        system.assert( uploadFile.parentIdValue != null );                                                   
        Test.stopTest();
    }
    
          /**
        * This Unit Test method will test the AWSFileUploadComponent for Program Reference File 
    **/
    static testMethod void myUnitTest5() { 
        GLSSetupData.setupData();
        Test.startTest();
        AWSFileUploadController uploadFile = new AWSFileUploadController();
        uploadFile.parentIdValue = GLSSetupData.quote.Id;
        uploadFile.parentObjStatus = GLSSetupData.quote.Status__c;
        uploadFile.FileNameStr='Test.txt';
        uploadFile.selectedFileType='Program Reference File';
        uploadFile.createAWSRecords();
        uploadFile.newFileName='Test.txt';
        uploadFile.fileSize=9000;
        uploadFile.AmazonPath = 'Test Amazon path';
        uploadFile.successFileupload();
        uploadFile.updateParentObjAWSInfo();                
        uploadFile.FileNameStr='Test12.txt';
        uploadFile.createAWSRecords();
        uploadFile.deleteFileName='Test.txt';     
        uploadFile.selectedFileType='Program Reference File';
        uploadFile.deleteUnuploadedFiles(); 
        uploadFile.selectedFileType='Program Reference File';        
        system.assert( uploadFile.parentIdValue != null );                                                   
        Test.stopTest();
    }
    
          /**
        * This Unit Test method will test the AWSFileUploadComponent for Purchase Agreement File 
    **/
    static testMethod void myUnitTest6() { 
        GLSSetupData.setupData();
        Test.startTest();
        AWSFileUploadController uploadFile = new AWSFileUploadController();
        uploadFile.parentIdValue = GLSSetupData.quote.Id;
        uploadFile.parentObjStatus = GLSSetupData.quote.Status__c;
        uploadFile.FileNameStr='Test.txt';
        uploadFile.createNewPurcAgreeFileRecord();                                              
        Test.stopTest();
    }
    
    /**
    * This method will create dummy credentials for testing 
    **/
    private static String createTestCredentials(){
        AWS_Credential__c testKey = new AWS_Credential__c(name='test keys',key__c='key',secret__c='secret');
        insert testKey;
        return testKey.name;
     } 
    /**
    * This method run amazon's signature's and policy tester
    **/
    static testmethod void policyTest() {
            GLSSetupData.setupData();
            Test.startTest();
            AWSFileUploadController uploadFile = new AWSFileUploadController();
            uploadFile.parentIdValue = GLSSetupData.quote.Id;            
            String credName = createTestCredentials();
            uploadFile.AWSCredentialName = credName;
            uploadFile.getawss3LoginCredentials();
            try{
                uploadFile.getHexPolicy( );
                uploadFile.getSignedPolicy();
            }catch(Exception ex){
            }
           system.assert( uploadFile.getHexPolicy() != null );
           system.assert( uploadFile.getSignedPolicy() != null );
           Test.stopTest(); 
     }
}