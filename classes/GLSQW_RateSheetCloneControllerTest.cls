@isTest
public  class GLSQW_RateSheetCloneControllerTest {
  @isTest
    public static void TestCustomClonectlr() {
    test.startTest();
        Rate_Sheet__c rateSheet= new Rate_Sheet__c();
        rateSheet.Name='Amgen';
        insert rateSheet;
        Rate_Sheet_Item__c rsi= new Rate_Sheet_Item__c ();
        rsi.Hourly_Rate__c=10.00;
        rsi.Min_Amount_Description__c='test';
       // rsi.Name='test';
        rsi.Rate_Sheet__c=rateSheet.id;
        insert rsi;
        Rate_Sheet_Subitem__c rssi = new Rate_Sheet_Subitem__c();
        rssi.Description__c='test';
        //rssi.Name='test';
        rssi.Rate__c=10.00;
        rssi.Rate_Sheet_Item__c=rsi.id;
       ApexPages.CurrentPage().getparameters().put('id', rateSheet.id);
       Apexpages.StandardController stdController = new Apexpages.StandardController(rateSheet); 
        //ApexPages.StandardController sc = new ApexPages.standardController(rateSheet.id);
        GLSQW_RateSheetCloneController Gccl= new GLSQW_RateSheetCloneController(stdController);
       
        Gccl.Clonedata();
        Gccl.cancelclone();
        test.stoptest();
    }
     @isTest
      public static void TestCustomClonectlr1() {
       test.startTest();
       Rate_Sheet__c rateSheet1= new Rate_Sheet__c();
        rateSheet1.Name='Testdata12345';
        insert rateSheet1;
        Rate_Sheet__c rateSheet= new Rate_Sheet__c();
        rateSheet.Name='Testdata1234';
        insert rateSheet;
        Rate_Sheet_Item__c rsi= new Rate_Sheet_Item__c ();
        rsi.Hourly_Rate__c=10.00;
        rsi.Min_Amount_Description__c='test';
       // rsi.Name='test';
        rsi.Rate_Sheet__c=rateSheet1.id;
        insert rsi;
        Rate_Sheet_Subitem__c rssi = new Rate_Sheet_Subitem__c();
        rssi.Description__c='test';
        //rssi.Name='test';
        rssi.Rate__c=10.00;
        rssi.Rate_Sheet_Item__c=rsi.id;
        insert rssi;
       ApexPages.CurrentPage().getparameters().put('id', rateSheet1.id);
       Apexpages.StandardController stdController = new Apexpages.StandardController(rateSheet1); 
        //ApexPages.StandardController sc = new ApexPages.standardController(rateSheet.id);
        GLSQW_RateSheetCloneController Gccl= new GLSQW_RateSheetCloneController(stdController);
       Gccl.ratesheetname ='mytest';
        Gccl.Clonedata();
      //  Gccl.cancelclone();
        test.stoptest();
    }
    
}