@isTest
public class LMS_Test_PopulateUserTrainingTrigger {
public static Profile adminProfile,userProfile ;
@isTest

public static void testPopulateUserTrainingTrigger(){
   adminProfile = [Select Id from Profile where name = 'System Administrator'];
        User systemAdmin = new User(
                            
                            ProfileId = adminProfile.Id,
                            Username = 'systemAdmin1@test.com',
                            Alias = 'sadmin',
                            Email='test@test.com',
                            EmailEncodingKey='UTF-8',
                            Firstname='TestFN',
                            Lastname='TestLN',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US',
                            TimeZoneSidKey='America/Chicago' );
        Database.insert(systemAdmin);
         system.runAs(systemAdmin){
          User  Translator = new User(Username = 'userTranslatornew@test.com',                               
                                                ProfileId=  adminProfile.id,
                                                Alias = 'ut3',
                                                Email = 'ut1@test.com',
                                                EmailEncodingKey = 'UTF-8',
                                                LastName = 'McTesty3',
                                                CommunityNickname = 'ut3',
                                                TimeZoneSidKey = 'America/Los_Angeles',
                                                LocaleSidKey = 'en_US',
                                                LanguageLocaleKey = 'en_US',
                                                Training_Profile__c = 'Translator'
                                              );
           Database.insert(Translator);
          User projectManager = new User(Username = 'userProjectManager@test.com',                               
                                    ProfileId=  adminProfile.id,
                                    Alias = 'ut1',
                                    Email = 'ut1@test.com',
                                    EmailEncodingKey = 'UTF-8',
                                    LastName = 'McTesty',
                                    CommunityNickname = 'ut1',
                                    TimeZoneSidKey = 'America/Los_Angeles',
                                    LocaleSidKey = 'en_US',
                                    LanguageLocaleKey = 'en_US',
                                    Training_Profile__c = 'Project Manager'
                                  );
            Database.insert(projectManager);
          
           test.StartTest();
          Training__c training = new Training__c();
            
            training.Training_Name__c = 'Test Training';
            training.Training_Methodologies__c= 'Document Oriented';
            training.URL__c ='www.google.com';
            training.Training_Description__c='test description';
            training.Duration__c = 2;
            training.criterion__c = 30;
            training.start_date__c = Date.today();
            training.end_date__c = Date.today().addDays(20);
            training.skills__c='java';
            training.Trainer_Name__c= 'Mr Bruce';
            training.Active__c = true;
            training.Training_Required_for__c = 'Translator';
            try {
                insert training; 
            } catch (Exception e) {
                system.debug('Exception : ' + e);
            }
            Training__c training2= new Training__c();
            
            training2.Training_Name__c = 'Test Training2';
            training2.Training_Methodologies__c= 'Document Oriented';
            training2.URL__c ='www.google.com';
            training2.Training_Description__c='test description';
            training2.Duration__c = 2;
            training2.criterion__c = 30;
            training2.start_date__c = Date.today();
            training2.end_date__c = Date.today().addDays(20);
            training2.skills__c='java';
            training2.Trainer_Name__c= 'Mr Bruce';
            training2.Active__c = true;
            training2.Training_Required_for__c = 'Project Manager';
            try {
                insert training2; 
            } catch (Exception e) {
                system.debug('Exception : ' + e);
            }
            projectManager.Training_Profile__c = 'Translator';
                update projectManager ;
         
            
           
     /*   LMS_Test_Util util = new LMS_Test_Util();
           // LMS_Test_Util.training.Training_Required_for__c = 'Translator';
           update LMS_Test_Util.training;*/
          test.stopTest();
   }
    }
}