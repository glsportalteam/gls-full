global class Bitly {
    Public String mode;
    Public String sUrl;
    
    /*Returns the value of short URL*/
    public String getShortURL () {
       String shorten;
       XmlStreamReader reader;
       HttpResponse res;
       String shorturl ='';
       //First, build the http request
       Http h = new Http();
       HttpRequest req = buildWebServiceRequestForToken();
       if(!Test.isRunningTest()){
          res = invokeWebService(h, req);
       }
       String token = res.getBody();
       
       HttpRequest req2 = buildWebServiceRequest(token,sUrl);
       if(!Test.isRunningTest()){
          res = invokeWebService(h, req2);
       }
       Map<String,object> responseMap = (Map<String,obJect>)JSON.deserializeUntyped(res.getBody());
       shorturl = String.valueOf(((Map<String,obJect>)responseMap.get('data')).get('url'));
       
       return shorturl;
    }
    
    /*Return request object for shortening URL request*/
    public static HttpRequest buildWebServiceRequest(String token,String URL){
        String endpoint;
        HttpRequest req = new HttpRequest();
        endpoint = 'https://api-ssl.bitly.com/v3/shorten?access_token='+token+'&longUrl='+URL;
        req.setEndpoint(endpoint); 
        req.setMethod('GET');
        req.setTimeout(20000);
        return req;
    }
    
    /*Return request object for access token URL request*/
    public static HttpRequest buildWebServiceRequestForToken(){
        Bitly_Cred__c bitlyCred = Bitly_Cred__c.getValues('Bitly GLS Cred');
        String username = bitlyCred.User_Name__c;
        String pwd = bitlyCred.Password__c;
        String cId = bitlyCred.Client_Id__c;
        String cSecret = bitlyCred.Client_Secret__c;        
        
        HttpRequest req = new HttpRequest();
        if(username != null && pwd != null && cId != null && cSecret != null){
            String endpoint;                
            endpoint = 'https://api-ssl.bitly.com/oauth/access_token';
            req.setEndpoint(endpoint); 
            req.setMethod('POST');
            
            //Blob headerValue = Blob.valueOf('yash_mittal@persistent.co.in'+':'+'y@shmitt@l');
            Blob headerValue = Blob.valueOf(username+':'+pwd);
            String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);
            req.setHeader('grant_type', 'password');
            req.setHeader('client_id', cId);
            req.setHeader('client_secret', cSecret);
            req.setHeader('Authorization', authorizationHeader);
        }           
        return req;
    }
    
    /*Send request*/
    public static HttpResponse invokeWebService(Http h, HttpRequest req){
         
         //Invoke Web Service
         HttpResponse res = h.send(req);
         return res;
    }   
    /*
    
  Client Id:aeda903f44ad68daef622d0ae39970ccb0f86dcf
  Client Secret :fd1f8919ae466c7eab19f00746d2ccfbfd96c269
  Authentication Token:6eaee107e617a126dc31139599cf71f556b36215
    */
}