/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSLPW_InvoiceDetailControllerTest {

    static testMethod void testInvoicePage() {
       Account accObj                   	= GLSLPW_SetUpData.createAccount('Amgen');
       Contact conObj                   	= GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
       Program__c prgObj                	= GLSLPW_SetUpData.createProgram(accObj.Id,conObj.Id,'Program_Test', 'Amgen');
       Protocol__c protObj              	= GLSLPW_SetUpData.createProtocol(prgObj.Id,'Closed');
       Purchase_Agreement__c paObj      	= GLSLPW_SetUpData.createPurchaseAgreement(protObj.Id);
       Invoice__c invObj					= GLSLPW_SetUpData.createInvoice(paObj.Id);
       Invoice__c invObj1					= GLSLPW_SetUpData.createInvoice(paObj.Id);
       Invoice__c invObj2					= GLSLPW_SetUpData.createInvoice(paObj.Id);
       Quote__c quoteObj                	= GLSLPW_SetUpData.createQuote(accObj.Id,conObj.Id,'New');
       quoteObj.Invoice__c 					= invObj.Id;
       update quoteObj;
       ApexPages.currentPage().getParameters().put('invId', invObj.Id);
       Test.startTest();
	       GLSLPW_InvoiceDetailController obj 	= new GLSLPW_InvoiceDetailController(new ApexPages.StandardController(invObj)); 
	       obj.customEdit();
	       obj.customCancel();
	       obj.saveInvoice();
	       obj.deleteInvoice();
	       invObj1.Description__c ='Test Description';
	       update invObj1;
	       ApexPages.currentPage().getParameters().put('invId', invObj1.Id);
	       GLSLPW_InvoiceDetailController obj1 	= new GLSLPW_InvoiceDetailController(new ApexPages.StandardController(invObj1)); 
	       obj1.deleteInvoice();
	       ApexPages.currentPage().getParameters().put('invId',null);
	       GLSLPW_InvoiceDetailController obj2 	= new GLSLPW_InvoiceDetailController(new ApexPages.StandardController(invObj2)); 
       Test.stopTest();
    }
}