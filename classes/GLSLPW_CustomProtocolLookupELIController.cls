/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSLPW_CustomProtocolLookupELIController Class
* Function: This class is used as a controller for the GLSLPW_CustomProtocolLookupELI
*/

public with sharing class GLSLPW_CustomProtocolLookupELIController {
    
    //Variable declaration for the class
    public List<Protocol__c> lstProtocols       {get; set;} 
    public String searchString                  {get; set;}
    
    private String prgId;
    private String estId;
    
    //Constructor
    public GLSLPW_CustomProtocolLookupELIController() {
        prgId = system.currentPageReference().getParameters().get('pid');
        estId = system.currentPageReference().getParameters().get('estId');
        searchString = System.currentPageReference().getParameters().get('lksrch');
        lstProtocols = getProtocolsList();
    }   
    
    /* 
    @Description: Method to get the required list of Protocols to be displayed on Lookup page.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference search() {
        if(searchString != null){
            searchString = String.escapeSingleQuotes(searchString);
        }
        lstProtocols = getProtocolsList();
        return null;
    }
    
    /* 
    @Description: Method to get the required list of Protocols to be displayed on Lookup page
    @Params: None
    @Return Type: List<Protocol__c>
    */
    private List<Protocol__c> getProtocolsList(){
        List<Estimate_Line_Item__c> lstELICreated = new List<Estimate_Line_Item__c>();
        lstELICreated = [SELECT Protocol_Id__c
                        FROM Estimate_Line_Item__c
                        WHERE Estimate_Id__c =: estId];
        Set<Id> setUsedProtocols = new Set<Id>();
        for(Estimate_Line_Item__c eli : lstELICreated){
            setUsedProtocols.add(eli.Protocol_Id__c);
        }
        List<Protocol__c> lstProtocolsLocal = new List<Protocol__c>();
        String query = 'SELECT Id, Name, Protocol_Name__c FROM Protocol__c';
        if(prgId != null && searchString == ''){
            query += ' WHERE Program_Name__c = \'' +  prgId + '\' AND Id NOT IN : setUsedProtocols'; 
        }
        else if(prgId != null && searchString != ''){
            query += ' WHERE Program_Name__c = \'' +  prgId + '\' AND Protocol_Name__c LIKE \'%'+ searchString +'%\' AND Id NOT IN : setUsedProtocols';
        }
        query += ' LIMIT 250';
        return Database.query(query);
    }
    
    /* 
    @Description: Used by the visualforce page to send the link to the right dom element
    @Params: None
    @Return Type: String
    */
    public String getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }
    
    /* 
    @Description: Used by the visualforce page to send the link to the right dom element for the text box
    @Params: None
    @Return Type: String
    */
    public String getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }
}