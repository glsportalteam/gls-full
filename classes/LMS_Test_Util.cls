public class LMS_Test_Util {
public static Profile adminProfile,userProfile ;
public static User projectManager,systemAdmin ;
public static User translator;
public static Training__c training;
public static User_Training__c  userTraining;
public static PM_Feedback__c feedback;
public LMS_Test_Util (){

    adminProfile = [Select Id from Profile where name = 'System Administrator'];
    //userProfile = [Select Id from Profile where name = 'LMS User Training'];
    
    systemAdmin = new User(
                            
                            ProfileId = adminProfile.Id,
                            Username = 'systemAdmin1@test.com',
                            Alias = 'sadmin',
                            Email='test@test.com',
                            EmailEncodingKey='UTF-8',
                            Firstname='TestFN',
                            Lastname='TestLN',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US',
                            TimeZoneSidKey='America/Chicago' );
        Database.insert(systemAdmin);
        system.runAs(systemAdmin){
            projectManager = new User(Username = 'userProjectManager@test.com',                               
                                    ProfileId=  adminProfile.id,
                                    Alias = 'ut1',
                                    Email = 'ut1@test.com',
                                    EmailEncodingKey = 'UTF-8',
                                    LastName = 'McTesty',
                                    CommunityNickname = 'ut1',
                                    TimeZoneSidKey = 'America/Los_Angeles',
                                    LocaleSidKey = 'en_US',
                                    LanguageLocaleKey = 'en_US',
                                    Training_Profile__c = 'Project Manager'
                                  );
            Database.insert(projectManager);
        
            translator = new User( Username = 'userTranslator1@test.com',                               
                                    ProfileId=  adminProfile.id,
                                    Alias = 'ut12',
                                    Email = 'ut12@test.com',
                                    EmailEncodingKey = 'UTF-8',
                                    LastName = 'McTesty1',
                                    CommunityNickname = 'ut12',
                                    TimeZoneSidKey = 'America/Los_Angeles',
                                    LocaleSidKey = 'en_US',
                                    LanguageLocaleKey = 'en_US',
                                    Training_Profile__c = 'Translator'
                              );
             Database.insert(translator);
             
            training = new Training__c();
            
            training.Training_Name__c = 'Test Training';
            training.Training_Methodologies__c= 'Document Oriented';
            training.URL__c ='www.google.com';
            training.Training_Description__c='test description';
            training.Duration__c = 2;
            training.criterion__c = 30;
            training.start_date__c = Date.today();
            training.end_date__c = Date.today().addDays(20);
            training.skills__c='java';
            training.Trainer_Name__c= 'Mr Bruce';
            training.Active__c = true;
            training.Training_Required_for__c = 'Project Manager';
            try {
                insert training; 
            } catch (Exception e) {
                system.debug('Exception : ' + e);
            }
            
            userTraining = new User_Training__c();
            
            userTraining.Training__c = training.id;           
            userTraining.status__c = 'In Progress';           
            userTraining.User_Name__c = projectManager.id;
            userTraining.Date_Of_Enrollment__c = Date.today();
            
            User_Training__c userTrainingCompleted = new User_Training__c();
            
            userTrainingCompleted.Training__c = training.id;           
            userTrainingCompleted.status__c = 'Completed';
            userTrainingCompleted.Score__c = 80;
            userTrainingCompleted.result__c = 'Pass';
            userTrainingCompleted.IsFeedbackSubmitted__c = true;
            userTrainingCompleted.Date_of_Completion__c = Date.today().addDays(5);         
            userTrainingCompleted.User_Name__c = projectManager.id;
            userTrainingCompleted.Date_Of_Enrollment__c = Date.today();
            
            Database.insert(userTrainingCompleted);   
            RecordType rectype = new RecordType();
            rectype.name = 'Training Feedback';
            //rectype.SobjectType  = 'PM_Feedback__c';
            
            
            feedback = new PM_Feedback__c();
            feedback.RecordType = rectype;
            feedback.Enrollment_UserTraining__c = userTrainingCompleted.id;
           // feedback.Best_Aspect_Of_Training__c = '    ';
            feedback.Training_Areas_To_Improve__C = 'abc';
            feedback.Further_Comments__c = 'abc';
            feedback.Overall_Training_Session__c ='Excellent';
            feedback.Trainer_Skills__c='Excellent';
            
            feedback.Training_Materials__c= 'Excellent';
            
            
           Database.insert(feedback); 
            
          training.Training_Required_for__c = 'Translator';
           update training;
         /* projectManager.Training_Profile__c = 'Translator';
            update projectManager; 
          */
                          
          }
    }

}