/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSLPW_ProtocolControllerTest {

    static testMethod void myUnitTest() {
        Account accObj = GLSLPW_SetUpData.createAccount('Amgen');
        Contact conObj = GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
        Program__c prgObj = GLSLPW_SetUpData.createProgram(accObj.Id,conObj.Id,'Program_Test', 'Amgen');
        Protocol__c protObj1 = GLSLPW_SetUpData.createProtocol(prgObj.Id,'Draft');
              
        Purchase_Agreement__c paObj1 = GLSLPW_SetUpData.createPurchaseAgreement(protObj1.Id);
        Purchase_Agreement__c paObj2 = GLSLPW_SetUpData.createPurchaseAgreement(protObj1.Id);
        Purchase_Agreement__c paObj3 = GLSLPW_SetUpData.createPurchaseAgreement(protObj1.Id);
        
        Country__c cObj1 = GLSLPW_SetUpData.createCountry('Test Country');
        Protocol_Country__c pcObj1 = GLSLPW_SetUpData.createProtocolCountry(protObj1.Id,cObj1.Id,5,'Active');
        Protocol_Country__c pcObj2 = GLSLPW_SetUpData.createProtocolCountry(protObj1.Id,cObj1.Id,6,'Active');
        
        Estimate__c eObj1 = GLSLPW_SetUpData.createEstimate(prgObj.Id, 'Approved');
        Estimate_Line_Item__c eliObj1 = GLSLPW_SetUpData.createELI(protObj1.Id, eObj1.Id);
        Estimate_Line_Item__c eliObj2 = GLSLPW_SetUpData.createELI(protObj1.Id, eObj1.Id);
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('Id', protObj1.Id);
        GLSLPW_ProtocolController obj = new GLSLPW_ProtocolController(new ApexPages.StandardController(protObj1));
        obj.paId = paObj2.Id;
        obj.deleteRecord();
        Test.stopTest();
         }
}