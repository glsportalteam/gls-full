/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSDeleteQuotePDFScheduler  Class
* Function: Scheduler to schedule the batch GLSDeleteQuotePDFBatch.
*/
global class GLSDeleteQuotePDFScheduler implements Schedulable{
    global void execute(SchedulableContext sc){
        GLSDeleteQuotePDFBatch deleteInstance = new GLSDeleteQuotePDFBatch();
        Id batchprocessid = Database.executeBatch(deleteInstance,50); 
    }
}