global with sharing class GLSGenerateQuotePDFController {
    
    public Quote__c quote{get;set;}
    public String quoteId ;
    public String orgAddress{get;set;}
    public String reference{get;set;}
    public String glsReference{get;set;}
    public String assignedTo{get;set;}
    public String orgContactNumber{get;set;}
    public String orgWebsite{get;set;}
    public List<Quote_Assignment__c> lstQAssgn {get;set;}
    public static String bucket = GLSconfig.bucketName;
    public List<Quote_Line_Item__c> lstQLI1{get;set;}
    public List<Quote_Line_Item__c> lstQLI{get;set;}
    public Decimal totalAmount {get;set;}
    public List<RFQ_SOW_Items_Table__c> lstRfqSOW{get;set;}
    public List<RFQ_Terms_Table__c> lstRfqTerms{get;set;}
    public List<RFQ_Special_Provisions__c> lstRQFSpecialProvisions{get;set;}
    public List<Quote_Assignment_Special_Provisions__c> lstQASpecialProvisions{get;set;}
    public Decimal expeditedSurcharge{get;set;}
    public String opTerms1{get;set;}
    public String opTerms2{get;set;}
    public String GLSQuotePDFFooterDocNum{get;set;}
    public List<String> quoteTermList{get;set;}
    public String crSHead {get;set;}
    public String crSterms {get;set;}
    public String crSClause1 {get;set;}
    public String crSClause2 {get;set;}
    public List<String> lstOpTerms {get;set;}
    public String pageURL {get; set;}
    private AWSKeys credentials {get;set;}
    private String AWSCredentialName = 'NAME OF KEY TO USE';
    private String secret { get {return credentials.secret;} }
    private S3.AmazonS3 as3 { get; private set; }
    public String key { get {return credentials.key;} }
    global static Boolean executeTrigger = false;
    
    public GLSGenerateQuotePDFController(ApexPages.StandardController controller){
        getawss3LoginCredentials();
        lstRQFSpecialProvisions = new List<RFQ_Special_Provisions__c>();
        lstQASpecialProvisions = new List<Quote_Assignment_Special_Provisions__c>();
        lstRfqSOW = new List<RFQ_SOW_Items_Table__c>();
        lstRfqTerms = new List<RFQ_Terms_Table__c>();
        crSHead = '';
        crSterms = '';
        crSClause1 = '';
        crSClause2 = '';
        quoteId = ApexPages.currentPage().getParameters().get('id');
        pageURL = '/apex/GLSGenerateQuotePDF?id='+quoteId;
        quote = [SELECT Id,Terms__c,CreatedDate,Actual_Total_Amount__c,QLIESOverridden__c, Quote_Name__c, Quote_Name_GLS__c, Quote_Number__c, Project_Management_Amount__c, Project_Sub_Total__c, Display_Languages_Without_Dialect__c, Discount_Percentage__c,Display_Rates_per_item_on_on_Quotes__c,Display_Subtotal_per_Language_pair__c,Contact__r.Email,Contact__r.MailingCountry,Contact__r.MailingPostalCode,Name,Total_Amount__c,
        Client__r.owner.Name, Client__r.Owner.Title, Client__r.owner.Email, Client__r.owner.Phone, Contact__r.MailingStreet,Contact__r.MailingCity,Contact__r.Name,Contact__r.Account.Name,Owner.Email,Owner.Title,
        Owner.Phone,Owner.Name,Quote_Due_Date__c,Notes__c,Quote_Introduction__c,Delivery_Schedule__c,Expedited_Surcharge_Amount__c,
        Assigned_To__r.Name,File_Prep_Format_Surcharge_Amount__c,
        Show_Expedited_Delivery_Schedule__c,Show_Manual_Delivery_Schedule__c,Addendums__c,Contact__r.Salutation,    Actual_Sub_Total_Amount__c,
        (SELECT id,File_Name__c FROM Quotes_Files__r),Signature_Block_Text__c,Contact__r.Phone,Contact__r.MailingState,
        (SELECT Name, Source_Language__c FROM Quote_Assignments__r),(SELECT IsStandard__c,Amount__c,Outsourced_Amount__c,Outsourced_Quantity__c,Outsourced_Service__c,Outsourced_Unit_Price__c,Outsourced_Service_Percentage__c,
        Quote__c,Standard_Amount__c,Standard_Quantity__c,Standard_Service__c,Standard_Unit_Price__c FROM Quote_Level_Line_Item__r),
        (SELECT id,Discount__c,Discount_Percentage__c,Discounted_Amount__c,Quote__c FROM Quote_Discount__r),
        (SELECT Service__r.Name, System_Standard_Delivery_Schedule__c, System_Expedited_Delivery_Schedule__c, Manual_Standard_Delivery_Schedule__c,
        Manual_Expedited_Delivery_Schedule__c,Expedited_Delivery_Schedule__c,Standard_Delivery_Schedule__c FROM Delivery_Schedule__r order by   Service__r.Order__c)
        FROM Quote__c WHERE Id =: quoteId LIMIT 1];
        expeditedSurcharge= quote.Expedited_Surcharge_Amount__c;
        totalAmount = quote.Total_Amount__c;
        
        if(quote.Quote_Name__c != null && quote.Quote_Name__c != '' )
            reference = 'Protocol/Reference: '+quote.Quote_Name__c;
        else
            reference ='';
        if(quote.Quote_Name_GLS__c != null && quote.Quote_Name_GLS__c != '' )
            glsReference = 'Sponsor/Reference: '+quote.Quote_Name_GLS__c;
        else
            glsReference ='';
        
        if(quote.Assigned_To__r.Name == null)
            assignedTo = quote.Owner.Name;
        else
            assignedTo = quote.Assigned_To__r.Name;
        
        String quoteTerm = quote.Terms__c;
        quoteTermList =  new List<String>();
        
        if(quoteTerm != null)
        {
            String[] quoteTermArray = quoteTerm.split('\n\n');
            
            for(String qTerm: quoteTermArray)
            {
                quoteTermList.add(qTerm);
            }
        }
        
        lstOpTerms = new List<String>();
        if(quote.Signature_Block_Text__c != null)
        lstOpTerms = quote.Signature_Block_Text__c.split('<br><br>');
        if(lstOpTerms != null && lstOpTerms.size() == 2){
            opTerms1 = lstOpTerms[0];
            opTerms2 = lstOpTerms[1];
        }
        lstRQFSpecialProvisions = [SELECT Standard_Special_Provisions__c, RFQ_Special_Provision_Text__c, RFQ_Special_Provision_Name__c,
        RFQ_Special_Provision_Modified__c, RFQ_Id__c, Name, Id FROM RFQ_Special_Provisions__c WHERE RFQ_Id__c =: quoteId];
        //If(!Test.isRunningTest()){
        lstQAssgn = [SELECT Name, Special_Instructions__c, Source_Language__c,Allowed_File_Formats__c,
        Source_Language__r.Name,Delivery_File_Format__r.Name,Quote_Reference_Files_Instruction__c,Quote_Task_Description__c,
        (SELECT Standard_Special_Provisions__c, Quote_Assignment_Special_Provision_Text__c, Quote_Assignment_Special_Provision_Name__c,
        Name, Id,Quote_Assignment_Id__r.Quote__c,Quote_Assignment_Id__c
        FROM Quote_Assignment_Special_Provisions__r WHERE Quote_Assignment_Id__r.Quote__c =: quoteId order by name ),
        (SELECT Name,Language_Name__c,Language_List_Item__r.Name FROM Quote_Assignment_Languages__r),Delivery_Format__c,
        (SELECT Quote_Files__c,Quote_Files__r.File_Name__c,File_Usage__c FROM Quote_Assignment_Files__r),
        (SELECT Client_Service__c FROM Quote_Assignment_Services__r)
        FROM Quote_Assignment__c WHERE Quote__c =: quoteId];
        lstRfqSOW = [SELECT RFQ_SOW_Item_Order__c, RFQ_SOW_Item_Name__c, RFQ_SOW_Item_Description__c, RFQ_SOWItemDescription__c, RFQ_Id__c
        FROM RFQ_SOW_Items_Table__c WHERE RFQ_Id__c =: quoteId ORDER BY RFQ_SOW_Item_Order__c];
        lstRfqTerms = [SELECT RFQ_Term_Order__c, RFQ_Term_Description__c, RFQ_Id__c
        FROM RFQ_Terms_Table__c WHERE RFQ_Id__c =: quoteId ORDER BY RFQ_Term_Order__c];
        

        if(lstRfqSOW != null && lstRfqSOW.size()>0){
            for(RFQ_SOW_Items_Table__c rSIT: lstRfqSOW ){
                if(rSIT.RFQ_SOW_Item_Name__c.equalsIgnoreCase('Third-party and Client Reviewers') || rSIT.RFQ_SOW_Item_Name__c.equalsIgnoreCase('Third-party and Client Reviewers(INC)')){
                    crSHead = 'Third-party and client reviewers are expected to follow GLS client review guidelines, as follows:';
                }
            }
        }
        List<Id> lstIds = new List<Id>();
        if(lstQAssgn != null && lstQAssgn.size()>0){
            for(Quote_Assignment__c qa : lstQAssgn){
                lstIds.add(qa.Id);
            }
        }
        lstQLI1 = [SELECT Id,Add_Discount__c,of_Pages__c, Rate_Type__c,Expedited_Surcharge__c,Expedited_Total_Amount__c, isTMFound__c,isReadyForAnalyse__c,isProcessed__c,isAnalyzed__c,XML_Data__c,X95_99_Word_Count__c,X85_95_Word_Count__c,X75_84_Word_Count__c,
        X50_74_Word_Count__c,X100_Word_Count__c,Unit_of_Measure__c,Translation_Memory_Options_Flag__c,Trados_Language_Pair__c,Additional_Line_Items__c,Total_Amount__c,Discounted_Percentage__c,
        Task_Status__c,Target_Language_ID__c,Target_Language_ID__r.Language_Name__c,TM_Export_Zip_File__c,TM_Export_Transaction_ID__c,TM_Export_File_ID__c,Subline_Total_Amount__c,
        Add_Outsourced_Services__c,Sub_Total_Amount__c, Source_Language_ID__c,Source_Language_ID__r.Language_Name__c,Service_Words__c,Repetitions_Word_Count__c,Rate__c, Quote_Line_Item_Id__c,QuoteID__c,Quantity__c,
        Perfect_Match__c,No_Match_Word_Count__c,Name,Min_Charge_Applied__c, Line_Item_Duration__c,File_Prep_Description__c,File_Prep_Amount__c,Extra_Days__c,
        Error_Message__c,Crossfile_Repetitions__c,Context_TM_Word_Count__c,Client_TM__c,Analyzed_Version_ID__c,DTP_Hours__c, Analyzed_Transaction_ID__c,
        Analyzed_File_URL__c,SubTotalAmount__c,Actual_Words__c,Back_Translation_Total_Words__c,File_Prep_Format_Surcharge_Amount__c,File_Prep_Format_Surcharge_Percentage__c,QuoteID__r.Total_Amount__c,Show_BT_TW_Count__c,Show_TW_on_Quote__c,
        (SELECT id,Discount__c,Discount_Percentage__c,Subline_Percentage__c FROM Quote_Line_Item_Discount__r),
        (SELECT Unit_of_Measure__c,Unit_Price__c, Quantity__c, IsOutsourced__c , Manual_Entry__c,Description__c, Amount__c,Client_Service__c, Client_Service__r.Name,default_order__c FROM Quote_Line_Item_Sublines__r Order by default_order__c, Default_Subline_Order__c)
        FROM Quote_Line_Item__c  WHERE QuoteID__c =: quoteId order by Trados_Language_Pair__c ];
        
        //List<String> langPairList = new List<String>();
        
        String langPair = '';
        lstQLI = new List<Quote_Line_Item__c>();
        
        
        for(Quote_Line_Item__c qli : lstQLI1){
            totalAmount += qli.QuoteID__r.Total_Amount__c;
        }
        
        if(!quote.Display_Languages_Without_Dialect__c)
        {
            
            for(Quote_Line_Item__c qli : lstQLI1)
            {
                
                String[] langPairList = qli.Trados_Language_Pair__c.split('-');
                
                if(langPairList.size() > 0)
                {
                    
                    if(langPairList[0].split('\\(').size() > 0 )
                    {
                        
                        if(langPairList[1].split('\\(').size() > 1)
                        {
                            langPair = langPairList[0].split('\\(')[0] + '- ' + langPairList[1].split('\\(')[0] ;
                        }else
                        {
                            langPair = langPairList[0].split('\\(')[0] + '- ' + langPairList[1];
                        }
                        
                        
                    } else
                    {
                        
                        if(langPairList[1].split('\\(').size() > 0)
                        {
                            langPair = langPairList[0] + '- ' + langPairList[1].split('\\(')[0] ;
                        }else
                        {
                            langPair = langPairList[0] + '- ' + langPairList[1];
                        }
                        
                    }
                    
                }
                
                qli.Trados_Language_Pair__c = langPair;
                
                lstQLI.add(qli);
                
            }
        }else
        {
            lstQLI = lstQLI1;
        }
        /*We are not able to perform DML on the Organization. 
        Hence the following piece of code is not allowed to execute when the test class is run.*/
        If(!Test.isRunningTest()){ 
            List<Quote_Assignment_Service__c> lstQAS = [SELECT Client_Service__c,Quote_Assignment__c,Name
            FROM Quote_Assignment_Service__c WHERE Quote_Assignment__c in: lstIds];
            Organization orgData = [SELECT Street, State, Phone, Fax, Country, City, PostalCode FROM Organization ];
            orgAddress = orgData.Street +', '+orgData.City +', '+orgData.State +' '+ orgData.PostalCode +', '+ orgData.Country;
            
            if(orgData.Phone != null && orgData.Fax == null){
                orgContactNumber = 'Phone: '+ orgData.Phone;
            }else if(orgData.Fax != null && orgData.Phone == null){
                orgContactNumber = 'Facsimile: '+orgData.Fax;
            }else if (orgData.Phone == null && orgData.Fax == null) {
                orgContactNumber = '';
            }else
                orgContactNumber = 'Phone: '+ orgData.Phone +'    Facsimile: '+orgData.Fax;       
        }
        orgWebsite = 'www.globallanguages.com';
        String description;
        String allDeliveryFormats = '';
        List<String> lstLangPair = new List<String>();
        
        List<GLS_Quote_PDF_Footer__c> GLSQuotePDFFooterDocNumList  = new List<GLS_Quote_PDF_Footer__c>();
        GLSQuotePDFFooterDocNumList = GLS_Quote_PDF_Footer__c.getall().values();
        System.debug('GLSQuotePDFFooterDocNumList====='+GLSQuotePDFFooterDocNumList);
        if(GLSQuotePDFFooterDocNumList.size()>0)
        {
            //GLSQuotePDFFooterDocNum = 'F-760.001.AK';
            GLSQuotePDFFooterDocNum = GLSQuotePDFFooterDocNumList[0].GLSQuotePDFFooterDocNum__c; // Changes made by Amit 11th May 2016
        }
    }
    
    /* 
    @Description: To get the login credentials for S3 Amazon Web Service
    @Params: None
    @Return Type: PageReference
    */
    public PageReference getawss3LoginCredentials(){
        try{
            credentials = new AWSKeys(AWSCredentialName);
            as3 = new S3.AmazonS3(credentials.key,credentials.secret);
        }catch(AWSKeys.AWSKeysException AWSEx){
            System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        return null;
    }
    
    Datetime expire = system.now().addDays(1);
    String formattedexpire = expire.formatGmt('yyyy-MM-dd')+'T'+expire.formatGmt('HH:mm:ss')+'.'+expire.formatGMT('SSS')+'Z';
    String policy { get {return
        '{ "expiration": "'+formattedexpire+'","conditions": [ {"bucket": "'+
        bucket +'" } ,{ "acl": "'+
        'private'+'" },'+
        '{"success_action_status": "201" },'+
    '["starts-with", "$key", ""] ]}';       } }
    
    /* 
    @Description: To get the base64 encoded policy for policy
    @Params: None
    @Return Type: String
    */
    public String getPolicy() {
        return EncodingUtil.base64Encode(Blob.valueOf(policy));
    }
    
    /* 
    @Description: To get the crypto encrypted - base64 encoded policy for signature
    @Params: None
    @Return Type: String
    */
    public String getSignedPolicy() {
        return make_sig(EncodingUtil.base64Encode(Blob.valueOf(policy)));
    }
    
    /* 
    @Description: To get the crypto encrypted - base64 encoded policy for signature
    @Params: Base64 encoded policy (String)
    @Return Type: String
    */
    private String make_sig(String canonicalBuffer) {
        String macUrl ;
        String signingKey = EncodingUtil.base64Encode(Blob.valueOf(secret));
        Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof(Secret));
        macUrl = EncodingUtil.base64Encode(mac);
        return macUrl;
    }
    
    /* 
    @Description: To get the name of bucket where the files are to be uploaded in S3.
    @Params: None
    @Return Type: String
    */
    public String bucketName {
        get{
            bucketName = GLSConfig.bucketName;
            return bucketName;
        }
        private set;
    }
    
    /* 
    @Description: Remote Action to upload files to S3 and create Quote PDF record to obtain the file path
    @Params: QuoteId (Id)
    @Return Type: String
    */
    @RemoteAction
    public static String insertAttachment(Id quoteId) {
        if(quoteId != null){
            String pdfContent;
            String ParentId = quoteId;
            PageReference pdf = new PageReference('/apex/GLSQuoteAttachment?id='+quoteId);
            Date d = System.today();
            Integer day = d.day();
            Integer month = d.month();
            Integer year = d.year();
            String dateToday = year + '_' + month + '_' + day;
            List<Quote__c> lstCurrentQuote = new List<Quote__c>();
            Set<Id> setAWSFileId = new Set<Id>();
            
            //Get all the AWS File record Id for all the Quote PDF files for the current Quote
            List<Quote_PDF_File__c> lstQuotePDF = [SELECT AWS_File_Id__c FROM Quote_PDF_File__c WHERE Quote_ID__c =: quoteId AND Method__c =: GLSConfig.Automatic];
            if(lstQuotePDF != null && lstQuotePDF.size() > 0){
                for(Quote_PDF_File__c qpdf : lstQuotePDF){
                    if(qpdf.AWS_File_Id__c != null){
                        setAWSFileId.add(qpdf.AWS_File_Id__c);
                    }
                }
            }
            
            //Get the file names for all the AWS files from the AWS File Version
            List<AWS_File_Version__c> lstAWSFileVersion = new List<AWS_File_Version__c>();
            if(!setAWSFileId.isEmpty()){
                lstAWSFileVersion = [SELECT File_Name__c, Id FROM AWS_File_Version__c WHERE AWS_File_Id__c IN : setAWSFileId ORDER BY CreatedDate DESC];
            }
            lstCurrentQuote = [SELECT Quote_Number__c, Client__r.Name, Id FROM Quote__c WHERE Id =: quoteId LIMIT 1];
            
            Decimal version = 0;
            String fileName;
            
            if(lstAWSFileVersion != null && lstAWSFileVersion.size() > 0){
                String fileNameLatest = lstAWSFileVersion[0].File_Name__c;
                //Check if it is the nth upload for the same day - to maintain version
                if(fileNameLatest.contains(dateToday)){
                    String strVersion = fileNameLatest.substringBetween(dateToday, '.');
                    //Remove leading _ from the file name
                    if(strVersion != '' && strVersion.contains('_')){
                        //Append incremented version for format like 2015_7_3_4.pdf
                        version = Integer.ValueOf(strVersion.substringAfterLast('_')) + 1;
                    }
                    else{
                        //Append version as 1 for format like 2015_7_3.pdf
                        version = 1;
                    }
                    fileName = lstCurrentQuote[0].Quote_Number__c + '_' + lstCurrentQuote[0].Client__r.Name + '_' + dateToday + '_' + version + GLSConfig.pdfExtension;
                }
                //If the upload is for different day, version is not appended in the file name
                else{
                    fileName = lstCurrentQuote[0].Quote_Number__c + '_' + lstCurrentQuote[0].Client__r.Name + '_' + dateToday + GLSConfig.pdfExtension;
                }
            }
            else{
                fileName = lstCurrentQuote[0].Quote_Number__c + '_' + lstCurrentQuote[0].Client__r.Name + '_' + dateToday + GLSConfig.pdfExtension;
            }

            Blob body;
            try{
                body = pdf.getContent();
            }catch (VisualforceException e) {
                body = Blob.valueOf('Error While attaching Quote PDF');
            }
            pdfContent = EncodingUtil.base64Encode(body);
            
            Quote_PDF_File__c quotePDF = new Quote_PDF_File__c(Method__c = GLSConfig.Automatic, 
                                                               Quote_ID__c = quoteId, 
                                                               File_Name__c = fileName);
            try{
                insert quotePDF;
            }
            catch(DmlException de){
                System.debug(GLSConfig.DMLExceptionOccured);
            }
            catch(Exception e){
                System.debug(GLSConfig.ExceptionOccured);
            }
            Quote_PDF_File__c quotePDFList = [SELECT AWSFilePath__c FROM Quote_PDF_File__c WHERE Id =: quotePDF.Id LIMIT 1];
            QuotePDFValues obj = new QuotePDFValues(quotePDFList.AWSFilePath__c, pdfContent, quotePDF.File_Name__c, quotePDF.Id);
            
            List<Attachment> lstAttachment = [SELECT Id FROM Attachment WHERE ParentId =: quoteId LIMIT 1];
            if(lstAttachment != null && lstAttachment.size() > 0){
                try{
                    delete lstAttachment;
                }
                catch(DmlException de){
                    System.debug(GLSConfig.DMLExceptionOccured);
                }
                catch(Exception e){
                    System.debug(GLSConfig.ExceptionOccured);
                }
            }
            Attachment att = new Attachment(Name = quotePDF.File_Name__c, isPrivate = false, Body = body, ParentId = quoteId);
            insert att;
            
            return JSON.serialize(obj);
        }
        else{
            return JSON.serialize(null);
        }
    }
    
    /* 
    @Description: Remote Action to create AWS File record and update Quote PDF record with AWS Record Id.
    @Params: Header, File Name, Full Path, Quote PDF Record Id (String, String, String Id)
    @Return Type: String
    */   
    @RemoteAction
    public static String successFileUpload(String header, String fName, String fullPath, Id quotePDFId){
        /*Records for AWS File and Quote PDF are created only when Quote PDF is generated through browser. 
        Also, single quote ID is communicated from the page at a time. Hence code is not bulkified here.*/
        executeTrigger = true;
        AWS_File__c awsRecord = new AWS_File__c(Bucket_Name__c = GLSConfig.bucketName, Amazon_S3_source_file_path__c = fullPath,
                                                File_Name__c = fName, version_id__c = header, Record_Type__c = GLSConfig.QuotePDF);
        try{
            insert awsRecord; //Trigger called to insert record for AWS File sharing and AWS File Version
        }
        catch(DmlException de){
            System.debug(GLSConfig.DMLExceptionOccured);
        }
        catch(Exception e){
            System.debug(GLSConfig.ExceptionOccured);
        }
        
        Quote_PDF_File__c qpdf = new Quote_PDF_File__c (Id = quotePDFId);
        if(qpdf != null){
            qpdf.AWS_File_Id__c = awsRecord.Id;
        }
        try{
            update qpdf;
        }
        catch(DmlException de){
            System.debug(GLSConfig.DMLExceptionOccured);
        }
        catch(Exception e){
            System.debug(GLSConfig.ExceptionOccured);
        }
        
        Quote__c quoteObj = new Quote__c(Id = [SELECT Quote_ID__c FROM Quote_PDF_File__c WHERE Id =: quotePDFId LIMIT 1].Quote_ID__c);
        quoteObj.File_Id__c = awsRecord.Id;
        try{
            update quoteObj;
        }
        catch(DmlException de){
            System.debug(GLSConfig.DMLExceptionOccured);
        }
        catch(Exception e){
            System.debug(GLSConfig.ExceptionOccured);
        }
        
        return header;
    }
    
    /* 
    @Description: Remote Action to delete Quote PDF records if file was not successfully uploaded to S3
    @Params: Quote PDF Id (Id)
    @Return Type: String
    */
    @RemoteAction
    public static String deleteQuotePDF(Id quotePDFId) {
        if(quotePDFId != null){
            list<Quote_PDF_File__c> lstQuotePDF = [SELECT Id FROM Quote_PDF_File__c WHERE Id =: quotePDFId LIMIT 1];
            delete lstQuotePDF;
        }
        return null;
    }
    
    //Inner Class to hold values to be passed to Remote Action
    public class QuotePDFValues{
        public String filePath {get; set;}
        public String attachValue {get; set;}
        public String fileName{get; set;}
        public Id quotePDFId {get; set;}
        public QuotePDFValues(String filePath, String attachValue, String fileName, Id quotePDFId){
            this.filePath = filePath;
            this.attachValue = attachValue;
            this.fileName = fileName;
            this.quotePDFId = quotePDFId;
        }
    }
}