/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSLPW_PCDetailControllerExt Class
* Function: This class is used as a controller extension for the GLSLPW_ProtocolCountryDetailPage
*/

public with sharing class GLSLPW_PCDetailControllerExt {
    
    //Variable declaration for the class
    public Protocol_Country__c pcObj                {get; set;}
    public Boolean isEditMode                       {get; set;}
    
    private Id pcId;
    
    //Constructor
    public GLSLPW_PCDetailControllerExt(ApexPages.StandardController stdcon){
        pcId = ApexPages.currentPage().getParameters().get('id');
        if(pcId != null){
            pcObj = [SELECT Id, Number_of_Sites__c, Country_Id__c, Protocol_Id__c, Protocol_Country__c.Name 
                    FROM Protocol_Country__c
                    WHERE Id =: pcId LIMIT 1];
        }
        isEditMode = false;
    }
    
    /* 
    @Description: Action method called when Save is clicked
    @Params: None
    @Return Type: Void
    */
    public void customSave(){
        try{
            update pcObj;
        }
        catch(DMLException de){
            system.debug(GLSConfig.DMLExceptionOccured + de);
        }
        catch(Exception e){
            system.debug(GLSConfig.ExceptionOccured + e);
        }
        isEditMode = false;
    }
    
    /* 
    @Description: Action method called when Edit is clicked
    @Params: None
    @Return Type: Void
    */
    public void customEdit(){
        isEditMode = true;
    }
    
    /* 
    @Description: Action method called when Cancel is clicked
    @Params: None
    @Return Type: Void
    */
    public void customCancel(){
        isEditMode =  false;
    }
    
    /* 
    @Description: Action method called when Delete is clicked
    @Params: None
    @Return Type: Void
    */
    public PageReference customDelete(){
        PageReference pRef = null;
        try{
            delete pcObj;
        }
        catch(DMLException de){
            system.debug(GLSConfig.DMLExceptionOccured + de);
        }
        catch(Exception e){
            system.debug(GLSConfig.ExceptionOccured + e);
        }
        isEditMode = false;
        pRef = new PageReference('/apex/GLSLPW_ProtocolDetailPage?id=' + pcObj.Protocol_Id__c);
		pRef.setRedirect(true);
		return pref;
    }
}