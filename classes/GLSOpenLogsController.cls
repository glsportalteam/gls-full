/* Copyright (c) 2014, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSOpenLogsController  Class
* Function: Show list of GLS API Logger associated with the Quote Line Item
*/
public with sharing class GLSOpenLogsController{
    
        public string QuoteLineItemId                   {get;set;}
        public List<GLS_API_Logger__c>lstGLSAPILogger   {get;set;}
        public string qliLanguagePair                   {get;set;}
        /**
        * @Description : Constructor    
        **/ 
         public GLSOpenLogsController(ApexPages.StandardController controller) {
            QuoteLineItemId = ApexPages.currentPage().getParameters().get('qliId');
            lstGLSAPILogger= new List<GLS_API_Logger__c>();
            lstGLSAPILogger =[select id,API_Rest_URL__c,API_Listener_Logger__c,API_Listener_Logger__r.Name,Error_Message__c,Quote_Line_Item_Number__c,
                              Quote_Line_Item_Number__r.Name,Name,Response_of_API_call__c,Status__c,Timestamp__c,URL_Parameter__c
                              from GLS_API_Logger__c where Quote_Line_Item_Number__c=:QuoteLineItemId];
            list<Quote_Line_Item__c> lstQLI= [select id,name,Trados_Language_Pair__c from Quote_Line_Item__c where id=:QuoteLineItemId ];
            //Set the quote line item name in variable
            if( lstQLI!= null && lstQLI.size() > 0){
                qliLanguagePair=lstQLI[0].Trados_Language_Pair__c;
            }
         }

}