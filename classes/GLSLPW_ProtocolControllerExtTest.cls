/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSLPW_ProtocolControllerExtTest {
	static testMethod void setUpTestData(){
	    Account accObj = GLSLPW_SetUpData.createAccount('Amgen');
        Contact conObj = GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
        Program__c prgObj = GLSLPW_SetUpData.createProgram(accObj.Id,conObj.Id,'Program_Test', 'Amgen');
        Protocol__c protObj1 = GLSLPW_SetUpData.createProtocol(prgObj.Id,'Closed');
		
		Purchase_Agreement__c paObj1 = GLSLPW_SetUpData.createPurchaseAgreement(protObj1.Id);
        Purchase_Agreement__c paObj2 = GLSLPW_SetUpData.createPurchaseAgreement(protObj1.Id);
        Purchase_Agreement__c paObj3 = GLSLPW_SetUpData.createPurchaseAgreement(protObj1.Id);
       
        Country__c cObj1 = GLSLPW_SetUpData.createCountry('Test Country');
        Protocol_Country__c pcObj1 = GLSLPW_SetUpData.createProtocolCountry(protObj1.Id,cObj1.Id,5,'Active');
        Protocol_Country__c pcObj2 = GLSLPW_SetUpData.createProtocolCountry(protObj1.Id,cObj1.Id,6,'Active');
        
        Estimate__c eObj1 = GLSLPW_SetUpData.createEstimate(prgObj.Id, 'Approved');
        Estimate_Line_Item__c eliObj1 = GLSLPW_SetUpData.createELI(protObj1.Id, eObj1.Id);
        Estimate_Line_Item__c eliObj2 = GLSLPW_SetUpData.createELI(protObj1.Id, eObj1.Id);
        }
        
 	static testMethod void testProtocolTrigger() {
	  	
	  	Account accObj = GLSLPW_SetUpData.createAccount('Amgen');
        Contact conObj = GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
        Program__c prgObj = GLSLPW_SetUpData.createProgram(accObj.Id,conObj.Id,'Program_Test', 'Amgen');
        Protocol__c protObj1 = GLSLPW_SetUpData.createProtocol(prgObj.Id,'Draft');
		Test.startTest();
			GLSLPW_ProtocolControllerExt obj = new GLSLPW_ProtocolControllerExt(new ApexPages.StandardController(protObj1));
			protObj1.Status__c = 'Open';
	        update protObj1;
	    Test.stopTest();
     }

    static testMethod void testDelete() {
       	setUpTestData();
    	Protocol__c protObj1 			= [SELECT Name,Id
            							   FROM Protocol__c LIMIT 1];
		Contact conObj		 			= [SELECT Name,Id 
										   FROM Contact LIMIT 1];
		Account accObj		 			= [SELECT Name,Id 
										   FROM Account LIMIT 1];
		Purchase_Agreement__c paObj2	= [SELECT Name,Id
										   FROM Purchase_Agreement__c LIMIT 1];
		Test.startTest();
	        ApexPages.currentPage().getParameters().put('Id', protObj1.Id);
	        GLSLPW_ProtocolControllerExt obj = new GLSLPW_ProtocolControllerExt(new ApexPages.StandardController(protObj1));
	        system.assert(obj.lstPurchaseAgreement.size()>0);
	        obj.paId = paObj2.Id;
	        obj.customEdit();
	        obj.customSave();
	        obj.customCancel();
	        obj.deleteRecord();
        Test.stopTest();
        }
               
        static testMethod void testQuoteTrigger(){
        	setUpTestData();
        	Contact conObj		 			= [SELECT Name,Id 
										   	   FROM Contact LIMIT 1];
			Account accObj		 			= [SELECT Name,Id 
										   	   FROM Account LIMIT 1];
	   	    Protocol__c protObj1 			= [SELECT Name,Id
            							       FROM Protocol__c LIMIT 1];
        	Opportunity oppObj1 = GLSLPW_SetUpData.createOpportunity(conObj.Id, accObj.Id, 'Test Opp1', 'Test Stage Name1', system.today());
	        Opportunity oppObj2 = GLSLPW_SetUpData.createOpportunity(conObj.Id, accObj.Id, 'Test Opp2', 'Test Stage Name2', system.today());
	        Opportunity oppObj3 = GLSLPW_SetUpData.createOpportunity(conObj.Id, accObj.Id, 'Test Opp3', 'Test Stage Name3', system.today());
	        Opportunity oppObj4 = GLSLPW_SetUpData.createOpportunity(conObj.Id, accObj.Id, 'Test Opp4', 'Test Stage Name4', system.today());
	        Opportunity oppObj5 = GLSLPW_SetUpData.createOpportunity(conObj.Id, accObj.Id, 'Test Opp5', 'Test Stage Name5', system.today());
	        Opportunity oppObj6 = GLSLPW_SetUpData.createOpportunity(conObj.Id, accObj.Id, 'Test Opp6', 'Test Stage Name6', system.today());
	              
	        Quote__c quoteObj1 		= GLSLPW_SetUpData.createQuote(accObj.Id,conObj.Id,'');
	        quoteObj1.Status__c 	= 'Cancelled';
	        quoteobj1.Feedback__c 	= 'Overall Costs';
	        oppObj1.Related_Quote__c = quoteObj1.id;
	      	update oppObj1;
	      	
	        Quote__c quoteObj5 		= GLSLPW_SetUpData.createQuote(accObj.Id,conObj.Id,'');
	        quoteObj5.Status__c		 = 'Cancelled';
	    	quoteobj5.Feedback__c 		= 'Turnaround time';
	        oppObj3.Related_Quote__c = quoteObj5.id;
	      	update oppObj3;
	      	
	      	Quote__c quoteObj6 		= GLSLPW_SetUpData.createQuote(accObj.Id,conObj.Id,'');
	        quoteObj6.Status__c		 = 'Cancelled';
	    	quoteobj6.Feedback__c 		= 'Exceeded end-clients budget';
	        oppObj4.Related_Quote__c = quoteObj6.id;
	      	update oppObj4;
	      	
	      	Quote__c quoteObj7 		= GLSLPW_SetUpData.createQuote(accObj.Id,conObj.Id,'');
	        quoteObj7.Status__c		 = 'Cancelled';
	    	quoteobj7.Feedback__c 		= 'Vendor selection by end-client';
	        oppObj5.Related_Quote__c = quoteObj7.id;
	      	update oppObj5;
	      	
	      	Quote__c quoteObj8 		= GLSLPW_SetUpData.createQuote(accObj.Id,conObj.Id,'');
	        quoteObj8.Status__c		 = 'Cancelled';
	    	quoteobj8.Feedback__c 		= '';
	        oppObj6.Related_Quote__c = quoteObj8.id;
	      	update oppObj6;
	        
	        Quote__c quoteObj2 		= GLSLPW_SetUpData.createQuote(accObj.Id,conObj.Id,'');
	        quoteObj2.Status__c = 'Approved';
	        oppObj2.Related_Quote__c = quoteObj2.id;
	      	update oppObj2;
	      	
	      	Quote__c quoteObj3 		= GLSLPW_SetUpData.createQuote(accObj.Id,conObj.Id,'');
	        quoteObj3.Status__c = 'Released';
	        
	        Quote__c quoteObj4 		= GLSLPW_SetUpData.createQuote(accObj.Id,conObj.Id,'');
	        quoteObj4.Status__c = 'Billed';
	        quoteObj4.Protocol__c = protObj1.Id;
	        
	        Test.startTest();
		        List<Quote__c> lstQuote	= new List<Quote__c>();
		        lstQuote.add(quoteObj1);
		        lstQuote.add(quoteObj2);
		        lstQuote.add(quoteObj3);
		        lstQuote.add(quoteObj4);
		        lstQuote.add(quoteObj5);
		        lstQuote.add(quoteObj6);
		        lstQuote.add(quoteObj7);
		        lstQuote.add(quoteObj8);
		        update lstQuote;
	        Test.stopTest();
      }
 }