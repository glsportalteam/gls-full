@isTest
Public class LMS_Test_AutoDeactivatedelTrainings{
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    @isTest
    public static void testAutoDeactivatedelTrainings(){
    LMS_Test_Util util = new LMS_Test_Util();
    system.runAs(LMS_Test_Util.systemAdmin ){
            Training__c delTraining= new Training__c();
            delTraining.Training_Name__c = 'Test delTraining';
            delTraining.Training_Methodologies__c= 'Document Oriented';
            delTraining.URL__c ='www.google.com';
            delTraining.Training_Description__c='test description';
            delTraining.Duration__c = 2;
            delTraining.criterion__c = 30;
            delTraining.start_date__c = Date.today() - 5;
            delTraining.end_date__c = Date.today() - 2;
            delTraining.skills__c='java';
            delTraining.Trainer_Name__c= 'Mr Bruce';
            delTraining.Active__c = true;
            delTraining.Training_Required_for__c = 'Project Manager';
            
            Database.insert (delTraining); 
           
   }
    
    Test.startTest();
       
      // Schedule the test job
      String jobId = System.schedule('LMS_AutoDeactivateTrainings',
                        CRON_EXP, 
                        new LMS_AutoDeactivateTrainings());
         
      // Get the information from the CronTrigger API object
      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];

      // Verify the expressions are the same
      System.assertEquals(CRON_EXP, 
         ct.CronExpression);

      // Verify the job has not run
      System.assertEquals(0, ct.TimesTriggered);

      // Verify the next time the job will run
      System.assertEquals('2022-03-15 00:00:00', 
         String.valueOf(ct.NextFireTime));
      // Verify the scheduled job hasn't run yet.
     // Merchandise__c[] ml = [SELECT Id FROM Merchandise__c 
     //                        WHERE Name = 'Scheduled Job Item'];
      //System.assertEquals(ml.size(),0);
            
            test.stopTest();
    
    }


}