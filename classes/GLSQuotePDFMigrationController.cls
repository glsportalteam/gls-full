/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSQuotePDFMigrationController  Class
* Function: This class is used to Upload already generated Quote PDF files to Amazon S3 server and create corresponding AWS file record 
            for the uploaded file. This is for the one-time migration activity.
*/

global with sharing class GLSQuotePDFMigrationController {
    
    //Variable declaration for the class
    public String key { get {return credentials.key;} }    
    public String bucket{get;set;}
    public String strJSON {get; set;}
    public set<Id> setQuoteID {get; set;}
    public Map<Id, QuotePDFValues> mapQuotePDF {get; set;}
    public Date d {get; set;}
    public Integer day {get; set;}
    public Integer month {get; set;}
    public Integer year {get; set;}
    private AWSKeys credentials {get;set;}
    private String AWSCredentialName = 'NAME OF KEY TO USE';
    private String secret { get {return credentials.secret;} }
    private S3.AmazonS3 as3 { get; private set; }
    private List<Attachment> lstQuotePDF {get; set;}
    private Map<Id, Blob> mapBlob {get; set;}
    global static Boolean executeAWSTrigger = false;
    
    public GLSQuotePDFMigrationController(){
        
        getawss3LoginCredentials();
        d = System.today();
        day = d.day();
        month = d.month();
        year = d.year();
        bucket = GLSConfig.bucketName;
        mapQuotePDF = new Map<Id, QuotePDFValues>();
        mapBlob = new Map<Id, Blob>();
        setQuoteID = new Set<Id>();
        
        lstQuotePDF = [SELECT Id, ParentId, Name, Body, ContentType, BodyLength FROM Attachment WHERE Name LIKE 'GLS_Quote%' LIMIT 10];
        if(lstQuotePDF.size() > 0 && lstQuotePDF != null){
            for(Attachment att : lstQuotePDF){
                setQuoteID.add(att.ParentId);
                mapBlob.put(att.ParentId, att.Body);
            }  
        } 
             
        List<Quote__c> lstQuoteWithAtt = [SELECT Quote_Number__c, Client__r.Name, Id FROM Quote__c WHERE Id IN :setQuoteID];
        if(lstQuoteWithAtt.size() > 0 && lstQuoteWithAtt != null){
            for(Quote__c quo : lstQuoteWithAtt){
                String fileValue = EncodingUtil.base64Encode(mapBlob.get(quo.Id));
                mapQuotePDF.put(quo.Id, new QuotePDFValues(quo.Id, quo.Quote_Number__c, quo.Client__r.Name, fileValue));
            }
        }
        if(!mapQuotePDF.isEmpty()){
            strJSON = JSON.Serialize(mapQuotePDF);
        }
        else{
            strJSON = null;
        }
    }
    
    /* 
    @Description: To get the login credentials for S3 Amazon Web Service
    @Params: None
    @Return Type: PageReference
    */
    public PageReference getawss3LoginCredentials(){
        try{
            credentials = new AWSKeys(AWSCredentialName);
            as3 = new S3.AmazonS3(credentials.key,credentials.secret);
         }catch(AWSKeys.AWSKeysException AWSEx){
            System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
            ApexPages.addMessage(errorMsg);
        } 
       return null;
    }
    
    Datetime expire = system.now().addDays(1);
    String formattedexpire = expire.formatGmt('yyyy-MM-dd')+'T'+
    expire.formatGmt('HH:mm:ss')+'.'+expire.formatGMT('SSS')+'Z';           
    String policy { get {return 
                '{ "expiration": "'+formattedexpire+'","conditions": [ {"bucket": "'+
        bucket +'" } ,{ "acl": "'+
       'private'+'" },'+
       '{"success_action_status": "201" },'+
       '["starts-with", "$key", ""] ]}';       } }  
 
    /* 
    @Description: To get the base64 encoded policy for policy
    @Params: None
    @Return Type: String
    */
    public String getPolicy() {
        return EncodingUtil.base64Encode(Blob.valueOf(policy));
    }
    
    /* 
    @Description: To get the crypto encrypted - base64 encoded policy for signature
    @Params: None
    @Return Type: String
    */
    public String getSignedPolicy() {    
        return make_sig(EncodingUtil.base64Encode(Blob.valueOf(policy)));        
    }
    
    /* 
    @Description: To get the crypto encrypted - base64 encoded policy for signature
    @Params: Base64 encoded policy (String)
    @Return Type: String
    */
    private String make_sig(String canonicalBuffer) {        
        String macUrl ;
        String signingKey = EncodingUtil.base64Encode(Blob.valueOf(secret));
        Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof(Secret)); 
        macUrl = EncodingUtil.base64Encode(mac);  
        return macUrl;
    } 
    
    public void successFileupload(){
    }
    
    /* 
    @Description: Remote Action to create AWS File record and Quote PDF record.
    @Params: QuoteId, File Name, Header, Full Path (Id, String, String, String)
    @Return Type: String
    */ 
    @RemoteAction
    global static String successFileUpload(Id qId, String fName, String header, String fullpath){
        if(qid != null){
            executeAWSTrigger = true;
            AWS_File__c awsRecord = new AWS_File__c(Bucket_Name__c = GLSConfig.bucketName,
                                                    Amazon_S3_source_file_path__c = fullPath,
                                                    File_Name__c = fName,
                                                    version_id__c = header,
                                                    Record_Type__c = GLSConfig.QuotePDF); 
            try{
                insert awsRecord; //Trigger called to insert record for AWS File sharing and AWS File Version
            } 
            catch(DmlException de){
                System.debug(GLSConfig.DMLExceptionOccured);
            }
            catch(Exception e){
                System.debug(GLSConfig.ExceptionOccured);
            }
            
            //Insert Quote PDF record
            Quote_PDF_File__c quotePDF = new Quote_PDF_File__c(AWS_File_Id__c = awsRecord.Id,
                                                               Method__c = GLSConfig.Automatic,
                                                               Quote_ID__c = qId,
                                                               File_Name__c = fName);
            try{
                insert quotePDF;
            } 
            catch(DmlException de){
                System.debug(GLSConfig.DMLExceptionOccured);
            }
            catch(Exception e){
                System.debug(GLSConfig.ExceptionOccured);
            }
            
            //Update name of attachment
            List<Attachment> att = [SELECT Id, Name, ParentId FROM Attachment WHERE ParentId =: qid LIMIT 1];
            List<Attachment> attUpdate = new List<Attachment>();
            if(att.size() > 0 && att != null){
                att[0].Name = fName;
                attUpdate.add(att[0]);
            }
            update attUpdate;
            
            Quote__c quoteObj = new Quote__c(Id = [SELECT Quote_ID__c FROM Quote_PDF_File__c WHERE Id =: quotePDF.Id LIMIT 1].Quote_ID__c);
            quoteObj.File_Id__c = awsRecord.Id;
            try{
                update quoteObj;
            }
            catch(DmlException de){
                System.debug(GLSConfig.DMLExceptionOccured);
            }
            catch(Exception e){
                System.debug(GLSConfig.ExceptionOccured);
            }
        }
        return qid;
    }
    
    //Inner Class
    public class QuotePDFValues{
        public String quoteID {get; set;}
        public String quoteNum {get; set;}
        public String clientName {get; set;}
        public String attachValue {get; set;}
        
        public QuotePDFValues(String quoteID, String quoteNum, String clientName, String attachValue){
            this.quoteID = quoteID;
            this.quoteNum = quoteNum;
            this.clientName = clientName;
            this.attachValue = attachValue;
        }
    }
}