global class GLSAmazonUtility{
    public static Boolean skipTrigger = false;
    public static AWSKeys credentials;
    public static S3.AmazonS3 as3 { get; private set; }
    public static  String AWSCredentialName = 'NAME OF KEY TO USE'; //Modify this string variable to be the name of the AWS Credential record that contains the proper AWS keys and secret
    public static string secret { get {return credentials.secret;} }
    public static string key { get {return credentials.key;} }
    public static datetime loginTime;
    
       /**
        *   @Description : This method sets Aws s3 credentials
        *   @Param : none
        **/ 
        
    public static datetime getLoginHistory(String userId){
        List<loginhistory> lstLHistory = new List<loginhistory> ();
        try{
            lstLHistory = [SELECT LoginTime from loginhistory where UserId =: userId order by logintime desc limit 2];
            system.debug('---lstLHistory---: '+lstLHistory);
            if(lstLHistory != null && lstLHistory.size()>0){
                for(loginhistory lHist : lstLHistory){
                    loginTime = lHist.LoginTime;
                }
            }
        }catch(QueryException qe){
            system.debug('---Exception---: '+qe);
            return null;
        }
        system.debug('---loginTime in Amazon utility---: '+loginTime);
        return loginTime;
    }
    
    @future
    public static void processUserRecords(List<ID> recordIds,Boolean isActivated){   
         // Get those records based on the IDs
         List<Contact> contacts = [SELECT Name FROM Contact WHERE Id IN :recordIds];
         if(contacts != null && contacts.size() > 0){
         	if(isActivated == true){
         		for(Contact c : contacts){
         			c.isPortalActivated__c = true;
         		}
         	}else{
         		for(Contact c : contacts){
         			c.isPortalActivated__c = false;
         		}
         	}
         	update contacts;
         }
         // Process records
    }
    
    public static void getawss3LoginCredentials(){
        try{

            credentials = new AWSKeys(AWSCredentialName);
            as3 = new S3.AmazonS3(credentials.key,credentials.secret);
            system.debug('-----as3----'+as3)    ;
        }catch(AWSKeys.AWSKeysException AWSEx){
            System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
        } 
    } 
  
   //method that will sign
    private static String make_sig(string canonicalBuffer) {                
        String macUrl ;
        String signingKey = EncodingUtil.base64Encode(Blob.valueOf(as3.secret));
        system.debug(as3.secret+'as3.secret--------------------------------------');
        Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof(as3.secret)); 
        macUrl = EncodingUtil.base64Encode(mac);
        system.debug(macUrl+'macUrl------------------');                    
        return macUrl;
    }
    /**
    * @Description :Method for downloading file from s3 in authenticate way 
    * @Param : none
    *
    **/
   
    WebService static string downloadQuoteFile(string fName,string bucketName){
        String url = '';
        try{
            getawss3LoginCredentials();
            String filename = EncodingUtil.urlEncode(fName,'UTF-8');
            Datetime now = DateTime.now();
            Datetime expireson = now.AddSeconds(360);
            Long Lexpires = expireson.getTime()/1000;
            String stringtosign = 'GET\n\n\n'+Lexpires+'\n/'+Glsconfig.bucketName+'/'+filename;                        
            String signed = make_sig(stringtosign);
            String codedsigned = EncodingUtil.urlEncode(signed,'UTF-8');            
            url = 'https://'+Glsconfig.bucketName+'.s3.amazonaws.com/'+filename+'?AWSAccessKeyId='+as3.key+'&Expires='+Lexpires+'&Signature='+codedsigned;
            system.debug('---url---: '+url)    ;        
        }catch(Exception e){
            url = '';
            system.debug('Exception in downloadQuoteFile '+e);
        }
    return url;
    }

 WebService static string deleteQuoteFile(string quoteFileId,string path,string bucket){
        string flag;
        try{
           getawss3LoginCredentials();
           Datetime now = Datetime.now();        
           //This performs the Web Service call to Amazon S3 and delete object
           system.debug('--bucket--: '+bucket);
           system.debug('--path--: '+path);
           system.debug('--bucket--: '+bucket);
           if(!Test.isRunningTest()){
              S3.Status deleteObjectResult= as3.DeleteObject(bucket.trim(),path.trim(),as3.key,now,as3.signature('DeleteObject',now), as3.secret);
           }
           User u=[SELECT Id,UserType FROM User WHERE Id = :userInfo.getUserId()limit 1];
           if(u.UserType =='PowerPartner'){
           flag= 'false';
           }else if(u.UserType =='Standard'){
            flag ='true';
           }
           delete [select id from Quotes_File__c where id=:quoteFileId.trim()];
           return flag;
        } catch(System.CalloutException callout){
          System.debug('CALLOUT EXCEPTION: ' + callout);
          return null;  
       }
       catch(Exception ex){
           System.debug(ex);
           return null; 
       }       
    }
    /*
    * @discription : this method delete the file from Amazon server as well as delete the its corresponding records in 
    *                quote_file__c object and AWS_File__C object 
    * @param  : First argument is the id of Quote_File__c object record which needs to be deleted
    *           Second param is the complete path of the file on s3 server
    *           Third param is the name of bucket 
    */ 
    WebService static string deleteQuoteFileFromAmazon(string quoteFileId,string path,string bucket){
        string flag;
        try{
           getawss3LoginCredentials();
           Datetime now = Datetime.now();        
           //This performs the Web Service call to Amazon S3 and delete object
           bucket = GLSConfig.bucketName;           
           if(!Test.isRunningTest()){
              S3.Status deleteObjectResult= as3.DeleteObject(bucket.trim(),path.trim(),as3.key,now,as3.signature('DeleteObject',now), as3.secret);
           }
           User u=[SELECT Id,UserType FROM User WHERE Id = :userInfo.getUserId()limit 1];
           if(u.UserType =='PowerPartner'){
            flag= 'false';
           }else if(u.UserType =='Standard'){
            flag ='true';
           }           
           list<Quotes_File__c> quoteFileList = [select id,AWS_File__c from Quotes_File__c where id=:quoteFileId.trim()];
           if(quoteFileList != null && quoteFileList.size() > 0){                           
               Id awsFileObjId = quoteFileList[0].AWS_File__c;
               if(awsFileObjId != null){
                   AWS_File__c awsObj = new AWS_File__c(id = awsFileObjId);                   
                   delete awsObj;
               }
               delete quoteFileList;                        
           }
           //delete [select id,AWS_File__c from Quotes_File__c where id=:quoteFileId.trim()];
           return flag;
        } catch(System.CalloutException callout){
          System.debug('CALLOUT EXCEPTION: ' + callout);
          return null;  
       }catch(DMLException e){
            system.debug(e.getMessage()+'DMLException----------------------');
            return null;
       }catch(Exception ex){
           System.debug(ex);
           return null; 
       }        
    }
    /*
    * @discription : this method delete the file from Amazon server as well as delete the its corresponding records in 
    *                quote_PDF_file__c object and AWS_File__C object 
    * @param  : First argument is the id of Quote_PDF__c object record which needs to be deleted
    *           Second param is the complete path of the file on s3 server
    *           Third param is the name of bucket 
    */ 
    WebService static string deleteQuotePDFFileFromAmazon(string quotePDFFileId,string path){
        string flag;
        String bucket = GLSConfig.bucketName;
        try{
           getawss3LoginCredentials();
           Datetime now = Datetime.now();        
           //This performs the Web Service call to Amazon S3 and delete object                      
           if(!Test.isRunningTest()){
              S3.Status deleteObjectResult= as3.DeleteObject(bucket.trim(),path.trim(),as3.key,now,as3.signature('DeleteObject',now), as3.secret);
           }
           User u=[SELECT Id,UserType FROM User WHERE Id = :userInfo.getUserId()limit 1];
           if(u.UserType =='PowerPartner'){
            flag= 'false';
           }else if(u.UserType =='Standard'){
            flag ='true';
           }           
           list<Quote_PDF_File__c> quotePDFFileList = [select id,AWS_File_Id__c from Quote_PDF_File__c where id=:quotePDFFileId.trim()];
           if(quotePDFFileList != null && quotePDFFileList.size() > 0){                          
               Id awsFileObjId = quotePDFFileList[0].AWS_File_Id__c;
               if(awsFileObjId != null){
                   AWS_File__c awsObj = new AWS_File__c(id = awsFileObjId);
                   system.debug(awsObj+'AWS_File__c-----------------');
                   delete awsObj;
               }
               delete quotePDFFileList;                         
           }
           //delete [select id,AWS_File__c from Quotes_File__c where id=:quoteFileId.trim()];
           return flag;
        } catch(System.CalloutException callout){
          System.debug('CALLOUT EXCEPTION: ' + callout);
          return null;  
       }catch(DMLException e){
            system.debug(e.getMessage()+'DMLException----------------------');
            return null;
       }catch(Exception ex){
           System.debug(ex);
           return null; 
       }        
    }
    WebService static string deleteFilesFromAmazon(string FileId,string path){
        string flag;
        String bucket = GLSConfig.bucketName;
        try{
           getawss3LoginCredentials();
           Datetime now = Datetime.now();        
           //This performs the Web Service call to Amazon S3 and delete object                      
           if(!Test.isRunningTest()){
              S3.Status deleteObjectResult= as3.DeleteObject(bucket.trim(),path.trim(),as3.key,now,as3.signature('DeleteObject',now), as3.secret);
           }
           User u=[SELECT Id,UserType FROM User WHERE Id = :userInfo.getUserId()limit 1];
           if(u.UserType =='PowerPartner'){
            flag= 'false';
           }else if(u.UserType =='Standard'){
            flag ='true';
           }           
           id fId=FileId;
           String parentObjectName = fId.getSobjectType().getDescribe().getName();
           list<sObject> quotePDFFileList;
           list<Estimate_Files__c> lstEstFiles;
           list<Purchase_Agreement_File__c> lstPAFiles;
           list<Program_Files__c> lstProgramFiles;
           if(parentObjectName != null && parentObjectName == 'Estimate_Files__c'){
           	  lstEstFiles=[select id,AWS_File_Id__c from Estimate_Files__c where id=:FileId.trim()];           	  
           }
           if(parentObjectName != null && parentObjectName == 'Purchase_Agreement_File__c'){
           	  lstPAFiles=[select id,AWS_File_Id__c from Purchase_Agreement_File__c where id=:FileId.trim()];           	  
           }
           if(parentObjectName != null && parentObjectName == 'Program_Files__c'){
           	  lstProgramFiles=[select id,AWS_File_Id__c from Program_Files__c where id=:FileId.trim()];           	  
           }                                
           if(parentObjectName!=null && parentObjectName=='Estimate_Files__c'){
	           if(lstEstFiles != null && lstEstFiles.size() > 0){	           	                             
	               Id awsFileObjId = lstEstFiles[0].AWS_File_Id__c;
	               if(awsFileObjId != null){
	                   AWS_File__c awsObj = new AWS_File__c(id = awsFileObjId);
	                   system.debug(awsObj+'AWS_File__c-----------------');
	                   delete awsObj;
	               }
	               delete lstEstFiles;                         
	           }
           }
           if(parentObjectName!=null && parentObjectName=='Purchase_Agreement_File__c'){
	           if(lstPAFiles != null && lstPAFiles.size() > 0){	           	                             
	               Id awsFileObjId = lstPAFiles[0].AWS_File_Id__c;
	               if(awsFileObjId != null){
	                   AWS_File__c awsObj = new AWS_File__c(id = awsFileObjId);
	                   system.debug(awsObj+'AWS_File__c-----------------');
	                   delete awsObj;
	               }
	               delete lstPAFiles;                         
	           }
           }
           if(parentObjectName!=null && parentObjectName=='Program_Files__c'){
	           if(lstProgramFiles != null && lstProgramFiles.size() > 0){	           	                             
	               Id awsFileObjId = lstProgramFiles[0].AWS_File_Id__c;
	               if(awsFileObjId != null){
	                   AWS_File__c awsObj = new AWS_File__c(id = awsFileObjId);
	                   system.debug(awsObj+'AWS_File__c-----------------');
	                   delete awsObj;
	               }
	               delete lstProgramFiles;                         
	           }
           }                              
           return flag;
        } catch(System.CalloutException callout){
          System.debug('CALLOUT EXCEPTION: ' + callout);
          return null;  
       }catch(DMLException e){
            system.debug(e.getMessage()+'DMLException----------------------');
            return null;
       }catch(Exception ex){
           System.debug(ex);
           return null; 
       }        
    }        
    WebService static string setObsoleteFileType(string awsFileId){
    return null;
    }
    
    WebService static string setObsoleteQuoteFileType(string quoteFileId){
        string flag;
        try{                              
           list<Quotes_File__c> awsFileList= [select id,File_Type__c,File_Status__c from Quotes_File__c where id=:quoteFileId.trim() limit 1];
           if(awsFileList!=null && awsFileList.size()>0){
                Quotes_File__c fileObj=awsFileList[0];
                fileObj.File_Status__c=GLSconfig.filetypeObsolete;
                update fileObj;
                return 'true'; 
           }else{
                return null;
           }           
        } catch(System.DmlException e){
          System.debug('DML EXCEPTION: ' + e);
          return null;  
       }
       catch(Exception ex){
           System.debug(ex);
           return null; 
       }        
    }  
    WebService static list<Quote__c> cloneQuote(string quoteId){
        system.debug('---inside GLSAmazonUtility method--' +quoteId);
        List<Quote__c> newQuote = new List<Quote__c>();
        List<Quotes_File__c> newQuoteFile=new List<Quotes_File__c>();
       try{
             newQuote = createQuoteClone(quoteId);
             system.debug('line 94 '+newQuote );
          /*   if(newQuote!= null && newQuote.size()>0){
                Datetime sysTime = System.now().addSeconds(5);      
                String chronExpression = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
              //  System.schedule( 'Clone quote with File- ' + sysTime, chronExpression, new GLSCloneQuoteSchedulerClass(quoteId,newQuote[0].id));
                return newQuote;
             }else return null;*/
             return newQuote;
        }catch(Exception ex){
           System.debug(GLSConfig.ExceptionOccured+ex);
           return null; 
       } 
    }
    
    WebService static list<Quote__c> cloneQuoteWithoutFiles(string quoteId){
        List<Quote__c> newQuote = new List<Quote__c>();
        List<Quotes_File__c> newQuoteFile=new List<Quotes_File__c>();
        try{
            newQuote = createQuoteClone(quoteId);
        } catch(Exception ex){
           System.debug(GLSConfig.ExceptionOccured+ex);
        }
        if(newQuote!= null && newQuote.size()>0){
            createQuoteAssignment(quoteId,newQuote[0].Id);
            return newQuote;
        }else return null;
    }
    
    private static List<Quote__c> createQuoteClone(String Qid){
       boolean flag = false;
        List<Quote__c> lstnewQuote = new List<Quote__c>();
        list<Quote__c> lstQuotes=[select isDirectOrder__c,BDM__c,Project_Name_Community__c,overallSupportFeedback__c,responsivenessFeedback__c,qualityFeedback__c,
                                          Submission_date__c,id,Project_Number__c,Client__c,Status__c, Original_Quote_Id__c,Original_Quote_Id__r.Original_Quote_Id__c, 
                                          Change_Order__c, Revised_Order__c, Quote_Number__c, Bucket_Name__c,Contact__c,Quote_Name__c,Quote_Due_Date__c,Urgency__c,
                                          Rate_Sheet__c,Notes__c,Project_Due_Date__c, Quote_Manager__c,Expedited__c,Purchase_Order__c,Delivery_Schedule__c,
                                          Protocol__c, Program__c, Sponsor__c, Request_Type__c, Previous_Project__c,
                                          (select Amazon_S3_source_file_path__c,File_Name__c,File_Format__c,version_id__c from Quotes_Files__r) 
                                     from Quote__c where Id =:Qid.trim()];
            
            if( lstQuotes !=null && lstQuotes.size()>0){
                for(Quote__c quote:lstQuotes){
                    Quote__c q = new Quote__c();
                  
                    if(quote.Status__c == 'Approved' || quote.Status__c == 'Direct Order'|| quote.Status__c == 'In Production' ){
                        q.Change_Order__c = true;
                    }else If(quote.Status__c == 'Released'){
                        q.Revised_Order__c = true;
                    }
                    /*if(quote.Status__c == 'Approved' || quote.Status__c == 'Direct Order'){
                        q.Status__c =quote.Status__c;
                    }
                    else{
                        q.Status__c = 'New';                    
                    }*/
                   
                    if(quote.Original_Quote_Id__c != null){
                        q.Original_Quote_Id__c = quote.Original_Quote_Id__c;
                    }else{
                        q.Original_Quote_Id__c = quote.id;
                    }
                    if((quote.Status__c== 'Released') && (quote.Change_Order__c)){
                        q.Original_Quote_Id__c = quote.id;
                    }   
                    if((quote.Status__c== 'Released') && (quote.Revised_Order__c)){
                        q.Original_Quote_Id__c = quote.Original_Quote_Id__c;
                    }   
                    if(((quote.Status__c== 'Approved') || (quote.Status__c == 'Direct Order')|| (quote.Status__c == 'In Production')) && (quote.Revised_Order__c)){
                        if(quote.Original_Quote_Id__r.Original_Quote_Id__c != null)
                            q.Original_Quote_Id__c = quote.Original_Quote_Id__r.Original_Quote_Id__c;
                    }
                    if(((quote.Status__c== 'Approved') || (quote.Status__c == 'Direct Order')|| (quote.Status__c == 'In Production')) && (quote.Change_Order__c)){
                            q.Original_Quote_Id__c = quote.Original_Quote_Id__c;
                    }
                    q.BDM__c = quote.BDM__c;
                    q.isDirectOrder__c=quote.isDirectOrder__c;
                    q.Project_Name_Community__c=quote.Project_Name_Community__c;
                    q.Submission_date__c=quote.Submission_date__c;
                    q.Project_Number__c=quote.Project_Number__c;  
                    q.Clone_From__c=Qid.trim();
                    q.Bucket_Name__c = quote.Bucket_Name__c;
                    q.Client__c = quote.Client__c;
                    q.Contact__c = quote.Contact__c;
                    q.Quote_Name__c = quote.Quote_Name__c;
                    q.Quote_Due_Date__c = quote.Quote_Due_Date__c;
                    q.Project_Due_Date__c = quote.Project_Due_Date__c;
                    q.Urgency__c = quote.Urgency__c;
                    q.Rate_Sheet__c = quote.Rate_Sheet__c;
                    q.Notes__c = quote.Notes__c;
                    q.Quote_Manager__c = quote.Quote_Manager__c;
                    q.Purchase_Order__c = quote.Purchase_Order__c;
                    q.Delivery_Schedule__c = quote.Delivery_Schedule__c;
                    q.Protocol__c = quote.Protocol__c;
                    q.Program__c = quote.Program__c;
                    q.Sponsor__c = quote.Sponsor__c ;
                    q.Request_Type__c = quote.Request_Type__c;
                    q.Previous_Project__c = quote.Previous_Project__c;
                
                    lstnewQuote.add(q);
                }     
                if(lstnewQuote != null && lstnewQuote.size()>0){
                    try{
                        Database.insert(lstnewQuote);
                        flag = true;
                    }catch(DMLException dmlE){
                        flag = false;
                        system.debug(GLSConfig.DMLExceptionOccured);
                    }catch(Exception e){
                        flag = false;
                        system.debug(GLSConfig.ExceptionOccured);
                    }
                }
            }
            if(flag) return lstnewQuote;
            else return null;
    }
    
    private static void createQuoteAssignment(String Qid, String currentQId){
        boolean result = false;
        List<Quote_Assignment__c> lstQA = new List<Quote_Assignment__c>();
        List<Quote_Assignment__c> lstNewQuoteAssg = new list<Quote_Assignment__c>();
        list<Quote_Assignment__c> lstParentQAsst = new list<Quote_Assignment__c>();
        List<Quote_Assignment_Service__c> lstQASParent = new List<Quote_Assignment_Service__c>();
        List<Quote_Assignment_Language__c> lstQALParent = new List<Quote_Assignment_Language__c>();
        List<Quote_Assignment_Special_Provisions__c> lstQASPParent = new List<Quote_Assignment_Special_Provisions__c>();
        if(Qid != null && Qid !=''){
            lstParentQAsst = [Select Id,Name,AssignmentNumber1__c, Quote__c, Countries__c, Delivery_Format__c ,Quote_Assignment_Id__c, 
                               isAssignmentUpdated__c, Source_Language__c, Allowed_File_Formats__c, Translation_Update__c
                        From Quote_Assignment__c 
                        Where Quote__c =: Qid];
        }
        
        if(lstParentQAsst != null && lstParentQAsst.size()>0){
            for (Quote_Assignment__c qa:lstParentQAsst){
                    Quote_Assignment__c quoteAss = new Quote_Assignment__c();
                    quoteAss.Quote__c = currentQId;
                    quoteAss.Translation_Update__c = qa.Translation_Update__c;
                    quoteAss.Countries__c = qa.Countries__c;
                    quoteAss.Source_Language__c = qa.Source_Language__c;
                    quoteAss.Quote_Assignment_Id__c = qa.Id;  
                  //  quoteAss.Allowed_File_Formats__c = qa.Allowed_File_Formats__c;
                  //  quoteAss.Delivery_Format__c = qa.Delivery_Format__c ;
                    quoteAss.isAssignmentUpdated__c = qa.isAssignmentUpdated__c ;
                    system.debug('----qa.Translation_Update__c----: '+qa.Translation_Update__c);
                    
                    lstQA.add(quoteAss);
            }
        }
        if(lstQA != null && lstQA.size()>0){
            try{
                insert lstQA;
                result = true;
            }catch(DMLException dmlE){
                result = false;
                system.debug(GLSConfig.DMLExceptionOccured);
            }catch(Exception e){
                result = false;
                system.debug(GLSConfig.ExceptionOccured);
            }
        }
     
      if(result){
        try{
            lstNewQuoteAssg =   [Select Id,Quote_Assignment_Id__c,Quote__c From Quote_Assignment__c Where Quote__c =: currentQId];
            
            lstQASParent    =   [Select Service_Name__c, Quote_Assignment__r.Quote__c, Quote_Assignment__c, Name, Id, Client_Service__c 
                                 From Quote_Assignment_Service__c 
                                 Where Quote_Assignment__r.Quote__c =: Qid]; 
                
            lstQALParent    =    [Select Quote_Assignment__r.Quote__c, Quote_Assignment__c, Name, Language_Name__c, Language_List_Item__c, Id 
                                 From Quote_Assignment_Language__c 
                                 Where Quote_Assignment__r.Quote__c =: Qid];
                
            lstQASPParent   =    [Select Standard_Special_Provisions__c, Quote_Assignment_Special_Provision_Text__c, Quote_Assignment_Special_Provision_Name__c, 
                                        Quote_Assignment_Id__c, Name, Id,Quote_Assignment_Id__r.Quote__c 
                                 From Quote_Assignment_Special_Provisions__c 
                                 Where Quote_Assignment_Id__r.Quote__c =: Qid];   
                                  
            
            Quote_Assignment_Service__c qaServiceNew ; 
            Quote_Assignment_Special_Provisions__c qaspNew ; 
            Quote_Assignment_Language__c qaTLangNew ; 
            List<Quote_Assignment_Service__c> lstQASNew = new List<Quote_Assignment_Service__c>();
            List<Quote_Assignment_Language__c> lstQATLangNew = new List<Quote_Assignment_Language__c>();
            List<Quote_Assignment_Special_Provisions__c> lstQASPNew = new List<Quote_Assignment_Special_Provisions__c>();
            
            system.debug('---lstNewQuoteAssg-- '+lstNewQuoteAssg);
                if(lstNewQuoteAssg != null && lstNewQuoteAssg.size()>0){
                    for(Quote_Assignment__c qaNew : lstNewQuoteAssg){
                        for(Quote_Assignment_Service__c qas : lstQASParent){
                            if(qas.Quote_Assignment__c == qaNew.Quote_Assignment_Id__c){
                                qaServiceNew = new Quote_Assignment_Service__c();
                                qaServiceNew.Client_Service__c = qas.Client_Service__c;
                                qaServiceNew.Quote_Assignment__c = qaNew.Id;
                                qaServiceNew.Service_Name__c = qas.Service_Name__c;
                                lstQASNew.add(qaServiceNew);
                            }
                        }
                        for(Quote_Assignment_Language__c qal : lstQALParent){
                            if(qal.Quote_Assignment__c == qaNew.Quote_Assignment_Id__c){
                                qaTLangNew = new Quote_Assignment_Language__c();
                                qaTLangNew.Language_List_Item__c = qal.Language_List_Item__c;
                                qaTLangNew.Quote_Assignment__c = qaNew.Id;
                                qaTLangNew.Language_Name__c = qal.Language_Name__c;
                                lstQATLangNew.add(qaTLangNew);
                            }
                        }
                        system.debug('---lstQASPParent-- '+lstQASPParent);
                        for(Quote_Assignment_Special_Provisions__c qasp : lstQASPParent){
                            if(qasp.Quote_Assignment_Id__c == qaNew.Quote_Assignment_Id__c){
                                qaspNew = new Quote_Assignment_Special_Provisions__c();
                                qaspNew.Quote_Assignment_Id__c = qaNew.Id;
                                qaspNew.Standard_Special_Provisions__c = qasp.Standard_Special_Provisions__c;
                                qaspNew.Quote_Assignment_Special_Provision_Name__c =qasp.Quote_Assignment_Special_Provision_Text__c;
                                qaspNew.Quote_Assignment_Special_Provision_Text__c = qasp.Quote_Assignment_Special_Provision_Text__c;
                                lstQASPNew.add(qaspNew);
                            }
                        }
                    }
                   
                }
                system.debug('---lstQASPNew-- '+lstQASPNew);
                if(lstQASNew != null && lstQASNew.size()>0){
                    skipTrigger = true;
                    Database.insert(lstQASNew); 
                    skipTrigger = false;
                }
                if(lstQATLangNew != null && lstQATLangNew.size()>0){
                    skipTrigger = true;
                    Database.insert(lstQATLangNew); 
                    skipTrigger = true;
                }
                if(lstQASPNew != null && lstQASPNew.size()>0){
                    Database.insert(lstQASPNew); 
                }
        }catch(DMLException de){
            system.debug(GLSConfig.DMLExceptionOccured+'--'+de);
        }catch(Exception e){
            system.debug(GLSConfig.ExceptionOccured+'--'+e);
        }
      
      }  
    }
}