@isTest
public with sharing class GLSCommunity_FAMCtrlTest {
	
	static testMethod void testConstructor(){
		profile p =[select id from profile where Name =: GLSConfig.Communityprofile limit 1];   
	    Account acc = new Account();
	    acc.name = 'Test Account';
	    acc.BillingStreet = 'Sample';
	    acc.BillingCity = 'Sample';
	    acc.BillingState = 'Sample';
	    acc.BillingStreet = 'Sample';
	    acc.BillingPostalCode='Sample';
	    acc.BillingCountry='Sample';
	    insert acc;
	    
	    List<Contact> conLst =  new List<Contact>();
	    Contact cont = new Contact();
	    cont.AccountId = acc.Id;
	    cont.FirstName= 'Test';
	    cont.LastName = 'Contact';
	    cont.Email = 'test@GLS.com';	    
	    conLst.add(cont);
	    
	    Contact cont2 = new Contact();
	    cont2.AccountId = acc.Id;
	    cont2.FirstName= 'Test2';
	    cont2.LastName = 'Contact2';
	    cont2.Email = 'test2@GLS.com';
	    conLst.add(cont2);
	    
	    Contact cont3 = new Contact();
	    cont3.AccountId = acc.Id;
	    cont3.FirstName= 'Test3';
	    cont3.LastName = 'Contact3';
	    cont3.Email = 'test@GLS.com';
	    conLst.add(cont3);
	    
	    insert conLst; 
	    
	    User urs = new User();
	    urs.FirstName='test';
	    urs.LastName='test';
	    urs.Email='test@test.com';
	    urs.Alias='abcd';
	    urs.ContactId=cont.id;
	    urs.Username='test@salesforce1234.com';
	    urs.CommunityNickname='abcd';	   
	    urs.EmailEncodingKey='UTF-8';
	    urs.timezonesidkey='America/Los_Angeles';
	    urs.LocaleSidKey='en_US';	     
	    urs.languagelocalekey='en_US';
	     //urs.IsPartner=true;
	    urs.ProfileId=p.id;
	    insert urs;
	    
	    Quote__c quote = new Quote__c();
	    quote = new Quote__c();
        quote.Quote_Name__c = 'Test Quote';        
        quote.Client__c = urs.AccountId;
        quote.Status__c='New';        
        quote.Contact__c = urs.contactId;
        insert quote;
        
        LPFileSharingAccessManagement__c accRec = new LPFileSharingAccessManagement__c();
        accRec.Email__c = 'test@GLS.com';
        accRec.First_Name__c='Test';
        accRec.Last_Name__c = 'contact';
        accRec.IncorrectLoginAttempt__c = 0;
        accRec.isAccessActive__c = true;
        accRec.Password__c = 'test';
        accRec.Quote_Id__c = quote.id;
        insert accRec;
                
	    Test.startTest();
	    
	    GLSCommunity_FAMCtrl obj;    		
		Pagereference pg;    			    	
		pg = Page.GLSCommunity_FAMPage;
		Test.setCurrentPageReference(pg);
		System.currentPageReference().getParameters().put('accId',acc.id);
		System.currentPageReference().getParameters().put('quoteId',quote.id);				    		    				        	            	        		         	        		    		       
		obj = new GLSCommunity_FAMCtrl();	    
	    obj.setSelectedUsers(new List<String>{cont2.id});
	    obj.addSelectedUsers();
	    obj.setSelectedUsers(new List<String>{cont3.id});
	    obj.addSelectedUsers();	    	    
	    obj.addNewSharingRecord();
	    
	    obj.newSharingRecord =  new LPFileSharingAccessManagement__c();
	    obj.newSharingRecord.First_Name__c = 'test';
	    obj.newSharingRecord.Email__c = 'test@test.com';
	    obj.addNewSharingRecord();
	    
	    obj.newSharingRecord =  new LPFileSharingAccessManagement__c();
	    obj.newSharingRecord.First_Name__c = 'test';
	    obj.newSharingRecord.Email__c = 'test123@test.com';
	    obj.newSharingRecord.Password__c = 'test123@test.com';
	    obj.addNewSharingRecord();
	    ApexPages.currentPage().getParameters().put('index','0');
	    obj.removeSelectedContact();	    
	    obj.discardSelection();
	    
	    obj.newSharingRecord =  new LPFileSharingAccessManagement__c();
	    obj.newSharingRecord.First_Name__c = 'test';
	    obj.newSharingRecord.Email__c = 'test@test.com';
	    obj.newSharingRecord.password__c = 'test123';
	    obj.addNewSharingRecord();
	    
	    obj.shareFAMRecords();	    
	    obj.updateBitlyURL();
	    
	    ApexPages.currentPage().getParameters().put('accessId',accRec.Id);
	    obj.editedSharingRec();
	    
	    ApexPages.currentPage().getParameters().put('fname','test');
	    ApexPages.currentPage().getParameters().put('lname','test');
	    ApexPages.currentPage().getParameters().put('pwd','testm');
	    ApexPages.currentPage().getParameters().put('inCorrAttmp','0');
	    obj.updateEditedRecord();
	    
	    ApexPages.currentPage().getParameters().put('accessId',accRec.Id);
	    obj.deleteAccessRecord();
	    
	    Test.stopTest();	    
	}

}