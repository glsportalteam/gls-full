/*
* Class 		: 	GLSCommunity_FAMCtrl
* Description	:	Controller extension class for creating new or edit existing LP File Sharing Access Managemnet records,
*					user will include the ability to-
*					1. Edit the LPFileSharingAccessManagement__c records.
*                   2. Mail will be send to users on creating or editing new records.
*					3. Delete the already existing records
*					4. Add and remove contacts from multi select picklist show in panel at top.
*					
* Dependencies	: 
*		Classes		: 	GLSConfig,Bitly
*		VF Page		: 	GLSCommunity_FAMPage
*				   	      
*/

public without sharing class GLSCommunity_FAMCtrl {
	
	public List<contact> contactLst                                                 {get;set;}
	
	public List<SelectOption> availableUsersOption                                  {get;set;}
	
	/*It is used for creating new sharing record from Not In List scetion on the page*/
	public LPFileSharingAccessManagement__c newSharingRecord                        {get;set;}
	
	private Map<Id,Id> contactToUserIdMap                                           {get;set;}
	
	private Map<Id,User> contactToUserMap                                           {get;set;}
			
	/*List for showing the already existing file access management records in the related list*/
	public list<LPFileSharingAccessManagement__c> fileSharingRecordLst              {get;set;}
	
	/*This list is used to display the list of selected users*/
	public List<sharingRecordWrapper> selectedUserLst                               {get;set;}
	 
	String[] selectedUsers = new String[]{};
	
	/*Map of contact Id to contact record, Used for creating the selected USer list*/
	Map<Id,contact> mapIdContact                                                    {get;set;}
	
	public Quote__c quoteObj                                                        {get;set;}
	
	public Account accountObj                                                       {get;set;}   
	
	public List<LPFileSharingAccessManagement__c> lstSelectedContacts               {get;set;}
	
	private Map<Id,String> contactIdBitlyURLMap                                     {get;set;}
	
	public boolean isAllRecordProcessed                                             {get;set;}
	
	public LPFileSharingAccessManagement__c editedSharingRec                        {get;set;}
	
	Private String quoteId;
	Private String accountId;
	private boolean isSharedSuccess                                                 {get;set;}
	
	public boolean isPortalUser                                                     {get;set;}
	
	/* Constructor */
	public GLSCommunity_FAMCtrl(){
			
		/*Fetch the account Id and quote Id from the URL parameters*/
		accountId = ApexPages.currentPage().getParameters().get('accId');		
		quoteId = ApexPages.currentPage().getParameters().get('quoteId');
		isSharedSuccess = false;				
		isAllRecordProcessed = false;	
		mapIdContact = new Map<Id,contact>();
		selectedUserLst =  new List<sharingRecordWrapper>();
		newSharingRecord = new LPFileSharingAccessManagement__c();
		contactToUserIdMap = new Map<Id,Id>();
		contactToUserMap = new Map<Id,User>();
		isPortalUser = false;
		if(accountId != null && quoteId != null){
			/*Fetch quote object*/
			quoteObj = [SELECT id,name,Status__c FROM quote__c WHERE id=:quoteId limit 1];
			
			/*Fetch account object*/
			accountObj = [SELECT id,name FROM account WHERE id=:accountId limit 1];
			
			/*For population list of available options*/
			populateAvailableUser();			 						   			
		}
	}
	
	/*
	*	Method		:	populateAvailableUser	 
	*	Description	:	Fetch the existing LPFileSharingAccessManagement__c and the contact record associated with them 
						and populate the list of availableContact that needs to be shown to the user.				 
	*   param       :   none
	* 	return 		: 	List<Contact>						
	*/
	public void populateAvailableUser(){
		
		/* below list contains the list of all the contact associated with acconut Id 
		   passed as a parameter.
		*/
		contactLst = getContactList(accountId);
		
		/* below list contains all the existing LPFileSharingAccessManagement__c records against
		   the quote id passed as a parameter.
		*/												   
		fileSharingRecordLst = getLPFileSharingAccessManagement(quoteId); 						   			
		
		availableUsersOption = new List<SelectOption>();
								   
		if(contactLst != null && contactLst.size() > 0){								
			
			List<User> usersLst = [SELECT id,ContactId,Username
								   FROM User 
								   WHERE contactId IN:contactLst 
								   LIMIT : (Limits.getLimitQueryRows()- Limits.getQueryRows())];
								   
			if(usersLst != null && usersLst.size() > 0){
				for(User s : usersLst){											
					contactToUserMap.put(s.contactId,s);
				}																   
			}
			
			/*Iterate on each existing LPFileSharingAccessManagement__c for removing contacts from multi select 
			  picklist against which sharing reord already exists
			*/
			for (Contact con : contactLst) {
				Boolean isAlreadyExist = false;
				if(fileSharingRecordLst != null && fileSharingRecordLst.size() > 0){
					for(LPFileSharingAccessManagement__c accessRec : fileSharingRecordLst){																					
						if(con.Email == accessRec.Email__c ){
							isAlreadyExist = true;
						}																																	
					}
				}
				
				/*If access record does not already exist against the contact than add that contact in the list 
				  showing available contact list. 
				*/
				if(!isAlreadyExist){
					availableUsersOption.add(new SelectOption(con.Id,con.name));
	            	mapIdContact.put(con.Id,con);
				}
	        }
		}
	}
	
	/*Getter for selectedUser*/
	public String[] getselectedUsers() {
        return selectedUsers;
    }
    
    /*Setter for selectedUser*/
    public void setselectedUsers(String[] selectedUsers) {
        this.selectedUsers = selectedUsers;
    }
    
	/*
	*	Method		:	getContactList	 
	*	Description	:	Returns the list of all contact associated with the account id passed.				 
	*   param       :   Account Record Id
	* 	return 		: 	List<Contact>						
	*/	
	private List<contact> getContactList(String accId){
		return [SELECT id,name,FirstName,LastName,Email,isPortalActivated__c
			    FROM contact 
			    WHERE AccountId=:accId
			    ORDER BY LastName
			    LIMIT : (Limits.getLimitQueryRows()- Limits.getQueryRows())];
	}
	
	/*
	*	Method		:	getLPFileSharingAccessManagement	 
	*	Description	:	Returns the list of all already existing LPFileSharingAccessManagement__c records 
	*                   against the quote id passed as a parameter				 
	*   param       :   Quote Record Id
	* 	return 		: 	List<LPFileSharingAccessManagement__c>						
	*/
	private List<LPFileSharingAccessManagement__c> getLPFileSharingAccessManagement(String qId){
		return [SELECT id,Email__c,First_Name__c,IncorrectLoginAttempt__c,User_Id__r.Name,
					   isAccessActive__c,Last_Name__c,Password__c,Name,
					   Quote_Id__c,User_Id__c,User_Id__r.contactId,User_Id__r.contact.isPortalActivated__c 
			   FROM LPFileSharingAccessManagement__c 
			   WHERE Quote_Id__c =: qId And isAccessActive__c = true
			   LIMIT : (Limits.getLimitQueryRows()- Limits.getQueryRows())];
	}
	    
    /*
	*	Method		:	addSelectedUsers	 
	*	Description	:	This method is used for creating LPFileSharingAccessManagement__c object records, for the 
	*					selected contact in left side panel(Available Users), on the click of move button. 	                    	
	*   param       :   None
	* 	return 		: 	PageReference(Null)						
	*/
    public PageReference addSelectedUsers(){
    	/* Iterate on each selected option value */
    	if(selectedUsers != null && selectedUsers.size() > 0){
    		/*Iterate on each selected contact*/
	    	for(String selectedUser : selectedUsers){
	    		
	    		if(mapIdContact.containsKey(selectedUser)){
		    		contact c = mapIdContact.get(selectedUser);
		    		
		    		/*
		    		  Check that if access record is already exist against the corresponding contact email id than show user 
		    		  an error message.
		    		*/
		    		if(selectedUserLst != null && selectedUserLst.size()>0){
			    		for(sharingRecordWrapper sharingObj : selectedUserLst){
			    		    if(c.Email != null && c.Email == sharingObj.accessRec.Email__c){
			    				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'File Sharing Record already exists with the selected contact email address.'));
			    				return null; 
			    			}
			    		}
		    		}		    				    		
		    		SharingRecordWrapper sharingRec = new SharingRecordWrapper(); 
		    		LPFileSharingAccessManagement__c userObj = new LPFileSharingAccessManagement__c();
		    		
	    			userObj.Email__c = c.Email;
	    			userObj.First_Name__c = c.FirstName;
	    			userObj.Last_Name__c = c.LastName;
	    			userObj.isAccessActive__c = true;
	    			userObj.IncorrectLoginAttempt__c = 0;
	    			if(quoteId != null && quoteId != ''){
	    				userObj.Quote_Id__c = quoteId;
	    			}
	    			
	    			if( contactToUserMap.containsKey(c.Id) && contactToUserMap.get(c.Id).Id != null ){
	    				userObj.User_Id__c = contactToUserMap.get(c.Id).Id;	
	    			}
	    					    			
		    		sharingRec.accessRec = 	userObj;
		    		sharingRec.contact = c;
		    		selectedUserLst.add(sharingRec);
		    		
		    		for(Integer i = 0; i < availableUsersOption.size(); i++){
		    			if(availableUsersOption[i].getValue() == selectedUser){
		    				availableUsersOption.remove(i);
		    				break;
		    			}
		    		}
	    		}
	    	}
    	}
    	return null;
    }
    
    /*
	*	Method		:	addNewSharingRecord	 
	*	Description	:	This method is for creating LPFileSharingAccessManagement__c record for the user that does
						not exists.											 	                    	
	*   param       :   None
	* 	return 		: 	PageReference(Null)						
	*/    
    public PageReference addNewSharingRecord(){
		SharingRecordWrapper sharingRec = new SharingRecordWrapper(); 
		
		/*
		  Check that if access record is already exist against the corresponding contact email id than show user 
		  an error message.
		*/
		if(selectedUserLst != null && selectedUserLst.size()>0){
    		for(sharingRecordWrapper sharingObj : selectedUserLst){
    			if(sharingObj.accessRec.Email__c != null && sharingObj.accessRec.Email__c == newSharingRecord.Email__c){
    				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'File Sharing Record already selected with the entered email address.'));
    				return null; 
    			}
    		}
		}	
		
		if(fileSharingRecordLst != null && fileSharingRecordLst.size() > 0){
			for(LPFileSharingAccessManagement__c sharingObj : fileSharingRecordLst){
    			if(sharingObj.Email__c != null && sharingObj.Email__c == newSharingRecord.Email__c){
    				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'File Sharing Record already exists with the entered email address.'));
    				return null; 
    			}
    		}
		}
			 
		newSharingRecord.isAccessActive__c = true;
		newSharingRecord.IncorrectLoginAttempt__c = 0;
		if(quoteId != null && quoteId !=''){
			newSharingRecord.Quote_Id__c = quoteId;
		}
		sharingRec.accessRec = 	newSharingRecord;
		selectedUserLst.add(sharingRec);
		newSharingRecord = new LPFileSharingAccessManagement__c();
		return null;
	}
	
	/*
	*	Method		:	removeSelectedContact	 
	*	Description	:	This method will remove the selected contact, on click of delete link, from list 
						of selected user section.
	*   param       :   Index of the record in table that needs to be deleted.
	* 	return 		: 	PageReference(Null)						
	*/    
    public void removeSelectedContact(){
    	String index = ApexPages.currentPage().getParameters().get('index');
    	if(index != null){
    	    Integer rowNum = Integer.valueOf(index);
			if(rowNum != null && selectedUserLst != null && selectedUserLst.size()>=rowNum){				
				if(selectedUserLst[rowNum].contact != null)
			    	availableUsersOption.add(new SelectOption(selectedUserLst[rowNum].contact.Id,selectedUserLst[rowNum].contact.name));
			    	
			    selectedUserLst.remove(rowNum); 
			}
    	}
    }
    
    /*
	*	Method		:	discardSelection	 
	*	Description	:	This method will deselect all the users selected by user, It will execute on click of cancel button. 						
	*   param       :   None
	* 	return 		: 	Void						
	*/    
    public void discardSelection(){
        for(sharingRecordWrapper sharingObj : selectedUserLst){
        	if(sharingObj.contact != null)
            	availableUsersOption.add(new SelectOption(sharingObj.contact.Id,sharingObj.contact.name));    
		}
		selectedUserLst =  new List<sharingRecordWrapper>();
    }
    
    /*
	*	Method		:	shareFAMRecords	 
	*	Description	:	This method will save the LPFileSharingAccessManagement__c records in database 
	*                   which are created from 'Selected Users' list,
						on click of 'Move' button or for new users from 'Not in list' section.  						
	*   param       :   None
	* 	return 		: 	Void						
	*/    
    public void shareFAMRecords(){
    	isSharedSuccess = false;
    	isAllRecordProcessed = false;
    	contactIdBitlyURLMap = new Map<Id,String>();
    	lstSelectedContacts = new List<LPFileSharingAccessManagement__c>(); 
    	Boolean isEmailEmpty = false;
    	Boolean invalidPass = false;
    	if(selectedUserLst!= null){
    		if(selectedUserLst.size()>100){
    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'Please select less than 101 contacts.'));
    		}else{
    			for(sharingRecordWrapper sharingObj : selectedUserLst){
    				/*Validate that Email__c is not null for each LPFileSharingAccessManagement__c record */
		    	    if( sharingObj.accessRec.Email__c != null ){
		    	        lstSelectedContacts.add(sharingObj.accessRec);    
		    	    }else{		    	        
		    	        isEmailEmpty = true;
		    	    }		    	    
		    	    /*Validate that password length is greater than 4 characters.*/
		    	    if( sharingObj.accessRec.Password__c != null && sharingObj.accessRec.Password__c.length() <= 4){
		    	        invalidPass = true;
		    	    }
				}
				/*Show error message on the page if validations gets fail otherwise insert record in database*/
				if(isEmailEmpty){
				    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'Email is required for all the selected users.'));
				}else if(invalidPass){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'Password length should be greater than 4 characters.'));
				}else{
				    if(lstSelectedContacts != null && lstSelectedContacts.size() > 0){
		    			try{
		    				insert lstSelectedContacts;
		    				fileSharingRecordLst.addAll(lstSelectedContacts);	
		    				isSharedSuccess = true;	    				
		    			}catch(DMLException e){
		    				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,e.getMessage()));
		    			}
		    		}    
				}
    		}
    	}
    }
        
    /*
	*	Method		:	updateBitlyURL	 
	*	Description	:	for populate bitly URL for each LPFileSharingAccessManagement__c records which is inserted 
					    on click of share button. It will executec once for each reord(To avoid callout time limit).
					    Bitly URL is populate by callout to Bitly services 	  						
	*   param       :   None
	* 	return 		: 	Void						
	*/
    public void updateBitlyURL(){    			
    	if(lstSelectedContacts != null && lstSelectedContacts.size() > 0 && isSharedSuccess){    		 
    		isAllRecordProcessed = false;   
    		/*Loop executed only once each time method is called*/		
    		for(Integer i=0;i<lstSelectedContacts.size();i++){
    			/*Callout to bitly services*/
    			Bitly b = new Bitly();
    			
    			/*Long URL*/
				b.sUrl = 'https://losangeles-prod-mygls.cs44.force.com/customer/GLSCommunity_ShareLinkPage?accessId='+lstSelectedContacts[i].Id;
				String bitLyLink='';				
				if(!Test.isRunningTest()){
					/*Returned short URL*/
					bitLyLink = b.getShortURL();
				}
				
				contactIdBitlyURLMap.put(lstSelectedContacts[i].Id,bitLyLink);
				lstSelectedContacts.remove(i);
				break;
    		}		
    	}else{    			
    		isAllRecordProcessed = true;
    	}	    	    	
    }    
    
    /*
	*	Method		:	updateAccessRecords	 
	*	Description	:	Bulk update the LPFileSharingAccessManagement__c records for saving generated Bilty URl from callout 	  						
	*   param       :   None
	* 	return 		: 	Void						
	*/
    public void updateAccessRecords(){    	 
    	if(contactIdBitlyURLMap != null && contactIdBitlyURLMap.size()>0){
    		List<LPFileSharingAccessManagement__c> accessRecLst = new List<LPFileSharingAccessManagement__c>();    		
    		for(Id recId : contactIdBitlyURLMap.keySet()){
    				LPFileSharingAccessManagement__c accRec = new LPFileSharingAccessManagement__c();
    				accRec.Id = recId;
    				accRec.Bitly_URL__c = contactIdBitlyURLMap.get(recId);
    				accessRecLst.add(accRec);
    		}
    		if(accessRecLst != null && accessRecLst.size() > 0){    			
    			update accessRecLst;    			 
    		}
    	}
    	if(isSharedSuccess){
	    	selectedUserLst =  new List<sharingRecordWrapper>();    
	    	lstSelectedContacts = new List<LPFileSharingAccessManagement__c>();
	    	populateAvailableUser();
    	}	
    	isSharedSuccess = false;
    }
    
    /*
	*	Method		:	editedSharingRec	 
	*	Description	:	Method used to capture the LPFileSharingAccessManagement__c record against which edit link is clicked
						in the table showing existing LPFileSharingAccessManagement__c records.  
	*   param       :   Id of the LPFileSharingAccessManagement__c record which needs to be edited.
	* 	return 		: 	Void						
	*/    
    public void editedSharingRec(){
    	isPortalUser = false;
    	String accessId = ApexPages.currentPage().getParameters().get('accessId');    	
    	editedSharingRec = new LPFileSharingAccessManagement__c();
    	if(accessId != null){    		
	    	for(LPFileSharingAccessManagement__c accRec :fileSharingRecordLst){	    		
	    		if(accRec.Id == accessId){
	    			if(accRec.User_Id__r != null && accRec.User_Id__r.contact.isPortalActivated__c != null){
	    				isPortalUser = accRec.User_Id__r.contact.isPortalActivated__c;
	    			}	    			
	    			editedSharingRec.Id =  accRec.Id;
	    			editedSharingRec.First_Name__c =  accRec.First_Name__c;
	    			editedSharingRec.Last_Name__c =  accRec.Last_Name__c;
	    			editedSharingRec.IncorrectLoginAttempt__c =  accRec.IncorrectLoginAttempt__c;
	    			editedSharingRec.Password__c =  accRec.Password__c;
	    			break;
	    		}
	    	}
    	}    	
    }
    
    /*
	*	Method		:	updateEditedRecord	 
	*	Description	:	Method used to update the LPFileSharingAccessManagement__c record in the database as per the 
					    changes made by the user using the edit popup on the page. 						
	*   param       :   --> Updated First name for record
						--> Updated Last Name for reocrd
						--> Updated Password.
						--> Updated incoorect login attempts
	* 	return 		: 	Void						
	*/
    public void updateEditedRecord(){
    	try{
    		String fname = ApexPages.currentPage().getParameters().get('fname');
    		String lname = ApexPages.currentPage().getParameters().get('lname');
    		String pwd = ApexPages.currentPage().getParameters().get('pwd');
    		String inCorr = ApexPages.currentPage().getParameters().get('inCorrAttmp');
    		    		
    		if(editedSharingRec != null){
    			editedSharingRec.First_Name__c = fname;
    			editedSharingRec.Last_Name__c = lname;
    			editedSharingRec.Password__c = pwd;
    			editedSharingRec.IncorrectLoginAttempt__c = Decimal.valueOf(inCorr);    			
    			update editedSharingRec;
    			
    			fileSharingRecordLst = getLPFileSharingAccessManagement(quoteId);
    		}
    		
    	}catch(DMLException dmlEx){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'An unexpected error occured, please contact system administrator.'));
    	}
    }
    
    /*
	*	Method		:	deleteAccessRecord	 
	*	Description	:	Method used to delete the LPFileSharingAccessManagement__c record against which delete link is clicked
						in the table showing existing LPFileSharingAccessManagement__c records.  
	*   param       :   Id of the LPFileSharingAccessManagement__c record which needs to be Deleted.
	* 	return 		: 	Void						
	*/    
    public void deleteAccessRecord(){
    	String accessId = ApexPages.currentPage().getParameters().get('accessId');
    	if(accessId != null){
    		LPFileSharingAccessManagement__c accRec = new LPFileSharingAccessManagement__c();
    		accRec.Id = accessId;
    		try{
    			delete accRec;
    			populateAvailableUser();
    			//fileSharingRecordLst = getLPFileSharingAccessManagement(quoteId);
    		}catch(DMLException ex){
    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'An unexpected error occured, please contact system administrator.'));
    		}	    	
    	}
    }
    
    /*Wrapper class*/
    public class SharingRecordWrapper{
    	public Contact contact                             {get;set;}
    	public LPFileSharingAccessManagement__c accessRec  {get;set;}
    }
    
}