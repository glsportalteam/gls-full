/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSLPW_CustomProtocolLookupController Class
* Function: This class is used as a controller for the GLSLPW_CustomProtocolLookup
*/

public with sharing class GLSLPW_CustomProtocolLookupController { 
  public List<Protocol__c> results{get;set;} // search results
  public string searchString{get;set;} // search keyword
  public String forInvoice   {get;set;}
  public string programId    {get;set;}
 
  public GLSLPW_CustomProtocolLookupController() {
    forInvoice = 'false';
    searchString = System.currentPageReference().getParameters().get('lksrch');
    forInvoice = System.currentPageReference().getParameters().get('forInvoice');
    programId  = System.currentPageReference().getParameters().get('prgmId');    
    runSearch();      
  }
 
  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }
 
  // prepare the query and issue the search command
  private void runSearch() {
    if(searchString != null)
        searchString = String.escapeSingleQuotes(searchString);
    if(forInvoice!=null && forInvoice=='true'){
        results = performSearchForProgram(searchString);
    }else{
        results = performSearch(searchString);
    }                       
  } 
 
  // run the search and return the records found. 
  private List<Protocol__c> performSearch(string searchString) {
    String account = System.currentPageReference().getParameters().get('account');
    List<Program__c> lstPrgram;
    List<Protocol__c> lstProtocol = new List<Protocol__c>();    
    if(account != null && account != ''){
        lstPrgram = [select id from Program__c where Account__r.id=:account];
    }   
    List<String> LstProgramId = new List<String>();    
    if(lstPrgram!=null && lstPrgram.size()>0){
        for(Program__c prgm : lstPrgram){
            LstProgramId.add(prgm.id);
        }       
    	String soql = 'select id,name,Program_Name__r.Program_Name__c,Status__c,Protocol_Name__c from Protocol__c';
	    if(searchString != '' && searchString != null)
	      soql = soql +  ' where name LIKE \'%' + searchString +'%\' and (Status__c=\'Pending\' or Status__c=\'Open\')';
	    else 
	      soql = soql +  ' where name LIKE \'%\'and (Status__c=\'Pending\' or Status__c=\'Open\')';  
	    if(LstProgramId != null && LstProgramId.size() > 0)
	      soql = soql + ' AND Program_Name__r.id IN : LstProgramId';
	    soql = soql + ' order by Name Desc limit 250';	    
	    return database.query(soql);            
    }else{
    	return null;
    }                
  }
  
  // run the search and return the records found. 
  private List<Protocol__c> performSearchForProgram(string searchString) {          
    String soql = 'select id,name,Program_Name__r.Program_Name__c,Status__c,Protocol_Name__c,(select id,Status__c from Quotes__r) from Protocol__c';
    if(searchString != '' && searchString != null)
      soql = soql +  ' where name LIKE \'%' + searchString +'%\'';
    else 
      soql = soql +  ' where name LIKE \'%\'';  
      
    if(programId != null && programId != '')
      soql = soql + ' AND Program_Name__r.id =: programId';
      
    soql = soql + ' order by Name Desc limit 250';
    System.debug(soql);
    List<Protocol__c> lstProtocols = database.query(soql);
    List<Protocol__c> lstValidProtocols = new List<Protocol__c>();
    for(Protocol__c pro : lstProtocols){
    	Boolean isBillableQuoteExist = false;
    	List<Quote__c> lstQuotes = pro.Quotes__r;
    	if(lstQuotes != null && lstQuotes.size() > 0){
    		for(Quote__c quote: lstQuotes){
				if(quote.status__c == GLSConfig.BillableStatus){
					isBillableQuoteExist = true;
					break;
				}		    	
	    	}
    	}
    	if(isBillableQuoteExist){
    		lstValidProtocols.add(pro);
    	}    	    	
    } 
    
    return lstValidProtocols;  
  } 
  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
 
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
}