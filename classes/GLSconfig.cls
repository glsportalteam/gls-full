/** Copyright (c) 2013, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSconfig
* Function: This class contains ll the hard coding values for error messages and EndPoint URL's parameters
*/

public with sharing class GLSconfig {
     
   // public static String tradosURLgetExt = '/WebSetupWcfService/ServiceTrados.svc/rest/';
   // public static String tradosURLAnalyze = '/WebSetupWcfServiceTest/ServiceTrados.svc/rest/';
   //public static String bucketName     = 'gls-us';
    public static List<Trados_Configuration__c> lstTradosConf = Trados_Configuration__c.getall().values();
    public static String bucketName  = lstTradosConf.size()>0 ? lstTradosConf[0].Storage_Bucket__c: '' ;
    public static Boolean skipQuotePDFFileTrigger = true;
    // Community Profile 
    public static String Communityprofile     			= 'Account Manager';
    public static String SystemAdministratorProfile  	= 'System Administrator';
    // tradosEndpointURl parameters 
    public static String paramSecretKey     = 'secretKey=';
    public static String paramBucketName    = '&bucketName=';
    public static String paramQuoteId       = '&quoteId=';
    public static String ParamoutputPath    = '&outputPath=';
    public static String paramGlsID         = '&glsIds=';
    public static String ParamSfFileIds     = '&SalesforceFileIds=';
    public static String ParamSfFileId      = '&SalesforceFileId=';
    public static String paramFileName      = '&fileNames=';
    public static String paramVersionId     = '&versionIds=';   
    public static String paramSource        = '&source=';  
    public static String paramClientId      = '&clientId=';  
    public static String paramtranslationMemory = '&translationMemory=';    
    public static String paramSourceLanguage = '&sourceLanguage=';
    public static String paramTargetLanguage = '&targetLanguage=';
    public static String paramUserId = '&userID=';
    public static String paramFilesId = '&filesId=';
    public static String paramAPIName = '&appName=';
    public static String paramIPAddress = '&IPAdress=';
    public static String paramReportCrossFileRepetitions = '&reportCrossFileRepetitions=';
    public static String paramReportInternalFuzzyMatchLeverage = '&reportInternalFuzzyMatchLeverage=';
    public static String paramOutputFormat = '&outputformat=';
    public static String paramFullTmExport = '&fullTmExport=';
    public static String paramCallDesc = '&callDesc='; 
    public static String paramcatTool = '&catTool=';
    
    public static String save_as_draft = 'Save as Draft';
    public static String Submit_Order = 'Submit Order';
    public static String Place_into_Production = 'Place into Production'; 
    
    public static string B_Goals_Shared ='B-Goals Shared';
     
    // Static values for GLSAPIListener
    public static String success = 'true';
    public static String fail = 'false';
    public static String sNull = 'NULL';
    public static String strue = '0';
    public static String sfalse = '1';
    public static String TransactionIDError = 'Transaction Id is missing'; 
    public static String TransactionIDNotMatched = 'No matching Transaction Id found in Salesforce';
    public static String SFUpdateFailes = 'Salesforce Update Failed';
    public static String AnalyzeFileURLNotDownloaded = 'Could not download Analyze File URL';
    public static String  AnalyzeFileSignatureNoGenerated = 'Could not generate S3 Signature';
    public static String xmlDataInvalid = 'Invalid XML data';
    public static String InvalidSFFileIds ='Invalid Salesforce File Id';
    public static String xmlParserIssue = 'Could not parse the Analyze File data';
    public static String XMLParseFailes = 'Failed to parse XML';
    
    //Parameters for QLI Status
    public static String ErrorStat = 'Error';
    public static String InCompleteStat = 'Incomplete';
    public static String SuccessStat = 'Success';
    public static String InPrepStat = 'In Preparation';
    public static String QLIAnalyzedStat = 'QLI Analyzed';
    public static String FailStat = 'Fail';
    public static String InProcessStat = 'In Process';
    
    //Parameters for defining API Call.
    public static String tmExportAPICall = 'TMExport';
    public static String analyzeFileAPICall = 'AnalyzeFiles';
    public static String getExternalFileAPICall = 'GetExternalFile';
    
    
    public static String assignedToGrpName  = 'assignedTo';
    public static String prodnDirectorGrpName  = 'Production Director';
    
    public static String DMLExceptionOccured = 'DML Exception occurred.';
    public static String ExceptionOccured = 'Exception occurred.';
    
    //Paramters for actual values for the tradosEndpointURl parameters   
    public static String namespaceTrados    ='http://schemas.datacontract.org/2004/07/TradosWcfService';
    public static String glsBucket = 'gls-bucket';
    public static Integer outputformat = 2;
    public static Boolean fullTmExport = false;
    public static String pdfExtension = '.pdf';
    public static String fileSource = 'Source';
    public static String access = 'public-read';
    public static String Automatic = 'Automatic';
    
    public static String standardDelivery = 'Standard';
    public static String expeditedDelivery = 'Expedited'; 
    public static String QuoteLineItemNotCreated = ' Quote line Item Not Created.'; 
    public static String NoError = 'No Error';
    public static String FileNotAssociateProperly = 'QLI Files Were Not Associated Properly.';
    public static String LineItemCreatedSuccessfully = 'Quote Line Item Created Successfully.';
    public static String TransactionIdNotUpdated = 'Source Quote Files Transaction Ids not updated.';
    public static String successCode = '200';
    public static String analyzeComplete ='Analyze All In Process, Please wait for few Minutes to complete the process.';
    public static String analyzeOneComplete = 'Analyze One QLI In Process, Please wait for few Minutes to complete the process.';
    public static String analyzeFail ='Analyze Transaction Failed.';
    public static String TMExportSuccess ='TM Export Success.';
    public static String TMExportFail ='TM Export Fail.';
    public static String QuoteAsstFileNotAssociated ='Quote Line Items Not Created - Please Select Quote Assignment Files.';
    public static String QuoteAsstNotCreated ='Quote Line Items Not Created - Please Create Quote Assignment First.';
    public static String NoQLICreatedforAnalyze = 'Quote Line Items Have Already Been Analyzed.';
    public static String NoQLICreatedforTMExport = 'No QLI Available for TM Export.';
    public static String NoTargetLanguageSelected = 'Please Select Target Language.';
    public static String NoSourceFileIsSelected='Quote Line Item Created Successfully. WARNING: No files are associated with the Assignment(s).';
    public static String QLIAlreadyCreated = 'Quote Line Items Have Already Been Created.';
    public static String TransactionIdNotFound = 'Transaction Id Not Found from Trados Server Response.';
    public static String ResponseNotRecieved = 'No Response Received from Trados Server';
    public static String NoServicesSelected = 'Please Select Quote Assignment Services.';
    public static String QLIServicenotAdded = 'Qoute Line Service Not Added.';
    public static String QLIFileTradosIdNotFound = 'Trados Id of Source Files Not Found for ';
    public static String pdfAttachmentName = 'GLS_Quote.pdf';
    public static String TMFileNotAdded = 'Please Select TM File for ';
    public static String NoFilesAdded = 'Files for analysis have not been selected. Please select at least one file, then Save and Done to continue for Language Pair- ';
    public static String NoTradosConfigurationSetup = 'Please Setup Trados Configuration Setting.';
    public static String inCorrectSelecteddate = 'From Date Must Be Less Than To Date';
    public static String SourceLangNotSelected = 'Please Select Source Language.';
    public static String noFilterCriteriaSelected = 'Please enter at least one filter criterion.';
    public static String AnalyzeDataNotRecieved = 'Analysis Data not Recieved From Trados';
    public static String TradosAPIIdNotRecieved = 'Communication error between systems, please try again for Language Pair- ';
    public static String TradosAPIQLIErrorMsg = 'Communication error between systems';
    public static String AnalyzedTransIdNotRcvd = 'Analyzed Transaction Id Not Received from Trados Server';
    public static String SpecialCharFilesNotAllowed = 'Special Character Files Not Allowed for Analysis';
    public static String SpecialCharFileWarnMsg = ' WARNING: External analysis suggested. One or more files contain special characters, the Trados API will not be able to analyze file(s).';
    public static String TxnInErrorMsg = ' WARNING: External analysis suggested. Communication error between systems.';
    
    /**
    * Parameters used in GLSQW_DefaultLoader Class
    */
    public static String SOWItemSuccess = 'Default SOW Items Loaded Successfully.';
    public static String SOWItemCopySuccess = 'SOW Items Copied Successfully.';
    public static String SOWItemPushSuccess = 'SOW Items Pushed Successfully.';
    public static String SOWTermsSuccess = 'Default Terms Loaded Successfully.';
    public static String SOWTermCopySuccess = 'Terms Copied Successfully.';
    public static String SOWTermPushSuccess = 'Terms Pushed Successfully.';
    public static String RSheetSuccess = 'Default Rate Sheet Loaded Successfully.';
    public static String RSheetCopySuccess = 'Rate Sheet Copied Successfully.';
    public static String RSheetPushSuccess = 'Rate Sheet Pushed Successfully.';
    public static String SOWItemFail = 'Default SOW Items Loading Failed.';
    public static String SOWItemCopyFail = 'SOW Items Copy Failed.';
    public static String SOWItemPushFail = 'SOW Items Push Failed.';
    public static String SOWTermsFail = 'Default Terms Loading Failed.';
    public static String SOWTermCopyFail = 'Terms Copy Failed.';
    public static String SOWTermPushFail = 'Terms Push Failed.';
    public static String RSheetFail = 'Default Rate Sheets Loading Failed.';
    public static String RSheetCopyFail = 'Rate Sheets Copy Failed.';
    public static String RSheetPushFail = 'Rate Sheets Push Failed.';
    public static String SOWItemExist = 'Default SOW Items Already Exist.';
    public static String SOWItemCopyExist = 'SOW Items Already Exist.';
    public static String SOWTermsExist = 'Default Terms Already Exist.';
    public static String SOWTermsCopyExist = 'Terms Already Exist.';
    public static String RSheetsExist = 'Default Rate Sheets Already Exist.';
    public static String RSheetsCopyExist = 'Rate Sheets Already Exist.';
    public static String SOWItemNotFound = 'Default SOW Items Not Found.';
    public static String SOWItemNotFoundCopy = 'No SOW Items found in parent account.';
    public static String SOWTermsNotFound = 'Default Terms Not Found.';
    public static String SOWTermsNotFoundCopy = 'No Terms found in parent account.';
    public static String RSheetNotFound = 'Default Rate Sheets Not Found.';
    public static String RSheetNotFoundCopy = 'No Rate Sheet found in parent account.';
    public static String clientIdNotFound = 'Client Id is not Valid.';
    public static String LoadAllDefaultsFail = 'Load All Defaults Fail.';
    public static String CopyFail = 'Copy Records Fail.';
    public static String pushFail = 'Push Records Fail.';
    public static String ChildAccountNotFound = 'Child Accounts Not Found.';
    
    /*
    AWS Files related labels
    */
    public static String filetypeObsolete = 'Obsolete';
    public static String filetypeActive = 'Active';
    public static Integer quotePDFVersion = 1;
    public static String Released = 'Released';
    public static String Approved = 'Approved';
    public static String Cancelled = 'Cancelled';
    public static String In_Production = 'In Production';
    public static String Direct_Order = 'Direct Order';
    public static String Completed = 'Completed';
    public static String Expired = 'Expired';
    public static String Open = 'Open';
    public static String Pending = 'Pending';
    
    public static String BilledStatus = 'Billed';
    public static String BillableStatus = 'Billable';           
    
    public static String LP_Direct_Order = 'Direct Order';
    public static String CCLS_Direct_Order = 'Direct Order-RP';
    
    public static String EstStatusObsolete  =   'Obsolete';   
    
    public static String Invoice_Email_Template  =   'Invoice_Email_Template';
    
    public static String BDMGroupName = 'Business Development Manager';
    /*
    Parent Object types
    */
    public static String QuoteObj = 'Quote__c';
    public static String QuoteFileObj = '	Quotes_File__c';
    public static String QuotePDFFileObj = 'Quote_PDF_File__c';
    
    public static String EstimateObj = 'Estimate__c';
    public static String EstimateFile = 'Estimate File';
    public static String EstFileTypeSource = 'Estimate';
    public static String EstFileTyperef = 'Reference';
    
    
    public static String ProgramObj = 'Program__c';
    public static String referenceFile = 'Reference File';
    public static String programRefFile = 'Program Reference File';    
    
    public static String purchaseAgreementObj = 'Purchase_Agreement__c';
    public static String purchase_agreement_file = 'Purchase Agreement File';
    /*
    Quote objects File Types
    */
    public static String Quote_File = 'Quote File';
    public static String QuotePDF = 'Quote PDF';
    
    
    public static String Overall_Costs = 'Overall Costs';
    public static String Turnaround_time = 'Turnaround time';
    public static String Exceeded_end_clients_budget = 'Exceeded end-clients budget';
    public static String Vendor_selection_by_end_client = 'Vendor selection by end-client';
    
    /*
    Quote object valid status for file upload
    */
    public static String NewQuote = 'New';
    public static String InPreparationQuote = 'In Preparation';
    public static String IncompleteQuote = 'Incomplete';
    public static String InReviewQuote = 'In Review';
    public static String ReviewedQuote = 'Reviewed';
    public static String DraftQuote = 'Draft';      
    public static list<string> ValidQuoteStatus = new List<string>{'New','Draft','In Preparation','Incomplete','In Review','Reviewed','Direct Order'};
    public static list<string> ValidEstimateStatus = new List<string>{'New','Draft','In Preparation','Reviewed','Released'};
    public static list<string> ValidVersionStatus = new List<string>{'New','Draft','In Preparation','In Review','Reviewed'};
         
    /*Set to store the status values to enable the button*/
    public static Set<String> btnEnableStatus = new Set<String>{'New', 'Draft', 'In Preparation','Incomplete','In Review','Reviewed', 'Direct Order', 'Billable', 'Billed'};
    
    /*Error message displayed on the New Program Page*/
    public static String enterSponsorRef = ' Please enter Sponsor or Sponsor Account.';
    public static String enterProgramName = 'A Program with the same name already exists for this Account. Please enter a different Program Name.';
    
    /*Error message displayed on the Estimate Line Page*/
    public static String selectCountry = 'Please select Country from the list.';
    public static String selectProtocol = 'Please enter Protocol';
    
    /*Set to store the status value to delete Quote PDF from notes and Attachments*/
	public static Set<String> deletePDFStatus = new Set<String>{'Cancelled', 'Expired', 'Completed'};
    
    /*Strings to hold the values for Submit For process*/
    public static String EstimateTracker = 'EstimateTracker';
    public static String InternalApproval = 'InternalApproval';
    public static String ReleaseToClient = 'ReleaseToClient';
    public static Set<String> statusSaveAmount = new Set<String>{'New', 'In Preparation', 'Reviewed'};
    public static Set<String> statusBlockELIAction = new Set<String>{'In Review', 'Released', 'Approved'};
    public static String recallMsg = 'Amount updated. Recalling the approval process.';
    public static String submitMsg = 'Submitting request for approval.';
    public static String recallAction = 'Removed';
    public static String taskStatus = 'Not Started';
    public static String taskPriority = 'Medium';
    public static String taskPublicGroup = 'Contracts Manager';
    public static String taskSubject = 'Estimate Submitted for Tracker';
    public static String contractManagerProfile = 'Contract Manager';  
    public static String warningMsg = 'Sorry! Your profile name does not match with the Custom Setting. Please contact your System Administrator.';  
        
    public static string Clone = 'Clone';
    public static string CloneFilesWithFiles = 'CloneFilesWithFiles';
    
    /*Strings to hold the estimate types like changeOrder,Estimate revision and Estimate version*/
    public static String ChangeOrder = 'ChangeOrder';
    public static String EstimateVersion = 'EstimateVersion';
    public static String EstimateRevision = 'EstimateRevision';
    
    /*Method to return the values of "Submit For" select list for LP Estimate Page*/
    public static List<SelectOption> submitForValueConfig(){
    	List<SelectOption> lstSubmitForValue = new List<SelectOption>();
		lstSubmitForValue.add(new SelectOption('','--None--'));
		lstSubmitForValue.add(new SelectOption('EstimateTracker','Estimate Tracker'));
		lstSubmitForValue.add(new SelectOption('InternalApproval','Internal Approval'));
		lstSubmitForValue.add(new SelectOption('ReleaseToClient','Release To Client'));		
		return lstSubmitForValue;
    }
    
    /*Method to return the values of "Client Decision" select list for LP Estimate Page*/
    public static List<SelectOption> clientDecisionConfig(){
    	List<SelectOption> lstClientDecisionValues = new List<SelectOption>();
		lstClientDecisionValues.add(new SelectOption('','--None--'));
		lstClientDecisionValues.add(new SelectOption('Approved','Approved'));
		lstClientDecisionValues.add(new SelectOption('Cancelled','Cancelled'));
		lstClientDecisionValues.add(new SelectOption('Obsolete','Obsolete'));
		lstClientDecisionValues.add(new SelectOption('EstimateRevision','Estimate Revision'));
		lstClientDecisionValues.add(new SelectOption('EstimateVersion','Estimate Version'));
		lstClientDecisionValues.add(new SelectOption('ChangeOrder','Change Order'));
		return lstClientDecisionValues;
    }
    
    /*Method to return the values of "Protocol Type" select list for LP Estimate Line Page*/
    public static List<SelectOption> protocolTypeConfig(){
    	List<SelectOption> lstprotocolTypeValues = new List<SelectOption>();
		lstprotocolTypeValues.add(new SelectOption('New','New'));
		lstprotocolTypeValues.add(new SelectOption('Existing','Existing'));
		return lstprotocolTypeValues;
    }
    
    /*Method to send emails to the submitter of job once the execution of delete Quote PDF File Batch is finished*/
    public static void sendEmailBatchTracking(String batchName, Integer proJobItems, Integer totalJobItems, Integer errorJobItems, 
    											Integer successCount, Integer failureCount, String status, dateTime completedTime,
    											Long seconds, String toAddress){
    	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[] {toAddress};
		mail.setToAddresses(toAddresses);
		mail.setSubject('Record Clean Up Status: ' + status);
		String messageBody = '<html><body><b>The execution of Batch ' + batchName 
							+ ' has completed.</b><br/> Below is the report for Batch Execution: <br/><br/>Total number of Job Items : ' + 
							+ totalJobItems + '<br/> Number of Job Items processed : ' + 
							+ proJobItems + '<br/> Number of Job Items containing error : ' + 
							+ errorJobItems + '<br/> Number of records successfully deleted : ' + 
							+ successCount + '<br/> Number of records that encountered error : ' + 
							+ failureCount + '<br/> Duration for Batch Run : ' + 
							+ seconds + ' seconds<br/> Batch Completed Time : ' +
							+ completedTime + '<br/><br/> Thank You!</body></html>';
		mail.setHtmlBody(messageBody);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }  
    
    
    /* Estimate Cancellation Reason*/
      public static String OverAllCost = 'Overall Costs';
      public static String TurnaroundTime = 'Turnaround Time';
      public static String ExceededClientBudget = 'Exceeded end-clients budget';
      public static String VendorSelection = 'Vendor selection by end-client';
      
      /* Opportunity Cancellation Stage*/
      public static String Lostpricing = 'L-Lost Pricing';
      public static String Lostschedule = 'L-Lost Schedule';
      public static String Lostcompetition = 'L-Lost to Competition';
      public static String LostGLSWithdraw = 'L-Lost Customer Withdrew';
      public static String Lostother = 'L-Lost';
      
      
      /* User Access Managment Profile Name */
      public static String AccountManager = 'Account Manager';
      public static String ProgramManager = 'LP Program Manager';
      public static String ProtocolManager = 'LP Protocol Manager';
      public static String LPStdUser = 'LP Standard User';
      public static String RPStdUser = 'RP Standard User';
      
      
      /* Quote File Type */
      public static String SourceFT = 'Source';
      public static String ReferenceFT = 'reference';
      public static String PreviousTFT = 'Previous Translation';
      public static String Translation = 'translation';
      public static String ReferenceFolder = 'Reference';
      
}