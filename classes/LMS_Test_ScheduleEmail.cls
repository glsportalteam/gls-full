@isTest
public class LMS_Test_ScheduleEmail{
    public static Training__c training1;
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    @isTest
    public static void testScheduleEmail() {
    LMS_Test_Util util = new LMS_Test_Util();
             system.runAs(LMS_Test_Util.systemAdmin){
           training1= new Training__c();
            
            training1.Training_Name__c = 'Test schedule Training';
            training1.Training_Methodologies__c= 'Document Oriented';
            training1.URL__c ='www.google.com';
            training1.Training_Description__c='test description';
            training1.Duration__c = 0;
            training1.criterion__c = 0;
            training1.start_date__c = Date.today() - 5 ;
            training1.end_date__c = Date.today().addDays(5);
            training1.skills__c='java';
            training1.Trainer_Name__c= 'Mr Bruce';
            training1.Active__c = true;
            training1.Training_Required_for__c = 'Project Manager';
            
           Database.insert (training1); 
           Training__c training2= new Training__c();
            
            training2.Training_Name__c = 'Test schedule Training2';
            training2.Training_Methodologies__c= 'Document Oriented';
            training2.URL__c ='www.google.com';
            training2.Training_Description__c='test description';
            training2.Duration__c = 2;
            training2.criterion__c = 2;
            training2.start_date__c = Date.today() - 5 ;
            training2.end_date__c = Date.today().addDays(5);
            training2.skills__c='java';
            training2.Trainer_Name__c= 'Mr Bruce';
            training2.Active__c = true;
            training2.Training_Required_for__c = 'Translator';
            
           Database.insert (training2);            
           User_Training__c batchUserTraining = new User_Training__c();
            
            batchUserTraining.Training__c = training1.id;           
            batchUserTraining.status__c = 'In Progress';           
            batchUserTraining.User_Name__c = LMS_Test_Util.translator.id;
            batchUserTraining.Date_Of_Enrollment__c = Date.today();
            
            Database.insert(batchUserTraining);   
             User_Training__c batchUserTraining1 = new User_Training__c();
            
            batchUserTraining1.Training__c = training2.id;           
            batchUserTraining1.status__c = 'In Progress';           
            batchUserTraining1.User_Name__c = LMS_Test_Util.projectManager.id;
            batchUserTraining1.Date_Of_Enrollment__c = Date.today();
            
            Database.insert(batchUserTraining1);   
            }
         Test.startTest();
        
      // Schedule the test job
      String jobId = System.schedule('LMS_ScheduleEmail',
                        CRON_EXP, 
                        new LMS_ScheduleEmail    ());
         
      // Get the information from the CronTrigger API object
      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];

      // Verify the expressions are the same
      System.assertEquals(CRON_EXP, 
         ct.CronExpression);

      // Verify the job has not run
      System.assertEquals(0, ct.TimesTriggered);

      // Verify the next time the job will run
      System.assertEquals('2022-03-15 00:00:00', 
         String.valueOf(ct.NextFireTime));
      // Verify the scheduled job hasn't run yet.
     // Merchandise__c[] ml = [SELECT Id FROM Merchandise__c 
     //                        WHERE Name = 'Scheduled Job Item'];
      //System.assertEquals(ml.size(),0);
            
            test.stopTest();
    }


}