/** 
* This global class is used for -
* 1. Load Default SOW Items
* 2. Load Default SOW Terms
* 3. Load Default Rate Sheet
* 4. Copy Parent Account SOW Items
* 5. Copy Parent Account SOW Terms
* 6. Copy Parent Account Rate Sheet
* 7. Push Parent Account SOW Items to Child Account
* 8. Push Parent Account SOW Terms to Child Account
* 9. Push Parent Account Rate Shee to Child Accountt
**/ 
global class GLSQW_AccountRelatedDataLoader {
   public static String returnMessage;   
    
    /**
    *  Method to load all default values of SOW Items, SOW Terms, and Rate Sheets
    **/
    WebService static string LoadDefaultAll(String acctId ){
        if(acctId != null){
            try{
                String rSheetMessage = LoadDefaultRateSheets(acctId);
                String sowTermsMessage = LoadDefaultSOWTerms(acctId);
                String sowItemsMessage = LoadDefaultSOWItems(acctId);
                returnMessage = rSheetMessage+'\n'+sowTermsMessage+'\n'+sowItemsMessage;
            }catch(Exception e){
                system.debug(GLSConfig.ExceptionOccured+e);
                returnMessage = GLSConfig.LoadAllDefaultsFail;
            }
        }else returnMessage = GLSConfig.clientIdNotFound;
    return returnMessage;
    }
    
    /**
    *  Method to copy values of SOW Items, SOW Terms, and Rate Sheets from Parent Account
    **/
    WebService static string CopyAllValues(String acctId, String pAcctId){
        if(acctId != null && pAcctId != null){
            returnMessage = '';
            String sowItemsMessage = '';
            String sowTermsMessage = '';
            String rSheetMessage = '';
            try{
                sowItemsMessage = copySOWItems(acctId,pAcctId);
                sowTermsMessage = copySOWTerms(acctId,pAcctId);
                rSheetMessage = copyRateSheets(acctId,pAcctId);
                returnMessage = rSheetMessage+'\n'+sowTermsMessage+'\n'+sowItemsMessage;
            }catch(Exception e){
                system.debug(GLSConfig.ExceptionOccured+e);
                returnMessage = GLSConfig.CopyFail;
            }
        }else returnMessage = GLSConfig.clientIdNotFound;
    return returnMessage;
    }
    
    /**
    *  Method to Push all SOW Items, SOW Terms, and Rate Sheets to Child accounts
    **/
    WebService static string pushToChildAccounts(String acctId ){
        if(acctId != null){
            List<String> lstPAccId= new List<id>();
            List<Account> lstCAccount = new List<Account>();
            try{
                lstCAccount = [Select Id From Account where ParentId = : acctId];
                if(lstCAccount != null && lstCAccount.size() > 0){
                	for(Account acc: lstCAccount){
                		lstPAccId.add(acc.Id);
                	}
                }
                if(lstPAccId != null && lstPAccId.size() >0){
	                String sowTermsMessage = PushAllTerms(acctId, lstPAccId);
	                String sowItemsMessage = PushAllSOWItems(acctId, lstPAccId);
	                String rSheetMessage   = PushAllRateSheets(acctId, lstPAccId);
	              returnMessage = rSheetMessage+'\n'+sowTermsMessage+'\n'+sowItemsMessage;
                }else returnMessage = GLSConfig.ChildAccountNotFound;
            }catch(Exception e){
                system.debug(GLSConfig.ExceptionOccured+e);
                returnMessage = GLSConfig.pushFail;
            }
        }else returnMessage = GLSConfig.clientIdNotFound;
    	return returnMessage;
    }
    
    /**
    *  Method to Push SOW Items to Child Accounts
    **/
    WebService static String PushAllSOWItems(String acctId, List<String> lstPAccId){
        system.debug('---inside PushAllSOWItems---: ');
        if(acctId != null && lstPAccId != null){
            returnMessage = '';
            Map<String, Set<String>> mapSowItems = new map<String, Set<String>>();
            List<Client_Default_SOW_Items_Table__c> lstSowItems = new List<Client_Default_SOW_Items_Table__c>();
            List<Client_Default_SOW_Items_Table__c> lstCSOWItems = [Select Id, Client_Id__c, Client_Service_Id__c, Order_on_Quote__c, Standard_SOW_Item_Id__c
                                                                    From Client_Default_SOW_Items_Table__c
                                                                    Where Client_Id__c in : lstPAccId];
                                                                    
            List<Client_Default_SOW_Items_Table__c> lstPCSOWItems= [Select Id, Client_Id__c, Client_Service_Id__c, Order_on_Quote__c, Standard_SOW_Item_Id__c
                                                                     From Client_Default_SOW_Items_Table__c
                                                                     Where Client_Id__c =: acctId];    
            
            if(lstCSOWItems != null && lstCSOWItems.size()>0){
                system.debug('---lstPAccId---: '+lstPAccId);
                for(String st : lstPAccId){
                	Set<String> lstTemp = new Set<String>();
                	for(Client_Default_SOW_Items_Table__c cSOWI : lstCSOWItems){
                		if(cSOWI.Client_Id__c == st){
                			lstTemp.add(cSOWI.Standard_SOW_Item_Id__c);
                    		mapSowItems.put(cSOWI.Client_Id__c, lstTemp);
                		}
                	}
                }
                
            }
            if(lstPCSOWItems != null && lstPCSOWItems.size()>0){
                for(Client_Default_SOW_Items_Table__c PCSOWItems: lstPCSOWItems){
                   for(String lstacIds : lstPAccId){
                   		set<string> tempSowItems;
                   		if(mapSowItems.containsKey(string.valueof(lstacIds))){
	                   		tempSowItems =  mapSowItems.get(lstacIds);
                   		} 
	                    if(tempSowItems == null || (tempSowItems != null && !tempSowItems.contains(PCSOWItems.Standard_SOW_Item_Id__c))){
		                        Client_Default_SOW_Items_Table__c cSowItems = new Client_Default_SOW_Items_Table__c();
		                        cSowItems.Client_Id__c = lstacIds;
		                        cSowItems.Standard_SOW_Item_Id__c = PCSOWItems.Standard_SOW_Item_Id__c;
		                        cSowItems.Order_on_Quote__c = PCSOWItems.Order_on_Quote__c;
		                        cSowItems.Client_Service_Id__c = PCSOWItems.Client_Service_Id__c;
		                        lstSowItems.add(cSowItems);
	                    }
                   	 }
                }
             }else return GLSConfig.SOWItemNotFoundCopy; 
            
            if(lstSowItems != null && lstSowItems.size()>0){
                try{
                    insert lstSowItems;
                    returnMessage = GLSConfig.SOWItemPushSuccess;   
                }catch(DMLException de){
                    system.debug(GLSConfig.DMLExceptionOccured+de);
                    returnMessage = GLSConfig.SOWItemPushFail;
                }
            }else returnMessage = GLSConfig.SOWItemCopyExist;                                                        
        }else returnMessage = GLSConfig.clientIdNotFound;
    return returnMessage;
   }
   
   /**
    *  Method to Push Terms to Child Accounts
    **/
    WebService static String PushAllTerms(String acctId, List<String> lstPAccId){
        system.debug('---inside PushAllTerms---: ');
        if(acctId != null && lstPAccId != null){
            returnMessage = '';
            Map<String, Set<String>> mapSowTerms = new map<String, Set<String>>();
            List<Client_Default_Terms_Table__c> lstSowTerms = new List<Client_Default_Terms_Table__c>();
            List<Client_Default_Terms_Table__c> lstCSOWTerms = [Select Id, Client_Id__c, Order_on_Quote__c, Standard_Term_Id__c
                                                                From Client_Default_Terms_Table__c
                                                                Where Client_Id__c in : lstPAccId];
                                                                    
            List<Client_Default_Terms_Table__c> lstPCSOWTerms= [Select Id, Client_Id__c, Order_on_Quote__c, Standard_Term_Id__c
                                                                From Client_Default_Terms_Table__c
                                                                Where Client_Id__c =: acctId];    
            
            if(lstCSOWTerms != null && lstCSOWTerms.size()>0){
                system.debug('---lstPAccId---: '+lstPAccId);
                for(String st : lstPAccId){
                	Set<String> lstTemp = new Set<String>();
                	for(Client_Default_Terms_Table__c cSOWT : lstCSOWTerms){
                		if(cSOWT.Client_Id__c == st){
                			lstTemp.add(cSOWT.Standard_Term_Id__c);
                    		mapSowTerms.put(cSOWT.Client_Id__c, lstTemp);
                		}
                	}
                }
                
            }
            if(lstPCSOWTerms != null && lstPCSOWTerms.size()>0){
                for(Client_Default_Terms_Table__c PCSOWTerms: lstPCSOWTerms){
                   for(String lstacIds : lstPAccId){
                   		set<string> tempSowTerms;
                   		if(mapSowTerms.containsKey(string.valueof(lstacIds))){
	                   		tempSowTerms =  mapSowTerms.get(lstacIds);
                   		} 
	                    if(tempSowTerms == null || (tempSowTerms != null && !tempSowTerms.contains(PCSOWTerms.Standard_Term_Id__c))){
		                        Client_Default_Terms_Table__c cSowTerms = new Client_Default_Terms_Table__c();
		                        cSowTerms.Client_Id__c = lstacIds;
		                        cSowTerms.Standard_Term_Id__c = PCSOWTerms.Standard_Term_Id__c;
		                        cSowTerms.Order_on_Quote__c = PCSOWTerms.Order_on_Quote__c;
		                        lstSowTerms.add(cSowTerms);
	                    }
                   	 }
                }
             }else return GLSConfig.SOWTermsNotFoundCopy; 
            
            system.debug('---lstSowTerms---: '+lstSowTerms);
            if(lstSowTerms != null && lstSowTerms.size()>0){
                try{
                    insert lstSowTerms;
                    returnMessage = GLSConfig.SOWTermPushSuccess;   
                }catch(DMLException de){
                    system.debug(GLSConfig.DMLExceptionOccured+de);
                    returnMessage = GLSConfig.SOWTermPushFail;
                }
            }else returnMessage = GLSConfig.SOWTermsCopyExist;                                                        
        }else returnMessage = GLSConfig.clientIdNotFound;
    return returnMessage;
   }
    
    /**
    *  Method to Push Rate Sheets to Child Accounts
    **/
    WebService static String PushAllRateSheets(String acctId, List<String> lstPAccId){
        system.debug('---inside PushAllRateSheets---: ');
        if(acctId != null && lstPAccId != null){
            returnMessage = '';
            Map<String, Set<String>> mapRSheets = new map<String, Set<String>>();
            List<Account_Rate_Sheet__c> lstRateSheets = new List<Account_Rate_Sheet__c>();
            List<Account_Rate_Sheet__c> lstCRateSheets = [Select Id, Account__c, Rate_Sheet__c, Price_Group_Rates__c
            											  From Account_Rate_Sheet__c
                                                          Where Account__c in : lstPAccId];
                                                                    
            List<Account_Rate_Sheet__c> lstPRateSheets= [Select Id, Account__c, Rate_Sheet__c, Price_Group_Rates__c
            											  		 From Account_Rate_Sheet__c
                                                         		 Where Account__c =: acctId];    
            
            if(lstCRateSheets != null && lstCRateSheets.size()>0){
                for(String st : lstPAccId){
                	Set<String> lstTemp = new Set<String>();
                	for(Account_Rate_Sheet__c cRSheet : lstCRateSheets){
                		if(cRSheet.Account__c == st){
                			lstTemp.add(cRSheet.Rate_Sheet__c);
                    		mapRSheets.put(cRSheet.Account__c, lstTemp);
                		}
                	}
                }
                
            }
            if(lstPRateSheets != null && lstPRateSheets.size()>0){
                for(Account_Rate_Sheet__c PCRSheets: lstPRateSheets){
                   for(String lstacIds : lstPAccId){
                   		set<string> tempRSheets;
                   		if(mapRSheets.containsKey(string.valueof(lstacIds))){
	                   		tempRSheets =  mapRSheets.get(lstacIds);
                   		} 
	                    if(tempRSheets == null || (tempRSheets != null && !tempRSheets.contains(PCRSheets.Rate_Sheet__c))){
		                        Account_Rate_Sheet__c cRSheets = new Account_Rate_Sheet__c();
		                        cRSheets.Account__c = lstacIds;
		                        cRSheets.Rate_Sheet__c = PCRSheets.Rate_Sheet__c;
		                        cRSheets.Price_Group_Rates__c = PCRSheets.Price_Group_Rates__c;
		                        lstRateSheets.add(cRSheets);
	                    }
                   	 }
                }
             }else return GLSConfig.RSheetNotFoundCopy; 
            
            if(lstRateSheets != null && lstRateSheets.size()>0){
                try{
                    insert lstRateSheets;
                    returnMessage = GLSConfig.RSheetPushSuccess;   
                }catch(DMLException de){
                    system.debug(GLSConfig.DMLExceptionOccured+de);
                    returnMessage = GLSConfig.RSheetPushFail;
                }
            }else returnMessage = GLSConfig.RSheetsCopyExist;                                                        
        }else returnMessage = GLSConfig.clientIdNotFound;
    return returnMessage;
   }
    
    /**
    *  Method to copy Parent SOW Items
    **/
    WebService static String copySOWItems(String acctId, String pAcctId){
        system.debug('---inside copySOWItems---: ');
        if(acctId != null && pAcctId != null){
            returnMessage = '';
            Set<String> setSowItems = new Set<String>();
            List<Client_Default_SOW_Items_Table__c> lstSowItems = new List<Client_Default_SOW_Items_Table__c>();
            List<Client_Default_SOW_Items_Table__c> lstCSOWItems = [Select Id, Client_Id__c, Client_Service_Id__c, Order_on_Quote__c, Standard_SOW_Item_Id__c
                                                                    From Client_Default_SOW_Items_Table__c
                                                                    Where Client_Id__c =: acctId];
            List<Client_Default_SOW_Items_Table__c> lstPCSOWItems= [Select Id, Client_Id__c, Client_Service_Id__c, Order_on_Quote__c, Standard_SOW_Item_Id__c
                                                                     From Client_Default_SOW_Items_Table__c
                                                                     Where Client_Id__c =: pAcctId];    
            
            if(lstCSOWItems != null && lstCSOWItems.size()>0){
                for(Client_Default_SOW_Items_Table__c cSOWI : lstCSOWItems){
                    setSowItems.add(cSOWI.Standard_SOW_Item_Id__c);
                }
            }
            if(lstPCSOWItems != null && lstPCSOWItems.size()>0){
                for(Client_Default_SOW_Items_Table__c PCSOWItems: lstPCSOWItems){
                    if(!setSowItems.Contains(PCSOWItems.Standard_SOW_Item_Id__c)){
                        Client_Default_SOW_Items_Table__c cSowItems = new Client_Default_SOW_Items_Table__c();
                        cSowItems.Client_Id__c = acctId;
                        cSowItems.Standard_SOW_Item_Id__c = PCSOWItems.Standard_SOW_Item_Id__c;
                        cSowItems.Order_on_Quote__c = PCSOWItems.Order_on_Quote__c;
                        cSowItems.Client_Service_Id__c = PCSOWItems.Client_Service_Id__c;
                        lstSowItems.add(cSowItems);
                    }
                }
            }else return GLSConfig.SOWItemNotFoundCopy; 
            
            if(lstSowItems != null && lstSowItems.size()>0){
                try{
                    insert lstSowItems;
                    returnMessage = GLSConfig.SOWItemCopySuccess;   
                }catch(DMLException de){
                    system.debug(GLSConfig.DMLExceptionOccured+de);
                    returnMessage = GLSConfig.SOWItemCopyFail;
                }
            }else returnMessage = GLSConfig.SOWItemCopyExist;                                                        
        }else returnMessage = GLSConfig.clientIdNotFound;
    return returnMessage;
   }
   
    /**
    *  Method to copy Parent SOW Terms
    **/
    WebService static String copySOWTerms(String acctId, String pAcctId){
        system.debug('---inside copySOWTerms---: ');
        if(acctId != null && pAcctId != null){
            returnMessage = '';
            Set<String> setSowTerms = new Set<String>();
            List<Client_Default_Terms_Table__c> lstSowTerms = new List<Client_Default_Terms_Table__c>();
            List<Client_Default_Terms_Table__c> lstCSOWTerms = [Select Id, Client_Id__c, Order_on_Quote__c, Standard_Term_Id__c
                                                                From Client_Default_Terms_Table__c
                                                                Where Client_Id__c =: acctId];
            List<Client_Default_Terms_Table__c> lstPCSOWTerms = [Select Id, Client_Id__c, Order_on_Quote__c, Standard_Term_Id__c
                                                                 From Client_Default_Terms_Table__c
                                                                 Where Client_Id__c =: pAcctId];
            if(lstCSOWTerms != null && lstCSOWTerms.size()>0){
                for(Client_Default_Terms_Table__c cSOWT : lstCSOWTerms){
                    setSowTerms.add(cSOWT.Standard_Term_Id__c);
                }
            }
            
            if(lstPCSOWTerms != null && lstPCSOWTerms.size()>0){
                for(Client_Default_Terms_Table__c PCSOWTerms: lstPCSOWTerms){
                    if(!setSowTerms.Contains(PCSOWTerms.Standard_Term_Id__c)){
                        Client_Default_Terms_Table__c cSowTerms = new Client_Default_Terms_Table__c();
                        cSowTerms.Client_Id__c = acctId;
                        cSowTerms.Standard_Term_Id__c = PCSOWTerms.Standard_Term_Id__c;
                        cSowTerms.Order_on_Quote__c = PCSOWTerms.Order_on_Quote__c;
                        lstSowTerms.add(cSowTerms);
                    }
                }
            }else return GLSConfig.SOWTermsNotFoundCopy;    
            
            if(lstSowTerms != null && lstSowTerms.size()>0){
                try{
                    insert lstSowTerms;
                    returnMessage = GLSConfig.SOWTermCopySuccess;
                }catch(DMLException de){
                    system.debug(GLSConfig.DMLExceptionOccured+de);
                    returnMessage = GLSConfig.SOWTermCopyFail;
                }
            }else returnMessage = GLSConfig.SOWTermsCopyExist;                                                               
        }else returnMessage = GLSConfig.clientIdNotFound;
    return returnMessage;
   }
   
   /**
    *  Method to copy Parent Rate Sheet
    **/
    WebService static String copyRateSheets(String acctId, String pAcctId){
        system.debug('---inside copyRateSheets---: ');
        if(acctId != null && pAcctId != null){
            returnMessage = '';
            Set<String> setRS = new Set<String>();
            List<Account_Rate_Sheet__c> lstcRateSheet = new List<Account_Rate_Sheet__c>();
            List<Account_Rate_Sheet__c> lstCRSheets = [Select Id, Account__c, Rate_Sheet__c, Price_Group_Rates__c From Account_Rate_Sheet__c Where Account__c =: acctId];   
            List<Account_Rate_Sheet__c> lstPRSheets = [Select Id, Account__c, Rate_Sheet__c, Price_Group_Rates__c From Account_Rate_Sheet__c Where Account__c =: pAcctId];  
            
            if(lstCRSheets != null && lstCRSheets.size()>0){
                for(Account_Rate_Sheet__c RS : lstCRSheets){
                    setRS.add(RS.Rate_Sheet__c);
                }
            }
            if(lstPRSheets != null && lstPRSheets.size()>0){
                for(Account_Rate_Sheet__c PCRSheets: lstPRSheets){
                    if(!setRS.Contains(PCRSheets.Rate_Sheet__c)){
                        Account_Rate_Sheet__c aRSheets = new Account_Rate_Sheet__c();
                        aRSheets.Account__c = acctId;
                        aRSheets.Rate_Sheet__c = PCRSheets.Rate_Sheet__c;
                        aRSheets.Price_Group_Rates__c = PCRSheets.Price_Group_Rates__c;
                        lstcRateSheet.add(aRSheets);
                    }
                }
            }else return GLSConfig.RSheetNotFoundCopy;
            
            if(lstcRateSheet != null && lstcRateSheet.size()>0){
                try{
                    insert lstcRateSheet;
                    returnMessage = GLSConfig.RSheetCopySuccess;    
                }catch(DMLException de){
                    system.debug(GLSConfig.DMLExceptionOccured+de);
                    returnMessage = GLSConfig.RSheetCopyFail;
                }
            }else returnMessage = GLSConfig.RSheetsCopyExist;                                                                
        }else returnMessage = GLSConfig.clientIdNotFound;
    return returnMessage;
   }
    
    /**
    *  Method to load default SOW Items
    **/
    WebService static string LoadDefaultSOWItems(String acctId ){
        if(acctId != null){
            List<Client_Default_SOW_Items_Table__c> lstSowItems = new List<Client_Default_SOW_Items_Table__c>();
            Set<String> setSowItems = new Set<String>();
            
            List<Standard_SOW_Items_Table__c> lstStdSOWItems = [Select Id, Name, Default__c, Default_Order__c From Standard_SOW_Items_Table__c Where Default__c =: true];
            if(lstStdSOWItems != null && lstStdSOWItems.size()>0){  
                List<Client_Default_SOW_Items_Table__c> lstClientSOWItems = [Select Id, Client_Id__c, Standard_SOW_Item_Id__c, Order_on_Quote__c
                                                                             From Client_Default_SOW_Items_Table__c
                                                                             Where Client_Id__c =: acctId];                                                      
                if(lstClientSOWItems != null && lstClientSOWItems.size()>0){
                    for(Client_Default_SOW_Items_Table__c cSOWI : lstClientSOWItems){
                        setSowItems.add(cSOWI.Standard_SOW_Item_Id__c);
                    }
                }
                for(Standard_SOW_Items_Table__c stdSOWItems : lstStdSOWItems){
                    if(!setSowItems.Contains(stdSOWItems.Id)){
                        Client_Default_SOW_Items_Table__c cSowItems = new Client_Default_SOW_Items_Table__c();
                        cSowItems.Client_Id__c = acctId;
                        cSowItems.Standard_SOW_Item_Id__c = stdSOWItems.Id;
                        cSowItems.Order_on_Quote__c = stdSOWItems.Default_Order__c;
                        lstSowItems.add(cSowItems);
                    }
                }
                if(lstSowItems != null && lstSowItems.size()>0){
                    try{
                        insert lstSowItems;
                        returnMessage = GLSConfig.SOWItemSuccess;   
                    }catch(DMLException de){
                        system.debug(GLSConfig.DMLExceptionOccured+de);
                        returnMessage = GLSConfig.SOWItemFail;
                    }
                }else returnMessage = GLSConfig.SOWItemExist;   
            }else returnMessage = GLSConfig.SOWItemNotFound;
        }else returnMessage = GLSConfig.clientIdNotFound;
    return returnMessage;
  }
    
    /**
    *  Method to load default Rate Sheets
    **/
    WebService static string LoadDefaultRateSheets(String acctId ){
        if(acctId != null){
            List<Account_Rate_Sheet__c> lstcRateSheet = new List<Account_Rate_Sheet__c>();
            Set<String> setRSheets = new Set<String>();
            
            List<Rate_Sheet__c> lstStdRSheets = [Select Id, Name, Default__c From Rate_Sheet__c Where Default__c =: true];
            
            if(lstStdRSheets != null && lstStdRSheets.size()>0){    
                List<Account_Rate_Sheet__c> lstAccRSheets = [Select Id, Account__c, Rate_Sheet__c From Account_Rate_Sheet__c Where Account__c =: acctId];                                                        
                
                if(lstAccRSheets != null && lstAccRSheets.size()>0){
                    for(Account_Rate_Sheet__c aRS : lstAccRSheets){
                        setRSheets.add(aRS.Rate_Sheet__c);
                    }
                }
                for(Rate_Sheet__c RS : lstStdRSheets){
                    if(!setRSheets.Contains(RS.Id)){
                        Account_Rate_Sheet__c aRSheets = new Account_Rate_Sheet__c();
                        aRSheets.Account__c = acctId;
                        aRSheets.Rate_Sheet__c = RS.Id;
                        lstcRateSheet.add(aRSheets);
                    }
                }
                if(lstcRateSheet != null && lstcRateSheet.size()>0){
                    try{
                        insert lstcRateSheet;
                        returnMessage = GLSConfig.RSheetSuccess;
                    }catch(DMLException de){
                        system.debug(GLSConfig.DMLExceptionOccured+de);
                        returnMessage = GLSConfig.RSheetFail;
                    }
                }else returnMessage = GLSConfig.RSheetsExist;   
            }else returnMessage = GLSConfig.RSheetNotFound;
        }else returnMessage = GLSConfig.clientIdNotFound;
    return returnMessage;
  }

     /**
    *  Method to load default SOW Terms
    **/
     WebService static string LoadDefaultSOWTerms(String acctId ){
        if(acctId != null){
            List<Client_Default_Terms_Table__c> lstSowTerms = new List<Client_Default_Terms_Table__c>();
            Set<String> setSowTerms = new Set<String>();
            
            List<Standard_Terms_Table__c> lstStdSOWTerms = [Select Id, Name, Default__c, Default_Order__c From Standard_Terms_Table__c Where Default__c =: true];
            if(lstStdSOWTerms != null && lstStdSOWTerms.size()>0){  
                List<Client_Default_Terms_Table__c> lstClientSOWTerms = [Select Id, Client_Id__c, Standard_Term_Id__c, Order_on_Quote__c
                                                                             From Client_Default_Terms_Table__c
                                                                             Where Client_Id__c =: acctId];                                                      
                if(lstClientSOWTerms != null && lstClientSOWTerms.size()>0){
                    for(Client_Default_Terms_Table__c cSOWT : lstClientSOWTerms){
                        setSowTerms.add(cSOWT.Standard_Term_Id__c);
                    }
                }
                for(Standard_Terms_Table__c stdSOWTerms : lstStdSOWTerms){
                    if(!setSowTerms.Contains(stdSOWTerms.Id)){
                        Client_Default_Terms_Table__c cSowTerms = new Client_Default_Terms_Table__c();
                        cSowTerms.Client_Id__c = acctId;
                        cSowTerms.Standard_Term_Id__c = stdSOWTerms.Id;
                        cSowTerms.Order_on_Quote__c = stdSOWTerms.Default_Order__c;
                        lstSowTerms.add(cSowTerms);
                    }
                }
                if(lstSowTerms != null && lstSowTerms.size()>0){
                    try{
                        insert lstSowTerms;
                        returnMessage = GLSConfig.SOWTermsSuccess;  
                    }catch(DMLException de){
                        system.debug(GLSConfig.DMLExceptionOccured+de);
                        returnMessage = GLSConfig.SOWTermsFail;
                    }
                }else returnMessage = GLSConfig.SOWTermsExist;  
            }else returnMessage = GLSConfig.SOWTermsNotFound;
        }else returnMessage = GLSConfig.clientIdNotFound;
    return returnMessage;
  }
  
  
  public static testMethod void loadDefaultAllTest() {
        GLSSetupData.DefaultLoaderSetupData();
        Test.startTest();
            GLSQW_AccountRelatedDataLoader.LoadDefaultAll(GLSSetupData.accountObjDL.Id);
        Test.stopTest();
    }
    
    public static testMethod void CopyAllValuesTest() {
        GLSSetupData.DefaultLoaderSetupData();
        Test.startTest();
            GLSQW_AccountRelatedDataLoader.CopyAllValues(GLSSetupData.accountObjDL.Id, GLSSetupData.pAccountObjDL.Id);
        Test.stopTest();
    }    
    public static testMethod void PushAllValuesTest() {
        GLSSetupData.DefaultLoaderSetupData();
        Test.startTest();
            GLSQW_AccountRelatedDataLoader.pushToChildAccounts(GLSSetupData.accountObjDL.Id);
        Test.stopTest();
    }
}