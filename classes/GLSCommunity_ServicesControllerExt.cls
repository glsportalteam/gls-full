/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSCommunity_ServicesControllerExt Class
* Function: This class is used as a controller extension for the GLSCommunity_ServicesPage
*/

public class GLSCommunity_ServicesControllerExt {
    
    //Variable declaration for the class
    public List<ServicesWrapper> lstServicesWrapperCommon       {get; set;}
    public List<ServicesWrapper> lstServicesWrapperOthers       {get; set;}
    public Id qaId                                              {get; set;}
    public Id qId                                               {get; set;}
    public Integer noOfServices                                 {get; set;}
    public Id contactId                                         {get; set;}
    public String projectName                                   {get; set;}
    public Boolean newTranslation                               {get; set;}
    
    private List<Quote_Assignment_Service__c> lstQAS;
    private Quote_Assignment__c qa;
    private Quote__c quoteobj;
    
    //Constructor
    public GLSCommunity_ServicesControllerExt(){
        
        qaId = ApexPages.currentPage().getParameters().get('qaId');
        qId = ApexPages.currentPage().getParameters().get('qId');
        contactId = ApexPages.currentPage().getParameters().get('conId');
        
        lstQAS = new List<Quote_Assignment_Service__c>();
        qa = new Quote_Assignment__c();
        newTranslation = false;
        lstServicesWrapperCommon = new List<ServicesWrapper>();
        lstServicesWrapperOthers = new List<ServicesWrapper>();
        projectName = '';
        quoteobj = new Quote__c();
        
        getAllServices();
        if(lstQAS != null && lstQAS.size() > 0){
            noOfServices = lstQAS.size();
        }
    }
    
    /* 
    @Description: Method to get the list of all services and populate the respective lists to be displayed on page.
    @Params: None
    @Return Type: None
    */
    private void getAllServices(){
        if(qaId != null){
            qa = [SELECT Translation_Update__c
                    FROM Quote_Assignment__c
                    WHERE Id =: qaId LIMIT 1];
        }
        if(qa != null){
            newTranslation = qa.Translation_Update__c ? false : true;
        }
        
        if(qId != null){
            quoteobj = [SELECT Id, Project_Name_Community__c
                        FROM Quote__c
                        WHERE Id =: qId LIMIT 1];
        
            if(quoteobj != null){
                projectName = quoteobj.Project_Name_Community__c;
            }
        }
        
        lstQAS = [SELECT Id, Client_Service__c
                   FROM Quote_Assignment_Service__c
                   WHERE Quote_Assignment__c =: qaId];
        Set<Id> existingServices = new Set<Id>();
        if(lstQAS != null && lstQAS.size() > 0){
            for(Quote_Assignment_Service__c qas : lstQAS){
                existingServices.add(qas.Client_Service__c);
            }
        }
        List<Client_Service__c> lstClientService = [SELECT Is_Common__c, Name, Description__c, Id
                                                    FROM Client_Service__c
                                                    ORDER BY Order__c];
            
        if(lstClientService != null && lstClientService.size() > 0){
            for(Client_Service__c cs : lstClientService){
                if(cs.Is_Common__c){
                    if(existingServices.contains(cs.Id)){
                        if(cs.Name == 'Translation' && newTranslation){
                            lstServicesWrapperCommon.add(new ServicesWrapper(cs, true));
                        }
                        else if(cs.Name == 'Translation Update' && !newTranslation){
                            lstServicesWrapperCommon.add(new ServicesWrapper(cs, true));
                        }
                        else{
                            lstServicesWrapperCommon.add(new ServicesWrapper(cs, true));
                        }
                    }
                    else{
                        if(cs.Name == 'Translation' && newTranslation){
                            lstServicesWrapperCommon.add(new ServicesWrapper(cs, true));
                        }
                        else if(cs.Name == 'Translation Update' && !newTranslation){
                            lstServicesWrapperCommon.add(new ServicesWrapper(cs, true));
                        }
                        else{
                            lstServicesWrapperCommon.add(new ServicesWrapper(cs, false));
                        }
                    }
                }
                else{
                    if(existingServices.contains(cs.Id)){
                        lstServicesWrapperOthers.add(new ServicesWrapper(cs, true));
                    }
                    else{
                        lstServicesWrapperOthers.add(new ServicesWrapper(cs, false));
                    }
                }
            }
        }
    }
    
    /* 
    @Description: Method called when "Save" is clicked from the Services page.
    @Params: None
    @Return Type: PageReference
    */
    public void saveServices(){
        List<Quote_Assignment_Service__c> lstQuoteAssignmentService = new List<Quote_Assignment_Service__c>();
        Quote_Assignment_Service__c qasObj = new Quote_Assignment_Service__c();
        try{
            if(lstQAS != null && lstQAS.size() > 0){
                delete lstQAS;
            }
            lstServicesWrapperCommon.addAll(lstServicesWrapperOthers);
            
            if(lstServicesWrapperCommon != null && lstServicesWrapperCommon.size() > 0){
                for(ServicesWrapper service : lstServicesWrapperCommon){
                    if(service.isChecked){
                        qasObj = new Quote_Assignment_Service__c();
                        qasObj.Quote_Assignment__c = qaId;
                        qasObj.Client_Service__c = service.clientService.Id;
                        qasObj.Service_Name__c = service.clientService.Name;
                        lstQuoteAssignmentService.add(qasObj);
                    }
                }
            }
            if(lstQuoteAssignmentService != null && lstQuoteAssignmentService.size() > 0){
                insert lstQuoteAssignmentService;
            }
            
            Quote__c quoteObj = new Quote__c(Id=qId);
            quoteObj.Last_Visted_Stage__c = 'GLSCommunity_PreviewPage';
            quoteObj.Last_Visited_Assignment_Id__c = qaId;
            update quoteObj;
            
        }
        catch(DMLException de){
            system.debug(GLSConfig.DMLExceptionOccured + de);
        }
        catch(Exception e){
            system.debug(GLSConfig.ExceptionOccured + e);
        }
        noOfServices = lstQuoteAssignmentService.size();
    }
    
    /* 
    @Description: Method to redirect to Preview Page
    @Params: None
    @Return Type: PageReference
    */
    public PageReference redirectToPreviewPage(){
        PageReference pageref = Page.GLSCommunity_PreviewPage;
        pageref.getParameters().put('qaId',qaId);
        pageref.getParameters().put('qid',qId);
        if(contactId != null){
            pageref.getParameters().put('conId',contactId);
        }
        pageref.setRedirect(true);
        return pageref;         
    }
    
    /* 
    @Description: Method to redirect to Language Page
    @Params: None
    @Return Type: PageReference
    */
    public PageReference redirectToLanguagePage(){
        PageReference pageref = Page.GLSCommunity_LanguagePage;
        pageref.getParameters().put('qaId',qaId);
        pageref.getParameters().put('qid',qId);
        if(contactId != null){
            pageref.getParameters().put('conId',contactId);
        }
        pageref.setRedirect(true);
        return pageref;         
    }
    
    //Wrapper for getting the selected services from the page.
    public class ServicesWrapper{
        public Client_Service__c clientService          {get; set;}
        public Boolean isChecked                        {get; set;}
        public ServicesWrapper(Client_Service__c clService, Boolean isChecked){
            this.clientService = clService;
            this.isChecked = isChecked;
        }
    }
}