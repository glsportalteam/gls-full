/** 
* This global class is used for -
* 1. Call AnalyzeFile API
**/ 

global class GLSAnalyzeFileAPICall {
	public static String returnMessage;   
	public static List<Trados_Configuration__c> lstTradosConf = new List<Trados_Configuration__c>(); 
	

	/**
    *  Method to call analyze file API for all the quote line items of a quote.
    **/
	WebService static string analyzeQuoteLineItems(String QuoteID ){
	        String sourceLanguage = '';
	        String targetLanguage = '';
	        String filesId = '';
	        String bucketName = '';
	        String translationMemory = '';
	        string FileMissingErrorMsg='';
	        string AnalyzeIdErrorMsg='';
	        string TMMissingErrorMsg='';
	        string qname = '';
	        Integer counter;
	        lstTradosConf = Trados_Configuration__c.getall().values();
	        system.debug('---lstTradosConf---: '+lstTradosConf);
        	If(lstTradosConf != null && lstTradosConf.size()>0){
        		system.debug('---inside if---');
		        List<Quote_Line_Item__c> lstQLI = [Select Id,Name,Analyzed_Transaction_ID__c,QuoteID__c,Trados_Language_Pair__c,isProcessed__c, Source_Language_ID__c,
		        										  QuoteID__r.Bucket_Name__c, Source_Language_ID__r.Language_Name__c,Target_Language_ID__r.Language_Name__c, 
		        										  Target_Language_ID__c,isReadyForAnalyse__c,isAnalyzed__c,Translation_Memory_Options_Flag__c, isTMFound__c,Client_TM__c,Client_TM__r.TM_Filename__c, Client_TM__r.TM_Name__c,
		                                          		 (Select Id, Source_File_Name__r.File_Name__c,Source_File_Name__r.TradosAPIFileID__c From Quote_Line_Object_Assignment_Files__r) 
		                                          From Quote_Line_Item__c 
		                                          Where QuoteID__c =: QuoteID AND 
		                                          		isAnalyzed__c = false AND
		                                          		isReadyForAnalyse__c = true order by Name] ;/*AND
		                                          		(Client_TM__c != null OR isTMFound__c = false)];*/
		        
		        List<Quote_Line_Item__c> lstQupdateQLI = new List<Quote_Line_Item__c>();
		        system.debug('---lstQLI---: '+lstQLI);
		        if(lstQLI != null && lstQLI.size()>0){
		            for(Quote_Line_Item__c qli : lstQLI){
			            	
			            	if((qli.Client_TM__c == null && (qli.isTMFound__c)) || (qli.Client_TM__c == null && (qli.Translation_Memory_Options_Flag__c))){
			            		if(TMMissingErrorMsg ==''){
			                   		TMMissingErrorMsg = GLSConfig.TMFileNotAdded+qli.Trados_Language_Pair__c;
			                   		if(qname == '')
			                   			qname=qli.Trados_Language_Pair__c;
			                   		else
			                   			qname +=','+qli.Trados_Language_Pair__c;
			                    }else{
			                   		TMMissingErrorMsg +=','+qli.Trados_Language_Pair__c;
			                   		qname +=','+qli.Trados_Language_Pair__c;
			                   	}
				             }
		           }	
		           if(TMMissingErrorMsg ==''){
			           counter =0;
			           for(Quote_Line_Item__c qli : lstQLI){
			                //if((qli.Client_TM__c != null || (!qli.isTMFound__c)) || (qli.Client_TM__c != null && (qli.Translation_Memory_Options_Flag__c)))
		            		if(counter <= 9){    
				                bucketName =  qli.QuoteID__r.Bucket_Name__c;  
				                sourceLanguage = qli.Source_Language_ID__r.Language_Name__c;
				                targetLanguage = qli.Target_Language_ID__r.Language_Name__c;  
				                for(Quote_Line_Object_Assignment_File__c QLIFiles : qli.Quote_Line_Object_Assignment_Files__r){
				                    if(filesId != ''){
				                        filesId = filesId+','+QLIFiles.Source_File_Name__r.TradosAPIFileID__c;
				                    }else{
				                        filesId = QLIFiles.Source_File_Name__r.TradosAPIFileID__c;
				                    }    
				                } 
				                if(qli.Client_TM__r.TM_Name__c != null && qli.Client_TM__r.TM_Name__c !=''){
				                    translationMemory = qli.Client_TM__r.TM_Name__c;
				                }else{
				                   /**
				                   * Below Items to be moved to GLSConifg.cls
				                   */
				                   translationMemory = '//glsnas/operations/Projects/L10N/Amgen/TMs/Empty/empty tm.sdltm';
				                    sourceLanguage = 'en-US';
				                    targetLanguage =  'en-GB';
				                }
				               system.debug('---filesId--- : '+filesId); 
				               if(filesId != null && filesId !=''){
				                   String analyzeTransactionId = callAnalyzeFileAPI(filesId,sourceLanguage,targetLanguage,qli.QuoteID__c,bucketName, translationMemory);                
				                   filesId = '';
				                   analyzeTransactionId= analyzeTransactionId.trim();
				                   system.debug('---analyzeTransactionId--- : '+analyzeTransactionId);
				                   if(analyzeTransactionId != null && analyzeTransactionId != ''){
				                   	    system.debug('---inside if---');
				                       qli.Analyzed_Transaction_ID__c = analyzeTransactionId;
				                       qli.isReadyForAnalyse__c= false;
				                       qli.isAnalyzed__c = true;
				                       lstQupdateQLI.add(qli);
				                   }else{
				                    if(AnalyzeIdErrorMsg =='')
				                    	AnalyzeIdErrorMsg = GLSConfig.ResponseNotRecieved+' for '+qli.Trados_Language_Pair__c;
				                    else
				                    	AnalyzeIdErrorMsg +=','+qli.Trados_Language_Pair__c;	
				                  }    
				               }else{
				                   if(FileMissingErrorMsg ==''){
				                   		FileMissingErrorMsg =GLSConfig.QLIFileTradosIdNotFound+qli.Trados_Language_Pair__c;
				                   		if(qname == '')
			                   				qname=qli.Trados_Language_Pair__c;
			                   			else
			                   				qname +=','+qli.Trados_Language_Pair__c;
				                   }else{
				                   		FileMissingErrorMsg +=','+qli.Trados_Language_Pair__c;
				                   		qname +=','+qli.Trados_Language_Pair__c;
				                   }
				                   system.debug('---qname---:' +qname);
				               } 
				               counter++; 
			           		}
			           		else{
			           			break;
			           		}
			            }
			            if(lstQupdateQLI != null && lstQupdateQLI.size()>0){
			                try{
			                	Database.update(lstQupdateQLI);
			                	returnMessage = GLSconfig.analyzeComplete;
			                }catch(DMLException de){
			                	system.debug(GLSConfig.DMLExceptionOccured);
			                	returnMessage = GLSconfig.analyzeFail+' - '+GLSConfig.DMLExceptionOccured;
			                }catch(Exception e){
			                	system.debug(GLSConfig.ExceptionOccured);
			                	returnMessage = GLSconfig.analyzeFail+' - '+GLSConfig.DMLExceptionOccured;
			                }
			            }
				      /*   if(counter == lstQLI.size()){		
			           		Quote__c qObj = new Quote__c(Id=QuoteID);
			           		qObj.IsAnalysisComplete__c = true;
			           		database.update(qObj);
			          	 }*/
			        }else{
				          Quote__c qObj = new Quote__c(Id=QuoteID);
			        	  try{
			        	  	qObj.IsAnalysisComplete__c = true;
			        	  	qObj.Pushback_Status__c = 'QLI Analyzed';
			        	  	database.update(qObj);
			        	  }catch(DMLException de){
			        	  	system.debug(GLSConfig.DMLExceptionOccured);
			        	  }catch(Exception e){
			        	  	system.debug(GLSConfig.ExceptionOccured);
			        	  }
			        	  return  TMMissingErrorMsg;
		           		}
		          if(counter == lstQLI.size()){		
		           		Quote__c qObj = new Quote__c(Id=QuoteID);
		           		qObj.IsAnalysisComplete__c = true;
		           		database.update(qObj);
		           }
		        }else{
		        	  Quote__c qObj = new Quote__c(Id=QuoteID);
		        	  try{
		        	  	qObj.IsAnalysisComplete__c = true;
		        	  	qObj.Pushback_Status__c = 'QLI Analyzed';
		        	  	database.update(qObj);
		        	  }catch(DMLException de){
		        	  	system.debug(GLSConfig.DMLExceptionOccured);
		        	  }catch(Exception e){
		        	  	system.debug(GLSConfig.ExceptionOccured);
		        	  }
		        	  returnMessage = GLSConfig.NoQLICreatedforAnalyze;
		              return  returnMessage;
		        	}
	        if((FileMissingErrorMsg == '') && (TMMissingErrorMsg == '') && (AnalyzeIdErrorMsg =='')){
	            returnMessage = GLSconfig.analyzeComplete;
	        }else{
	            returnMessage = GLSconfig.analyzeFail+' for '+qname+'\n\n'+FileMissingErrorMsg+'\n'+TMMissingErrorMsg +'\n'+AnalyzeIdErrorMsg;
        	}
        	system.debug('-----returnMessage----------'+returnMessage);
        	}else returnMessage = GLSConfig.NoTradosConfigurationSetup;
        	
	     return returnMessage;
	   }
	    /** 
	    * Method to call Trados Analyze API
	    **/
	    
	    private static String callAnalyzeFileAPI(String filesId, String sourceLanguage, String targetLanguage,ID QuoteID, String bucketName, String translationMemory){
			system.debug('---Inside call analyse file api--' );
	        translationMemory = EncodingUtil.urlEncode(translationMemory, 'UTF-8');
	        String secure ='';
                if(lstTradosConf[0].isSecure__c)
                	secure = 'https://';
                else
                	secure = 'http://';
            Integer UserId = Integer.valueOf(lstTradosConf[0].User_Id__c);
	        String endPointURL = secure+lstTradosConf[0].DNS_Name__c+GLSconfig.analyzeFileAPICall+'?'+ GLSconfig.paramSecretKey + lstTradosConf[0].Secret_Key__c + 
	        						GLSconfig.paramUserId + UserId + GLSconfig.paramFilesId + filesId + GLSconfig.paramtranslationMemory + translationMemory + 
	        						GLSconfig.paramSourceLanguage + sourceLanguage + GLSconfig.paramTargetLanguage + targetLanguage + GLSconfig.paramAPIName+ lstTradosConf[0].API_Name__c +
	        						GLSconfig.paramIPAddress + lstTradosConf[0].IP_Address__c + GLSconfig.paramReportCrossFileRepetitions+ lstTradosConf[0].Cross_File_Repetitions__c +
	        						GLSconfig.paramReportInternalFuzzyMatchLeverage + lstTradosConf[0].Internal_Fuzzy_Match_Leverage__c + GLSconfig.paramOutputFormat + GLSconfig.outputformat +
	        						GLSconfig.paramBucketName + bucketName + GLSconfig.paramQuoteId + QuoteID;  //String Value
	        system.debug('---endPointURL---:' +endPointURL);
	        HttpResponse resL;
	         if(!Test.isRunningTest()){
	            resL = getResponse(endPointURL);
	         }
	         else{
	            resL = new HttpResponse();
	            resL.setBody('<int xmlns="http://schemas.microsoft.com/2003/10/Serialization/">1856</int>');
	            resL.setStatusCode(200);
	            resL.setStatus('OK');
	         }
	        String transactionId = '';
	        system.debug('---resL get body--' +resL.getBody());
            system.debug('---resL getStatus()--' +resL.getStatus());
	        if(resL != null){
	        	system.debug('---resL get body--' +resL.getBody());
            	system.debug('---resL getStatus()--' +resL.getStatus());
	            String response = resL.getBody() ;          
	            transactionId = response.substringBetween('>','<');
	        }else returnMessage = GLSConfig.ResponseNotRecieved;
	       system.debug('---transactionId--' +transactionId);
	       return transactionId;
	    }
	    
	    /**
	    *  Calling AnalyzeFile method for each Quote Line Item individually
	    **/
	    WebService static string analyzeFile(String QLIId){
	        string errorMsg='';
	        String sourceLanguage = '';
	        String targetLanguage = '';
	        String filesId = '';
	        String bucketName = '';
	        String translationMemory='';
	        List<Quote_Line_Item__c> quoteLI = new List<Quote_Line_Item__c>();
	        
	        lstTradosConf = Trados_Configuration__c.getall().values();
        	If(lstTradosConf != null && lstTradosConf.size()>0){
		        quoteLI = [Select Id, Name, Analyzed_Transaction_ID__c, QuoteID__c, Trados_Language_Pair__c, isProcessed__c, Source_Language_ID__c,
		        				 QuoteID__r.Bucket_Name__c, Source_Language_ID__r.Language_Name__c,Target_Language_ID__r.Language_Name__c, Target_Language_ID__c,
		        				 isAnalyzed__c,isTMFound__c,Client_TM__r.TM_Filename__c, Client_TM__r.TM_Name__c,
		                         (Select Id, Source_File_Name__r.File_Name__c, Source_File_Name__r.TradosAPIFileID__c From Quote_Line_Object_Assignment_Files__r) 
		                  From Quote_Line_Item__c 
		                  Where Id =: QLIId And
		                  		isAnalyzed__c = false AND
		                        isReadyForAnalyse__c = true limit 1];/*AND
		                        (Client_TM__c != null OR isTMFound__c = false)
		                  		limit 1];*/
		        system.debug('---quoteLI---' +quoteLI);
		        if(quoteLI != null && quoteLI.size()>0){
		        	if((quoteLI[0].Client_TM__c != null || (!quoteLI[0].isTMFound__c)) || (quoteLI[0].Client_TM__c != null && (quoteLI[0].Translation_Memory_Options_Flag__c))){
			            bucketName =  quoteLI[0].QuoteID__r.Bucket_Name__c;  
			            sourceLanguage = quoteLI[0].Source_Language_ID__r.Language_Name__c;
			            targetLanguage = quoteLI[0].Target_Language_ID__r.Language_Name__c;    
		             
			            for(Quote_Line_Object_Assignment_File__c QLIFiles : quoteLI[0].Quote_Line_Object_Assignment_Files__r){
			                if(filesId != ''){
			                    filesId = filesId+','+QLIFiles.Source_File_Name__r.TradosAPIFileID__c;
			                }else{
			                    filesId = QLIFiles.Source_File_Name__r.TradosAPIFileID__c;
			                }    
			            }  
		            	system.debug('---sourceLanguage---' +sourceLanguage);
		            	system.debug('---targetLanguage---' +targetLanguage);
		            	system.debug('---filesId---' +filesId);
		            	system.debug('---quoteLI[0].Client_TM__r.TM_Name__c---' +quoteLI[0].Client_TM__r.TM_Name__c);
		            	if(quoteLI[0].Client_TM__r.TM_Name__c != null && quoteLI[0].Client_TM__r.TM_Name__c !=''){
		                    translationMemory = quoteLI[0].Client_TM__r.TM_Name__c;
		                }else{
		                   /**
		                   * Below Items to be moved to GLSConifg.cls
		                   */
		                   translationMemory = '//glsnas/operations/Projects/L10N/Amgen/TMs/Empty/empty tm.sdltm';
		                    sourceLanguage = 'en-US';
		                    targetLanguage =  'en-GB';
		                }
		                system.debug('---translationMemory---' +translationMemory);
		            	if(filesId != null && filesId !=''){    
				            String analyzeTransactionId = callAnalyzeFileAPI(filesId,sourceLanguage,targetLanguage,quoteLI[0].QuoteID__c,bucketName, translationMemory);                
				            analyzeTransactionId = analyzeTransactionId.trim();
				            filesId = '';
				            analyzeTransactionId= analyzeTransactionId.trim();
				            system.debug('---analyzeTransactionId---' +analyzeTransactionId);
				            if(analyzeTransactionId != '' && analyzeTransactionId != null){
				                quoteLI[0].Analyzed_Transaction_ID__c = analyzeTransactionId;
				                quoteLI[0].isReadyForAnalyse__c= false;
				                quoteLI[0].isAnalyzed__c = true;
				                try{
				                	update quoteLI;
				                	returnMessage = GLSconfig.analyzeComplete;
				                }catch(DMLException de){
				                	system.debug(GLSConfig.DMLExceptionOccured);
				                	returnMessage = GLSconfig.analyzeFail+' - '+GLSConfig.DMLExceptionOccured;
				                }catch(Exception e){
				                	system.debug(GLSConfig.ExceptionOccured);
				                	returnMessage = GLSconfig.analyzeFail+' - '+GLSConfig.DMLExceptionOccured;
				                }
				            }else{
			                	returnMessage = GLSconfig.analyzeFail+' - '+GLSConfig.ResponseNotRecieved;
			            	} 
			         	}else{
		                   returnMessage = GLSconfig.analyzeFail+' - '+GLSConfig.QLIFileTradosIdNotFound;
		             	} 
		        	} else returnMessage = GLSConfig.TMFileNotAdded+quoteLI[0].Name;         
		        }else{
		            returnMessage = GLSConfig.NoQLICreatedforAnalyze;
		        }
        	}else returnMessage = GLSConfig.NoTradosConfigurationSetup;
	      system.debug('---returnMessage---' +returnMessage);
	      return returnMessage; 
	    }
	      
	    /*Method for getting response of any API Call. */
	    
	    public static HttpResponse getResponse(String url){
	        try{
	          //  if(!Test.isRunningTest()){
	                Http hL = new Http();
	                HttpRequest reqL = new HttpRequest();
	                reqL.setTimeout(20000);  
	                reqL.setEndpoint(url);  //String Value
	                reqL.setMethod('GET');
	                reqL.setCompressed(true);   
	                HttpResponse resL = hL.send(reqL);
	                return resL;
	          /*  }else{
	                HttpResponse resL = new HttpResponse();
	                resL.setBody('<ExternalFiles xmlns="http://schemas.datacontract.org/2004/07/TradosWcfService" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><Ids><FileIds><dbId>3237</dbId><glsId>a0kZ0000001iN0VIAU</glsId></FileIds><FileIds><dbId>3238</dbId><glsId>a0kZ0000001iN0aIAE</glsId></FileIds></Ids><UploadTransId>f8196661-7a47-4e31-9b80-89276451689b</UploadTransId></ExternalFiles>');
	                resL.setStatusCode(200);
	                resL.setStatus('OK');
	                return resL;
	            }*/
	        }
	        catch(Exception e){
	            system.debug('---Exception in getResponse Method---'+e)         ;
	            return null;
	        }
	    }  
	    
	    
	    /**
	    *  Method to call tm export API for all the quote line items of a quote. TBD
	    ** /  
	    
	    WebService static String tmExportQuoteLineItems(ID QuoteID ){
	        List<Quote_Line_Item__c> lstQuoteLI = new List<Quote_Line_Item__c>();
	        if(QuoteID != '' || QuoteID != null){
	            lstQuoteLI = [Select Id, Source_Language_ID__c, Target_Language_ID__c,Source_Language_ID__r.Language_Name__c,Target_Language_ID__r.Language_Name__c,
	                          (Select Id, Source_File_Name__r.Name , Source_File_Name__r.TradosAPIFileID__c From Quote_Line_Object_Assignment_Files__r)
	                           From Quote_Line_Item__c Where id=:QuoteID limit 1];
	            if (lstQuoteLI.size() > 0){
	                String filesId = '';
	                String sourceLanguage = lstQuoteLI[0].Source_Language_ID__r.Language_Name__c;
	                String targetLanguage = lstQuoteLI[0].Target_Language_ID__r.Language_Name__c;
	                for(Quote_Line_Item__c qLI: lstQuoteLI){
	                    for(Quote_Line_Object_Assignment_File__c quoteFile: qLI.Quote_Line_Object_Assignment_Files__r){
	                        if(filesId != '')
	                            filesId = filesId+','+quoteFile.Source_File_Name__r.TradosAPIFileID__c;
	                        else
	                            filesId = quoteFile.Source_File_Name__r.TradosAPIFileID__c;
	                    }
	                }
	                if(filesId != '' ){
	                    callTMExportAPI(QuoteID, filesId, sourceLanguage, targetLanguage);
	                }
	            }
	        }
	        return GLSconfig.TMExportSuccess;
	    }
	    /**
	    *Following method is used for TX exporting for individual Quote Line Items.  TBD
	    ** / 
	    WebService static void exportTM(String QuoteLIId){
	        List<Quote_Line_Item__c> lstQuoteLI = new List<Quote_Line_Item__c>();
	        if(QuoteLIId != '' || QuoteLIId != null){
	            lstQuoteLI = [Select Id, Source_Language_ID__c, Target_Language_ID__c,Source_Language_ID__r.Language_Name__c,Target_Language_ID__r.Language_Name__c,
	                          (Select Id, Source_File_Name__r.Name , Source_File_Name__r.TradosAPIFileID__c From Quote_Line_Object_Assignment_Files__r)
	                           From Quote_Line_Item__c Where id=:QuoteLIId limit 1];
	            if (lstQuoteLI.size() > 0){
	                String filesId = '';
	                String sourceLanguage = lstQuoteLI[0].Source_Language_ID__r.Language_Name__c;
	                String targetLanguage = lstQuoteLI[0].Target_Language_ID__r.Language_Name__c;
	                for(Quote_Line_Item__c qLI: lstQuoteLI){
	                    for(Quote_Line_Object_Assignment_File__c quoteFile: qLI.Quote_Line_Object_Assignment_Files__r){
	                        if(filesId != '')
	                            filesId = filesId+','+quoteFile.Source_File_Name__r.TradosAPIFileID__c;
	                        else
	                            filesId = quoteFile.Source_File_Name__r.TradosAPIFileID__c;
	                    }
	                }
	                if(filesId != '' ){
	                    callTMExportAPI(QuoteLIId, filesId, sourceLanguage, targetLanguage);
	                }
	            }
	        }
	    }
	    
	    /** 
	    * Method to call Trados TMExport API, TBD
	    ** /
	    private static void callTMExportAPI (String quoteLineItemId, String filesId, String sourceLanguage, String targetLanguage){
	        String endPointURL = GLSconfig.tradosURLAnalyze+GLSconfig.tmExportAPICall+'?'+GLSconfig.paramSecretKey+GLSconfig.tradosSecretKey+GLSconfig.paramUserId+GLSconfig.userID+GLSconfig.paramFilesId+filesId+GLSconfig.paramtranslationMemory+GLSconfig.translationMemory+GLSconfig.paramSourceLanguage+sourceLanguage+GLSconfig.paramTargetLanguage+targetLanguage+GLSconfig.paramAPIName+GLSconfig.apiName+GLSconfig.paramIPAddress+GLSconfig.ipAddress+GLSconfig.paramFullTmExport+GLSconfig.fullTmExport+GLSconfig.paramBucketName+GLSconfig.glsBucket+GLSconfig.paramQuoteId+quoteLineItemId;
	        HttpResponse resL = getResponse(endPointURL);
	        
	        if(resL != null){
	            String httpResponseStatusCode = String.valueOf(resL.getStatusCode());
	            if(httpResponseStatusCode == GLSconfig.successCode)  {
	                String response = resL.getBody() ;  
	                String exportTransactionId = response.substringBetween('>','<');
	                Quote_Line_Item__c qli = [Select Id, TM_Export_Transaction_ID__c, TM_Export_Zip_File__c From Quote_Line_Item__c
	                                          Where Id =: quoteLineItemId limit 1];
	                String TMExportTransId = exportTransactionId;
	                qli.id = quoteLineItemId;
	                qli.TM_Export_Transaction_ID__c = TMExportTransId;
	                
	                try{
	                    update qli;
	                }catch(DmlException de){
	                    System.Debug('DML Error Occured---'+de);
	                }catch(Exception e){
	                    System.Debug('Error Occured In TM Export API Call---'+e);
	                }   
	            } 
	        }  
	    }
	*/    
}