/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSQuotePDFMigCtrlTest {
	
	public static void setupData(){
        
        Account accountObj = new Account();
        accountObj.Name = 'Amgen';
        accountObj.BillingStreet = 'Sample';
        accountObj.BillingCity = 'Sample';
        accountObj.BillingState = 'Sample';
        accountObj.BillingStreet = 'Sample';
        accountObj.BillingPostalCode='Sample';
        accountObj.BillingCountry='Sample';
        insert accountObj;
    	system.assertEquals('Amgen',accountObj.Name);
    	
        Contact contactObj = new Contact();
        contactObj.FirstName = 'Jennifer A';
        contactObj.LastName = 'Jennifer A';
        contactObj.AccountId = accountObj.Id;
        insert contactObj;
        system.assertEquals('Jennifer A',contactObj.FirstName);
        
        List<Quote__c> lstQuote = new List<Quote__c>();
        
        Quote__c quote = new Quote__c();
        quote.Quote_Name__c = 'Test Quote';
        quote.Client__c = accountObj.Id;
        quote.Status__c='In Preparation';
        quote.Contact__c = contactObj.Id;
        lstQuote.add(quote);
        
        Quote__c quote1 = new Quote__c();
        quote1.Quote_Name__c = 'Test Quote';
        quote1.Client__c = accountObj.Id;
        quote1.Contact__c = contactObj.Id;
        quote1.Clone_From__c=quote.id;
        quote1.Original_Quote_Id__c=quote.id;
        quote1.Status__c='Released';
        lstQuote.add(quote1);
        
        insert lstQuote;
        system.assertNotEquals(lstQuote[0].Id,null);
        
        Attachment attachPDF = new Attachment();
        attachPDF.Name = GLSConfig.pdfAttachmentName;
        attachPDF.ParentId = lstQuote[0].Id;
        String pdfContent = 'Sample PDF Body'; 
        attachPDF.Body = Blob.valueof(pdfContent);
        attachPDF.IsPrivate = false;
        attachPDF.ContentType = 'application/pdf';
        insert attachPDF;
        system.assertEquals('application/pdf',attachPDF.ContentType);
	}
	
    static testMethod void myUnitTest() {
    	
    	setupData();
    	Test.startTest();
	        GLSQuotePDFMigrationController migCtrlobj = new GLSQuotePDFMigrationController();
	        
	        PageReference pg = migCtrlobj.getawss3LoginCredentials();
	        system.assertEquals(pg,null);
			migCtrlobj.successFileupload();
	        
	        String returnPolicy = migCtrlobj.getPolicy();
	        system.assertNotEquals(returnPolicy,'');
	        
	        String returnSignedPolicy = migCtrlobj.getSignedPolicy();
	        system.assertNotEquals(returnSignedPolicy,'');
	        
	        Id qid = [SELECT ParentId FROM Attachment WHERE Name =: GLSConfig.pdfAttachmentName limit 1].ParentId;
	        String fname = 'TestFileName';
	        String header = 'TestHeader';
	        String fPath = 'TestPath';
	        
	        String returnSuccessFile = GLSQuotePDFMigrationController.successFileUpload(qid, fname, header, fPath);
	        system.assertNotEquals(returnSuccessFile,'');
	    Test.stopTest();
    }
}