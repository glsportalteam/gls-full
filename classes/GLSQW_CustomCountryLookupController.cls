public with sharing class GLSQW_CustomCountryLookupController {


  public List<Country__c> results{get;set;} // search results
  public string searchString{get;set;} // search keyword
 
  public GLSQW_CustomCountryLookupController () {
    searchString = System.currentPageReference().getParameters().get('lksrch');
    runSearch();  
  }
 
  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }
 
  // prepare the query and issue the search command
  private void runSearch() {
    if(searchString != null)
        searchString = String.escapeSingleQuotes(searchString);
    results = performSearch(searchString);               
  } 
 
  // run the search and return the records found. 
  private List<Country__c> performSearch(string searchString) {
     String qliId = System.currentPageReference().getParameters().get('qliId');
     system.debug('---qliId---:' +qliId);
     String targetLangId ='';
     List<String> lstCountryId = new List<String>(); 
     List<Country_Language__c> lstCountryLang = new List<Country_Language__c>(); 
     String soql = 'select id, name from Country__c';
     
     if(qliId != '' && qliId != null)
        targetLangId = [Select Target_Language_Id__c from Quote_Line_Item__c Where Id =: qliId].Target_Language_Id__c;
    
     if(targetLangId != '' && targetLangId!= null){
        lstCountryLang = [select Id, Country__c From Country_Language__c where Language__c = :targetLangId];
     }
    for(Country_Language__c  cntryId : lstCountryLang){
        lstCountryId.add(String.valueOf(cntryId.Country__c));
    }
    
    Boolean flag = false;
    if(searchString != null && searchString != '' )
        soql = soql +  ' where name LIKE \'%' + searchString +'%\' ';
    else
          flag = true;
    
    if(lstCountryId != null && lstCountryId.size()>0)   {
        if(flag==true){
            soql = soql + ' where Id In:lstCountryId';
        }/*else{
            soql = soql + 'Id In:lstCountryId';
        }*/
    }      
    soql = soql + ' order by Name limit 250';
    system.debug('---soql---: '+soql);
    return database.query(soql); 
 
  }
 
  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
 
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
}