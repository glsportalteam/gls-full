/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 */
 
@isTest
private class GLSCustomFileLookupControllerTest {  
      /**
       * This Unit Test method
     */
    static testMethod void myUnitTest() {  
        GLSSetupData.setupData();
        test.startTest();
            ApexPages.currentPage().getParameters().put('lksrch','test');
            ApexPages.currentPage().getParameters().put('quote',GLSSetupData.quote.id);
           GLSCustomFileLookupController  customCont = new GLSCustomFileLookupController();
        test.stopTest();
    }
    static testMethod void searchTest() {
       GLSSetupData.setupData();
       test.startTest();
            ApexPages.currentPage().getParameters().put('lksrch','test');
            ApexPages.currentPage().getParameters().put('quote',GLSSetupData.quote.id);
			GLSCustomFileLookupController  customCont = new GLSCustomFileLookupController();
			customCont.search();
    	test.stopTest();
    } 	
    static testMethod void getFormTagTest() {
       GLSSetupData.setupData();
       test.startTest();
            ApexPages.currentPage().getParameters().put('lksrch','test');
            ApexPages.currentPage().getParameters().put('quote',GLSSetupData.quote.id);
			GLSCustomFileLookupController  customCont = new GLSCustomFileLookupController();    	
            string str=customCont.getFormTag();
    	test.stopTest();
    }
    static testMethod void getTextBoxTest() {
       GLSSetupData.setupData();
       test.startTest();
          	ApexPages.currentPage().getParameters().put('lksrch','test');
            ApexPages.currentPage().getParameters().put('quote',GLSSetupData.quote.id);
			GLSCustomFileLookupController  customCont = new GLSCustomFileLookupController();
    	 	string str=customCont.getTextBox();
    	test.stopTest();
    }
}