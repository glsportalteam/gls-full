/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSGenerateQuotePDFControllerTest {
 	
    static testMethod void generatePDFTest() {
    	GLSSetupData.createQLIServices();
    	
    	test.startTest();
    		
    		ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.Id);
	       	GLSGenerateQuotePDFController generatePDFController = new GLSGenerateQuotePDFController(new ApexPages.StandardController(GLSSetupData.quote));
	       	
	       	AWS_Credential__c awsCreds = new AWS_Credential__c();
     		awsCreds.Name = 'NAME OF KEY TO USE';
     		awsCreds.Key__c = 'AKIAJOBAQEOXUL36TEST';
     		awsCreds.Secret__c = 'jdtcH2X5XUc4V40w6Hn12uRFu0iXHP3y4sumTEST';
     		insert awsCreds;
 			system.assertEquals('AKIAJOBAQEOXUL36TEST',awsCreds.Key__c); 
 			
	       	generatePDFController.getawss3LoginCredentials();
	       	generatePDFController.getPolicy();
	       	generatePDFController.getSignedPolicy();
	       	String bucketTest = generatePDFController.bucketName;
	       	
	       	AWS_File__c awsRecord = new AWS_File__c(Bucket_Name__c = 'TestBucket', Amazon_S3_source_file_path__c = 'TestPath', File_Name__c = 'TestFileName3.pdf', version_id__c = 'TestHeader', Record_Type__c = 'QuotePDF');
	       	insert awsRecord;
	       	system.assertEquals('TestBucket',awsRecord.Bucket_Name__c); 
	       	
	        String returnInsertAtt = GLSGenerateQuotePDFController.insertAttachment(GLSSetupData.quote.Id);
	       	system.assertNotEquals(returnInsertAtt,''); 
	       	
	       	Attachment attachPDF = new Attachment();
	        attachPDF.Name = GLSConfig.pdfAttachmentName;
	        attachPDF.ParentId = GLSSetupData.quote.Id;
	        String pdfContent = 'Sample PDF Body'; 
	        attachPDF.Body = Blob.valueof(pdfContent);
	        attachPDF.IsPrivate = false;
	        attachPDF.ContentType = 'application/pdf';
	        insert attachPDF;	
	        system.assertEquals(GLSConfig.pdfAttachmentName,attachPDF.Name); 
	        
	        String fName = 'TestFileName1.pdf';
	        String header = 'TestHeader';
	        String fPath = 'TestPath';
			
			Quote_PDF_File__c quotePDF = [SELECT Id FROM Quote_PDF_File__c LIMIT 1];
			
	        String returnSuccessFile = GLSGenerateQuotePDFController.successFileUpload(header, fName, fPath, quotePDF.Id);
	        system.assertEquals(returnSuccessFile,header); 
	        
       	test.stopTest(); 
    }  
    
    static testMethod void generatePDFTest2() {
    	GLSSetupData.createQLIServices();
    	
    	test.startTest();
    		ApexPages.currentPage().getParameters().put('id',GLSSetupData.quote.Id);
	       	GLSGenerateQuotePDFController generatePDFController = new GLSGenerateQuotePDFController(new ApexPages.StandardController(GLSSetupData.quote));
 			
 			Date d = System.today();
            Integer day = d.day();
            Integer month = d.month();
            Integer year = d.year();
            String dateToday = year + '_' + month + '_' + day;
 			
 			list<AWS_File__c> lstAWSRecord = new list<AWS_File__c>();
	       	AWS_File__c awsRecord = new AWS_File__c(Bucket_Name__c = 'TestBucket', Amazon_S3_source_file_path__c = 'TestPath', 
	       											File_Name__c = 'TestFileName' + dateToday + '_3.pdf', version_id__c = 'TestHeader', 
	       											Record_Type__c = 'QuotePDF');
	        lstAWSRecord.add(awsRecord);
	        AWS_File__c awsRecord1 = new AWS_File__c(Bucket_Name__c = 'TestBucket', Amazon_S3_source_file_path__c = 'TestPath', 
	       											File_Name__c = 'TestFileName' + dateToday + '_2.pdf', version_id__c = 'TestHeader', 
	       											Record_Type__c = 'QuotePDF');
	        lstAWSRecord.add(awsRecord1);
	       	insert lstAWSRecord;
	       	system.assertNotEquals(lstAWSRecord[0].Id,null);
	       	
	       	list<Quote_PDF_File__c> lstQuotePdfRecord = new list<Quote_PDF_File__c>();
	       	Quote_PDF_File__c quotePDF = new Quote_PDF_File__c(AWS_File_Id__c = lstAWSRecord[0].Id, Quote_ID__c = GLSSetupData.quote.Id);
	       	lstQuotePdfRecord.add(quotePDF);
	       	Quote_PDF_File__c quotePDF1 = new Quote_PDF_File__c(AWS_File_Id__c = lstAWSRecord[1].Id, Quote_ID__c = GLSSetupData.quote.Id);
	       	lstQuotePdfRecord.add(quotePDF1);
	       	Quote_PDF_File__c quotePDF2 = new Quote_PDF_File__c(Quote_ID__c = GLSSetupData.quote.Id);
	       	lstQuotePdfRecord.add(quotePDF2);
	       	
	       	insert lstQuotePdfRecord;
	       	system.assertNotEquals(lstQuotePdfRecord[0].Id,null);
	       	
	       	String returnInsertAtt = GLSGenerateQuotePDFController.insertAttachment(GLSSetupData.quote.Id);
	       	system.assertNotEquals(returnInsertAtt,''); 
	        
	        GLSGenerateQuotePDFController.deleteQuotePDF(quotePDF2.Id);
       	test.stopTest();
    }

}