/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest

private class GLSDeleteQuotePDFSchedulerTest {
    
    static testMethod void schedularMethodTest() {
        
        GLSSetupData.createQLIServices(); 
        
        Quote_PDF_File__c quotePDF = new Quote_PDF_File__c (Quote_ID__c = GLSSetupData.quote.Id, File_Name__c = 'TestFile.pdf');
        insert quotePDF;
        system.assertEquals(GLSSetupData.quote.Id, quotePDF.Quote_ID__c);
        
        Quotes_File__c quoteFile = new Quotes_File__c (Quote__c = GLSSetupData.quote.Id, File_Name__c = 'TestFile.pdf');
        insert quoteFile;
        system.assertEquals(GLSSetupData.quote.Id, quoteFile.Quote__c);
        
        Test.startTest();
            String jobId = System.schedule('GLSDeleteQuotePDFSchedulerTest','0 0 6 * * ? ', new GLSDeleteQuotePDFScheduler());
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered,NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals('0 0 6 * * ?',ct.CronExpression);
        Test.stopTest();
    }
    
}