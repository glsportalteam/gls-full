/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSQW_NewQuoteControllerTest {

    static testMethod void multipleMethodsTest() {
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id', GLSSetupData.quote4.id);
            ApexPages.currentPage().getParameters().put('parentId', GLSSetupData.quote1.id);
            GLSQW_NewQuoteController newQuoteControllerObj = new GLSQW_NewQuoteController(new ApexPages.StandardController(GLSSetupData.quote1));
            newQuoteControllerObj.updateRateSheetInputField();
            newQuoteControllerObj.refreshQuoteFileList();  
             
            newQuoteControllerObj.customSave();
            newQuoteControllerObj.getItems();
            
            newQuoteControllerObj.cloneFileMethod();
            System.assertEquals(newQuoteControllerObj.isCloneComplete,false);
             
            PageReference pg=new PageReference('');
            pg=newQuoteControllerObj.saveAndNew();
            //system.debug('Pg in test= '+pg);
            System.assertEquals('/apex/GLSQW_NewQuotePage',pg.getUrl().substring(0,24));
            
            pg=newQuoteControllerObj.customCancel();
            System.assertEquals('/apex/GLS_QWQuoteListView',pg.getUrl().substring(0,25));
            
            newQuoteControllerObj.customCancelonEditing();
            system.assertEquals( newQuoteControllerObj.isReadOnly,true);
            
            newQuoteControllerObj.customEdit();
            system.assertEquals( newQuoteControllerObj.isReadOnly,false);
            
            pg=newQuoteControllerObj.goToNext();
            System.assertEquals('/apex/GLSQW_ScopeOfServicesPage?id='+GLSSetupData.quote4.id,pg.getUrl());
             
           pg= newQuoteControllerObj.renderQuoteasPDF();   
           System.assertEquals( '/apex/GLSGenerateQuotePDF?id='+GLSSetupData.quote4.id,pg.getUrl()); 
           
            
            newQuoteControllerObj.submitAndProcessApprovalRequest();
            
            
            newQuoteControllerObj.saveAddendumsDetails();
            
            
            newQuoteControllerObj.cancelQuote();
            System.assertEquals(newQuoteControllerObj.newQuote.Status__c , 'Cancelled');
              
            System.assertEquals(newQuoteControllerObj.quoteId,GLSSetupData.quote4.id);
              
        test.stopTest();
    }
     static testMethod void multipleMethodsTestFirst() {
          GLSSetupData.createQLIServices();
          test.startTest();
            GLSQW_NewQuoteController newQuoteControllerObj = new GLSQW_NewQuoteController(new ApexPages.StandardController(GLSSetupData.quote1));
            newQuoteControllerObj.getItems();
            newQuoteControllerObj.customCancelonEditing();
            system.assertEquals( newQuoteControllerObj.isReadOnly,false);
           test.stopTest(); 
     }
     static testMethod void myUnitTest(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('aid', GLSSetupData.accountObj.Id);
            ApexPages.currentPage().getParameters().put('cid', GLSSetupData.contactObj.Id);
            GLSQW_NewQuoteController newQuoteControllerObj = new GLSQW_NewQuoteController(new ApexPages.StandardController(GLSSetupData.quote6));
        test.stopTest();
        
    }
     static testMethod void myUnitTest1(){
        GLSSetupData.createQLIServices();
        test.startTest();
            GLSQW_NewQuoteController newQuoteControllerObj = new GLSQW_NewQuoteController(new ApexPages.StandardController(GLSSetupData.quote));
            newQuoteControllerObj.submitAndProcessApprovalRequest();
        test.stopTest();
        
    }
    static testMethod void multipleMethodsTestSecond(){
        GLSSetupData.createQLIServices();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id', GLSSetupData.quote4.id);
            ApexPages.currentPage().getParameters().put('parentId', GLSSetupData.quote6.id);
            GLSQW_NewQuoteController newQuoteControllerObj1 = new GLSQW_NewQuoteController(new ApexPages.StandardController(GLSSetupData.quote6));
            newQuoteControllerObj1.customSave();
        test.stopTest();
        
    }
    
    static testMethod void NullClientSave(){
        GLSSetupData.createQLIServices();
        test.startTest();
            Quote__c quoteObj = new Quote__c (Id=GLSSetupData.quote4.id);
            quoteObj.Client__c =null;
            update quoteObj ;
            ApexPages.currentPage().getParameters().put('id', quoteObj.Id);
            ApexPages.currentPage().getParameters().put('parentId', GLSSetupData.quote6.id);
            GLSQW_NewQuoteController newQuoteControllerObj1 = new GLSQW_NewQuoteController(new ApexPages.StandardController(quoteObj));
            newQuoteControllerObj1.customSave();
        test.stopTest();
    }
    
    static testMethod void multipleMethodWithFileTest(){
        GLSSetupData.createQLIServices();
        test.startTest();
        
            ApexPages.currentPage().getParameters().put('id', GLSSetupData.quote4.id);
            ApexPages.currentPage().getParameters().put('parentId', GLSSetupData.quote1.id);
            GLSQW_NewQuoteController newQuoteControllerObj = new GLSQW_NewQuoteController(new ApexPages.StandardController(GLSSetupData.quote1));
            newQuoteControllerObj.qFileId = GLSSetupData.qFiles4.id;
          
            newQuoteControllerObj.downloadQuoteFile();
            
            newQuoteControllerObj.editQuoteFile();
            
            
           PageReference pg1=new PageReference('');
           pg1= newQuoteControllerObj.deleteQuoteFile();
           System.assertEquals(null,pg1);
          
             Quote__c quoteObj = new Quote__c (Id=GLSSetupData.quote4.id);
             quoteObj .Status__c ='In Preparation';
             update quoteObj ;
             ApexPages.currentPage().getParameters().put('id', quoteObj.id);
             
             ApexPages.currentPage().getParameters().put('selectedapprover','Submit to BDM');
             newQuoteControllerObj.submitAndProcessApprovalRequest();
             
            /* User Usr = new User(alias = 'gport111', email='sysadmin@testGLS.com',emailencodingkey='UTF-8',
                                lastname='Testing', languagelocalekey='en_US',localesidkey='en_US', profileid = GLSSetupData.p.Id, ContactId = quoteObj.Contact__c,
                                timezonesidkey='America/Los_Angeles', username='sysadminTest@gls.com');
            
             insert Usr;*/
             ApexPages.currentPage().getParameters().put('selectedapprover', 'Submit to Client for Approval');
             newQuoteControllerObj.submitAndProcessApprovalRequest();
              
             ApexPages.currentPage().getParameters().put('selectedapprover', 'Submitted for Approval');
             newQuoteControllerObj.submitAndProcessApprovalRequest();
             
               newQuoteControllerObj.recallApprovalProcess();
               
        test.stopTest();
    }
    
    
    
    static testMethod void MethodgetQuoteFileDetails(){
        GLSSetupData.setupData();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id', GLSSetupData.quote.id);
            GLSQW_NewQuoteController newQuoteControllerObj = new GLSQW_NewQuoteController(new ApexPages.StandardController(GLSSetupData.quote));
            newQuoteControllerObj.getQuoteFileDetails();
        test.stopTest();
    }
    
     static testMethod void recallGEFAPITest(){
        GLSSetupData.setupData();
        test.startTest();
            ApexPages.currentPage().getParameters().put('id', GLSSetupData.quote.id);
            GLSQW_NewQuoteController newQuoteControllerObj = new GLSQW_NewQuoteController(new ApexPages.StandardController(GLSSetupData.quote));
            newQuoteControllerObj.recallGEFAPI();
        test.stopTest();
    }
}