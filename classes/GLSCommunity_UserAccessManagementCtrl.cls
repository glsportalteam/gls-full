/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSCommunity_UserAccessManagementCtrl Class
* Function: This class is used as a controller for the GLSCommunity_UserAccessManagementPage
*/

public with sharing class GLSCommunity_UserAccessManagementCtrl {
    
    //Variable Declaration for the class
    public List<SelectOption> lstProfiles                           {get; set;}
    public List<SelectOption> lstUsers                              {get; set;}
    public String selectedProfile                                   {get; set;}
    public String selectedUser                                      {get; set;}
    public List<ProgramWrapper> lstProgramWrapper                   {get; set;} 
    public Boolean isProgramManager                                 {get; set;}
    public Boolean isProtocolManager                                {get; set;}
    public List<User_Access_Management__c> lstUserAccessMgmt        {get; set;}
    public Id deleteUAMRecordId                                     {get; set;}
    public Map<Id, List<ProtocolWrapper>> mapProtocolWrapper        {get; set;}
    public List<Program__c> lstPrgProtocolMenu                      {get; set;}
    public Boolean noRecordsAvailable                               {get; set;}
    
    private User currentUser;
    private Set<Id> setProgramInUAM;
    private Set<Id> setProtocolInUAM;
    private Set<String> setProfiles;
    private Map<String, List<User>> mapProfileUsers;
    private Id contactId;
    private List<User_Access_Management__c> lstUserAccessMgmtAll;
    
    //Constructor
    public GLSCommunity_UserAccessManagementCtrl(){
        
        //Capture parameters from URL
        contactId = ApexPages.currentPage().getParameters().get('conID');
        
        //Initialization
        lstProfiles = new List<SelectOption>();
        lstUsers = new List<SelectOption>();
        selectedProfile = '';
        selectedUser = '';
        lstProgramWrapper = new List<ProgramWrapper>();
        isProgramManager = false;
        isProtocolManager = false;
        currentUser = new User();
        setProgramInUAM = new Set<Id>();
        lstUserAccessMgmt = new List<User_Access_Management__c>();
        lstUserAccessMgmtAll = new List<User_Access_Management__c>();
        setProtocolInUAM = new Set<Id>();
        deleteUAMRecordId = null;
        mapProtocolWrapper = new Map<Id, List<ProtocolWrapper>>();
        lstPrgProtocolMenu = new List<Program__c>();
        noRecordsAvailable = false;
        setProfiles = new Set<String>();
        mapProfileUsers = new Map<String, List<User>>();
        
        //Method Calls
        getCurrentUserRecord();
        getProfiles();
        lstUserAccessMgmt = getUserAccessRecords();
        lstUsers.add(new SelectOption('','--None--'));
    }
    
    /* 
    @Description: Method to get the details of logged in user
    @Params: None
    @Return Type: None
    */
    private void getCurrentUserRecord(){
        //If logged in as Internal User
        if(contactId != null){
            currentUser = [SELECT ContactId, AccountId, Profile.Name, Id, Name 
                            FROM User 
                            WHERE ContactId =: contactId LIMIT 1];
        }
        //Portal login
        else{
            currentUser = [SELECT ContactId, AccountId, Profile.Name, Id, Name 
                            FROM User 
                            WHERE Id =: userInfo.getUserId() LIMIT 1];
        }   
    }
    
    /* 
    @Description: Method to populate the picklist for profile selection based on the Profile of logged in user
    @Params: None
    @Return Type: None
    */
    private void getProfiles(){
        if(currentUser != null){
            lstProfiles.add(new SelectOption('', '--None--'));
            if(currentUser.Profile.Name == GLSconfig.AccountManager){
                lstProfiles.add(new SelectOption(GLSconfig.ProgramManager, GLSconfig.ProgramManager));
                lstProfiles.add(new SelectOption(GLSconfig.ProtocolManager, GLSconfig.ProtocolManager));
                lstProfiles.add(new SelectOption(GLSconfig.LPStdUser, GLSconfig.LPStdUser));
                setProfiles.add(GLSconfig.ProgramManager);
                setProfiles.add(GLSconfig.ProtocolManager);
                setProfiles.add(GLSconfig.LPStdUser);
            }
            else if(currentUser.Profile.Name == GLSconfig.ProgramManager){
                lstProfiles.add(new SelectOption(GLSconfig.ProtocolManager, GLSconfig.ProtocolManager));
                lstProfiles.add(new SelectOption(GLSconfig.LPStdUser, GLSconfig.LPStdUser));
                setProfiles.add(GLSconfig.ProtocolManager);
                setProfiles.add(GLSconfig.LPStdUser);
            }
            else if(currentUser.Profile.Name == GLSconfig.ProtocolManager){
                lstProfiles.add(new SelectOption(GLSconfig.LPStdUser, GLSconfig.LPStdUser));
                setProfiles.add(GLSconfig.LPStdUser);
            }
        }
        getAllUsers();
    }
    
    /* 
    @Description: Method to populate the map for corresponding list of users for profile.
    @Params: None
    @Return Type: None
    */
    private void getAllUsers(){
        List<Profile> lstProfilesLocal = [SELECT Name, (SELECT Id, Name 
                                                        FROM Users 
                                                        WHERE IsActive = true 
                                                        AND AccountId =: currentUser.AccountId)
                                            FROM Profile
                                            WHERE Name IN : setProfiles];
        if(lstProfilesLocal != null && lstProfilesLocal.size() > 0){
            for(Profile p : lstProfilesLocal){
                List<User> lstUsersLocal = new List<User>();
                for(User u : p.Users){
                    lstUsersLocal.add(u);
                }
                mapProfileUsers.put(p.Name, lstUsersLocal);
            }
        }
    }
    
    /* 
    @Description: Method to populate list of all records for User_Access_Management__c and those created by the logged in user.
    @Params: None
    @Return Type: List<User_Access_Management__c>
    */
    private List<User_Access_Management__c> getUserAccessRecords(){
        List<User_Access_Management__c> lstUserAccessMgmtLocal = new List<User_Access_Management__c>();
        lstUserAccessMgmtAll = [SELECT Profile_Name__c, ProgramId__c, ProgramId__r.Program_Name__c,
                                ProtocolId__c, ProtocolId__r.Protocol_Name__c, User_Name__c, 
                                User_Name__r.Name, OwnerId 
                                FROM User_Access_Management__c];
                
        for(User_Access_Management__c uam : lstUserAccessMgmtAll){
            if(uam.OwnerId == currentUser.Id){
                lstUserAccessMgmtLocal.add(uam);
            }
        }
        return lstUserAccessMgmtLocal;
    }
    
    /* 
    @Description: Method to get the list of Active users for selected Profile.
    @Params: None
    @Return Type: None
    */
    public void getUsersForProfile(){
        isProgramManager = true;
        lstUsers = new List<SelectOption>();
        lstUsers.add(new SelectOption('', '--None--'));
        if(selectedProfile != ''){
            if(mapProfileUsers.containsKey(selectedProfile)){
                for(User u : mapProfileUsers.get(selectedProfile)){
                    lstUsers.add(new SelectOption(u.Id, u.Name));
                }
            }
        }
    }
    
    /* 
    @Description: Method to get the list of Programs and Protocols based on the selected Profile and set the Booleans to hide or show the 
                  output panel of Program/Protocol selection.
    @Params: None
    @Return Type: None
    */
    public void getProgramsProtocols(){
        if(selectedProfile == GLSconfig.ProgramManager){
            isProgramManager = true;
            isProtocolManager = false;
            getProgramList();
        }
        else if(selectedProfile == GLSconfig.ProtocolManager || selectedProfile == GLSconfig.LPStdUser){
            isProgramManager = false;
            isProtocolManager = true;
            getProtocolList();
        }
        else{
            isProgramManager = false;
            isProtocolManager = false;
        }
    }
    
    /* 
    @Description: Method to insert User_Access_Management__c record selected from page.
    @Params: None
    @Return Type: None
    */
    public void addUAMRecords(){
        List<User_Access_Management__c> lstUAMInsert = new List<User_Access_Management__c>();
        User_Access_Management__c uam = new User_Access_Management__c();
        try{
            if(selectedProfile == GLSconfig.ProgramManager){
                if(lstProgramWrapper != null && lstProgramWrapper.size() > 0){
                    for(ProgramWrapper pw : lstProgramWrapper){
                        if(pw.isSelected){
                            uam = new User_Access_Management__c();
                            uam.Profile_Name__c = selectedProfile;
                            uam.ProgramId__c = pw.prg.Id;
                            uam.User_Name__c = selectedUser;
                            lstUAMInsert.add(uam);
                        }
                    }
                }
                if(lstUAMInsert != null && lstUAMInsert.size() > 0){
                    insert lstUAMInsert;
                }
                lstUserAccessMgmt = getUserAccessRecords();
                getProgramList();
            }
            
            else if(selectedProfile == GLSconfig.ProtocolManager || selectedProfile == GLSconfig.LPStdUser){
                if(mapProtocolWrapper != null && mapProtocolWrapper.size() > 0){
                    for(List<ProtocolWrapper> lstWrap : mapProtocolWrapper.values()){
                        for(ProtocolWrapper objWrap : lstWrap){
                            if(objWrap.isSelected){
                                uam = new User_Access_Management__c();
                                uam.Profile_Name__c = selectedProfile;
                                uam.ProtocolId__c = objWrap.protocol.Id;
                                uam.User_Name__c = selectedUser;
                                lstUAMInsert.add(uam);
                            }
                        }
                    }
                }
                if(lstUAMInsert != null && lstUAMInsert.size() > 0){
                    insert lstUAMInsert;
                }
                lstUserAccessMgmt = getUserAccessRecords();
                getProtocolList();
            }
        }
        catch(DMLException de){
            system.debug(GLSConfig.DMLExceptionOccured + de);
        }
        catch(Exception e){
            system.debug(GLSConfig.ExceptionOccured + e);
        }
    }
    
    /* 
    @Description: Method to delete User_Access_Management__c record selected from page.
    @Params: None
    @Return Type: None
    */
    public void deleteUAMRecord(){
        try{
            User_Access_Management__c delRecord = new User_Access_Management__c(Id = deleteUAMRecordId);
            delete delRecord;
            
            lstUserAccessMgmt = getUserAccessRecords();
        }
        catch(DMLException de){
            system.debug(GLSConfig.DMLExceptionOccured + de);
        }
        catch(Exception e){
            system.debug(GLSConfig.ExceptionOccured + e);
        }
    }
    
    /* 
    @Description: Method to get the list of Programs that have the Account same as that of logged in user and yet do not have the access.
    @Params: None
    @Return Type: None
    */
    private void getProgramList(){
        lstProgramWrapper = new List<ProgramWrapper>();
        if(currentUser.AccountId != null){
            setProgramInUAM = new Set<Id>();
            for(User_Access_Management__c uam : lstUserAccessMgmtAll){
                if(uam.User_Name__c == selectedUser){
                    setProgramInUAM.add(uam.ProgramId__c);
                }
            }
            lstProgramWrapper = new List<ProgramWrapper>();
            
            List<Program__c> lstProgram = [SELECT Id, Program_Name__c
                                            FROM Program__c 
                                            WHERE Id NOT IN : setProgramInUAM
                                            AND Account__c =: currentUser.AccountId];
            if(lstProgram != null && lstProgram.size() > 0){
                for(Program__c prg : lstProgram){
                    lstProgramWrapper.add(new ProgramWrapper(prg, false));
                }
                noRecordsAvailable = false;
            }
            else{
                noRecordsAvailable = true;
                isProgramManager = false;
                isProtocolManager = false;
            }
        }
    }
    
    /* 
    @Description: Method to get the list of Protocols that have the Account same as that of logged in user and yet do not have the access
                    and logged in user have access to the Protocol's Program.
    @Params: None
    @Return Type: None
    */
    private void getProtocolList(){
        
        lstProgramWrapper = new List<ProgramWrapper>();
        mapProtocolWrapper = new Map<Id, List<ProtocolWrapper>>();
        lstPrgProtocolMenu = new List<Program__c>();
        
        //Set to store the Id of programs whose access is given to the logged in user.
        Set<Id> setProgramAccessGiven = new Set<Id>();
        //Set to store the Id of protocols whose access is given to the logged in user.
        Set<Id> setProtocolAccessGiven = new Set<Id>();
        
        if(currentUser.AccountId != null){
            setProtocolInUAM = new Set<Id>();
            for(User_Access_Management__c uam : lstUserAccessMgmtAll){
                if(uam.User_Name__c == selectedUser){
                    setProtocolInUAM.add(uam.ProtocolId__c);
                }
                if(uam.User_Name__c == currentUser.Id){
                    setProgramAccessGiven.add(uam.ProgramId__c);
                    setProtocolAccessGiven.add(uam.ProtocolId__c);
                }
            }
            List<Program__c> lstProgram = new List<Program__c>();
            if(currentUser.Profile.Name == GLSconfig.AccountManager){
                lstProgram = [SELECT Id, Program_Name__c, (SELECT Id, Protocol_Name__c FROM Protocols__r 
                                                            WHERE Id NOT IN : setProtocolInUAM)
                                FROM Program__c 
                                WHERE Account__c =: currentUser.AccountId];
            }
            else if(currentUser.Profile.Name == GLSconfig.ProtocolManager){
                lstProgram = [SELECT Id, Program_Name__c, (SELECT Id, Protocol_Name__c FROM Protocols__r 
                                                            WHERE Id IN : setProtocolAccessGiven
                                                            AND Id NOT IN : setProtocolInUAM)
                                FROM Program__c 
                                WHERE Account__c =: currentUser.AccountId];
            }
            else{
                lstProgram = [SELECT Id, Program_Name__c, (SELECT Id, Protocol_Name__c FROM Protocols__r 
                                                            WHERE Id NOT IN : setProtocolInUAM)
                                FROM Program__c 
                                WHERE Account__c =: currentUser.AccountId
                                AND Id IN : setProgramAccessGiven];
            }
            if(lstProgram != null && lstProgram.size() > 0){
                for(Program__c prg : lstProgram){
                    List<ProtocolWrapper> lstProtocolWrapper = new List<ProtocolWrapper>();
                    for(Protocol__c pro : prg.Protocols__r){
                        lstProtocolWrapper.add(new ProtocolWrapper(pro, false));
                    }
                    if(lstProtocolWrapper != null && lstProtocolWrapper.size() > 0){
                        mapProtocolWrapper.put(prg.Id, lstProtocolWrapper);
                        lstPrgProtocolMenu.add(prg);
                    }
                }
                noRecordsAvailable = false;
            }
            else{
                noRecordsAvailable = true;
                isProgramManager = false;
                isProtocolManager = false;
            }
            if(!(lstPrgProtocolMenu != null && lstPrgProtocolMenu.size() > 0)){
                noRecordsAvailable = true;
            }
        }
    }
    
    //Wrapper to get the list of selected Programs for which the access is to be provided.
    public class ProgramWrapper{
        public Program__c prg               {get; set;}
        public Boolean isSelected           {get; set;}

        //Constructor
        public ProgramWrapper(Program__c prg, Boolean isSelected){
            this.prg = prg;
            this.isSelected = isSelected;
        }
    }
    
    //Wrapper to get the list of selected Protocols for which the access is to be provided.
    public class ProtocolWrapper{
        public Protocol__c protocol         {get; set;}
        public Boolean isSelected           {get; set;}
        
        public ProtocolWrapper(Protocol__c protocol, Boolean isSelected){
            this.protocol = protocol;
            this.isSelected = isSelected;
        }
    }
}