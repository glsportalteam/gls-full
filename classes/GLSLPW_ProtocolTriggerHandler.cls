/*
* Rename the class name to GLSLPW_ProtocolTriggerHandler
*/
public with sharing class GLSLPW_ProtocolTriggerHandler {
    Public static void createPurchaseAgrrement(Map<Id, Protocol__c> oldProtocolMap,Map<Id, Protocol__c> newProtocolMap){
        List<Protocol__c> LstEstProtocol;
        List<Purchase_Agreement__c> LstPurchaseAgreement = new List<Purchase_Agreement__c>();  
        if(oldProtocolMap.keySet() !=null){
            LstEstProtocol = [select id,(select id from Purchase_Agreement__r),Status__c from Protocol__c where id IN: oldProtocolMap.keySet()];
        }       
        for(Protocol__c estPro:LstEstProtocol){
            Purchase_Agreement__c EstAgreement; 
             if(newProtocolMap.get(estPro.id).Status__c == GLSConfig.Open && oldProtocolMap.get(estPro.id).status__c != newProtocolMap.get(estPro.id).status__c && !GLSLPW_PurchaseAgreementTriggerHandler.skipPATrigger){
                 if(estPro.Purchase_Agreement__r != null && estPro.Purchase_Agreement__r.size() > 0){
                    
                }else{
                    EstAgreement = new Purchase_Agreement__c();
                    EstAgreement.Total_Amount__c =0;
                    EstAgreement.Protocol_Id__c = estPro.Id;
                    EstAgreement.Status__c = 'Pending';
                    LstPurchaseAgreement.add(EstAgreement);  
                }
            }
        }
        try{
            if(LstPurchaseAgreement!=null && LstPurchaseAgreement.size()>0)
                insert LstPurchaseAgreement;
        }catch(DMLException e){
            system.debug(e.getMessage()+'-------error occured----------');
        }       
    }
}