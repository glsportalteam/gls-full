public with sharing class GLSAccessUtility {
	public static List<Program__c> getProgramList(String accountId){
		if(accountId != null && accountId !='' ){
       		// fetch list of program associated with account of logged in user
       		return [select id,name,Account__r.name,(select id,Name,Status__c from Protocols__r where Status__c='open' order by CreatedDate ) from Program__c where Account__c=:accountId order by CreatedDate];
        }
        return null;
	}
}