/* Copyright (c) 2016, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSQW_UploadCSVFilesController  Class
* Function: Upload files to Amazon S3 server and create corresponding Quote file record for the uploaded file
*/

@isTest
private class GLSQW_UploadCSVFilesControllerTest {

    /**
    * This Unit Test method will test the constructor 
    **/
    static testMethod void myUnitTest() { 
        GLSSetupData.setupData();
        Test.startTest();
         
        ApexPages.currentPage().getParameters().put('quoteId',GLSSetupData.quote.Id);
        GLSQW_UploadCSVFilesController uploadFile = new GLSQW_UploadCSVFilesController(new ApexPages.StandardController(GLSSetupData.quote));
      
        System.runAs(GLSSetupData.u){ 
         uploadFile = new GLSQW_UploadCSVFilesController(new ApexPages.StandardController(GLSSetupData.quote));
       }
        Test.stopTest();
    }

      /**
    * This Unit Test method will test successful creation of Quote file record in Salesforce 
    **/
    static testMethod void successFileuploadTest() {
        GLSSetupData.createQLI();
        Test.startTest();
        ApexPages.currentPage().getParameters().put('quoteId',GLSSetupData.quote.Id);
        System.debug('quoteLI1===='+GLSSetupData.quoteLI1);
        GLSQW_UploadCSVFilesController uploadFile = new GLSQW_UploadCSVFilesController(new ApexPages.StandardController(GLSSetupData.quoteLI1));
        uploadFile.newFileName = 'Test.txt';
        uploadFile.csvDataToParse = ';;;Context TM;;;;Repetitions;;;;100% Matches;;;;95% - 99%;;;;85% - 94%;;;;75% - 84%;;;;50% - 74%;;;;No Match;;;;Total\n'+
            'File;Tagging Errors;Chars/Word;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Characters\n'+
            'S1512_InfConsent_091416.docx | en_us>es_us;0;4.86;231;2893;0;49.74;1;10;0;0.17;101;1301;0;22.37;23;239;0;4.11;5;86;0;1.48;7;130;0;2.24;22;336;0;5.78;38;821;0;14.12;428;5816;0;28279\n'+
            'S1602_InfConsent_92116.docx | en_us>es_us;0;4.85;108;1599;0;27.13;3;27;0;0.46;113;1452;0;24.64;42;391;0;6.63;12;241;0;4.09;4;51;0;0.87;43;645;0;10.95;82;1487;0;25.23;407;5893;0;28571';
        uploadFile.successFileupload();

        GLSQW_UploadCSVFilesController uploadFile1 = new GLSQW_UploadCSVFilesController(null);
        uploadFile1.csvDataToParse = ';;;Context TM;;;;Repetitions;;;;100% Matches;;;;95% - 99%;;;;85% - 94%;;;;75% - 84%;;;;50% - 74%;;;;No Match;;;;Total\n'+
            'File;Tagging Errors;Chars/Word;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Percent;Segments;Words;Placeables;Characters\n'+
            'S1512_InfConsent_091416.docx | en_us>es_us;0;4.86;231;2893;0;49.74;1;10;0;0.17;101;1301;0;22.37;23;239;0;4.11;5;86;0;1.48;7;130;0;2.24;22;336;0;5.78;38;821;0;14.12;428;5816;0;28279\n'+
            'S1602_InfConsent_92116.docx | en_us>es_us;0;4.85;108;1599;0;27.13;3;27;0;0.46;113;1452;0;24.64;42;391;0;6.63;12;241;0;4.09;4;51;0;0.87;43;645;0;10.95;82;1487;0;25.23;407;5893;0;28571';
        uploadFile1.successFileupload();
        Test.stopTest();
    }
    /**
    * This method will create dummy credentials for testing 
    **/
    private static String createTestCredentials(){
        AWSKey__c testKey = new AWSKey__c(name='test keys',key__c='key',secret__c='secret');
        insert testKey;
        return testKey.name;
     } 
    /**
    * This method run amazon's signature's and policy tester
    **/
    static testmethod void policyTest() {
            GLSSetupData.createQLI();
            Test.startTest();
            ApexPages.currentPage().getParameters().put('quoteId',GLSSetupData.quote.Id);
            GLSQW_UploadCSVFilesController uploadFile = new GLSQW_UploadCSVFilesController(new ApexPages.StandardController(GLSSetupData.quote));
            String credName = createTestCredentials();
            uploadFile.AWSCredentialName = credName;
            uploadFile.getawss3LoginCredentials();
            try{
                uploadFile.getHexPolicy( );
                uploadFile.getSignedPolicy();
            }catch(Exception ex){
            }
           system.assert( uploadFile.getHexPolicy() != null );
           system.assert( uploadFile.getSignedPolicy() != null );
           Test.stopTest(); 
     }
}