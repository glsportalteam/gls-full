public class GLSCustomFileLookupController{
    public Quotes_File__c quoteFile {get;set;} // new Quote file to create
    public List<Quotes_File__c> results{get;set;} // search results
    public string searchString{get;set;} // search keyword 
    public string quoteId{get;set;} //associated quote id
    public String[] userOptions = new String[]{};
    
    public GLSCustomFileLookupController() {
        quoteFile = new Quotes_File__c();
        // get the current search string
        searchString = System.currentPageReference().getParameters().get('lksrch');
        quoteId = System.currentPageReference().getParameters().get('quote');
        runSearch();  
    } 
 
    // performs the keyword search
    public PageReference search() {
        runSearch();
        return null; 
    }
    // prepare the query and issue the search command
    private void runSearch() {
        // TODO prepare query string for complex serarches & prevent injections
        results = performSearch(searchString);               
    }  
 
    // run the search and return the records found. 
    private List<Quotes_File__c> performSearch(string searchString) {
        string whereClause='';
        String soql;
        List<Quotes_File__c> lstQuoteFiles;
            if(searchString != '' && searchString != null){
              whereClause = ' AND File_Name__c LIKE \'%' + searchString +'%\'';
          }
        soql = 'select id,Name,File_Name__c,Version__c from Quotes_File__c where Quote__c=\''+quoteId+'\''+ whereClause +'limit 50000';    
        lstQuoteFiles=database.query(soql);
        return lstQuoteFiles; 
     
    }
 
    // used by the visualforce page to send the link to the right dom element
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }
 
    // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    } 
    
  
}