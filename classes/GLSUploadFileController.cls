/* Copyright (c) 2013, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSUploadFileController  Class
* Function: Upload files to Amazon S3 server and create corresponding Quote file record for the uploaded file
*/

global class GLSUploadFileController {

    public string QuoteId_value{get;set;}
    public S3.AmazonS3 as3 { get; private set; }
    private boolean flag=false;
    public string currQuote;
    public string userType{get;set;}
    public string Quote_Name{get;set;}
    public static string ClientName{get;set;}
    public static string quoteName {get;set;}
    public static string clientId {get;set;}
    public string Account_Name{get;set;}
    public string newFileName{get;set;}
    public static string QFileName{get;set;}
    public static boolean forAllQFiles{get;set;}
    public Decimal fileSize {get;set;}
    public Boolean IsSPCharFile {get;set;}
    public string AmazonPath{get;set;}     
    public string VersionId{get;set;}
    public static string bucket{get;set;}
    public string QFileId{get;set;}
    public Quotes_File__c record{get;set;} 
    public AWSKeys credentials {get;set;}
    public String AWSCredentialName = 'NAME OF KEY TO USE'; //Modify this string variable to be the name of the AWS Credential record that contains the proper AWS keys and secret
    public string secret { get {return credentials.secret;} }
    public string key { get {return credentials.key;} }
    public List<string>lstQuoteFileName {get;set;}
    public list<Quotes_File__c> lstQuoteFile;
    public List<File_Format__c> ext;
    public static List<Trados_Configuration__c> lstTradosConf = new List<Trados_Configuration__c>();
    public Boolean disableBtn {get; set;}
       /**
        *   @Description : Constructor
        *       
        **/ 
         public GLSUploadFileController(ApexPages.StandardController controller) {
           lstQuoteFile= new list<Quotes_File__c>();
           lstQuoteFileName= new list<String>();
           getawss3LoginCredentials();
           IsSPCharFile = false;
           forAllQFiles = false;
           QFileId = '';
           disableBtn = true;
           QuoteId_value = ApexPages.currentPage().getParameters().get('id');
           if(QuoteId_value.length()!=15)
                QuoteId_value=QuoteId_value.subString(0,15);
            
            Quote__c currQuote=[SELECT id,Name,Quote_Number__c, Client__r.Name,Bucket_Name__c, Status__c FROM Quote__c WHERE id=:QuoteId_value limit 1];
            if(GLSConfig.btnEnableStatus.contains(currQuote.Status__c)){
	        		disableBtn = false;
        	}
            lstQuoteFile=[select id,File_Name__c from Quotes_File__c where Quote__c =:QuoteId_value];
            
            if(lstQuoteFile.size()>0){
                for(Quotes_File__c qf:lstQuoteFile){
                    string fileName = qf.File_Name__c;
                    if(fileName.contains('\'')){
                        fileName = fileName.replaceAll('\'', '_');
                    }
                lstQuoteFileName.add(fileName);
                }
            }
         //  Quote_Name=currQuote.Name;
           Quote_Name = currQuote.Quote_Number__c;
           ClientName = EncodingUtil.urlEncode(currQuote.Client__r.Name, 'UTF-8');
           clientId   = currQuote.Client__c;
           bucket     = currQuote.Bucket_Name__c;
           quoteName  = currQuote.Quote_Number__c+'_'+QuoteId_value;
           
           this.record = new Quotes_File__c(); 
           User u=[SELECT Id,UserType,Contact.Account.Name  FROM User WHERE Id = :userInfo.getUserId()limit 1];
           userType=u.UserType;
           if(u.UserType=='Standard'){
              Account_Name=currQuote.Client__r.Name;
           }else if(u.UserType=='PowerPartner'){
              Account_Name=u.Contact.Account.Name;
           }
        }
       /**
        *   @Description : This method sets Aws s3 credentials
        *   @Param : none
        **/
        
    public PageReference getawss3LoginCredentials(){
    
        try{
            credentials = new AWSKeys(AWSCredentialName);
            as3 = new S3.AmazonS3(credentials.key,credentials.secret);
         }catch(AWSKeys.AWSKeysException AWSEx){
            System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
            ApexPages.addMessage(errorMsg);
            //throw new AWSKeys.AWSKeysException(AWSEx);
            //ApexPages.addMessage(AWSEx);    
        } 
       return null;
    }
    
    Datetime expire = system.now().addDays(1);
    String formattedexpire = expire.formatGmt('yyyy-MM-dd')+'T'+
    expire.formatGmt('HH:mm:ss')+'.'+expire.formatGMT('SSS')+'Z';           
    string policy { get {return 
                '{ "expiration": "'+formattedexpire+'","conditions": [ {"bucket": "'+
        bucket +'" } ,{ "acl": "'+
       'private'+'" },'+
       '{"success_action_status": "201" },'+
       '["starts-with", "$key", ""] ]}';       } }  
 
       /**
        *   @Description : This method returns s3 policies
        *   @Param : none
        **/
    
        public String getPolicy() {
            return EncodingUtil.base64Encode(Blob.valueOf(policy));
        }
    
       /**
        *   @Description : This method returns signature
        *   @Param : none
        **/
    
        public String getSignedPolicy() {    
            return make_sig(EncodingUtil.base64Encode(Blob.valueOf(policy)));        
        }
    
       /**
        *   @Description : This method returns encoded policy 
        *   @Param : none
        **/
    
        public String getHexPolicy() {
            String p = getPolicy();
            return EncodingUtil.convertToHex(Blob.valueOf(p));
        }
    
       /**
        *   @Description : This method returns encrypted signature.
        *   @Param : String Canonical Buffer
        **/
    
        private String make_sig(string canonicalBuffer) {        
            String macUrl ;
            String signingKey = EncodingUtil.base64Encode(Blob.valueOf(secret));
            Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof(Secret)); 
            macUrl = EncodingUtil.base64Encode(mac);                
            return macUrl;
        } 
    
       /**
        *   @Description : This method saves Quote File Record
        *   @Param : none
        **/
          
        public void successFileupload() {
            system.debug('---inside successFileupload---: '+newFileName);
            if(QuoteId_value!=null)
            {     
               string extension;
               string transactionId = '';
               ext= new List<File_Format__c>();
               if(newFileName.lastIndexOf('.')!= -1){
                    extension=newFileName.substring(newFileName.lastIndexOf('.'),newFileName.length());
                    ext=[select id,File_Extension__c,Name from File_Format__c where File_Extension__c INCLUDES(:extension) limit 1];
                    Quotes_File__c objRecord= new Quotes_File__c();
                    objRecord.Quote__c = QuoteId_value;
                    objRecord.File_Name__c = newFileName;
                    if(bucket != null)
                        objRecord.Bucket_Name__c = bucket;
                    else
                        objRecord.Bucket_Name__c = GLSConfig.bucketName;
                    fileSize = (fileSize/1024);
                    objRecord.File_Size__c = fileSize;
                    objRecord.isSPCharFile__c = IsSPCharFile;
                    
                    if(ext.size() > 0 ){
                        objRecord.File_Format__c=ext[0].id;
                    }
                    if(AmazonPath.length() > 255)
                         AmazonPath = AmazonPath.substring(0,255);
                    objRecord.Amazon_S3_source_file_path__c = AmazonPath;
                    decimal maxversion=0;
                    List <Quotes_File__c> File_list = new List <Quotes_File__c>();
                    
                    try{
                        File_list= [SELECT Id, Version__c FROM Quotes_File__c WHERE File_Name__c = :newFileName and Quote__c = :QuoteId_value];    
                    }catch(QueryException de){
                        System.debug('Query Exception occured.');
                    }catch(Exception e){
                        System.debug('Exception occured.');
                    }
                    if(File_list != null && File_list.size()>0){
                        for (Quotes_File__c temp : File_list) {
                            if(maxversion<temp.Version__c)
                                  maxversion=temp.Version__c;
                                  objRecord.Id= temp.Id; 
                        }
                    }else
                        objRecord.Translatable__c = 'Error';
                    objRecord.Version__c=maxversion+1;
                    objRecord.version_id__c=VersionId;
                   
                    try{
                        upsert(objRecord);
                        QFileId = objRecord.Id;
                        flag = true;
                     }catch(DmlException de){
                        flag = false;
                        QFileId = '';
                        System.debug('DML Exception occured.');
                     }catch(Exception e){
                        flag = false;
                        QFileId = '';
                        System.debug('Exception occured.');
                     }
                  } 
               }
            }
            
            WebService static string recallGEFAPI(String FileId){
                if(FileId != null){
                    try{
                        QFileName = FileId;
                        forAllQFiles = true;
                        callGetExternalFileWS();
                    }catch(Exception e){
                        system.debug(GLSConfig.ExceptionOccured+'-'+e);
                    }
                }
                return null;
            }
            
            
            public static void callGetExternalFileWS(){
                system.debug('---inside callGetExternalFileWS---: '+QFileName);
                    GLS_API_Logger__c glsApiLogger= new GLS_API_Logger__c();
                     RecordType recordType=[Select  SobjectType, Name,Id,DeveloperName,Description From RecordType  where  SObjectType='GLS_API_Logger__c' and DeveloperName='Quote_File_Logger_Record_Type'];
                    lstTradosConf = Trados_Configuration__c.getall().values();
                    if(lstTradosConf != null && lstTradosConf.size()>0){
                         List<Quotes_File__c> qFile = new List<Quotes_File__c>();
                         try{
                            system.debug('---forAllQFiles---: '+forAllQFiles);
                            if(forAllQFiles){
                                system.debug('---inside 1');
                                qFile = [Select Id, Quote__c, File_Name__c, version_id__c, TradosAPIFileID__c, Quote__r.Client__c,
                                         Quote__r.Client__r.Name, Quote__r.Quote_Number__c, Quote__r.Bucket_Name__c
                                         From Quotes_File__c 
                                         Where Id=: QFileName And (TradosAPIFileID__c = '' OR TradosAPIFileID__c = null)
                                         limit 1];
                            }else{
                                system.debug('---inside 2');
                                qFile = [Select Id, Quote__c, File_Name__c, version_id__c, TradosAPIFileID__c, Quote__r.Client__c,
                                         Quote__r.Client__r.Name, Quote__r.Quote_Number__c, Quote__r.Bucket_Name__c
                                         From Quotes_File__c 
                                         Where Id=: QFileName And (TradosAPITransactionID__c = '' OR TradosAPITransactionID__c = null)
                                         limit 1];
                            }
                         }catch(QueryException de){
                            System.debug('Query Exception occured. '+de);
                        }
                        system.debug('----qFile----: '+qFile);
                        if(qFile != null && qFile.size()>0){
                            String transactionId = '';
                            clientName = '';
                            clientId = '';
                            if(qFile[0].Quote__r.Client__r.Name != null)
                                clientName = EncodingUtil.urlEncode(qFile[0].Quote__r.Client__r.Name, 'UTF-8');
                            String qId = qFile[0].Quote__c;
                            if(qId.length()!=15)
                                qId=qId.subString(0,15);
                            quoteName = qFile[0].Quote__r.Quote_Number__c+'_'+qId;
                            clientId = qFile[0].Quote__r.Client__c;
                            bucket =  qFile[0].Quote__r.Bucket_Name__c;
                            String quotePath = clientName+'/'+quoteName;
                            String fileName = EncodingUtil.urlEncode(qFile[0].File_Name__c, 'UTF-8');
                            String fileVersionId = qFile[0].version_id__c;
                            String sfFileIds = qFile[0].Id;
                            String secure ='';
                            glsApiLogger.Quote_Number__c=qFile[0].Quote__c;
                            glsApiLogger.Quote_File_Number__c=qFile[0].Id;
                            glsApiLogger.RecordTypeId  =recordType.Id; 
                            glsApiLogger.API_Call_Type__c='GetExternalFile';
                            Quotes_File__c qFileUpdate = new Quotes_File__c(Id =qFile[0].Id);
                            HttpResponse resL;
                            
                            if(lstTradosConf[0].isSecure__c)
                             secure = 'https://';
                            else
                             secure = 'http://';
                            Integer UserId = Integer.valueOf(lstTradosConf[0].User_Id__c);
                            if(clientName != '' && clientId != '' && quoteName != '' && bucket != ''){
                                String tradosEndpointURl = secure+lstTradosConf[0].DNS_Name__c + GLSconfig.getExternalFileAPICall+'?'+ GLSconfig.paramSecretKey + lstTradosConf[0].Secret_Key__c + 
                                                   GLSconfig.paramUserId + UserId + GLSconfig.paramBucketName + bucket + GLSconfig.ParamoutputPath + quotePath + 
                                                   GLSconfig.ParamSfFileIds + sfFileIds + GLSconfig.paramFileName + fileName + GLSconfig.paramVersionId + fileVersionId +
                                                   GLSconfig.paramSource + lstTradosConf[0].IP_Address__c + GLSconfig.paramClientId + clientId; 
                                glsApiLogger.API_Rest_URL__c=tradosEndpointURl;
                                glsApiLogger.URL_Parameter__c=parameterKeyValuePair(tradosEndpointURl);
                                glsApiLogger.Timestamp__c=system.now();
                                resL = GLSQLICreator.getResponse(tradosEndpointURl); 
                                system.debug('---resL---: '+resL);
                                if(resL != null){
                                    String httpResponseStatusCode = String.valueOf(resL.getStatusCode());
                                    if(httpResponseStatusCode == GLSconfig.successCode){
                                        Dom.XmlNode xmlNode = resL.getBodyDocument().getRootElement();
                                        if(xmlNode != null && xmlNode.getText() != ''){
                                            transactionId = xmlNode.getText();
                                            qFileUpdate.Translatable__c = 'Still Processing';
                                        }
                                        if(transactionId == null || transactionId == ''){
                                            transactionId = '0';
                                            qFileUpdate.Translatable__c = 'Error';
                                            qFileUpdate.Error_Message__c = GLSConfig.TransactionIdNotFound;
                                        } 
                                        glsApiLogger.Status__c='Success';
                                    }else{
                                         Dom.XmlNode errxmlNode = resL.getBodyDocument().getRootElement();
                                         if(errxmlNode != null && errxmlNode.getText() != ''){
                                           transactionId = '0';
                                           qFileUpdate.Translatable__c = 'Error';
                                           qFileUpdate.Error_Message__c = errxmlNode.getText();
                                           glsApiLogger.Error_Message__c=errxmlNode.getText();
                                         }
                                         glsApiLogger.Status__c='Error';
                                    } 
                                    glsApiLogger.Response_of_API_call__c=transactionId;                             
                                }else{
                                    transactionId = '0';
                                    qFileUpdate.Translatable__c = 'Error';
                                    qFileUpdate.Error_Message__c = GLSConfig.ResponseNotRecieved;
                                    glsApiLogger.Error_Message__c=GLSConfig.ResponseNotRecieved;
                                    glsApiLogger.Status__c='Error';
                                }
                                qFileUpdate.TradosAPITransactionID__c = transactionId;
                                try{
                                    update qFileUpdate;
                                }catch(DMLException de){
                                    system.debug(GLSConfig.DMLExceptionOccured+'-'+de);
                                }
                                try{
                                   insert glsApiLogger;
                                }catch(DMLException de){
                                    system.debug(GLSConfig.DMLExceptionOccured+'-'+de);
                                }catch(Exception ex){
                                
                                }
                            }
                        }
                    }
            }
           
           /* 
            public static void callGetExternalFile(){
                    system.debug('---callGetExternalFile---: ');
                    String cName = '';
                    String cId = '';
                    String bName = '';
                    String qName  = '';
                    String qId = '';
                    if(rfqId != null){
                        Quote__c currQuote=[select id,Name,Quote_Number__c, Client__r.Name,Bucket_Name__c from Quote__c where id=:rfqId limit 1];
                        qId = rfqId;
                         if(qId.length()!=15)
                                qId=qId.subString(0,15);
                        if(currQuote != null){
                            if(currQuote.Client__r.Name !=null)
                                cName = currQuote.Client__r.Name;
                            cId   = currQuote.Client__c;
                            bName = currQuote.Bucket_Name__c;
                            qName  = currQuote.Quote_Number__c+'_'+qId;
                        }
                        system.debug('----cName----: '+cName);
                        system.debug('----cId----: '+cId);
                        system.debug('----qName----: '+qName);
                        system.debug('----bName----: '+bName);
                        if(cName != '' && cId != '' && qName != '' && bName != ''){
                            try{
                               String transactionId = GLSQLICreator.gls_callGetExternalFileWS(qId, cName, qName, bName, cId, 'QuoteEditPage');
                            }catch(Exception e){
                                system.debug('---GLS API Exception---: '+e);
                            } 
                        }
                  }
            }*/
            
            private static string parameterKeyValuePair(string tradosEndpointURl){
            
            string parameter='';
             parameter+= 'secretKey'+tradosEndpointURl.substringBetween('secretKey','&')+'\n';
             parameter+= 'userID'+tradosEndpointURl.substringBetween('userID','&')+'\n';
             parameter+= 'bucketName'+tradosEndpointURl.substringBetween('bucketName','&')+'\n';
             parameter+= 'outputPath'+tradosEndpointURl.substringBetween('outputPath','&')+'\n';
             parameter+= 'SalesforceFileIds'+tradosEndpointURl.substringBetween('SalesforceFileIds','&')+'\n';
             parameter+= 'fileNames'+tradosEndpointURl.substringBetween('fileNames','&')+'\n';
             parameter+= 'versionIds'+tradosEndpointURl.substringBetween('versionIds','&')+'\n';
             parameter+= 'source'+tradosEndpointURl.substringBetween('source','&')+'\n';
             parameter+= 'clientId'+tradosEndpointURl.substringAfter('clientId');

             return parameter;
            
        }
}