/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSCommunity_PreviewPageCtrlExtTest {

    static testMethod void myUnitTest() {
         Account accObj 				= GLSLPW_SetUpData.createAccount('Amgen');
         Contact conObj 				= GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
     Quote__c quoteObj  				= GLSLPW_SetUpData.createQuote(accObj.Id,conObj.Id,'');
     Quote_Assignment__c qaObj			= GLSLPW_SetUpData.createQuoteAssignment(quoteObj.Id);
     Quotes_File__c qfObj				= GLSLPW_SetUpData.createQuoteFile(quoteObj.Id);
     Quote_Assignment_File__c qaf 		= GLSLPW_SetUpData.createQuoteAssignmentFile(qfObj.Id);
     Quote_Assignment_Service__c qas	= GLSLPW_SetUpData.createQuoteAssignmentService(qaObj.ID, 'Translation');
     Quote_Assignment_Language__c qal	= GLSLPW_SetUpData.createQuoteAssignmentLanguage(qaObj.Id, 'Test Language');
     
     Test.startTest();
     		ApexPages.currentPage().getParameters().put('qaId', qaObj.Id);
     		ApexPages.currentPage().getParameters().put('qId', quoteObj.Id);
			GLSCommunity_PreviewPageControllerExt obj = new GLSCommunity_PreviewPageControllerExt(new ApexPages.StandardController(quoteObj));
	 Test.stopTest();		
    }
}