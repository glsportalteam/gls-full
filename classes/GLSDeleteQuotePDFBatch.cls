/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSDeleteQuotePDFBatch  Class
* Function: This batch apex is used to delete the Quote PDF meta data record that do not have a corresponding AWS File record.
*/
global class GLSDeleteQuotePDFBatch implements Database.Batchable<sObject>, Database.Stateful{
	
	global String query;
	global Integer successCount = 0;
	global Integer failureCount = 0;
	global DateTime startTime = System.now();
	
	//Constructor
	global GLSDeleteQuotePDFBatch(){
	}
	
	//Start method of Batch context that decides the scope of batch class by returning the query.
	global Database.QueryLocator start(Database.BatchableContext startBatchCtx){
		query = 'SELECT Id FROM Quote_PDF_File__c WHERE AWS_File_Id__c = \'\' LIMIT 200';
    	return Database.getQueryLocator(query);
	}
	 
    //Execute method of batch class which deletes the records from Recycle Bin as well. 
	global void execute(Database.BatchableContext executeBatchCtx, List<sObject> scope){
		GLSconfig.skipQuotePDFFileTrigger = false;
		startTime = System.now();
		Database.DeleteResult[] drList = Database.delete(scope, false);
		for(Database.DeleteResult dr : drList){
			if(dr.isSuccess()){
				successCount++;
			}
		}
		failureCount = scope.size() - successCount;
		DataBase.emptyRecycleBin(scope); 
	}
	
	//Finish method which executes the batch class to delete Quote File records.
	global void finish(Database.BatchableContext finishBatchCtx){
		AsyncApexJob asyncObj =[SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email, CompletedDate
						FROM AsyncApexJob 
						WHERE Id =:finishBatchCtx.getJobId()];
		Long startSeconds = startTime.getTime(); 
		Long completedSeconds = asyncObj.CompletedDate.getTime();
		Long seconds = (completedSeconds - startSeconds)/1000;
		GLSconfig.sendEmailBatchTracking('GLSDeleteQuotePDFBatch', asyncObj.JobItemsProcessed, asyncObj.TotalJobItems, asyncObj.NumberOfErrors, 
										successCount, failureCount, asyncObj.Status, asyncObj.CompletedDate, seconds, asyncObj.CreatedBy.Email);
		Datetime sysTime = System.now().addDays(1);
		String chronExpression = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + 
									sysTime.month() + ' ? ' + sysTime.year();
		if(!Test.isRunningTest()){ 
		    System.schedule( 'Record Clean Up ' + sysTime, chronExpression, new GLSDeleteQuotePDFScheduler());
		}
	}
}