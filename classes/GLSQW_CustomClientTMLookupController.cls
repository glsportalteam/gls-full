public class GLSQW_CustomClientTMLookupController {
public List <Client_TM__c > lstClientTm {get;set;}
    public set <id> quoteiIds= new set<id>();
    public string sourcelangaugeval{get;set;}
    public string targetlangaugeval{get;set;}
    public string quoteId1;
    public string quoteId2;
    public boolean renderVariable{get;set;}
     public  GLSQW_CustomClientTMLookupController(){
         quoteId1 = System.currentPageReference().getParameters().get('clientid');
         system.debug('line8'+quoteId1);
         quoteId2 = System.currentPageReference().getParameters().get('lksrch');
         renderVariable=false;
        
     }
    /** method for custom lookup in GLSQW_NewQuotePage **/
     public void openCustomLookup(){
       //quoteId1='a0qZ0000002zKFN';
        system.debug('line536'+quoteId1);
    
        if(quoteId1 != null && quoteId1 !=''){
            
            
            for(    Quote__c q1 : [select id,Client__c from Quote__c where id=:quoteId1]){
                    if(q1.Client__c !=null)
                    quoteiIds.add(q1.Client__c );
            }
        }
        
       // system.debug('line540'+ quoteiId1);
        if(quoteiIds != null && quoteiIds.size()>0){
            
            for (Account acc : [select id,Parentid from Account where id in: quoteiIds]){
                    if(acc.ParentId != null)
                    quoteiIds.add(acc.ParentId);
                
            }
            
        }
        system.debug('line 550'+quoteiIds);
        if(quoteiIds != null && quoteiIds.size()>0){    
            lstClientTm = New List<Client_TM__c>();
            if( quoteId2 != null &&  quoteId2 !=''){
                lstClientTm = [select id,Client_Name__r.name ,  Name ,Source_Language_ID__r.name,Target_Language_ID__r.name ,   TM_Name__c from Client_TM__c where  Name =: quoteId2 and IsActive__c=true order by name ];
                
            }
            else
            lstClientTm = [select id,Client_Name__r.name ,  Name ,Source_Language_ID__r.name,Target_Language_ID__r.name ,   TM_Name__c from Client_TM__c where  Client_Name__c in : quoteiIds and IsActive__c=true order by name];
        }
        if(lstClientTm != null && lstClientTm.size()>0)
            renderVariable=true; 
      }
    
    public void getQueryString(){
         lstClientTm = New List<Client_TM__c>();
        string clause;
        string clause1;
        system.debug('line 565'+sourcelangaugeval + 'targetlang'+targetlangaugeval +'ids'+quoteiIds);
       
        string query = 'select id,Client_Name__r.name,Name  ,TM_Name__c,Source_Language_ID__r.name,Target_Language_ID__r.name from Client_TM__c ';
        if(sourcelangaugeval !='' && targetlangaugeval !='' ){
            system.debug('ok2');
            clause = 'where Source_Language_ID__r.name LIKE \'' + sourcelangaugeval + '%\' and  Target_Language_ID__r.name LIKE \'' + targetlangaugeval + '%\' and Client_Name__c in : quoteiIds and IsActive__c=true order by name' ;
            
        }
        else if(sourcelangaugeval !='' && sourcelangaugeval != null){
        //  clause = 'where name =' + sourcelangaugeval ;
            system.debug('ok');
        clause = ' where Source_Language_ID__r.name LIKE \'' + sourcelangaugeval + '%\' and Client_Name__c in : quoteiIds and IsActive__c=true order by name' ;
            
        }
        else if (targetlangaugeval !='' && targetlangaugeval !=null){
            system.debug('ok1');
            clause = 'where Target_Language_ID__r.name LIKE \'' + targetlangaugeval + '%\' and Client_Name__c in : quoteiIds and IsActive__c=true order by name'  ;
        }
        else{
        
            clause = 'where Client_Name__c in : quoteiIds and IsActive__c=true order by name';
        }
        //clause1= ' and Client_Name__c in : quoteiIds';
        query =query + clause ;
        system.debug('line586'+ query);
        lstClientTm = database.query(query);
        if(lstClientTm !=null && lstClientTm.size()>0){
        renderVariable=true;
        }
        else{
            renderVariable=false;
        }
        system.debug('line 586'+lstClientTm);
        
    }
    
    
 
  // used by the visualforce page to send the link to the right dom element
      public String getFormTagdetails() {
         system.debug('line80'+System.currentPageReference().getParameters().get('frm') );
    return System.currentPageReference().getParameters().get('frm');
    }
    // used by the visualforce page to send the link to the right dom element for the text box
     public String getTextBoxdetails() {
        system.debug('line86'+System.currentPageReference().getParameters().get('txt') );
    return System.currentPageReference().getParameters().get('txt');
    }
    

}