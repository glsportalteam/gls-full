/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSLPW_PCDetailControllerExt Class
* Function: This class is used as a controller extension for the GLSLPW_ProtocolCountryDetailPage
*/

public with sharing class GLSLPW_ELIDetailControllerExt {
    
    //Variable declaration for the class
    public Estimate_Line_Item__c eliObj                 {get; set;}
    public Boolean isEditMode                           {get; set;}
    public List<Estimate_Line_Country__c> lstELICtry    {get; set;}
    public Id estLineId                                 {get; set;}
    
    private Id eliId;
    
    //Constructor
    public GLSLPW_ELIDetailControllerExt(ApexPages.StandardController stdcon){
        eliId = ApexPages.currentPage().getParameters().get('id');
        estLineId = null;
        if(eliId != null){
            eliObj = [SELECT Id, Amount__c, Estimate_Id__c, Protocol_Id__c, Protocol_Id__r.Name, Estimate_Id__r.Name
                    FROM Estimate_Line_Item__c
                    WHERE Id =: eliId LIMIT 1];
        
            lstELICtry = [SELECT Id, Number_of_Sites__c, Country_Id__r.Name, Estimate_Line_Id__c, Estimate_Line_Id__r.Name
                        FROM Estimate_Line_Country__c
                        WHERE Estimate_Line_Id__c =: eliId];
        }
        
        isEditMode = false;
    }
    
    /* 
    @Description: Action method called when Save is clicked
    @Params: None
    @Return Type: Void
    */
    public void customSave(){
        try{
            update eliObj;
        }
        catch(DMLException de){
            system.debug(GLSConfig.DMLExceptionOccured + de);
        }
        catch(Exception e){
            system.debug(GLSConfig.ExceptionOccured + e);
        }
        isEditMode = false;
    }
    
    /* 
    @Description: Action method called when Edit is clicked
    @Params: None
    @Return Type: Void
    */
    public void customEdit(){
        isEditMode = true;
    }
    
    /* 
    @Description: Action method called when Cancel is clicked
    @Params: None
    @Return Type: Void
    */
    public void customCancel(){
        isEditMode =  false;
    }
    
    /* 
    @Description: Action method called when Delete is clicked
    @Params: None
    @Return Type: Void
    */
    public PageReference customDelete(){
        PageReference pRef = null;
        try{
            delete eliObj;
        }
        catch(DMLException de){
            system.debug(GLSConfig.DMLExceptionOccured + de);
        }
        catch(Exception e){
            system.debug(GLSConfig.ExceptionOccured + e);
        }
        isEditMode = false;
        pRef = new PageReference('/apex/GLSLPW_ProtocolDetailPage?id=' + eliObj.Protocol_Id__c);
        pRef.setRedirect(true);
        return pref;
    }
    
     /* 
    @Description: Method to delete the Estimate_Line_Item_Country__c record.
    @Params: None
    @Return Type: PageReference
    */
    public PageReference deleteELICtryRecord(){
        Estimate_Line_Country__c eliObj = new Estimate_Line_Country__c();
        if(estLineId != null){
            eliObj = [SELECT Id 
                        FROM Estimate_Line_Country__c 
                        WHERE Id =: estLineId LIMIT 1];     
            Integer delIndex;
            if(eliObj != null){
                try{
                    delete eliObj; 
                    for(Integer index=0; index < lstELICtry.size(); index++){
                        if(lstELICtry[index].Id == estLineId){
                            delIndex = index;
                        }
                    }
                    lstELICtry.remove(delIndex);
                }
                catch(DMLException de){
                    system.debug(GLSconfig.DMLExceptionOccured+de);
                }
                catch(Exception e){
                    system.debug(GLSconfig.ExceptionOccured+e);
                }
            }
        }
        return null;
    }
    
}