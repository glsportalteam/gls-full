/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSLPW_ELIDetailControllerExtTest {

    static testMethod void myUnitTest() {
        Account accObj                      = GLSLPW_SetUpData.createAccount('Amgen');
        Contact conObj                      = GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
        Program__c prgObj                   = GLSLPW_SetUpData.createProgram(accObj.Id,conObj.Id,'Program_Test', 'Amgen');
        Protocol__c protObj                 = GLSLPW_SetUpData.createProtocol(prgObj.Id,'Draft');
        
        Estimate__c eObj                    = GLSLPW_SetUpData.createEstimate(prgObj.Id, 'Approved');
        Estimate_Line_Item__c eliObj1       = GLSLPW_SetUpData.createELI(protObj.Id, eObj.Id);
        Country__c cObj1                    = GLSLPW_SetUpData.createCountry('Test Country1');
        Country__c cObj2                    = GLSLPW_SetUpData.createCountry('Test Country2');
        Estimate_Line_Country__c elcObj1    = GLSLPW_SetUpData.createELC(5,cObj1.Id , eliObj1.Id);
        Estimate_Line_Country__c elcObj2    = GLSLPW_SetUpData.createELC(10,cObj2.Id , eliObj1.Id);
                
        ApexPages.currentPage().getParameters().put('id', eliObj1.Id);
        Test.startTest();
            GLSLPW_ELIDetailControllerExt obj = new GLSLPW_ELIDetailControllerExt(new ApexPages.StandardController(eliObj1));
            obj.customCancel();
            
            obj.customEdit();
            obj.customSave();
            obj.estLineId = elcObj1.Id;
            obj.deleteELICtryRecord();
            obj.customDelete();
        Test.stopTest();
    }
}