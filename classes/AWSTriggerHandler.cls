/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  AWSTriggerHandler  Class
* Function: This class is the handler for AWS trigger and creates corresponding AWS file record for the uploaded file. 
            
*/

public with sharing class AWSTriggerHandler {    
    //Method to create records for AWS File Version and AWS File Sharing object.    
    public static void handleAfterInsertUpdate(List<AWS_File__c> lstAWSFiles,Boolean isUpdateTrigger,Map<Id, AWS_File__c> oldAWSFileMap){    	   	   
    	List <AWS_File_Version__c> lstAWSFversionadd = new List <AWS_File_Version__c>();
    	decimal maxversion;
    	List<AWS_File_Version__c> lsAWSFversion = new List <AWS_File_Version__c>();
    	List<String> lstAWSFId = new List <String>();
    	AWS_File__c oldFiles = new AWS_File__c(); 
    	  
	    for(AWS_File__c AWSfileId : lstAWSFiles){
	        lstAWSFId.add(AWSfileId.Id);
	    }
	    if(lstAWSFId != null && lstAWSFId.size()>0){
	        lsAWSFversion= [SELECT Id, File_Name__c, SF_Version__c FROM AWS_File_Version__c WHERE AWS_File_Id__c IN: lstAWSFId];    
	    }           
	    for(AWS_File__c AWSfile:lstAWSFiles){	
	       if(isUpdateTrigger){
	            oldFiles = oldAWSFileMap.get(AWSfile.ID);	            	       	     
	       }       
	        if(oldFiles.Version__c != AWSfile.Version__c){	        	
	            AWS_File_Version__c AWSFversion= new AWS_File_Version__c();
	            maxversion = 0;
	            AWSFversion.AWS_File_Id__c = AWSfile.Id;
	            AWSFversion.File_Name__c = AWSfile.File_Name__c;
	            AWSFversion.Bucket_Name__c = AWSfile.Bucket_Name__c;            
	            AWSFversion.Amazon_S3_source_file_path__c = AWSfile.Amazon_S3_source_file_path__c;
	            
	            if(lsAWSFversion != null && lsAWSFversion.size()>0){
	                for (AWS_File_Version__c temp : lsAWSFversion) {
	                     if(temp.File_Name__c == AWSfile.File_Name__c && (maxversion < temp.SF_Version__c))
	                         maxversion = temp.SF_Version__c;
	                }
	            }
	            AWSFversion.Comment__c = AWSfile.Comment__c;
	            AWSFversion.SF_Version__c = maxversion+1;
	            AWSFversion.S3_Version_Id__c = AWSfile.version_id__c;
	            lstAWSFversionadd.add(AWSFversion);
	        }
	    }    
	    if(lstAWSFversionadd != null && lstAWSFversionadd.size() > 0){
	        try{
	            insert lstAWSFversionadd;
	        }catch(DmlException de){
	            System.debug(GLSConfig.DMLExceptionOccured);
	        }catch(Exception e){
	        System.debug(GLSConfig.ExceptionOccured);
	        }
	    }    	    
    }
}