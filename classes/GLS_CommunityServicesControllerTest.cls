/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod 
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest

private class GLS_CommunityServicesControllerTest{
        public static Account accountObj;  
      	public static User u;
      	public static Contact contactObj;    
        public static Quote__c quote;
  		public static Quote_Assignment__c qAsst1;
     	public static Language_List_Item__c defLanguage;
    	public static Client_Service__c service1;
    	public static Client_Service__c service2;
    	public static Client_Service__c service3;
    	public static Quote_Assignment_Service__c qAssnService;     	
    
    	static testMethod void myUnitTest1() {
             	test.startTest();
             	
            	accountObj = new Account();
                accountObj.Name = 'Amgen';
                accountObj.BillingState ='Texas';
                insert accountObj;
                 
                contactObj = new Contact(); 
                contactObj.FirstName = 'Jennifer A';
                contactObj.LastName = 'Jennifer A';
                contactObj.AccountId = accountObj.Id;
                insert contactObj; 
            	Profile p = [select id from profile where name=: GLSConfig.Communityprofile];
        	/*	u = new User(alias = 'gport1', email='GLSProgram1@testGLS.com',emailencodingkey='UTF-8',ContactId=contactObj.Id,
                                lastname='Testing', languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id,
                                timezonesidkey='America/Los_Angeles', username='program1@gls.com');
        		insert u;
            */
            	u = [SELECT Id, contactId, accountId  FROM User where contactId != null AND accountid != null ][0];
            	service1 = new Client_Service__c();       
                service1.Name = 'Translation';
                service1.Is_Common__c = true;
                insert service1;
                    
                service2 = new Client_Service__c();       
                service2.Name = 'Clinician Review';
                service2.Is_Common__c = false;
                insert service2;
            
            	service3 = new Client_Service__c();       
                service3.Name = 'Cognitive Debriefing';
                service3.Is_Common__c = false;
                insert service3;
            
      			System.runAs ( new User(Id = u.id) ){
                    quote = new Quote__c();
                    quote.Quote_Name__c = 'Test Quote';
                    quote.Client__c = u.AccountId;
                    quote.Status__c='New';
                    quote.Contact__c = u.contactId;
                    insert quote;
                    
                    
                    defLanguage = new Language_List_Item__c();
                    defLanguage.Name = 'English';
                    defLanguage.Language_Name__c = 'en-US';
                    insert defLanguage;
                    
 					qAsst1 = new Quote_Assignment__c();
                    qAsst1.Quote__c = quote.Id; 
                    qAsst1.Source_Language__c = defLanguage.Id;     
                    qAsst1.Allowed_File_Formats__c = 'MS Word';
                    qAsst1.Document_Type__c = 'IFC,SomeData;';
                    qAsst1.Keywords__c='one;';
                    insert qAsst1;  
                                        
                    
                    GLS_CommunityUploadFileController obj = new GLS_CommunityUploadFileController();
                    obj.quoteObj = quote;
                    obj.quoteAssignmentObj = qAsst1;
                    //service = [select id, name from Client_Service__c limit 1];
                                 
                    GLS_CommunityServicesController.ServicesWrapper sw1 = new GLS_CommunityServicesController.ServicesWrapper();
                    GLS_CommunityServicesController.ServicesWrapper sw2 = new GLS_CommunityServicesController.ServicesWrapper(service1, true);
                    GLS_CommunityServicesController ctrlObj = new GLS_CommunityServicesController(obj);
                    ctrlObj.prepopulateServices();
                    ctrlObj.saveOnContinue();
                    
                    obj.quoteAssignmentObj = null;
					ctrlObj.prepopulateServices();  
                    
                    obj.quoteObj = null;
                    ctrlObj.saveOnContinue();
                }    
                test.stopTest();
         }
          
}