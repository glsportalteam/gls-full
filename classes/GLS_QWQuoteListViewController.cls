/**
  @Author       : Global Language solution Systems Ltd.
  @Date         : 20/12/2013
  @Description  : This Class is for generating Quote List View
*/


public class GLS_QWQuoteListViewController {
    public List<Quote__c> lstQuote {get; set;}
    public String searchQuoteStr {get; set;}
    public String accountVal {get; set;}
    public String contactVal {get; set;}
    public String StatusVal {get; set;}
    public String ownerVal {get; set;}
    public Quote__c quoteObj{get;set;}
    public boolean isAllChecked{get;set;}
    public boolean renderQuoteListView {get;set;}
    public String cloneQID {get;set;}
    public String selectedQID {get;set;}
    public Quote__c quoteObjUpdated{get;set;}
    public AWSKeys credentials {get;set;} 
    public String AWSCredentialName = 'NAME OF KEY TO USE'; //Modify this string variable to be the name of the AWS Credential record that contains the proper AWS keys and secret
    public string secret { get {return credentials.secret;} }
    public string key { get {return credentials.key;} }
    public S3.AmazonS3 as3 { get; private set; }
    public string bucket {get;set;}
    public list<Quote_PDF_File__c> quotePDFFilesList {get; set;}
    public Boolean iscloneWithFiles {get;set;}
    public Boolean iscloneWithoutFiles {get;set;}
    public transient List<ToDisplay> toDisplayList{get;set;} 
    public String selectedQuoteId{get;set;}
    public integer totalRecs {get;set;}
	public integer OffsetSize {get;set;}
	public integer LimitSize {get;set;}
	public integer totalPages {get;set;}
	public Integer selectedPage {get;set;}
	public List<SelectOption> lstRecordsPerPageOptions {get;set;}
	public string searchStr   {get;set;}
	public String sortField {get;set;}
    public String sortOrder {get;set;}
    public list<selectOption> lstprojectDUeDateFilter  {get;set;}
    public string filterDateRange          {get;set;}
    public string createdDateRangeFilter   {get;set;}
    public Set<Id> setAssignedToUserId     {get;set;}
    public list<user> assignedToUser       {get;set;}     
    public String selectedUserId           {get;set;}
    public String startDate                {get;set;}
    public String endDate                  {get;set;}     
    public String isCustomSearch           {get;set;}
    public string pref {get; set;}
    public boolean isPublicUserExist {get;set;}
    public ApexPages.StandardSetController setCtrlInst  {get;set;}
    
    public GLS_QWQuoteListViewController(ApexPages.StandardController stdcon) {
    	system.debug('---sessiom Id---:'+ UserInfo.getSessionId());
    	getawss3LoginCredentials();
    	lstprojectDUeDateFilter = new list<SelectOption>();
    	lstprojectDUeDateFilter.add(new Selectoption('1W','Last 1 Week'));
    	lstprojectDUeDateFilter.add(new Selectoption('1M','Last 1 Month'));
    	lstprojectDUeDateFilter.add(new Selectoption('3M','Last 3 Months'));    	
    	lstRecordsPerPageOptions = new list<SelectOption>(); 
    	lstRecordsPerPageOptions.add(new Selectoption('10','10'));
    	lstRecordsPerPageOptions.add(new Selectoption('25','25'));
    	lstRecordsPerPageOptions.add(new Selectoption('50','50'));
    	lstRecordsPerPageOptions.add(new Selectoption('100','100'));
    	lstRecordsPerPageOptions.add(new Selectoption('500','500'));
    	LimitSize= 10;
    	OffsetSize = 0;
    	totalRecs = 0;        
        quoteObj = (Quote__c)stdcon.getRecord(); 
        lstQuote = new List<Quote__c>();
        isAllChecked = true;
        bucket = GLSConfig.bucketName;
        sortField = 'CreatedDate';
        sortOrder = 'Desc';
        searchQuote();
        quoteObjUpdated=new Quote__c();
        isPublicUserExist = false;
        getAssignedToUser();
        if(setAssignedToUserId!=null && setAssignedToUserId.size()>0){
        	assignedToUser = [select Id,name from user where Id IN:setAssignedToUserId];
        }
        system.debug(assignedToUser+'assignedToUser---------------------');
    }
    public void changeQuoteAssignee(){    	
    	if(quoteObjUpdated.id !=null && selectedUserId!=null && selectedUserId!=''){
    		try{
    			quoteObjUpdated.Assigned_To__c=selectedUserId;
    			update quoteObjUpdated;
    		}catch(DMLException de){
    			system.debug(de.getMessage());
    		}
    	}
    }       
    
    /**
    *   @Description : This method sets Aws s3 credentials
    *   @Param : none
    **/        
    public PageReference getawss3LoginCredentials(){    
        try{
            credentials = new AWSKeys(AWSCredentialName);
            as3 = new S3.AmazonS3(credentials.key,credentials.secret);
         }catch(AWSKeys.AWSKeysException AWSEx){
            System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
            ApexPages.addMessage(errorMsg);              
        } 
       return null;
    }

    Datetime expire = system.now().addDays(1);
    String formattedexpire = expire.formatGmt('yyyy-MM-dd')+'T'+
    expire.formatGmt('HH:mm:ss')+'.'+expire.formatGMT('SSS')+'Z';           
    string policy { get {return 
               '{ "expiration": "'+formattedexpire+'","conditions": [ {"bucket": "'+
       bucket +'" } ,{ "acl": "'+
       'private'+'" },'+
       '{"success_action_status": "201" },'+
       '["starts-with", "$key", ""] ]}';       } }  
    
    /**
    *   @Description : This method returns s3 policies
    *   @Param : none
    **/    
    public String getPolicy() {
        return EncodingUtil.base64Encode(Blob.valueOf(policy));
    }

   /**
    *   @Description : This method returns signature
    *   @Param : none
    **/    
    public String getSignedPolicy() {    
        return make_sig(EncodingUtil.base64Encode(Blob.valueOf(policy)));        
    }

   /**
    *   @Description : This method returns encoded policy 
    *   @Param : none
    **/    
    public String getHexPolicy() {
        String p = getPolicy();
        return EncodingUtil.convertToHex(Blob.valueOf(p));
    }                
    
   /**
    *   @Description : This method returns encrypted signature.
    *   @Param : String Canonical Buffer
    **/
    private String make_sig(string canonicalBuffer) {        
        String macUrl ;
        String signingKey = EncodingUtil.base64Encode(Blob.valueOf(secret));
        Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof(Secret)); 
        macUrl = EncodingUtil.base64Encode(mac);                
        return macUrl;
    } 
    
    /* 
	@Description: Method to download the most recent copy of Quote PDF from Amazon S3.
	@Params: None
	@Return Type: PageReference
	*/
    public void fecthQuotePDFFile(){
    	String stMessage = '';
        String statusMessage ='';
        pref = '';
        if(selectedQID!=null && selectedQID!=''){
            Quote__c currentQuote = [SELECT Id, Status__c
        							 FROM Quote__c
        							 WHERE Id =: selectedQID LIMIT 1];
            quotePDFFilesList = [SELECT Id, AWSFilePath__c, File_Name__c 
								FROM Quote_PDF_File__c 
								WHERE Quote_ID__c =: selectedQID 
								AND AWS_File_Id__c != ''
								ORDER BY LastModifiedDate DESC];          
            if(quotePDFFilesList != null && quotePDFFilesList.size()>0){
                Quote_PDF_File__c downloadFile = quotePDFFilesList[0];
                String path = downloadFile.AWSFilePath__c + downloadFile.File_Name__c;                
                if(path != null){                                 
                    statusMessage = GLSAmazonUtility.downloadQuoteFile(path,GLSConfig.bucketName); 
                    try{
                        if(statusMessage != null){
                        	list<string> urlParams = statusMessage.split('Signature=');
                        	if(urlParams != null && urlParams.size() > 1){                        	
	                        	String signature = EncodingUtil.urlDecode(urlParams[1],'UTF-8');   
	                        	String sigEncoded;
	                        	/*To overcome the error of "Signature Does Not Match" when the generated signature contains space which is in 
	                        	turn replaced by "+" */
	                        	if(signature.contains('+')){
	                        		sigEncoded = signature.replace('+','%2B');
	                        	}    
	                        	else{
	                        		sigEncoded = signature;
	                        	}    
	                            pref = urlParams[0] + 'Signature=' + sigEncoded;
                        	}
                        }                                                                                                                        
                    }
                    catch(Exception e){                    
                        stMessage = e.getMessage();                    
                    }
                }        
            } 
            else{
            	if(GLSconfig.btnEnableStatus.contains(currentQuote.Status__c)){
               		pref = System.URL.getSalesforceBaseUrl().toExternalForm() + '/apex/GLSGenerateUploadQuotePDF?id=' + selectedQID;
               	}
               	else{
               	}
        	}
        }
        system.debug('-------pref-------'+pref);
    }               
    public void searchInQuote(){    	
    	List<Quote__c> lstQuote;    	    		
    	if(searchStr != null && searchStr.length() > 0){
	    	String searchString='%'+searchStr+'%';
	    	string query = 'Select Id, Name, Quote_Number__c, Client__c, Client__r.Name, Contact__c,Contact_Hidden__c, Contact__r.Name, CreatedDate,Status__c, Owner.Name, Quote_Name__c, Quote_Due_Date__c,Project_Due_Date__c,Assigned_To__r.Name From Quote__c';
	    	string whereClause = ' where (Quote_Number__c LIKE:searchString or Status__c LIKE:searchString or Contact__r.Name LIKE:searchString or Assigned_To__r.Name Like:searchString or Client__r.Name LIKE:searchString) LIMIT 1000';	    	
	    	query = query + whereClause;	    		    	
	    	lstQuote = Database.query(query);	    	        
    	}                                    	    	
    	if(lstQuote != null && lstQuote.size() > 0){    	
	    	toDisplayList = new List<ToDisplay>();        
	        try{    		        	        	
	        	for(Integer i=0; i < lstQuote.size(); i++){
	        		toDisplayList.add(
	            	new toDisplay( lstQuote[i].Id,lstQuote[i].Quote_Number__c,lstQuote[i].Createddate,lstQuote[i].Status__c,
	                           lstQuote[i].Client__r.Name, lstQuote[i].Client__c, lstQuote[i].Contact__r.Name,lstQuote[i].Contact__c,
	                           lstQuote[i].Project_Due_Date__c, lstQuote[i].owner.Name, lstQuote[i].Assigned_To__r.Name));
	                            
	       		}    	    	
	    	}catch(Exception e){
	    		system.debug('Exception occured'+e);
	    	}	    	
	    } 
    }         
    public String createOrderClause(){
    	String orderClause='';
    	if(sortField != null && sortField != ''){
    		orderClause = orderClause +' order by '+sortField;
    		if(sortOrder == 'Desc'){
    			orderClause=orderClause +' '+sortOrder;	
    		}  
    		orderClause=orderClause + ' NULLS LAST';
    	}
    	system.debug(orderClause+'orderClause-----------------------------');
    	return orderClause;
    }    
    public pageReference searchQuote() { 
    	system.debug(accountVal+'----'+contactVal+'------'+OwnerVal+'----'+startDate+'---------'+statusVal);
        DateTime startDateTimeObj;
        DateTime endDateTimeObj;
        string query = 'Select Id, Name, Quote_Number__c, Client__c, Client__r.Name, Contact__c,Contact_Hidden__c, Contact__r.Name, CreatedDate,Status__c, Owner.Name, Quote_Name__c, Quote_Due_Date__c,Project_Due_Date__c, Assigned_To__r.Name From Quote__c';
        string countRemainingLimit = ' limit 40000';
        string queryCount = 'Select count() From Quote__c ';
        string whereClause = '';
        //String orderby = ' order by Createddate desc';        
        String orderby = createOrderClause();
        string expiredStatus='Expired';
        if(isAllChecked){
            whereClause =' Where Status__c !=: expiredStatus';
        }else
        if (accountVal == '' && contactVal  == '' && statusVal == '' && OwnerVal == '' && (startDate == '' || endDate=='')) {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.INFO, GLSConfig.noFilterCriteriaSelected));
            renderQuoteListView = true;
            return null;
        }else if(accountVal != '' && contactVal  != '' && statusVal != '' && OwnerVal != '') {        	
            whereClause = ' Where Client__r.Name =:accountVal And Contact__r.Name =:contactVal And Status__c =: statusVal And (Assigned_To__r.Name =:ownerVal)';
        }else if(accountVal != '' && contactVal  == '' && statusVal == '' && OwnerVal == '') {
            whereClause = ' Where Client__r.Name =:accountVal and Status__c !=: expiredStatus';
        }else if(accountVal == '' && contactVal  != '' && statusVal == '' && OwnerVal == '') {
            whereClause = ' Where Contact__r.Name =:contactVal and Status__c !=: expiredStatus';
        }else if(accountVal == '' && contactVal  == '' && statusVal != '' && OwnerVal == '') {
            whereClause = ' Where status__c =:statusVal';
        }else if(accountVal == '' && contactVal  == '' && statusVal == '' && OwnerVal != '') {
            whereClause = ' Where (owner.Name =:ownerVal Or Assigned_To__r.Name =:ownerVal) and Status__c !=: expiredStatus';
        }else if(accountVal != '' && contactVal  != '' && statusVal == '' && OwnerVal == '') {
            whereClause = ' Where Client__r.Name =:accountVal And Contact__r.Name =:contactVal and Status__c !=: expiredStatus';
        }else if(accountVal != '' && contactVal  != '' && statusVal != '' && OwnerVal == '') {
            whereClause = ' Where Client__r.Name =:accountVal And Contact__r.Name =:contactVal AND Status__c =: statusVal';
        }/*else if(accountVal != '' && contactVal  == '' && statusVal == '' && OwnerVal == '') {
            whereClause = ' Where Client__r.Name =:accountVal And Status__c =: statusVal';
        }*/else if(accountVal != '' && contactVal  != '' && statusVal == '' && OwnerVal != '') {
            whereClause = ' Where Client__r.Name =:accountVal And Contact__r.Name =:contactVal And (Assigned_To__r.Name =:ownerVal) and Status__c !=: expiredStatus';
        }else if(accountVal != '' && contactVal  == '' && statusVal != '' && OwnerVal != '') {
            whereClause = ' Where Client__r.Name =:accountVal And Status__c =: statusVal And (Assigned_To__r.Name =:ownerVal)';
        }else if(accountVal == '' && contactVal  != '' && statusVal != '' && OwnerVal != '') {
            whereClause = ' Where contact__r.Name =:contactVal And Status__c =: statusVal And (Assigned_To__r.Name =:ownerVal)';
        }else if(accountVal == '' && contactVal  == '' && statusVal != '' && OwnerVal != '') {
            whereClause = ' Where Status__c =: statusVal And (Assigned_To__r.Name =:ownerVal)';
        }else if(accountVal == '' && contactVal  != '' && statusVal != '' && OwnerVal == '') {
            whereClause = ' Where contact__r.Name =:contactVal And Status__c =: statusVal';
        }else if(accountVal != '' && contactVal  == '' && statusVal == '' && OwnerVal != '') {
            whereClause = ' Where client__r.Name =:accountVal And (Assigned_To__r.Name =:ownerVal) and Status__c !=: expiredStatus';
        }else if(accountVal == '' && contactVal  != '' && statusVal == '' && OwnerVal != '') {
            whereClause = ' Where contact__r.Name =:contactVal And (Assigned_To__r.Name =:ownerVal) and Status__c !=: expiredStatus';
        }else if(accountVal != '' && contactVal  == '' && statusVal != '' && OwnerVal == '') {
            whereClause = ' Where client__r.Name =:accountVal And Status__c =: statusVal';
        }
        if(startDate!=null && startDate!='' && enddate!=null && enddate!=''){
        	
        	Integer startDateMonth;
        	Integer startDateDay;
        	Integer startDateYear;
        	Integer endDateMonth;
        	Integer endDateDay;
        	Integer endDateYear;
        	list<String> startDateIns = startDate.split('/');
        	startDateMonth = Integer.valueOf(startDateIns[0]);
        	startDateDay = Integer.valueOf(startDateIns[1]);
        	startDateYear = Integer.valueOf(startDateIns[2]);
        	list<String> endDateIns = endDate.split('/');
        	endDateMonth = Integer.valueOf(endDateIns[0]);
        	endDateDay = Integer.valueOf(endDateIns[1]);
        	endDateYear = Integer.valueOf(endDateIns[2]);        	
        	startDateTimeObj = DateTime.newInstanceGMT(startDateYear,startDateMonth,startDateDay,0,0,0);        	
        	endDateTimeObj = DateTime.newInstanceGMT(endDateYear,endDateMonth,endDateDay,23,59,59);        
        	if(startDateTimeObj>endDateTimeObj){
        		ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.INFO, GLSConfig.inCorrectSelecteddate));
	            renderQuoteListView = true;
	            return null;
        	}	 
        	if(whereClause==''){
        		whereClause = whereClause+' Where Createddate >=: startDateTimeObj and Createddate <=: endDateTimeObj';
        	}else{
        		whereClause = whereClause+' and Createddate >=: startDateTimeObj and Createddate <=: endDateTimeObj';
        	}       	        	        	        	
        }
        if(isCustomSearch=='true'){
        	OffsetSize=0;
        	isCustomSearch='false';
        }
        toDisplayList = new List<ToDisplay>();        
        //String limitClause= ' LIMIT '+LimitSize+' OFFSET '+ OffsetSize;
        String limitClause= ' LIMIT '+LimitSize; 
        //query = query+whereClause +orderby+limitClause;
        query = query+whereClause +orderby +' LIMIT 10000';        
        system.debug(query);                
        try{
        	if(isAllChecked){
        		totalRecs = Database.countQuery(queryCount+countRemainingLimit);
        	}else{
        		totalRecs = Database.countQuery(queryCount+whereClause+countRemainingLimit);
        	}
        	system.debug(totalRecs+'totalRecs------------');
        	if(totalRecs<=10000){
	        	if(math.mod(totalRecs,LimitSize) == 0 ){
		        	totalPages=totalRecs/LimitSize;
		        }else{
		        	totalPages=(totalRecs/LimitSize)+1;
		        }
        	}else{
        		if(math.mod(totalRecs,LimitSize) == 0 ){
		        	totalPages=10000/LimitSize;
		        }else{
		        	totalPages=(10000/LimitSize);
		        }
        	}           
	        if(OffsetSize > 10000){
	        	selectedPage = (10000/LimitSize);
	        }else{
	        	selectedPage = (OffsetSize/LimitSize)+1;
	        }
	        setCtrlInst = new ApexPages.StandardSetController(Database.getQueryLocator(query));
	        setCtrlInst.setPageSize(LimitSize);
	        setCtrlInst.setPageNumber(selectedPage);
	        lstQuote = (List<Quote__c>)setCtrlInst.getRecords();
	        system.debug(lstQuote+'------------lstQuote--------------');             	   	
            //lstQuote = Database.query(query);                        
            for(Integer i=0; i<lstQuote.size(); i++){
            toDisplayList.add(
                new toDisplay( lstQuote[i].Id,lstQuote[i].Quote_Number__c,lstQuote[i].Createddate,lstQuote[i].Status__c,
                               lstQuote[i].Client__r.Name, lstQuote[i].Client__c, lstQuote[i].Contact__r.Name,lstQuote[i].Contact__c,
                               lstQuote[i].Project_Due_Date__c, lstQuote[i].owner.Name, lstQuote[i].Assigned_To__r.Name));
                                
           }                      
        }catch(QueryException qe){
            system.debug('---Query Exception--'+qe);
        }
        
        renderQuoteListView = true;
        return null;
    }         
    public void getAssignedToUser(){
            // store the results in a set so we don't get duplicates
            setAssignedToUserId = new Set<Id>();
            String userType = Schema.SObjectType.User.getKeyPrefix();              
            String grpName = GLSConfig.assignedToGrpName;          
        	// Loop through all group members in a group
        	for (GroupMember m : [Select Id, UserOrGroupId From GroupMember Where  group.name = :grpName]){
            	// If the user or group id is a user
            	if (((String)m.UserOrGroupId).startsWith(userType)){
            		isPublicUserExist = true;
                	setAssignedToUserId.add(m.UserOrGroupId);
            	}            	
        	}
    }
    public void FirstPage(){    	
		OffsetSize = 0;
		searchQuote();
	}
	
	public void previous(){		
		OffsetSize = OffsetSize - LimitSize;
		searchQuote();
	}
	
	public void next(){		
		OffsetSize = OffsetSize + LimitSize;		
		searchQuote();
	}
	
	public void LastPage(){		
		if(totalRecs<=10000){
			if(math.mod(totalRecs,LimitSize)==0){
				OffsetSize = totalrecs - LimitSize;
			}else{
				OffsetSize = totalrecs - math.mod(totalRecs,LimitSize);
			}				
		}else{
			OffsetSize = 10000 - LimitSize;
		}
		searchQuote();
	}
	
	public boolean getprev(){
		if(OffsetSize == 0)
		return true;
		else
		return false;
	}
	
	public boolean getnxt(){
		if(totalRecs<= 10000){
			if((OffsetSize + LimitSize) >= totalRecs)
			return true;
			else
			return false;
		}else{
			if((OffsetSize + LimitSize) >= 10000)
			return true;
			else
			return false;
		}		
	}
	
     public pageReference callCloneMethod() { 
        system.debug('----inside callCloneMethod------');
        list<Quote__c> lstCloneQuote = new list<Quote__c>();
        PageReference pRef ;
        if(cloneQId != null){
            try{
                if(iscloneWithFiles){
                    lstCloneQuote = GLSAmazonUtility.cloneQuote(cloneQId);
                    pRef = new PageReference('/apex/GLSQW_NewQuotePage?id='+lstCloneQuote[0].id+'&parentId='+cloneQId+'&act=clone'); 
                }
                else if(iscloneWithoutFiles){
                    lstCloneQuote = GLSAmazonUtility.cloneQuoteWithoutFiles(cloneQId);
                    pRef = new PageReference('/apex/GLSQW_NewQuotePage?id='+lstCloneQuote[0].id); 
                }
            }catch(Exception e) {
                system.debug(GLsConfig.Exceptionoccured);
            }
        }
        system.debug('---lstCloneQuote--' +lstCloneQuote);
        if(lstCloneQuote != null && lstCloneQuote.size()>0){
            pRef.setRedirect(true);
            return pRef;
        }else return null;
     }
    
     public pageReference editQuote() { 
        if(cloneQId != null ){
            PageReference pRef = new PageReference('/apex/GLSQW_NewQuotePage?id='+cloneQId);
            pRef.setRedirect(true);
            return pRef;
        }
        else return null;
     }
    
    public pagereference cancelQuote(){
        if(cloneQId != null){
            PageReference pRef = new PageReference('/apex/GLS_QWQuoteListView');    
                try{
                    Quote__c qOb = new Quote__c(id=cloneQId);
                    qOb.Status__c = 'Cancelled';
                    Database.update(qOb);
                    pRef.setRedirect(true);
                    return pRef;
                }catch(Exception e){
                    return null;    
                }
        } 
        return null;
    }
        
     public Pagereference decideRedirectPage(){
        PageReference pref = null;
        if(selectedQuoteId != null){
            List<Quote_Assignment__c> lstQA = [Select Id From Quote_Assignment__c Where Quote__c =: selectedQuoteId];
            if(lstQA != null && lstQA.size() > 0){
                pref = new Pagereference('/apex/GLSQW_QuoteOverview?id='+selectedQuoteId);
            }else{
                Quote_Assignment__c newQAssg = new Quote_Assignment__c(Quote__c = selectedQuoteId);     
                try{
                    Database.SaveResult result = Database.insert(newQAssg);
                    pref = new Pagereference('/apex/GLSQW_QuoteOverview?id='+selectedQuoteId);
                }catch(DMLException de){
                    system.debug(GLSConfig.DMLExceptionOccured+de);
                }
            }
        }
        return pref;
     }
     public class ToDisplay{
        public String quoteId{get;set;}
        public String quoteNumber{get;set;}
        public DateTime createdDate{get;set;}
        public String Status{get;set;}
        public String AccountName{get;set;}
        public String AccountId{get;set;}
        public String contactName{get;set;}
        public String contactId{get;set;}
        public Date pDueDate{get;set;}
        public String ownerName{get;set;}
        public String assignedTo{get;set;}

       
        public ToDisplay(   
                            string quoteId,
                            String quoteNumber,
                            DateTime createdDate,
                            String Status,
                            String AccountName,
                            String AccountId,
                            String contactName,
                            String contactId,
                            Date pDueDate,
                            String ownerName,
                            String assignedTo
                            ){
            this.quoteId      = quoteId      ;
            this.quoteNumber      = quoteNumber      ;
            this.createdDate      = createdDate     ;
            this.Status           = Status        ;
            this.AccountName      = AccountName       ;
            this.AccountId        = AccountId      ;
            this.contactName      = contactName   ;
            this.contactId        = contactId   ;
            this.pDueDate         = pDueDate   ;
            this.ownerName        = ownerName     ;
            this.assignedTo       = assignedTo == null ? 'None' : assignedTo;
                    
        }
    }
}