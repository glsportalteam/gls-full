/* Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSLPW_ProgramListViewControllerExt Class
* Function: This class is used as a controller extension for the GLSLPW_ProgramListView
*/

public class GLSLPW_ProgramListViewControllerExt {
    
    //Variable declaration for the class
    public String spName                    {get;set;}
    public String prgName                   {get;set;}
    public String tArea                     {get;set;}
    public String prgAccount                {get;set;}
    public List<Program__c> lstProgram      {get;set;}
    public Boolean isChecked                {get;set;}
       
    //Constructor
    public GLSLPW_ProgramListViewControllerExt(ApexPages.StandardController stdcon){
       //Initialization
        isChecked	=  true;
        lstProgram 	= [SELECT Name,Program_Name__c,Sponsor_Image__c,Account__r.name,Contact__r.name,Status__c 
        			   FROM Program__c ORDER BY CreatedDate DESC  ];
    }
    
    /* 
    @Description: Searches the record in Program and populates them into a list which is used in page 
    @Params: None
    @Return Type: PageReference
    */
    
    public PageReference searchProgram(){
        
        lstProgram 				= new List<Program__c>();
        String whereClause		='';
        String orderBy 			=' ORDER BY CreatedDate DESC LIMIT 1000';
        String query			='SELECT Name,Program_Name__c,Sponsor_Image__c,Account__r.name,Contact__r.name,Status__c FROM Program__c ';
        String searchPrg		='%'+prgName+'%';   
   		String searchSponsor	='%'+spName +'%';   
                
                if(isChecked)
                    whereClause='';
                else if(isChecked==false && prgName=='' && spName=='' && tArea=='' && prgAccount=='' )
                    { 
                      ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.INFO, GLSConfig.noFilterCriteriaSelected));
                      return null;
                    }
                else if(prgName!='' && spName=='' && tArea=='' && prgAccount=='') 
                    whereClause=' WHERE (Program_Name__c LIKE:searchPrg)';
                else if(prgName=='' && spName!='' && tArea=='' && prgAccount=='') 
                    whereClause='WHERE Sponsor_Image__c LIKE:searchSponsor';
                else if(prgName=='' && spName=='' && tArea!='' && prgAccount=='') 
                    whereClause='WHERE Therapeutic_Area__c=:tArea';
                else if(prgName=='' && spName=='' && tArea=='' && prgAccount!='') 
                   whereClause='WHERE Program__c.Account__r.name =:prgAccount';
                else if(prgName!='' && spName!='' && tArea=='' && prgAccount=='') 
                    whereClause='WHERE Program_Name__c LIKE:searchPrg AND Sponsor_Image__c LIKE:searchSponsor';
                else if(prgName!='' && spName=='' && tArea!='' && prgAccount=='') 
                    whereClause='WHERE Program_Name__c LIKE:searchPrg AND Therapeutic_Area__c=:tArea';
                else if(prgName!='' && spName=='' && tArea=='' && prgAccount!='') 
                    whereClause='WHERE Program_Name__c LIKE:searchPrg AND Program__c.Account__r.name =:prgAccount';
                else if(prgName=='' && spName!='' && tArea!='' && prgAccount=='') 
                    whereClause='WHERE Sponsor_Image__c LIKE:searchSponsor AND Therapeutic_Area__c=:tArea ';
                else if(prgName=='' && spName!='' && tArea=='' && prgAccount!='') 
                    whereClause='WHERE Sponsor_Image__c LIKE:searchSponsor AND Program__c.Account__r.name =:prgAccount';
                else if(prgName=='' && spName=='' && tArea!='' && prgAccount!='') 
                    whereClause='WHERE Therapeutic_Area__c=:tArea AND Program__c.Account__r.name =:prgAccount';
                else if(prgName!='' && spName!='' && tArea!='' && prgAccount=='') 
                    whereClause='WHERE Program_Name__c LIKE:searchPrg AND Sponsor_Image__c LIKE:searchSponsor AND Therapeutic_Area__c=:tArea' ;
                else if(prgName!='' && spName!='' && tArea=='' && prgAccount!='') 
                    whereClause='WHERE Program_Name__c LIKE:searchPrg AND Sponsor_Image__c LIKE:searchSponsor AND Program__c.Account__r.name =:prgAccount' ;
                else if(prgName!='' && spName=='' && tArea!='' && prgAccount!='') 
                    whereClause='WHERE Program_Name__c LIKE:searchPrg AND Therapeutic_Area__c=:tArea AND Program__c.Account__r.name =:prgAccount' ;
                else if(prgName=='' && spName!='' && tArea!='' && prgAccount!='') 
                    whereClause='WHERE Sponsor_Image__c LIKE:searchSponsor AND Therapeutic_Area__c=:tArea AND Program__c.Account__r.name =:prgAccount' ;
                else if(prgName!='' && spName!='' && tArea!='' && prgAccount!='') 
                    whereClause='WHERE Program_Name__c LIKE:searchPrg AND Sponsor_Image__c LIKE:searchSponsor AND Therapeutic_Area__c=:tArea AND Program__c.Account__r.name =:prgAccount';
               else
                    return null;
                
            query= query+whereClause+orderBy;
            lstProgram  = Database.query(query);
            return null;
        }
  }