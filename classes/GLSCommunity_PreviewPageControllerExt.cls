public class GLSCommunity_PreviewPageControllerExt {
		public ID qaID																	{get;set;}
		public ID qID																	{get;set;}
		public ID contactId																{get;set;}
		public String dueDate															{get;set;}
		public Integer refListSize														{get;set;}
		public Integer prevListSize														{get;set;}
		public Integer fileListSize														{get;set;}
		public Integer serviceListSize													{get;set;}
		public Integer languageListSize													{get;set;}
		public Quote__c quoteObj														{get;set;}
		public Quote_Assignment__c qaObj												{get;set;}
		public List<String> sourceFileList 							                    {get;set;}
		public List<String> refFileList 								                {get;set;}
		public List<String> previousFileList 							                {get;set;}
	    public List<Quote__c> quoteList 												{get;set;}
	    public List<Quote_Assignment__c> quoteAssignmentList 							{get;set;}
	    public List<Quote_Assignment_File__c> quoteAssignmentFileList 					{get;set;}
	    public List<Quote_Assignment_Service__c> quoteAssignmentServicesList			{get;set;}
		public List<Quote_Assignment_Language__c> quoteAssignmentLanguageList			{get;set;}
		public Boolean proceed															{get;set;}
	
	public GLSCommunity_PreviewPageControllerExt(ApexPages.StandardController stdcon){
		//public GLSCommunity_PreviewPageControllerExt(){
        qaID   = ApexPages.currentPage().getParameters().get('qaId');
        qID   = ApexPages.currentPage().getParameters().get('qId');
        contactId = ApexPages.currentPage().getParameters().get('conId');
        sourceFileList = new List<String>();
        refFileList = new List<String>();
        previousFileList = new List<String>();
        qaObj = new Quote_Assignment__c();
		if(qaID != null || qaID != ''){              
	       quoteAssignmentList 	 = [SELECT  Id,Quote__c,Delivery_Format__c,Additional_Note__c,
	       									Previous_GLS_Project_Number__c,
	       									Source_Language__r.Name
	       						    FROM Quote_Assignment__c 
	       						    WHERE Id =:qaID];
	       	qaObj = quoteAssignmentList[0];
		}
       	if(quoteAssignmentList != null && quoteAssignmentList.size() > 0){
			quoteObj =[SELECT Id,Protocol__r.Protocol_Name__c,
							   Request_Type__c,Quote_Name__c,
							   Program__c,Sponsor__c,Project_Due_Date__c,
							   Expedited__c,Notes__c,Certification_Type__c,
							   Is_Quote_requested__c,Project_Name_Community__c,
							   Quote_Name_GLS__c,Client_Reference__c
						FROM Quote__c 
						WHERE Id =:qaObj.Quote__c limit 1];  
			
			if(quoteObj!=null){
				DateTime dueDateOld = quoteObj.Project_Due_Date__c;
				dueDate = dueDateOld.format('dd/MM/yyyy');
			}
			
			quoteAssignmentServicesList = [SELECT Id,Client_Service__r.Name 
										   FROM Quote_Assignment_Service__c
										   WHERE  Quote_Assignment__r.Id =:qaID];
				serviceListSize			= quoteAssignmentServicesList.size();
										   
		   	quoteAssignmentFileList = [SELECT Id,File_Usage__c,Quote_Files__r.File_Name__c, Quote_Files__r.File_Type__c
		   							   FROM Quote_Assignment_File__c
		   							   WHERE Quote_Assignment__r.Id =:qaID];
   					 fileListSize	= 	quoteAssignmentFileList.size();	  
   			
   			quoteAssignmentLanguageList = [SELECT Id,Language_Name__c,Language_List_Item__r.Name
   										   FROM Quote_Assignment_Language__c
   										   WHERE Quote_Assignment__r.Id =:qaID];
   						languageListSize	= 	quoteAssignmentLanguageList.size();
   			
   			for(Quote_Assignment_File__c qaf:quoteAssignmentFileList){
				if(qaf.Quote_Files__r.File_Type__c =='Source'){
					sourceFileList.add(qaf.Quote_Files__r.File_Name__c);
				}
				else if(qaf.Quote_Files__r.File_Type__c =='Reference'){
					refFileList.add(qaf.Quote_Files__r.File_Name__c);
				}
				else if(qaf.Quote_Files__r.File_Type__c == 'Previous Translation' ){
					previousFileList.add(qaf.Quote_Files__r.File_Name__c);
				}
			}
			refListSize = refFileList.size();
			prevListSize = previousFileList.size();
       	}
       	
	}
	
	public PageReference redirectToProjectDetailPage(){
		PageReference pageref = Page.GLSCommunity_ProjectDetailPage;
		pageref.getParameters().put('qaId',qaId);
		pageref.getParameters().put('qid',qId);
		if(contactId != null){
			pageref.getParameters().put('conId',contactId);
		}
		pageref.setRedirect(true);
		return pageref;			
	}
	
	public PageReference redirectToServicesPage(){
		PageReference pageref = Page.GLSCommunity_ServicesPage;
		pageref.getParameters().put('qaId',qaId);
		pageref.getParameters().put('qid',qId);
		if(contactId != null){
			pageref.getParameters().put('conId',contactId);
		}
		pageref.setRedirect(true);
		return pageref;			
	}
}