/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSLPW_PurchaseAgreementCtrlExtTest {

        static testMethod void setUpTestData(){
            Account accObj = GLSLPW_SetUpData.createAccount('Amgen');
            Contact conObj = GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
            Program__c prgObj = GLSLPW_SetUpData.createProgram(accObj.Id,conObj.Id,'Program_Test', 'Amgen');
            Protocol__c protObj = GLSLPW_SetUpData.createProtocol(prgObj.Id,'Draft');
            Purchase_Agreement__c paObj = GLSLPW_SetUpData.createPurchaseAgreement(protObj.Id);
            Purchase_Agreement__c paObj2 = GLSLPW_SetUpData.createPurchaseAgreement(protObj.Id);
            AWS_File__c  awsObj = GLSLPW_SetUpData.createAWSFile('testPath','gls-us','testFile','testRecordType','testVersion',1);
            Purchase_Agreement_File__c  pafObj1 = GLSLPW_SetUpData.createPurchaseAgreementFile(paObj.Id,awsObj.Id);
            Purchase_Agreement_File__c  pafObj2 = GLSLPW_SetUpData.createPurchaseAgreementFile(paObj.Id,awsObj.Id);
            
            
        }
       
        static testMethod void testNotNullId(){
            setUpTestData();
            Protocol__c protObj1                = [SELECT Name
                                                   FROM Protocol__c LIMIT 1]; 
            Purchase_Agreement__c paObj3        = [SELECT Name 
                                                   FROM Purchase_Agreement__c LIMIT 1];
            Purchase_Agreement_File__c  pafObj3 = [SELECT id,Name 
                                                   FROM Purchase_Agreement_File__c LIMIT 1];
            Test.startTest();
                ApexPages.currentPage().getParameters().put('prtId', protObj1.Id);
                ApexPages.currentPage().getParameters().put('paId', paObj3.Id);
                GLSLPW_PurchaseAgreementControllerExt obj = new GLSLPW_PurchaseAgreementControllerExt(new ApexPages.StandardController(paObj3));
                obj.pafileId = pafObj3.id;
                system.assert(obj.paId!=Null);
                obj.savePARecord();
                obj.getPAFileRecord();
                obj.customEdit();
                obj.downloadPAFile();
                obj.deletePAFile();
            Test.stopTest();
        }
        
        static testMethod void testNullId() {
            setUpTestData();
            Protocol__c protObj2                = [SELECT Name
                                                   FROM Protocol__c LIMIT 1];
            Purchase_Agreement__c paObj4        = [SELECT Name,Expiration_Date__c, Issue_Date__c 
                                                   FROM Purchase_Agreement__c LIMIT 1];
            Purchase_Agreement_File__c  pafObj4 = [SELECT Name FROM Purchase_Agreement_File__c LIMIT 1];
            Test.startTest();
                ApexPages.currentPage().getParameters().put('prtId', protObj2.Id);
                ApexPages.currentPage().getParameters().put('paId', null);
                GLSLPW_PurchaseAgreementControllerExt obj1 = new GLSLPW_PurchaseAgreementControllerExt(new ApexPages.StandardController(paObj4));
                system.assert(obj1.paId==Null);
                obj1.savePARecord();
            Test.stopTest();
        }
}