public with sharing class submitOrderController {
	Public String quoteId  {get;set;} 
	public string qStatus  {get;set;}
	public string protocolId  {get;set;}
	public string qName  {get{
	isWarningExist=false;
		if(quoteId!=null && quoteId!=''){
			List<Quote__c> lstQuote =[select id,Status__c,Name,Protocol__r.id from quote__c where id=:quoteId limit 1];
			if(lstQuote!=null && lstQuote.size()>0){
				qStatus = lstQuote[0].Status__c;
				qName = lstQuote[0].name;
			}
			protocolId=lstQuote[0].Protocol__r.id;
			if(protocolId != null && protocolId!='' && qStatus == GLSConfig.Direct_Order){
				list<Protocol__c> lstProtocol = [select id,(select id from Purchase_Agreement__r) from Protocol__c where id=:protocolId];
				if(lstProtocol != null && lstProtocol.size() > 0){
					if(lstProtocol[0].Purchase_Agreement__r != null && lstProtocol[0].Purchase_Agreement__r.size() > 0){
						isWarningExist=false;
					}else{
						isWarningExist=true;
					}
				}
			}
		}		  
		return qName;
	}set;}
	public boolean isWarningExist{get;set;}
}