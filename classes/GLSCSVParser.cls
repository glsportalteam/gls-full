/* Copyright (c) 2016, Global Languages Solutions, Inc. All rights reserved.
* Class   :  GLSCSVParser  Class
* Function: Parse Analysis file CSV data and populate statistic data in the Quote line item record
*/
public class GLSCSVParser {
    public String parsedText {get; set;}
    public list<Quote_Line_Object_Assignment_File__c> lstQuoteAssignFile = new list<Quote_Line_Object_Assignment_File__c>();
    public set<Quote_Line_Object_Assignment_File__c> setQuoteAssignFile  = new set<Quote_Line_Object_Assignment_File__c>();
    public Quote_Line_Object_Assignment_File__c qliaf;
    public list<string> lstFileName= new list<string>();
    public integer counter=0;
    public String[] filelines = new String[]{};
    public String parser(String qliId, String quoteId, String URL, String errorMessage, String taskStatus, String csvData, String analyzedFor, Boolean isMultiLang) {
        System.debug('***'+qliId+'***'+quoteId+'***'+URL+'***'+errorMessage+'***'+taskStatus+'***'+csvData+'***'+analyzedFor+'***'+isMultiLang);
        List<Quote_Line_Object_Assignment_File__c> lstQlaf= [ Select id,Name,Source_File_name__r.File_Name__c,Quote_Line_Item__c,IsAnalysisProcessed__c 
                                                            from Quote_Line_Object_Assignment_File__c where Quote_Line_Item__c =:qliId]; 
        map<string,Quote_Line_Object_Assignment_File__c> mapTemp;
        map<String,String> mapQuoteStatus = new map<String,String>();
        List<Quote__c> lstquoteUpdate = new List<Quote__c>();
        String quoteStatus ='';
        mapTemp= new map<string,Quote_Line_Object_Assignment_File__c>();
        
        for(Quote_Line_Object_Assignment_File__c assign: lstQlaf){
            mapTemp.put(assign.Source_File_name__r.File_Name__c,assign);
        }   

        if(csvData != '') {
            parsedText = parse(csvData,qliId,URL,errorMessage,taskStatus,mapTemp,isMultiLang);
            system.debug('---parsedText----: '+parsedText);
            if(analyzedFor.equalsIgnoreCase('analyzedSingleQLI')){
            if(parsedText.equalsIgnoreCase('Success') || parsedText.equalsIgnoreCase('Error')){
                    boolean flagInProcess=false;
                    boolean flagInError=false;
                    List<Quote_Line_Item__c> lstupdatedQli = [Select Id, QuoteID__c,Process_Status__c From Quote_Line_Item__c where QuoteID__c =: quoteId];
                    for(Quote_Line_Item__c Qli: lstupdatedQli){
                        if(Qli.Process_Status__c.equalsIgnoreCase('In Process'))
                            flagInProcess = true;
                        if(Qli.Process_Status__c.equalsIgnoreCase('Error'))
                            flagInError= true;
                    }   
                    /*if(!flagInProcess){
                        Quote__c qObj= new Quote__c(id=quoteId);
                        if(flagInError)
                       qObj.Status__c = GLSConfig.InCompleteStat;
                        else
                        qObj.Status__c = GLSConfig.InPrepStat;
                        lstquoteUpdate.add(qObj);
                    }*/
                    if (lstquoteUpdate != null && lstquoteUpdate.size()>0){
                        try{
                            Database.update(lstquoteUpdate);
                        }catch (DMLException dml){
                            system.debug(GLSConfig.DMLExceptionOccured+'---'+dml.getMessage());
                        }
                        catch(Exception e)
                        {
                            system.debug(GLSConfig.ExceptionOccured+'---'+e.getMessage()+'Line Number=' +e.getLineNumber());
                        }
                    }
                }
            }
        } 
        else 
            parsedText=null;     
        system.debug('----parsedText----: '+parsedText);
        if(parsedText.equalsIgnoreCase('Success') || parsedText.equalsIgnoreCase('Error'))
            return parsedText;
        else 
            return null;
    }
    public String parse(String toParse,Id quoteId,String URL, String errorMessage, String taskStatus, map<string,Quote_Line_Object_Assignment_File__c> mapQuoteAssignment, Boolean isMultilang) {
            System.debug('###'+toParse+'###'+quoteId+'###'+URL+'###'+errorMessage+'###'+taskStatus+'###'+mapQuoteAssignment+'###'+isMultiLang);
            if(toParse != null && toParse != ''){
            try 
            {
                String Status ='';
                String wlkthrough = walkThrough(toParse,quoteId,URL,errorMessage,taskStatus,mapQuoteAssignment,isMultiLang); 
                system.debug('----wlkthrough----' +wlkthrough);
                Status = [select Process_Status__c from Quote_Line_Item__c where Id =:quoteId].Process_Status__c;
                system.debug('----Status----' +Status+'--'+quoteId);
                return Status;
            }catch (Exception e) 
            {  // invalid XML
                system.debug(GLSConfig.ExceptionOccured+'---'+e.getMessage()+'Line Number=' +e.getLineNumber());
                Quote_Line_Item__c qliErrUpdate = new Quote_Line_Item__c(id=quoteId);
                qliErrUpdate.isProcessed__c = false;
                update qliErrUpdate;
                return e.getMessage();
            }
        }
        return null;
    }
    private String walkThrough(String csvData,Id quoteId,String URL, String errorMessage, String taskStatus, map<string,Quote_Line_Object_Assignment_File__c> mapQuoteAssignment,Boolean isMultilang) {
       System.debug('+++'+csvData+'+++'+quoteId+'+++'+URL+'+++'+errorMessage+'+++'+taskStatus+'+++'+mapQuoteAssignment+'+++'+isMultiLang);
        String result = '\n';
        String fileName;
        Decimal repeated=0.00;
        Decimal exact=0.00;
        Decimal contexTM = 0;
        Decimal X95_99WordCount=0.00;
        Decimal X85_94WordCount=0.00;
        Decimal X75_84WordCount=0.00;
        Decimal X50_74WordCount=0.00;
        Decimal noMatch=0.00;
        Decimal total=0.00;
        if(csvData!='')
        {
            filelines = csvData.split('\n');
            System.debug(filelines);
            if(isMultilang == false)
            {
                counter = filelines.size() -2;
                for (Integer i=2;i<filelines.size();i++)
                {
                    String[] inputvalues = new String[]{};
                    inputvalues = filelines[i].split(',');
                   // System.debug('==='+inputvalues);
                    String[] mapFields = new String[]{};
                    mapFields = inputvalues[0].split(';');
                    //System.debug('==='+mapFields[0]);
                    String[] fileNameArr = new String[]{};
                    fileNameArr = mapFields[0].split('\\|');
                    System.debug('==='+fileNameArr[0]);
                    System.debug('==='+mapFields[4]+'==='+mapFields[4]); 
                   if(mapFields[4]!=''||mapFields[4]!=null)
                    contexTM = contexTM + Decimal.valueOf(mapFields[4]);
                    if(mapFields[8]!=''||mapFields[8]!=null) 
                    repeated = repeated + Decimal.valueOf(mapFields[8]);
                     if(mapFields[12]!=''||mapFields[12]!=null)
                    exact    = exact + Decimal.valueOf(mapFields[12]);
                    if(mapFields[16]!=''||mapFields[16]!=null)
                    X95_99WordCount = X95_99WordCount + Decimal.valueOf(mapFields[16]);
                    if(mapFields[20]!=''||mapFields[20]!=null)
                    X85_94WordCount = X85_94WordCount + Decimal.valueOf(mapFields[20]);
                    if(mapFields[24]!=''||mapFields[24]!=null)
                    X75_84WordCount = X75_84WordCount + Decimal.valueOf(mapFields[24]);
                    if(mapFields[28]!=''||mapFields[28]!=null)
                    X50_74WordCount =  X50_74WordCount + Decimal.valueOf(mapFields[28]);
                    if(mapFields[32]!=''||mapFields[32]!=null)
                    noMatch = noMatch + Decimal.valueOf(mapFields[32]);
                    if(mapFields[36]!=''||mapFields[36]!=null)
                    total = total + Decimal.valueOf(mapFields[36]);
                    
                    fileName=fileNameArr[0];
                    lstFileName.add(fileName.substring(0,fileName.lastIndexOf('.')));
                    System.debug('###==='+lstFileName);
                    if(mapQuoteAssignment.get(fileName.substring(0,fileName.lastIndexOf('.'))) != null){
                        qliaf= new Quote_Line_Object_Assignment_File__c();
                        qliaf = new Quote_Line_Object_Assignment_File__c(id = mapQuoteAssignment.get(fileName.substring(0,fileName.lastIndexOf('.'))).id);
                        qliaf.IsAnalysisProcessed__c=true;
                        lstQuoteAssignFile.add(qliaf);
                    }
                    //qliaf.IsAnalysisProcessed__c=true;
                }
            }
            else
            {
                counter = filelines.size();
                for (Integer i=0;i<filelines.size();i++)
                {
                    String[] inputvalues = new String[]{};
                    inputvalues = filelines[i].split(',');
                    System.debug('==='+inputvalues);

                    String[] mapFields = new String[]{};
                    mapFields = inputvalues[0].split(';');
                    System.debug('==='+mapFields);
                    //System.debug('==='+mapFields[0]);
                    String[] fileNameArr = new String[]{};
                    fileNameArr = mapFields[0].split('\\|');
                    System.debug('==='+fileNameArr[0]);
                    System.debug('==='+mapFields[12]+'==='+mapFields[8]); 
                    System.debug('==='+mapFields[4]+'==='+mapFields[4]); 
                    if(mapFields[8]!=''||mapFields[8]!=null) 
                    repeated = repeated + Decimal.valueOf(mapFields[8]);
                     if(mapFields[12]!=''||mapFields[12]!=null)
                    exact    = exact + Decimal.valueOf(mapFields[12]);
                    if(mapFields[4]!=''||mapFields[4]!=null)
                    contexTM = contexTM + Decimal.valueOf(mapFields[4]);
                    if(mapFields[16]!=''||mapFields[16]!=null)
                    X95_99WordCount = X95_99WordCount + Decimal.valueOf(mapFields[16]);
                    if(mapFields[20]!=''||mapFields[20]!=null)
                    X85_94WordCount = X85_94WordCount + Decimal.valueOf(mapFields[20]);
                    if(mapFields[24]!=''||mapFields[24]!=null)
                    X75_84WordCount = X75_84WordCount + Decimal.valueOf(mapFields[24]);
                    if(mapFields[28]!=''||mapFields[28]!=null)
                    X50_74WordCount =  X50_74WordCount + Decimal.valueOf(mapFields[28]);
                    if(mapFields[32]!=''||mapFields[32]!=null)
                    noMatch = noMatch + Decimal.valueOf(mapFields[32]);
                    if(mapFields[36]!=''||mapFields[36]!=null)
                    total = total + Decimal.valueOf(mapFields[36]);
                    
                    fileName=fileNameArr[0];
                    lstFileName.add(fileName.substring(0,fileName.lastIndexOf('.')));
                    System.debug('###==='+lstFileName);
                    if(mapQuoteAssignment.get(fileName.substring(0,fileName.lastIndexOf('.'))) != null){
                        qliaf= new Quote_Line_Object_Assignment_File__c();
                        qliaf = new Quote_Line_Object_Assignment_File__c(id = mapQuoteAssignment.get(fileName.substring(0,fileName.lastIndexOf('.'))).id);
                        qliaf.IsAnalysisProcessed__c=true;
                        lstQuoteAssignFile.add(qliaf);
                    }
                    //qliaf.IsAnalysisProcessed__c=true;
                }
            }
            System.debug('==='+total);
            system.debug('----counter----' +counter);
            system.debug('###===' +mapQuoteAssignment);
            Quote_Line_Item__c qliUpdate = new Quote_Line_Item__c(id=quoteId);
            if( counter != mapQuoteAssignment.size()){
                lstQuoteAssignFile= new list<Quote_Line_Object_Assignment_File__c>();
                setQuoteAssignFile= new set<Quote_Line_Object_Assignment_File__c>();
                qliUpdate.Task_Status__c='Warning';
                qliUpdate.Process_Status__c = 'Error';
                qliUpdate.Error_Message__c='Files in Analysis File does not match with the files in the Quote Line Assignment Files';
                qliUpdate.isAnalyzed__c = false;       
                for(string str:lstFileName){
                    if(mapQuoteAssignment.get(str) != null){
                        qliaf = new Quote_Line_Object_Assignment_File__c(id = mapQuoteAssignment.get(str).id);
                        qliaf.IsAnalysisProcessed__c=true;
                        //    qliaf.Quote_Line_Item__c = qliUpdate.Id;
                        setQuoteAssignFile.add(qliaf); 
                    }                       
                }
            }else{
                qliUpdate.Task_Status__c = taskStatus;
                qliUpdate.Process_Status__c = 'Success';
                qliUpdate.Error_Message__c = errorMessage;
                qliUpdate.isAnalyzed__c = true;    
            }
            System.debug('###==='+setQuoteAssignFile);
            list<Quote_Line_Item__c> lstQLI = [Select Id, Target_Language_ID__r.Name From Quote_Line_Item__c Where Id =: qliUpdate.Id limit 1];
            String targetLanguage = '';
            if(lstQLI.size()> 0)
            {
                targetLanguage = lstQLI[0].Target_Language_ID__r.Name;
                System.debug('###==='+targetLanguage);
            }
            
            if(targetLanguage.contains('English'))
            {
                Decimal totals;
                totals = total;
                qliUpdate.Actual_Words__c = Math.ceil(totals + (totals*0.15));
                qliUpdate.Back_Translation_Total_Words__c = total;
            }else
            {
                qliUpdate.Actual_Words__c=total;
                Decimal totals = total;
                qliUpdate.Back_Translation_Total_Words__c = Math.ceil(totals + (totals*0.15));
            }    
                
            qliUpdate.TWC__c = total;    
            qliUpdate.Repetitions_Word_Count__c= repeated;
            qliUpdate.X100_Word_Count__c= exact;
            qliUpdate.Context_TM_Word_Count__c = contexTM;
            qliUpdate.X95_99_Word_Count__c= X95_99WordCount;
            qliUpdate.X85_95_Word_Count__c= X85_94WordCount;
            qliUpdate.X75_84_Word_Count__c= X75_84WordCount;
            qliUpdate.X50_74_Word_Count__c= X50_74WordCount;
            //qliUpdate.File_Count__c = counter;

            qliUpdate.No_Match_Word_Count__c= noMatch;
      
            /* line 220 to 224 for solving 255 characters limit */
            if(URL.length() >= 255){
                URL = URL.substring(0, 254);
            }
            qliUpdate.Analyzed_File_URL__c = URL;
            
            if(!setQuoteAssignFile.isEmpty()) { 
                lstQuoteAssignFile.addAll(setQuoteAssignFile);  
                system.debug('----lstQuoteAssignFile----' +lstQuoteAssignFile);
                if(lstQuoteAssignFile != null && lstQuoteAssignFile.size()>0){
                    try{
                        Database.update(lstQuoteAssignFile);
                    }catch(DMLException de){
                        system.debug(GLSConfig.DMLExceptionOccured+'-'+de.getMessage());
                    }
                } 
            }
            try{
                Database.update(qliUpdate);  
                counter=0;
                lstQuoteAssignFile= new list<Quote_Line_Object_Assignment_File__c>();
            }catch(DMLException de){
                system.debug(GLSConfig.DMLExceptionOccured+'---'+de.getMessage()+'Line Number=' +de.getLineNumber());
            }catch(Exception e){
                system.debug(GLSConfig.ExceptionOccured+'---'+e.getMessage() + 'Line Number=' +e.getLineNumber());
            }
             System.debug('###==='+result);
            return result;
           
        }
        return '';  //should never reach here
    }
}