public with sharing class GLSQW_QuoteOverviewController {
  
    public Quote__c quoteRecord{get;set;} 
    public String quoteNumber{get;set;}
    public List<AssgDetails> lstAssgDetails{get;set;}
    public String assgDetailsStr{get;set;}
    public String selSpecialProvisions{get;set;} 
    public List<SelectOption> specialProvision{get;set;}
    public List<SelectOption> specialProvisionQuote{get;set;}
    public String specialProvisions{get;set;}
    public String specialProvisionsQuote{get;set;}
    public List<SelectOption> standardSpecialProv{get;set;}
    public List<SelectOption> allSourceFiles{get;set;}
    public List<SelectOption> allRefFiles{get;set;}
    List<Standard_Special_Provisions__c> lstSSP{get;set;}
    public String selSourceFile{get;set;}
    public String selRefFile{get;set;}
    public Boolean hasSourceFiles{get;set;}
    public Boolean hasRefFiles{get;set;}
    public String pages{get;set;} 
    public boolean isAssgEmpty{get;set;}
    public String statusMessage{get;set;}
    public Boolean isSPEdit {get;set;}
    public String sPToEdit {get;set;}
    public String sPToEditText {get;set;}
    public String rfqSPToEditText{get;set;}
    public String rfqSPToEdit{get;set;}
    public String assTargLanguages {get;set;}
    public String quoteIdStr;
    public Boolean disableBtn {get; set;}
 
      
    public GLSQW_QuoteOverviewController(ApexPages.StandardController controller) {
       isAssgEmpty = false;
       isSPEdit = false;
       sPToEdit = '';
       assTargLanguages = '';
       disableBtn = true;
       if(!Test.isRunningTest()){
	       String url = ApexPages.currentPage().getUrl();
	       url = url.split('\\?')[0];
	       pages = url.substring(url.lastIndexOf('/')+1);
	       if(pages != null) pages = pages.toLowerCase();	
	       System.debug('PAGE URL --'+ url +  ' Pages Value '+ pages);
       }
        quoteIdStr = ApexPages.currentPage().getParameters().get('id');
        hasSourceFiles = true;
        hasRefFiles =  true;
        lstSSP = new List<Standard_Special_Provisions__c>();
        specialProvisionQuote= new List<SelectOption>();
        prepareData(quoteIdStr);
        if(GLSConfig.btnEnableStatus.contains(quoteRecord.Status__c)){
        		disableBtn = false;
    	}
    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('gls_qwquotelistview','Quote List View'));
        options.add(new SelectOption('glsqw_newquotepage','Quote Edit'));
        options.add(new SelectOption('glsqw_scopeofservicespage','Scope of Services'));
        options.add(new SelectOption('glsqw_quoteoverview','Quote Overview'));
        options.add(new SelectOption('glsqw_ratesummarypage','Rate Summary'));
        options.add(new SelectOption('glsqw_termsandsowpage','Terms And SOW'));
        options.add(new SelectOption('glsqw_approvalprocesspage','Review And Submit'));

        return options;
    }
    
    public void prepareData(String quoteIdStr){
        
        String quoteId = quoteIdStr;
        specialProvisions = '';
        specialProvisionsQuote='';
        sPToEditText = '';
        lstAssgDetails = new List<AssgDetails>();
        standardSpecialProv = new List<SelectOption>();
        List<SelectOption> sourceFileOptions = new List<SelectOption>(); 
        List<SelectOption> refFileOptions = new List<SelectOption>(); 
    
        try{
            quoteRecord = [Select isCopyWithFiles__c, Urgency__c, Contact__r.FirstName, Total_Word_Count__c, Total_DTP_Hours__c, Total_Amount__c, TAT_TEST_FIELD__c, Status__c, 
                              Special_Provisions__c, Signature_Block_Text__c, Contact__r.Name,
                              Show_Manual_Delivery_Schedule__c, Show_Expedited_Delivery_Schedule__c, Revision_Number__c, Revised_Order__c, 
                              Reason_for_Decline_ID__c, Reason__c,Quote_Manager__c, Rate_Sheet__c, Quote_Version__c, Quote_Number__c, Quote_Name__c, 
                              Quote_Introduction__c, Quote_Expiration_Date__c,Purchase_Order__c, Project_Due_Date__c, OwnerId, 
                              Original_Quote_Id__c, Number_of_languages__c, Notes__c, Quote_Due_Date__c, Name, IsDeleted, IsCreateProject__c, 
                              Id,Display_Subtotal_per_Language_pair__c, 
                              Display_Rates_per_item_on_on_Quotes__c, Discount_Percentage__c, Description__c, Delivery_Schedule__c, Contact__c,
                              Clone_From__c, Client__c, Client__r.Name, Change_Order__c, client__r.ownerId, Bucket_Name__c, Actual_Total_Amount__c From Quote__c Where Id =: quoteId LIMIT 1];
            quoteNumber = quoteRecord.Quote_Number__c;
            List<Quote_Assignment__c> lstQAssg = [Select isAssignmentUpdated__c, Special_Instructions__c, Source_Language__c, Quote__c, Quote_Task_Description__c, 
                                                Quote_Reference_Files_Instruction__c,Quote_Assignment_Id__c, OwnerId, Name, Id, Delivery_Format__c, 
                                                Delivery_File_Format__c, Countries__c, AssignmentNumber1__c, Allowed_File_Formats__c, 
                                                (Select Id, OwnerId, Name, Quote_Assignment_Id__c, Quote_Assignment_Special_Provision_Name__c, 
                                                Quote_Assignment_Special_Provision_Text__c, Standard_Special_Provisions__c 
                                                From Quote_Assignment_Special_Provisions__r), 
                                                (Select Id, OwnerId, Name, Client_Service__c, Quote_Assignment__c, Service_Name__c, Client_Service__r.Name 
                                                From Quote_Assignment_Services__r), 
                                                (Select Id, OwnerId, Name, Language_List_Item__c, Quote_Assignment__c, Language_Name__c,Language_List_Item__r.Name
                                                From Quote_Assignment_Languages__r),
                                                (Select Id, Name, Quote_Files__c, Quote_Assignment__c, File_Usage__c, Quote_Files__r.File_Name__c 
                                                From Quote_Assignment_Files__r)  
                                                From Quote_Assignment__c q Where Quote__c =: quoteId];
            lstSSP = [Select Special_Provision_Text__c, Special_Provision_Name__c, Name, Id From Standard_Special_Provisions__c order by Special_Provision_Name__c];
            specialProvisionQuote.add(new Selectoption('+Add Common Special Provisions','+Add Common Special Provisions'));
                for(Standard_Special_Provisions__c ssp : lstSSP){
                    specialProvisionQuote.add(new Selectoption(ssp.Special_Provision_Text__c,ssp.Special_Provision_Text__c));
                }
            String assgServices = '';
            String assgLangs = '';
            String assgSpecialProv = '';
            String assgDeliverables = '';
            String title = '';
            String selectedSPOption = '';
            integer counter = 1;
            if(lstQAssg != null && lstQAssg.size() == 0){
            	isAssgEmpty = true;
            }
            for(Quote_Assignment__c qAssg : lstQAssg){
            	standardSpecialProv = new List<SelectOption>();
                selectedSPOption = '';
                assgDetailsStr = '';
                assgSpecialProv = '';
                assgDeliverables = '';
                assgLangs = '';
                assgServices = '';
                title = '';   
                for(Quote_Assignment_File__c  qassgFile : qAssg.Quote_Assignment_Files__r){
                    if(qassgFile.File_Usage__c == 'Client'){
                        sourceFileOptions.add(new Selectoption(qassgFile.Quote_Files__r.File_Name__c,qassgFile.Quote_Files__r.File_Name__c));
                    }else if(qassgFile.File_Usage__c == 'Reference'){
                        refFileOptions.add(new Selectoption(qassgFile.Quote_Files__r.File_Name__c,qassgFile.Quote_Files__r.File_Name__c));
                    }
                }   
                if(sourceFileOptions.size() > 0 && hasSourceFiles){
                    hasSourceFiles = true;
                }else{
                    hasSourceFiles = false;
                }
                if(refFileOptions.size() > 0 && hasRefFiles){
                    hasRefFiles = true;
                }else{
                    hasRefFiles = false;
                }
                standardSpecialProv.add(new Selectoption('+Add Common Special Provisions','+Add Common Special Provisions'));
                for(Standard_Special_Provisions__c ssp : lstSSP){
                    standardSpecialProv.add(new Selectoption(ssp.Special_Provision_Text__c,ssp.Special_Provision_Text__c));
                }
                if(qAssg.Quote_Assignment_Special_Provisions__r.size() == 1){
                    assgSpecialProv = qAssg.Quote_Assignment_Special_Provisions__r[0].Quote_Assignment_Special_Provision_Text__c; 
                }
                else if(qAssg.Quote_Assignment_Special_Provisions__r.size() == 2){
                    assgSpecialProv = assgSpecialProv + qAssg.Quote_Assignment_Special_Provisions__r[0].Quote_Assignment_Special_Provision_Text__c+'\n'+qAssg.Quote_Assignment_Special_Provisions__r[1].Quote_Assignment_Special_Provision_Text__c;
                }
                else if(qAssg.Quote_Assignment_Special_Provisions__r.size()> 2){
                    for(integer i = 0; i < qAssg.Quote_Assignment_Special_Provisions__r.size(); i++){
                        if(i != qAssg.Quote_Assignment_Special_Provisions__r.size()-1){
                            assgSpecialProv = assgSpecialProv + qAssg.Quote_Assignment_Special_Provisions__r[i].Quote_Assignment_Special_Provision_Text__c+'\n';
                        }
                        if(i == qAssg.Quote_Assignment_Special_Provisions__r.size()-1){
                            integer index = assgSpecialProv.lastIndexOf('\n');
                            assgSpecialProv = assgSpecialProv.substring(0,index)+'\n'+qAssg.Quote_Assignment_Special_Provisions__r[i].Quote_Assignment_Special_Provision_Text__c;
                        }
                    }
                }
                if(qAssg.Quote_Assignment_Languages__r.size() == 1){
                    assgLangs = qAssg.Quote_Assignment_Languages__r[0].Language_List_Item__r.Name; 
                }
                else if(qAssg.Quote_Assignment_Languages__r.size() == 2){
                    assgLangs = assgLangs + qAssg.Quote_Assignment_Languages__r[0].Language_List_Item__r.Name+', and '+qAssg.Quote_Assignment_Languages__r[1].Language_List_Item__r.Name;
                }
                else if(qAssg.Quote_Assignment_Languages__r.size()> 2){
                    for(integer i = 0; i < qAssg.Quote_Assignment_Languages__r.size(); i++){
                        if(i != qAssg.Quote_Assignment_Languages__r.size()-1){
                            assgLangs = assgLangs + qAssg.Quote_Assignment_Languages__r[i].Language_List_Item__r.Name+' , ';
                        }
                        if(i == qAssg.Quote_Assignment_Languages__r.size()-1){
                            integer index = assgLangs.lastIndexOf(',');
                            assgLangs = assgLangs.substring(0,index)+', and '+qAssg.Quote_Assignment_Languages__r[i].Language_List_Item__r.Name;
                        }
                    }
                }
                assTargLanguages = assgLangs;
                if(qAssg.Quote_Assignment_Services__r.size() == 1){
                    assgServices = qAssg.Quote_Assignment_Services__r[0].Client_Service__r.Name; 
                }
                else if(qAssg.Quote_Assignment_Services__r.size() == 2){
                    assgServices = assgServices + qAssg.Quote_Assignment_Services__r[0].Client_Service__r.Name+', and '+qAssg.Quote_Assignment_Services__r[1].Client_Service__r.Name;
                }
                else if(qAssg.Quote_Assignment_Services__r.size()> 2){
                    for(integer i = 0; i < qAssg.Quote_Assignment_Services__r.size(); i++){
                        if(i != qAssg.Quote_Assignment_Services__r.size()-1){
                            assgServices = assgServices + qAssg.Quote_Assignment_Services__r[i].Client_Service__r.Name+' , ';
                        }
                        if(i == qAssg.Quote_Assignment_Services__r.size()-1){
                            integer index = assgServices.lastIndexOf(',');
                            assgServices = assgServices.substring(0,index)+', and '+qAssg.Quote_Assignment_Services__r[i].Client_Service__r.Name;
                        }
                    }
                }
                if (qAssg.Delivery_Format__c != null)
                	assgDeliverables = qAssg.Delivery_Format__c;
                title = 'Special (Assignment '+counter+') Provisions';
                lstAssgDetails.add(new AssgDetails(qAssg.Id,title,assgServices,assgLangs,assgDeliverables,assgSpecialProv,standardSpecialProv,counter,selectedSPOption));       
                assgDetailsStr = '';
                Integer assgCount = 1;
                if(lstAssgDetails != null && lstAssgDetails.size() > 0){
                    for(AssgDetails ad : lstAssgDetails){            
                        assgDetailsStr += 'Assignment '+assgCount+'\nServices: '+ad.services+'\nLanguages: '+ad.languages+'\nDeliverables: '+ad.deliverables+'\nSpecial (Assignment '+assgCount+') Provisions:\n'+ ad.notesAndSpecialInst+'\n\n';
                        assgCount++;
                    }
                }
                if(quoteRecord.Notes__c != null && quoteRecord.Notes__c != ''){
                	assgDetailsStr += 'Notes/Special(Quote) Instructions: '+quoteRecord.Notes__c;
                }
                specialProvision = standardSpecialProv;
                counter++;
            }
            allSourceFiles = sourceFileOptions;
            allRefFiles = refFileOptions;
        }catch(Exception e){
            system.debug('-------e.getMessage()-----'+e.getMessage());
        }
    }
    
    public Pagereference updateSpecialProvisionsMethod(){
        String strSP = ApexPages.currentPage().getParameters().get('selectedSPStr');
        String currentStrSP = ApexPages.currentPage().getParameters().get('currentSPStr');    
        for(Standard_Special_Provisions__c ssp : lstSSP){
            if(ssp.Special_Provision_Text__c == strSP){     
                if(currentStrSP != ''){
                    currentStrSP += '\n'+ssp.Special_Provision_Text__c;
                }else{
                    currentStrSP = ssp.Special_Provision_Text__c;
                }
            }
        }
        specialProvisions = currentStrSP;
        return null;
    }
     public Pagereference updateSpecialProvisionsMethodQuote(){
        String strSP = ApexPages.currentPage().getParameters().get('selectedSPSt');
        String currentStrSP = ApexPages.currentPage().getParameters().get('currentSPSt');    
        for(Standard_Special_Provisions__c ssp : lstSSP){
            if(ssp.Special_Provision_Text__c == strSP){     
                if(currentStrSP != ''){
                    currentStrSP += '\n'+ssp.Special_Provision_Text__c;
                }else{
                    currentStrSP = ssp.Special_Provision_Text__c;
                }
            }
        }
        specialProvisionsQuote = currentStrSP;
        return null;
    }
    
public Pagereference updateAssgSpecialProvisionsMethod(){
  		String strAssgSP = ApexPages.currentPage().getParameters().get('currentSPs');
  		String strAssgNumber = ApexPages.currentPage().getParameters().get('assgNumber');
  		String checkBoxVal = ApexPages.currentPage().getParameters().get('checkValue');
  		List<String> lstStrAssgSP = new List<String>();
  		map<string,boolean> assgSPMap = new map<string,boolean>();
  		Id assId;
  		if(strAssgSP != '')
  			lstStrAssgSP = strAssgSP.split('\n');
  		for(AssgDetails ad : lstAssgDetails){
  			if(ad.assgNumber == (Integer.valueof(strAssgNumber)+1)){
  				assId = ad.assgId;
  				break;
  			}
  		}
  		List<Quote_Assignment_Special_Provisions__c> lstQAS = new List<Quote_Assignment_Special_Provisions__c>();
  		List<Quote_Assignment_Special_Provisions__c> lstQASDel = new List<Quote_Assignment_Special_Provisions__c>();
  		Quote_Assignment_Special_Provisions__c qas;
  		list<Quote_Assignment_Special_Provisions__c> lstTempQas = [Select Id, Quote_Assignment_Special_Provision_Name__c from Quote_Assignment_Special_Provisions__c where Quote_Assignment_Id__c =: assId AND Quote_Assignment_Special_Provision_Name__c in: lstStrAssgSP];
  		for(String assgSP : lstStrAssgSP){
  			if(assgSP != ''){
  				String tempassgSP ='';
  				if(assgSP.length() > 255){
  					tempassgSP = assgSP.substring(0,255);
  				}else{
  					tempassgSP = assgSP;
  				}
  				for(Quote_Assignment_Special_Provisions__c qasp: lstTempQas){
	  				if(qasp.Quote_Assignment_Special_Provision_Name__c == tempassgSP){
	  					assgSPMap.put(tempassgSP, true);
	  				}
	  			}
	  			if(assgSPMap.get(tempassgSP) == null){
	  				assgSPMap.put(tempassgSP, false);
	  			}
  			}
  		}
  		for(String assgSP : lstStrAssgSP){
  			if(assgSP != ''){
  				String tempassgSP ='';
  				if(assgSP.length() > 255){
  					tempassgSP = assgSP.substring(0,255);
  				}else{
  					tempassgSP = assgSP;
  				}
  				if(assgSPMap.get(tempassgSP) != null && assgSPMap.get(tempassgSP)){
  					//already present
  				}else{
		  			qas = new Quote_Assignment_Special_Provisions__c();
		  			qas.Quote_Assignment_Special_Provision_Text__c = assgSP;
		  			if(assgSP.length() > 255){
		  				qas.Quote_Assignment_Special_Provision_Name__c = assgSP.substring(0,255);
		  			}else{
		  				qas.Quote_Assignment_Special_Provision_Name__c = assgSP;
		  			}
		  			qas.Quote_Assignment_Id__c = assId;
		  			lstQAS.add(qas);
  				}
  			}
  		}
  		if(checkBoxVal =='true'){
  			lstQASDel = [Select Id from Quote_Assignment_Special_Provisions__c where Quote_Assignment_Id__c =: assId AND Quote_Assignment_Special_Provision_Name__c NOT IN: lstStrAssgSP];
	  		if(lstQASDel != null && lstQASDel.size()>0){
	  			try{
	  				delete lstQASDel;
	  			}catch(DMLException de){
	  				system.debug(GLSConfig.DMLExceptionOccured+de);
	  			}
	  		}
  		}
  		if(lstQAS != null && lstQAS.size() > 0){
  			List<Database.Saveresult> result = Database.insert(lstQAS);
  		}
  		return null;
  	}
  	  	
  	public Pagereference updateAssgSpecialProvisionsMethodQuote(){
  		String strAssgSP = ApexPages.currentPage().getParameters().get('currentSP');
  		String checkBoxVal = ApexPages.currentPage().getParameters().get('checkBoxValue');
  		List<String> lstRFQSpePro = strAssgSP.split('\n');
  		List<RFQ_Special_Provisions__c> lstRFQSpecialPro= new List<RFQ_Special_Provisions__c>();
  		List<RFQ_Special_Provisions__c> lstRFQSpecialProDel= new List<RFQ_Special_Provisions__c>();
  		List<Quote_Assignment_Special_Provisions__c> lstQAS = new List<Quote_Assignment_Special_Provisions__c>();
  		List<Quote_Assignment_Special_Provisions__c> lstQASDel = new List<Quote_Assignment_Special_Provisions__c>();
  		List<Id> lstRFQSpclPrvId= new List<Id>();
  		List<Database.Saveresult> result= new List<Database.Saveresult>();
  		RFQ_Special_Provisions__c rsq;
  		List<Quote_Assignment__c> lstQuoteAssg = [Select id From Quote_Assignment__c Where Quote__c =: quoteIdStr];
  		for(String rfqPro : lstRFQSpePro){
  			if(rfqPro != ''){
	  			rsq = new RFQ_Special_Provisions__c();
	  			rsq.RFQ_Special_Provision_Text__c = rfqPro;
	  			if(rfqPro.length() > 255){
	  				rsq.RFQ_Special_Provision_Name__c = rfqPro.substring(0,255);
	  			}else{
	  				rsq.RFQ_Special_Provision_Name__c = rfqPro;
	  			}
	  			rsq.RFQ_Id__c = quoteIdStr;
	  			lstRFQSpecialPro.add(rsq);
  			}
  		}
  		if(checkBoxVal =='true'){
  			lstRFQSpecialProDel = [Select Id from RFQ_Special_Provisions__c where RFQ_Id__c =: quoteIdStr];
	  		if(lstRFQSpecialProDel != null && lstRFQSpecialProDel.size()>0){
	  			try{
	  				delete lstRFQSpecialProDel;
	  			}catch(DMLException de){
	  				system.debug(GLSConfig.DMLExceptionOccured+de);
	  			}
	  		}
	  		lstQASDel = [Select Id from Quote_Assignment_Special_Provisions__c where Quote_Assignment_Id__c in: lstQuoteAssg];
	  		if(lstQASDel != null && lstQASDel.size()>0){
	  			try{
	  				delete lstQASDel;
	  			}catch(DMLException de){
	  				system.debug(GLSConfig.DMLExceptionOccured+de);
	  			}
	  		}
  		}
  		if(lstRFQSpecialPro != null && lstRFQSpecialPro.size() > 0){
  		    result = Database.insert(lstRFQSpecialPro);
  		}
  		if(result != null){
  			for (Database.SaveResult sr : result) {
			    if (sr.isSuccess()) {
	   			 lstRFQSpclPrvId.add(sr.getId());
	   			 }
	  		}
  	   }
  	    List<RFQ_Special_Provisions__c> lstRFQSPObj = [Select id,RFQ_Special_Provision_Text__c,RFQ_Special_Provision_Name__c From RFQ_Special_Provisions__c  Where id in:lstRFQSpclPrvId ];
	  	if(lstQuoteAssg != null && lstQuoteAssg.size()>0){   
	  	Quote_Assignment_Special_Provisions__c qas;   
	  	   for(Quote_Assignment__c qs :lstQuoteAssg){
	  	    for(RFQ_Special_Provisions__c rfq:lstRFQSPObj){
	  	        qas = new Quote_Assignment_Special_Provisions__c();
	  			qas.Quote_Assignment_Special_Provision_Text__c = rfq.RFQ_Special_Provision_Text__c;
	  			qas.Quote_Assignment_Special_Provision_Name__c = rfq.RFQ_Special_Provision_Name__c;
	  			qas.Quote_Assignment_Id__c = qs.Id;
	  			lstQAS.add(qas);
	  	     }
	  	   }
	  	}
	  	if(lstQAS != null && lstQAS.size() > 0){
  			List<Database.Saveresult> lstQASObj = Database.insert(lstQAS);
  		}	   
  	   return null;
  		
  	}
  
  	
  	public Pagereference getAllSpecialProvision(){
  		isSPEdit = true;
  		String strAssgNumber = ApexPages.currentPage().getParameters().get('assgNumber');
  		Id assId;
  		sPToEdit ='';
  		for(AssgDetails ad : lstAssgDetails){
  			if(ad.assgNumber == (Integer.valueof(strAssgNumber)+1)){
  				assId = ad.assgId;
  				break;
  			}
  		}
  		system.debug('---assId--- : '+assId);
  		List<Quote_Assignment_Special_Provisions__c> lstQASP = new List<Quote_Assignment_Special_Provisions__c>();
  		lstQASP = [Select Id, Quote_Assignment_Special_Provision_Text__c, Quote_Assignment_Id__c 
  				  From Quote_Assignment_Special_Provisions__c
  				  Where Quote_Assignment_Id__c =:  assId];
  		system.debug('---lstQASP--- : '+lstQASP);
  		if(lstQASP != null && lstQASP.size()>0){
  			for (Quote_Assignment_Special_Provisions__c lstSP : lstQASP){
  				if(sPToEdit != ''){
                    sPToEdit += '\n'+lstSP.Quote_Assignment_Special_Provision_Text__c;
                }else{
                    sPToEdit = lstSP.Quote_Assignment_Special_Provision_Text__c;
                }
  			}
  		}
  		sPToEditText = sPToEdit;
  		system.debug('---sPToEditText--- : '+sPToEditText);
  		return null;
  	}
  	
  	public Pagereference getAllSpecialProvisionQuote(){
  		rfqSPToEdit ='';
  		List<RFQ_Special_Provisions__c> lstRFQSP = new List<RFQ_Special_Provisions__c>();
  		lstRFQSP = [Select id,RFQ_Special_Provision_Text__c,RFQ_Special_Provision_Name__c From RFQ_Special_Provisions__c  Where RFQ_Id__c =:quoteIdStr];
  		if(lstRFQSP != null && lstRFQSP.size()>0){
  			for (RFQ_Special_Provisions__c rfq : lstRFQSP){
  				if(rfqSPToEdit != ''){
                    rfqSPToEdit += '\n'+rfq.RFQ_Special_Provision_Text__c;
                }else{
                    rfqSPToEdit = rfq.RFQ_Special_Provision_Text__c;
                }
  			}
  		}
  		rfqSPToEditText = rfqSPToEdit;
  		return null;
  	}
  	
  	public Pagereference sendEmailMethod(){
  		statusMessage ='';
  		String quoteStatus = '';
	  	String emailAction = ApexPages.currentPage().getParameters().get('emailAction');
	  	String quoteId = ApexPages.currentPage().getParameters().get('id');
	  	list<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
	  	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	  	string approverName ='';
	  	approverName = quoteRecord.OwnerId;
	  	String sourceLang = '';
	  	String targetLang = '';
	  	String fileSize ='';
	  	string notes = '';
	  	string flieLink = 'https://c.cs11.visual.force.com/apex/GLSQW_NewQuotePage?id='+quoteRecord.id;
	  	string link = URL.getSalesforceBaseUrl().toExternalForm()+'/'+quoteRecord.id;
	  	String userName = Userinfo.getName();
	  	User userEmail = new User();
	  	
	  	if(quoteRecord.Notes__c != null)
	  		notes = quoteRecord.Notes__c;
	  	system.debug('-----notes----: '+notes);
	  	List<Quote_Assignment__c> lstQuoteAssignment = new List<Quote_Assignment__c>();
	  	List<Quotes_File__c> lstqFiles = new List<Quotes_File__c>();
	  	Set<String> setQAssgSourceLang = new Set<String>();
	  	Set<String> setQAssgTargLang = new Set<String>();
	  	  		
	  	lstQuoteAssignment = [Select Id,Source_Language__r.Name,
	  	  					  (Select Id,Language_List_Item__r.Name, Quote_Assignment__c from Quote_Assignment_Languages__r) From Quote_Assignment__c Where Quote__c =:quoteRecord.id];
	  	  		
	  	lstqFiles = [Select id from Quotes_File__c where Quote__c =: quoteRecord.id];
	  	if(lstqFiles != null && lstqFiles.size()>0)
	  	  	fileSize = lstqFiles.size()+'';
	  	
	  	if(lstQuoteAssignment != null && lstQuoteAssignment.size()>0){
  	  		for(Quote_Assignment__c qasId: lstQuoteAssignment){
  	  			setQAssgSourceLang.add(qasId.Source_Language__r.Name);
	  	  		for(Quote_Assignment_Language__c qAstLang : qasId.Quote_Assignment_Languages__r ){	
	  	  			setQAssgTargLang.add(qAstLang.Language_List_Item__r.Name);
	  	  		}
  	  		}
	  	  	if(setQAssgSourceLang != null && setQAssgSourceLang.size()>0){
	  	  		for(String sLang: setQAssgSourceLang){
	  	  			if(sourceLang =='')
	  	  				sourceLang = sLang;
	  	  			else
	  	  				sourceLang = sourceLang+','+sLang;
	  	  		}
	  	  	}
	  	  		
	  	  	if(setQAssgTargLang != null && setQAssgTargLang.size()>0){
	  	  		for(String tLang: setQAssgTargLang){
	  	  			if(targetLang =='')
	  	  				targetLang = tLang;
	  	  			else
	  	  				targetLang = targetLang+','+tLang;
	  	  		}
	  	  	}
  	  	}
	  	
	  	if(emailAction.equalsIgnoreCase('BDM')){
		  	  system.debug('----inside BDM----: ');
		  	  userEmail = [Select Id, Name, Email From User Where id=:approverName limit 1]; 
		      mail.setSubject('Follow-up Required | '+quoteNumber+'| '+quoteRecord.client__r.Name);
		      mail.setHtmlBody('<font style="font-size:14px" face="Calibri"><p>Hello '+userEmail.Name+','+'<br><br>'+
		                       'We have generated Quote for your review,<br><br> Please find below Quote details:<br>'+
		                       '<br>RFQ Number:  '+quoteNumber+'<br>'+
		                       '<br> Additional Comemnt: '+notes+'<br><br>'+
		                       'For more details, click the following link:'+'<br>'+
		                        link+'<br><br>'+
		                       'If you have any questions or concerns, please let us know.'+
		                       '<br></r><br>Best Regards, <br>'+userName+'</p></font>');
	  	  }else if(emailAction.equalsIgnoreCase('Client')){
		  	  system.debug('----inside Client----: ');
		      userEmail = [Select Id, Name, Email From User Where UserType ='PowerPartner' and ContactId =: quoteRecord.contact__c limit 1];
		      mail.setSubject('GLS Inquiry | '+quoteNumber+'| '+quoteRecord.client__r.Name);
		      mail.setHtmlBody('<font style="font-size:14px" face="Calibri"><p>Hello '+quoteRecord.Contact__r.Name+','+'<br><br>'+
		                       'Thank you for your request! We have generated Quote for your review, Its attached in the Quote record. <br><br> Please find below Quote details:<br>'+
		                       '<br>RFQ Number:  '+quoteNumber+'<br>'+
		                       '<br> Additional Comemnt: '+notes+'<br><br>'+
		                       'For more details, click the following link:'+'<br>'+
		                        link+'<br><br>'+
		                       'If you have any questions or concerns, please let us know.'+
		                       '<br></r><br>Best Regards, <br>'+userName+'</p></font>');
	  	  }else if(emailAction.equalsIgnoreCase('File Prep')){
	  	  		system.debug('----inside File Prep----: ');
	  	  		userEmail = [Select Id, Name, Email From User Where id=:approverName limit 1]; 
	  	  		mail.setSubject('File Prep Request | '+quoteNumber+' | '+quoteRecord.client__r.Name);
	  	  		mail.setHtmlBody('<table width="60%"  border="1" style="border-color:black">'+
								'<tr><th colspan="2" style="width:60%; border-collapse:collapse; background-color:#1589FF;color: #FFFFFF; font-size:14px; face=Calibri; padding-top:3px; padding-bottom: 3px;" align="left">File Prep Request</th></tr>'+
								'<tr><td style="width:50%; font-size:14px; face=Calibri; ">Language(s)</td><td  style="width:60%;font-size:14px; face=Calibri;">&nbsp'+targetLang+'</td></tr>'+
								'<tr><td style="width:50%; font-size:14px; face=Calibri; ">Page Count</td><td  style="width:50%;font-size:14px; face=Calibri;">&nbsp</td></tr>'+
								'<tr><td style="width:50%; font-size:14px; face=Calibri; ">Date Sent</td><td  style="width:50%;font-size:14px; face=Calibri;">&nbsp</td></tr>'+
								'<tr><td style="width:50%; font-size:14px; face=Calibri; ">Date Due</td><td  style="width:50%;font-size:14px; face=Calibri;">&nbsp</td></tr>'+
								'<tr><td style="width:50%; font-size:14px; face=Calibri; ">Link to File(s)</td><td style="width:50%;font-size:14px; face=Calibri;">&nbsp'+flieLink+'</td></tr>'+
								'<tr><td style="width:50%; font-size:14px; face=Calibri; ">Location of Issue</td><td  style="width:50%;font-size:14px; face=Calibri;">&nbsp</td></tr>'+
								'<tr><td style="width:50%; font-size:14px; face=Calibri; ">Specific Error Message or Issue</td><td  style="width:50%;font-size:14px; face=Calibri;">&nbsp</td></tr>'+
								'<tr><td style="width:50%; font-size:14px; face=Calibri; ">Expected Result</td><td  style="width:50%;font-size:14px; face=Calibri;">&nbsp</td></tr>'+
								'<tr><td style="width:50%; font-size:14px; face=Calibri; ">What Has Been Done</td><td  style="width:50%;font-size:14px; face=Calibri;">&nbsp</td></tr>'+
								'<tr><td style="width:50%; font-size:14px; face=Calibri; ">Additional Comments</td><td  style="width:50%;font-size:14px; face=Calibri;">&nbsp'+notes+'</td></tr>'+
								'</table>'
				                       );
				quoteStatus = 'File/Scope Review';
	  	  }else if(emailAction.equalsIgnoreCase('Scope Review')){
	  	  		system.debug('----Scope Review----: ');
	  	  		userEmail = [Select Id, Name, Email From User Where id=:approverName limit 1]; 

	  	  		mail.setSubject('Internal/Scope Review | '+quoteNumber+' | '+quoteRecord.client__r.Name);
	  	  		mail.setHtmlBody('<table width="60%"  border="1" style="border-color:black">'+
									'<tr><th colspan="2" style="width:100%; border-collapse:collapse;background-color:#1589FF;color: #FFFFFF; font-size:14px; face=Calibri;padding-top:3px; padding-bottom: 3px; " align="left">Quote Information</th></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri;">Quote Number/Account:</td><td  style="width:60%;font-size:14px; face=Calibri;">&nbsp'+quoteNumber+'/'+quoteRecord.client__r.Name+'</td></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri;">Contact:</td><td  style="width:60%;font-size:14px; face=Calibri;">&nbsp'+quoteRecord.Contact__r.Name+'</td></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri;">Source Language(s):</td><td  style="width:60%;font-size:14px; face=Calibri;">&nbsp'+sourceLang+'</td></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri;">Target Language(s):</td><td   style="width:60%;font-size:14px; face=Calibri;">&nbsp'+targetLang+'</td></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri;">Profit Margin (if applicable):</td><td style="width:60%;font-size:14px; face=Calibri;">&nbsp</td></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri;">Number of Files:</td><td  style="width:60%;font-size:14px; face=Calibri;">&nbsp'+fileSize+'</td></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri;">Document Description / File Types:</td><td  style="width:60%;font-size:14px; face=Calibri;">&nbsp'+link+'</td></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri;">Delivery Date to Client:</td><td  style="width:60%;font-size:14px; face=Calibri;">&nbsp</td></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri;">Link to Quote:</td><td  style="width:60%;font-size:14px; face=Calibri;">'+link+'</td></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri;">Special Provisions (if any):</td><td  style="width:60%;font-size:14px; face=Calibri;">&nbsp</td></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri;">Glossary and Reference Links (if any):</td><td  style="width:60%;font-size:14px; face=Calibri;">&nbsp'+link+'</td></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri;">Link to Previous Relevant Project (if any):</td><td  style="width:60%;font-size:14px; face=Calibri;">&nbsp</td></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri;">Summary of Scope/Type of Project:</td><td  style="width:60%;font-size:14px; face=Calibri;">&nbsp</td></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri;">Additional Comments/Questions (if any):</td><td  style="width:60%;font-size:14px; face=Calibri;">&nbsp'+notes+'</td></tr>'+
									'<tr><th colspan="2" style="width:100%; border-collapse:collapse;background-color:#1589FF;color: #FFFFFF; font-size:14px; face=Calibri; padding-top:3px; padding-bottom: 3px;" align="left">Account Owner/Client Information</th></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri; background-color:#B7CEEC;">Attn All Link (if any):</td><td  style="width:60%;font-size:14px; face=Calibri;">&nbsp</td></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri; background-color:#B7CEEC;">Account Owner Note(s):</td><td  style="width:60%;font-size:14px; face=Calibri;">&nbsp</td></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri; background-color:#B7CEEC;">Client Note(s)/Special Instruction(s):</td><td  style="width:60%;font-size:14px; face=Calibri;">&nbsp</td></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri; background-color:#B7CEEC;">Link to Source file Index (if used):</td><td  style="width:60%;font-size:14px; face=Calibri;">&nbsp</td></tr>'+
									'<tr><td style="width:40%; font-size:14px; face=Calibri; background-color:#B7CEEC;">Analysis Notes:</td><td  style="width:60%;font-size:14px; face=Calibri;">&nbsp</td></tr>'+
									'</table>'
				                       );
  		  }
	  	  
	  	  system.debug('----userEmail---- '+userEmail.Name+'---' +userEmail.Email);
	  	  system.debug('----mail.setHtmlBody---- ' +mail.getHtmlBody());
	  	  mail.setTargetObjectId(userEmail.id);
		  mail.setSaveAsActivity(false);
		  mailList.add(mail);
	      if(mailList.size()>0){
         		List<Messaging.SendEmailResult> results;
         		try{
         			results = Messaging.sendEmail(mailList); 
         		}catch(Exception e){
         			system.debug(GLSConfig.ExceptionOccured);
         		}
      		system.debug('----results---- ' +results[0].isSuccess());
         	 if(results[0].isSuccess()){
         	 	statusMessage = 'Email has been sent successfully';
         	 	if (quoteStatus != ''){
         	 		quoteRecord.Status__c = quoteStatus;
         	 		update quoteRecord;
         	 	}
         	 }else{
         	 	statusMessage = 'Email has not been sent';
         	 }
	      ApexPages.Message statusMsg = new ApexPages.Message(ApexPages.Severity.INFO, statusMessage);
	      ApexPages.addMessage(statusMsg);
	      }  
         return null;
  	}



    public class AssgDetails{
        
        public Id assgId{get;set;}
        public String title{get;set;}
        public String services{get;set;}
        public String languages{get;set;}
        public String deliverables{get;set;}
        public String notesAndSpecialInst{get;set;}
        public List<Selectoption> lstSelectSpecialProv{get;set;}
        public Integer assgNumber{get;set;}
        public String selectedOption{get;set;}
        
        public AssgDetails(Id assgId, String title,String services, String languages, String deliverables, String notesAndSpecialInst,List<Selectoption> lstSelectSpecialProv,Integer assgNumber,String selectedOption){
            this.assgId = assgId;
            this.title = title;
            this.services = services;
            this.languages = languages;
            this.deliverables = deliverables;
            this.notesAndSpecialInst = notesAndSpecialInst;
            this.lstSelectSpecialProv = lstSelectSpecialProv;
            this.assgNumber = assgNumber;
            this.selectedOption = selectedOption;
        }
    }
}