/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSLPW_EstimateLineControllerExtTest {
	static testMethod void setUpTestData(){
		Account accObj					 	= GLSLPW_SetUpData.createAccount('Amgen');
        Contact conObj						= GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
        Program__c prgObj					= GLSLPW_SetUpData.createProgram(accObj.Id,conObj.Id,'Program_Test', 'Amgen');
        Protocol__c protObj 				= GLSLPW_SetUpData.createProtocol(prgObj.Id,'Draft');
        
        Estimate__c eObj 					= GLSLPW_SetUpData.createEstimate(prgObj.Id, 'Approved');
        Estimate_Line_Item__c eliObj1 		= GLSLPW_SetUpData.createELI(protObj.Id, eObj.Id);
        Estimate_Line_Item__c eliObj2 		= GLSLPW_SetUpData.createELI(protObj.Id, eObj.Id);
        Country__c cObj1 					= GLSLPW_SetUpData.createCountry('Test Country1');
        Country__c cObj2 					= GLSLPW_SetUpData.createCountry('Test Country2');
        Country__c cObj3 					= GLSLPW_SetUpData.createCountry('Test Country3');
        Estimate_Line_Country__c elcObj1	= GLSLPW_SetUpData.createELC(5,cObj1.Id , eliObj1.Id);
        Estimate_Line_Country__c elcObj2	= GLSLPW_SetUpData.createELC(10,cObj2.Id , eliObj1.Id);
        
        Estimate_Line_Country__c elcObj3	= GLSLPW_SetUpData.createELC(10,cObj2.Id , eliObj2.Id);
        Estimate_Line_Country__c elcObj4	= GLSLPW_SetUpData.createELC(2,cObj3.Id , eliObj2.Id);
		
	}

    static testMethod void testNotNullId(){
    	setUpTestData();
   		Estimate__c eObj1							= [SELECT id,name FROM Estimate__c LIMIT 1];
   		List<Estimate_Line_Item__c> lstEliObj		= [SELECT id,name FROM Estimate_Line_Item__c ];
        Estimate_Line_Item__c eliObj1 				= lstEliObj[0];
        Estimate_Line_Item__c eliObj2 				= lstEliObj[1];
        
        List<Country__c> lstCObj					= [SELECT id,name FROM Country__c ];
        Country__c cObj1 							= lstCObj[0];
        Country__c cObj2 							= lstCObj[1];
        Country__c cObj3 							= lstCObj[2];
        
		List<Estimate_Line_Country__c> lstElcObj	= [SELECT id,name FROM Estimate_Line_Country__c ];
        Estimate_Line_Country__c elcObj1			= lstElcObj[0];	
        Estimate_Line_Country__c elcObj2			= lstElcObj[1];
        Estimate_Line_Country__c elcObj3			= lstElcObj[2];
        Estimate_Line_Country__c elcObj4			= lstElcObj[3];
        
        ApexPages.currentPage().getParameters().put('eid', eObj1.Id);
        ApexPages.currentPage().getParameters().put('id', eliObj1.Id);
        Test.startTest();
	        GLSLPW_EstimateLineControllerExt obj = new GLSLPW_EstimateLineControllerExt(new ApexPages.StandardController(eliObj1));
	        system.assert(obj.estId!=null);
	        obj.mapcountrySites.put(cObj2.Name, 5);
	       	obj.saveAction();
	        obj.mapcountrySites.remove(cObj2.Name);
	        obj.saveAction();
	        eObj1.Status__c = 'New';
	        eObj1.isEstimateRevision__c = false;
	        eObj1.isEstimateVersion__c = false;
	        update eObj1;
        Test.stopTest();      
        }
        
        static testMethod void testNullId() {
        setUpTestData();
        Estimate__c eObj2							= [SELECT id,name FROM Estimate__c LIMIT 1];
   		List<Estimate_Line_Item__c> lstEliObj		= [SELECT id,name FROM Estimate_Line_Item__c ];
        Estimate_Line_Item__c eliObj1 				= lstEliObj[0];
        Estimate_Line_Item__c eliObj2 				= lstEliObj[1];
        
        List<Country__c> lstCObj					= [SELECT id,name FROM Country__c ];
        Country__c cObj2 							= lstCObj[0];
        Country__c cObj3 							= lstCObj[1];
        Country__c cObj4 							= lstCObj[2];
		
        Estimate_Line_Country__c elcObj2	= GLSLPW_SetUpData.createELC(10,cObj2.Id , eliObj1.Id);
        Estimate_Line_Country__c elcObj3	= GLSLPW_SetUpData.createELC(2,cObj3.Id , eliObj1.Id);
        Estimate_Line_Country__c elcOb2		= GLSLPW_SetUpData.createELC(5,cObj2.Id , eliObj1.Id);
        
        ApexPages.currentPage().getParameters().put('eid', eObj2.Id);
        ApexPages.currentPage().getParameters().put('id', null);
        eObj2.Status__c='Reviewed';
        update eObj2;
        Test.startTest();
	        GLSLPW_EstimateLineControllerExt obj1 = new GLSLPW_EstimateLineControllerExt(new ApexPages.StandardController(eliObj1));
	        system.assert(obj1.estId!=null);
	        obj1.saveAction();     
	        obj1.removeCountryAction();
	        obj1.addCountries();
	        //obj1.country = 'Test';
	        obj1.lstCountry.add('Test');
	        obj1.addCountries();
	        obj1.newProtocol = 'Test Protocol';
	        obj1.saveAction();
    		List<SelectOption> protocolTypeList = obj1.getProtocolTypeValues();
	        obj1.protocolType = 'Existing';
	        obj1.protocolNameField();
			obj1.mapcountrySites.put(cObj4.Name, 5);
			List<SelectOption> countryList = obj1.getCountryValues();
	        obj1.saveAction();
        Test.stopTest();
        }
        
        static testMethod void testNullEid() {
		setUpTestData();
        List<Estimate_Line_Item__c> lstEliObj		= [SELECT id,name FROM Estimate_Line_Item__c ];
        Estimate_Line_Item__c eliObj1 				= lstEliObj[0];
     
        ApexPages.currentPage().getParameters().put('eid',null);
        ApexPages.currentPage().getParameters().put('id', eliObj1.Id);
        Test.startTest();
	        GLSLPW_EstimateLineControllerExt obj3 	= new GLSLPW_EstimateLineControllerExt(new ApexPages.StandardController(eliObj1));
	        system.assert(obj3.estId==null);
	        //obj3.country = 'Test country6';
	        obj3.lstCountry.add('Test country6');
	        //obj3.numSites = 2;
	        obj3.addCountries();
			obj3.saveAction();
		Test.stopTest();
        
        }
}