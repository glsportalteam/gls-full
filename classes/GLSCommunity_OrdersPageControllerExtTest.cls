/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSCommunity_OrdersPageControllerExtTest {
	public static User u;
    static testMethod void myUnitTest() {
        Account accObj 						= GLSLPW_SetUpData.createAccount('Amgen');
         Contact conObj 					= GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
     	Quote__c quoteObj  					= GLSLPW_SetUpData.createQuote(accObj.Id,conObj.Id,'Draft');
     	quoteObj.isDirectOrder__c			=true;
     	update quoteObj;
     	Quote_Assignment__c qaObj			= GLSLPW_SetUpData.createQuoteAssignment(quoteObj.Id);
     	Quotes_File__c qfObj				= GLSLPW_SetUpData.createQuoteFile(quoteObj.Id);
     	qfObj.Released__c = true;
     	qfObj.File_Type__c = 'Deliverable';
     	update qfObj;
     	AWS_File__c  awsObj 				= GLSLPW_SetUpData.createAWSFile('testPath','gls-us','testFile','testRecordType','testVersion',1);
    	Quote_PDF_File__c qpdfObj			= GLSLPW_SetUpData.createQuotePDFFile(quoteObj.Id);
    	qpdfObj.AWS_File_Id__c				= awsObj.Id;
    	update qpdfObj;
     
     Test.startTest();
     		Profile p = [select id from profile where name=: GLSConfig.Communityprofile];
	          u = new User(LastName = 'test user 2', 
                             Username = 'test.user.2@example.com', 
                             Email = 'test.2@example.com', 
                             Alias = 'testu2',
                             contactId = conObj.Id, 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = p.Id, 
                             LanguageLocaleKey = 'en_US'
                             );
            System.runAs (u){
 			GLSCommunity_OrdersPageControllerExt obj = new GLSCommunity_OrdersPageControllerExt();
 			obj.selectedQID = quoteObj.Id;
 			obj.popupQuoteId = quoteObj.Id;
 			obj.existingOverallSupportFeedback =4;
 			obj.existingQualityFeedback =4;
 			obj.existingResponsivenessFeedback =3;
 			obj.OverallSupportFeedback ='';
 			obj.QualityFeedback ='';
 			obj.ResponsivenessFeedback ='';
 			obj.populateDeliverables();
			obj.DownLoaddeliverables();
			obj.DownLoadAllDeliverables();
			obj.displayOrders();
			obj.populateStatusLabelMap();
			obj.populatedeliverablesLinkDisableMap();
			obj.populateQuoteInfo();
			obj.saveRating();
			obj.fecthQuotePDFFile();
			//obj.sendemail(quoteObj.Id,quoteObj.Quote_Number__c,'Approved');
			obj.quoteidacceptedorrejected=quoteObj.Id;
			obj.Source ='Hold';
			obj.approveandrejectaction();
			obj.Source ='Cancel';
			obj.approveandrejectaction();
			quoteObj.overallSupportFeedback__c=2;
			quoteObj.qualityFeedback__c =4;
			quoteObj.responsivenessFeedback__c = 3;
			update quoteObj;
			obj.populateQuoteInfo();
			
            }
	 Test.stopTest();		
    }
}