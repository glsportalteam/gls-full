/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GLSLPW_ProtocolCountryControllerExtTest {
	
	static testMethod void setUpTestData(){
		
	}
	
    static testMethod void myUnitTest() {
        //setUpTestData();
        Account accObj					 	= GLSLPW_SetUpData.createAccount('Amgen');
        Contact conObj						= GLSLPW_SetUpData.createContact(accObj.Id,'TestName');
        Program__c prgObj					= GLSLPW_SetUpData.createProgram(accObj.Id,conObj.Id,'Program_Test', 'Amgen');
        Protocol__c protObj1 				= GLSLPW_SetUpData.createProtocol(prgObj.Id,'Draft');
        Country__c cObj1 					= GLSLPW_SetUpData.createCountry('Test Country1');
        Country__c cObj2 					= GLSLPW_SetUpData.createCountry('Test Country2');
        Country__c cObj3 					= GLSLPW_SetUpData.createCountry('Test Country3');
        Protocol__c protObj = [SELECT Id FROM Protocol__c LIMIT 1];
        List<Country__c> countryObj = [SELECT Id FROM Country__c];
        Protocol_Country__c pc1 			= GLSLPW_SetUpData.createProtocolCountry(protObj.Id, countryObj[0].Id, 3, 'TestStatus');
        Protocol_Country__c pc2 			= GLSLPW_SetUpData.createProtocolCountry(protObj.Id, countryObj[1].Id, 3, 'TestStatus');
        List<Protocol_Country__c> lstPC = [SELECT Id, Protocol_Id__c FROM Protocol_Country__c WHERE Protocol_Id__c =: protObj.Id];
        ApexPages.currentPage().getParameters().put('id', protObj.Id);
        Test.startTest();
        	GLSLPW_ProtocolCountryControllerExt obj = new GLSLPW_ProtocolCountryControllerExt(new ApexPages.StandardController(protObj));
        	obj.lstCountries.add('Test Country3');
        	obj.countryName = 'Test Country2';
        	obj.getCountryValues();
        	obj.addCountries();
        	obj.removeCountryAction();
        	obj.saveAction();
        Test.stopTest();
    }
}