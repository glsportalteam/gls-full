public class LMS_Literals {
    public static final String InitialEnrollmentStatus = 'In Progress';
    public static final String ProductObjectTrainingRecordTypeName = 'Trainings';
    public static final String HomePageName = 'glsLmsHome';
    public static final String TrainingFeedbackRecordTypeName = 'Training Feedback';
    public static final String RequiredValueInOptionalError = 'A value present in Training Required For field is also present in Optional For field';
    public static final String OptionalValueInRequiredError = 'A value present in Optional For field is also present in Training Required For field';

	}