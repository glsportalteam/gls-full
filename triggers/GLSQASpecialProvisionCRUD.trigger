trigger GLSQASpecialProvisionCRUD on Quote_Assignment_Special_Provisions__c (before insert, before update) {
	List<Standard_Special_Provisions__c> lstStandardSP = new List<Standard_Special_Provisions__c>();
	List<Id> lstStandardSPId = new List<Id>();
	
	if(Trigger.isBefore && Trigger.isInsert){		
		for(Quote_Assignment_Special_Provisions__c qaSp : Trigger.new){
			lstStandardSPId.add(qaSp.Standard_Special_Provisions__c);
		}
		lstStandardSP = [ Select Special_Provision_Text__c, Special_Provision_Name__c, Id 
		                  From Standard_Special_Provisions__c Where Id in: lstStandardSPId];
		for(Quote_Assignment_Special_Provisions__c qaSp : Trigger.new){
			for(Standard_Special_Provisions__c ssp : lstStandardSP){
				if(qaSp.Standard_Special_Provisions__c == ssp.Id){
					qaSp.Quote_Assignment_Special_Provision_Name__c = ssp.Special_Provision_Name__c;
					qaSp.Quote_Assignment_Special_Provision_Text__c = ssp.Special_Provision_Text__c;
				}
			}
		}
	}
	
	if(Trigger.isBefore && Trigger.isUpdate){
		Quote_Assignment_Special_Provisions__c oldSp = new Quote_Assignment_Special_Provisions__c();
		for(Quote_Assignment_Special_Provisions__c qaSp : Trigger.new){
			oldSp = Trigger.oldMap.get(qaSp.Id);
			if(oldSp.Standard_Special_Provisions__c != qaSp.Standard_Special_Provisions__c){
				lstStandardSPId.add(qaSp.Standard_Special_Provisions__c);
			}
		}
		lstStandardSP = [ Select Special_Provision_Text__c, Special_Provision_Name__c, Id 
		                  From Standard_Special_Provisions__c Where Id in: lstStandardSPId];
		for(Quote_Assignment_Special_Provisions__c qaSp : Trigger.new){
			for(Standard_Special_Provisions__c ssp : lstStandardSP){
				if(qaSp.Standard_Special_Provisions__c == ssp.Id){
					qaSp.Quote_Assignment_Special_Provision_Name__c = ssp.Special_Provision_Name__c;
					qaSp.Quote_Assignment_Special_Provision_Text__c = ssp.Special_Provision_Text__c;
				}
			}
		}
	}
}