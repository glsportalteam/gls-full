trigger GLSAccountCloneCrud on Account (after update){
	Account oldAccount = new Account();
    String acctId = '';
    String pAcctId = '';
    for(Account acc: trigger.new){
        oldAccount = Trigger.oldMap.get(acc.ID);
         if(acc.ParentId != null && acc.ParentId !=  oldAccount.ParentId){
            acctId 	= acc.Id;
            pAcctId = acc.ParentId;
         }
    }
  	if(acctId != '' && pAcctId != '') {
  		try{
	  		String sowItemsMessage = GLSQW_AccountRelatedDataLoader.copyRateSheets(acctId,pAcctId);
	  		String rSheetMessage = GLSQW_AccountRelatedDataLoader.copySOWItems(acctId,pAcctId);
	  		String sowTermsMessage = GLSQW_AccountRelatedDataLoader.copySOWTerms(acctId,pAcctId);
  		}catch(Exception e){
            system.debug(GLSConfig.ExceptionOccured+e);
        }
  	}
 }