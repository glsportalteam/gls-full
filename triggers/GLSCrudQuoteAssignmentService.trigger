/* Copyright (c) 2013, Global Languages Solutions, Inc. All rights reserved.
* Trigger   :  GLSCurdQuoteAssignmentService trigger
* Function: Populate Services and the Language pairs in the particular format in the Quote Assignment object field Quote Task Description 
*/

trigger GLSCrudQuoteAssignmentService on Quote_Assignment_Service__c (after insert,after update,after delete) {
	  
	list<Quote_Assignment_Language__c> lstQuoteAL = new list<Quote_Assignment_Language__c>();
  	list<Id> lstQAssignmentId = new list<Id>();
  	list<Quote_Assignment__c> lstQAssignment = new list<Quote_Assignment__c>();
  	list<Quote_Assignment__c> lstQuoteAS = new list<Quote_Assignment__c>();
  	list<Quote__c> lstQuoteBT = new list<Quote__c>();
  	List<Id> lstQuoteASBTId = new List<Id>();
  	Set<String> clientServiceStr = new Set<String>();
  	public Map<id,Quote_Assignment__c> mapQuoteAssignment = new Map<id,Quote_Assignment__c>();
 
 	if(Trigger.isAfter && Trigger.isInsert && !GLSQW_CloneFileController.skipTrigger){   
 		List<Client_Service__c> lstbtCS = [Select Id From Client_Service__c Where Name = 'Back Translation' limit 1];
 		for(Quote_Assignment_Service__c qas:Trigger.New){
			lstQAssignmentId.add(qas.Quote_Assignment__c);
			if(lstbtCS != null && lstbtCS.size()>0){
				if(lstbtCS[0].Id == qas.Client_Service__c){
					lstQuoteASBTId.add(qas.Quote_Assignment__c);
				}
			}
		}
		if(lstQAssignmentId != null && lstQAssignmentId.size()>0){
			List<Quote_Assignment__c> lstQASsg = [Select isAssignmentUpdated__c, Quote_Task_Description__c, Delivery_Format__c,Source_Language__r.Name, 
			                                      Display_Languages_Without_Dialect__c,Certified__c,(Select Client_Service__r.Name, Service_Name__c, Client_Service__r.Order__c 
			                                      From Quote_Assignment_Services__r Order by Client_Service__r.Order__c, Name), 
			                                      (Select Language_List_Item__c,Language_List_Item__r.Name,Language_List_Item__r.Order__c 
			                                      From Quote_Assignment_Languages__r Order by Language_List_Item__r.Name) 
			                                      From Quote_Assignment__c Where Id in: lstQAssignmentId];
			mapQuoteAssignment = updateQuoteTaskDescription(lstQASsg);
			if(mapQuoteAssignment.size()>0){
				update mapQuoteAssignment.values();
			}
		}
		if(lstQuoteASBTId != null && lstQuoteASBTId.size() > 0){
			lstQuoteAS = [Select Id ,(Select Client_Service__r.Name, Service_Name__c, Client_Service__r.Id 
			                              From Quote_Assignment_Services__r) 
			              From Quote_Assignment__c Where Id in: lstQuoteASBTId];
			Client_Service__c translationClientServiceId = [Select Id From Client_Service__c Where Name = 'Translation' limit 1];
			List<Quote_Assignment_Service__c> newQATranslation = new List<Quote_Assignment_Service__c>();
			Quote_Assignment_Service__c qaObj = new Quote_Assignment_Service__c();
			for(Quote_Assignment__c qa : lstQuoteAS){
				clientServiceStr = new Set<String>();
				qaObj = new Quote_Assignment_Service__c();
				for(Quote_Assignment_Service__c qas : qa.Quote_Assignment_Services__r){
					clientServiceStr.add(qas.Client_Service__r.Name);
				}
				if(!clientServiceStr.contains('Translation')){
					qaObj.Client_Service__c = translationClientServiceId.Id;
					qaObj.Quote_Assignment__c = qa.Id;
					newQATranslation.add(qaObj);
				}
			}
			if(newQATranslation != null && newQATranslation.size()>0){
				database.insert(newQATranslation);
			}
		}
 	}
   	
   	if(Trigger.isAfter && Trigger.isUpdate && !GLSQW_CloneFileController.skipTrigger){
    	mapQuoteAssignment= new Map<id,Quote_Assignment__c>();
     	Quote_Assignment__c qa= new Quote_Assignment__c();
		for(Quote_Assignment_Service__c qas: Trigger.new){
        	Quote_Assignment_Service__c oldQas = Trigger.oldMap.get(qas.Id);
          	if (qas.Client_Service__c != oldQas.Client_Service__c){
	        	qa = new Quote_Assignment__c(id=qas.Quote_Assignment__c);
	          	qa.isAssignmentUpdated__c=true;
	          	mapQuoteAssignment.put(qa.id,qa);
          	}
        }
        if(mapQuoteAssignment.size()>0)
			update mapQuoteAssignment.values();
    }	
  	
  	if(Trigger.isAfter && Trigger.isDelete && !GLSQW_CloneFileController.skipTrigger){
     	mapQuoteAssignment= new Map<id,Quote_Assignment__c>();
    	List<Id> lstQuoteAssgId = new List<Id>();
		for(Quote_Assignment_Service__c qas: Trigger.old){
			lstQuoteAssgId.add(qas.Quote_Assignment__c);
		}
		if(lstQuoteAssgId != null && lstQuoteAssgId.size()>0){
			List<Quote_Assignment__c> lstQASsg = [Select isAssignmentUpdated__c, Quote_Task_Description__c, Delivery_Format__c,Source_Language__r.Name, 
			                                      Certified__c,Display_Languages_Without_Dialect__c,(Select Client_Service__r.Name, Service_Name__c, Client_Service__r.Order__c 
			                                      From Quote_Assignment_Services__r order by Client_Service__r.Order__c,Client_Service__r.Name), 
			                                      (Select Language_List_Item__c,Language_List_Item__r.Name, Language_List_Item__r.Order__c	 
			                                      From Quote_Assignment_Languages__r order by Language_List_Item__r.Name) 
			                                      From Quote_Assignment__c Where Id in: lstQuoteAssgId];
			mapQuoteAssignment = updateQuoteTaskDescription(lstQASsg);
			if(mapQuoteAssignment.size()>0)
				update mapQuoteAssignment.values();
		}
     }
     
     /*Method to update the Quote Task Description field of Quote Assignment*/
     
     public Map<id,Quote_Assignment__c> updateQuoteTaskDescription(List<Quote_Assignment__c> lstQASsg){
    	Map<id,Quote_Assignment__c> mapQuoteAssignmentUpdate = new Map<id,Quote_Assignment__c>();
    	for(Quote_Assignment__c qa : lstQASsg){
			list<string> lstService= new List<string>();
		  	for(Quote_Assignment_Service__c qas:qa.Quote_Assignment_Services__r){
		    	lstService.add(qas.Client_Service__r.Name);
		  	}
	  		set<string> setTargetLanguage = new set<string>();
	  		list<string> lstTargetLanguage = new list<string>();
		  	for(Quote_Assignment_Language__c qal:qa.Quote_Assignment_Languages__r){
		  		String tmpStr; 
		  		if(!qa.Display_Languages_Without_Dialect__c){
		  			tmpStr = qal.Language_List_Item__r.Name;
		  			if(tmpStr.contains('(')){
		  				integer indexOfbrackets = tmpStr.indexOf('(');
		  				setTargetLanguage.add(tmpStr.substring(0,indexOfbrackets-1));
		  			}else{
		  				setTargetLanguage.add(qal.Language_List_Item__r.Name);
		  			}
		  		}else{
		      		setTargetLanguage.add(qal.Language_List_Item__r.Name);
		  		}
		  	}	
		  	lstTargetLanguage.addAll(setTargetLanguage);
		  	lstTargetLanguage.sort();
	  		string listOfService = '';
		  	if(lstService.size() == 1){
		    	listOfService = lstService[0];
		    }else if(lstService.size() == 2){
		    	listOfService = listOfService+lstService[0]+' and '+lstService[1];
		    }else if(lstService.size()> 2){
		    	for(integer i = 0; i < lstService.size();i++){
		        	if(i != lstService.size()-1){
		            	listOfService = listOfService +lstService[i]+', ';
		           	}
		           	if(i == lstService.size()-1){
		            	integer index = listOfService.lastIndexOf(',');
		            	listOfService = listOfService.substring(0,index)+', and '+lstService[i];
		          	}
		       	}
		   	}
		   	string listOfLanguage = '';    
		   	if(lstTargetLanguage.size() == 1){
		    	listOfLanguage = lstTargetLanguage[0]; 
		    }else if(lstTargetLanguage.size() == 2){
		       	listOfLanguage = listOfLanguage+lstTargetLanguage[0]+' and '+lstTargetLanguage[1];
		    }else if(lstTargetLanguage.size()> 2){
		       	for(integer i = 0; i < lstTargetLanguage.size();i++){
		        	if(i != lstTargetLanguage.size()-1){
		            	listOfLanguage = listOfLanguage +lstTargetLanguage[i]+', ';
		          	}
		          	if(i == lstTargetLanguage.size()-1){
		             	integer index = listOfLanguage.lastIndexOf(',');
		             	listOfLanguage = listOfLanguage.substring(0,index)+', and '+lstTargetLanguage[i];
		          	}
		       	}
		   	}
		   	String sourceLang = '';
		   	if(!qa.Display_Languages_Without_Dialect__c){
		   		String tmpStr = qa.Source_Language__r.Name;
		  			if(tmpStr.contains('(')){
		  				integer indexOfbrackets = tmpStr.indexOf('(');
		  				sourceLang = tmpStr.substring(0,indexOfbrackets-1);
		  			}else{
		  				sourceLang = qa.Source_Language__r.Name;
		  			}
		   	}else{
		   		sourceLang = qa.Source_Language__r.Name;
		   	}
		   	string quoteTaskDesc;
		   	if(qa.Certified__c){
		   		quoteTaskDesc = 'Certified '+listOfService +' of the following document(s) from '+sourceLang+' into '+listOfLanguage+':\n'; 
		   	}
		   	else{
		   		quoteTaskDesc = listOfService +' of the following document(s) from '+sourceLang+' into '+listOfLanguage+':\n';
		   	}
	       	qa.Quote_Task_Description__c = quoteTaskDesc;
	      	qa.isAssignmentUpdated__c = true;
		   	mapQuoteAssignmentUpdate.put(qa.id,qa);
		}
		return mapQuoteAssignmentUpdate;
    }
}