/** Copyright (c) 2013, Global Languages Solutions, Inc. All rights reserved.
* Trigger   :  GLSQuotesFilesCRUD trigger
* Function: Maintain Quote File versioning 
*/

trigger GLSQuotesFilesCRUD on Quotes_File__c (after insert, after update, before update, before delete) {
    if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate) && !GLSQW_CloneFileController.skipTrigger){   
    List <Quote_File_Version__c> lsQFversionadd = new List <Quote_File_Version__c>();
    decimal maxversion;
    List<Quote_File_Version__c> lsQFversion = new List <Quote_File_Version__c>();
    List<String> lsQFId = new List <String>();
    Quotes_File__c oldFiles = new Quotes_File__c();    
    
    for(Quotes_File__c qfId :Trigger.New){
        lsQFId.add(qfId.Id);
    }
    if(lsQFId != null && lsQFId.size()>0){
        lsQFversion= [SELECT Id, File_Name__c, SF_Version__c FROM Quote_File_Version__c WHERE Quote_Files__c IN: lsQFId];    
    }          
    for(Quotes_File__c qf:Trigger.New){
       if(Trigger.isUpdate){
            oldFiles = Trigger.oldMap.get(qf.ID);
       }       
        if(oldFiles.Version__c != qf.Version__c){
            Quote_File_Version__c qFversion= new Quote_File_Version__c();
            maxversion = 0;
            qFversion.Quote_Files__c = qf.Id;
            qFversion.File_Name__c = qf.File_Name__c;
            qFversion.Bucket_Name__c = qf.Bucket_Name__c;
            qFversion.File_Format__c= qf.File_Format__c;
            qFversion.Amazon_S3_source_file_path__c = qf.Amazon_S3_source_file_path__c;
            
            if(lsQFversion != null && lsQFversion.size()>0){
                for (Quote_File_Version__c temp : lsQFversion) {
                     if(temp.File_Name__c == qf.File_Name__c && (maxversion < temp.SF_Version__c))
                         maxversion=temp.SF_Version__c;
                }
            }
            
            qFversion.SF_Version__c = maxversion+1;
            qFversion.S3_Version_Id__c = qf.version_id__c;
            lsQFversionadd.add(qFversion);
        }
    }
    
    if(lsQFversionadd != null && lsQFversionadd.size()>0){
        try{
            insert lsQFversionadd;
        }catch(DmlException de){
            System.debug(GLSConfig.DMLExceptionOccured);
        }catch(Exception e){
        System.debug(GLSConfig.ExceptionOccured);
        }
    }    
    }
    
     /**
     *  The below code will prevent the user to Edit the Quote File if the Quote Staus is either one of following status
     *  Darft, New, In Preparation, In Complete, In Review and Reviewed
     */
     if(Trigger.isBefore && Trigger.isDelete){
        List<String> quoteId = new List<String> ();
        Map<Id, Quote__c> mapQuote;
        for(Quotes_File__c qf: Trigger.old){
            quoteId.add(qf.Quote__c);
         }
         
        if(quoteId != null && quoteId.size()>0){
            mapQuote = new Map <Id, Quote__c>([Select Id, Status__c From Quote__c Where Id in : quoteId and Status__c not in ('Draft', 'New', 'In Preparation', 'InComplete', 'In Review','Reviewed', 'Direct Order')]) ;
        }
        
        for(Quotes_File__c qFiles: Trigger.old){
            if (mapQuote != null &&  mapQuote.ContainsKey(qFiles.Quote__c))
                qFiles.addError('Delete Quote File is not allowed at this Quote status.');
        }
     }
     
   
    /**
     *  The below code will prevent the user to Edit the Quote File if the Quote Staus is either one of following status
     *  Darft, New, In Preparation, In Complete, In Review and Reviewed
     */
     if(Trigger.isBefore && Trigger.isUpdate){
        List<String> quoteId = new List<String> ();
        Map<Id, Quote__c> mapQuote;
        for(Quotes_File__c qf: Trigger.new){
            quoteId.add(qf.Quote__c);
         }
         
        if(quoteId != null && quoteId.size()>0){
            mapQuote = new Map <Id, Quote__c>([Select Id, Status__c From Quote__c Where Id in : quoteId and Status__c not in ('Draft', 'New', 'In Preparation', 'InComplete', 'In Review','Reviewed', 'Direct Order')]) ;
        }
        
        for(Quotes_File__c qFiles: Trigger.new){
            if (mapQuote != null &&  mapQuote.ContainsKey(qFiles.Quote__c))
                qFiles.addError('Edit Quote File is not allowed at this Quote status.');
        }
     }
    
    
    if(Trigger.isBefore && Trigger.isDelete && !GLSQW_CloneFileController.skipTrigger && GLSconfig.skipQuotePDFFileTrigger){
        Map<id,Quote_Assignment__c> mapQuoteAssignment=New Map<id,Quote_Assignment__c>();
        List<Quote_Assignment_File__c> lstQAFiles= new List<Quote_Assignment_File__c>();
        List<Id> lstQAIds= new List<Id>();
        Quote_Assignment__c qaObj= new Quote_Assignment__c();
        List<String> qfId = new List<String> ();
         for(Quotes_File__c qf: Trigger.old){
            qfId.add(qf.Id);
         }
        if(qfId != null && qfId.size()>0){
            lstQAFiles = [Select Id, Quote_Assignment__c, Quote_Files__c From Quote_Assignment_File__c Where Quote_Assignment__c <> null And Quote_Files__c in:(qfId)] ;
        }
        if(lstQAFiles != null && lstQAFiles.size()>0){
            for(Quote_Assignment_File__c qafiles: lstQAFiles){
                lstQAIds.add(qafiles.Quote_Assignment__c);
            }
        }
        if(lstQAIds != null && lstQAIds.size()>0){
            for(ID qa: lstQAIds){
                qaObj = new Quote_Assignment__c(id=qa);
                qaObj.isAssignmentUpdated__c=true;
                mapQuoteAssignment.put(qa,qaObj);
            }
        }
        if(mapQuoteAssignment.size()>0)
            update mapQuoteAssignment.values();
    }
    
    if(trigger.isupdate){
     set<id> getsourcefileid = new set<id>();
      set<id> getsourcefileid1 = new set<id>();
     list<quote__c> allquote= new list <quote__c>();
     list< Quotes_File__c> qfl = new list<Quotes_File__c>();
     list<Quote_Assignment_File__c> qaf= new list<Quote_Assignment_File__c>();
     list<Quote_Assignment_File__c> dellist= new list<Quote_Assignment_File__c>();
     list<Quote_Assignment__c> qa = new list<Quote_Assignment__c>();
     set<id>qaid= new set<id>();
    // map<id,id> mymap= new map<id,id>();
      map<id,Quote_Assignment_File__c> mymap= new map<id,Quote_Assignment_File__c>();
      
     
     for(  Quotes_File__c  qf : trigger.new){
         if(trigger.oldmap.get(qf.id).File_Type__c != qf.File_Type__c  && qf.File_Type__c=='Obsolete' ){
             getsourcefileid.add(qf.Quote__c);
             getsourcefileid1.add(qf.id);
         }
     }
     if(getsourcefileid1.size()>0){
        qfl= [select id from  Quotes_File__c where id in: getsourcefileid1 ];
      }
     
     if(getsourcefileid.size()>0 && getsourcefileid !=null){
        allquote= [select id  from Quote__c where id in : getsourcefileid limit 1];
     }
     if( allquote.size()>0  && allquote !=null ){
         qa=[select id ,Quote__c from Quote_Assignment__c where Quote__c =: allquote[0].id];
      }
      if(qa.size()>0 && qa !=null){
           for(Quote_Assignment__c quoteass :qa){
                 qaid.add(quoteass.id);
            }
       } 
       if(qaid.size()>0 && qaid !=null){
         qaf = [select id ,Quote_Files__c,Quote_Assignment__c from Quote_Assignment_File__c where Quote_Assignment__c  in :qaid];
       }
     if(qaf.size()>0 && qaf !=null){
         for(Quote_Assignment_File__c qaf1 : qaf){
         
             mymap.put(qaf1.Quote_Files__c,qaf1);
     
         }
     }
     if(mymap.size()>0){
        if( qfl.size()>0){
            for( Quotes_File__c qq :qfl){
                if(mymap.containsKey(qq.id)==true){
                
                    dellist.add(mymap.get(qq.id));
                }
            }
         }
     }
     
     if(dellist.size()>0){
         try{
             delete dellist;
         }
          catch (exception e){
          system.debug('----Exception---' + e.getmessage());
          
          }
     }
     
   }
    if(trigger.isinsert){
    set  <id> qid= new set <id>();
    boolean myflag;
     list<Quotes_File__c> mylist= new list<Quotes_File__c>();
      list<Quotes_File__c> updatelist= new list<Quotes_File__c>();
     for(  Quotes_File__c  qf : trigger.new){
            qid.add(qf.id);
         
         }
         if(qid != null && qid.size()>0){
          mylist=[select id ,File_Name__c from Quotes_File__c where id in :qid];
         
         }
         if(mylist.size()>0){
           for( Quotes_File__c qf :mylist){
             string a= qf.File_Name__c;
              for(integer i =0;i<=a.length()-1; i++){
                   integer b=0;
                   string c= string.valueof(a.charAt(i));
                   b=integer.valueof(c.trim());
                   if(b>127 || b==44){
                     qf.isSPCharFile__c=true;
                     updatelist.add(qf);
                     break;
                   }
                }
              
                                   
           }
           if( updatelist.size()>0){
               try{
               update updatelist;
               }
               catch( exception e){
               system.debug('exception');
               }
           
           
           }
         }
     }
  
    
}