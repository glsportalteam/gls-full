trigger GLSQuoteCrud on Quote__c (after update,after insert,before insert, before update) {
    
    Quote__c oldQuote = new Quote__c();
    List<Quote_Line_Item_Subline__c> lstQLISubline = new List<Quote_Line_Item_Subline__c>();
    public string addUnitMeasure;
    /*if(Trigger.isAfter && Trigger.isUpdate && !GLSQW_RateSummaryController.isNotExecutQLITrigger){*/
    if(Trigger.isAfter && Trigger.isUpdate && !GLSQW_RateSummaryController.isNotExecutQLITrigger){
        List<Id> lstQuoteId = new List<Id>();
        List<Id> lstQuoteWordCountId = new List<Id>();
        List<Id> lstRateSheetId = new List<Id>();
        List<Id> lstClientId = new List<Id>();
        List<Id> lstIdQuoteCertfied = new List<Id>();
        
        Map<string,decimal> mapRateSummary= new map<string,decimal>();
        Map<string,Decimal> mapRateSummaryDefault = new Map<string,Decimal>();
        Map<String,Quote_Line_Item_Subline__c> tmpMapSubLine = new Map<String,Quote_Line_Item_Subline__c>();
        Map<String,Quote_Line_Item_Subline__c> mapDelSubLine = new Map<String,Quote_Line_Item_Subline__c>();
        Map<String,Quote_Line_Item_Subline__c> mapSubLine = new Map<String,Quote_Line_Item_Subline__c>();
        Map<Id,Quote_Line_Item__c> mapQLI = new Map<Id,Quote_Line_Item__c>();
        List<Rate_Sheet_Item__c> lstRateSheetItems = new List<Rate_Sheet_Item__c>();
        List<Rate_Sheet_Item__c> lstRateSheetItemsTmp = new List<Rate_Sheet_Item__c>();
        List<Quote_Line_Item__c> lstQLI = new List<Quote_Line_Item__c> ();
        List<Quote__c> lstQuote = new List<Quote__c>();
        List<Sub_Item_Trados_Category__c> lstSubItemTC = new List<Sub_Item_Trados_Category__c>();
        map<String,Rate_Sheet_Item__c> mapRSIName = new map<String,Rate_Sheet_Item__c>();
        boolean isRateSheetItemExist = false;
        
        try{
            Set<Id> quoteIdDelAttachment = new Set<Id>();
            for(Quote__c quote : trigger.new){
                oldQuote = Trigger.oldMap.get(quote.ID);
                lstClientId.add(quote.Client__c);
                
                //Delete Quote PDF from Notes and Attachments
                if(GLSconfig.deletePDFStatus.contains(quote.Status__c)){
                	quoteIdDelAttachment.add(quote.Id);
                }
                
                if(quote.Rate_Sheet__c !=  oldQuote.Rate_Sheet__c){
                    lstQuoteId.add(quote.id);
                    lstRateSheetId.add(quote.Rate_Sheet__c);
                    
                }
                if(quote.Pushback_Status__c !=  oldQuote.Pushback_Status__c && (quote.Pushback_Status__c ==  'Success' || quote.Pushback_Status__c ==  'Error')){
                    lstQuoteWordCountId.add(quote.id);
                    lstQuoteId.add(quote.id);
                    lstRateSheetId.add(quote.Rate_Sheet__c);
                    system.debug('---inside trigger condition---- '+lstQuoteId);
                }
                if(quote.Certified__c != oldQuote.Certified__c || quote.Display_Languages_Without_Dialect__c != oldQuote.Display_Languages_Without_Dialect__c){
                    lstIdQuoteCertfied.add(quote.Id);
                    
                } 
            }
            List<Attachment> lstAttachment = [SELECT Id FROM Attachment WHERE ParentId IN: quoteIdDelAttachment];
            if(lstAttachment != null && lstAttachment.size() > 0){
            	try{
            		delete lstAttachment;
            	}
            	catch(DmlException de){
		            system.debug(GLSConfig.DMLExceptionOccured);
		        }
		        catch(Exception e){
		            system.debug(GLSConfig.ExceptionOccured);
	        	}
            }
            
            list<Rate_Summary_Line_Item_Master__c> lstRSLIM= [select id,Default_Value__c,Quantity__c,Default_Order__c,Rate__c,Trados_Category__c from Rate_Summary_Line_Item_Master__c];
            for(Rate_Summary_Line_Item_Master__c rslim:lstRSLIM){
                mapRateSummary.put(rslim.Trados_Category__c,rslim.Default_Order__c);
                if(rslim.Default_Value__c != null)
                mapRateSummaryDefault.put(rslim.Trados_Category__c,rslim.Default_Value__c);
            }
            /**/
            if(lstIdQuoteCertfied != null && lstIdQuoteCertfied.size()>0){
                List<Quote_Assignment__c> lstQACertifiedUpdate = new List<Quote_Assignment__c>();
                List<Quote_Assignment__c> lstQACetified = [Select Id, Certified__c, Quote__c From Quote_Assignment__c Where Quote__c in: lstIdQuoteCertfied];
                for(Quote__c quote : trigger.new){
                    for(Quote_Assignment__c qassg : lstQACetified){
                        if(qassg.Quote__c == quote.Id){
                            qassg.Certified__c = quote.Certified__c;
                            qassg.Display_Languages_Without_Dialect__c = quote.Display_Languages_Without_Dialect__c;
                            lstQACertifiedUpdate.add(qassg);
                        }
                    }
                }
                if(lstQACertifiedUpdate != null && lstQACertifiedUpdate.size() > 0){
                    Database.update(lstQACertifiedUpdate);
                }
            }
            if(lstQuoteId != null && lstQuoteId.size()>0){
                //  lstQuote = [Select Id,Rate_Sheet__c,Rate_Sheet__r.Name From Quote__c Where Id in: lstQuoteId];
                lstQuote = [Select Id,Rate_Sheet__c,Rate_Sheet__r.Name,Standard__c,Notarized__c,
                (Select Id From Quote_Level_Line_Item__r) From Quote__c Where Id in: lstQuoteId];
                
                
                
                List<Id> langId = new List<Id>();
                lstQLI = [select Id,QuoteId__c,Source_Language_ID__r.Language_Name__c,Actual_Words__c,Back_Translation_Total_Words__c,Target_Language_ID__r.Language_Name__c,Context_TM_Word_Count__c,
                DTP_Hours__c,X95_99_Word_Count__c,X85_95_Word_Count__c,X75_84_Word_Count__c,X50_74_Word_Count__c,No_Match_Word_Count__c,Task_Status__c,
                Target_Language_ID__r.Name,Source_Language_ID__r.Name, Perfect_Match__c,Crossfile_Repetitions__c,X100_Word_Count__c,Repetitions_Word_Count__c,QuoteId__r.Rate_Sheet__c,
                (Select Id,Unit_Price__c,Description__c,Quantity__c, Client_Service__c From Quote_Line_Item_Sublines__r),
                (Select Service_Id__c,Service_Id__r.Unit_of_Measure__c, Service_Id__r.IsTWCLogic__c,Service_Id__r.IsOutsourced__c, Service_Id__r.Additional_Percentage__c,Service_Id__r.Name From Quote_Line_Services__r),
                (Select Id From Quote_Line_Object_Assignment_Files__r)
                from Quote_Line_Item__c Where QuoteID__c in: lstQuoteId];
                for(Quote_Line_Item__c qli : lstQLI){
                    if(qli.Target_Language_ID__r.Name.contains('English'))
                    langId.add(qli.Source_Language_ID__c);
                    else
                        langId.add(qli.Target_Language_ID__c);
                }
                lstRateSheetItems = [Select Id, Rate_Sheet__c, Min_Amount__c,Unit_of_Measure__c,Min_Amount_Description__c,Service__c,
                Service__r.Name,Service__r.Additional_Percentage__c,Language__r.Name,Language__c,Target_Language_Code__c,
                Rate_Sheet__r.Name,Hourly_Rate__c,(Select Rate__c, Description__c, Trados_Categories__c From Rate_Sheet_Item__r),
                Source_Language_Code__c From Rate_Sheet_Item__c Where Rate_Sheet__c in: lstRateSheetId AND Language__c in: langId];
                map<Id,string> mapLangIds = new map<Id,string>();
                List<Id> lstLangIdNotInSelectedRS = new List<Id>();
                if(lstRateSheetItems.size() > 0){
                    for(Rate_Sheet_Item__c rsi : lstRateSheetItems){
                        /*for(Quote_Line_Item__c qli : lstQLI){
                        if(qli.Target_Language_ID__r.Name == rsi.Language__r.Name){
                        continue;
                        }else if(rsi.Rate_Sheet__r.Name != 'Standard'){
                        lstLangIdNotInSelectedRS.add(qli.Target_Language_ID__c)  ;
                        }
                        }*/
                        mapLangIds.put(rsi.Language__c,rsi.Language__r.Name);
                        
                    }
                    for(Quote_Line_Item__c qli : lstQLI){
                        if(!qli.Target_Language_ID__r.Name.contains('English'))
                        {
                            if(!mapLangIds.isEmpty() && mapLangIds.get(qli.Target_Language_ID__c) == null){
                                lstLangIdNotInSelectedRS.add(qli.Target_Language_ID__c);
                            }
                        }else
                        {
                            if(!mapLangIds.isEmpty() && mapLangIds.get(qli.Source_Language_ID__c) == null){
                                lstLangIdNotInSelectedRS.add(qli.Source_Language_ID__c);
                            }
                        }
                    }
                }else{
                    for(Quote_Line_Item__c qli : lstQLI){
                        if(qli.Target_Language_ID__r.Name.contains('English'))
                        lstLangIdNotInSelectedRS.add(qli.Source_Language_ID__c);
                        else
                            lstLangIdNotInSelectedRS.add(qli.Target_Language_ID__c)  ;
                    }
                }
                /* lstRateSheetItemsTmp = [Select Id, Rate_Sheet__c, Min_Amount__c,Unit_of_Measure__c,Min_Amount_Description__c,Service__c,
                Service__r.Name,Service__r.Additional_Percentage__c,Language__r.Name,Language__c,Target_Language_Code__c,
                Rate_Sheet__r.Name,  (Select Rate__c, Description__c, Trados_Categories__c From Rate_Sheet_Item__r),
                Source_Language_Code__c From Rate_Sheet_Item__c Where Rate_Sheet__r.Name = 'Standard' AND Language__c in: lstLangIdNotInSelectedRS];*/
                lstRateSheetItemsTmp = [Select Id, Rate_Sheet__c, Min_Amount__c,Unit_of_Measure__c,Min_Amount_Description__c,Service__c,
                Service__r.Name,Service__r.Additional_Percentage__c,Language__r.Name,Language__c,Target_Language_Code__c,
                Rate_Sheet__r.Name, Hourly_Rate__c, (Select Rate__c, Description__c, Trados_Categories__c From Rate_Sheet_Item__r),
                Source_Language_Code__c From Rate_Sheet_Item__c Where Rate_Sheet__r.Name = 'Standard' AND Language__c in: lstLangIdNotInSelectedRS];
                lstRateSheetItems.addAll(lstRateSheetItemsTmp);
                List<Id> lstRSSIIds = new List<Id>();
                for(Rate_Sheet_Item__c rsi : lstRateSheetItems){
                    
                    if((rsi.Service__r !=null) && (rsi.Language__r !=null) && (rsi.Service__r.Name.equalsIgnoreCase('Translation') || rsi.Service__r.Name.equalsIgnoreCase('Back Translation'))){
                        mapRSIName.put(rsi.Service__r.Name + '-' +rsi.Language__r.Name, rsi);
                    }
                    
                    system.debug('---mapRSIName----: '+mapRSIName );
                    
                    for(Rate_Sheet_Subitem__c rssi : rsi.Rate_Sheet_Item__r){
                        lstRSSIIds.add(rssi.Id);
                    }
                }
                lstSubItemTC = [Select id,Trados_Category__c,Rate_Sheet_Subitem__c
                From Sub_Item_Trados_Category__c Where Rate_Sheet_Subitem__c in: lstRSSIIds];
            }
            Decimal totalAmount = 0;
            Decimal totalWords = 0;
            List<Quote_Line_Item__c> lstQLIUpdate = new List<Quote_Line_Item__c>();
            List<Quote_Line_Item_Subline__c> tempLstQLISubline = new List<Quote_Line_Item_Subline__c>();
            Quote_Line_Item_Subline__c qlisl = new Quote_Line_Item_Subline__c();
            /*List<Client_Trados_Category__c> lstCTC = [Select Trados_Categories__c,Client__c, Service__c, Name, Id, Description__c
            From Client_Trados_Category__c Where Client__c in: lstClientId];*/
            List<String> lstCTCString ;
            Decimal additionalPercent;
            Decimal additionPercentageForBackTrans = 0;
            
            List<Client_Service__c> lstClientServices = [Select Additional_Percentage__c, Name From Client_Service__c];
            
            for(Client_Service__c services : lstClientServices){
                if(services.Name.contains('Back Translation') && services.Additional_Percentage__c != null)
                {
                    additionPercentageForBackTrans = Decimal.valueOf(services.Additional_Percentage__c);
                }
            }
            
            for(Quote__c quote : lstQuote){
                system.debug('---inside for lstQuote----: '+lstQuote);
                for(Quote_Line_Item__c qli : lstQLI){
                    
                    String rateSheetName = quote.Rate_Sheet__r.Name;
                    qli.Rate_Type__c = (rateSheetName != null && rateSheetName.contains('Standard'))? 'Standard Rates':(rateSheetName != null && rateSheetName.contains('Promo'))?'':'Client Rates';
                    lstQLIUpdate.add(qli);
                    for(Quote_Line_Item_Subline__c qlis : qli.Quote_Line_Item_Sublines__r){
                        mapDelSubLine.put(qlis.Id,qlis);
                    }
                    for(Quote_Line_Service__c qlis : qli.Quote_Line_Services__r){
                        isRateSheetItemExist = false;
                        String unitOfMeasure = qlis.Service_Id__r.Unit_of_Measure__c ;
                        
                        if(qlis.Service_Id__r.IsTWCLogic__c){
                            map<String,Rate_Sheet_Subitem__c> mapRSSI = new map<String,Rate_Sheet_Subitem__c>();
                            Rate_Sheet_Item__c translationRSI = new Rate_Sheet_Item__c();
                            totalWords = 0;
                            totalAmount = 0;
                            
                            if(qli.Target_Language_ID__r.Name.contains('English')){
                                translationRSI = mapRSIName.get('Back Translation-'+qli.Source_Language_ID__r.Name);
                            }else{
                                translationRSI = mapRSIName.get('Translation-'+qli.Target_Language_ID__r.Name);
                            }
                            if(translationRSI != null){
                                for(Rate_Sheet_Subitem__c rssi : translationRSI.Rate_Sheet_Item__r){
                                    if(rssi.Description__c.contains('Repetitions'))
                                    {
                                        mapRSSI.put('Repetitions',rssi);
                                    }else if(rssi.Description__c.contains('Total Word Count')) {
                                        mapRSSI.put('Total Word Count',rssi);
                                    }
                                }
                                List<Rate_Sheet_Subitem__c> lstRSSI = new List<Rate_Sheet_Subitem__c>();
                                Decimal rate = 0;
                                String description = '';
                                if(mapRSSI.containsKey('Total Word Count')){
                                    system.debug('-----Inside TWC if-----');
                                    description = mapRSSI.get('Total Word Count').Description__c;
                                    lstRSSI.add(mapRSSI.get('Total Word Count'));
                                    rate = mapRSSI.get('Total Word Count').Rate__c;
                                }else if(mapRSSI.containsKey('Repetitions')){
                                    system.debug('-----Inside 100% and Repetitions if-----');
                                    description = mapRSSI.get('Repetitions').Description__c;
                                    lstRSSI.add(mapRSSI.get('Repetitions'));
                                    rate = mapRSSI.get('Repetitions').Rate__c;
                                }
                                
                                totalWords = qli.X95_99_Word_Count__c + qli.X85_95_Word_Count__c + qli.X75_84_Word_Count__c + qli.X50_74_Word_Count__c + qli.No_Match_Word_Count__c + qli.Context_TM_Word_Count__c + qli.Repetitions_Word_Count__c + qli.Crossfile_Repetitions__c + qli.X100_Word_Count__c + qli.Perfect_Match__c;
                                
                                qlisl = new Quote_Line_Item_Subline__c();
                                qlisl.Description__c = qlis.Service_Id__r.Name;
                                qlisl.Unit_Price__c = rate;
                                qlisl.Unit_of_Measure__c = 'per Word';
                                qlisl.Quote_Line_Item_Id__c = qli.Id;
                                //TBC
                                qlisl.Client_Service__c = qlis.Service_Id__c;
                                string descriptionCat = qlis.Service_Id__r.Name;
                                if(mapRateSummary.get(descriptionCat) != null)
                                qlisl.Default_Subline_Order__c=mapRateSummary.get(descriptionCat);
                                
                                if(qlis.Service_Id__r.Name.equalsIgnoreCase('Translation')){
                                    qlisl.Default_Order__c=1;
                                }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Back Translation')){
                                    qlisl.Default_Order__c=2;
                                }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Desktop Publishing')){
                                    qlisl.Default_Order__c=3;
                                }else{
                                    qlisl.Default_Order__c=4;
                                }
                                qlisl.Manual_Entry__c = false;
                                
                                if(qli.Target_Language_ID__r.Name.contains('English')){
                                    
                                    qlisl.Quantity__c = Math.ceil(totalWords+( qli.Back_Translation_Total_Words__c*additionPercentageForBackTrans/100));
                                }else{
                                    qlisl.Quantity__c = totalWords;
                                }
                                
                                //qlisl.Quantity__c = totalWords;
                                totalAmount += totalWords * rate;
                                tmpMapSubLine.put(qli.Id+'-'+qlisl.Client_Service__c+'-'+qlisl.Description__c,qlisl);
                                
                                if(translationRSI.Min_Amount__c != null && totalAmount <= translationRSI.Min_Amount__c){
                                    qlisl = new Quote_Line_Item_Subline__c();
                                    qlisl.Quote_Line_Item_Id__c = qli.Id;
                                    
                                    addUnitMeasure='per Word';
                                    
                                    qlisl.Unit_of_Measure__c = addUnitMeasure;
                                    qlisl.Description__c = qlis.Service_Id__r.Name+', Minimum Charge';
                                    qlisl.Unit_Price__c = translationRSI.Min_Amount__c;
                                    qlisl.Quantity__c = 1;
                                    qlisl.Manual_Entry__c = false;
                                    if(mapRateSummary.get(description) != null)
                                    qlisl.Default_Subline_Order__c = mapRateSummary.get(description);
                                    
                                    if(qlis.Service_Id__r.Name.equalsIgnoreCase('Translation')){
                                        qlisl.Default_Order__c=1;
                                    }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Back Translation')){
                                        qlisl.Default_Order__c=2;
                                    }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Desktop Publishing')){
                                        qlisl.Default_Order__c=3;
                                    }else{
                                        qlisl.Default_Order__c=4;
                                    }
                                    qlisl.Show_Units__c = false;
                                    qlisl.Show_Unit_of_Measure__c = false;
                                    qlisl.Show_Description__c = true;
                                    qlisl.Show_Subline_Total__c = true;
                                    qlisl.Show_Unit_Price__c = false;
                                    qlisl.Sort_Order__c = 1;
                                    qli.Min_Charge_Applied__c = true;
                                    qlisl.Client_Service__c = qlis.Service_Id__c;
                                    mapSubLine.put(qli.Id+'-'+qlisl.Client_Service__c+'-'+qlisl.Description__c,qlisl);
                                    mapQLI.put(qli.Id,qli);
                                }
                                else{
                                    mapSubLine.putAll(tmpMapSubLine);
                                }
                                totalWords = 0;
                                isRateSheetItemExist = true;
                            } else
                            {   Decimal rate = 0;
                                qlisl = new Quote_Line_Item_Subline__c();
                                qlisl.Description__c = qlis.Service_Id__r.Name;
                                qlisl.Unit_Price__c = rate;
                                qlisl.Unit_of_Measure__c = 'per Word';
                                qlisl.Quote_Line_Item_Id__c = qli.Id;
                                //TBC
                                qlisl.Client_Service__c = qlis.Service_Id__c;
                                string descriptionCat = qlis.Service_Id__r.Name;
                                if(mapRateSummary.get(descriptionCat) != null)
                                qlisl.Default_Subline_Order__c=mapRateSummary.get(descriptionCat);
                                
                                if(qlis.Service_Id__r.Name.equalsIgnoreCase('Translation')){
                                    qlisl.Default_Order__c=1;
                                }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Back Translation')){
                                    qlisl.Default_Order__c=2;
                                }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Desktop Publishing')){
                                    qlisl.Default_Order__c=3;
                                }else{
                                    qlisl.Default_Order__c=4;
                                }
                                qlisl.Manual_Entry__c = false;
                                
                                mapSubLine.put(qli.Id+'-'+qlisl.Client_Service__c+'-'+qlisl.Description__c,qlisl);
                                
                            }
                        }else if(qlis.Service_Id__r.IsOutsourced__c){
                            Decimal rate = 0;
                            
                            if(mapRateSummaryDefault.get(qlis.Service_Id__r.Name) != null){
                                rate = mapRateSummaryDefault.get(qlis.Service_Id__r.Name);
                            }
                            
                            qlisl = new Quote_Line_Item_Subline__c();
                            qlisl.Description__c = qlis.Service_Id__r.Name;
                            qlisl.Unit_Price__c = rate;
                            if(qlis.Service_Id__r.Additional_Percentage__c != null && qlis.Service_Id__r.Additional_Percentage__c.trim() != '')
                            qlisl.Outsourced_Service_Percentage__c = qlis.Service_Id__r.Additional_Percentage__c;
                            else
                                qlisl.Outsourced_Service_Percentage__c =  string.valueOf(60);
                            
                            qlisl.IsOutsourced__c = true;
                            qlisl.Unit_of_Measure__c = qlis.Service_Id__r.Unit_of_Measure__c ;
                            qlisl.Quote_Line_Item_Id__c = qli.Id;
                            qlisl.Client_Service__c = qlis.Service_Id__c;
                            
                            string description = qlis.Service_Id__r.Name;
                            if(mapRateSummary.get(description) != null)
                            qlisl.Default_Subline_Order__c=mapRateSummary.get(description);
                            
                            if(qlis.Service_Id__r.Name.equalsIgnoreCase('Translation')){
                                qlisl.Default_Order__c=1;
                            }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Back Translation')){
                                qlisl.Default_Order__c=2;
                            }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Desktop Publishing')){
                                qlisl.Default_Order__c=3;
                            }else{
                                qlisl.Default_Order__c=4;
                            }
                            qlisl.Quantity__c = 1;
                            qlisl.Manual_Entry__c = false;
                            mapSubLine.put(qli.Id+'-'+qlisl.Client_Service__c+'-'+qlisl.Description__c,qlisl);
                            //mapSubLine.putAll(tmpMapSubLine);
                            totalAmount = 0;
                            totalWords = 0;
                            isRateSheetItemExist = true;
                        } else if(unitOfMeasure !=null && unitOfMeasure.contains('Hour')){
                            Rate_Sheet_Item__c translationRSI = new Rate_Sheet_Item__c();
                            if(qli.Target_Language_ID__r.Name.contains('English')){
                                translationRSI = mapRSIName.get('Back Translation-'+qli.Source_Language_ID__r.Name);
                            }else{
                                translationRSI = mapRSIName.get('Translation-'+qli.Target_Language_ID__r.Name);
                            }
                            if(translationRSI != null){
                                qlisl = new Quote_Line_Item_Subline__c();
                                qlisl.Description__c = 'Hour(s), '+qlis.Service_Id__r.Name;
                                
                                qlisl.Unit_Price__c = translationRSI.Hourly_Rate__c;
                                qlisl.Unit_of_Measure__c = 'per Hour';
                                qlisl.Quote_Line_Item_Id__c = qli.Id;
                                qlisl.Client_Service__c = qlis.Service_Id__c;
                                /*Set <String> serviceNameSet = new Set<String>();
                                serviceNameSet = mapRateSummary.keySet();
                                String rateSummaryKey = '';
                                for(String name : serviceNameSet){
                                if(name.contains(qlis.Service_Id__r.Name)){
                                rateSummaryKey = name;
                                }
                                }*/
                                if(mapRateSummary.get('Hour(s), ' + qlis.Service_Id__r.Name) != null)
                                qlisl.Default_Subline_Order__c=mapRateSummary.get('Hour(s), ' + qlis.Service_Id__r.Name);
                                qlisl.Default_Order__c=4;
                                qlisl.Manual_Entry__c = false;
                                totalWords = 1;
                                if(qlis.Service_Id__r.Name.contains('Desktop Publishing')){
                                    totalWords = qli.DTP_Hours__c;
                                    if(totalWords > 1)
                                    {
                                        qlisl.Quantity__c = qli.DTP_Hours__c;
                                    }else
                                        qlisl.Quantity__c = 1;
                                }else{
                                    qlisl.Quantity__c = 1;
                                }
                                //totalAmount += totalWords * translationRSI.Hourly_Rate__c;
                                mapSubLine.put(qli.Id+'-'+qlisl.Client_Service__c+'-'+qlisl.Description__c,qlisl);
                                totalWords = 0;
                                
                                isRateSheetItemExist = true;
                            }else
                            {
                                qlisl = new Quote_Line_Item_Subline__c();
                                qlisl.Description__c = 'Hour(s), '+qlis.Service_Id__r.Name;
                                
                                qlisl.Unit_Price__c = 0;
                                qlisl.Unit_of_Measure__c = 'per Hour';
                                qlisl.Quote_Line_Item_Id__c = qli.Id;
                                qlisl.Client_Service__c = qlis.Service_Id__c;
                                /*Set <String> serviceNameSet = new Set<String>();
                                serviceNameSet = mapRateSummary.keySet();
                                String rateSummaryKey = '';
                                for(String name : serviceNameSet){
                                if(name.contains(qlis.Service_Id__r.Name)){
                                rateSummaryKey = name;
                                }
                                }*/
                                if(mapRateSummary.get('Hour(s), ' + qlis.Service_Id__r.Name) != null)
                                qlisl.Default_Subline_Order__c=mapRateSummary.get('Hour(s), ' + qlis.Service_Id__r.Name);
                                qlisl.Default_Order__c=4;
                                qlisl.Manual_Entry__c = false;
                                totalWords = 1;
                                if(qlis.Service_Id__r.Name.contains('Desktop Publishing') && qli.DTP_Hours__c > 1){
                                    totalWords = qli.DTP_Hours__c;
                                    qlisl.Quantity__c = totalWords;
                                }else{
                                    qlisl.Quantity__c = 1;
                                }
                                //totalAmount += totalWords * translationRSI.Hourly_Rate__c;
                                mapSubLine.put(qli.Id+'-'+qlisl.Client_Service__c+'-'+qlisl.Description__c,qlisl);
                                totalWords = 0;
                            }
                        }
                        else if(unitOfMeasure!=null && unitOfMeasure.contains('Word')){
                            totalAmount = 0;
                            totalWords = 0;
                            Rate_Sheet_Item__c danRSI = new Rate_Sheet_Item__c();
                            string tLangaue  = qli.Target_Language_ID__r.Name;
                            if(qli.Target_Language_ID__r.Name.contains('English'))
                            tLangaue = qli.Source_Language_ID__r.Name;
                            
                            for(Rate_Sheet_Item__c rsi : lstRateSheetItems){
                                
                                if(qlis.Service_Id__c == rsi.Service__c && rsi.Language__r.Name == tLangaue){
                                    {
                                        if(qlis.Service_Id__r.Name.equalsIgnoreCase('Translation') && qli.Target_Language_ID__r.Name.contains('English'))
                                        {
                                            danRSI = mapRSIName.get('Back Translation-'+qli.Source_Language_ID__r.Name);
                                            
                                            system.debug('---Back Translation--danRSSI----: '+danRSI );
                                        }
                                        else  if(qlis.Service_Id__r.Name.equalsIgnoreCase('Back Translation') && qli.Target_Language_ID__r.Name.contains('English'))
                                        {
                                            danRSI = mapRSIName.get('Translation-'+qli.Source_Language_ID__r.Name);
                                            
                                            system.debug('---Translation---danRSSI----: '+danRSI );
                                        }else{
                                            danRSI = rsi;
                                        }
                                        
                                        if(danRSI != null)
                                        {
                                            
                                            system.debug('---danRSSI----: '+danRSI );
                                            
                                            for(Rate_Sheet_Subitem__c rssi : danRSI.Rate_Sheet_Item__r){
                                                
                                                if(!rssi.Description__c.trim().contains('Total Word Count'))
                                                {
                                                    
                                                    for(Sub_Item_Trados_Category__c sitc : lstSubItemTC){
                                                        if(sitc.Rate_Sheet_Subitem__c == rssi.Id){
                                                            if(sitc.Trados_Category__c.equalsIgnoreCase('95% - 99%'))
                                                            {
                                                                totalWords = totalWords + qli.X95_99_Word_Count__c;
                                                            }
                                                            if(sitc.Trados_Category__c.equalsIgnoreCase('85% - 94%'))
                                                            {
                                                                totalWords = totalWords + qli.X85_95_Word_Count__c;
                                                            }
                                                            if(sitc.Trados_Category__c.equalsIgnoreCase('75% - 84%'))
                                                            {
                                                                totalWords = totalWords + qli.X75_84_Word_Count__c;
                                                            }
                                                            if(sitc.Trados_Category__c.equalsIgnoreCase('50% - 74%'))
                                                            {
                                                                totalWords = totalWords + qli.X50_74_Word_Count__c;
                                                            }
                                                            if(sitc.Trados_Category__c.equalsIgnoreCase('New'))
                                                            {
                                                                totalWords = totalWords + qli.No_Match_Word_Count__c;
                                                            }
                                                            if(sitc.Trados_Category__c.equalsIgnoreCase('Context Match'))
                                                            {
                                                                totalWords = totalWords + qli.Context_TM_Word_Count__c;
                                                            }
                                                            if(sitc.Trados_Category__c.equalsIgnoreCase('Repetitions'))
                                                            {
                                                                totalWords = totalWords + qli.Repetitions_Word_Count__c;
                                                            }
                                                            if(sitc.Trados_Category__c.equalsIgnoreCase('Cross-file Repetitions'))
                                                            {
                                                                totalWords = totalWords + qli.Crossfile_Repetitions__c;
                                                            }
                                                            if(sitc.Trados_Category__c.equalsIgnoreCase('100%'))
                                                            {
                                                                totalWords = totalWords + qli.X100_Word_Count__c;
                                                            }
                                                            if(sitc.Trados_Category__c.equalsIgnoreCase('Perfect_Match__c'))
                                                            {
                                                                totalWords = totalWords + qli.Perfect_Match__c;
                                                            }
                                                        }
                                                    }
                                                    qlisl = new Quote_Line_Item_Subline__c();
                                                    
                                                    if(qlis.Service_Id__r != null && qli.Target_Language_ID__r !=null)
                                                    {
                                                        if(qlis.Service_Id__r.Name.equalsIgnoreCase('Translation') && qli.Target_Language_ID__r.Name.contains('English') && rssi.Description__c.contains('Back Translation'))
                                                        {
                                                            if(rssi.Description__c.split(',').size()> 1)
                                                            {
                                                                qlisl.Description__c = rssi.Description__c.split(',')[1].trim();
                                                            }
                                                        }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Back Translation') && qli.Target_Language_ID__r.Name.contains('English'))
                                                        {
                                                            qlisl.Description__c = 'Back Translation, ' + rssi.Description__c.trim();
                                                        }else
                                                        {
                                                            qlisl.Description__c = rssi.Description__c.trim();
                                                        }
                                                    }else
                                                    {
                                                        qlisl.Description__c = rssi.Description__c.trim();
                                                    }
                                                    
                                                    qlisl.Unit_Price__c = rssi.Rate__c;
                                                    if(rssi.Description__c.contains('Hour') || rssi.Description__c.contains('Desktop Publishing') || rssi.Description__c.contains('Minimum')){
                                                        addUnitMeasure='per Hour';
                                                    }else if (rssi.Description__c.contains('Certification')){
                                                        addUnitMeasure='';
                                                    }else{
                                                        addUnitMeasure='per Word';
                                                    }
                                                    qlisl.Unit_of_Measure__c = addUnitMeasure;
                                                    qlisl.Quote_Line_Item_Id__c = qli.Id;
                                                    qlisl.Client_Service__c = rsi.Service__c;
                                                    
                                                    String description =  qlisl.Description__c;
                                                    
                                                    if(mapRateSummary.get(description) != null)
                                                    qlisl.Default_Subline_Order__c=mapRateSummary.get(description);
                                                    if(rsi.Service__r.Name.equalsIgnoreCase('Translation')){
                                                        qlisl.Default_Order__c=1;
                                                    }else if(rsi.Service__r.Name.equalsIgnoreCase('Back Translation')){
                                                        qlisl.Default_Order__c=2;
                                                    }else if(rsi.Service__r.Name.equalsIgnoreCase('Desktop Publishing')){
                                                        qlisl.Default_Order__c=3;
                                                    }else{
                                                        qlisl.Default_Order__c=4;
                                                    }
                                                    qlisl.Manual_Entry__c = false;
                                                    
                                                    if(((qlis.Service_Id__r.Name.contains('Back Translation') && !qli.Target_Language_ID__r.Name.contains('English'))|| (qlis.Service_Id__r.Name.equalsIgnoreCase('Translation') && qli.Target_Language_ID__r.Name.contains('English'))) && rssi.Trados_Categories__c.contains('New')){
                                                        if(qli.Target_Language_ID__r.Name.contains('English'))
                                                        {
                                                            qlisl.Quantity__c = Math.ceil(totalWords+( qli.Back_Translation_Total_Words__c*additionPercentageForBackTrans/100));
                                                        }else
                                                        {
                                                            qlisl.Quantity__c = Math.ceil(totalWords+( qli.Actual_Words__c*additionPercentageForBackTrans/100));
                                                        }
                                                    }else{
                                                        qlisl.Quantity__c = totalWords;
                                                    }
                                                    totalAmount += qlisl.Quantity__c * rssi.Rate__c;
                                                    tmpMapSubLine.put(qli.Id+'-'+qlisl.Client_Service__c+'-'+qlisl.Description__c,qlisl);
                                                    totalWords = 0;
                                                    
                                                    isRateSheetItemExist =  true;
                                                }
                                            }
                                            if(rsi.Min_Amount__c != null && totalAmount <= rsi.Min_Amount__c){
                                                qlisl = new Quote_Line_Item_Subline__c();
                                                qlisl.Quote_Line_Item_Id__c = qli.Id;
                                                if(rsi.Min_Amount_Description__c.contains('Hour') || rsi.Min_Amount_Description__c.contains('Desktop Publishing') || rsi.Min_Amount_Description__c.contains('Minimum')){
                                                    addUnitMeasure='per Hour';
                                                }else if(rsi.Min_Amount_Description__c.contains('Certification')){
                                                    addUnitMeasure='';
                                                }else{
                                                    addUnitMeasure='per Word';
                                                }
                                                qlisl.Unit_of_Measure__c = addUnitMeasure;
                                                qlisl.Description__c = rsi.Min_Amount_Description__c;
                                                qlisl.Unit_Price__c = rsi.Min_Amount__c;
                                                qlisl.Quantity__c = 1;
                                                qlisl.Manual_Entry__c = false;
                                                if(mapRateSummary.get('Minimum Charge') != null)
                                                qlisl.Default_Subline_Order__c = mapRateSummary.get('Minimum Charge');
                                                if(rsi.Service__r.Name.equalsIgnoreCase('Translation')){
                                                    qlisl.Default_Order__c=1;
                                                }else if(rsi.Service__r.Name.equalsIgnoreCase('Back Translation')){
                                                    qlisl.Default_Order__c=2;
                                                }else if(rsi.Service__r.Name.equalsIgnoreCase('Desktop Publishing')){
                                                    qlisl.Default_Order__c=3;
                                                }else{
                                                    qlisl.Default_Order__c=4;
                                                }
                                                qlisl.Show_Units__c = false;
                                                qlisl.Show_Unit_of_Measure__c = false;
                                                qlisl.Show_Description__c = true;
                                                qlisl.Show_Subline_Total__c = true;
                                                qlisl.Show_Unit_Price__c = false;
                                                qlisl.Sort_Order__c = 1;
                                                qli.Min_Charge_Applied__c = true;
                                                qlisl.Client_Service__c = rsi.Service__c;
                                                mapSubLine.put(qli.Id+'-'+qlisl.Client_Service__c+'-'+qlisl.Description__c,qlisl);
                                                mapQLI.put(qli.Id,qli);
                                            }
                                            else{
                                                mapSubLine.putAll(tmpMapSubLine);
                                            }
                                        }
                                    }
                                    
                                    totalAmount = 0;
                                }
                            }
                        } else {
                            Decimal rate = 0;
                            qlisl = new Quote_Line_Item_Subline__c();
                            qlisl.Description__c = qlis.Service_Id__r.Name;
                            qlisl.Unit_Price__c = rate;
                            qlisl.Unit_of_Measure__c = qlis.Service_Id__r.Unit_of_Measure__c;
                            qlisl.Quote_Line_Item_Id__c = qli.Id;
                            
                            //TBC
                            qlisl.Client_Service__c = qlis.Service_Id__c;
                            if(mapRateSummary.get(qlis.Service_Id__r.Name) != null)
                            qlisl.Default_Subline_Order__c=mapRateSummary.get(qlis.Service_Id__r.Name);
                            
                            if(qlis.Service_Id__r.Name.equalsIgnoreCase('Translation')){
                                qlisl.Default_Order__c=1;
                            }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Back Translation')){
                                qlisl.Default_Order__c=2;
                            }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Desktop Publishing')){
                                qlisl.Default_Order__c=3;
                            }else{
                                qlisl.Default_Order__c=4;
                            }
                            
                            totalWords = 0;
                            qlisl.Manual_Entry__c = false;
                            //totalAmount += totalWords * rssi.Rate__c;
                            totalAmount = 0;
                            mapSubLine.put(qli.Id+'-'+qlisl.Client_Service__c+'-'+qlisl.Description__c,qlisl);
                            //mapSubLine.putAll(tmpMapSubLine);
                            totalWords = 0;
                        }
                        
                        tmpMapSubLine.clear();
                    }
                    
                    // tmpMapSubLine.clear();
                }
                
                
            }
            if(mapSubLine.size()>0)
            upsert mapSubLine.values();
            if(mapDelSubLine.size()>0)
            delete mapDelSubLine.values();
            if(lstQLIUpdate.size() > 0)
            update lstQLIUpdate;
            
        }catch(Exception e){
            system.debug(e);
        }
    }
    if(Trigger.isAfter && Trigger.isUpdate){
        
        System.debug('---Delivery Schedule Code flow---');
        
        /* Code for calculating the delivery schedule for the Quote : start*/
        List<Id> lstQuoteWordCountIdForDS = new List<Id>();
        List<Delivery_Schedule__c> lstDS = new List<Delivery_Schedule__c>();
        List<Delivery_Schedule__c> lstDelDS = new List<Delivery_Schedule__c>();
        Delivery_Schedule__c ds;
        List<Quote_Line_Item_Subline__c> lstQLISublines = new List<Quote_Line_Item_Subline__c>();
        List<Quote__c> lstQuoteDS = new List<Quote__c>();
        List<Quote_Assignment_Service__c> lstQuoteAssgService = new List<Quote_Assignment_Service__c>();
        List<Quote_Line_Service__c> lstQuoteLineService = new List<Quote_Line_Service__c>();
        List<Quote_Level_Line_Item__c> lstQLLI = new List<Quote_Level_Line_Item__c>();
        List<Id> lstQuoteIdForCertification = new List<Id>();
        for(Quote__c quote : trigger.new){
            oldQuote = Trigger.oldMap.get(quote.ID);
            if(quote.Actual_Total_Amount__c != oldQuote.Actual_Total_Amount__c || quote.Total_DTP_Hours__c != oldQuote.Total_DTP_Hours__c){
                lstQuoteWordCountIdForDS.add(quote.id);
            }
            if(quote.Standard__c != oldQuote.Standard__c || quote.Notarized__c != oldQuote.Notarized__c){
                lstQuoteIdForCertification.add(quote.id);
            }
        }
        
        
        /*  if(lstQuoteWordCountIdForDS != null && lstQuoteWordCountIdForDS.size()>0){*/
        if(lstQuoteWordCountIdForDS != null && lstQuoteWordCountIdForDS.size()>0 ){ //&& !iscalledFromsubmit
            lstQuoteDS = [Select Total_Word_Count__c, Total_Amount__c, Delivery_Schedule__c,Total_DTP_Hours__c,Total_Quote_DTP_Hours__c,
            (Select QuoteID__c, Sub_Total_Amount__c, Actual_Words__c, Back_Translation_Total_Words__c,Subline_Total_Amount__c,
            DTP_Hours__c,Name From Quote_Line_Items__r Order By Actual_Words__c desc),
            (Select Id, Quote_Id__c, Service__c,Service__r.Name,System_Standard_Delivery_Schedule__c, Service__r.IsOutsourced__c,
            System_Expedited_Delivery_Schedule__c,Expedited_Delivery_Schedule__c,Standard_Delivery_Schedule__c
            From Delivery_Schedule__r) From Quote__c Where Id in: lstQuoteWordCountIdForDS ];
            lstQuoteAssgService = [Select Quote_Assignment__r.Quote__c, Quote_Assignment__c,Client_Service__r.Name,Client_Service__c From Quote_Assignment_Service__c
            Where Quote_Assignment__r.Quote__c in: lstQuoteWordCountIdForDS];
            lstQuoteLineService = [Select Quote_Line_Item_Id__c,Name,Service_Id__c,Service_Id__r.Name,Quote_Line_Item_Id__r.QuoteID__c
            From Quote_Line_Service__c Where Quote_Line_Item_Id__r.QuoteID__c in: lstQuoteWordCountIdForDS];
            lstQLISublines = [Select Quote_Line_Item_Id__r.QuoteID__c, Quote_Line_Item_Id__c, Client_Service__r.Id, Client_Service__c,Client_Service__r.Name, Amount__c, Quantity__c
            From Quote_Line_Item_Subline__c Where Quote_Line_Item_Id__r.QuoteID__c in: lstQuoteWordCountIdForDS];
            lstQLLI = [Select IsStandard__c,Id,Outsourced_Quantity__c,Outsourced_Service__c,Quote__c
            From Quote_Level_Line_Item__c Where Quote__c in: lstQuoteWordCountIdForDS];
            Decimal totDTPHrs = 0.0;
            Decimal dtpWeeks = 0.0;
            Decimal dtpDays = 0.0;
            Decimal minDays = 0.0;
            String btTime;
            Long dtpDaysRoundUp;
            Decimal totWordCount = 0;
            String days1;
            Decimal days2 = 0.0;
            String standardDeliverySchedule;
            String btStandardDeliverySchedule;
            String expeditedDeliverySchedule;
            String btExpeditedDeliverySchedule;
            List<Quote__c> lstQuoteUpdate = new List<Quote__c>();
            map<string,decimal> mapServiceWords = new map<string,decimal>();
            List<Id> lstServiceId = new List<Id>();
            Set<Id> setOldServiceId = new Set<Id>();
            Set<Id> setServiceId = new Set<Id>();
            Client_Service__c btCS = [Select Name,Id From Client_Service__c Where Name = 'Back Translation' limit 1];
            List<Client_Service__c> lstClientServices = [Select Unit_of_Measure__c, Number_of_Extra_Days__c, Name, Manual_Processing__c, Manager_Approval__c, IsTWCLogic__c, IsOutsourced__c, Id,
            Client_ServiceID__c, Additional_Percentage__c From Client_Service__c];
            List<Quote_Line_Item_Subline__c> qlisIsOutsourced = [Select Quote_Line_Item_Id__r.QuoteID__c, Quote_Line_Item_Id__c, IsOutsourced__c, Description__c
            From Quote_Line_Item_Subline__c
            Where Quote_Line_Item_Id__r.QuoteID__c in: lstQuoteDS AND IsOutsourced__c = true];
            for(Quote__c quotes : lstQuoteDS){
                for(Quote_Line_Service__c qls:  lstQuoteLineService){
                    if(qls.Quote_Line_Item_Id__r.QuoteID__c == quotes.Id){
                        setServiceId.add(qls.Service_Id__c);
                    }
                }
                for(Quote_Line_Item_Subline__c  qlis: lstQLISublines){
                    if(qlis.Quote_Line_Item_Id__r.QuoteID__c == quotes.Id){
                        if(mapServiceWords.get(qlis.Client_Service__r.Name) != null){
                            if(mapServiceWords.get(qlis.Client_Service__r.Name) < qlis.Quantity__c){
                                mapServiceWords.put(qlis.Client_Service__r.Name,qlis.Quantity__c);
                            }
                        }else{
                            mapServiceWords.put(qlis.Client_Service__r.Name,qlis.Quantity__c);
                        }
                    }
                }
                for(Quote_Level_Line_Item__c qlli : lstQLLI){
                    if(qlli.Quote__c == quotes.Id){
                        if(mapServiceWords.get(qlli.Outsourced_Service__c) != null){
                            if(mapServiceWords.get(qlli.Outsourced_Service__c) < qlli.Outsourced_Quantity__c){
                                mapServiceWords.put(qlli.Outsourced_Service__c,qlli.Outsourced_Quantity__c);
                            }
                        }else{
                            mapServiceWords.put(qlli.Outsourced_Service__c,qlli.Outsourced_Quantity__c);
                        }
                    }
                }
                Set<String> dsOldServices = new Set<String>();
                Map<String, Delivery_Schedule__c> mapOldDS = new Map<String, Delivery_Schedule__c>();
                for(Delivery_Schedule__c dsOld : quotes.Delivery_Schedule__r){
                    
                    System.debug('---Delivery Schedule old---');
                    
                    if(mapServiceWords.get(dsOld.Service__r.Name) == null){
                        lstDelDS.add(dsOld);
                    }
                    mapOldDS.put(dsOld.Service__r.Name,dsOld);
                    dsOldServices.add(dsOld.Service__r.Name);
                }
                lstServiceId.addAll(setServiceId);
                if(quotes.Quote_Line_Items__r.size() > 0){
                    if(quotes.Quote_Line_Items__r[0].Actual_Words__c == null){
                        quotes.Quote_Line_Items__r[0].Actual_Words__c = 0;
                        quotes.Quote_Line_Items__r[0].Back_Translation_Total_Words__c = 0;
                    }
                    if(quotes.Quote_Line_Items__r[0].DTP_Hours__c == null){
                        quotes.Quote_Line_Items__r[0].DTP_Hours__c = 0;
                    }
                    Quote__c quoteUpdate = new Quote__c( id=quotes.id ) ;
                    totWordCount = quotes.Total_Word_Count__c;
                    totDTPHrs = quotes.Total_Quote_DTP_Hours__c;
                    dtpDays = totDTPHrs/8;
                    dtpDaysRoundUp = dtpDays.round(system.roundingMode.CEILING);
                    minDays = /*dtpDaysRoundUp + */(totWordCount/2000) + (totWordCount/8000) + (totWordCount/16000);
                    if(minDays == 0){
                        dtpWeeks = 0;
                    }else if(minDays > 12){
                        dtpWeeks = minDays/5;
                    }else{
                        dtpWeeks = 0;
                    }
                    if(minDays == 0){
                        days2 = 0;
                    }else if(dtpWeeks == 0){
                        days2 = minDays + 1;
                    }else{
                        days2 = dtpWeeks + 1;
                    }
                    if(minDays == 0){
                        days1 = '';
                    }else if(dtpWeeks == 0){
                        days1 = minDays.round(system.roundingMode.CEILING) + ' to ' + days2.round(system.roundingMode.CEILING);
                    }else{
                        days1 = dtpWeeks.round(system.roundingMode.CEILING) + ' to ' + days2.round(system.roundingMode.CEILING);
                    }
                    if(minDays < 3){
                        standardDeliverySchedule = '3 to 4 business day(s)';
                        //    expeditedDeliverySchedule = '2 to 3 business days';
                        expeditedDeliverySchedule = '2 business day(s)';
                    }else if(dtpWeeks == 0){
                        standardDeliverySchedule = days1+' business day(s)';
                        //expeditedDeliverySchedule = (minDays.round(system.roundingMode.CEILING)-1) + ' to ' + (days2.round(system.roundingMode.CEILING)-1)+' business days';
                        expeditedDeliverySchedule = (minDays.round(system.roundingMode.CEILING)-1) + ' business day(s)';
                        
                    }else{
                        standardDeliverySchedule = days1+' business week(s)';
                        //  expeditedDeliverySchedule = (dtpWeeks.round(system.roundingMode.CEILING)-1) + ' to ' + (days2.round(system.roundingMode.CEILING)-1)+' business weeks';
                        expeditedDeliverySchedule = (dtpWeeks.round(system.roundingMode.CEILING)-1) + ' business week(s)';
                        
                    }
                    if(minDays == 0){
                        btTime = '';
                    }else if(dtpWeeks == 0){
                        btTime = (minDays+1.0).round(system.roundingMode.CEILING)+' to '+(days2+1).round(system.roundingMode.CEILING);
                    }else{
                        btTime = (dtpWeeks+1).round(system.roundingMode.CEILING)+' to '+(days2+1).round(system.roundingMode.CEILING);
                    }
                    if(minDays < 3){
                        btStandardDeliverySchedule = '4 to 5 business day(s)';
                        //   btExpeditedDeliverySchedule = '3 to 4 business days';
                        btExpeditedDeliverySchedule = '3 business day(s)';
                    }else if(dtpWeeks == 0){
                        btStandardDeliverySchedule = btTime+' business day(s)';
                        // btExpeditedDeliverySchedule = (minDays).round(system.roundingMode.CEILING)+' to '+(days2).round(system.roundingMode.CEILING)+' business days';
                        btExpeditedDeliverySchedule = (minDays).round(system.roundingMode.CEILING)+' business day(s)';
                        
                    }else{
                        btStandardDeliverySchedule = btTime+' business week(s)';
                        //  btExpeditedDeliverySchedule = (dtpWeeks).round(system.roundingMode.CEILING)+' to '+(days2).round(system.roundingMode.CEILING)+' business weeks';
                        btExpeditedDeliverySchedule = (dtpWeeks).round(system.roundingMode.CEILING)+' business week(s)';
                        
                    }
                    for(Client_Service__c service : lstClientServices){
                        if(mapServiceWords.get(service.Name) != null){
                            
                            System.debug('---Delivery Schedule Code flow 1---');
                            
                            if(dsOldServices.contains(service.Name)){
                                System.debug('---Delivery Schedule Code flow 2---');
                                Delivery_Schedule__c ds1 = mapOldDS.get(service.Name);
                                ds = new Delivery_Schedule__c(id=ds1.id);
                            }else{
                                ds = new Delivery_Schedule__c();
                                ds.Quote_Id__c = quotes.Id;
                                ds.Service__c = service.Id;
                            }
                            if(service.IsOutsourced__c){
                                Decimal dsVal = mapServiceWords.get(service.Name);
                                dsVal = dsVal/8;
                                dsVal = dsVal.round(system.roundingMode.CEILING);
                                String dsValue;
                                String expeditedDSVal;
                                if(dsVal != 0){
                                    dsValue = dsVal+' business day(s)';
                                    expeditedDSVal = (dsVal-1)+' business day(s)';
                                }else{
                                    dsValue = '1 business day';
                                    expeditedDSVal = '1 business day';
                                }
                                ds.System_Expedited_Delivery_Schedule__c = expeditedDSVal;
                                ds.System_Standard_Delivery_Schedule__c = dsValue;
                                ds.Standard_Delivery_Schedule__c = dsValue;
                                ds.Expedited_Delivery_Schedule__c = expeditedDSVal;
                                //ds = null;
                            }else if(service.Name.contains('Desktop Publishing')){
                                if(dtpDays > 1){
                                    ds.System_Standard_Delivery_Schedule__c = dtpDaysRoundUp+' business day(s)';
                                    ds.System_Expedited_Delivery_Schedule__c = (dtpDaysRoundUp-1)+' business day(s)';
                                    ds.Standard_Delivery_Schedule__c = dtpDaysRoundUp+' business day(s)';
                                    ds.Expedited_Delivery_Schedule__c = (dtpDaysRoundUp-1)+' business day(s)';
                                }
                                else{
                                    ds.System_Standard_Delivery_Schedule__c = '2 business day(s)';
                                    ds.System_Expedited_Delivery_Schedule__c = '1 business day(s)';
                                    ds.Standard_Delivery_Schedule__c = '2 business day(s)';
                                    ds.Expedited_Delivery_Schedule__c = '1 business day(s)';
                                }
                            }else if(btCS.Id == service.Id & !service.IsTWCLogic__c){
                                ds.System_Expedited_Delivery_Schedule__c = btExpeditedDeliverySchedule;
                                ds.System_Standard_Delivery_Schedule__c = btStandardDeliverySchedule;
                                ds.Standard_Delivery_Schedule__c = btStandardDeliverySchedule;
                                ds.Expedited_Delivery_Schedule__c = btExpeditedDeliverySchedule;
                                
                            }else if(service.Name.equalsIgnoreCase('Translation') || service.IsTWCLogic__c){
                                ds.System_Expedited_Delivery_Schedule__c = expeditedDeliverySchedule;
                                ds.System_Standard_Delivery_Schedule__c = standardDeliverySchedule;
                                ds.Standard_Delivery_Schedule__c = standardDeliverySchedule;
                                ds.Expedited_Delivery_Schedule__c = expeditedDeliverySchedule;
                            }
                            else{
                                Decimal dsVal = mapServiceWords.get(service.Name);
                                dsVal = dsVal/8;
                                dsVal = dsVal.round(system.roundingMode.CEILING);
                                String dsValue;
                                String expeditedDSVal;
                                if(dsVal != 0){
                                    dsValue = dsVal+' business day(s)';
                                    expeditedDSVal = (dsVal-1)+' business day(s)';
                                }else{
                                    dsValue = '1 business day';
                                    expeditedDSVal = '1 business day';
                                }
                                ds.System_Expedited_Delivery_Schedule__c = expeditedDSVal;
                                ds.System_Standard_Delivery_Schedule__c = dsValue;
                                ds.Standard_Delivery_Schedule__c = dsValue;
                                ds.Expedited_Delivery_Schedule__c = expeditedDSVal;
                            }
                            
                            if(ds != null){
                                lstDS.add(ds);
                            }
                        }
                    }
                }
            }
            if(lstDS != null && lstDS.size()>0)
            Database.upsert(lstDS);
            
            if(lstDelDS != null && lstDelDS.size()>0)
            Database.delete(lstDelDS);
        }
        
        /* Code for calculating the delivery schedule for the Quote : end*/
    }
    if(Trigger.isAfter && Trigger.isInsert){
        List<Id> lstClientId = new List<Id>();
        List<Id> parentQuoteId = new List<Id>();
        List<Id> allQuotesId = new List<Id>();
        //GLSCloneQuoteController gls;
        for(Quote__c quote : trigger.new){
            lstClientId.add(quote.Client__c);
            if(quote.Original_Quote_Id__c != null){
                parentQuoteId.add(quote.Original_Quote_Id__c);
            }
            allQuotesId.add(quote.Id);
            /*gls = new GLSCloneQuoteController();
            gls.cloneQuote(quote.id);*/
        }
        
        List<Quote__c> lstQuoteUpdate = new List<Quote__c>();
        List<Account> lstClients = new List<Account>();
        List<Quote__c> lstQuotes = new List<Quote__c>();
        List<Quote__c> lstQuotesCHO = new List<Quote__c>();
        List<Quote__c> lstQuotesRO = new List<Quote__c>();
        if(lstClientId != null && lstClientId.size()>0)
        lstClients = [Select Id,(Select Order_on_Quote__c, Standard_SOW_Item_Id__c, Standard_SOW_Item_Id__r.Standard_SOW_Item_Text__c,Standard_SOW_Item_Id__r.Standard_SOWItemText__c,
        Standard_SOW_Item_Id__r.Standard_SOW_Item_Display_Name__c
        From Client_Default_SOW_Items_Tables__r),
        (Select Order_on_Quote__c, Standard_Term_Id__c,Standard_Term_Id__r.Standard_Term_Text__c,
        Standard_Term_Id__r.Standard_Term_Name__c From Client_Default_Terms_Items_Tables__r order by Order_on_Quote__c)
        From Account Where Id in: lstClientId];
        
        if(parentQuoteId != null && parentQuoteId.size()>0){
            lstQuotes = [Select Id,Name,Quote_Number__c From Quote__c Where Id in: parentQuoteId];
            lstQuotesCHO = [Select Id,Quote_Version__c From Quote__c Where Original_Quote_Id__c in: parentQuoteId And Change_Order__c = true Order by Quote_Version__c DESC Limit 1];
            lstQuotesRO = [Select Id,Quote_Version__c From Quote__c Where Original_Quote_Id__c in: parentQuoteId And Revised_Order__c = true Order by Quote_Version__c DESC Limit 1];
            
        }
        List<RFQ_SOW_Items_Table__c> lstRfqSow = new List<RFQ_SOW_Items_Table__c>();
        List<RFQ_Terms_Table__c> lstRfqTerms = new List<RFQ_Terms_Table__c>();
        
        try{
            
            for(Quote__c quote : trigger.new){
                for(Account acc : lstClients){
                    if(quote.Client__c == acc.id){
                        RFQ_SOW_Items_Table__c rfqSOW = new RFQ_SOW_Items_Table__c();
                        RFQ_Terms_Table__c rfqTerm = new RFQ_Terms_Table__c();
                        for(Client_Default_SOW_Items_Table__c defSOW : acc.Client_Default_SOW_Items_Tables__r){
                            rfqSOW = new RFQ_SOW_Items_Table__c();
                            rfqSOW.RFQ_SOW_Item_Description__c = defSOW.Standard_SOW_Item_Id__r.Standard_SOW_Item_Text__c;
                            rfqSOW.RFQ_SOWItemDescription__c = defSOW.Standard_SOW_Item_Id__r.Standard_SOWItemText__c;
                            rfqSOW.RFQ_SOW_Item_Name__c = defSOW.Standard_SOW_Item_Id__r.Standard_SOW_Item_Display_Name__c;
                            rfqSOW.RFQ_Id__c = quote.id;
                            rfqSOW.RFQ_SOW_Item_Order__c = defSOW.Order_on_Quote__c;
                            lstRfqSow.add(rfqSOW);
                        }
                        for(Client_Default_Terms_Table__c defterms : acc.Client_Default_Terms_Items_Tables__r){
                            rfqTerm = new RFQ_Terms_Table__c();
                            rfqTerm.RFQ_Id__c = quote.id;
                            rfqTerm.RFQ_Term_Description__c = defterms.Standard_Term_Id__r.Standard_Term_Text__c;
                            rfqTerm.RFQ_Term_Order__c = defterms.Order_on_Quote__c;
                            rfqTerm.RFQ_Term_Name__c = defterms.Standard_Term_Id__r.Standard_Term_Name__c;
                            lstRfqTerms.add(rfqTerm);
                        }
                    }
                }
                Quote__c updateQuote ;
                for(Quote__c parentQuote : lstQuotes){
                    Decimal Version = 0;
                    String tempVersion ='';
                    updateQuote = new Quote__c(id=quote.Id);
                    if(quote.Revised_Order__c && quote.Original_Quote_Id__c == parentQuote.Id){
                        for(Quote__c revisedQuote : lstQuotesRO){
                            Version = lstQuotesRO[0].Quote_Version__c+1;
                        }
                        if(Version < 10){
                            tempVersion = 0+''+Version ;
                        }else{
                            tempVersion = Version+'';
                        }
                        updateQuote.Quote_Version__c = Version;
                        updateQuote.Quote_Number__c = parentQuote.Quote_Number__c+'-R'+tempVersion;
                    }else if (quote.Change_Order__c && quote.Original_Quote_Id__c == parentQuote.Id){
                        for(Quote__c ChangOrderQuote : lstQuotesCHO){
                            Version = lstQuotesCHO[0].Quote_Version__c+1;
                        }
                        if(Version < 10){
                            tempVersion = 0+''+Version ;
                        }else{
                            tempVersion = Version+'';
                        }
                        updateQuote.Quote_Version__c = Version;
                        updateQuote.Quote_Number__c = parentQuote.Quote_Number__c+'-CHO'+tempVersion;
                    }else{
                        String qName = quote.Name;
                        updateQuote.Quote_Number__c = qName.substring(2,qName.Length());
                    }
                    lstQuoteUpdate.add(updateQuote);
                }
            }
            
            if(lstQuoteUpdate != null && lstQuoteUpdate.size()>0)
            Database.update(lstQuoteUpdate);
            if(lstRfqSow != null && lstRfqSow.size()>0)
            Database.insert(lstRfqSow);
            if(lstRfqTerms != null && lstRfqTerms.size()>0)
            Database.insert(lstRfqTerms);
            
        }catch(Exception e){
            system.debug(e);
        }
    }
    
    if(Trigger.isBefore && Trigger.isInsert){
    	
        for(Quote__c quote : trigger.new){
        	if(quote.Urgency__c == GLSconfig.expeditedDelivery){
    			quote.Expedited__c = true;
    		}
    		else{
    			quote.Expedited__c = false;
    		}
            if(quote.Rate_Sheet__c == null){
                List<Account_Rate_Sheet__c> listAccRateSheet = new List<Account_Rate_Sheet__c>();
                String clientId = quote.Client__c;
                list<Account> tempacc = [Select Name, Id from Account where Id =: quote.Client__c];
                if(tempacc != null && tempacc.size()>0){
                    String accName = tempacc[0].Name;
                    listAccRateSheet=   [ select Rate_Sheet__r.Id,Rate_Sheet__r.Name from Account_Rate_Sheet__c where (Account__c =: clientId and Rate_Sheet__r.Name= :accName)];
                }
                if(listAccRateSheet!= null && listAccRateSheet.size()>0){
                    Account_Rate_Sheet__c accRateSheet = listAccRateSheet[0];
                    quote.Rate_Sheet__c=accRateSheet.Rate_Sheet__r.Id;
                }else{
                    listAccRateSheet= [ select Rate_Sheet__r.Id,Rate_Sheet__r.Name from Account_Rate_Sheet__c where (Account__c =: clientId and Rate_Sheet__r.Name= 'Standard')];
                    if(listAccRateSheet != null && listAccRateSheet.size()>0){
                        Account_Rate_Sheet__c accRateSheet = listAccRateSheet[0];
                        quote.Rate_Sheet__c=accRateSheet.Rate_Sheet__r.Id;
                    }else{
                        quote.Rate_Sheet__c=null;
                    }
                }
            }
        }
        
        for(Quote__c quote : trigger.new){
            if(quote.Signature_Block_Text__c != ''){
                quote.Signature_Block_Text__c = 'GLS requires receipt of all materials for the execution of desired services and clarifying the requirements in a timely manner to ensure the work pipeline is maintained. Any delays due to non-compliance thereof will exonerate GLS for non-deliverance proportionally to the time it takes client to clarify the requirements and for the sections of the work affected by the requirements.<br><br>I have read and accept the terms and conditions of this proposal and authorize work to begin on the services described. Proposal can be faxed back to (949) 606-9367.';
                }
        }
    }
    
    if(Trigger.isBefore && Trigger.isUpdate){
    	for(Quote__c quote : trigger.new){
    		if(quote.Urgency__c == GLSconfig.expeditedDelivery){
    			quote.Expedited__c = true;
    		}
    		else{
    			quote.Expedited__c = false;
    		}
    		if(quote.Expedited__c){
    			quote.Urgency__c = GLSconfig.expeditedDelivery;
    		}
    		else{
    			quote.Urgency__c = GLSconfig.standardDelivery;
    		}
    	}
    }
    
}