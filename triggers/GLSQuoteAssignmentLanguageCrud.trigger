/* Copyright (c) 2013, Global Languages Solutions, Inc. All rights reserved.
* Trigger   :  GLSQuoteAssignmentLanguageCrud trigger
* Function: Populate Services and the Language pairs in the particular format in the Quote Assignment object field Quote Task Description 
*/


trigger GLSQuoteAssignmentLanguageCrud on Quote_Assignment_Language__c (after insert,after update,after delete) {
  	list<Quote_Assignment_Language__c> lstQuoteAL = new list<Quote_Assignment_Language__c>();
  	list<Id> lstQAssignmentId = new list<Id>();
  	list<Quote_Assignment__c> lstQAssignment = new list<Quote_Assignment__c>();
  	list<Quote_Assignment_Service__c> lstQuoteAS= new list<Quote_Assignment_Service__c>();
  	public Map<id,Quote_Assignment__c> mapQuoteAssignment = new Map<id,Quote_Assignment__c>();
  	public map<Id,String> mapQALName = new map<Id,String>();
 	
 	if(Trigger.isAfter && Trigger.isInsert && !GLSQW_CloneFileController.skipTrigger && !GLSAmazonUtility.skipTrigger){    
		Set<id> setQALId = new Set<id>();
		for(Quote_Assignment_Language__c qal:Trigger.New){
			lstQAssignmentId.add(qal.Quote_Assignment__c);
			setQALId.add(qal.Id);
		}
		system.debug('-------setQALId-------'+setQALId);	
	  	if(lstQAssignmentId != null && lstQAssignmentId.size()>0){
			List<Quote_Assignment__c> lstQASsg = [Select isAssignmentUpdated__c, Quote_Task_Description__c, Delivery_Format__c,Source_Language__r.Name, 
			                                      Display_Languages_Without_Dialect__c,Certified__c,(Select Client_Service__r.Name, Service_Name__c,Client_Service__r.Order__c 
			                                      From Quote_Assignment_Services__r Order by Client_Service__r.Order__c, Name), 
			                                      (Select Language_List_Item__c,Language_List_Item__r.Name,Language_List_Item__r.Order__c 
			                                       From Quote_Assignment_Languages__r order by Language_List_Item__r.Name) 
			                                      From Quote_Assignment__c Where Id in: lstQAssignmentId];
			Set<string> lstName = new Set<string>();
			List<Quote_Assignment_Language__c> lstQALDel = new List<Quote_Assignment_Language__c>();
			for(Quote_Assignment__c qa : lstQASsg){
				for(Quote_Assignment_Language__c qal : qa.Quote_Assignment_Languages__r){
					mapQALName.put(qal.Id,qal.Language_List_Item__r.Name);
				}
			}
			system.debug('-------mapQALName-------'+mapQALName);
			if(lstQALDel != null && lstQALDel.size() > 0){
				Database.delete(lstQALDel);
			}
			mapQuoteAssignment = updateQuoteTaskDescription(lstQASsg);
			if(mapQuoteAssignment.size()>0)
				update mapQuoteAssignment.values();
		}
  	}
  
  	if(Trigger.isAfter && Trigger.isUpdate && !GLSQW_CloneFileController.skipTrigger && !GLSAmazonUtility.skipTrigger){
    	mapQuoteAssignment= new Map<id,Quote_Assignment__c>();	  
     	Quote_Assignment__c qa= new Quote_Assignment__c();
		for(Quote_Assignment_Language__c qal: Trigger.new){
			Quote_Assignment_Language__c oldQal = Trigger.oldMap.get(qal.Id);
	    	if (qal.Language_List_Item__c != oldQal.Language_List_Item__c){
		    	qa = new Quote_Assignment__c(id=qal.Quote_Assignment__c);
		        qa.isAssignmentUpdated__c=true;
		        mapQuoteAssignment.put(qa.id,qa);
	        }    
     	}
     	if(mapQuoteAssignment.size()>0)
	     	update mapQuoteAssignment.values();
    }	 	  
  	
  	if(Trigger.isAfter && Trigger.isDelete && !GLSQW_CloneFileController.skipTrigger && !GLSAmazonUtility.skipTrigger){
    	mapQuoteAssignment= new Map<id,Quote_Assignment__c>();
    	List<Id> lstQuoteAssgId = new List<Id>();
		for(Quote_Assignment_Language__c qal: Trigger.old){
			lstQuoteAssgId.add(qal.Quote_Assignment__c);
		}
		if(lstQuoteAssgId != null && lstQuoteAssgId.size()>0){
			List<Quote_Assignment__c> lstQASsg = [Select isAssignmentUpdated__c, Quote_Task_Description__c, Delivery_Format__c,Source_Language__r.Name, 
			                                      Display_Languages_Without_Dialect__c,Certified__c, (Select Client_Service__r.Name,Client_Service__r.Order__c, Service_Name__c 
			                                      From Quote_Assignment_Services__r Order by Client_Service__r.Order__c, Name), 
			                                      (Select Language_List_Item__c,Language_List_Item__r.Name,Language_List_Item__r.Order__c 
			                                      From Quote_Assignment_Languages__r Order by Language_List_Item__r.Name) 
			                                      From Quote_Assignment__c Where Id in: lstQuoteAssgId];
			mapQuoteAssignment = updateQuoteTaskDescription(lstQASsg);
			if(mapQuoteAssignment.size()>0)
				update mapQuoteAssignment.values();
		}
    } 
    /*Method to update the Quote Task Description field of Quote Assignment*/
    
    public Map<id,Quote_Assignment__c> updateQuoteTaskDescription(List<Quote_Assignment__c> lstQASsg){
    	Map<id,Quote_Assignment__c> mapQuoteAssignmentUpdate = new Map<id,Quote_Assignment__c>();
    	for(Quote_Assignment__c qa : lstQASsg){
			list<string> lstService= new List<string>();
		  	for(Quote_Assignment_Service__c qas:qa.Quote_Assignment_Services__r){
		    	lstService.add(qas.Client_Service__r.Name);
		  	}
		  	set<string> settargetLanguage = new set<String>();
	  		list<string> lstTargetLanguage= new List<string>();
		  	for(Quote_Assignment_Language__c qal:qa.Quote_Assignment_Languages__r){
		  		String tmpStr; 
		  		if(!qa.Display_Languages_Without_Dialect__c){
		  			tmpStr = qal.Language_List_Item__r.Name;
		  			if(tmpStr.contains('(')){
		  				integer indexOfbrackets = tmpStr.indexOf('(');
		  				settargetLanguage.add(tmpStr.substring(0,indexOfbrackets-1));
		  			}else{
		  				settargetLanguage.add(qal.Language_List_Item__r.Name);
		  			}
		  		}else{
		      		settargetLanguage.add(qal.Language_List_Item__r.Name);
		  		}
		  	}	
		  	lstTargetLanguage.addAll(settargetLanguage);
		  	lstTargetLanguage.sort();
	  		string listOfService = '';
		  	if(lstService.size() == 1){
		    	listOfService = lstService[0];
		    }else if(lstService.size() == 2){
		    	listOfService = listOfService+lstService[0]+' and '+lstService[1];
		    }else if(lstService.size()> 2){
		    	for(integer i = 0; i < lstService.size();i++){
		        	if(i != lstService.size()-1){
		            	listOfService = listOfService +lstService[i]+', ';
		           	}
		           	if(i == lstService.size()-1){
		            	integer index = listOfService.lastIndexOf(',');
		            	listOfService = listOfService.substring(0,index)+', and '+lstService[i];
		          	}
		       	}
		   	}
		   	string listOfLanguage = '';    
		   	if(lstTargetLanguage.size() == 1){
		    	listOfLanguage = lstTargetLanguage[0]; 
		    }else if(lstTargetLanguage.size() == 2){
		       	listOfLanguage = listOfLanguage+lstTargetLanguage[0]+' and '+lstTargetLanguage[1];
		    }else if(lstTargetLanguage.size()> 2){
		       	for(integer i = 0; i < lstTargetLanguage.size();i++){
		        	if(i != lstTargetLanguage.size()-1){
		            	listOfLanguage = listOfLanguage +lstTargetLanguage[i]+', ';
		          	}
		          	if(i == lstTargetLanguage.size()-1){
		             	integer index = listOfLanguage.lastIndexOf(',');
		             	listOfLanguage = listOfLanguage.substring(0,index)+', and '+lstTargetLanguage[i];
		          	}
		       	}
		   	}
		   	String sourceLang = '';
		   	if(!qa.Display_Languages_Without_Dialect__c){
		   		String tmpStr = qa.Source_Language__r.Name;
		  			if(tmpStr.contains('(')){
		  				integer indexOfbrackets = tmpStr.indexOf('(');
		  				sourceLang = tmpStr.substring(0,indexOfbrackets-1);
		  			}else{
		  				sourceLang = qa.Source_Language__r.Name;
		  			}
		   	}else{
		   		sourceLang = qa.Source_Language__r.Name;
		   	}
		   	string quoteTaskDesc ;
		   	if(qa.Certified__c){
		   		quoteTaskDesc =  'Certified '+listOfService +' of the following document(s) from '+sourceLang+' into '+listOfLanguage+':\n'; 
		   	}else{
		   		quoteTaskDesc =  listOfService +' of the following document(s) from '+sourceLang+' into '+listOfLanguage+':\n'; 
		   	}
	       	qa.Quote_Task_Description__c = quoteTaskDesc;
	      	qa.isAssignmentUpdated__c = true;
		   	mapQuoteAssignmentUpdate.put(qa.id,qa);
		}
		return mapQuoteAssignmentUpdate;
    }
}