//&& newTraining.Training_Required_for__c != ''
trigger LMS_TrainingTrigger on Training__c (after insert, after update, before insert, 
before update) 
{

    public List<User> usersToBeEnrolled;
    public Profile portalProfile;
    public List<User_Training__c> userEnrollmentRecords;
    public List<User_Training__c> enrollmentsToBeDeleted;
    public List<User_Training__c> enrollmentsNotDeleted;
    public List<User_Training__c> completedTrainingUsers;
    public User_Training__c singleUserEnrollment; 
    public List<id> newTrainingList;
//  public RecordType trainingRT;
    public Map<Id,User> allUsers;
    public Map<Id,User_Training__c> allUserTrainings; 
    public Map<Id,Set<String>> mapString;

   
    
    /* 
    Automatically enroll all the users if their training profile in mentioned as in 'Training Required For' field of a new training
    /**/    
    if(Trigger.isInsert && Trigger.isAfter) {
    	
        userEnrollmentRecords = new List<User_Training__c>(); 
        singleUserEnrollment = new User_Training__c();
        allUsers = new Map<Id,User>([ select Id, Name, Training_Profile__c from User where Training_Profile__c != null and IsActive = true]);
        system.debug('---allUsers---: '+allUsers);
        
        for (Training__c newTraining : Trigger.new ) {
            if( allUsers != null && allUsers.size() > 0 ) {
                for (User currentUser : allUsers.values()) {
                    String trainingProfile = '';
                    if(currentUser.Training_Profile__c != null)
                        trainingProfile = currentUser.Training_Profile__c;
                    if(newTraining.Training_Required_for__c != null && newTraining.Training_Required_for__c != '' && newTraining.Training_Required_for__c.contains(trainingProfile) && newTraining.Active__c == true )
                     {                              
                        system.debug('---inside if -- Insert ');
                        Decimal  duration =0;
                        singleUserEnrollment = new User_Training__c();
                        if(newTraining.End_Date__c != null)
                        {
                            if(newTraining.Duration__c != null)
                                duration = newTraining.Duration__c;
                            if((Date.today().addDays(Integer.valueOf(duration))) <=  newTraining.End_Date__c)
                            {
                                
                                if( Date.today().addDays(Integer.valueOF(newTraining.Criterion__c)) <
                                 newTraining.End_Date__c )
                                {
                                        singleUserEnrollment.Due_Date__c = Date.today().addDays(Integer.valueOF(newTraining.Criterion__c));
                                }
                                else
                                {
                                        singleUserEnrollment.Due_Date__c = newTraining.End_Date__c;
                                }
                            }
                            
                        }
                        else{
                            singleUserEnrollment.Due_Date__c =Date.today().addDays(Integer.valueOf(newTraining.Criterion__c));
                        }
                        if(singleUserEnrollment.Due_Date__c != null){
                            singleUserEnrollment.User_Name__c = currentUser.id;
                            singleUserEnrollment.Training__c = newTraining.Id;
                            singleUserEnrollment.Status__c = LMS_Literals.InitialEnrollmentStatus;
                            singleUserEnrollment.Date_of_Enrollment__c = Date.today();
                        
                            userEnrollmentRecords.add(singleUserEnrollment);
                        }
                    }
                }           
            }
        }
        if (userEnrollmentRecords != null && userEnrollmentRecords.size() > 0) {
            insert userEnrollmentRecords;
        } 
    }

   
    if(Trigger.isUpdate && Trigger.isAfter) {
       // portalProfile = new Profile();
        userEnrollmentRecords = new List<User_Training__c>(); 
        enrollmentsToBeDeleted = new List<User_Training__c>(); 
        enrollmentsNotDeleted = new List<User_Training__c>();
        singleUserEnrollment = new User_Training__c();
        completedTrainingUsers = new List<User_Training__c>();
        List<User_Training__c> lstUpdateUserTraining = new List<User_Training__c>();
        newTrainingList =new  List<Id>();
        mapString = new Map<Id,Set<String>>();
         List<id> userIdlist = new List<id>();
        
    	for(Training__c newTraining : Trigger.new ){
    			newTrainingList.add(newTraining.id);
    		}
    	
    	completedTrainingUsers = [select id,Training__c,User_Name__c from User_Training__c where (status__c = 'Completed' or status__c = 'Feedback Pending')  and  Training__c in :newTrainingList];    
        
        for(User_Training__c utraining: completedTrainingUsers){
    	 	userIdlist.add(utraining.User_Name__c);
    	 }
    	 	
        allUsers = new Map<Id,User>([ select Id, Name, Training_Profile__c from User where Training_Profile__c!= null and  IsActive = true and  id NOT in:userIdlist]);
        
        allUserTrainings = new Map<Id,User_Training__c>([ select Id, Name, Training__c,Status__c, User_name__r.Training_Profile__c ,User_name__c from User_Training__c  where Training__c in :newTrainingList]);
        
        for (Training__c newTraining : Trigger.new ) {
        	set<string> username =new set<string>();
        	
        	for(User_Training__c userT: allUserTrainings.values()){
        			if(newTraining.id==userT.Training__c)
        				username.add(userT.User_name__c);
        	}
        	mapString.put(newTraining.Id, username);
        }
        
        for (Training__c newTraining : Trigger.new ) {
            for (Training__c oldTraining : Trigger.old ) {
                if(newTraining.Id == oldTraining.Id && 
                    (((newTraining.Training_Required_for__c != oldTraining.Training_Required_for__c ||  newTraining.Active__c != oldTraining.Active__c ) &&
                    		(newTraining.Criterion__c != oldTraining.Criterion__c ||  newTraining.Duration__c != oldTraining.Duration__c ||  newTraining.End_Date__c != oldTraining.End_Date__c) )||
                    				(newTraining.Training_Required_for__c != oldTraining.Training_Required_for__c ||  (newTraining.Active__c != oldTraining.Active__c && newTraining.Active__c == true) ))) {
                    
		                   system.debug('---Inside if----: '+mapString); 
		                  
		                        if (allUsers.size() > 0) {
                    					for (User currentUser : allUsers.values()) {
                    						
                    					set<string> tempSet=mapString.get(newTraining.id);
                    					 
                    					 		for (User_Training__c delUserTrainings : allUserTrainings.values()) {
                    					 		  if(delUserTrainings.Training__c==newTraining.id){ 	
                    					 			 	
			                            				if(delUserTrainings.Status__c != 'Completed' && delUserTrainings.Status__c != 'Feedback Pending'  && delUserTrainings.User_name__c == currentUser.Id && !newTraining.Training_Required_for__c.contains(currentUser.Training_Profile__c)) {
			                              		  			system.debug('*****inside If*****');
			                              		  			enrollmentsToBeDeleted.add(delUserTrainings);
			                            				}
		                            					
                    					 		  }
                    					 		}  	
		                            			 if(!tempSet.contains(currentUser.Id) && newTraining.Training_Required_for__c.contains(currentUser.Training_Profile__c) && newTraining.Active__c == true){
			                            					system.debug('*****inside else*****');
			                            					String trainingProfile='';	
							                                singleUserEnrollment = new User_Training__c();
							                                singleUserEnrollment.Due_Date__c = userTrainingCreator(newTraining);
							                                if(singleUserEnrollment.Due_Date__c != null){
							                                    singleUserEnrollment.User_Name__c = currentUser.id;
							                                    singleUserEnrollment.Training__c = newTraining.Id;
							                                    singleUserEnrollment.Status__c = LMS_Literals.InitialEnrollmentStatus;
							                                    singleUserEnrollment.Date_of_Enrollment__c = Date.today();                          
							                                    userEnrollmentRecords.add(singleUserEnrollment);
							                                }
		                            				}	
		                            					
                    							
		                            		
		                        		}
		                    		}
              				}
               else if((newTraining.Id == oldTraining.Id ) && ((newTraining.Criterion__c != oldTraining.Criterion__c ||  newTraining.Duration__c != oldTraining.Duration__c ||  newTraining.End_Date__c != oldTraining.End_Date__c))){
                	  system.debug('---Inside else----: '); 
                	  if(allUserTrainings.size() > 0) {
                        for (User_Training__c updateUserTrainings : allUserTrainings.values()) {
                            if( updateUserTrainings.Training__c == newTraining.Id && updateUserTrainings.Status__c != 'Completed') {
                            		User_Training__c uTr = new User_Training__c(Id = updateUserTrainings.Id );
                            	    uTr.Due_Date__c = userTrainingCreator(newTraining);
                    				lstUpdateUserTraining.add(uTr);
                            }
                         }
                	  }
                }
            }
        }
        system.debug('*****userEnrollmentRecords********' +userEnrollmentRecords);
        if (userEnrollmentRecords != null && userEnrollmentRecords.size() > 0) {
            insert userEnrollmentRecords;
        }
        if (lstUpdateUserTraining != null && lstUpdateUserTraining.size() > 0) {
            update lstUpdateUserTraining;
        }
         system.debug('*******enrollmentsToBeDeleted******' +enrollmentsToBeDeleted);
        if (enrollmentsToBeDeleted != null && enrollmentsToBeDeleted.size()>0) {
            delete enrollmentsToBeDeleted;
        }
    } 
     
      private Date userTrainingCreator(Training__c newTraining){
        system.debug('---inside userTrainingCreator----');
               Decimal  duration =0;
               Date dueDate;
	           if(newTraining.End_Date__c != null)
	            {
	                if(newTraining.Duration__c != null)
	                    duration = newTraining.Duration__c;
                    if((Date.today().addDays(Integer.valueOf(duration))) <=  newTraining.End_Date__c){
                        if( Date.today().addDays(Integer.valueOF(newTraining.Criterion__c)) <  newTraining.End_Date__c )
                        {
                             dueDate = Date.today().addDays(Integer.valueOf(newTraining.Criterion__c));
                        }
                        else{
                             dueDate = newTraining.End_Date__c;
                        }
                    }
	            }
	            else
	            {
	            
	                dueDate =Date.today().addDays(Integer.valueOF(newTraining.Criterion__c));
	            }
	            
	         return dueDate;
      }
}