trigger GLSQW_ClientDefaultTermTableCRUD on Client_Default_Terms_Table__c (after insert, before delete, after update) {
	set<id> clientid= new set<id>();
	set<id> parentid= new set<id>();
	set<id >cdttid= new set <id>();
	list<Account> lstAccount = new list<Account>();
	Client_Default_Terms_Table__c clonecdtt = new Client_Default_Terms_Table__c();
	list<Client_Default_Terms_Table__c> lstClientDefaultTermTable= new list<Client_Default_Terms_Table__c>();
	
	list<Client_Default_Terms_Table__c> updateLstClientDefaultTermTable= new list<Client_Default_Terms_Table__c>();
		if(trigger.isInsert){
			for(Client_Default_Terms_Table__c cdtt : trigger.new){
				if(cdtt.Client_Id__c !=null){
				//	clonecdtt =Trigger.newmap.get(cdtt.Id);
					
					clientid.add(cdtt.Client_Id__c);
				}
			}
		
		system.debug('line17'+clientid + 'inserted value'+clonecdtt);
		if(clientid != null && clientid.size()>0){
			
			lstAccount = [select id from Account where 	Parentid in : clientid];
		}
		if(lstAccount != null && lstAccount.size()>0){
			for(Account acc : lstAccount){
				
				parentid.add(acc.id);
			}
		}
		system.debug('line28'+parentid);
	  if(parentid != null && parentid.size()>0){
	  	
	  	lstClientDefaultTermTable = [select id,Client_Id__c,Standard_Term_Id__c from Client_Default_Terms_Table__c where Client_Id__c  in :parentid];
	  	
	  }
		 if(lstClientDefaultTermTable != null && lstClientDefaultTermTable.size()>0){
		  for(Client_Default_Terms_Table__c cdtt2 :lstClientDefaultTermTable){
		 
		 	cdttid.add(cdtt2.Client_Id__c);
		 	}
		  }
	 if(lstAccount != null && lstAccount.size()>0){
	 	for(Client_Default_Terms_Table__c cdtt : trigger.new){  
			 for(Account acc1 :lstAccount ){
			 	
			 		clonecdtt =Trigger.newmap.get(cdtt.Id);
				 if(lstClientDefaultTermTable != null && lstClientDefaultTermTable.size()>0){
				 
				 	boolean check1=false;
				 	for(Client_Default_Terms_Table__c cdtt1 :lstClientDefaultTermTable){
					 	if(acc1.id==cdtt1.Client_Id__c){
					 		Client_Default_Terms_Table__c	temp = new Client_Default_Terms_Table__c();
					 			if(check1== false){
					 				if(clonecdtt.Standard_Term_Id__c ==cdtt1.Standard_Term_Id__c){
					 					check1=true;
					 			    }
					 		   }
					 	 }
				 	}
				 	
				 	if(check1==false){
				 				Client_Default_Terms_Table__c newcdtt= new Client_Default_Terms_Table__c();
				 			newcdtt.Client_Id__c=acc1.id;
				 			newcdtt.Order_on_Quote__c=clonecdtt.Order_on_Quote__c;
				 			newcdtt.Standard_Term_Id__c=clonecdtt.Standard_Term_Id__c;
				 			
				 				updateLstClientDefaultTermTable.add(newcdtt);
				 	}
				 }
		   }
	 
		 }
	  }
	 system.debug('line45'+updateLstClientDefaultTermTable);
		 if(updateLstClientDefaultTermTable !=null && updateLstClientDefaultTermTable.size()>0){
		 	try{
		 		Insert updateLstClientDefaultTermTable;
		 	}
		 	catch (Exception e){
		 		system.debug('-----Exception-----'+ e.getmessage());
		 		
		 	}
		 	
		 }
	}
	if(trigger.isdelete){
			for(Client_Default_Terms_Table__c cdtt : trigger.old){
				if(cdtt.Client_Id__c !=null){
					//clonecdtt =Trigger.oldmap.get(cdtt.Id);
				
					clientid.add(cdtt.Client_Id__c);
				}
			}
		
		system.debug('line17'+clientid + 'inserted value'+clonecdtt);
		if(clientid != null && clientid.size()>0){
			
			lstAccount = [select id from Account where 	Parentid in : clientid];
		}
		if(lstAccount != null && lstAccount.size()>0){
			for(Account acc : lstAccount){
				
				parentid.add(acc.id);
			}
		}
		system.debug('line28'+parentid);
	  if(parentid != null && parentid.size()>0){
	  	
	  	lstClientDefaultTermTable = [select id,Client_Id__c,Standard_Term_Id__c from Client_Default_Terms_Table__c where Client_Id__c  in :parentid];
	  	
	  }
	 if(lstAccount != null && lstAccount.size()>0){
	 	for(Client_Default_Terms_Table__c cdtt : trigger.old){
	 	clonecdtt =Trigger.oldmap.get(cdtt.Id);
			for(Account acc1 :lstAccount ){
					 if(lstClientDefaultTermTable != null && lstClientDefaultTermTable.size()>0){
					 	
					 	for(Client_Default_Terms_Table__c cdtt1 :lstClientDefaultTermTable){
					 		if(acc1.id==cdtt1.Client_Id__c){
					 			if(clonecdtt.Standard_Term_Id__c ==cdtt1.Standard_Term_Id__c){
					 			
					 				updateLstClientDefaultTermTable.add(cdtt1);
					 			}
					 		}
					 	  }
					 	}
					 }
		  }
	
	 }
	 system.debug('line45'+updateLstClientDefaultTermTable);
		 if(updateLstClientDefaultTermTable !=null && updateLstClientDefaultTermTable.size()>0){
		 	try{
		 		delete updateLstClientDefaultTermTable;
		 	}
		 	catch (Exception e){
		 		system.debug('-----Exception-----'+ e.getmessage());
		 		
		 	}
		 	
		 }
		
		
	}
/*	if(trigger.isupdate){
		for(Client_Default_Terms_Table__c cdtt : trigger.new){
			if(Trigger.newmap.get(cdtt.Id)!=Trigger.oldmap.get(cdtt.Id) && Trigger.newmap.get(cdtt.Id)!=null){
				if(cdtt.Client_Id__c !=null){
				//	clonecdtt =Trigger.newmap.get(cdtt.Id);
					
					clientid.add(cdtt.Client_Id__c);
				}
				}
			}
		
		system.debug('line17'+clientid + 'inserted value'+clonecdtt);
		if(clientid != null && clientid.size()>0){
			
			lstAccount = [select id from Account where 	Parentid in : clientid];
		}
		if(lstAccount != null && lstAccount.size()>0){
			for(Account acc : lstAccount){
				
				parentid.add(acc.id);
			}
		}
		system.debug('line28'+parentid);
	  if(parentid != null && parentid.size()>0){
	  	
	  	lstClientDefaultTermTable = [select id,Client_Id__c,Standard_Term_Id__c from Client_Default_Terms_Table__c where Client_Id__c  in :parentid];
	  	
	  }
		 if(lstClientDefaultTermTable != null && lstClientDefaultTermTable.size()>0){
		  for(Client_Default_Terms_Table__c cdtt2 :lstClientDefaultTermTable){
		 
		 	cdttid.add(cdtt2.Client_Id__c);
		 	}
		  }
	 if(lstAccount != null && lstAccount.size()>0){
	 	for(Client_Default_Terms_Table__c cdtt : trigger.new){  
			 for(Account acc1 :lstAccount ){
			 	
			 		clonecdtt =Trigger.newmap.get(cdtt.Id);
					 if(lstClientDefaultTermTable != null && lstClientDefaultTermTable.size()>0){
					 
					 	boolean check1=false;
						 for(Client_Default_Terms_Table__c cdtt1 :lstClientDefaultTermTable){
							 	
						 		if(acc1.id==cdtt1.Client_Id__c){
							 			if(clonecdtt.Standard_Term_Id__c ==cdtt1.Standard_Term_Id__c){
							 						//Client_Default_Terms_Table__c newcdtt= new Client_Default_Terms_Table__c();
								 			cdtt1.Client_Id__c=clonecdtt.Client_Id__c;
								 			cdtt1.Order_on_Quote__c=clonecdtt.Order_on_Quote__c;
								 			cdtt1.Standard_Term_Id__c=clonecdtt.Standard_Term_Id__c;
						 					updateLstClientDefaultTermTable.add(cdtt1);
							 			}
						  	    }
						}
			       }
	 
		  }
	   }
	 }
	 system.debug('line45'+updateLstClientDefaultTermTable);
		 if(updateLstClientDefaultTermTable !=null && updateLstClientDefaultTermTable.size()>0){
		 	try{
		 		update updateLstClientDefaultTermTable;
		 	}
		 	catch (Exception e){
		 		system.debug('-----Exception-----'+ e.getmessage());
		 		
		 	}
		 	
		 }
		
		
		
		
	}*/
}