/* Copyright (c) 2013, Global Languages Solutions, Inc. All rights reserved.
* Trigger   :  GLSQuoteAssignmentFileCrud trigger
* Function: This trigger perform following task: 
            1)Populated delivery format in Quote Assignment object depending upon the extension of the file attached to Quote Assignment File object 
            2)Populated the value in field Quote Reference Files Instruction of Quote Assignment
            3)Check if the duplicate file is attached to the Quote Assignment File
*/
trigger GLSQuoteAssignmentFileCrud on Quote_Assignment_File__c (before insert,after insert,after update,after delete) {
  public Set<Id> setQuoteAssId= new Set<Id>();
  public List<Id> lstQuoteAssId= new List<Id>();
  public List<Quotes_File__c> lstAssFileName= new List<Quotes_File__c>();
  public List<Id> lstQuoteFileId= new List<Id>();
  public List<Quotes_File__c> lstQuoteFile= new List<Quotes_File__c>();
  public List<Quote_Assignment_File__c> lstQAssignmentFile= new list<Quote_Assignment_File__c>();
  public Map<Id,string> mapQuoteFile= new Map<Id,String>();
  public Map<Id,set<string>> mapQuoteFileFormat= new Map<Id,set<String>>();
  public Map<id,Quote_Assignment__c>mapQuoteAssignment=New Map<id,Quote_Assignment__c>();
  public Map<string,Id>mapQuoteFileNameAssignment=New Map<string,Id>();
  public Map<string,String>mapQuoteFileUsageAssignment=New Map<string,String>();
  public Map<string,Decimal> mapQFSize=New Map<string,Decimal>();
  public List<string> lstQaFileId = New List<string>();
  string fileName  = '';
  string fileUsage  = '';
  string fileExtension = '';
  string ErrorExtension = '';
  string fileErrorExtension = '';
  string fileAlreradyPresent = '';
  string fileExtValue = '';
  Decimal fileSize;
  Decimal QFileSize;
  String sizeLimitErrorMessage ='';
  String FileExtErrorMessage ='';
  String fileAlreadyerrorMessage ='';
  String qAssigId ='';
  public Map<id,decimal> Quotesfilesize=New Map<id,decimal>();
  
  if(Trigger.isBefore && Trigger.isInsert && !GLSQW_CloneFileController.skipTrigger){
    for(Quote_Assignment_File__c qaf: Trigger.new){
             lstQuoteAssId.add(qaf.Quote_Assignment__c);
             lstQaFileId.add(qaf.Quote_Files__c);
          }
      if(lstQuoteAssId.size()>0){
           lstQAssignmentFile=[select id,Quote_Assignment__c,Quote_Files__r.File_Name__c, File_Usage__c from Quote_Assignment_File__c where Quote_Assignment__c in:lstQuoteAssId];
      } 
      if(lstQAssignmentFile.size()>0){
        for(Quote_Assignment_File__c file:lstQAssignmentFile){
               mapQuoteFileNameAssignment.put(file.Quote_Files__r.File_Name__c,file.Quote_Assignment__c);
               mapQuoteFileUsageAssignment.put(file.Quote_Files__r.File_Name__c,file.File_Usage__c);
        }
      }
    
     if(lstQaFileId!= null && lstQaFileId.size()>0){
        lstAssFileName = [ Select Id, File_Name__c, File_Size__c From Quotes_File__c Where Id in : lstQaFileId];
      }
      Map<Id,String> mapQuoteFiles = new Map<Id,String>(); 
      for(Quotes_File__c obj : lstAssFileName){
            Quotesfilesize.put(obj.Id,obj.File_Size__c);
            mapQuoteFiles.put(obj.Id, obj.File_Name__c);
      } 
        
      List<Trados_Supported_Files__c> lstTSF = Trados_Supported_Files__c.getall().values();
      Map<String, String> mapFileExt = new Map<String, String>();
      Map<String, Decimal> mapFileSize = new Map<String, Decimal>();
      for(Trados_Supported_Files__c TSF: lstTSF){
            mapFileExt.put(TSF.Extension_Name__c.toLowerCase(),TSF.Name);
            mapFileSize.put(TSF.Extension_Name__c.toLowerCase(),TSF.File_Size_Limit__c);
      }
      
        
      for(Quote_Assignment_File__c qaf: Trigger.new){
          fileName= mapQuoteFileNameAssignment.get(mapQuoteFiles.get(qaf.Quote_Files__c));
          fileUsage = mapQuoteFileUsageAssignment.get(mapQuoteFiles.get(qaf.Quote_Files__c));
          fileExtension = mapQuoteFiles.get(qaf.Quote_Files__c);
          QFileSize = Quotesfilesize.get(qaf.Quote_Files__c);
          if(fileExtension !=null)
             fileExtension=fileExtension.substring(fileExtension.lastIndexOf('.'),fileExtension.length()); 
             fileExtension = fileExtension.toLowerCase();
          if(fileName != null && fileUsage == qaf.File_Usage__c){
                if(fileAlreradyPresent == '')
                    fileAlreradyPresent = mapQuoteFiles.get(qaf.Quote_Files__c);
                else
                    fileAlreradyPresent +=','+mapQuoteFiles.get(qaf.Quote_Files__c);
                if(fileAlreradyPresent != '')
                    fileAlreadyerrorMessage = 'File with the name "'+fileAlreradyPresent+'" is already associated with this Quote Assignment'   ;       
          }else{   
                if(qaf.File_Usage__c == 'Source'){
                 	
                    fileExtValue= mapFileExt.get(fileExtension);
                    fileSize = mapFileSize.get(fileExtension);
                    if(QFileSize > fileSize){
                        if(ErrorExtension == '')
                            ErrorExtension = mapQuoteFiles.get(qaf.Quote_Files__c);
                        else
                            ErrorExtension +=','+mapQuoteFiles.get(qaf.Quote_Files__c);
                     if(ErrorExtension != '')
                        sizeLimitErrorMessage = 'File Size is too large for "'+ErrorExtension+'" to be processed in Studio.';
                    }else if(fileExtValue == null){
                        //qaf.Quote_Files__c.addError('File with the extension "'+fileExtension+'" is not supported in Trados');
                        if(fileErrorExtension == '')
                            fileErrorExtension = fileExtension;
                        else fileErrorExtension +=','+fileExtension;
                       if (fileErrorExtension != '')
                        FileExtErrorMessage = 'File with the extension "'+fileErrorExtension+'" is not supported in Trados';
                    }
                }
            }

      }
     if(sizeLimitErrorMessage !='' || fileAlreadyerrorMessage !='' || FileExtErrorMessage !=''){
     for(Quote_Assignment_File__c qaf: Trigger.new){
            qaf.Quote_Files__c.addError(sizeLimitErrorMessage+'\n'+fileAlreadyerrorMessage+'\n'+FileExtErrorMessage);
            break;  
        }   
     }   
   } 
    
      
    if(Trigger.isAfter && Trigger.isInsert && !GLSQW_CloneFileController.skipTrigger ){   
        Map<string,string> mapQuoteAssgFormat= new Map<string,string>();
        for(Quote_Assignment_File__c qaf: Trigger.new){
            setQuoteAssId.add(qaf.Quote_Assignment__c);
        }
        list<Quote_Assignment__c> lstQuoteAssignment=[select id,Allowed_File_Formats__c,Quote__c from Quote_Assignment__c where id in: setQuoteAssId];
            for(Quote_Assignment__c qa:lstQuoteAssignment){
                mapQuoteAssgFormat.put(qa.id,qa.Allowed_File_Formats__c);
        }
        lstQuoteAssId.addAll(setQuoteAssId);
        if(lstQuoteAssId.size()>0){
            lstQAssignmentFile=[select id,Quote_Assignment__c,Quote_Files__r.File_Name__c,File_Usage__c from Quote_Assignment_File__c 
                                where Quote_Assignment__c in:lstQuoteAssId and File_Usage__c =: 'Client'];
        } 
        for(Quote_Assignment_File__c qaf:lstQAssignmentFile){
            lstQuoteFileId.add(qaf.Quote_Files__c);
        }
        lstQuoteFile= [select id,File_Format__r.Name from Quotes_File__c where id in:lstQuoteFileId];
        List<String> lstFormat= new list<String>(); 
        Set<String> setTemp= new Set<String>();
        for(Quotes_File__c qf: lstQuoteFile){
            setTemp.add(qf.File_Format__r.Name);
        }
        string  myString;
        string fileFormat='';
        Quote_Assignment__c qa = new Quote_Assignment__c();
        set<id> setAssgId = new set<id>();
        for(Quote_Assignment_File__c qaf: Trigger.new){                 
            lstFormat.clear();
            string str = mapQuoteAssgFormat.get(qaf.Quote_Assignment__c);
            
            if(str != null){
                list<string> lstCurrFormat = str.split(';');
                for(string s : lstCurrFormat){
                    setTemp.add(s);
                }
            }    
            if(mapQuoteAssignment.get(qaf.Quote_Assignment__c) == null)
                qa = new Quote_Assignment__c(id = qaf.Quote_Assignment__c);
            lstFormat.addAll(setTemp);
            myString = String.join(lstFormat, ';');
           // qa.Allowed_File_Formats__c = myString;
            if(lstFormat.size() == 1){
                fileFormat = lstFormat[0]; 
            }
            else if(lstFormat.size() == 2){
                fileFormat = fileFormat+lstFormat[0]+' and '+lstFormat[1];
            }
            else if(lstFormat.size()> 2){
                for(integer i = 0; i < lstFormat.size(); i++){
                    if(i != lstFormat.size()-1){
                        fileFormat = fileFormat +lstFormat[i]+' , ';
                    }
                    if(i == lstFormat.size()-1){
                        integer index = fileFormat.lastIndexOf(',');
                        fileFormat = fileFormat.substring(0,index)+' and '+lstFormat[i];
                    }
                }
            }
            qa.Delivery_Format__c = fileFormat;
            if(qaf.File_Usage__c == 'Reference'){
                qa.Quote_Reference_Files_Instruction__c = 'Using the following Reference File(s):\n';
            } 
            qa.isAssignmentUpdated__c = true;
            if(qaf.File_Usage__c == 'Source'){
                qa.isFileAssociated__c = true;
            } 
            mapQuoteAssignment.put(qa.id,qa);
            fileFormat = '';
        }
        if(mapQuoteAssignment.size()>0)
            try{
               upsert mapQuoteAssignment.values();
            }catch(DMLException de){
                system.debug(GLSConfig.DMLExceptionOccured+de);
            }
            catch(Exception e){
                system.debug(GLSConfig.ExceptionOccured+e);
            }
    }
    if(Trigger.isAfter && Trigger.isDelete && !GLSQW_CloneFileController.skipTrigger ){
        mapQuoteAssignment= new Map<id,Quote_Assignment__c>();
        //Set<String> setQAssigId = new Set<String>();
        Set<String> setQId = new Set<String>();
        List<Quote__c> lstqId = new List<Quote__c>();
        List<Quote_Assignment__c> lstqaId = new List<Quote_Assignment__c>();
        List<Quote_Assignment_File__c> lstqaFiles = new List<Quote_Assignment_File__c>();
        Quote_Assignment__c qa= new Quote_Assignment__c();
        boolean fileFlag= false;
        for(Quote_Assignment_File__c qaf: Trigger.old){
        //  setQAssigId.add(qaf.Quote_Assignment__c);
            qAssigId = qaf.Quote_Assignment__c;
            qa = new Quote_Assignment__c(id=qaf.Quote_Assignment__c);
            qa.isAssignmentUpdated__c=true;
            mapQuoteAssignment.put(qa.id,qa);
        }
        if(qAssigId != '' ){
            lstqaFiles = [Select Id, File_Usage__c from Quote_Assignment_File__c where Quote_Assignment__c =: qAssigId];
            Quote_Assignment__c qaObj= new Quote_Assignment__c(id=qAssigId);
            if(lstqaFiles.size()==0){
                qaObj.isFileAssociated__c = false;
                qaObj.isAssignmentUpdated__c=true;
                mapQuoteAssignment.put(qaObj.id,qaObj);
            }else{
                for(Quote_Assignment_File__c qaFiles: lstqaFiles){
                    if(qaFiles.File_Usage__c=='Source'){
                        fileFlag = true;
                    }
                }
                if(fileFlag){
                    qaObj.isFileAssociated__c = true;
                }else{
                    qaObj.isFileAssociated__c = false;
                }
                qaObj.isAssignmentUpdated__c=true;
                mapQuoteAssignment.put(qaObj.id,qaObj);
            }
        }
        if(mapQuoteAssignment.size()>0)
            try{
                update mapQuoteAssignment.values();
            }catch(DMLException de){
                system.debug(GLSConfig.DMLExceptionOccured+de);
            }
            catch(Exception e){
                system.debug(GLSConfig.ExceptionOccured+e);
            }
    }
}