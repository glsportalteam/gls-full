trigger GLSQuotePDFFileCRUD on Quote_PDF_File__c (before delete, before update) {
    
    /**
     *  The below code will prevent the user to Edit the Quote PDF if the Quote Staus is either one of following status
     *  Darft, New, In Preparation, In Complete, In Review and Reviewed
     */
     if(Trigger.isBefore && Trigger.isDelete && GLSconfig.skipQuotePDFFileTrigger){
        List<String> quoteId = new List<String> (); 
        Map<Id, Quote__c> mapQuote;
        Map<String, Quote_PDF_File__c> mapQuotePDF = new Map<String, Quote_PDF_File__c>();
        for(Quote_PDF_File__c qPDF: Trigger.old){
            quoteId.add(qPDF.Quote_ID__c);
            mapQuotePDF.put(qPDF.File_Name__c, qpdf);
        }
        
        List<Attachment> lstAttachment = [SELECT Id, Name FROM Attachment WHERE ParentId IN : quoteId];
        
        if(quoteId != null && quoteId.size()>0){
            list<String> quoteStatus = GLSConfig.ValidQuoteStatus;
            mapQuote = new Map <Id, Quote__c>([Select Id, Status__c From Quote__c Where Id in : quoteId and Status__c not in:quoteStatus]) ;
        }
        
        for(Quote_PDF_File__c qPDFFiles: Trigger.old){
            if (mapQuote != null &&  mapQuote.ContainsKey(qPDFFiles.Quote_ID__c)){
                qPDFFiles.addError('Delete Quote PDF File is not allowed at this Quote status.');
            }
            else{
                for(Attachment att : lstAttachment){
                    if(mapQuotePDF.ContainsKey(att.Name)){
                        delete att;
                    }
                }
            }
        }
     }
     
   
    /**
     *  The below code will prevent the user to Edit the Quote File if the Quote Staus is either one of following status
     *  Darft, New, In Preparation, In Complete, In Review and Reviewed
     */
     if(Trigger.isBefore && Trigger.isUpdate){
        List<String> quoteId = new List<String> ();
        Map<Id, Quote__c> mapQuote;
        for(Quote_PDF_File__c qPDF: Trigger.new){
            quoteId.add(qPDF.Quote_ID__c);
         }
         
        if(quoteId != null && quoteId.size()>0){
            list<String> quoteStatus = GLSConfig.ValidQuoteStatus;
            mapQuote = new Map <Id, Quote__c>([Select Id, Status__c From Quote__c Where Id in : quoteId and Status__c not in :quoteStatus]) ;
        }
        
        for(Quote_PDF_File__c qPDFFiles: Trigger.new){
            if (mapQuote != null &&  mapQuote.ContainsKey(qPDFFiles.Quote_ID__c))
                qPDFFiles.addError('Edit Quote PDF File is not allowed at this Quote status.');
        }
     }

}