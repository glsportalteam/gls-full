trigger GLSQW_AccountRateSheetCRUD on Account_Rate_Sheet__c (after insert, before delete) {

	set<id> clientid= new set<id>();
	set<id> parentid= new set<id>();
	set<id >cdttid= new set <id>();
	list<Account> lstAccount = new list<Account>();
	 Account_Rate_Sheet__c clonecdtt = new  Account_Rate_Sheet__c();
	list<  Account_Rate_Sheet__c> lstClientDefaultTermTable= new list<  Account_Rate_Sheet__c>();
	
	list< Account_Rate_Sheet__c> updateLstClientDefaultTermTable= new list< Account_Rate_Sheet__c>();
		if(trigger.isInsert){
			for( Account_Rate_Sheet__c cdtt : trigger.new){
				if(cdtt.Account__c !=null){
					clonecdtt =Trigger.newmap.get(cdtt.Id);
					
					clientid.add(cdtt.Account__c);
				}
			}
		
		system.debug('line17'+clientid + 'inserted value'+clonecdtt);
		if(clientid != null && clientid.size()>0){
			
			lstAccount = [select id from Account where 	Parentid in : clientid];
		}
		if(lstAccount != null && lstAccount.size()>0){
			for(Account acc : lstAccount){
				
				parentid.add(acc.id);
			}
		}
		system.debug('line28'+parentid);
	  if(parentid != null && parentid.size()>0){
	  	
	  	lstClientDefaultTermTable = [select id,Account__c ,Rate_Sheet__c  from  Account_Rate_Sheet__c where Account__c  in :parentid];
	  	
	  }
		 if(lstClientDefaultTermTable != null && lstClientDefaultTermTable.size()>0){
		  for( Account_Rate_Sheet__c cdtt2 :lstClientDefaultTermTable){
		 
		 	cdttid.add(cdtt2.Account__c);
		 	}
		  }
	 if(lstAccount != null && lstAccount.size()>0){
	 	for( Account_Rate_Sheet__c cdttt : trigger.new){
	 		clonecdtt =Trigger.newmap.get(cdttt.Id);
			 for(Account acc1 :lstAccount ){
				 if(lstClientDefaultTermTable != null && lstClientDefaultTermTable.size()>0){
				 
				 	boolean check1=false;
				 	for( Account_Rate_Sheet__c cdtt1 :lstClientDefaultTermTable){
					 	if(acc1.id==cdtt1.Account__c){
					 		Account_Rate_Sheet__c	temp = new  Account_Rate_Sheet__c();
					 			if(check1== false){
					 				if(clonecdtt.Rate_Sheet__c ==cdtt1.Rate_Sheet__c){
					 					check1=true;
					 			    }
					 		   }
					 	 }
				 	}
				 	
				 	if(check1==false){
				 			 Account_Rate_Sheet__c newcdtt= new  Account_Rate_Sheet__c();
				 			newcdtt.Account__c=acc1.id;
				 			newcdtt.Price_Group_Rates__c=clonecdtt.Price_Group_Rates__c;
				 			newcdtt.Rate_Sheet__c=clonecdtt.Rate_Sheet__c;
				 			
				 				updateLstClientDefaultTermTable.add(newcdtt);
				 	}
				 }
		     }
	
	 	}
	 }
	 system.debug('line45'+updateLstClientDefaultTermTable);
		 if(updateLstClientDefaultTermTable !=null && updateLstClientDefaultTermTable.size()>0){
		 	try{
		 		Insert updateLstClientDefaultTermTable;
		 	}
		 	catch (Exception e){
		 		system.debug('-----Exception-----'+ e.getmessage());
		 		
		 	}
		 	
		 }
	}
	if(trigger.isdelete){
			for( Account_Rate_Sheet__c cdtt : trigger.old){
				
				if(cdtt.Account__c !=null){
					//clonecdtt =Trigger.oldmap.get(cdtt.Id);
				
					clientid.add(cdtt.Account__c);
				}
			}
		
		system.debug('line17'+clientid + 'inserted value'+clonecdtt);
		if(clientid != null && clientid.size()>0){
			
			lstAccount = [select id from Account where 	Parentid in : clientid];
		}
		if(lstAccount != null && lstAccount.size()>0){
			for(Account acc : lstAccount){
				
				parentid.add(acc.id);
			}
		}
		system.debug('line28'+parentid);
	  if(parentid != null && parentid.size()>0){
	  	
	  	lstClientDefaultTermTable = [select id,Account__c,Rate_Sheet__c from  Account_Rate_Sheet__c where Account__c  in :parentid];
	  	
	  }
	 if(lstAccount != null && lstAccount.size()>0){
	 	for( Account_Rate_Sheet__c cdttt : trigger.old){
	 		clonecdtt = new  Account_Rate_Sheet__c();
	 		clonecdtt =Trigger.oldmap.get(cdttt.Id);
		for(Account acc1 :lstAccount ){
				 if(lstClientDefaultTermTable != null && lstClientDefaultTermTable.size()>0){
				 	
				 	for( Account_Rate_Sheet__c cdtt1 :lstClientDefaultTermTable){
				 		if(acc1.id==cdtt1.Account__c){
				 			if(clonecdtt.Rate_Sheet__c ==cdtt1.Rate_Sheet__c){
				 			
				 				updateLstClientDefaultTermTable.add(cdtt1);
				 			}
				 		}
				 	  }
				 	}
				 }
		  }
	 }
	 
	 system.debug('line45'+updateLstClientDefaultTermTable);
		 if(updateLstClientDefaultTermTable !=null && updateLstClientDefaultTermTable.size()>0){
		 	try{
		 		delete updateLstClientDefaultTermTable;
		 	}
		 	catch (Exception e){
		 		system.debug('-----Exception-----'+ e.getmessage());
		 		
		 	}
		 	
		 }
		
		
	}	
}