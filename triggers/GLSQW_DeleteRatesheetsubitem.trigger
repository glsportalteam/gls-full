trigger GLSQW_DeleteRatesheetsubitem on Rate_Sheet__c (before delete) {
    public set<id> deletedrecordids= new set<id>();
    public set<id> deletedrecordids1= new set<id>();
    public set<id> deletedrecordids2= new set<id>();
    public list<Rate_Sheet_Item__c> Listratesheetsubitm= new list<Rate_Sheet_Item__c>();
    public list<Rate_Sheet_Subitem__c> delListratesheetsubitm= new list<Rate_Sheet_Subitem__c>();
    public list <Sub_Item_Trados_Category__c> Listsubitemtadoscatagory= new list<Sub_Item_Trados_Category__c>();
    public list<Account_Rate_Sheet__c> delaccount = new list<Account_Rate_Sheet__c>();
    for(Rate_Sheet__c rsi :trigger.old){
        deletedrecordids.add(rsi.id);
    }
    if(deletedrecordids != null && deletedrecordids.size()>0){
    delaccount=[select id ,Rate_Sheet__c from Account_Rate_Sheet__c where Rate_Sheet__c =: deletedrecordids];
        if(delaccount  !=null && delaccount.size()>0){
            try{
                delete delaccount;
            }
            catch(Exception e){
                System.debug('Exception e'+ e.getmessage());
            }
        }
    }
    
    system.debug('linee9'+deletedrecordids);
    if(deletedrecordids != null && deletedrecordids.size()>0){
        
        Listratesheetsubitm = [select id from  Rate_Sheet_Item__c where  Rate_Sheet__c in : deletedrecordids];
        
    }
    
    system.debug('line12'+Listratesheetsubitm);
    if(Listratesheetsubitm !=null && Listratesheetsubitm.size()>0){
        for(Rate_Sheet_Item__c rsheetitem :Listratesheetsubitm){
        deletedrecordids1.add(rsheetitem.id);
        }
    }
    system.debug('line12'+deletedrecordids1);
    if(deletedrecordids1  != null && deletedrecordids1.size()>0){
        
        delListratesheetsubitm =    [select id from Rate_Sheet_Subitem__c where Rate_Sheet_Item__c in: deletedrecordids1 ]; 
        
    }
    if(delListratesheetsubitm !=null && delListratesheetsubitm.size()>0){
        for(Rate_Sheet_Subitem__c rssi :delListratesheetsubitm){
        system.debug('line30'+ rssi.id);
        deletedrecordids2.add(rssi.id);
        }
      //  delete delListratesheetsubitm;
    }
    system.debug('line34'+ deletedrecordids2);
    if(deletedrecordids2 != null && deletedrecordids2.size()>0){
        
        Listsubitemtadoscatagory    = [select id from Sub_Item_Trados_Category__c where Rate_Sheet_Subitem__c in  :deletedrecordids2];
        system.debug('line40'+ Listsubitemtadoscatagory);
    }
        if(Listsubitemtadoscatagory !=null && Listsubitemtadoscatagory.size()>0){
            try{
                delete Listsubitemtadoscatagory;
            }
            catch( Exception e){
            
            system.debug('----Exception'+ e.getmessage());
            }
            
        }
   if(delListratesheetsubitm !=null && delListratesheetsubitm.size()>0){
   try{
       delete delListratesheetsubitm;
       }
       catch (Exception e){
       system.debug('------Exception e------');
       
       }
   }
    
}