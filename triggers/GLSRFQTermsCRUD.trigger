trigger GLSRFQTermsCRUD on RFQ_Terms_Table__c (before update, after insert) {
    Map<id,Quote__c> mapQuote= new Map<id,Quote__c>();
    list<string> lstQuote= new list<string>();
    map<string,string> mapTemp= new map<string,string>(); 
    if(Trigger.isBefore && trigger.isUpdate){ 
        RFQ_Terms_Table__c oldTerm = new RFQ_Terms_Table__c();      
        for(RFQ_Terms_Table__c termItem:trigger.new){
            oldTerm = Trigger.oldMap.get(termItem.Id);
            if(oldTerm.RFQ_Term_Description__c != termItem.RFQ_Term_Description__c || oldTerm.RFQ_Term_Order__c != termItem.RFQ_Term_Order__c){
                termItem.RFQ_Term_Modified__c = true;
            }
        }
    }
    if(Trigger.isAfter && trigger.isInsert){
      	map<Id,RFQ_Terms_Table__c> mapQuoteId = new map<Id,RFQ_Terms_Table__c>();
      	for(RFQ_Terms_Table__c termItem:trigger.new){
      		mapQuoteId.put(termItem.RFQ_Id__c,termItem);
      	}
      	Set<Id> setQuoteId = new Set<Id>();
      	setQuoteId = mapQuoteId.keySet();
      	map<Id,Quote__c> mapQuoteUpdate = new map<Id,Quote__c>();
      	List<Quote__c> lstQuotes = [Select Id,Terms__c From Quote__c Where Id in: setQuoteId];
      	string termsValue='';
      	for(Quote__c quote : lstQuotes){
      		if(quote.Terms__c != null)
      			termsValue = quote.Terms__c;
      		for(RFQ_Terms_Table__c termItem:trigger.new){
      			if(termItem.RFQ_Id__c == quote.id){
      				termsValue = termsValue + termItem.RFQ_Term_Description__c + '\n\n';
      				
      			}
      		}
      		Quote__c quoteUpdate = new Quote__c(id=quote.id);
        	quoteUpdate.terms__c=termsValue;
        	mapQuoteUpdate.put(quoteUpdate.Id,quoteUpdate);
      	}
      	List<Database.Saveresult> lstResult =  Database.update(mapQuoteUpdate.values());
    }
}