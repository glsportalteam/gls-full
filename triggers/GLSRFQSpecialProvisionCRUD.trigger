trigger GLSRFQSpecialProvisionCRUD on RFQ_Special_Provisions__c (before update, before insert) {
	
	List<Standard_Special_Provisions__c> lstStandardSP = new List<Standard_Special_Provisions__c>();
	List<Id> lstStandardSPId = new List<Id>();
	
	if(Trigger.isBefore && Trigger.isInsert){		
		for(RFQ_Special_Provisions__c rfqSp : Trigger.new){
			lstStandardSPId.add(rfqSp.Standard_Special_Provisions__c);
		}
		lstStandardSP = [ Select Special_Provision_Text__c, Special_Provision_Name__c, Id 
		                  From Standard_Special_Provisions__c Where Id in: lstStandardSPId];
		for(RFQ_Special_Provisions__c rfqSp : Trigger.new){
			for(Standard_Special_Provisions__c ssp : lstStandardSP){
				if(rfqSp.Standard_Special_Provisions__c == ssp.Id){
					rfqSp.RFQ_Special_Provision_Name__c = ssp.Special_Provision_Name__c;
					rfqSp.RFQ_Special_Provision_Text__c = ssp.Special_Provision_Text__c;
				}
			}
		}
	}
	
	if(Trigger.isBefore && Trigger.isUpdate){
		RFQ_Special_Provisions__c oldSp = new RFQ_Special_Provisions__c();
		for(RFQ_Special_Provisions__c rfqSp : Trigger.new){
			oldSp = Trigger.oldMap.get(rfqSp.Id);
			if(oldSp.Standard_Special_Provisions__c != rfqSp.Standard_Special_Provisions__c){
				lstStandardSPId.add(rfqSp.Standard_Special_Provisions__c);
			}
			if(oldSp.RFQ_Special_Provision_Name__c != rfqSp.RFQ_Special_Provision_Name__c || oldSp.RFQ_Special_Provision_Text__c != rfqSp.RFQ_Special_Provision_Text__c){
				rfqSp.RFQ_Special_Provision_Modified__c = true;
			}
		}
		lstStandardSP = [ Select Special_Provision_Text__c, Special_Provision_Name__c, Id 
		                  From Standard_Special_Provisions__c Where Id in: lstStandardSPId];
		for(RFQ_Special_Provisions__c rfqSp : Trigger.new){
			for(Standard_Special_Provisions__c ssp : lstStandardSP){
				if(rfqSp.Standard_Special_Provisions__c == ssp.Id){
					rfqSp.RFQ_Special_Provision_Name__c = ssp.Special_Provision_Name__c;
					rfqSp.RFQ_Special_Provision_Text__c = ssp.Special_Provision_Text__c;
				}
			}
		}
	}
}