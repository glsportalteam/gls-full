trigger GLSQuoteLineItemCRUD on Quote_Line_Item__c (after update,after insert,before update, before insert) {

    Quote_Line_Item__c oldQLI = new Quote_Line_Item__c();
    public string addUnitMeasure;
    
    if(Trigger.isAfter && Trigger.isUpdate && !GLSQW_RateSummaryController.isNotExecutQLITrigger){
        try{
        	
        	 System.debug('---Abhishek--QLI Trigger' );
        	
            List<Quote_Line_Item_Subline__c> tempLstQLISubline = new List<Quote_Line_Item_Subline__c>();
            List<Id> quoteId = new List<Id>();
            List<Id> qliId = new List<Id>();
            List<Id> rsId = new List<Id>();
            List<Id> langId = new List<Id>();
            map<string,decimal> mapRateSummary= new map<string,decimal>();
            Map<string,Decimal> mapRateSummaryDefault = new Map<string,Decimal>();
            map<String,Rate_Sheet_Item__c> mapRSIName = new map<String,Rate_Sheet_Item__c>();
            boolean isRateSheetItemExist = false;
            
            for(Quote_Line_Item__c qli : trigger.New){
                quoteId.add(qli.QuoteId__c);
                qliId.add(qli.Id);
            }
            List<Quote__c> lstQuote = [Select Id,Rate_Sheet__c From Quote__c Where Id in: quoteId];
            List<Rate_Sheet_Item__c> lstRateSheetItems = new List<Rate_Sheet_Item__c>();
            List<Rate_Sheet_Subitem__c> lstRateSheetSI = new List<Rate_Sheet_Subitem__c>();
            List<Sub_Item_Trados_Category__c> lstSubItemTC = new List<Sub_Item_Trados_Category__c>();
            for(Quote__c q : lstQuote){ 
                rsId.add(q.Rate_Sheet__c);
            }
            list<Rate_Summary_Line_Item_Master__c> lstRSLIM= [select id,Default_Value__c,Quantity__c,Default_Order__c,Rate__c,Trados_Category__c from Rate_Summary_Line_Item_Master__c];
            for(Rate_Summary_Line_Item_Master__c rslim:lstRSLIM){
                mapRateSummary.put(rslim.Trados_Category__c,rslim.Default_Order__c);
                if(rslim.Default_Value__c != null)
                       mapRateSummaryDefault.put(rslim.Trados_Category__c,rslim.Default_Value__c);
            }
            List<Quote_Line_Item__c> lstQLI = [Select Id,QuoteId__c,Source_Language_ID__r.Language_Name__c,Actual_Words__c,Back_Translation_Total_Words__c,Target_Language_ID__r.Language_Name__c,
                                                Context_TM_Word_Count__c,DTP_Hours__c,Subline_Total_Amount__c,Target_Language_ID__r.Name,
                                               X95_99_Word_Count__c,X85_95_Word_Count__c,X75_84_Word_Count__c,X50_74_Word_Count__c,No_Match_Word_Count__c,
                                               Perfect_Match__c,Crossfile_Repetitions__c,X100_Word_Count__c,Repetitions_Word_Count__c,
                                               (Select Id,Unit_Price__c,Description__c,Quantity__c From Quote_Line_Item_Sublines__r),
                                              (Select Service_Id__c,Service_Id__r.Unit_of_Measure__c, Service_Id__r.IsTWCLogic__c,Service_Id__r.IsOutsourced__c,Service_Id__r.Additional_Percentage__c,Service_Id__r.Name From Quote_Line_Services__r) 
                                               From Quote_Line_Item__c Where Id in: qliId];   
            for(Quote_Line_Item__c qli : lstQLI){
                if(qli.Target_Language_ID__r.Name.contains('English'))
                langId.add(qli.Source_Language_ID__c);              
             else	
                langId.add(qli.Target_Language_ID__c);
            }
            lstRateSheetItems = [Select Id,Rate_Sheet__c,Min_Amount__c,Hourly_Rate__c, Min_Amount_Description__c,Service__c,Service__r.Name,
                                  Service__r.Additional_Percentage__c,Unit_of_Measure__c,Source_Language_Code__c,Target_Language_Code__c,
                                  Language__r.Name,Language__c,
                                  (Select Rate__c, Name, Id, Description__c,Rate_Sheet_Item__c, Trados_Categories__c
                                  From Rate_Sheet_Item__r) 
                                From Rate_Sheet_Item__c Where Rate_Sheet__c in: rsId AND Language__c in: langId];
            List<Id> lstRSSIIds = new List<Id>();        
            for(Rate_Sheet_Item__c rsi : lstRateSheetItems){
                for(Rate_Sheet_Subitem__c rssi : rsi.Rate_Sheet_Item__r){
                    
                    if((rsi.Service__r !=null) && (rsi.Language__r !=null) && (rsi.Service__r.Name.equalsIgnoreCase('Translation') || rsi.Service__r.Name.equalsIgnoreCase('Back Translation'))){
                               mapRSIName.put(rsi.Service__r.Name + '-' +rsi.Language__r.Name, rsi);
                    }
                 
                    lstRSSIIds.add(rssi.Id);
                }
            }
            lstSubItemTC = [Select id,Trados_Category__c,Rate_Sheet_Subitem__c From Sub_Item_Trados_Category__c Where Rate_Sheet_Subitem__c in: lstRSSIIds];
            Map<String,Quote_Line_Item_Subline__c> tmpMapSubLine = new Map<String,Quote_Line_Item_Subline__c>();
            Map<String,Quote_Line_Item_Subline__c> mapSubLine = new Map<String,Quote_Line_Item_Subline__c>();
            Map<String,Quote_Line_Item_Subline__c> mapDelSubLine = new Map<String,Quote_Line_Item_Subline__c>();
            Map<Id,Quote_Line_Item__c> mapQLI = new Map<Id,Quote_Line_Item__c>();
            Quote_Line_Item_Subline__c qlisl = new Quote_Line_Item_Subline__c();
            Decimal totalWords = 0;
            Decimal totalAmount = 0;
            Decimal additionalPercent = 0;
            
            Decimal additionPercentageForBackTrans = 0;
        
	        List<Client_Service__c> lstClientServices = [Select Additional_Percentage__c, Name From Client_Service__c];                                      
	           
	        for(Client_Service__c services : lstClientServices){              	
                if(services.Name.contains('Back Translation') && services.Additional_Percentage__c != null)
                {
                   additionPercentageForBackTrans = Decimal.valueOf(services.Additional_Percentage__c);
                }
              }
         
         
            for(Quote__c quote : lstQuote){
                for(Quote_Line_Item__c qli : lstQLI){  
                    oldQLI = Trigger.oldMap.get(qli.ID);
                    if(qli.QuoteId__c == quote.Id && oldQLI.Actual_Words__c != qli.Actual_Words__c /*|| oldQLI.DTP_Hours__c != qli.DTP_Hours__c) && oldQLI.Analyzed_File_URL__c != qli.Analyzed_File_URL__c*/){
                        for(Quote_Line_Item_Subline__c qlis : qli.Quote_Line_Item_Sublines__r){
                            mapDelSubLine.put(qlis.Id,qlis);
                        }
		               
		               for(Quote_Line_Service__c qlis : qli.Quote_Line_Services__r){
		                    
		                    totalWords = 0;
		                    totalAmount = 0;
		                           
		                    isRateSheetItemExist = false;
		                    String unitOfMeasure = qlis.Service_Id__r.Unit_of_Measure__c ;
		                    
		                     if(qlis.Service_Id__r.IsTWCLogic__c){
		                        map<String,Rate_Sheet_Subitem__c> mapRSSI = new map<String,Rate_Sheet_Subitem__c>();
		                        Rate_Sheet_Item__c translationRSI = new Rate_Sheet_Item__c();
		                        if(qli.Target_Language_ID__r.Name.contains('English')){
		                            translationRSI = mapRSIName.get('Back Translation-'+qli.Source_Language_ID__r.Name);
		                        }else{
		                            translationRSI = mapRSIName.get('Translation-'+qli.Target_Language_ID__r.Name);
		                        }
		                        if(translationRSI != null){
		                            for(Rate_Sheet_Subitem__c rssi : translationRSI.Rate_Sheet_Item__r){
		                              if(rssi.Description__c.contains('Repetitions'))
		                              {
		                                mapRSSI.put('Repetitions',rssi);
		                              }else if(rssi.Description__c.contains('Total Word Count')) {
		                                mapRSSI.put('Total Word Count',rssi);
		                              }
		                            }
		                            List<Rate_Sheet_Subitem__c> lstRSSI = new List<Rate_Sheet_Subitem__c>();
		                            Decimal rate = 0;
		                            String description = '';
		                            if(mapRSSI.containsKey('Total Word Count')){
		                                system.debug('-----Inside TWC if-----');
		                                description = mapRSSI.get('Total Word Count').Description__c;
		                                lstRSSI.add(mapRSSI.get('Total Word Count'));
		                                rate = mapRSSI.get('Total Word Count').Rate__c;                                           
		                            }else if(mapRSSI.containsKey('Repetitions')){
		                                system.debug('-----Inside 100% and Repetitions if-----');
		                                description = mapRSSI.get('Repetitions').Description__c;
		                                lstRSSI.add(mapRSSI.get('Repetitions'));
		                                rate = mapRSSI.get('Repetitions').Rate__c;                                                  
		                            }
		                            
		                            totalWords = qli.X95_99_Word_Count__c + qli.X85_95_Word_Count__c + qli.X75_84_Word_Count__c + qli.X50_74_Word_Count__c + qli.No_Match_Word_Count__c + qli.Context_TM_Word_Count__c + qli.Repetitions_Word_Count__c + qli.Crossfile_Repetitions__c + qli.X100_Word_Count__c + qli.Perfect_Match__c;
		                        
		                            qlisl = new Quote_Line_Item_Subline__c();
		                            qlisl.Description__c = qlis.Service_Id__r.Name;
		                            qlisl.Unit_Price__c = rate;
		                            qlisl.Unit_of_Measure__c = 'per Word';
		                            qlisl.Quote_Line_Item_Id__c = qli.Id;
		                            //TBC
		                            qlisl.Client_Service__c = qlis.Service_Id__c;
		                            if(mapRateSummary.get(description) != null)
		                                 qlisl.Default_Subline_Order__c=mapRateSummary.get(description);
		                            
		                            if(qlis.Service_Id__r.Name.equalsIgnoreCase('Translation')){
		                              qlisl.Default_Order__c=1;
		                            }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Back Translation')){
		                              qlisl.Default_Order__c=2; 
		                            }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Desktop Publishing')){
		                              qlisl.Default_Order__c=3; 
		                            }else{
		                              qlisl.Default_Order__c=4; 
		                            }
		                            qlisl.Manual_Entry__c = false;
		                            if(qli.Target_Language_ID__r.Name.contains('English')){ 
                             
	                                    qlisl.Quantity__c = Math.ceil(totalWords+( qli.Back_Translation_Total_Words__c*additionPercentageForBackTrans/100));
	                                }else{
	                                    qlisl.Quantity__c = totalWords;
	                                }
		                           // qlisl.Quantity__c = totalWords;
		                            totalAmount += totalWords * rate;
		                            tmpMapSubLine.put(qli.Id+'-'+qlisl.Client_Service__c+'-'+qlisl.Description__c,qlisl);
		                            
		                            if(translationRSI.Min_Amount__c != null && totalAmount <= translationRSI.Min_Amount__c){
		                                    qlisl = new Quote_Line_Item_Subline__c();
		                                    qlisl.Quote_Line_Item_Id__c = qli.Id;
		                                    if(translationRSI.Min_Amount_Description__c.contains('Hour') || translationRSI.Min_Amount_Description__c.contains('Desktop Publishing') || translationRSI.Min_Amount_Description__c.contains('Minimum')){
		                                        addUnitMeasure='per Hour';
		                                    }else if(translationRSI.Min_Amount_Description__c.contains('Certification')){
		                                        addUnitMeasure='';
		                                    }else{
		                                        addUnitMeasure='per Word';
		                                    }
		                                    qlisl.Unit_of_Measure__c = addUnitMeasure;
		                                    qlisl.Description__c = qlis.Service_Id__r.Name+', Minimum Charge';
		                                    qlisl.Unit_Price__c = translationRSI.Min_Amount__c;
		                                    qlisl.Quantity__c = 1;
		                                      qlisl.Manual_Entry__c = false;
		                                    if(mapRateSummary.get(description) != null)
		                                         qlisl.Default_Subline_Order__c = mapRateSummary.get(description);
		                                         
		                                    if(qlis.Service_Id__r.Name.equalsIgnoreCase('Translation')){
		                                      qlisl.Default_Order__c=1;
		                                    }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Back Translation')){
		                                      qlisl.Default_Order__c=2; 
		                                    }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Desktop Publishing')){
		                                      qlisl.Default_Order__c=3; 
		                                    }else{
		                                      qlisl.Default_Order__c=4; 
		                                    }
		                                    qlisl.Show_Units__c = false;
		                                    qlisl.Show_Unit_of_Measure__c = false;
		                                    qlisl.Show_Description__c = true;
		                                    qlisl.Show_Subline_Total__c = true; 
		                                    qlisl.Show_Unit_Price__c = false;
		                                    qlisl.Sort_Order__c = 1;
		                                    qli.Min_Charge_Applied__c = true;
		                                    qlisl.Client_Service__c = qlis.Service_Id__c;
		                                    mapSubLine.put(qli.Id+'-'+qlisl.Client_Service__c+'-'+qlisl.Description__c,qlisl);
		                                    mapQLI.put(qli.Id,qli);
		                                }
		                                else{
		                                    mapSubLine.putAll(tmpMapSubLine);
		                                } 
		                            totalWords = 0;
		                            isRateSheetItemExist = true;
		                        } else
		                        {   Decimal rate = 0;
		                            qlisl = new Quote_Line_Item_Subline__c();
		                            qlisl.Description__c =  qlis.Service_Id__r.Name;
		                            qlisl.Unit_Price__c = rate;
		                            qlisl.Unit_of_Measure__c = 'per Word';
		                            qlisl.Quote_Line_Item_Id__c = qli.Id;
		                            //TBC
		                            qlisl.Client_Service__c = qlis.Service_Id__c;
		                            string descriptionCat = qlis.Service_Id__r.Name;
		                            if(mapRateSummary.get(descriptionCat) != null)
		                                 qlisl.Default_Subline_Order__c=mapRateSummary.get(descriptionCat);
		                            
		                            if(qlis.Service_Id__r.Name.equalsIgnoreCase('Translation')){
		                              qlisl.Default_Order__c=1;
		                            }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Back Translation')){
		                              qlisl.Default_Order__c=2; 
		                            }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Desktop Publishing')){
		                              qlisl.Default_Order__c=3; 
		                            }else{
		                              qlisl.Default_Order__c=4; 
		                            }
		                            qlisl.Manual_Entry__c = false;
		                            
		                            mapSubLine.put(qli.Id+'-'+qlisl.Client_Service__c+'-'+qlisl.Description__c,qlisl);
		                        
		                        }
		                    }else if(qlis.Service_Id__r.IsOutsourced__c){                                   
		                        Decimal rate = 0;
		                        
		                        if(mapRateSummaryDefault.get(qlis.Service_Id__r.Name) != null){
                                    rate = mapRateSummaryDefault.get(qlis.Service_Id__r.Name); 
                                }
                                
		                        qlisl = new Quote_Line_Item_Subline__c();
		                        qlisl.Description__c = qlis.Service_Id__r.Name;
		                        qlisl.Unit_Price__c = rate;
		                        qlisl.Outsourced_Service_Percentage__c = qlis.Service_Id__r.Additional_Percentage__c;
		                        qlisl.IsOutsourced__c = true;
		                        qlisl.Unit_of_Measure__c = qlis.Service_Id__r.Unit_of_Measure__c ;
		                        qlisl.Quote_Line_Item_Id__c = qli.Id;
		                        qlisl.Client_Service__c = qlis.Service_Id__c;
		                        if(mapRateSummary.get(qlis.Service_Id__r.Name) != null)
		                             qlisl.Default_Subline_Order__c=mapRateSummary.get(qlis.Service_Id__r.Name);
		                       
		                        if(qlis.Service_Id__r.Name.equalsIgnoreCase('Translation')){
		                          qlisl.Default_Order__c=1;
		                        }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Back Translation')){
		                          qlisl.Default_Order__c=2; 
		                        }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Desktop Publishing')){
		                          qlisl.Default_Order__c=3; 
		                        }else{
		                          qlisl.Default_Order__c=4; 
		                        }
		                        qlisl.Quantity__c = totalWords;
		                        qlisl.Manual_Entry__c = false;
		                        mapSubLine.put(qli.Id+'-'+qlisl.Client_Service__c+'-'+qlisl.Description__c,qlisl);
		                        //mapSubLine.putAll(tmpMapSubLine);
		                        totalAmount = 0;
		                        totalWords = 0;
		                        isRateSheetItemExist = true;
		                    } else if(unitOfMeasure!=null && unitOfMeasure.contains('Hour')){
		                        Rate_Sheet_Item__c translationRSI = new Rate_Sheet_Item__c();
		                        if(qli.Target_Language_ID__r.Name.contains('English')){
		                            translationRSI = mapRSIName.get('Back Translation-'+qli.Source_Language_ID__r.Name);
		                        }else{
		                            translationRSI = mapRSIName.get('Translation-'+qli.Target_Language_ID__r.Name);
		                        }
		                        if(translationRSI != null){
		                            qlisl = new Quote_Line_Item_Subline__c();
		                            qlisl.Description__c = 'Hour(s), '+qlis.Service_Id__r.Name;
		                           
		                            qlisl.Unit_Price__c = translationRSI.Hourly_Rate__c;
		                            qlisl.Unit_of_Measure__c = 'per Hour';
		                            qlisl.Quote_Line_Item_Id__c = qli.Id;
		                            qlisl.Client_Service__c = qlis.Service_Id__c;
		                            /*Set <String> serviceNameSet = new Set<String>();
		                            serviceNameSet = mapRateSummary.keySet();
		                            String rateSummaryKey = '';
		                            for(String name : serviceNameSet){
		                                if(name.contains(qlis.Service_Id__r.Name)){
		                                    rateSummaryKey = name;
		                                }
		                            }*/
		                            String description = qlis.Service_Id__r.Name;
		                            if(mapRateSummary.get(description) != null)
		                                 qlisl.Default_Subline_Order__c=mapRateSummary.get(description);
		                            qlisl.Default_Order__c=4; 
		                            qlisl.Manual_Entry__c = false;
		                            totalWords = 1;
		                            if(qlis.Service_Id__r.Name.contains('Desktop Publishing')){
		                                totalWords = qli.DTP_Hours__c;
		                                qlisl.Quantity__c = totalWords;      
		                            }else{
		                                qlisl.Quantity__c = 1;
		                            }
		                            totalAmount += totalWords * translationRSI.Hourly_Rate__c;
		                            mapSubLine.put(qli.Id+'-'+qlisl.Client_Service__c+'-'+qlisl.Description__c,qlisl);
		                            totalWords = 0;
		                            
		                            isRateSheetItemExist = true;
		                        }else
		                        {
		                           qlisl = new Quote_Line_Item_Subline__c();
		                            qlisl.Description__c = 'Hour(s), '+qlis.Service_Id__r.Name;
		                           
		                            qlisl.Unit_Price__c = 0;
		                            qlisl.Unit_of_Measure__c = 'per Hour';
		                            qlisl.Quote_Line_Item_Id__c = qli.Id;
		                            qlisl.Client_Service__c = qlis.Service_Id__c;
		                            /*Set <String> serviceNameSet = new Set<String>();
		                            serviceNameSet = mapRateSummary.keySet();
		                            String rateSummaryKey = '';
		                            for(String name : serviceNameSet){
		                                if(name.contains(qlis.Service_Id__r.Name)){
		                                    rateSummaryKey = name;
		                                }
		                            }*/
		                            if(mapRateSummary.get('Hour(s), ' + qlis.Service_Id__r.Name) != null)
		                                 qlisl.Default_Subline_Order__c=mapRateSummary.get('Hour(s), ' + qlis.Service_Id__r.Name);
		                            qlisl.Default_Order__c=4; 
		                            qlisl.Manual_Entry__c = false;
		                            totalWords = 1;
		                            if(qlis.Service_Id__r.Name.contains('Desktop Publishing')){
		                                totalWords = qli.DTP_Hours__c;
		                                qlisl.Quantity__c = totalWords;      
		                            }else{
		                                qlisl.Quantity__c = 1;
		                            }
		                            //totalAmount += totalWords * translationRSI.Hourly_Rate__c;
		                            mapSubLine.put(qli.Id+'-'+qlisl.Client_Service__c+'-'+qlisl.Description__c,qlisl);
		                            totalWords = 0;
		                        }
		                    }
		                    else if(unitOfMeasure!=null && unitOfMeasure.contains('Word')){
		                        
		                        Rate_Sheet_Item__c danRSI = new Rate_Sheet_Item__c();
		                        string tLangaue  = qli.Target_Language_ID__r.Name;                        
		                        if(qli.Target_Language_ID__r.Name.contains('English'))	
		                           tLangaue = qli.Source_Language_ID__r.Name;
                                                   
		                        for(Rate_Sheet_Item__c rsi : lstRateSheetItems){
		                                                                                    
		                            if(qlis.Service_Id__c == rsi.Service__c && rsi.Language__r.Name == tLangaue){
		                                {
		                                  if(qlis.Service_Id__r.Name.equalsIgnoreCase('Translation') && qli.Target_Language_ID__r.Name.contains('English'))
		                                  {
		                                    danRSI = mapRSIName.get('Back Translation-'+qli.Source_Language_ID__r.Name);
		                                     
		                                  }
		                                  else  if(qlis.Service_Id__r.Name.equalsIgnoreCase('Back Translation') && qli.Target_Language_ID__r.Name.contains('English'))
		                                  {
		                                    danRSI = mapRSIName.get('Translation-'+qli.Source_Language_ID__r.Name);
		                                     
		                                  }else{
		                                    danRSI = rsi;
		                                  } 
		                               
		                               if(danRSI != null)
                                       {   
		                                system.debug('---danRSI----: '+danRSI );   
		                                                                
		                                for(Rate_Sheet_Subitem__c rssi : danRSI.Rate_Sheet_Item__r){  
		                                	
		                                  if(!rssi.Description__c.trim().contains('Total Word Count'))
                                 	       { 	                                                       
		                                    
		                                        for(Sub_Item_Trados_Category__c sitc : lstSubItemTC){
		                                            if(sitc.Rate_Sheet_Subitem__c == rssi.Id){
		                                                if(sitc.Trados_Category__c.equalsIgnoreCase('95% - 99%'))
		                                                {
		                                                    totalWords = totalWords + qli.X95_99_Word_Count__c;
		                                                }        
		                                                if(sitc.Trados_Category__c.equalsIgnoreCase('85% - 94%'))
		                                                {
		                                                   totalWords = totalWords + qli.X85_95_Word_Count__c;
		                                                }
		                                                if(sitc.Trados_Category__c.equalsIgnoreCase('75% - 84%'))
		                                                {
		                                                   totalWords = totalWords + qli.X75_84_Word_Count__c;
		                                                }
		                                                if(sitc.Trados_Category__c.equalsIgnoreCase('50% - 74%'))
		                                                {
		                                                   totalWords = totalWords + qli.X50_74_Word_Count__c;
		                                                }
		                                                if(sitc.Trados_Category__c.equalsIgnoreCase('New'))
		                                                {
		                                                   totalWords = totalWords + qli.No_Match_Word_Count__c;
		                                                }
		                                                if(sitc.Trados_Category__c.equalsIgnoreCase('Context Match'))
		                                                {
		                                                   totalWords = totalWords + qli.Context_TM_Word_Count__c;
		                                                }
		                                                if(sitc.Trados_Category__c.equalsIgnoreCase('Repetitions'))
		                                                {
		                                                   totalWords = totalWords + qli.Repetitions_Word_Count__c;
		                                                }
		                                                if(sitc.Trados_Category__c.equalsIgnoreCase('Cross-file Repetitions'))
		                                                {
		                                                   totalWords = totalWords + qli.Crossfile_Repetitions__c;
		                                                }
		                                                if(sitc.Trados_Category__c.equalsIgnoreCase('100%'))
		                                                {
		                                                   totalWords = totalWords + qli.X100_Word_Count__c;
		                                                }  
		                                                if(sitc.Trados_Category__c.equalsIgnoreCase('Perfect_Match__c'))
		                                                {
		                                                   totalWords = totalWords + qli.Perfect_Match__c;
		                                                }                                                                                                                                                  
		                                            }
		                                        }
		                                        qlisl = new Quote_Line_Item_Subline__c();
		                                        
		                                        if(qlis.Service_Id__r != null && qli.Target_Language_ID__r !=null)
		                                        {
			                                        if(qlis.Service_Id__r.Name.equalsIgnoreCase('Translation') && qli.Target_Language_ID__r.Name.contains('English') && rssi.Description__c.contains('Back Translation'))
			                                        {
			                                          if(rssi.Description__c.split(',').size()> 1)	
			                                          {
			                                        	  qlisl.Description__c = rssi.Description__c.split(',')[1];
			                                          }
			                                        }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Back Translation') && qli.Target_Language_ID__r.Name.contains('English'))
			                                        {
			                                             qlisl.Description__c = 'Back Translation, ' + rssi.Description__c;
			                                        }else
			                                        {
			                                             qlisl.Description__c = rssi.Description__c;
			                                        }
		                                        }else
		                                        {
		                                                qlisl.Description__c = rssi.Description__c;
		                                        }
		                                        
		                                        qlisl.Unit_Price__c = rssi.Rate__c;
		                                        if(rssi.Description__c.contains('Hour') || rssi.Description__c.contains('Desktop Publishing') || rssi.Description__c.contains('Minimum')){
		                                            addUnitMeasure='per Hour';
		                                        }else if (rssi.Description__c.contains('Certification')){
		                                            addUnitMeasure='';
		                                        }else{
		                                            addUnitMeasure='per Word';
		                                        }
		                                        qlisl.Unit_of_Measure__c = addUnitMeasure;
		                                        qlisl.Quote_Line_Item_Id__c = qli.Id;
		                                        qlisl.Client_Service__c = rsi.Service__c;
		                                        String description =  qlisl.Description__c;
		                                        
		                                        if(mapRateSummary.get(description) != null)
		                                             qlisl.Default_Subline_Order__c=mapRateSummary.get(description);
		                                        if(rsi.Service__r.Name.equalsIgnoreCase('Translation')){
		                                          qlisl.Default_Order__c=1;
		                                        }else if(rsi.Service__r.Name.equalsIgnoreCase('Back Translation')){
		                                          qlisl.Default_Order__c=2; 
		                                        }else if(rsi.Service__r.Name.equalsIgnoreCase('Desktop Publishing')){
		                                          qlisl.Default_Order__c=3; 
		                                        }else{
		                                          qlisl.Default_Order__c=4; 
		                                        }
		                                        qlisl.Manual_Entry__c = false;
		                                        if(rssi.Description__c.contains('Desktop Publishing')){
		                                            totalWords = qli.DTP_Hours__c;
		                                            qlisl.Quantity__c = totalWords;      
		                                        }
		                                        
		                                        if(((qlis.Service_Id__r.Name.contains('Back Translation') && !qli.Target_Language_ID__r.Name.contains('English'))|| (qlis.Service_Id__r.Name.equalsIgnoreCase('Translation') && qli.Target_Language_ID__r.Name.contains('English'))) && rssi.Trados_Categories__c.contains('New')){
		                                            
		                                           if(qli.Target_Language_ID__r.Name.contains('English'))
			                                           {
			                                            qlisl.Quantity__c = Math.ceil(totalWords+( qli.Back_Translation_Total_Words__c*additionPercentageForBackTrans/100));
			                                           }else
			                                           {
			                                            qlisl.Quantity__c = Math.ceil(totalWords+( qli.Actual_Words__c*additionPercentageForBackTrans/100));
			                                           }		                                            
		                                        }else{
		                                            qlisl.Quantity__c = totalWords;
		                                        }
		                                        
		                                        totalAmount += totalWords * rssi.Rate__c;
		                                        tmpMapSubLine.put(qli.Id+'-'+qlisl.Client_Service__c+'-'+qlisl.Description__c,qlisl);
		                                        totalWords = 0;   
		                                        
		                                        isRateSheetItemExist =  true;                           
		                                }
                                       }
		                                if(rsi.Min_Amount__c != null && totalAmount <= rsi.Min_Amount__c){
		                                    qlisl = new Quote_Line_Item_Subline__c();
		                                    qlisl.Quote_Line_Item_Id__c = qli.Id;
		                                    if(rsi.Min_Amount_Description__c.contains('Hour') || rsi.Min_Amount_Description__c.contains('Desktop Publishing') || rsi.Min_Amount_Description__c.contains('Minimum')){
		                                        addUnitMeasure='per Hour';
		                                    }else if(rsi.Min_Amount_Description__c.contains('Certification')){
		                                        addUnitMeasure='';
		                                    }else{
		                                        addUnitMeasure='per Word';
		                                    }
		                                    qlisl.Unit_of_Measure__c = addUnitMeasure;
		                                    qlisl.Description__c = rsi.Min_Amount_Description__c;
		                                    qlisl.Unit_Price__c = rsi.Min_Amount__c;
		                                    qlisl.Quantity__c = 1;
		                                    qlisl.Manual_Entry__c = false;
		                                    if(mapRateSummary.get('Minimum Charge') != null)
		                                         qlisl.Default_Subline_Order__c = mapRateSummary.get('Minimum Charge');
		                                    if(rsi.Service__r.Name.equalsIgnoreCase('Translation')){
		                                      qlisl.Default_Order__c=1;
		                                    }else if(rsi.Service__r.Name.equalsIgnoreCase('Back Translation')){
		                                      qlisl.Default_Order__c=2; 
		                                    }else if(rsi.Service__r.Name.equalsIgnoreCase('Desktop Publishing')){
		                                      qlisl.Default_Order__c=3; 
		                                    }else{
		                                      qlisl.Default_Order__c=4; 
		                                    }
		                                    qlisl.Show_Units__c = false;
		                                    qlisl.Show_Unit_of_Measure__c = false;
		                                    qlisl.Show_Description__c = true;
		                                    qlisl.Show_Subline_Total__c = true; 
		                                    qlisl.Show_Unit_Price__c = false;
		                                    qlisl.Sort_Order__c = 1;
		                                    qli.Min_Charge_Applied__c = true;
		                                    qlisl.Client_Service__c = rsi.Service__c;
		                                    mapSubLine.put(qli.Id+'-'+qlisl.Client_Service__c+'-'+qlisl.Description__c,qlisl);
		                                    mapQLI.put(qli.Id,qli);
		                                }
		                                else{
		                                    mapSubLine.putAll(tmpMapSubLine);
		                                }  
		                                }
		                            }
		                           
		                            totalAmount = 0;
		                        }
		                        } 
		                    } else {
		                            Decimal rate = 0;
		                            qlisl = new Quote_Line_Item_Subline__c();
		                            qlisl.Description__c = qlis.Service_Id__r.Name;
		                            qlisl.Unit_Price__c = rate;
		                            qlisl.Unit_of_Measure__c = qlis.Service_Id__r.Unit_of_Measure__c;
		                            qlisl.Quote_Line_Item_Id__c = qli.Id;
		                             
		                            //TBC
		                            qlisl.Client_Service__c = qlis.Service_Id__c;
		                            if(mapRateSummary.get(qlis.Service_Id__r.Name) != null)
		                                 qlisl.Default_Subline_Order__c=mapRateSummary.get(qlis.Service_Id__r.Name);
		                            if(qlis.Service_Id__r.Name.equalsIgnoreCase('Translation')){
		                              qlisl.Default_Order__c=1;
		                            }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Back Translation')){
		                              qlisl.Default_Order__c=2; 
		                            }else if(qlis.Service_Id__r.Name.equalsIgnoreCase('Desktop Publishing')){
		                              qlisl.Default_Order__c=3; 
		                            }else{
		                              qlisl.Default_Order__c=4; 
		                            }
		                            
		                            totalWords = 0;
		                            qlisl.Manual_Entry__c = false;
		                            //totalAmount += totalWords * rssi.Rate__c;
		                            totalAmount = 0;
		                            mapSubLine.put(qli.Id+'-'+qlisl.Client_Service__c+'-'+qlisl.Description__c,qlisl);
		                            //mapSubLine.putAll(tmpMapSubLine);
		                            totalWords = 0;                        
		                    }
		                    
		                     tmpMapSubLine.clear();
		                
		                    }
                   }
                }
            }
            if(mapDelSubLine.size()>0)
                try{
                    delete mapDelSubLine.values();
                }Catch(DMLException de){
                    System.debug(GLSConfig.DMLExceptionOccured);
                }
            if(mapSubLine.size()>0){
                try{
                    upsert mapSubLine.values();
                }catch(DMLException de){
                    system.debug(GLSConfig.DMLExceptionOccured);
                }catch(Exception e){
                    system.debug(GLSConfig.ExceptionOccured);
                }
            }
            if(mapQLI.size()>0){
                try{
                    upsert mapQLI.values();     
                }catch(DMLException de){
                    system.debug(GLSConfig.DMLExceptionOccured);
                }catch(Exception e){
                    system.debug(GLSConfig.ExceptionOccured);
                }
            }      
        }catch(Exception e){
            system.debug(GLSConfig.ExceptionOccured+e.getMessage());
        }
    }
    
    if(trigger.isAfter && trigger.isInsert){
        List<Id> lstQLIId = new List<Id>();
        List<Quote_Line_Item__c> lstQLIs = new List<Quote_Line_Item__c>();
        List<Quote_Line_Item__c> lstQLIsUpdate = new List<Quote_Line_Item__c>();
        try{
            for(Quote_Line_Item__c qli : trigger.new){
                lstQLIId.add(qli.Id);
            }
            lstQLIs = [Select QuoteID__r.Rate_Sheet__r.Name,Subline_Total_Amount__c,QuoteID__c,QuoteID__r.Client__r.Name, Id,Rate_Type__c From Quote_Line_Item__c Where Id in: lstQLIId];
            for(Quote_Line_Item__c qli : lstQLIs){
                if(qli.QuoteID__r.Rate_Sheet__r.Name == 'Standard'){
                    qli.Rate_Type__c = 'Standard Rates';
                    lstQLIsUpdate.add(qli);
                }else if(qli.QuoteID__r.Client__r.Name == qli.QuoteID__r.Rate_Sheet__r.Name){
                    qli.Rate_Type__c = 'Client Rates';
                    lstQLIsUpdate.add(qli);
                }
            }
            if(lstQLIsUpdate != null && lstQLIsUpdate.size() > 0){
                database.update(lstQLIsUpdate);
            }
        }catch(Exception e){
            system.debug('------e.getMessage()--------'+e.getMessage());
        }
    }
    
    if(trigger.isUpdate && trigger.isBefore){
        for(Quote_Line_Item__c qli : trigger.new){
            oldQLI = Trigger.oldMap.get(qli.ID);
            if(qli.Client_TM__c != oldQLI.Client_TM__c){
                qli.isAnalyzed__c = false;
                qli.isProcessed__c = false;
                qli.isReadyForAnalyse__c = true;
            }
        }
    }
}