trigger GLS_CommunityLeadCrudCommunityLeadSignup on Lead (after insert) {
 set<String> strleds = new Set<String>();
 for(Lead le:Trigger.new){
 	if(le.isWebToLead__c)
  		strleds.add(le.id);
  }
     if(strleds.size()>0) 
      	GLS_CommunityNewLeadByWebClass.newLeadByWebMethod(strleds);
}