trigger Gls_Uncheck_Initiate_Approval_Process on Case (after update,after insert) {
   set <id> caseid = new set<id>();
   set <id> caseid1 = new set<id>();
   set <id> userid = new set<id>();
   set <id> conid= new set<id>();
   set<id> contactid= new set<id>();
   list<Case> lstCase= new list<Case>();
   list<user>lstUser= new list<User>();
   list<Contact>lstContact = new list<Contact>();
   list<Contact>lstContact2 = new list<Contact>();
   list<Contact>lstContact1 = new list<Contact>();
   if(trigger.isupdate){
   	
    for(Case cs : trigger.new){
    	
        if(cs.Status=='Closed' && trigger.oldmap.get(cs.id).Status!='Closed'  && cs.Should_Contact_Be_Updated__c =='Accepted' ) {
        caseid.add(cs.id);
        }
        if(cs.Status=='Closed' && trigger.oldmap.get(cs.id).Status!='Closed'  && cs.Should_Contact_Be_Updated__c =='Rejected' ){
        
             caseid1.add(cs.id);
        
        }
    }
    if(caseid != null && caseid.size()>0){
        lstCase= [select id,ownerid,status,Temp_Phone__c,Temp_fax__c,Temp_lname__c,Temp_fname__c,Temp_title__c,Temp_dept__c,Temp_email__c,contact__r.id from case where id in : caseid];
    }
    
    if(lstCase.size()>0){
        for(case c: lstCase){
           userid.add(c.contact__r.id);
         }
    }

    if(userid.size()>0 && userid !=null){
        lstContact =[select id ,Initiate_Approval_Process__c,Temp_Fax__c ,Fax,Temp_Phone__c, phone,LastName,FirstName,Temp_MailingStreet__c,
                        Temp_MailingCity__c,Temp_MailingState__c,Temp_MailingCountry__c,Temp_MailingPostalCode__c ,
                         MailingStreet,MailingCity,MailingState,MailingCountry,MailingPostalCode,Cc_Email__c,Temp_Department__c,Temp_Title__c 
                          from Contact where id in:userid];
    }
   
    if(lstContact  !=null && lstContact.size()>0){
    for(Contact c :lstContact ){
           c.Initiate_Approval_Process__c=false;
          if( lstCase[0].Temp_Fax__c !=null ||lstCase[0].Temp_Fax__c !=''){
              c.Fax=  lstCase[0].Temp_Fax__c;
          }
          if(lstCase[0].Temp_Phone__c !=null || lstCase[0].Temp_Phone__c!=''){
              c.phone=lstCase[0].Temp_Phone__c;
          
          }
          if(lstCase[0].Temp_lname__c != null && lstCase[0].Temp_lname__c !=''){
            c.LastName=lstCase[0].Temp_lname__c;
          }
          if(lstCase[0].Temp_fname__c != null && lstCase[0].Temp_fname__c !=''){
            c.FirstName=lstCase[0].Temp_fname__c;
          }
          if(lstCase[0].Temp_dept__c !=null && lstCase[0].Temp_dept__c !='' ){
            c.Department=lstCase[0].Temp_dept__c ;
          }
          if(lstCase[0].Temp_title__c != null && lstCase[0].Temp_title__c !=''){
            c.Title=lstCase[0].Temp_title__c;
           }
           if(lstCase[0].Temp_email__c != null && lstCase[0].Temp_email__c !=''){
            c.Cc_Email__c=lstCase[0].Temp_email__c;
          }
                
          if(c.Temp_MailingStreet__c !=null && c.Temp_MailingStreet__c !=''){
                    
            c.MailingStreet=c.Temp_MailingStreet__c;
           }
          if(c.Temp_MailingCity__c != null && c.Temp_MailingCity__c !=''){
            c.MailingCity=c.Temp_MailingCity__c;
            }
           if(c.Temp_MailingState__c != null && c.Temp_MailingState__c !=''){
             c.MailingState=c.Temp_MailingState__c;
            }
           if(c.Temp_MailingCountry__c !=null && c.Temp_MailingCountry__c !=''){
              c.MailingCountry=c.Temp_MailingCountry__c;
            }
           if(c.Temp_MailingPostalCode__c !=null && c.Temp_MailingPostalCode__c !=''){
             c.MailingPostalCode=c.Temp_MailingPostalCode__c;
            }
            lstContact1.add(c);
     }
    }
    if(lstContact1.size()>0 && lstContact1 !=null){
    try{
      update lstContact1;
    }
    catch( exception e){
        system.debug('exception');
    }
    
   }
   lstCase= new list<Case>();
    if(caseid1 != null && caseid1.size()>0){
        lstCase= [select id,ownerid,status,Contact__r.id from case where id in : caseid1];
    }
    if(lstCase != null && lstCase.size()>0){
        for( Case cs : lstCase){
            contactid.add(cs.Contact__r.id );
            
        }
    }
  
    
    if(contactid != null && contactid.size()>0){
         lstContact2 =[select id ,Department,Title,Initiate_Approval_Process__c,Temp_Fax__c ,Fax,Temp_Phone__c, phone,LastName,FirstName,MailingPostalCode,Temp_MailingPostalCode__c,
                         MailingStreet,Temp_MailingStreet__c,MailingCity, Temp_MailingCity__c, MailingState, Temp_MailingState__c,
                         MailingCountry,Temp_MailingCountry__c,Cc_Email__c,Temp_cc_email__c ,Temp_Title__c,Temp_Department__c 
                         from Contact where id in:contactid];
        
    }
    if(lstContact2 != null && lstContact2.size()>0){
        lstContact1= new list <Contact>();
        for(Contact con : lstContact2){
            con.Initiate_Approval_Process__c=false;
            if(con.fax != null && con.fax != null){
             con.Temp_Fax__c=con.fax;   
            }
            if(con.phone !=null){
                con.Temp_Phone__c =con.phone;
            }
            if(con.FirstName != null && con.FirstName!=''){
                    con.Temp_Name__c=con.FirstName;
            }
            else{
                    con.Temp_Name__c='';
            }
            if(con.LastName != null && con.LastName!=''){
                    con.Temp_LastName__c=con.LastName;
            }
            if(con.MailingStreet != null && con.MailingStreet !=''){
                con.Temp_MailingStreet__c=con.MailingStreet;
            }
            else{
                    con.Temp_MailingStreet__c='';
            }
            if(con.MailingCity != null && con.MailingCity !=''){
                con.Temp_MailingCity__c=con.MailingCity;
            }
            else{
                 con.Temp_MailingCity__c='';
            }
            if(con.MailingState != null && con.MailingState !=''){
                con.Temp_MailingState__c= con.MailingState;
            }
            else{
             con.Temp_MailingState__c='';
            }
            if(con.MailingCountry != null && con.MailingCountry !=''){
                con.Temp_MailingCountry__c=con.MailingCountry;
            }
            else{
             con.Temp_MailingCountry__c='';
            
            }
            if(con.MailingPostalCode !=null && con.MailingPostalCode !=''){
                con.Temp_MailingPostalCode__c =con.MailingPostalCode;
            }
            if(con.Cc_Email__c!=null && con.Cc_Email__c!=''){
                con.Temp_cc_email__c =con.Cc_Email__c;
            }
            else{
            con.Temp_cc_email__c='';
            }
            if(con.Title!=null && con.Title!=''){
                con.Temp_Title__c =con.Title;
            }
            else{
            con.Temp_Title__c='';
            }
            if(con.Department!=null && con.Department!=''){
                con.Temp_Department__c =con.Title;
            }
            else{
            con.Temp_Department__c='';
            }
            lstContact1.add(con);
        }
    }
    if(lstContact1.size()>0 && lstContact1 !=null){
        try{
        update lstContact1;
        }
        catch (Exception e){
            System.debug('Exception');
            
        }
    }
}
if(trigger.isInsert){
	    	
	
          for(Case cs : trigger.new){
              caseid.add(cs.id);
            }
         if(caseid != null && caseid.size()>0){
            lstCase= [select id,ownerid,status,Temp_Phone__c,Temp_fax__c,Temp_lname__c,Temp_fname__c,Contact__r.id,Temp_title__c,Temp_dept__c from case where id in : caseid];
         }
        if(lstCase.size()>0){
            for(case c: lstCase){
                userid.add(c.Contact__r.id);
             }
        }
        if(userid !=null && userid.size()>0){
            
             lstContact2 =[select id ,Initiate_Approval_Process__c,Temp_Fax__c ,Fax,Temp_Phone__c, phone,LastName,FirstName from Contact where id in:userid];
             
        }
        if(lstContact2 != null && lstContact2.size()>0){
            lstContact1= new list <Contact>();
            for(Contact con : lstContact2){
              /*  if(lstCase[0].Temp_dept__c !=null && lstCase[0].Temp_dept__c !='' ){
                    con.Department=lstCase[0].Temp_dept__c ;
                }
                if(lstCase[0].Temp_title__c != null && lstCase[0].Temp_title__c !=''){
                        con.Title=lstCase[0].Temp_title__c;
                }*/
                if(lstCase[0].Temp_Fax__c != null || lstCase[0].Temp_lname__c != null || lstCase[0].Temp_fname__c != null || lstCase[0].Temp_Phone__c !=null || lstCase[0].Temp_lname__c !='' || lstCase[0].Temp_fname__c !='' || lstCase[0].Temp_dept__c !=''|| lstCase[0].Temp_title__c !=''){
                
                    con.Initiate_Approval_Process__c=true;
                }
                  lstContact1.add(con);
            }
        }
        if(lstContact1 != null && lstContact1.size()>0){
            try{
            update lstContact1;
            
            }
            catch( Exception e){
                system.debug('--- Exception---');
            }
            
     }
    }
}