/** Copyright (c) 2015, Global Languages Solutions, Inc. All rights reserved.
* Trigger   :  AWSFileCRUD trigger
* Function: Maintain versioning. 
* Triggering Points: Will be executed when AWS records are inserted from GLSGenerateQuotePDFController and GLSQuotePDFMigrationController
*/
trigger AWSFileCRUD on AWS_File__c (after insert, after update, before delete) {
    
    Set<Id> setAWSId = new Set<Id>();
    
    if(Trigger.isAfter && Trigger.isInsert && !GLSQW_CloneFileController.skipTrigger 
        && (GLSGenerateQuotePDFController.executeTrigger || GLSQuotePDFMigrationController.executeAWSTrigger)){
        for(AWS_File__c awsFiles : Trigger.new){
            setAWSId.add(awsFiles.Id);
        }

        AWSTriggerHandler.handleAfterInsertUpdate(setAWSId); //Handler called to insert records of AWS File Sharing and AWS File Version
    }
}