<apex:page standardController="Program__c" extensions="GLSLPW_ProgramControllerExt" sidebar="false" id="thePage" tabstyle="Program__c">
 
    <apex:stylesheet value="{!$Resource.jqueryui}" />
    <apex:includeScript value="{!URLFOR($Resource.TCS_JQuery_DataTables, 'js/jquery.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.CommetD, 'js/cometd.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.CommetD, 'js/json2.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.CommetD, 'js/jquery.cometd.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.TCS_JQuery_DataTables, 'js/jquery.dataTables.js')}"/> 
    <apex:stylesheet value="{!URLFOR($Resource.TCS_JQuery_DataTables, 'css/dataTables.css')}"/>
    
    <style>
        .estimateLPBtnCls{
            margin-left: 85% !important;
        }
        .header {
            font-family:"Calibri";
            text-align:center !important;
            width: 50px;
            
        }
        .cursor{
            cursor: pointer;
        }
        .contentCenter{
            text-align:center !important;
        }
        .refreshFilesBtn{
            margin-top: -3% !important;
        }
        .removeHyperlink{
            text-decoration:none;
        }
    </style>
    <apex:form id="theForm">
        <apex:pageMessages id="msgPanel"/>
        <apex:actionFunction name="downloadFileAction" action="{!fetchProgramFile}" rerender="dummy">
            <apex:param name="refFileId" assignTo="{!pfileId}" value=""/>
        </apex:actionFunction>
        <apex:actionFunction name="deleteFileAction" action="{!deleteProgramFile}" rerender="refFilesPanel,msgPanel">
            <apex:param name="refFileId" assignTo="{!pfileId}" value=""/>
        </apex:actionFunction>
        
        <apex:pageBlock id="thePgBlockPrgDetails" title="Program Details">
            <apex:pageBlockButtons location="top">
                <apex:commandButton value="Edit" action="{!customEdit}" rendered="{!(!editMode)}"/>
                <apex:commandlink action="{!createNewLPEstimate}" target="_self" styleclass="removeHyperlink" rendered="{!(!editMode)}">
                    <apex:commandButton id="estimateLPBtn" value="New LP Estimate" />
                </apex:commandLink>
                <apex:commandButton value="Save" action="{!customSave}" rendered="{!editMode}"/>
                <apex:commandButton value="Cancel" action="{!customCancel}" rendered="{!editMode}"/>
            </apex:pageBlockButtons>
            <apex:pageBlockSection columns="2" id="prgDetails"> 
                <apex:outputField value="{!prg.Name}"/>
                <apex:outputField value="{!prg.Status__c}"/>
                <apex:outputField value="{!prg.Program_Name__c}"/>
                <apex:outputField value="{!prg.Approved_Amount__c}"/>
                <apex:outputField value="{!prg.Sponsor_Image__c}"/>
                <apex:outputField value="{!prg.PA_Amount__c}"/>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Account"/>
                    <apex:outputText value="{!prg.Contact__r.Account.Name}" id="acc"/>
                </apex:pageBlockSectionItem>
                <apex:outputField value="{!prg.Invoiced_Amount__c}"/>
                <apex:pageBlockSectionItem rendered="{!(!editMode)}">
                    <apex:outputLabel value="Contact"/>
                    <apex:outputText value="{!prg.Contact__r.Name}"/>
                </apex:pageBlockSectionItem>
                <apex:inputField value="{!prg.Contact__c}" rendered="{!editMode}"/>
                <apex:pageBlockSectionItem rendered="{!(!editMode)}">
                    <apex:outputLabel value="Program Lead"/>
                    <apex:outputText value="{!prg.Program_Manager__r.Name}"/>
                </apex:pageBlockSectionItem>
                <apex:inputField value="{!prg.Program_Manager__c}" rendered="{!editMode}"/>
                <apex:outputField value="{!prg.Therapeutic_Area__c}"/>
            </apex:pageBlockSection> 
        </apex:pageBlock>
    
        <apex:pageBlock title="Protocols" rendered="{!(!editMode)}"/>
        <apex:outputPanel id="protocolPanel" rendered="{!(!editMode)}">
            <table class="display" id="protocolTable">
                <thead>
                    <tr>
                        <th class="contentCenter">Action</th>
                        <th class="contentCenter">ID</th>
                        <th class="contentCenter">Protocol Name</th>
                        <th class="contentCenter"># of Countries</th>
                        <th class="contentCenter">Status</th>
                        <th class="contentCenter">Approved Amt</th>
                        <th class="contentCenter">PA Amt</th>
                        <th class="contentCenter">Invoiced Amt</th>
                    </tr>
                </thead>
                <tbody>
                    <apex:repeat value="{!lstProtocolWrapper}" var="protocols">
                        <tr>
                            <td class="contentCenter"> <a class="cursor" onclick="window.open('/apex/GLSLPW_ProtocolDetailPage?id={!protocols.proId}', '_self');">View</a>&nbsp;                
                            </td>
                            <td class="contentCenter"> <apex:outputText value="{!protocols.protocolId}"/> </td>
                            <td class="contentCenter"> <apex:outputText value="{!protocols.protocolName}"/> </td>
                            <td class="contentCenter"> <apex:outputText value="{0, number, 0}"><apex:param value="{!protocols.numCountries}"/>
                                                        </apex:outputText></td>
                            <td class="contentCenter"> <apex:outputText value="{!protocols.status}"/> </td>
                            <td class="contentCenter"> <apex:outputText value="${0, number,##,##0.00}"><apex:param value="{!protocols.approvedAmt}"/>
                                                        </apex:outputText></td>
                            <td class="contentCenter"> <apex:outputText value="${0, number,##,##0.00}"><apex:param value="{!protocols.poAmt}"/>
                                                        </apex:outputText></td>
                            <td class="contentCenter"> <apex:outputText value="${0, number,##,##0.00}"><apex:param value="{!protocols.consumedAmt}"/>
                                                        </apex:outputText></td>
                        </tr>
                    </apex:repeat>
                </tbody>
            </table>
        </apex:outputPanel>
        <br/>
        <apex:pageBlock title="Upload Files" rendered="{!(!editMode)}"/>
        <apex:iframe src="" id="FileUploadComponent" height="150px" rendered="{!(!editMode)}"/>
        <apex:pageBlock title="Reference Files" rendered="{!(!editMode)}">
            <apex:pageBlockButtons location="top">
                <apex:commandButton value="Refresh Files" action="{!getReferenceFiles}" rerender="refFilesPanel"/>
            </apex:pageBlockButtons>
        </apex:pageBlock>
        <apex:outputPanel id="refFilesPanel" rendered="{!(!editMode)}">
            <table class="display" id="refFilesTable" cellSpacing="0" cellPadding="5">
                <thead>
                    <tr>
                        <th class="contentCenter">Action</th>
                        <th class="contentCenter">Reference File ID</th>
                        <th class="contentCenter">Reference File Name</th>
                        <th class="contentCenter">Description</th>
                        <th class="contentCenter">Uploaded By</th>
                        <th class="contentCenter">Uploaded Date</th>
                    </tr>
                </thead>
                <tbody>
                    <apex:repeat value="{!lstRefFiles}" var="refFiles">
                        <tr>
                            <td class="contentCenter">   
                                 <a class="cursor" onclick="window.open('/{!refFiles.Id}', '_blank');">Edit</a>&nbsp;/&nbsp;
                                 <a class="cursor" onclick="downloadFile('{!refFiles.Id}');">View</a>&nbsp;
                                 <apex:outputPanel rendered="{!enableDelete}">/&nbsp;                   
                                 <a class="cursor" onclick="deleteFile('{!refFiles.Id}', '{!refFiles.Program_File_Name__c}');">Delete</a> 
                                 </apex:outputPanel>
                            </td>
                            <td class="contentCenter"> <apex:outputText value="{!refFiles.Name}"/> </td>
                            <td class="contentCenter"> <apex:outputText value="{!refFiles.Program_File_Name__c}"/> </td>
                            <td class="contentCenter"> <apex:outputText value="{!refFiles.Description__c}"/> </td>
                            <td class="contentCenter"> <apex:outputText value="{!refFiles.CreatedBy.Name}"/> </td>
                            <td class="contentCenter"> <apex:outputText value="{0, date, MM'/'dd'/'yyyy}"> <apex:param value="{!refFiles.CreatedDate}"/>
                                                        </apex:outputText></td>
                        </tr>
                    </apex:repeat>
                </tbody>
            </table>
        </apex:outputPanel>
        <br/>
        <apex:pageBlock title="Estimate History" rendered="{!(!editMode)}"/>
        <apex:outputPanel id="estimatePanel" rendered="{!(!editMode)}">
            <table class="display" id="estimateTable">
                <thead>
                    <tr>
                        <th class="contentCenter">Action</th>
                        <th class="contentCenter">Estimate #</th>
                        <th class="contentCenter">Amount</th>
                        <th class="contentCenter">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <apex:repeat value="{!lstEstimate}" var="estimates">
                        <tr>
                            <td class="contentCenter"> <a class="cursor" 
                                                onclick="window.open('/apex/GLSLPW_EstimateDetailPage?id={!estimates.Id}', '_self');">View</a></td>
                            <td class="contentCenter"> <apex:outputText value="{!estimates.LP_Estimate_ID__c}"/> </td>
                            <td class="contentCenter"> <apex:outputText value="${0, number,##,##0.00}"><apex:param value="{!estimates.Estimate_Amount__c}"/>
                                                        </apex:outputText></td>
                            <td class="contentCenter"> <apex:outputText value="{!estimates.Status__c}"/> </td>
                        </tr>
                    </apex:repeat>
                </tbody>
            </table>
        </apex:outputPanel>
    </apex:form>
    <script>
        $(document).ready(function() {
                createProtocolTable();
                createEstimateTable();
                createRefFilesTable();                
                $("[id$='FileUploadComponent']").attr("src","{!URLFOR($Page.AWSFileProgramUploadPage)}?id={!Program__c.id}");
        });
        
        function createProtocolTable(){
             var oTable =  $('#protocolTable').dataTable( { 
                         "paging":   false,
                         "ordering": false,
                         "info":     false,
                         "bFilter" : false,               
                         "bLengthChange": false,
                         "bInfo": false, 
                         "bPaginate": false,
                         "oLanguage": {"sSearch": "false"},
                         "sPaginationType": "full_numbers",
                         "oLanguage": {"sLengthMenu": "Show _MENU_ records per page"},
                         "aoColumns": [null,null,null,null,null,null,null,null]                    
            });  
        }   
        
        function createEstimateTable(){
             var oTable =  $('#estimateTable').dataTable( { 
                         "paging":   false,
                         "ordering": false,
                         "info":     false,
                         "bFilter" : false,               
                         "bLengthChange": false,
                         "bInfo": false, 
                         "bPaginate": false,
                         "oLanguage": {"sSearch": "false"},
                         "sPaginationType": "full_numbers",
                         "oLanguage": {"sLengthMenu": "Show _MENU_ records per page"},
                         "aoColumns": [null,null,null,null]                    
            });  
        } 
        
        function createRefFilesTable(){
             var oTable =  $('#refFilesTable').dataTable( { 
                         "paging":   false,
                         "ordering": false,
                         "info":     false,
                         "bFilter" : false,               
                         "bLengthChange": false,
                         "bInfo": false, 
                         "bPaginate": false,
                         "oLanguage": {"sSearch": "false"},
                         "sPaginationType": "full_numbers",
                         "oLanguage": {"sLengthMenu": "Show _MENU_ records per page"},
                         "aoColumns": [null,null,null,null,null,null]                    
            });  
        } 
        
        function downloadFile(refFileId){
            downloadFileAction(refFileId);
        }
        
        function deleteFile(refFileId, fileName){
            var result=confirm('Are you sure to Delete the File: '+fileName+'?');
            if(result ==true){
                deleteFileAction(refFileId);
            }
        }
        
        function openPageEditMode(){
            window.open('/apex/GLSLPW_ProgramDetailPage?id={!prg.Id}','_self');
        }
        
        function openLookup(baseURL, width, modified, searchParam){
            var originalbaseURL = baseURL;
            var originalwidth = width;
            var originalmodified = modified;
            var originalsearchParam = searchParam;
            var lookupType = baseURL.substr(baseURL.indexOf("&lktp=")+6, 3);
            if (modified == '1'){
                baseURL = baseURL + searchParam;
            }
            var isCustomLookup = false;
            
            if(lookupType == "003"){
                var urlArr = baseURL.split("&");
                var txtId = '';
                if(urlArr.length > 2) {
                    urlArr = urlArr[1].split('=');
                    txtId = urlArr[1];
                }
                
                clientID = '{!prg.Contact__r.Account}';
                baseURL = "/apex/CustomContactLookup?txt=" + txtId;
                baseURL = baseURL + "&account=" + escapeUTF(clientID);
                
                baseURL = baseURL + "&frm=" + escapeUTF("{!$Component.theForm}");
                if (modified == '1') {
                    baseURL = baseURL + "&lksearch=" + searchParam;
                }
                
                isCustomLookup = true;
            }
            console.log('isCustomLookup'+isCustomLookup);
            if(isCustomLookup == true){
                if(clientID=='' || clientID==null){
                }else{
                    openPopup(baseURL, "lookup", 350, 480, "width="+width+",height=480,toolbar=no,status=no,directories=no,menubar=no,resizable=yes,scrollable=no", true);
                }
            }
            else {
                if (modified == '1') originalbaseURL = originalbaseURL + originalsearchParam;
                openPopup(originalbaseURL, "lookup", 350, 480, "width="+originalwidth+",height=480,toolbar=no,status=no,directories=no,menubar=no,resizable=yes,scrollable=no", true);
            }
        }
    </script> 
    
</apex:page>