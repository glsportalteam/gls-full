trigger LMS_PopulateUserCreatedDate on User (after Insert,after update) {
    public List<User> userList= new List<User>();
    
    public List<Training__c> allTrainings;
   // public RecordType trainingRT;
    public List<User_Training__c> userEnrollmentRecords;
    public List<User_Training__c> enrollmentsToBeDeleted;
    public User_Training__c singleUserEnrollment;
    public Map<Id,Set<String>> mapString;
    
    if(Trigger.isInsert && Trigger.isAfter) {
        User user = new User();
     //   trainingRT = new RecordType();
     User_Training__c objUserTraining  = new User_Training__c();
        userEnrollmentRecords = new List<User_Training__c>(); 
        singleUserEnrollment = new User_Training__c();
      //  trainingRT = [ select Id, Name, SobjectType from RecordType where SobjectType = 'Training__c' and Name = :LMS_Literals.ProductObjectTrainingRecordTypeName];
        allTrainings = [ select Id, Name, criterion__c, End_Date__c, Active__c,
                                Pre_requisites__c, Seats__c,  Start_Date__c, Trainer_Name__c, Training_Description__c,
                                Training_Methodologies__c, Training_Required_for__c, URL__c, Venue__c, Duration__c
                               //  (Select Id, Training__c from User_Trainings__r Where Status__c ='Completed' and Status__c ='Feedback Pending')
                         from Training__c where Active__c = true
                         ];
        
         
        for (User newUser : Trigger.new) {
            user = new User(id=newUser.Id);
            user.Custom_Created_Date__c = newUser.CreatedDate;
            userList.add(user);
             
            for(Training__c training : allTrainings ) {
                if(newUser.Training_Profile__c != null 
                    && training.Training_Required_for__c != null && training.Training_Required_for__c != '' 
                    && training.Training_Required_for__c.contains(newUser.Training_Profile__c)
                    && training.Active__c == true) {
                   /*      system.debug('---training.User_Trainings__r ---: '+training.User_Trainings__r); 
                        if(training.User_Trainings__r != null && training.User_Trainings__r.size()>0){
                            for(User_Training__c ut: training.User_Trainings__r){
                                system.debug('---User training Name ---: '+ut.Training__c); 
                                if(ut.Training__c != training.Id){
                                    objUserTraining = userTrainingCreator(training.End_Date__c, training.Duration__c,training.Criterion__c, newUser.Id, training.Id);
                                    if(objUserTraining != null)
                                        userEnrollmentRecords.add(objUserTraining);
                                }   
                            }    
                        }else{*/
                            objUserTraining = userTrainingCreator(training.End_Date__c, training.Duration__c,training.Criterion__c, newUser.Id, training.Id);
                            system.debug('----objUserTraining----: '+objUserTraining);
                            if(objUserTraining != null)
                                userEnrollmentRecords.add(objUserTraining);
                     //   }
                         
                    }
                }
            }            
        
        if (userList.size() > 0) {
            try{
                update userList;
            }catch(DMLException de){
                system.debug('---DML Exception Occured---'+de);
            }
        }
        
        if (userEnrollmentRecords!= null && userEnrollmentRecords.size() > 0) {
            try{
                insert userEnrollmentRecords;
            }catch(DMLException de){
                system.debug('---DML Exception Occured---'+de);
            }   
        }
}
    
    if(Trigger.isUpdate && Trigger.isAfter) {
        User user = new User();
        mapString = new Map<Id,Set<String>>();
        userEnrollmentRecords = new List<User_Training__c>(); 
        User_Training__c objUserTraining = new User_Training__c();
        enrollmentsToBeDeleted = new List<User_Training__c>(); 
        singleUserEnrollment = new User_Training__c();
        List<String> lstFutureUsersId = new List<String>();
        allTrainings = [ select Id, Name, criterion__c,  End_Date__c, Active__c, 
                                Pre_requisites__c, Seats__c,  Start_Date__c, Trainer_Name__c, Training_Description__c,
                                Training_Methodologies__c, Training_Required_for__c, URL__c, Venue__c,Duration__c,
                                (Select Id, Training__c,name, User_name__c from User_Trainings__r )
                               
                         from Training__c Where Active__c = true And Training_Required_for__c != null
                        ];
                        List<User> usrlst= new List<User>();
         for (User newUser : Trigger.new) {  
            usrlst.add(newUser);
         }
       
       system.debug('---allTrainings---: '+allTrainings);
       for (Training__c allTr : allTrainings) {
            set<string> username =new set<string>();
            if(allTr.User_Trainings__r != null){
                for(User_Training__c userT: allTr.User_Trainings__r){
                            username.add(userT.User_name__c);
                }
            }
            mapString.put(allTr.Id, username);
        }
       
       
       Map<Id,User_Training__c> tempDeleteList = new Map<Id,User_Training__c>([ select Id, Name, Training__c, User_Name__c, Status__c from User_Training__c where  (Status__c != 'Completed' or   Status__c !='Feedback Pending') and User_Name__c in :usrlst ]);
        
        system.debug('---isUserUpdated---: '+LMS_UpdateUserFuture.isUserUpdated);
        
        for (User newUser : Trigger.new) {  
            for (User oldUser : Trigger.old) {
                if(oldUser.Id == newUser.Id && ((oldUser.Training_Profile__c != newUser.Training_Profile__c ) || LMS_UpdateUserFuture.isUserUpdated == true )&& (oldUser.IsActive == newUser.IsActive && newUser.IsActive == true)) {
                   system.debug('----inside trigger-----' );
                        String trainingProfile = newUser.Training_Profile__c;
                        if(newUser.Training_Profile__c == null || newUser.Training_Profile__c == '' )
                             trainingProfile = 'None';
                        
                   /** 
                   * Delete User Training Records
                   */
                        for(Training__c training : allTrainings ) {
                            set<string> tempSet=mapString.get(training.id);
                            
                            for(User_Training__c usrt : tempDeleteList.values()){
                                
                                if(usrt.Training__c == training.id){
                                     if(usrt.Status__c != 'Completed' && usrt.Status__c != 'Feedback Pending' && usrt.User_Name__c == (newUser.Id) && !training.Training_Required_For__c.contains(trainingProfile)){
                                            enrollmentsToBeDeleted.add(usrt);
                                            system.debug('---enrollmentsToBeDeleted---: '+enrollmentsToBeDeleted); 
                                     } 
                                }
                            }
                                
                        
                    /** 
                    *   Create User Training Records
                    */
                        system.debug('----tempSet----: '+tempSet);
                       
                        if(tempSet.size()>0){
                            if(!tempSet.contains(newUser.Id) && training.Training_Required_for__c.contains(trainingProfile) && training.Active__c == true){
                                    system.debug('*****inside else*****');
                                    objUserTraining = userTrainingCreator(training.End_Date__c, training.Duration__c,training.Criterion__c, newUser.Id, training.Id);
                                        if(objUserTraining != null)
                                            userEnrollmentRecords.add(objUserTraining);
                              } 
                        }else if(training.Training_Required_for__c.contains(trainingProfile) && training.Active__c == true){
                                objUserTraining = userTrainingCreator(training.End_Date__c, training.Duration__c,training.Criterion__c, newUser.Id, training.Id);
                                    if(objUserTraining != null)
                                        userEnrollmentRecords.add(objUserTraining);
                        }   
                      } 
                   }else if ((oldUser.Id == newUser.Id && oldUser.IsActive != newUser.IsActive && newUser.IsActive == true)) {
                        system.debug('----inside Else----');
                        lstFutureUsersId.add(newUser.Id);
                    }
                }
        }
      
      system.debug('----lstFutureUsersId---- : '+lstFutureUsersId);
      if (lstFutureUsersId != null && lstFutureUsersId.size() > 0) {
            try{
                LMS_UpdateUserFuture.updateUser(lstFutureUsersId);
            }catch(Exception e){
                system.debug('--- Exception Occured---'+e);
            }   
        }  
     
      if (enrollmentsToBeDeleted != null && enrollmentsToBeDeleted.size() > 0) {
            try{
                delete enrollmentsToBeDeleted;
            }catch(DMLException de){
                system.debug('---DML Exception Occured---'+de);
            }   
        }  
      
       system.debug('---userEnrollmentRecords---: '+userEnrollmentRecords); 
        if (userEnrollmentRecords!= null && userEnrollmentRecords.size() > 0) {
           try{
                insert userEnrollmentRecords; 
           }catch(DMLException de){
                system.debug('---DML Exception Occured---'+de);
           }     
        }
              
    }
    
    private User_Training__c userTrainingCreator(Date endDate, Decimal tDuration, Decimal criteria, Id userId, Id trainingId){
        system.debug('---inside userTrainingCreator----');
               Decimal  duration =0;
               if(criteria == null)
                criteria = 30;
               List<User_Training__c> lstUserTraining = new List<User_Training__c>();
                singleUserEnrollment = new User_Training__c();
                singleUserEnrollment.Training__c = trainingId;
                if(endDate != null)
                {
                    if(tDuration != null)
                        duration = tDuration;
                        if((Date.today().addDays(Integer.valueOf(duration))) <=  endDate)
                        {
                            
                            if( Date.today().addDays(Integer.valueOF(criteria)) <
                             endDate )
                            {
                                    singleUserEnrollment.Due_Date__c = Date.today().addDays(Integer.valueOF(criteria));
                            }
                            else{
                                    singleUserEnrollment.Due_Date__c = endDate;
                            }
                        }
                }else{
                
                    singleUserEnrollment.Due_Date__c =Date.today().addDays(Integer.valueOf(criteria));
                }
                if(singleUserEnrollment.Due_Date__c != null){
                    singleUserEnrollment.User_Name__c = userId;
                    singleUserEnrollment.Status__c = LMS_Literals.InitialEnrollmentStatus;
                    singleUserEnrollment.Date_of_Enrollment__c = Date.today();
                    //lstUserTraining.add(singleUserEnrollment);
                }else return null;
            
                if(singleUserEnrollment != null)
                    return singleUserEnrollment;
                else
                    return null;
        }
}