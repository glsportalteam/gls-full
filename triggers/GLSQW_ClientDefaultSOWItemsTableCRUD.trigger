trigger GLSQW_ClientDefaultSOWItemsTableCRUD on Client_Default_SOW_Items_Table__c (after insert, before delete) {


set<id> clientid= new set<id>();
	set<id> parentid= new set<id>();
	set<id >cdttid= new set <id>();
	list<Account> lstAccount = new list<Account>();
	 Client_Default_SOW_Items_Table__c clonecdtt = new  Client_Default_SOW_Items_Table__c();
	list< Client_Default_SOW_Items_Table__c> lstClientDefaultTermTable= new list< Client_Default_SOW_Items_Table__c>();
	
	list< Client_Default_SOW_Items_Table__c> updateLstClientDefaultTermTable= new list< Client_Default_SOW_Items_Table__c>();
		if(trigger.isInsert){
			for( Client_Default_SOW_Items_Table__c cdtt : trigger.new){
				if(cdtt.Client_Id__c !=null){
					clonecdtt =Trigger.newmap.get(cdtt.Id);
					
					clientid.add(cdtt.Client_Id__c);
				}
			}
		
		system.debug('line17'+clientid + 'inserted value'+clonecdtt);
		if(clientid != null && clientid.size()>0){
			
			lstAccount = [select id from Account where 	Parentid in : clientid];
		}
		if(lstAccount != null && lstAccount.size()>0){
			for(Account acc : lstAccount){
				
				parentid.add(acc.id);
			}
		}
		system.debug('line28'+parentid);
	  if(parentid != null && parentid.size()>0){
	  	
	  	lstClientDefaultTermTable = [select id,Client_Id__c,Standard_SOW_Item_Id__c from  Client_Default_SOW_Items_Table__c where Client_Id__c  in :parentid];
	  	
	  }
		 if(lstClientDefaultTermTable != null && lstClientDefaultTermTable.size()>0){
		  for( Client_Default_SOW_Items_Table__c cdtt2 :lstClientDefaultTermTable){
		 
		 	cdttid.add(cdtt2.Client_Id__c);
		 	}
		  }
	 if(lstAccount != null && lstAccount.size()>0){
	 	for( Client_Default_SOW_Items_Table__c cdttt : trigger.new){
	 		clonecdtt =Trigger.newmap.get(cdttt.Id);
			 for(Account acc1 :lstAccount ){
					 if(lstClientDefaultTermTable != null && lstClientDefaultTermTable.size()>0){
					 
					 	boolean check1=false;
					 	for( Client_Default_SOW_Items_Table__c cdtt1 :lstClientDefaultTermTable){
						 	if(acc1.id==cdtt1.Client_Id__c){
						 		 Client_Default_SOW_Items_Table__c	temp = new  Client_Default_SOW_Items_Table__c();
						 			if(check1== false){
						 				if(clonecdtt.Standard_SOW_Item_Id__c ==cdtt1.Standard_SOW_Item_Id__c){
						 					check1=true;
						 			    }
						 		   }
						 	 }
					 	}
					 	
					 	if(check1==false){
					 				 Client_Default_SOW_Items_Table__c newcdtt= new  Client_Default_SOW_Items_Table__c();
					 			newcdtt.Client_Id__c=acc1.id;
					 			newcdtt.Order_on_Quote__c=clonecdtt.Order_on_Quote__c;
					 			newcdtt.Standard_SOW_Item_Id__c=clonecdtt.Standard_SOW_Item_Id__c;
					 			
					 				updateLstClientDefaultTermTable.add(newcdtt);
					 	}
					 }
		     }
	
	 	}
	 }
	 system.debug('line45'+updateLstClientDefaultTermTable);
		 if(updateLstClientDefaultTermTable !=null && updateLstClientDefaultTermTable.size()>0){
		 	try{
		 		Insert updateLstClientDefaultTermTable;
		 	}
		 	catch (Exception e){
		 		system.debug('-----Exception-----'+ e.getmessage());
		 		
		 	}
		 	
		 }
	}
	if(trigger.isdelete){
			for( Client_Default_SOW_Items_Table__c cdtt : trigger.old){
				
				if(cdtt.Client_Id__c !=null){
					clonecdtt =Trigger.oldmap.get(cdtt.Id);
				
					clientid.add(cdtt.Client_Id__c);
				}
			}
		
		system.debug('line17'+clientid + 'inserted value'+clonecdtt);
		if(clientid != null && clientid.size()>0){
			
			lstAccount = [select id from Account where 	Parentid in : clientid];
		}
		if(lstAccount != null && lstAccount.size()>0){
			for(Account acc : lstAccount){
				
				parentid.add(acc.id);
			}
		}
		system.debug('line28'+parentid);
	  if(parentid != null && parentid.size()>0){
	  	
	  	lstClientDefaultTermTable = [select id,Client_Id__c,Standard_SOW_Item_Id__c from  Client_Default_SOW_Items_Table__c where Client_Id__c  in :parentid];
	  	
	  }
	 if(lstAccount != null && lstAccount.size()>0){
	 	for( Client_Default_SOW_Items_Table__c cdttt : trigger.old){
	 		clonecdtt = new  Client_Default_SOW_Items_Table__c();
	 		clonecdtt =Trigger.oldmap.get(cdttt.Id);
		for(Account acc1 :lstAccount ){
				 if(lstClientDefaultTermTable != null && lstClientDefaultTermTable.size()>0){
				 	
				 	for( Client_Default_SOW_Items_Table__c cdtt1 :lstClientDefaultTermTable){
				 		if(acc1.id==cdtt1.Client_Id__c){
				 			if(clonecdtt.Standard_SOW_Item_Id__c ==cdtt1.Standard_SOW_Item_Id__c){
				 			
				 				updateLstClientDefaultTermTable.add(cdtt1);
				 			}
				 		}
				 	  }
				 	}
				 }
		  }
	 }
	 
	 system.debug('line45'+updateLstClientDefaultTermTable);
		 if(updateLstClientDefaultTermTable !=null && updateLstClientDefaultTermTable.size()>0){
		 	try{
		 		delete updateLstClientDefaultTermTable;
		 	}
		 	catch (Exception e){
		 		system.debug('-----Exception-----'+ e.getmessage());
		 		
		 	}
		 	
		 }
		
		
	}	
}