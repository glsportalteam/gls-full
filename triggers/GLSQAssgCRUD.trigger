trigger GLSQAssgCRUD on Quote_Assignment__c (after update,before insert, before update) {
    if(Trigger.isBefore && Trigger.isUpdate){
        string fileFormat = '';
        String description = '';
        List<Id> qaID = new List<Id>();
        List<Quote_Assignment_Service__c> qasServices = new List<Quote_Assignment_Service__c>();
        List<Quote_Assignment_Language__c> qasTargetLanguages = new List<Quote_Assignment_Language__c>();
        List<Quote_Assignment__c> lstQAssg = new List<Quote_Assignment__c>();
        map<Id,string> sourceLangMap = new map<Id,string>();
        for(Quote_Assignment__c qAssg : Trigger.new){
            qaID.add(qAssg.id);
        }
        if(qaID != null && qaID.size() > 0){
            qasServices = [Select Client_Service__r.Name, Service_Name__c, Client_Service__r.Order__c, Quote_Assignment__c 
                                                             From Quote_Assignment_Service__c Where Quote_Assignment__c in: qaID 
                                                             order by Client_Service__r.Order__c,Client_Service__r.Name];
            qasTargetLanguages = [Select Language_List_Item__c,Language_List_Item__r.Name, Language_List_Item__r.Order__c,   
                                                                     Quote_Assignment__c From Quote_Assignment_Language__c Where Quote_Assignment__c in: qaID 
                                                                     order by Language_List_Item__r.Name];
            lstQAssg = [Select Id,Source_Language__r.Name From Quote_Assignment__c Where Id in: qaID];
            for(Quote_Assignment__c qa : lstQAssg){
                sourceLangMap.put(qa.id,qa.Source_Language__r.Name);
            }
        }
        for(Quote_Assignment__c qAssg : Trigger.new){
            if(qAssg.Allowed_File_Formats__c != null){
                string myString = qAssg.Allowed_File_Formats__c;
                list<string> lstFormat = myString.split(';');
                if(lstFormat.size() == 1){
                    fileFormat = lstFormat[0]; 
                }
                else if(lstFormat.size() == 2){
                    fileFormat = fileFormat+lstFormat[0]+' and '+lstFormat[1];
                }
                else if(lstFormat.size() > 2){
                    for(integer i = 0; i < lstFormat.size();i++){
                        if(i != lstFormat.size()-1){
                            fileFormat = fileFormat +lstFormat[i]+', ';
                        }
                        if(i == lstFormat.size()-1){
                            integer index = fileFormat.lastIndexOf(',');
                            fileFormat = fileFormat.substring(0,index)+', and '+lstFormat[i];
                        }
                    }
                }
                qAssg.Delivery_Format__c = fileFormat;
            }else{
                qAssg.Delivery_Format__c = '';
            } 
            list<string> lstService = new List<string>();
            for(Quote_Assignment_Service__c qas:qasServices){
                if(qas.Quote_Assignment__c == qAssg.Id){
                    lstService.add(qas.Client_Service__r.Name);
                }
            }
            set<string> settargetLanguage = new set<String>();
            list<string> lstTargetLanguage= new List<string>();
            for(Quote_Assignment_Language__c qal:qasTargetLanguages){
                String tmpStr; 
                if(qal.Quote_Assignment__c == qAssg.Id){
                    if(!qAssg.Display_Languages_Without_Dialect__c){
                        tmpStr = qal.Language_List_Item__r.Name;
                        if(tmpStr.contains('(')){
                            integer indexOfbrackets = tmpStr.indexOf('(');
                            settargetLanguage.add(tmpStr.substring(0,indexOfbrackets-1));
                        }else{
                            settargetLanguage.add(qal.Language_List_Item__r.Name);
                        }
                    }else{
                        settargetLanguage.add(qal.Language_List_Item__r.Name);
                    }
                }
            }       
            lstTargetLanguage.addAll(settargetLanguage);
            lstTargetLanguage.sort();
            string listOfService = '';
            if(lstService.size() > 0){
                if(lstService.size() == 1){
                    listOfService = lstService[0];
                }else if(lstService.size() == 2){
                    listOfService = listOfService+lstService[0]+' and '+lstService[1];
                }else if(lstService.size()> 2){
                    for(integer i = 0; i < lstService.size();i++){
                        if(i != lstService.size()-1){
                            listOfService = listOfService +lstService[i]+', ';
                        }
                        if(i == lstService.size()-1){
                            integer index = listOfService.lastIndexOf(',');
                            listOfService = listOfService.substring(0,index)+', and '+lstService[i];
                        }
                    }
                }
            }
            string listOfLanguage = '';    
            if(lstTargetLanguage.size() > 0){
                if(lstTargetLanguage.size() == 1){
                    listOfLanguage = lstTargetLanguage[0]; 
                }else if(lstTargetLanguage.size() == 2){
                    listOfLanguage = listOfLanguage+lstTargetLanguage[0]+' and '+lstTargetLanguage[1];
                }else if(lstTargetLanguage.size()> 2){
                    for(integer i = 0; i < lstTargetLanguage.size();i++){
                        if(i != lstTargetLanguage.size()-1){
                            listOfLanguage = listOfLanguage +lstTargetLanguage[i]+', ';
                        }
                        if(i == lstTargetLanguage.size()-1){
                            integer index = listOfLanguage.lastIndexOf(',');
                            listOfLanguage = listOfLanguage.substring(0,index)+', and '+lstTargetLanguage[i];
                        }
                    }
                }
            }
            string quoteTaskDesc = '';
            String sourceLang = '';
            if(lstTargetLanguage.size() > 0 && lstService.size() > 0 && sourceLangMap.get(qAssg.Id) != null){
                if(!qAssg.Display_Languages_Without_Dialect__c){
                    String tmpStr = sourceLangMap.get(qAssg.Id);
                    if(tmpStr.contains('(')){
                        integer indexOfbrackets = tmpStr.indexOf('(');
                        sourceLang = tmpStr.substring(0,indexOfbrackets-1);
                    }else{
                        sourceLang = sourceLangMap.get(qAssg.Id);
                    }
                }else{
                    sourceLang = sourceLangMap.get(qAssg.Id);
                }
                if(qAssg.Certified__c){
                    quoteTaskDesc = 'Certified '+listOfService +' of the following document(s) from '+sourceLang+' into '+listOfLanguage+':\n'; 
                }
                else{
                    quoteTaskDesc = listOfService +' of the following document(s) from '+sourceLang+' into '+listOfLanguage+':\n';
                }
                
            }
            qAssg.Quote_Task_Description__c = quoteTaskDesc;
            //qAssg.isAssignmentUpdated__c = true;
        }
    }
    if(Trigger.isBefore && Trigger.isInsert){
        String description = '';
        //changes 29/09/2014 satrt
        String combinedFileformat='';
        //changes 29/09/2014 ends
        Quote__c parentQuote = new Quote__c();
        Map<String, String> mapFileExt = new Map<String, String>();
        List<id> quoteId = new List<id>();
        List<Trados_Supported_Files__c> lstTSF = Trados_Supported_Files__c.getall().values();
        
        for(Quote_Assignment__c qAssg : Trigger.new){
            quoteId.add(qAssg.Quote__c);
            system.debug('line 151'+qAssg.Allowed_File_Formats__c);
        }
        List<Quote__c> lstQuotes = [Select Id, Display_Languages_Without_Dialect__c, Certified__c, 
                                   (Select Id, File_Type__c, File_Name__c, File_Format__r.Name From Quotes_Files__r Where File_Type__c = 'Source') 
                                   From Quote__c Where Id in: quoteId];

        for(Trados_Supported_Files__c TSF: lstTSF){
            mapFileExt.put(TSF.Extension_Name__c,TSF.Name);
        }
        String fileFormatNames = '';
        for(Quote__c quote : lstQuotes){
            fileFormatNames = '';
            for(Quotes_File__c qf : quote.Quotes_Files__r){
                if(qf.File_Format__r.Name != null){
                    if(fileFormatNames == ''){
                        fileFormatNames = qf.File_Format__r.Name;
                    }else{
                        if(!fileFormatNames.contains(qf.File_Format__r.Name))
                            fileFormatNames = fileFormatNames+';'+qf.File_Format__r.Name;  
                    }
                }
            }
            
           /* for(Quotes_File__c qf : quote.Quotes_Files__r){
                fileFormatNames += qf.File_Format__r.Name+';';  
            }
            */
            for(Quote_Assignment__c qAssg : Trigger.new){
                system.debug('---fileFormatNames in trigger---: '+fileFormatNames);
                string fileQEFormat = '';   
                string fileFormat = '';
                list<string> lstFormat;
                list<string> lstQEFormat;
                if(quote.id == qAssg.Quote__c){
                    combinedFileformat=fileFormatNames;
                    qAssg.Display_Languages_Without_Dialect__c = quote.Display_Languages_Without_Dialect__c;
                    qAssg.Certified__c = quote.Certified__c;
                    //replaced qAssg.Allowed_File_Formats__c with combinedFileformat
                    if(combinedFileformat!= null){
                    system.debug('line177'+qAssg.Allowed_File_Formats__c);
                       if( qAssg.Allowed_File_Formats__c !=null){
                        string  myString = qAssg.Allowed_File_Formats__c;
                         lstFormat = myString.split(';');
                        }
                        system.debug('line183'+lstFormat);
                        if( lstFormat !=null ){
                        if(lstFormat.size() == 1){
                            fileFormat = lstFormat[0]; 
                        }
                        else if(lstFormat.size() == 2){
                            fileFormat = fileFormat+lstFormat[0]+' and '+lstFormat[1];
                        }
                        else if(lstFormat.size() > 2){
                            for(integer i = 0; i < lstFormat.size();i++){
                                if(i != lstFormat.size()-1){
                                    fileFormat = fileFormat +lstFormat[i]+', ';
                                }
                                if(i == lstFormat.size()-1){
                                    integer index = fileFormat.lastIndexOf(',');
                                    fileFormat = fileFormat.substring(0,index)+' and '+lstFormat[i];
                                }
                            }
                        }
                        }
                        qAssg.Delivery_Format__c = fileFormat;
                    }else{
                        qAssg.Delivery_Format__c = '';
                    } 
                   if(!GLS_CommunityUploadFileController.runTrigger && !GLS_CommunityOrdersController.runTrigger1){
                        qAssg.Allowed_File_Formats__c = fileFormatNames;
                        system.debug('----fileFormatNames----: '+fileFormatNames);
                        if(fileFormatNames != null){
                            string  myQEString = fileFormatNames;
                            lstQEFormat = myQEString.split(';');
                            system.debug('----lstQEFormat----: '+lstQEFormat.size()+'---'+lstQEFormat);
                            if(lstQEFormat !=null ){
                                if(lstQEFormat.size() == 1){
                                    fileQEFormat = lstQEFormat[0]; 
                                }else if(lstQEFormat.size() == 2){
                                    fileQEFormat = fileQEFormat+lstQEFormat[0]+' and '+lstQEFormat[1];
                                }else if(lstQEFormat.size() > 2){
                                    for(integer i = 0; i < lstQEFormat.size();i++){
                                        if(i != lstQEFormat.size()-1){
                                          fileQEFormat = fileQEFormat +lstQEFormat[i]+', ';
                                        }
                                        if(i == lstQEFormat.size()-1){
                                          integer index = fileQEFormat.lastIndexOf(',');
                                          fileQEFormat = fileQEFormat.substring(0,index)+' and '+lstQEFormat[i];
                                        }
                                    }
                                }
                                    
                            }
                            system.debug('----fileQEFormat----: '+fileQEFormat);
                            qAssg.Delivery_Format__c = fileQEFormat;
                     }else{
                        qAssg.Delivery_Format__c = '';
                     }
                   } 
                   if(qAssg.Quote_Task_Description__c != null && qAssg.Quote_Task_Description__c != ''){
                      description = qAssg.Quote_Task_Description__c;
                      if(qAssg.Certified__c && !description.contains('Certified')){
                          qAssg.Quote_Task_Description__c = 'Certified '+description;
                      }else if(!qAssg.Certified__c && description.contains('Certified')){
                          qAssg.Quote_Task_Description__c = description.replaceAll('Certified ','');
                      }
                  }
               }
            }
        }
    }
}