trigger GLSCommunity_UserCRUD on User (after update, after insert) {
	if(trigger.isInsert){
		if(Trigger.new != null && Trigger.new.size()>0){
			List<User> usersLst = Trigger.new;
			if(usersLst != null && usersLst.size()>0){
				List<Id> contactLst = new List<Id>();
				for(User u:usersLst){
					if(u.contactId != null && u.IsActive){
						//con.isPortalActivated__c = true;
						contactLst.add(u.contactId);
					}
				}
				if(contactLst != null && contactLst.size()>0){
					GLSAmazonUtility.processUserRecords(contactLst,true);
				}
			}
		}
	}
	if(Trigger.isUpdate){
		if(Trigger.newMap != null && Trigger.newMap.size() > 0 ){
			List<Id> contactLstActive = new List<Id>();
			List<Id> contactLstInActive = new List<Id>();
			for( Id userId : Trigger.newMap.keySet() ){
			  if(Trigger.oldMap.get( userId ).contactId != null && Trigger.oldMap.get( userId ).IsActive != Trigger.newMap.get( userId ).IsActive ){
			     if(Trigger.newMap.get( userId ).IsActive){
			     	//Contact con = new Contact();
					//con.Id = Trigger.newMap.get( userId ).contactId;
					//con.isPortalActivated__c = true;
					contactLstActive.add(Trigger.oldMap.get( userId ).contactId);
			     }else{
			     	//Contact con = new Contact();
					//con.Id = Trigger.newMap.get( userId ).contactId;
					//con.isPortalActivated__c = false;
					contactLstInActive.add(Trigger.newMap.get( userId ).contactId);
			     }
			  }
			}
			if(contactLstActive != null && contactLstActive.size()>0){
				GLSAmazonUtility.processUserRecords(contactLstActive,true);
			}
			if(contactLstInActive != null && contactLstInActive.size()>0){
				GLSAmazonUtility.processUserRecords(contactLstInActive,false);
			}
		}
	}
}