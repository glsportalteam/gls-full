/** Copyright (c) 2013, Global Languages Solutions, Inc. All rights reserved.
* Trigger   :  GLSAWSFileTrigger trigger
* Discription: Maintain AWS File versioning,If File with same name and type will be 
*			   inserted for more than one time than a new AWC_File_Version__C will be created 
*			   and associated with AWS_Files__c record
*/
trigger GLSAWSFileCRUD on AWS_File__c (after insert, after update) {
	Set<Id> setAWSId = new Set<Id>();     
	//&& !AWSFileUploadController.skipTrigger
	if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){ 		
		list<AWS_File__c> lstAWSFiles = new list<AWS_File__c>();
		Map<Id, AWS_File__c> oldAWSFileMap = new Map<Id, AWS_File__c>(); 
		
		for(AWS_File__c AWSfile :Trigger.New){
	        lstAWSFiles.add(AWSfile);
	    }	    
	    if(Trigger.isUpdate){
	    	oldAWSFileMap = Trigger.oldMap; 
	    }
		AWSTriggerHandler.handleAfterInsertUpdate(lstAWSFiles,Trigger.isUpdate,oldAWSFileMap);     	
	}        
}