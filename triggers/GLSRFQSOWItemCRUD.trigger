trigger GLSRFQSOWItemCRUD on RFQ_SOW_Items_Table__c (before update) {
	system.debug('--inside GLSRFQSOWItemCRUD-- '+trigger.new);
	if(Trigger.isBefore && trigger.isUpdate){
		RFQ_SOW_Items_Table__c oldItem = new RFQ_SOW_Items_Table__c();
		for(RFQ_SOW_Items_Table__c sowItem:trigger.new){
			oldItem = Trigger.oldMap.get(sowItem.Id);
			if(oldItem.RFQ_SOW_Item_Description__c != sowItem.RFQ_SOW_Item_Description__c || oldItem.RFQ_SOW_Item_Name__c != sowItem.RFQ_SOW_Item_Name__c || oldItem.RFQ_SOW_Item_Order__c != sowItem.RFQ_SOW_Item_Order__c){
				sowItem.RFQ_SOW_Item_Modified__c = true;
			}
		}
	}
}