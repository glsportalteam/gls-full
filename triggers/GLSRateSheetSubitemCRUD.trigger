trigger GLSRateSheetSubitemCRUD on Rate_Sheet_Subitem__c (after insert, after update) {
    
    Rate_Sheet_Subitem__c oldRSSI = new Rate_Sheet_Subitem__c();
    
    if(Trigger.isAfter && trigger.isInsert ){
        List<String> lstTC = new List<String>();
        List<Sub_Item_Trados_Category__c> lstSITC = new List<Sub_Item_Trados_Category__c>();
        Sub_Item_Trados_Category__c sitc = new Sub_Item_Trados_Category__c();
        for( Rate_Sheet_Subitem__c rssi : trigger.new){
            if(rssi.Trados_Categories__c != null && oldRSSI.Trados_Categories__c != rssi.Trados_Categories__c){
                lstTC = rssi.Trados_Categories__c.split(',');
                for(String tc : lstTC){
                    sitc.Trados_Category__c = tc;
                    sitc.Rate_Sheet_Subitem__c = rssi.Id;
                    lstSITC.add(sitc);
                    sitc = new Sub_Item_Trados_Category__c();
                }
                lstTC.clear();
            }
        }
        if(lstSITC != null && lstSITC.size() > 0){
            Database.insert(lstSITC);
        }
    }
    if(Trigger.isAfter && trigger.isUpdate ){
        List<String> lstTC = new List<String>();
        List<Id> lstRSSIId = new List<Id>();
        for( Rate_Sheet_Subitem__c rssi : trigger.new ){
            lstRSSIId.add(rssi.Id);
        }
        List<Sub_Item_Trados_Category__c> lstOldSITC = [Select Id,Rate_Sheet_Subitem__c From Sub_Item_Trados_Category__c Where Rate_Sheet_Subitem__c in: lstRSSIId];
        List<Sub_Item_Trados_Category__c> lstSITC = new List<Sub_Item_Trados_Category__c>();
        List<Sub_Item_Trados_Category__c> lstDelSITC = new List<Sub_Item_Trados_Category__c>();
        Sub_Item_Trados_Category__c sitc = new Sub_Item_Trados_Category__c();
        for( Rate_Sheet_Subitem__c rssi : trigger.new){
            oldRSSI = Trigger.oldMap.get(rssi.id);
            if(rssi.Trados_Categories__c != null && oldRSSI.Trados_Categories__c != rssi.Trados_Categories__c){
                lstTC = rssi.Trados_Categories__c.split(',');
                for(Sub_Item_Trados_Category__c sitcOld : lstOldSITC){
                    if(sitcOld.Rate_Sheet_Subitem__c == rssi.Id){
                        lstDelSITC.add(sitcOld);
                    }
                }
                for(String tc : lstTC){
                    sitc.Trados_Category__c = tc;
                    sitc.Rate_Sheet_Subitem__c = rssi.Id;
                    lstSITC.add(sitc);
                    sitc = new Sub_Item_Trados_Category__c();
                }
                lstTC.clear();
            }
        }
        if(lstSITC != null && lstSITC.size() > 0){
            Database.insert(lstSITC);
        }
        if(lstDelSITC != null && lstDelSITC.size() > 0){
            Database.delete(lstDelSITC);
        }
    }
}